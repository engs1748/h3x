#  ifndef _CPREC_
#  define _CPREC_

/*3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
         1         2         3         4         5         6         7         8         9         0         1         2

   Author          Luca di Mare <l.di.mare@ic.ac.uk>
   Created         Wed Jul 14 18:11:25 BST 2010
   Changes History -
   Next Change(s)  ( work in progress )
 */
            
#    define INT_                         int
#    define UINT_                        unsigned int
#    define REAL_                        double

     typedef int INT_2[2];
     typedef int INT_4[4];

/**@ingroup precision */

/**@{*/

/** Print to std::iostream the size of C/C++ default integer, real and pointer types, as declared by 
    INT, REAL and void *. 
   @brief C/C++ precision enquiry.
  */
     void                               cprecenq( );

/** Print to standard output the size of Fortran default integer, real and pointer types, as declared by 
    INT, REAL and PTR.
   @brief Fortran precision enquiry.
  */
     extern "C" void                    fprecenq( );

/**@}*/

#endif
