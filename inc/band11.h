#  ifndef _BANMXD_

#  include <sizes.h>
#  include <cprec.h>
#  include <cassert>
#  include <cstdio>
#  include <cstring>

#  define D 4
#  include <block.h>

   void  ilus( block_t mat, block_t vec, INT_ n, REAL_ *a, REAL_ *b );
   void  iluf( block_t mat, INT_ n, REAL_ *a );
   void adotx( block_t mat, block_t vec, REAL_ *a, REAL_ *x, REAL_ *y );

#  endif
