#  ifndef _QV_
#  define _QV_

#  include <cstdio>
#  include <cstdlib>
#  include <cmath>
#  include <cassert>
#  include <cstring>

#  include <typd.h>
#  include <dir.h>
#  include <vsize.h>


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Mon  6 Aug 23:58:37 BST 2018
// Changes History -
// Next Change(s)  -

/** Data structure to represent faces in a hexahedral unstructured grid.
    Vertices are numbered as follows:

    2 ----- 3
             
    |       |
    |       |
             
    0 ----- 1

    **/

   struct qvad_t
  {
      INT_            h[2]; /** the exahedra attached to the face **/
      INT_            q[2]; /** the position of the face on the two exahedra **/

      qvad_t()
     {
         h[0]= -1;
         h[1]= -1;
         q[0]= -1;
         q[1]= -1;
     };

/** Write to file renaming hexahedra according to mask **/

      void fwrite( FILE *f, INT_ *mask )
     {
         char buf[512];
         INT_     h1[2];
         size_t len=0;

         h1[0]= h[0];
         h1[1]= h[1];

         if( h1[0] > -1 ){ h1[0]= mask[h[0]]; }// note this could become -1
         if( h1[1] > -1 ){ h1[1]= mask[h[1]]; }// note this could become -1

         memcpy( buf+len,  h1, 2*sizeof(INT_)  ); len+= 2*sizeof(INT_);
         memcpy( buf+len,   q, 2*sizeof(INT_)  ); len+= 2*sizeof(INT_);

         assert( len < 512 );
         assert( len == sizeof(qvad_t) );
       ::fwrite( buf,1,len, f );

         return;
     }; 

/** Read from file **/

      void fread( FILE *f )
     {
         char buf[512];
         size_t len=0;

         len = sizeof(qvad_t);
       ::fread( buf,1,len, f );

         len= 0;
         memcpy(    h,buf+len, 2*sizeof(INT_)  ); len+= 2*sizeof(INT_);
         memcpy(    q,buf+len, 2*sizeof(INT_)  ); len+= 2*sizeof(INT_);

         return;
     }; 

  };

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Mon  6 Aug 23:58:37 BST 2018
// Changes History -

/** Data structure to reference faces on hexahedra **/

   struct qref_t
  {
      INT_       q;  /** face **/
      INT_       g;  /** boundary group **/
      INT_       b;  /** position in the boundary group **/
      unsigned  r:2; /** permutation of the corresponding face on the hexahedron **/
      unsigned  o:1; /** orientation on the exahedron **/

/** Initialize to avoid uninitialised bytes reports from valgrind **/

      void init(){ memset( this,0,sizeof(qref_t)); };
  };



#  endif
