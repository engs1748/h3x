#  ifndef _PT_
#  define _PT_

#  include <cstdio>
#  include <cstdlib>
#  include <cmath>
#  include <cassert>
#  include <cstring>

#  include <typd.h>
#  include <dir.h>
#  include <vsize.h>


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Mon  6 Aug 23:58:37 BST 2018
// Changes History -
// Next Change(s)  -

/** A data structure to represent vertices in a hexahedral unstructured grid **/

   struct pt_t
  {
      INT_             id; /** Hexahedron attached to the vertex **/
      INT_              h; /** Hexahedron attached to the vertex **/
      INT_              k; /** Local position of the vertex on the hexahedron h **/

      pt_t()
     {
         id= -1;
         h=  -1;
         k=  -1;
     };

     ~pt_t(){};
  
/** Write to file renaming hexahedra according to mask **/

      void fwrite( FILE *f, INT_ *mask )
     {
         char buf[16];
         INT_     h1;
         size_t len=0;

         h1= mask[h];  // note this could become -1

         memcpy( buf+len, &id,   sizeof(INT_)  ); len+= sizeof(INT_);
         memcpy( buf+len, &h1,   sizeof(INT_)  ); len+= sizeof(INT_);
         memcpy( buf+len,  &k,   sizeof(INT_)  ); len+= sizeof(INT_);

         assert( len < 16 );
         assert( len == sizeof(pt_t) );
       ::fwrite( buf,1,len, f );

         return;
     }; 

/** Read from file **/

      void fread( FILE *f )
     {
         char buf[16];
         size_t len=0;

         len = sizeof(pt_t);
       ::fread( buf,1,len, f );

         len= 0;
         memcpy(  &id,buf+len,   sizeof(INT_)  ); len+= sizeof(INT_);
         memcpy(   &h,buf+len,   sizeof(INT_)  ); len+= sizeof(INT_);
         memcpy(   &k,buf+len,   sizeof(INT_)  ); len+= sizeof(INT_);

         return;
     }; 
  };


#  endif
