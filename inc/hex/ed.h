#  ifndef _ED_
#  define _ED_

#  include <cstdio>
#  include <cstdlib>
#  include <cmath>
#  include <cassert>
#  include <cstring>

#  include <typd.h>
#  include <dir.h>
#  include <vsize.h>


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Mon  6 Aug 23:58:37 BST 2018
// Changes History -
// Next Change(s)  -

/** A data structure to represent edges in a unstructured hexahedral mesh **/

   struct edg_t
  {
      INT_            h; /** Hexahedron attached to the edge **/
      INT_            k; /** Local position of the edge in the hexahedron h **/

      edg_t()
     {
         h=-1;
         k=-1;
     };

/** Find the two vertices forming the edge **/

      void pts( struct hex_t *hx, INT_2 buf );


/** Write to file renaming hexahedra accoridnt to mask **/

      void fwrite( FILE *f, INT_ *mask )
     {
         char buf[512];
         INT_     h1;
         size_t len=0;

         h1= mask[h];  // note this could become -1
         memcpy( buf+len, &h1,   sizeof(INT_)  ); len+= sizeof(INT_);
         memcpy( buf+len,  &k,   sizeof(INT_)  ); len+= sizeof(INT_);

         assert( len < 512 );
         assert( len == sizeof(edg_t) );
       ::fwrite( buf,1,len, f );
         

         return;
     }; 

/** Read from file **/

      void fread( FILE *f )
     {
         char buf[512];
         size_t len=0;

         len = sizeof(edg_t);
       ::fread( buf,1,len, f );

         len= 0;
         memcpy(   &h,buf+len,   sizeof(INT_)  ); len+= sizeof(INT_);
         memcpy(   &k,buf+len,   sizeof(INT_)  ); len+= sizeof(INT_);

         return;
     }; 
  };

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Mon  6 Aug 23:58:37 BST 2018
// Changes History -

/** Data structure to reference edges on hexahedra **/

   struct eref_t
  {
      INT_       e;  /** edge **/
      unsigned  o:1; /** orientation  **/
  };



#  endif
