#  ifndef _HEX_
#  define _HEX_

#  include <cstdio>
#  include <cstdlib>
#  include <cmath>
#  include <cassert>
#  include <cstring>

#  include <typd.h>
#  include <dir.h>
#  include <vsize.h>
#  include <hex/pt.h>
#  include <hex/qv.h>
#  include <hex/ed.h>

#  define NO_DCNTRL 999999.


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Mon  6 Aug 23:58:37 BST 2018
// Changes History -
// Next Change(s)  -

/** Local topolgoy of hexahedra, quadrilateral faces and edges **/

/** face -> vertex on an hexahedron **/

   const INT_4 mhq[6]= { {6,4,0,2},
                         {5,7,3,1},
                         {4,5,1,0},
                         {7,6,2,3},
                         {0,1,3,2}, 
                         {6,7,5,4} };

   const INT_4 mhqo[6]={ {7,5,1,3},
                         {4,6,2,0},
                         {6,7,3,2},
                         {5,4,0,1},
                         {4,5,7,6}, 
                         {2,3,1,0} };
/** permute the vertices on a local face **/

   const INT_4  mqp[2][4]={ { {0,1,2,3},
                              {1,2,3,0}, 
                              {2,3,0,1},
                              {3,0,1,2} },
                            { {0,3,2,1},
                              {1,0,3,2},
                              {2,1,0,3},
                              {3,2,1,0} } };

/** inverse permutation for the vertices on a local face **/

   const INT_4 mqp1[2][4]={ { {0,1,2,3},
                              {3,0,1,2},
                              {2,3,0,1},
                              {1,2,3,0} },
                            { {0,3,2,1},
                              {1,0,3,2},
                              {2,1,0,3},
                              {3,2,1,0} } };


/** edge -> vertex on an hexahedron **/

   const INT_2 mhe[12]= { {0,1},
                          {1,3},
                          {3,2},
                          {2,0},
                          {4,5},
                          {5,7},
                          {7,6},
                          {6,4},
                          {0,4},
                          {1,5},
                          {3,7},
                          {2,6} };

/** face -> edge on an hexahedron **/

   const INT_4 mqe[6]= {  { 7, 8, 3,11},
                          { 5,10, 1, 9},
                          { 4, 9, 0, 8},
                          { 6,11, 2,10},
                          { 0, 1, 2, 3},
                          { 6, 5, 4, 7} };

/** edge -> face on an hexahedron **/

   const INT_2 meq[12]= { {4,2},
                          {4,1},
                          {4,3},
                          {4,0},
                          {2,5},
                          {1,5},
                          {3,5},
                          {0,5},
                          {2,0},
                          {1,2},
                          {3,1},
                          {0,3} };

/** edge -> face local edge on an hexahedron **/

   const INT_2 neq[12]= { {0,2},
                          {1,2},
                          {2,2},
                          {3,2},
                          {0,2},
                          {0,1},
                          {0,0},
                          {0,3},
                          {3,1},
                          {3,1},
                          {3,1},
                          {3,1} };

/** opposite face on an hexahedron **/

   const INT_  opq[6]= {1,0,3,2,5,4};

/** oppsoite faces on an hexahedron: first the two faces oppsite each other, then the perimetral faces **/

   const INT_  oqq[2][3][6]= { { {0,1,2,4,5,3},{2,3,0,4,5,1},{4,5,0,2,3,1} },
                               { {1,0,2,4,5,3},{3,2,0,4,5,1},{5,4,0,2,3,1} } };

/** opposite vertex according to direction opp[o][i][j] is opposite opp[!o][i][j] along direction i **/

   const INT_  opp[2][3][4]= { { {0,2,4,6}, {0,1,4,5}, {0,1,2,3} },
                               { {1,3,5,7}, {2,3,6,7}, {4,5,6,7} } };

/** permute the vertices for rotation hrot[o][i][:] along direction i with orientation o **/

   const INT_ hrot[2][3][8]= { { {4,5,0,1,6,7,2,3},
                                 {4,0,6,2,5,1,7,3}, 
                                 {2,0,3,1,6,4,7,5} },
                               { {2,3,6,7,0,1,4,5},
                                 {1,5,3,7,0,4,2,6}, 
                                 {1,3,0,2,5,7,4,6} } };

/** permute the face for rotation qrot[o][i][:] along direction i with orientation o **/

   const INT_ qrot[2][3][8]= { { {0,1,5,4,2,3},
                                 {5,4,2,3,0,1}, 
                                 {3,2,0,1,4,5} },
                               { {0,1,4,5,3,2},
                                 {4,5,2,3,1,0}, 
                                 {2,3,1,0,4,5} } };


/** correspondence between vertices and directions **/

   const dir_t loc[8]= { dir_t(0,0,0),
                         dir_t(1,0,0),
                         dir_t(0,1,0),
                         dir_t(1,1,0),
                         dir_t(0,0,1),
                         dir_t(1,0,1),
                         dir_t(0,1,1),
                         dir_t(1,1,1) };


/** minimum element of a quartet **/

   inline INT_ min4( INT_4 data )
  {

      INT_       i0=0,i1=2;
      INT_       val;
      if( data[ 1] < data[ 0] ){ i0=1; };
      if( data[ 3] < data[ 2] ){ i1=3; };

      val= i0;
      if( data[i1] < data[i0] ){ val=i1; };
 
      return val;
      
  };

/** test efficiently if two quartets are the same **/

   inline bool same( INT_4 a, INT_4 b )
  {
      assert( a[0] == b[0] );
      if( a[1] != b[1] ){  return false; }
      if( a[2] != b[2] ){  return false; }
      if( a[3] != b[3] ){  return false; }
      return true;
  }


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Mon  6 Aug 23:58:37 BST 2018
// Changes History -

/** Data structure to represent hexahedra in a unstructured grid **/

// face in direction a

      inline INT_  face( dir_t a )
     {
         INT_ val;
         val= a.dir();
         val*=2;
         val+=a.sign();
         return val;
     };

   struct hex_t
  {

      INT_           id;        /** identity **/
      INT_4          iz;        /** zone information **/

      INT_            w[8];     /** vertex ID (for debugging purposes only) **/
      INT_            v[8];     /** vertices **/
      INT_            x[8];     /** vertex coordinates **/
      qref_t          q[6];     /** faces **/
      eref_t          e[12];    /** edges **/

      REAL_3          d[8];     /** Control functions **/

      hex_t()
     {

         id=-1;
         iz[0]=-1;
         iz[1]=-1;
         iz[2]=-1;
         iz[3]=-1;

         memset( (void*)v, -1, 8*sizeof(INT_) );
         memset( (void*)x, -1, 8*sizeof(INT_) );
         memset( (void*)w, -1, 8*sizeof(INT_) );
         memset( (void*)q,  0, 6*sizeof(qref_t) );
         memset( (void*)e,  0,12*sizeof(eref_t) );

         for( INT_ i=0;i<6;i++ )
        {
            q[i].q= -1;
            q[i].g= -1;
            q[i].b= -1;
            q[i].o=  0;
            q[i].r=  0;
        }
         for( INT_ i=0;i<12;i++ )
        {
            e[i].e= -1;
            e[i].o=  0;
        }
         for( INT_ i=0;i<8;i++ )
        {
            d[i][0]= NO_DCNTRL;
            d[i][1]= NO_DCNTRL;
            d[i][2]= NO_DCNTRL;
        }
         return;
     };

      void reset()
     {
         id=-1;
         iz[0]=-1;
         iz[1]=-1;
         iz[2]=-1;
         iz[3]=-1;
         memset( (void*)w, -1, 8*sizeof(INT_) );
         memset( (void*)v, -1, 8*sizeof(INT_) );
         memset( (void*)x, -1, 8*sizeof(INT_) );
         memset( (void*)q,  0, 6*sizeof(qref_t) );
         memset( (void*)e,  0,12*sizeof(eref_t) );
         for( INT_ i=0;i<6;i++ )
        {
            q[i].q= -1;
            q[i].g= -1;
            q[i].b= -1;
            q[i].o=  0;
            q[i].r=  0;
        }
         for( INT_ i=0;i<12;i++ )
        {
            e[i].e= -1;
            e[i].o=  0;
        }
         for( INT_ i=0;i<8;i++ )
        {
            d[i][0]= NO_DCNTRL;
            d[i][1]= NO_DCNTRL;
            d[i][2]= NO_DCNTRL;
        }
         return;
     };

      void fwrite( FILE *f, INT_ *wp, INT_ *we, INT_ *wq )
     {
         char     buf[512];
         assert( sizeof(hex_t)<512);
         INT_      v1[8];
         INT_      x1[8];
         qref_t    q1[6];
         eref_t    e1[12];

         size_t    len=0;
         INT_      i;

         for( i=0;i< 8;i++ ){ v1[i]= wp[ v[i] ]; assert( v1[i] > -1 ); }
         for( i=0;i< 8;i++ ){ x1[i]= wp[ v[i] ]; assert( v1[i] > -1 ); }
         for( i=0;i< 6;i++ ){ q1[i]=  q[i]; q1[i].q= wq[q1[i].q]; }// assert( q1[i].q > -1 ); }
         for( i=0;i<12;i++ ){ e1[i]=  e[i]; e1[i].e= we[e1[i].e]; assert( e1[i].e > -1 ); }

         memcpy( buf+len, &id,      sizeof(INT_) ); len+=    sizeof(INT_);
         memcpy( buf+len,  iz,      sizeof(INT_4)); len+=    sizeof(INT_4);
         memcpy( buf+len,   w,  8*  sizeof(INT_) ); len+=  8*sizeof(INT_);
         memcpy( buf+len,  v1,  8*  sizeof(INT_) ); len+=  8*sizeof(INT_);
         memcpy( buf+len,  x1,  8*  sizeof(INT_) ); len+=  8*sizeof(INT_);
         memcpy( buf+len,  q1,  6*sizeof(qref_t) ); len+=  6*sizeof(qref_t);
         memcpy( buf+len,  e1, 12*sizeof(eref_t) ); len+= 12*sizeof(eref_t);
         memcpy( buf+len,   d,  8*sizeof(REAL_3) ); len+=  8*sizeof(REAL_3);

         assert( len < 512 );
/*     ::printf( "%d \n",sizeof(hex_t));
       ::printf( "%d \n",len );*/
         assert( len <= sizeof(hex_t) );
         

       ::fwrite(&len,  1,sizeof(size_t), f );
       ::fwrite( buf,  1,len,            f );

         return;
     };

      void fread( FILE *f )
     {
         char     buf[512];
         assert( sizeof(hex_t)<512);

         size_t    len;

       ::fread(&len,  1,sizeof(size_t), f );
       ::fread( buf,  1,len,            f );

         len=0; 
         memcpy( &id,buf+len,       sizeof(INT_) ); len+=    sizeof(INT_);
         memcpy(  iz,buf+len,       sizeof(INT_4)); len+=    sizeof(INT_4);
         memcpy(   w,buf+len,   8*  sizeof(INT_) ); len+=  8*sizeof(INT_);
         memcpy(   v,buf+len,   8*  sizeof(INT_) ); len+=  8*sizeof(INT_);
         memcpy(   x,buf+len,   8*  sizeof(INT_) ); len+=  8*sizeof(INT_);
         memcpy(   q,buf+len,   6*sizeof(qref_t) ); len+=  6*sizeof(qref_t);
         memcpy(   e,buf+len,  12*sizeof(eref_t) ); len+= 12*sizeof(eref_t);
         memcpy(   d,buf+len,   8*sizeof(REAL_3) ); len+=  8*sizeof(REAL_3);

         assert( len < 512 );

         return;
     };

// generate faces

      inline void quad( INT_ j, INT_4 buf )
     {
         INT_4 w; 
         INT_ i,h,k;
         INT_ a,b;
         for( i=0;i<4;i++ ){ w[i]= v[ mhq[j][i] ]; };

         h= min4( w );

         a= mqp[0][h][1];
         b= mqp[1][h][1];

         k= 0; if( w[b] < w[a] ){ k=1; };

         for( i=0;i<4;i++ ){ buf[i]= w[ mqp[k][h][i] ]; };
         for( i=0;i<4;i++ ){ assert( buf[ mqp1[k][h][i] ] == w[ i ] ); };
         q[j].o= k;
         q[j].r= h;
     };

// generate edges

      inline void edge( INT_ j, INT_2 buf )
     {
         bool k;
         INT_ a,b;

         a= v[ mhe[j][0] ];
         b= v[ mhe[j][1] ];

         k= a>b;
         if( k ){ swap( a,b ); }
         
         buf[0]= a;
         buf[1]= b;

         e[j].o= k;
     }

// rotate nodes

      inline void rot( INT_ o, INT_ d )
     {
         INT_ wrk[8];
         INT_ i;

         for( i=0;i<8;i++ ){ wrk[i]= v[ hrot[o][d][i] ]; }
         for( i=0;i<8;i++ ){ v[i]= wrk[i]; }
     };

// local frame for face i
      inline void pdir( INT_ i, frme_t f )
     {
         assert( i >-1 );
         assert( i < 8 );


         INT_  a;
         a= i;    f[0]= dir_t( !(a%2),0 );
         a= a>>1; f[1]= dir_t( !(a%2),1 );
         a= a>>1; f[2]= dir_t( !(a%2),2 );

     };

      inline void qdir( INT_ i, frme_t a )
     {

         INT_4 buf[2];
         INT_  l,d,m,n;

         pack( i, buf[0],buf[1] );

         l= q[i].r;
         d= q[i].o;

         m= mhq[i][ mqp[d][l][0] ];
         n= mhq[i][ mqp[d][l][1] ];

         assert( v[m] == buf[0][0] );
         assert( v[n] == buf[0][1] );

         a[0]= loc[n]- loc[m];
         n= mhq[i][ mqp[d][l][3] ];

         assert( v[n] == buf[0][3] );

         a[1]= loc[n]- loc[m];

         
         a[2]= dir_t( d,i/2 );

     };

      inline void edir( INT_ i, frme_t f )
     {

         INT_  a,b,c,d;

         a= mhe[i][0];
         b= mhe[i][1];

         c= meq[i][0];
         d= meq[i][1];

         if( v[a] > v[b] ){ swap( a,b ); swap( c,d ); }
         f[0]= loc[b]- loc[a];
         f[1]= dir_t( !(c%2),c/2 );
         f[2]= dir_t( !(d%2),d/2 );

     };


      inline bool neig( qvad_t *c, INT_ i, INT_ &j, INT_ &k )
     {
         INT_ l;
         l= q[i].q;
         k= q[i].o;

         k= !k;

         j= c[l].h[k];
         k= c[l].q[k];

         return (j>-1); 
     }

      void printf()
     {
         ::printf( "[ %2d %2d %2d %2d %2d %2d %2d %2d ]\n", 
                    v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7] );
     };


/** Pack vertices and vertex coordinates belonging to face i using the face ordering**/
      inline void pack( INT_ i, INT_4 buf, INT_4 xbuf )
# include<hex/public/pack>

/** Pack vertices and vertex coordinates belonging to face i using the hex ordering**/
      inline void npack( INT_ i, INT_4 buf, INT_4 xbuf )
# include<hex/public/npack>

/** Pack vertices and vertex coordinates belonging to the opposite of face i **/

      inline void opack( INT_ i, INT_4 buf, INT_4 xbuf )
# include<hex/public/opack>

/** Unpack vertices and vertex coordinates belonging to face i **/

      inline void unpack( INT_ i, INT_4 buf, INT_4 xbuf )
# include<hex/public/unpack>

      inline void sctrl( INT_ g, REAL_ e, INT_ &f )
     {

         if( g == 0 ){ d[0][0]= 1./max( e,1./d[0][0] ); 
                       d[2][0]= 1./max( e,1./d[2][0] ); 
                       d[4][0]= 1./max( e,1./d[4][0] ); 
                       d[6][0]= 1./max( e,1./d[6][0] );  f= 1; }

         if( g == 1 ){ d[1][0]= 1./max( e,1./d[1][0] ); 
                       d[3][0]= 1./max( e,1./d[3][0] ); 
                       d[5][0]= 1./max( e,1./d[5][0] ); 
                       d[7][0]= 1./max( e,1./d[7][0] );  f= 1; }

         if( g == 2 ){ d[0][1]= 1./max( e,1./d[0][1] ); 
                       d[1][1]= 1./max( e,1./d[1][1] ); 
                       d[4][1]= 1./max( e,1./d[4][1] ); 
                       d[5][1]= 1./max( e,1./d[5][1] );  f= 1; }

         if( g == 3 ){ d[2][1]= 1./max( e,1./d[2][1] ); 
                       d[3][1]= 1./max( e,1./d[3][1] ); 
                       d[6][1]= 1./max( e,1./d[6][1] ); 
                       d[7][1]= 1./max( e,1./d[7][1] );  f= 1; }

         if( g == 4 ){ d[0][2]= 1./max( e,1./d[0][2] ); 
                       d[1][2]= 1./max( e,1./d[1][2] ); 
                       d[2][2]= 1./max( e,1./d[2][2] ); 
                       d[3][2]= 1./max( e,1./d[3][2] );  f= 1; }

         if( g == 5 ){ d[4][2]= 1./max( e,1./d[4][2] ); 
                       d[5][2]= 1./max( e,1./d[5][2] ); 
                       d[6][2]= 1./max( e,1./d[6][2] ); 
                       d[7][2]= 1./max( e,1./d[7][2] );  f= 1; }
     }
  };

#  endif
