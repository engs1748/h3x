#  ifndef _IDEALGAS_
#  define _IDEALGAS_

#  include <cstdio>

#  include <gas/uni.h>
#  include <limiters.h>
#  include <m33.h>

#  define ID_NVAR_ 5
#  define RSIZE_  24

   struct idgas_t
  {
      REAL_        gam;
      REAL_       gam1;
      REAL_         wg;
      REAL_         rg;
      REAL_         nu;
      REAL_         pr;
      REAL_        eps;

      inline INT_ nvar(){ return ID_NVAR_; };
      void varname( INT_ i, char *c );

      void read( char **data );
      void init();

      inline void delta1( REAL_ *q, REAL_ *g, REAL_ *f )
# include <gas/ideal/delta1>

      inline void updte1( REAL_ *q, REAL_ d, REAL_ *g, REAL_ *f )
# include <gas/ideal/updte1>


// Transformation to and from characteristic variables

      inline void ravg1( REAL_ *ql, REAL_ *qr, REAL_ *n, REAL_ wn, REAL_ *wrk )
# include <gas/ideal/ravg1>

      inline void waves1( REAL_  *wrk, REAL_  *n, REAL_ *dq, REAL_ *dw )
# include <gas/ideal/waves1>

      inline void prims1( REAL_ *wrk, REAL_  *n, REAL_ *du, REAL_ *dq )
# include <gas/ideal/prims1>

// Tangential terms for characteristic variable extrapolation
      inline void agrdq( REAL_ *n, REAL_ rho, REAL_ ux, REAL_ uy, REAL_ uz, REAL_ t, REAL_ p, 
                         REAL_ *d, REAL_ *d0, REAL_ *d1, REAL_ *d2, REAL_ *f )
# include <gas/ideal/agrdq>

// Second order inviscid flux

   inline void riem2( REAL_ *wrk, REAL_ *n, REAL_ wn, REAL_ *ql, REAL_ *qr, REAL_ *dql, REAL_ *dqr, REAL_ *f, REAL_ &lmax )
# include <gas/ideal/riem2>

   inline void riem3( REAL_ *wrk, REAL_ *n, REAL_ wn, REAL_ *ql, REAL_ *qr, REAL_ *dql, REAL_ *dqr, REAL_ *f, REAL_ &lmax )
# include <gas/ideal/riem3>

   inline void riemw( REAL_ *n, REAL_ wn, REAL_ *ql, REAL_ *dqs, REAL_ *dqt, REAL_ *f, REAL_ &lmax )
# include <gas/ideal/riemw>


   inline void flux2( REAL_ *x, REAL_ *n, REAL_ wn, REAL_ *x0, REAL_ *x1, REAL_ *v0, REAL_ *v1, REAL_ *dx0, REAL_ *dx1, REAL_ *dv0, REAL_ *dv1, REAL_ *f, REAL_ *z, REAL_ *l )
# include <gas/ideal/flux2>

// Second order inviscid boundary flux

   inline void fluxb( REAL_ *x, REAL_ *n, REAL_ wn, REAL_ *x0, REAL_ *v0, REAL_ *v1, REAL_ *v2, REAL_ *dx0, REAL_ *dv0, REAL_ *f, REAL_ *bf, REAL_ *l )
# include <gas/ideal/fluxb>                                                                     
                                                                                                
// Second order inviscid wall flux                                                              
   inline void fluxw( REAL_ *x, REAL_ *n, REAL_ wn, REAL_ *x0, REAL_ *v0, REAL_ *v1, REAL_ *v2, REAL_ *dx0, REAL_ *dv0, REAL_ *f, REAL_ *z, REAL_ *bf, REAL_ *l )
# include <gas/ideal/fluxw>                                                                                                       
                                                                                                                                  
// Second order adiabatic viscous wall flux                                                                                       
   inline void fluxa( REAL_ *x, REAL_ *n, REAL_ wn, REAL_ *x0, REAL_ *v0, REAL_ *v1, REAL_ *v2, REAL_ *dx0, REAL_ *dv0, REAL_ *f, REAL_ *z, REAL_ *bf, REAL_ *l )
# include <gas/ideal/fluxa>                                                                                                       
                                                                                                                                  
// Second order isothermal viscous wall flux                                                                                      
   inline void fluxt( REAL_ *x, REAL_ *n, REAL_ wn, REAL_ *x0, REAL_ *v0, REAL_ *v1, REAL_ *v2, REAL_ *dx0, REAL_ *dv0, REAL_ *f, REAL_ *z, REAL_ *bf, REAL_ *l )
# include <gas/ideal/fluxt>

// Bias - to be removed

      inline void wbias1( REAL_ *x, REAL_ *w, REAL_ *x0, REAL_ *x1, REAL_ *dv, REAL_ *dx0, REAL_ *dx1, REAL_ *dv0, REAL_ *dv1, REAL_ *y, REAL_ *z )
# include <gas/ideal/wbias1>

      
// all this to be removed
      inline void iflux1( REAL_  *ql, REAL_  *qr, REAL_  *n, REAL_  wn, REAL_  *f, REAL_  *l )
# include <gas/ideal/iflux1>

      inline void wflux1( REAL_   *q, REAL_  *n, REAL_  wn, REAL_  *f, REAL_  *l )
# include <gas/ideal/wflux1>

      inline void ifluxv( REAL_V *ql, REAL_V *qr, REAL_V *n, REAL_V wn, REAL_V *f, REAL_V &lmax )
# include <gas/ideal/ifluxv>

      inline void auxv1( REAL_ *q, REAL_ *aux )
# include <gas/ideal/auxv1>

      inline void areac1( REAL_ *l, REAL_ *q, REAL_ phi, REAL_ *q1 )
# include <gas/ideal/areac1>

      inline void darcyf( REAL_ *wi, REAL_ *ql, REAL_ *qr, REAL_ phil, REAL_ phir, REAL_ *q0, REAL_ *q1, REAL_ &aa, REAL_ *g0, REAL_ *g1 )
# include <gas/ideal/areac2>

      inline void darcys( REAL_ *q, REAL_ vol, REAL_ k1, REAL_ k2, REAL_ *f, REAL_ &lmax )
# include <gas/ideal/darcs1>

      inline void vflux1( REAL_ *xl, REAL_ *dxdxl, REAL_ *ql, REAL_ *dqdxl, 
                          REAL_ *xr, REAL_ *dxdxr, REAL_ *qr, REAL_ *dqdxr, REAL_ *w, REAL_ *f, REAL_ &lmax )
# include <gas/ideal/vflux1>

      inline void fflux1( REAL_ *x, REAL_ *q, REAL_ *w, REAL_  *f, REAL_  &lmax )
# include <gas/ideal/fflux1>



  };
#  endif
