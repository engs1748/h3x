# ifndef _GAS_
# define _GAS_


# include <gas/ideal/ideal.h>
# include <gas/isoth/isoth.h>

# define   NVAR_         ID_NVAR_
# define   NAUX_         4

# define   GAS_    idgas_t

# define   READ    read   

# define   WAVES   waves1
# define   PRIMS   prims1
# define   WBIAS   wbias1

# define   IFLUX   iflux1
# define   WFLUX   wflux1
# define   FFLUX   fflux1

# define   DELTA   delta1
# define   UPDTE   updte1

# define   VFLUX   vflux1

# define   DARCF   darcyf
# define   DARCS   darcys

# define   RAVG    ravg1
# define   AUXV    auxv1

# endif
