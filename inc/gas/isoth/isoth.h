#  ifndef _ISOTHGAS_
#  define _ISOTHGAS_

#  include <gas/uni.h>

   struct isoth_t
  {
      REAL_        gam;
      REAL_       gam1;
      REAL_       tref;
      REAL_         wg;
      REAL_         rg;
      REAL_        eps;

      inline INT_ nvar(){ return 5; };
      void varname( INT_ i, char *c );

      void read( char ** );
      void init();

      inline void delta1( REAL_ *q, REAL_ *g, REAL_ *f )
# include <gas/isoth/delta1>

      inline void update( REAL_ *q, REAL_ d, REAL_ *g, REAL_ *f )
# include <gas/isoth/updte1>
      
      inline void iflux1( REAL_  *ql, REAL_  *qr, REAL_  *n, REAL_  wn, REAL_  *f, REAL_  &lmax )
# include <gas/isoth/iflux1>

      inline void wflux1( REAL_   *q, REAL_  *n, REAL_  wn, REAL_  *f, REAL_  &lmax )
# include <gas/isoth/wflux1>

      inline void ifluxv( REAL_V *ql, REAL_V *qr, REAL_V *n, REAL_V wn, REAL_V *f, REAL_V &lmax )
# include <gas/isoth/ifluxv>

      inline void ravg1( REAL_ *ql, REAL_ *auxl, REAL_ *qr, REAL_ *auxr, REAL_ *qa, REAL_ *auxa )
# include <gas/isoth/ravg1>

      inline void auxv1( REAL_ *q, REAL_ *aux )
# include <gas/isoth/auxv1>

  };
#  endif
