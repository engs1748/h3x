# ifndef _LINALG_
# define _LINALG_

      //          N NRHS   A     LDA  IPIV   B  LDB,  info )
   extern "C" void dgesv_(         int *, int *, double *, int *, int *, double *,                  int *, int * );

           //  jobVL, jobVR,   N,   A,  LDA, WR, WI, VL, LDVL, VR,  LDVR, WORK,  LWORK,  INFO
   extern "C" void dgeev_( char *, char *, int *, double *, int *, double *, double *, double *, int *, double *, int *, double *, int *, int * );

# endif

