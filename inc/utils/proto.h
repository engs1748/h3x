#  ifndef _UTILS_
#  define _UTILS_

#  include <cstdlib>
#  include <cassert>

#  include <typd.h>

/**@{ @ingroup utils */


/** Concatenation of integer pairs**/

   struct concat_t
  {
      INT_                 n;
      INT_             *ilst[2];
      INT_                 m;
      INT_             *lprm;
  };

/** Debug
   @brief debug
  */
   void stophere();
  
/** set entries ist to ien in a data buffer to a specified value .
   @param ist                                   starting location.
   @param ien                                   ending location.
   @param val                                   value.
   @param data                                   buffer.
  */
   template < typename type > void setv( INT_ ist, INT_ ien, type val, type *data )
  {
      INT_ i;
      for( i=ist;i<ien;i++ )
     {
         data[i]= val;
     }
  };

/** set entries ist to ien in a data buffer to a specified value .
   @param ist                                   starting location.
   @param ien                                   ending location.
   @param n                                     number of columns
   @param val                                   value.
   @param data                                   buffer.
  */
   template < typename type > void setv( INT_ ist, INT_ ien, INT_ n, type val, type *data[] )
  {
      INT_ i;
      for( i=0;i<n;i++ )
     {
         setv( ist,ien, val,data[i] );
     }
  };

/** set entries ist to ien in a data buffer to a specified value .
   @param ist                                   starting location.
   @param ien                                   ending location.
   @param val                                   value.
   @param data                                   buffer.
  */
   template < typename type > void setv( INT_ n, type *val, type *data )
  {
      INT_ i;
      for( i=0;i<n;i++ )
     {
         data[i]= val[i];
     }
  };

/** set entries specified in the reference array iref from ist to ien in a data buffer to a specified value.
   @param ist                                   starting location.
   @param ien                                   ending location.
   @param n                                     second dimension of data.
   @param iref                                  refernce array.
   @param val                                   value.
   @param data                                  buffer.
  */
   template < typename type > void setv( INT_ ist, INT_ ien, INT_ n, INT_ *iref, type val, type *data[] )
#  include                                      <utils/public/setvn>
;

/** Reallocate a pointer to pointers to accomodate increased size.
   @param  len                                  The current size of the pointer.
   @param  dlen                                 The size increase.
   @param  data                                 The pointer.
   @brief  Reallocate.
  */
   template < class object > void realloc( INT_ *len, INT_ dlen, object ***data )
#  include                                      <utils/public/realloc>
;

/** Reallocate a pointer to elementary types to accomodate increased size.
   @param  len                                  The current size of the pointer.
   @param  dlen                                 The size increase.
   @param  data                                 The pointer.
   @brief  Reallocate.
  */
   template < typename type > void realloc( INT_ *len, INT_ dlen, type  **data )
#  include                                      <utils/public/realloct>
;

/** Reallocate a pointer to elementary types to accomodate increased size and appends the entries in data.
   @param  len                                  The current size of the pointer.
   @param  dlen                                 The size increase.
   @param  data                                 The pointer.
   @brief  Reallocate.
  */
   template < typename type > void append( INT_ *len, type **var, INT_ dlen, type *data )
#  include                                      <utils/public/append>
;

/** Swap two values 
   @param v0                                    First value.
   @param v1                                    Second value.
   @brief Swap.
  */
   template <typename type > void swap( type *v0, type *v1 ){ type dum; dum= *v0; *v0= *v1; *v1= dum; };

/** Pre-compute section pointer to access a linear storage array in row-major mode.
   @param n1                                    Numer of columns.
   @param n1                                    Number of rows.
   @param a                                     Pointer to the storage.
   @param as                                    Pointers to the columns.
   @brief Access pointers.
  */
   template < typename type > void subv( INT_ n1, INT_ n2, type *a, type *as[] )
  {
      INT_ i;
      as[0]= a;
      for( i=1;i<n1;i++ )
     {
         as[i]= as[i-1]+n2;
     }
  }

   template <typename type > void identv( INT_ n, type *ival )
  {
      INT_ i;
      for( i=0;i<n;i++ )
     {
         ival[i]=(type)i;
     }
  }

/** Performs binary search on a sorted array. The array is sorted in ascending order 
    and the function returns the position of the smallest entry larger than var.
   @param                    var              Search key.
   @param                    n                Number of entries.
   @param                    data             Array.
   @return                                    The position of the smallest entry in data larger than var.
 **/
   template < typename type > INT_ bsearch( type var, INT_ n, type *data )
#  include                               <utils/public/bsearch>
;

/** Performs binary search on an array sorted by the permutation iprm. The array is sorted in ascending order 
    and the function returns the position of the smallest entry larger than var.
   @param                    var              Search key.
   @param                    n                Number of entries.
   @param                    data             Array.
   @param                    iprm             Permutation array for data. data[iprm[k]] is the k-th smallest entry in data.
   @return                                    The position of the smallest entry in data larger than var.
 **/
   template < typename type > INT_ bsearch( type var, INT_ n, type *data, INT_ *iprm )
#  include                               <utils/public/bsearchi>
;


/** Reverse the order of the entries in data.
   @param                     n                Size of the array.
   @param                     data             Array to be reversed.
 **/
   template < typename type > void reverse( INT_ n, type *data )
#  include                               <utils/public/invert>
;

   template < typename type > INT_ inlst( type var, INT_ n, type *data )
#  include                               <utils/public/inlst>
;
   void accml( INT_ n, INT_ *ia );

   template < typename type > void shftl( INT_ n, type *data )
#  include                               <utils/public/shftl>
;

   template < typename type > void shftr( INT_ n, type *data )
#  include                               <utils/public/shftr>
;

   template < typename type > void shftc( INT_ n, type *data )
#  include                               <utils/public/shftc1>
;

   template < typename type > void shftc( INT_ n, type *data, INT_ m )
#  include                               <utils/public/shftm>
;

   template < typename type > void shftc( INT_ ist, INT_ ien, type *data )
#  include                               <utils/public/shftc>
;

   template < typename type > void line( INT_ i, INT_ n, type *data[], type *var )
#  include                               <utils/public/linen>
;
   template < typename type > void line2( INT_ i, type *data[], type *var )
#  include                               <utils/public/line2>
;

   template < typename type > void line3( INT_ i, type *data[], type *var )
#  include                               <utils/public/line3>
;


/** Permute the array data according to the permutation table iprm. E.g. data[i] becomes data[iprm[i]];
   @param                                   n                array size;
   @param                                   data             array to be permuted;
   @param                                   iprm             permutation array;
**/
   template < typename type > void permute( INT_ n, type *data, INT_ *iprm )
#  include                               <utils/public/permute>
;


/** Determine a permutation and a set of equivalent classess for a concatenated sequence of integer numbers. 
    Version for periodic sequences.
   @param       n           Length of the sequence.
   @param       ilst        Sequence.
   @param       iprm        Permutation array.
   @param       m           Number of equivalence classes.
   @param       lprm        Ending index of each equivalence class.
 **/
   void   concatp( INT_ n, INT_ *ilst[], INT_ *iprm, INT_ *m, INT_ *lprm );


/** Determine a permutation and a set of equivalent classess for a concatenated sequence of integer numbers. 
    General sequences.
   @param       n           Length of the sequence.
   @param       ilst        Sequence.
   @param       iprm        Permutation array.
   @param       m           Number of equivalence classes.
   @param       lprm        Ending index of each equivalence class.
 **/
   void    concatg( INT_ n, INT_ *ilst[], INT_ *iprm, INT_ *m, INT_ *lprm );

/** Determine if at least one of the equivalence classes for a concatenated sequence is reducbile and if so, reports 
    its position in lprm. If no reducble classes are found return -1. 
   @param       n           Length of the sequence.
   @param       ilst        Sequence.
   @param       iprm        Permutation array.
   @param       m           Number of equivalence classes.
   @param       lprm        Ending index of each equivalence class.
   @return                  The position of one of the reducible classes in lprm.
 **/
   INT_  reducible( INT_ n, INT_ *ilst[], INT_ *iprm, INT_  m, INT_ *lprm );

/** Match two sequences and return a permutation array for the second
   @param       n           Length of the sequences.
   @param       ilst0       First sequence.
   @param       ilst1       Second sequence.
   @param       iprm        Permuation index for the second sequence.
 **/

    void match( INT_ n, INT_ *ilst0[], INT_ *ilst1[], INT_ *iprm );

/**@}*/

#  endif
