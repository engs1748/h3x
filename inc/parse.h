#  ifndef _PARSE_
#  define _PARSE_

#  include <cstdlib>
#  include <cstdio>
#  include <cstring>
#  include <cassert>

#  include <typd.h>
#  include <vsize.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Sat 18 Aug 11:08:35 BST 2018
// Changes History -
// Next Change(s)  -

#  define   N 4096

   typedef vsize_t<char,16> buf_t;

   void parse( FILE *f, INT_ &m, buf_t *buf ); /** parse data files written in FLUENT format(?) **/

#  endif

