#  ifndef _BIL_
#  define _BIL_

/** bilinear surfaces.  Position is 
    x= q[0]*x0+ q[1]*x[1]+ q[2]*x[2]+ q[3]*x[3]
    where
    q[0]= (1-s[0])*(1-s[1])
    q[1]=    s[0] *(1-s[1])
    q[2]= (1-s[0])*   s[1] 
    q[3]=    s[0] *   s[1] 
    Automatically generated patch covers the interval [0,1]x[0,1]
**/

   class bilinear_c: public surf_c
  {
      private:
         vtx_t       x0;
         vtx_t       x1;
         vtx_t       x2;
         vtx_t       x3;
      public:
         bilinear_c()
        { 
            memset( &x0,-1,  sizeof(vtx_t));
            memset( &x1,-1,  sizeof(vtx_t));
            memset( &x2,-1,  sizeof(vtx_t));
            memset( &x3,-1,  sizeof(vtx_t));
        };
         virtual surf_e type(){ return surf_bilinear; };
         void read( char **arg );
         void copy( surf_c **var );

         void autov( char **arg );
         void interp( REAL_ *s, vtx_t  &x );
         void interp( REAL_ *s, vtx_t  &x, vtx_t  *dx );
         void interp( REAL_ *s, vtx_t  &x, vtx_t  *dx, vtx_t *dx2 );
         void test( char *name );


         void fread( FILE *f );
         void fwrite( FILE *f );
  };

#  endif
