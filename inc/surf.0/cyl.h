#  ifndef _CYL_
#  define _CYL_

/** cylindrical surfaces.  Position is 
    x= x0+ l[0]*cos(s[0])+ l[1]*sin(s[0])+ l[2]*s[1]
    note s[0] is in radiants.
    Automatically generated patches are the patches covering ranges
    0: [ pi/4,3pi/4] x [a,b]
    1: [3pi/4,5pi/4] x [a,b]
    2: [5pi/4,7pi/4] x [a,b]
    3: [7pi/4,9pi/4] x [a,b]
    a,b are two parameters specified in the input.
**/

   class cyl_c: public surf_c
  {

      private:

         vtx_t       x0;
         vtx_t       l[3];
         REAL_       r;

      public:

         cyl_c()
        { 
            memset( &x0,-1,  sizeof(vtx_t));
            memset(  l, -1,3*sizeof(vtx_t));
            r=0;
        };
         surf_e type(){ return surf_cylinder; };
         void read( char **arg );
         void copy( surf_c **var );

         void interp( REAL_ *s, vtx_t  &x );
         void interp( REAL_ *s, vtx_t  &x, vtx_t  *dx );
         void interp( REAL_ *s, vtx_t  &x, vtx_t  *dx, vtx_t *dx2 );

         void fread( FILE *f );
         void fwrite( FILE *f );
   
         INT_ vtx( char **arg );
         void autov( char **arg );
         void test( char *name );
  };

#  endif
