#  ifndef _PLA_
#  define _PLA_


/** planes. Position is 
    x= x0+ l[0]*s[0]+ l[1]*s[1]
    Automatically generated patch covers the interval [0,1]x[0,1]
 **/

   class plane_c: public surf_c
  {
      private:
         vtx_t       x0;
         vtx_t       l[2];
      public:
         plane_c()
        { 
            memset( &x0,-1,  sizeof(vtx_t));
            memset(  l, -1,2*sizeof(vtx_t));
        };
         virtual surf_e type(){ return surf_plane; };
         void read( char **arg );
         void copy( surf_c **var );

         void interp( REAL_ *s, vtx_t  &x );
         void interp( REAL_ *s, vtx_t  &x, vtx_t  *dx );
         void interp( REAL_ *s, vtx_t  &x, vtx_t  *dx, vtx_t *dx2 );
         void autov( char **arg );
         void test( char *name );
         void fread( FILE *f );
         void fwrite( FILE *f );
  };

#  endif
