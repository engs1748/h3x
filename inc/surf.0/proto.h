#  ifndef _SURF_
#  define _SURF_

#  include <cstdlib>
#  include <cstdio>
#  include <cstring>
#  include <cassert>
#  include <cmath>

#  include <consts.h>
#  include <vsize.h>
#  include <typd.h>
#  include <m33.h>
#  include <vtx.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Sun 19 Aug 21:59:17 BST 2018
// Changes History -
// Next Change(s)  -

   enum surf_e { surf_bad=-1, surf_cylinder, surf_plane, surf_bilinear, surf_biquadratic,
                 surf_sampled, surf_spline, surf_cspline, surf_num };

/** constructor **/

   void newsurf( INT_ t, class surf_c **val );

   
   typedef vsize_t<surf_c*,32> vsurf_t;

   struct patch_t
  {
      INT_4        v;
      REAL_        x0[3];
      REAL_         z[9];
      REAL_         d[3];

      patch_t()
     { 
         memset( v,0,sizeof(INT_4)); 
         memset(x0,0,3*sizeof(REAL_)); 
         memset( z,0,9*sizeof(REAL_)); 
         memset( d,0,3*sizeof(REAL_));
     };
  };

   

   class surf_c
  {

      public:

         wsize_t<REAL_,2,32>   v;
         vsize_t<patch_t,32>   p;

         char             name[128];

         surf_c(){};
         virtual ~surf_c(){};

         virtual surf_e type(){ return surf_bad; };

         void copy( surf_c **var )
        {
            assert( !(*var) );
            newsurf( type(),var );

            memcpy( (*var)->name,name, 128 );

         ((*var)->v).n= v.n;((*var)->v).resize(0.);
            patch_t dum;
         ((*var)->p).n= p.n;((*var)->p).resize(dum);

            for( INT_ i=0;i<v.n;i++ ){ for( INT_ j=0;j<2;j++ ){ ((*var)->v)[i][j]= v[i][j]; }}
            for( INT_ i=0;i<p.n;i++ ){ ((*var)->p)[i]= p[i]; }

/*       ((*var)->v).copy(v);
         ((*var)->p).copy(p);*/

            return;
        };

   
/** Read surface parameters from an array of strings **/
         virtual void read( char **arg ){};

/** 3D coordinates x of the point at parametric coordinates s **/
         virtual void interp( REAL_ *s, vtx_t &x                         ){ assert( false ); };

/** 3D coordinates x and tangent vectors dx of the point at parametric coordinates s **/
         virtual void interp( REAL_ *s, vtx_t &x, vtx_t  *dx             ){ assert( false ); };

/** 3D coordinates x, tangent vectors dx, and curvature tensor dx2 of the point at parametric coordinates s **/
         virtual void interp( REAL_ *s, vtx_t &x, vtx_t  *dx, vtx_t *dx2 ){ assert( false ); };


/** Test the surface in the patch with corners y0,y1,y2,y3 **/
         void test( REAL_2 y0, REAL_2 y1, REAL_2 y2, REAL_2 y3, char *f );

/** Test the surface in patch i **/
         void test( INT_ i, char *f );

/** Test the whole surface **/
         virtual void test( char **arg ){};


/** Add a named vertex **/
         virtual INT_ vtx( char **arg );

/** Add a named patch **/
         INT_ patch( char **arg );

/** Automatically generate patches **/
         virtual void autov( char **arg ){};

/** Approximate project of 3D vertex v on patch j. y are the surface coordinates of the projection **/
         void aprj( vtx_t &v, REAL_ *y, INT_ j, aprj_t &stat );

/** Exact projection of 3D vertex v. y are the surface coordinates of the projection **/
         void  prj( vtx_t &v, REAL_ *y,         aprj_t &stat );

/** For frame of reference for patch p **/
         void  ref( INT_ p );


/** Write to binary file **/
         virtual void fwrite( FILE *f )
        {
          ::fwrite( name,128,sizeof(char), f );
          ::fwrite(&(v.n)     ,1,  sizeof(INT_),   f );
          ::fwrite( v.data() ,v.n,sizeof(REAL_2),   f );
          ::fwrite(&(p.n)     ,1,  sizeof(INT_),   f );
          ::fwrite( p.data() ,p.n,sizeof(patch_t),   f );
            return;
        }

/** Write to binary file **/
         virtual void fread( FILE *f )
        {
          ::fread( name,128,sizeof(char), f );
          ::fread( &(v.n)     ,1,  sizeof(INT_),   f );
            v.resize(-1);
          ::fread( v.data() ,v.n,sizeof(REAL_2),   f );
          ::fread(&(p.n)     ,1,  sizeof(INT_),   f );
            patch_t pdum;
            p.resize(pdum);
          ::fread( p.data() ,p.n,sizeof(patch_t),   f );
            return;
        }
  };

#   include <surf/cyl.h>
#   include <surf/bil.h>
#   include <surf/pla.h>
#   include <surf/biq.h>
#   include <surf/sam.h>
#   include <surf/spl.h>

   void fwrite( vsurf_t &var, FILE *f );
   void  fread( vsurf_t &var, FILE *f );

#  endif
