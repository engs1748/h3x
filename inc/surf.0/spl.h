#  ifndef _SPL_
#  define _SPL_

#  include <spline/spline2.h>

#  define SPL_QSIZE 512

/** sampled surfaces
    Automatically generated patch covers the interval [0,1]x[0,1]
**/

   class spline_c: public surf_c
  {
      private:
         vsize_t<REAL_,32>       x0,x1;
         vsize_t<vtx_t,32>       y,y2;
         INT_                    n,m;
      public:
         spline_c()
        { 
            n=0; 
            m=0; 
        };
         virtual surf_e type(){ return surf_spline; };
         void read( char **arg );
         void copy( surf_c **var );

         void autov( char **arg );
         void interp( REAL_ *s, vtx_t  &x );
         void interp( REAL_ *s, vtx_t  &x, vtx_t  *dx );
         void interp( REAL_ *s, vtx_t  &x, vtx_t  *dx, vtx_t *dx2 );
         void test( char *name );

         void fread( FILE *f );
         void fwrite( FILE *f );
  };

   class cspline_c: public surf_c
  {
      private:
         REAL_                   t;
         vsize_t<REAL_,32>       x0,x1;
         vsize_t<vtx_t,32>       y,y2;
         INT_                    n,m;
      public:
         cspline_c()
        { 
            n=0; 
            m=0; 
        };
         virtual surf_e type(){ return surf_cspline; };
         void read( char **arg );
         void copy( surf_c **var );

         void autov( char **arg );
         void interp( REAL_ *s, vtx_t  &x );
         void interp( REAL_ *s, vtx_t  &x, vtx_t  *dx );
         void interp( REAL_ *s, vtx_t  &x, vtx_t  *dx, vtx_t *dx2 );
         void test( char *name );

         void fread( FILE *f );
         void fwrite( FILE *f );
  };

#  endif
