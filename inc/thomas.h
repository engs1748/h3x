
#ifndef _THOMAS_
#define _THOMAS_


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Thu Dec  2 10:45:50 GMT 2010
// Changes History
// Next Change(s)  -

#  include <cmath>
#  include <cprec.h>

/** Free-standing tridiagonal LDU routines*/

/** thmsf performs the LDU factorisation of a tridiagonal matrix of order N.
    On entry the arguments Am,A,Ap contain the lower diagonal,diagonal and upper diagonal
    of the matrix. On exit they contain the L,D,U factors respectively
   @param N         size of the matrix
   @param Am        lower diagonal
   @param A         diagonal
   @param Ap        upper diagonal 
   @brief LDU factorisation of a tridiagonal matrix 
  */
      void thmf( Int n, Real *am, Real *a, Real *ap );

/** thmss uses the LDU factorisation computed by thmsf to solve linear systems of the form Ax=b
    with multiple right hand sides.
    On entry the arguments Am,A,Ap contain L,D,U factors respectively, X contains the RHS.
    On exit X containes the solution 
   @param n         size of the matrix
   @param am        lower diagonal factor
   @param a         diagonal factor
   @param ap        upper diagonal factor
   @param ldb       number of RHSs
   @param nb        size of RHS 
   @param b         RHS (solution on exit) 
   @brief solution of tridiagonal linear problem
  */
      void thms( Int n, Real *am, Real *a, Real *ap, Int ldb, Int nb,Real *sb );

/** thmss uses the LDU factorisation computed by thmsf to solve linear systems of the form A'x=b
    with multiple right hand sides.
    On entry the arguments Am,A,Ap contain L,D,U factors respectively, X contains the RHS.
    On exit X containes the solution.
   @param n         size of the matrix
   @param am        lower diagonal factor
   @param a         diagonal factor
   @param ap        upper diagonal factor
   @param ldb       number of RHSs
   @param nb        size of RHS
   @param sb        RHS (solution on exit) 
   @brief  solution of tridiagonal linear problem
 */
      void thmst( Int n, Real *am, Real *a, Real *ap, Int ldb, Int nb,Real *sb );

/** thmsfp performs the LDU factorisation of a tridiagonal matrix of order N.
    On entry the arguments Am,A,Ap contain the lower diagonal,diagonal and upper diagonal
    of the matrix. On exit they contain the L,D,U factors respectively.
    @param n         size of the matrix
    @param am        lower diagonal
    @param a         diagonal
    @param ap        upper diagonal 
    @param ldb       number of RHSs
    @param nb        size of RHS
    @param sb        RHS (solution on exit)
    @param wrk       work array (minimum size N) 
    @brief LDU factorisation of a periodic tridiagonal matrix with Schur's complement technique.
 */
      void thomasp( Int n, Real *am, Real *a, Real *ap, Int ldb, Int nb,Real *sb, Real *wrk, Real *wrk2 );

      void thomas( Int n, Real *am, Real *a, Real *ap, Int ldb, Int nb,Real *b );

#  define _THOMAS

#endif


