#  ifndef _LANG_
#  define _LANG_

#  include <cstring>
#  include <cstdio>
#  include <cstdlib>
#  include <cassert>


#  include <typd.h>
#  include <vsize.h>

   const char delim[]=" ,;\n";

   typedef void fun_t( INT_ l, INT_ n, char **arg, void *data );

   struct line_t
  {
      size_t   n;
      char   *buf;

      line_t()
     {
         n=0;
         buf=NULL;
     }

      ~line_t()
     {
         n=0;
         free(buf); buf=NULL;
     }

      void reset(){ n=0; buf=NULL; };
      void clear(){ free(buf); buf=NULL; n=0; };

      bool empty()
     {
         return( (n==0)&&(buf==NULL) );
     }

      void parse( vsize_t<char*,8> &tok )
     {
         char *wrk=NULL;
         
         wrk=strtok( buf,delim );
         while( wrk )
        {
            INT_ j= (tok.n)++; tok.resize(NULL);
            tok[j]= wrk;
            wrk= strtok(NULL,delim);
        }
   
     }
  };

   struct cmd_t
  {
      char    name[48];
      char    synopsis[1024];
      fun_t  *fun;

      cmd_t ()
     {
         memset( name,0,sizeof(name) );
         fun=NULL;
     };
 
  };

   const cmd_t  cdum;
   const line_t ldum;


   struct lang_t
  {

      vsize_t<cmd_t,8>    cmd;
      vsize_t<line_t,8>  line;

      lang_t(){}

      void command( const char *name, fun_t *f, const char *syn )
     {

         INT_ m= (cmd.n)++; cmd.resize(cdum);
         strcpy( cmd[m].name,     name );
         strcpy( cmd[m].synopsis, syn );
         cmd[m].fun= f;
         
     };

      void read( FILE *f )
     {
         line_t wrk;
         INT_  i=0;
         ssize_t len;
         while( true )
        {
            wrk.reset();
            len= getline( &(wrk.buf), &(wrk.n), f );

            if( len < 0 ){ break; }

            if( len >1 )
           { 
               if( wrk.buf[0]!='#' )
              {
                  i= (line.n)++; line.resize(ldum);
                  line[i]= wrk;
              }
               else
              {
                  wrk.clear();
              }
           } 
        }     
     }
   
      void execute( INT_ k, void *data )
     {
         INT_ j,l;
         vsize_t<char*,8> tok;

         line[k].parse( tok );
         if( tok.n > 1 )
        {

// first operand
            sscanf( tok[0],"%d", &l );

// operator
            bool flag=false;
            for( j=0;j<cmd.n;j++ )
           {
               if( strncmp(tok[1], cmd[j].name, 3 )==0 )
              {
                  flag= true;
                  break;
              }
           }
            assert( flag );
       
          (*cmd[j].fun)(l,tok.n-2,tok.data()+2,data);
        }
     }
  };

#  endif

