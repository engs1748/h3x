#  ifndef _HMESH_
#  define _HMESH_

#  include <cstdio>
#  include <cstdlib>
#  include <cmath>
#  include <cassert>
#  include <cstring>

#  include <typd.h>
#  include <hex/hex.h>
#  include <vtx.h>
#  include <frame/frame.h>
#  include <m33.h>
#  include <surf/proto.h>
#  include <sctrl.h>


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 09:51:27 BST 2018
// Changes History
// Next Change(s)  -

#  define DLEN_H 64
#  define NO_DCNTR 999999.

#  define MXGRP 16384

   typedef vsize_t<INT_,DLEN_H>   VINT_;
   typedef wsize_t<INT_,2,DLEN_H> VINT_2;
   typedef wsize_t<INT_,3,DLEN_H> VINT_3;
   typedef wsize_t<INT_,4,DLEN_H> VINT_4;
   typedef wsize_t<INT_,5,DLEN_H> VINT_5;

   void map( dir_t c0, dir_t c1, dir_t *a, dir_t *b, dir_t d0, dir_t d1 );

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 09:51:27 BST 2018
// Changes History
// Next Change(s)  -

   inline REAL_ vol( REAL_3 x0,REAL_3 x1,REAL_3 x2,REAL_3 x3,REAL_3 x4,REAL_3 x5,REAL_3 x6,REAL_3 x7 )
  {
      REAL_  d[9];
      
      d[0+0]= 0.25*( x1[0]+ x3[0]+ x5[0]+ x7[0]- x0[0]- x2[0]- x4[0]- x6[0] );
      d[0+1]= 0.25*( x1[1]+ x3[1]+ x5[1]+ x7[1]- x0[1]- x2[1]- x4[1]- x6[1] );
      d[0+2]= 0.25*( x1[2]+ x3[2]+ x5[2]+ x7[2]- x0[2]- x2[2]- x4[2]- x6[2] );

      d[3+0]= 0.25*( x2[0]+ x3[0]+ x6[0]+ x7[0]- x0[0]- x1[0]- x4[0]- x5[0] );
      d[3+1]= 0.25*( x2[1]+ x3[1]+ x6[1]+ x7[1]- x0[1]- x1[1]- x4[1]- x5[1] );
      d[3+2]= 0.25*( x2[2]+ x3[2]+ x6[2]+ x7[2]- x0[2]- x1[2]- x4[2]- x5[2] );

      d[6+0]= 0.25*( x4[0]+ x5[0]+ x6[0]+ x7[0]- x0[0]- x1[0]- x2[0]- x3[0] );
      d[6+1]= 0.25*( x4[1]+ x5[1]+ x6[1]+ x7[1]- x0[1]- x1[1]- x2[1]- x3[1] );
      d[6+2]= 0.25*( x4[2]+ x5[2]+ x6[2]+ x7[2]- x0[2]- x1[2]- x2[2]- x3[2] );

      return det(d);
  };

   inline REAL_ vol( vtx_t x0,vtx_t x1,vtx_t x2,vtx_t x3,vtx_t x4,vtx_t x5,vtx_t x6,vtx_t x7 )
  {
      REAL_  d[9];
      
      d[0+0]= 0.25*( x1[0]+ x3[0]+ x5[0]+ x7[0]- x0[0]- x2[0]- x4[0]- x6[0] );
      d[0+1]= 0.25*( x1[1]+ x3[1]+ x5[1]+ x7[1]- x0[1]- x2[1]- x4[1]- x6[1] );
      d[0+2]= 0.25*( x1[2]+ x3[2]+ x5[2]+ x7[2]- x0[2]- x2[2]- x4[2]- x6[2] );

      d[3+0]= 0.25*( x2[0]+ x3[0]+ x6[0]+ x7[0]- x0[0]- x1[0]- x4[0]- x5[0] );
      d[3+1]= 0.25*( x2[1]+ x3[1]+ x6[1]+ x7[1]- x0[1]- x1[1]- x4[1]- x5[1] );
      d[3+2]= 0.25*( x2[2]+ x3[2]+ x6[2]+ x7[2]- x0[2]- x1[2]- x4[2]- x5[2] );

      d[6+0]= 0.25*( x4[0]+ x5[0]+ x6[0]+ x7[0]- x0[0]- x1[0]- x2[0]- x3[0] );
      d[6+1]= 0.25*( x4[1]+ x5[1]+ x6[1]+ x7[1]- x0[1]- x1[1]- x2[1]- x3[1] );
      d[6+2]= 0.25*( x4[2]+ x5[2]+ x6[2]+ x7[2]- x0[2]- x1[2]- x2[2]- x3[2] );

      return det(d);
  };

/** Boundary face **/

   struct bnd_t
  {
      INT_       id;      /** Boundary face ID **/
      INT_       h;       /** Hex **/
      INT_       q;       /** Location on hex  **/
      INT_4      v;       /** Boundary vertices **/
      INT_4      n;       /** Neighboring faces **/
      INT_4      e;       /** Edge on neighbouring faces **/
      frme_t     f;       /** ijk frame **/

/** Hints for projection **/

      INT_       s;       /** Surface **/
      INT_       p;       /** Patch **/
      INT_       b;       /** Branch **/

      bnd_t()
     {
         id=-1;
         h= -1;
         q= -1;
         s= -1;
         p= -1;
         b= -1;
         v[0]= -1;
         v[1]= -1;
         v[2]= -1;
         v[3]= -1;
         n[0]= -1;
         n[1]= -1;
         n[2]= -1;
         n[3]= -1;
         e[0]= -1;
         e[1]= -1;
         e[2]= -1;
         e[3]= -1;
         memset( f,0,sizeof(frme_t));
     };
  };

/** Boundary face **/

   struct ibnd_t
  {
      INT_       id;      /** face ID **/
      INT_2      h;       /** Hex **/
      INT_2      q;       /** Location on hex  **/
      INT_4      v;       /** Boundary vertices **/
      frme_t     f[2];    /** ijk frame **/

/** Hints for projection **/

      INT_       s;       /** Surface **/
      INT_       p;       /** Patch **/
      INT_       b;       /** Branch **/

      ibnd_t()
     {
         id=-1;
         h[0]= -1;
         h[1]= -1;
         q[0]= -1;
         q[1]= -1;
         p= -1;
         b= -1;
         v[0]= -1;
         v[1]= -1;
         v[2]= -1;
         v[3]= -1;
         memset( f[0],0,sizeof(frme_t));
         memset( f[1],0,sizeof(frme_t));
     };
  };

/** Boundary surface **/

   struct bsrf_t
  {
      vsize_t<bvtx_t,DLEN_H>   bx;      /** Boundary vertices **/
      vsize_t<bnd_t,DLEN_H>    bo;      /** Hex faces on boundary **/
      INT_2                   lbl;      /** Boundary labels **/
      REAL_                     b;      /** Attraction to this boundary **/
      bool                      m;      /** Use mapping **/
      bool                      n;      /** Use patches for projection **/

      REAL_                    x0[3];   /** Baricenter coordinates **/
      REAL_                     d[3];   /** Eigenvalues of local frame **/
      REAL_                     z[9];   /** directions  of local frame **/

      bsrf_t()
     {

         b=      999999.;
         m=       false;
         n=       false;

         lbl[0]=     -1;
         lbl[1]=     -1;

         memset( x0,0,3*sizeof(REAL_) );
         memset(  d,0,3*sizeof(REAL_) );
         memset(  z,0,9*sizeof(REAL_) );
         
     };
  };

/** Boundary surface **/
   const pt_t     pdum;
   const dir_t    ddum;
   const vtx_t    xdum;
   const bvtx_t   ydum;
   const hex_t    hdum;
   const qvad_t   qdum;
   const edg_t    edum;
   const bnd_t    bdum;
   const FRAME_   fdum;
   const ibnd_t  jbdum;

   struct ibsrf_t
  {
      INT_4                     z;      /** Zone information **/
      INT_                      s;      /** Surface          **/
      INT_                      p;      /** Patch            **/
      INT_                      r;      /** Branch           **/
      REAL_                     d;
      vsize_t<bvtx_t,DLEN_H>   bx;      /** Vertices         **/
      vsize_t<ibnd_t,DLEN_H>   bo;      /** Faces            **/

      ibsrf_t()
     {

         z[0]= -1;
         z[1]= -1;
         z[2]= -1;
         z[3]= -1;

         p= -1;
         s= -1;
         r= -1;
         
     };

      void fwrite( FILE *f )
     {
       ::fwrite(      z ,1,sizeof(INT_4),f );
       ::fwrite(     &s ,1,sizeof(INT_ ),f );
       ::fwrite(     &p ,1,sizeof(INT_ ),f );
       ::fwrite(     &r ,1,sizeof(INT_ ),f );
       ::fwrite( &(bx.n),1,sizeof(INT_ ),f );
       ::fwrite( &(bo.n),1,sizeof(INT_ ),f );
       ::fwrite( bx.data(),bx.n,sizeof(bvtx_t),f );
       ::fwrite( bo.data(),bo.n,sizeof(ibnd_t),f );
         return;
     };

      void fread( FILE *f )
     {
       ::fread(      z ,1,sizeof(INT_4),f );
       ::fread(     &s ,1,sizeof(INT_ ),f );
       ::fread(     &p ,1,sizeof(INT_ ),f );
       ::fread(     &r ,1,sizeof(INT_ ),f );
       ::fread( &(bx.n),1,sizeof(INT_ ),f );
       ::fread( &(bo.n),1,sizeof(INT_ ),f );
         bx.resize(  ydum );
         bo.resize( jbdum );
       ::fread( bx.data(),bx.n,sizeof(bvtx_t),f );
       ::fread( bo.data(),bo.n,sizeof(ibnd_t),f );
         return;
     };
  };

   const ibsrf_t ibdum;


/** Cavity **/

   struct href_t
  {
      frme_t    f;
      INT_      h;
      INT_      id;
      href_t(){ h=-1;id=-1; };
  };
   const href_t hhdum;

   struct hqref_t
  {
      bool       b;
      INT_6      v;
      hqref_t(){ v[0]=-1;v[1]=-1;v[2]=-1;v[3]=-1;v[4]=-1;v[5]=-1; b=false; };
  };
   const hqref_t hqdum;

   struct cavty_t
  {
      INT_      l,m,n;
      INT_      off[16];
      VINT_     v;
      vsize_t<hqref_t,DLEN_H>  q0,q1,q2,q3,q4,q5;
 
      cavty_t()
     {
         l= 0;
         m= 0;
         n= 0;
         memset( off,16,sizeof(INT_) );
     }

/*    inline INT_ ijk( INT_ i, INT_ j, INT_k ){ return i+l*( j+ m*k ); }
      inline INT_  jk(         INT_ j, INT_k ){ return j+m*  k;        }
      inline INT_  ik(         INT_ j, INT_k ){ return i+l*  k;        }
      inline INT_  ij(         INT_ j, INT_k ){ return i+l*  j;        }*/

// FACE I0

      inline INT_ vtx0( INT_ j,INT_ k )
     {
         INT_ val;
         if( j == 0 )
        {
            if( k == 0 ){ val= 0; }
            else
           {
               if( k < n  ){ val= off[2]+ 4*(k-1); }
               else        { val= 4;}
           }
        }
         else
        {
            if( j < m )
           {
               if( k == 0 ){ val= off[1]+ 4*(j-1); } 
               else
              {
                  if( k < n  ) { val= off[3]+ 2*( j-1+ (m-1)*(k-1) ); }
                  else         { val= off[1]+ 4*(j-1)+ 2; }
              }
           }
            else
           {
               if( k == 0 ) { val= 2; }
               else
              {
                  if( k < n  ) { val= off[2]+ 4*(k-1)+ 2; }
                  else         { val= 6; }
              }
           }
        }
         return v[val];
     };

      inline INT_ vtx1( INT_ j,INT_ k )
     {
         INT_ val;
         if( j == 0 )
        {
            if( k == 0 ){ val= 1; }
            else
           {
               if( k < n  ){ val= off[2]+ 4*(k-1)+1; }
               else        { val= 5;}
           }
        }
         else
        {
            if( j < m )
           {
               if( k == 0 ){ val= off[1]+ 4*(j-1)+1; } 
               else
              {
                  if( k < n  ) { val= off[3]+ 2*( j-1+ (m-1)*(k-1) )+1; }
                  else         { val= off[1]+ 4*(j-1)+ 3; }
              }
           }
            else
           {
               if( k == 0 ) { val= 3; }
               else
              {
                  if( k < n  ) { val= off[2]+ 4*(k-1)+ 3; }
                  else         { val= 7; }
              }
           }
        }
         return v[val];
     };
 
      inline INT_ vtx2( INT_ i,INT_ k )
     {
         INT_ val;
         if( i == 0 )
        {
            if( k == 0 ){ val= 0; }
            else
           {
               if( k < n  ){ val= off[2]+ 4*(k-1); }
               else        { val= 4;}
           }
        }
         else
        {
            if( i < l )
           {
               if( k == 0 ){ val= off[0]+ 4*(i-1); } 
               else
              {
                  if( k < n  ) { val= off[4]+ 2*( i-1+ (l-1)*(k-1) ); }
                  else         { val= off[0]+ 4*(i-1)+ 2; }
              }
           }
            else
           {
               if( k == 0 ) { val= 1; }
               else
              {
                  if( k < n  ) { val= off[2]+ 4*(k-1)+ 1; }
                  else         { val= 5; }
              }
           }
        }
         return v[val];
     };

      inline INT_ vtx3( INT_ i,INT_ k )
     {
         INT_ val;
         if( i == 0 )
        {
            if( k == 0 ){ val= 2; }
            else
           {
               if( k < n  ){ val= off[2]+ 4*(k-1)+2; }
               else        { val= 6;}
           }
        }
         else
        {
            if( i < l )
           {
               if( k == 0 ){ val= off[0]+ 4*(i-1)+1; } 
               else
              {
                  if( k < n  ) { val= off[4]+ 2*( i-1+ (l-1)*(k-1) )+ 1; }
                  else         { val= off[0]+ 4*(i-1)+ 3; }
              }
           }
            else
           {
               if( k == 0 ) { val= 3; }
               else
              {
                  if( k < n  ) { val= off[2]+ 4*(k-1)+ 3; }
                  else         { val= 7; }
              }
           }
        }
         return v[val];
     };



      inline INT_ vtx4( INT_ i,INT_ j )
     {
         INT_ val;
         if( i == 0 )
        {
            if( j == 0 ){ val= 0; }
            else
           {
               if( j < m  ){ val= off[1]+ 4*(j-1); }
               else        { val= 2;}
           }
        }
         else
        {
            if( i < l )
           {
               if( j == 0 ){ val= off[0]+ 4*(i-1); } 
               else
              {
                  if( j < m  ) { val= off[5]+ 2*( (i-1)+ (l-1)*(j-1) ); }
                  else         { val= off[0]+ 4*(i-1)+ 1; }
              }
           }
            else
           {
               if( j == 0 ) { val= 1; }
               else
              {
                  if( j < m  ) { val= off[1]+ 4*(j-1)+ 1; }
                  else         { val= 3; }
              }
           }
        }
         return v[val];
     };

      inline INT_ vtx5( INT_ i,INT_ j )
     {
         INT_ val;
         if( i == 0 )
        {
            if( j == 0 ){ val= 4; }
            else
           {
               if( j < m  ){ val= off[1]+ 4*(j-1)+2; }
               else        { val= 6;}
           }
        }
         else
        {
            if( i < l )
           {
               if( j == 0 ){ val= off[0]+ 4*(i-1)+2; } 
               else
              {
                  if( j < m  ) { val= off[5]+ 2*( (i-1)+ (l-1)*(j-1) )+1; }
                  else         { val= off[0]+ 4*(i-1)+ 3; }
              }
           }
            else
           {
               if( j == 0 ) { val= 5; }
               else
              {
                  if( j < m  ) { val= off[1]+ 4*(j-1)+ 3; }
                  else         { val= 7; }
              }
           }
        }
         return v[val];
     };
  };

   struct initz_t
  {
      INT_             v;
      vsize_t<INT_,8>  b;
      initz_t(){ v= -1; };
  };
   const initz_t dinitz;


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 09:51:27 BST 2018
// Changes History
// Next Change(s)  -

   typedef vsize_t< vtx_t,DLEN_H>  vvtx_t;
   typedef vsize_t<  pt_t,DLEN_H>   vpt_t;
   typedef vsize_t< hex_t,DLEN_H>  vhex_t;
   typedef vsize_t<qvad_t,DLEN_H> vqvad_t;


   inline void pts( vhex_t &hx, vqvad_t &qv, INT_ q, INT_ *buf )
#include<hmesh/extern/pts>

   inline INT_  quad0( vhex_t &hx, vqvad_t &qv, VINT_ &key, VINT_ &link, INT_ i, INT_ j )
#include<hmesh/extern/quad0>

   struct hmesh_t
  {
     
      vsize_t<FRAME_,8>        fr; /** Frames **/

      vvtx_t                   vx; /** Vetex coordinates **/
      vpt_t                    vt; /** Vertices **/
      vhex_t                   hx; /** Hexahedra **/
      vqvad_t                  qv; /** Quads **/

      vsize_t<  edg_t,DLEN_H>  eg; /** Edges **/

      wsize_t<INT_,4,DLEN_H>   pr; /** Pariodic surfaces **/

      VINT_                  indx; /** Hex ids **/

      INT_                     ng; /** Number of boundary quads **/

      INT_                     nb; /** Number of boundary surfaces **/
      INT_                     ni; /** Number of internal surfaces **/

      bsrf_t                   bz[MXGRP]; /** Boundary surfaces **/
      vsize_t<ibsrf_t,DLEN_H>  bi; /** Internal boundaries **/

      INT_                     ns; /** Number of boundary size controls **/
      sctrl_t                  sc[128]; /** Boundary size control **/

//    vsize_t<surf_c*,DLEN_H>  su; /** Surfaces **/

      vsize_t<initz_t,DLEN_H>  vz;

      hmesh_t();
     ~hmesh_t();

      inline void claim( INT_ i )
#include<hmesh/public/claim>

    inline void relabel( INT_ v, INT_ x, INT_ w, INT_ y, VINT_2 &hl, VINT_2 &ql )
#include<hmesh/public/relabel>

    inline void substitute( INT_ v, INT_ x, VINT_2 &hl, VINT_2 &ql, INT_ w, INT_ y, VINT_2 &hw, VINT_2 &qw )
#include<hmesh/public/substitute>

      inline INT_ bvtx( INT_  g, INT_ x, VINT_2 &hl, VINT_2 &ql, INT_ &i )
#include<hmesh/public/bvtx>


      inline void force( INT_ a, INT_ b, INT_ c, INT_ d )
#include<hmesh/public/force>

      inline void moveto( INT_ a, INT_ b )
#include<hmesh/public/moveto>

      inline void qswap( INT_ a, INT_ b )
#include<hmesh/public/qswap>

      inline void attach( INT_ g, INT_ n, INT_ j, INT_ s, INT_ p, INT_ r )
#include<hmesh/public/attach>

       inline void detach( INT_ g, INT_ m )
#include<hmesh/public/detach>

       inline void detach1( INT_ r, INT_ s )
#include<hmesh/public/detach1>

      inline void destroy( INT_ g, INT_ m )
#include<hmesh/public/destroy>

      inline void replace( INT_ a, INT_ i, INT_ b, INT_ j )
#include<hmesh/public/replace>

      inline void split( INT_ q )
#include<hmesh/public/split>

      inline void join( INT_ a, INT_ i, INT_ b, INT_ j )
#include<hmesh/public/join>

      inline void trimb()
#include<hmesh/public/trimb>

      inline void hex(  INT_ n, INT_ v0, INT_ v1, INT_ v2, INT_ v3,
                                INT_ v4, INT_ v5, INT_ v6, INT_ v7,
                                INT_ x0, INT_ x1, INT_ x2, INT_ x3,
                                INT_ x4, INT_ x5, INT_ x6, INT_ x7 )
#include<hmesh/public/hex>

      REAL_ vol( INT_ n ){ return ::vol( vx[hx[n].x[0]], vx[hx[n].x[1]], vx[hx[n].x[2]], vx[hx[n].x[3]], vx[hx[n].x[4]], vx[hx[n].x[5]], vx[hx[n].x[6]], vx[hx[n].x[7]] ); };

      void       read( FILE *f );
      void        ijk( INT_ l, INT_ m, INT_ n, INT_2 *mask, vsurf_t &su );
             
      void      quads();
      void      quads( VINT_ &p, INT_ ist, INT_ ien );
      void      quadz( VINT_ &p, INT_ ist, INT_ ien );
      void      edges();


      bool       walk( INT_ &l, INT_ &dir, VINT_ &mark );
      bool       walk( INT_ &i, dir_t *a,  VINT_ &mark );

      void     spread( INT_  i, dir_t  a );
      void      split( INT_ i0, dir_t dir );

      void      frame( INT_ k, char **data )
     { 
         if( k >= fr.n )
        {
            fr.insert(k,fdum);
        }
         printf( "try to set data for frame %d\n",k );
         fr[k].read( data ); 
     };

      void   periodic( );
      void      inter( INT_ b, vsurf_t &su );
             
      void      checq( const char* name );
      void      checx( const char* name );
      void        tec( const char* name );
      void        vtk( const char *name );
      void       btec( const char* name );
      void        wco( const char* name );
             
      void      qrend( INT_ l, FILE *f );
      void      hrend( INT_ l, FILE *f );
      void      hrend( INT_ i, dir_t a, FILE *f );
             
      void        del( INT_ i, INT_ *mask );
      void       del0( INT_ i, INT_ *mask );
      void        qel( INT_ id );

             
      void      point( INT_  v, INT_ w, INT_ x, VINT_2 &hl, VINT_2 &ql );
      void      point( INT_  v,                 VINT_2 &hl, VINT_2 &ql );

      void      point( INT_  v, INT_ w, INT_ x, VINT_2 &hl, VINT_2 &ql, VINT_2 &bl );
      void      point( INT_  v,                 VINT_2 &hl, VINT_2 &ql, VINT_2 &bl );

      void       edge( INT_  v,                 VINT_2 &list );
      void       edge( INT_  v,                 VINT_2 &hl, VINT_2 &ql );

      void        mul( INT_ j, INT_ k, INT_ n, INT_ *mask );
      void        add( hmesh_t &var, INT_ *mask );
      void        add( hmesh_t &var, VINT_2 &list, INT_ *mask, vsurf_t &s0, vsurf_t &s1 );
             
      bool        map( INT_ i, INT_ d, dir_t *a );

      void directions( INT_ i, INT_ o, INT_ *v, INT_ *w );

      void     spread( INT_ i,  dir_t   a,          VINT_ &list, vsize_t<dir_t,DLEN_H> &c );
      void     corner( INT_ i0, frme_t a0, INT_ *l, VINT_ &list );

      void   collapse( INT_ m, dir_t dir, VINT_ &list, VINT_4 &mkv, VINT_4 &mkh );

      void   collapse( INT_ m, dir_t   d0, dir_t d1, dir_t d2,VINT_ &list, VINT_4 &mkv, VINT_4 &mkh );

      void   collapse( INT_ i0, dir_t g, VINT_ &list );

      void      drill( INT_ i0, frme_t a0, VINT_ &list, frme_t *c  );
      void       pass( INT_ i0, frme_t a0, INT_ n, INT_ c0, INT_ c1, VINT_ &list );

      void    sbranch( REAL_3 v, INT_ i0, dir_t  d0, INT_ *mask, bool flag );

      void    zbranch( REAL_3 v, INT_ i0, dir_t d0, dir_t  d1, INT_ *mask, bool flag );

      void    fbranch( REAL_3 v, INT_ i0, dir_t  d0, INT_ *mask );

      void     tunnel( INT_ i0, frme_t a0, VINT_ &list );

      void       smap( vsurf_t &su );
      void       bmap( INT_ b, bool flag );
      void        geo( INT_ g, INT_ s, INT_ p, INT_ b, vsurf_t &su );

      void      sctrl( INT_ k );

      INT_     search(INT_ g,vtx_t p );
      void       fread( FILE *f );
      void      fwrite( FILE *f );

      void       check();
      void      broken()
#  include <hmesh/public/broken>

      void        dump();
      bool     compare( INT_ f, INT_ d, INT_ b, hmesh_t &o, INT_ c, VINT_ &prd, VINT_4 &prq );
      void    compare1( INT_ f, INT_ d, INT_ b, hmesh_t &o, INT_ c, VINT_ &pv, VINT_4 &qv );
      void    compare2( INT_ f, INT_ d, INT_ b, hmesh_t &o, INT_ c, VINT_ &pv, VINT_4 &qv );

      void      frme_walk( INT_ id0, frme_t f0, INT_ d, INT_ &id1, frme_t &f1 );
      void frame2vertices( INT_ id, frme_t f, INT_ v[2][2][2], INT_ x[2][2][2] );
      void    slot_kernel( INT_  hr0, frme_t f0, INT_ b2, INT_ b3, INT_  hl2, INT_  hl3, INT_  hl4, INT_ &hr2, INT_ &hr3, INT_ &hr4 );
      void           slot( INT_ id, frme_t f0, INT_ n, INT_ b0, INT_ b1, INT_ b2, INT_ b3 );
      void        lbranch( INT_ i0, frme_t f0, INT_ *b0 );                                                                             
      void       lbranch1( INT_ i0, frme_t f0, INT_ *b0 );                                                                             
      void sbranch2kernel( INT_ h0, frme_t f0, INT_ *n );

      void           digb( dir_t d0, dir_t d1, INT_ k0, REAL_3 l, INT_ *mask, REAL_ d );
      void           dig0( dir_t d0, dir_t d1, INT_ k0, REAL_3 l, INT_ *mask );
      void           dig1( dir_t d0, dir_t d1, INT_ k0, REAL_3 l, INT_ *mask );
      void           dig2( dir_t d0, dir_t d1, INT_ k0, REAL_3 l, INT_ *mask );
      void           dig3( dir_t d0, dir_t d1, INT_ k0, REAL_3 l, INT_ *mask );
      void           dig4( dir_t d0, dir_t d1, INT_ k0, REAL_3 l, INT_ *mask );
      void           dig5( dir_t d0, dir_t d1, INT_ k0, REAL_3 l, INT_ *mask );
      void           dig6( dir_t d0, dir_t d1, INT_ k0, REAL_3 l, INT_ *mask, REAL_ d );

      void           cvty( INT_ l0, INT_ m0, INT_ n0, dir_t d0, dir_t d1, INT_ k0, cavty_t &var, bool flag );
      void            ex0( dir_t d0, dir_t d1, INT_ k0, INT_ l0, INT_ m0, REAL_3 l, INT_ *mask );

      void           lref( dir_t d0, dir_t d1, INT_ k0, INT_ l0, INT_ m0 );



      void            box( REAL_3 dx, REAL_3 dy, VINT_ &list );
      void          ngref( INT_ a, INT_ b, hqref_t &var );
  };

   struct shmesh_t
  {
      hmesh_t  msh[16];
      vsurf_t  geo[16];
  };
#  endif

