#  ifndef SORT_H
#  define SORT_H

/**@ingroup system
  *@{
 **/

/**@defgroup sorting Sorting algorithms
  *@{
 **/

#  include <typd.h>
#  include <utils/proto.h>

   template < typename type > void sift_down( INT_ n, type *val, INT_ *iprm, INT_ l , INT_ r )
#  include <sort/public/sift_down>
;

   template < typename type > void sift_down( INT_ n, type *var, INT_ *iprm )
#  include <sort/public/sift_down0>
;

   template < typename type > void select( INT_ k, INT_ n, INT_ *iprm, type *x )
#  include <sort/public/select>
;

/** Heap sort
   @param n                 Number of elements
   @param val               Elements
   @param iprm              Permutation array
   @todo  Known bug for small n.
 **/
   template < typename type > void hsort( INT_ n, type *val, INT_ *iprm )
#  include <sort/public/hsort>
;
/** Bubble sort. Use only if n<5.
   @param n                 Number of elements
   @param val               Elements
   @param iprm              Permutation array
 **/
   template < typename type > void bsort( INT_ n, type *val, INT_ *iprm )
#  include <sort/public/bsort>
;

/** In place bubble sort. On exit iprm is 1 if the number of permutations needed to sort the array is even,
   -1 otherwise.
   @param n                 Number of elements
   @param val               Elements
   @param iprm              Permutation index.
 **/
   template < typename type > void psort( INT_ n, type *val, INT_ *iprm )
#  include <sort/public/psort>
;

/**
  *@}
 **/

/**@
  *@}
 **/

#  endif 
