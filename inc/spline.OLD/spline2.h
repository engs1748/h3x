#  ifndef _SPLINE2_
#  define _SPLINE2_

#  include <spline/spline.h>

   inline void splin2(REAL_ *x1a, REAL_ *x2a, REAL_ *ya, REAL_ *y2a, INT_ m, INT_ n, REAL_ x1, REAL_ x2, REAL_ &y)
  {
      INT_   j,k;
      REAL_ *ytmp,*yytmp;

      ytmp=  new REAL_[m];
      yytmp= new REAL_[m];

      k= 0;
      for(j=0;j<m;j++)
     {
         splint(x2a,ya+k,y2a+k,n,x2, yytmp[j]);
         k+= n;
     }
      spline(x1a,yytmp,m,SPLINE_NOGRAD,SPLINE_NOGRAD,ytmp);
      splint(x1a,yytmp,ytmp,m,x1,y);

      delete[] yytmp;
      delete[]  ytmp;
  }

   inline void splin2(REAL_ *x1a, REAL_ *x2a, REAL_ *ya, REAL_ *y2a, INT_ m, INT_ n, REAL_ x1, REAL_ x2, REAL_ &y, REAL_ *dy)
  {
      INT_   j,k;
      REAL_ *ytmp,*yytmp;
      REAL_ *dytmp,*dyytmp;

      ytmp=  new REAL_[m];
      yytmp= new REAL_[m];

      dytmp=  new REAL_[m];
      dyytmp= new REAL_[m];

      k= 0;
      for(j=0;j<m;j++)
     {
         splint(x2a,ya+k,y2a+k,n,x2, yytmp[j],dyytmp[j]);
         k+= n;
     }
      spline(x1a, yytmp, m,SPLINE_NOGRAD,SPLINE_NOGRAD, ytmp);
      spline(x1a,dyytmp,m,SPLINE_NOGRAD,SPLINE_NOGRAD,dytmp);
      splint(x1a, yytmp, ytmp,m,x1,y,dy[0]);
      splint(x1a,dyytmp,dytmp,m,x1,dy[1]);

      delete[] yytmp;
      delete[]  ytmp;
      delete[] dyytmp;
      delete[] dytmp;
  }

   inline void splin2(REAL_ *x1a, REAL_ *x2a, REAL_ *ya, REAL_ *y2a, INT_ m, INT_ n, REAL_ x1, REAL_ x2, REAL_ &y, REAL_ *dy, REAL_ *d2y )
  {
      INT_   j,k;
      REAL_   *ytmp  ,*yytmp;
      REAL_  *dytmp,* dyytmp;
      REAL_ *d2ytmp,*d2yytmp;

      ytmp=  new REAL_[m];
      yytmp= new REAL_[m];

      dytmp=  new REAL_[m];
      dyytmp= new REAL_[m];

      d2ytmp=  new REAL_[m];
      d2yytmp= new REAL_[m];

      k= 0;
      for(j=0;j<m;j++)
     {
         splint(x2a,ya+k,y2a+k,n,x2, yytmp[j],dyytmp[j],d2yytmp[j]);
         k+= n;
     }

      spline(x1a,  yytmp,m, SPLINE_NOGRAD,SPLINE_NOGRAD,  ytmp);
      spline(x1a, dyytmp,m, SPLINE_NOGRAD,SPLINE_NOGRAD, dytmp);
      spline(x1a,d2yytmp,m, SPLINE_NOGRAD,SPLINE_NOGRAD,d2ytmp);

      splint(x1a,  yytmp,  ytmp,m,x1,  y,    dy[0],d2y[0]);
      splint(x1a, dyytmp, dytmp,m,x1, dy[1],d2y[1]);
      splint(x1a,d2yytmp,d2ytmp,m,x1,d2y[2]);

      delete[] yytmp;
      delete[]  ytmp;
      delete[] dyytmp;
      delete[] dytmp;
  }

   inline void splie2(REAL_ *x1a, REAL_ *x2a, REAL_ *ya, INT_ m, INT_ n, REAL_ *y2a)
  {
      INT_ j,k;

      k=0;
      for(j=0;j<m;j++)
     { 
         spline(x2a,ya+k,n,SPLINE_NOGRAD,SPLINE_NOGRAD,y2a+k); 
         k+=n;
     }
  }

   inline void splin2(REAL_ *x1a, REAL_ *x2a, REAL_3 *ya, REAL_3 *y2a, INT_ m, INT_ n, REAL_ x1, REAL_ x2, REAL_3 y)
  {
      INT_   j,k;
      REAL_3 *ytmp,*yytmp;
      REAL_3  dum={SPLINE_NOGRAD,SPLINE_NOGRAD,SPLINE_NOGRAD};

      ytmp=  new REAL_3[m];
      yytmp= new REAL_3[m];

      k= 0;
      for(j=0;j<m;j++)
     {
         splint(x2a,ya+k,y2a+k,n,x2, yytmp[j]);
         k+= n;
     }
      spline(x1a,yytmp,m,dum,dum,ytmp);
      splint(x1a,yytmp,ytmp,m,x1,y);

      delete[] yytmp;
      delete[]  ytmp;
  }

   inline void splie2(REAL_ *x1a, REAL_ *x2a, REAL_3 *ya, INT_ m, INT_ n, REAL_3 *y2a)
  {
      INT_ j,k;
      REAL_3  dum={SPLINE_NOGRAD,SPLINE_NOGRAD,SPLINE_NOGRAD};

      k=0;
      for(j=0;j<m;j++)
     { 
         spline(x2a,ya+k,n,dum,dum,y2a+k); 
         k+=n;
     }
  }

   inline void splin2(REAL_ *x1a, REAL_ *x2a, REAL_3 *ya, REAL_3 *y2a, INT_ m, INT_ n, REAL_ x1, REAL_ x2, REAL_3 y, REAL_3 *dy)
  {
      INT_   j,k;
      REAL_3 *ytmp,*yytmp;
      REAL_3 *dytmp,*dyytmp;

      REAL_3  dum={SPLINE_NOGRAD,SPLINE_NOGRAD,SPLINE_NOGRAD};

      ytmp=  new REAL_3[m];
      yytmp= new REAL_3[m];

      dytmp=  new REAL_3[m];
      dyytmp= new REAL_3[m];

      k= 0;
      for(j=0;j<m;j++)
     {
         splint(x2a,ya+k,y2a+k,n,x2, yytmp[j],dyytmp[j]);
         k+= n;
     }
      spline(x1a, yytmp, m,dum,dum, ytmp);
      spline(x1a,dyytmp,m,dum,dum,dytmp);
      splint(x1a, yytmp, ytmp,m,x1,y,dy[0]);
      splint(x1a,dyytmp,dytmp,m,x1,dy[1]);

      delete[] yytmp;
      delete[]  ytmp;
      delete[] dyytmp;
      delete[] dytmp;
  }

   inline void splin2(REAL_ *x1a, REAL_ *x2a, REAL_3 *ya, REAL_3 *y2a, INT_ m, INT_ n, REAL_ x1, REAL_ x2, REAL_3 y, REAL_3 *dy, REAL_3 *d2y )
  {
      INT_   j,k;
      REAL_3   *ytmp  ,*yytmp;
      REAL_3  *dytmp,* dyytmp;
      REAL_3 *d2ytmp,*d2yytmp;

      REAL_3  dum={SPLINE_NOGRAD,SPLINE_NOGRAD,SPLINE_NOGRAD};

      ytmp=    new REAL_3[m];
      yytmp=   new REAL_3[m];

      dytmp=   new REAL_3[m];
      dyytmp=  new REAL_3[m];

      d2ytmp=  new REAL_3[m];
      d2yytmp= new REAL_3[m];

      k= 0;
      for(j=0;j<m;j++)
     {
         splint(x2a,ya+k,y2a+k,n,x2, yytmp[j],dyytmp[j],d2yytmp[j]);
         k+= n;
     }

      spline(x1a,  yytmp,m, dum,dum,  ytmp);
      spline(x1a, dyytmp,m, dum,dum, dytmp);
      spline(x1a,d2yytmp,m, dum,dum,d2ytmp);

      splint(x1a,  yytmp,  ytmp,m,x1,  y,    dy[0],d2y[0]);
      splint(x1a, dyytmp, dytmp,m,x1, dy[1],d2y[1]);
      splint(x1a,d2yytmp,d2ytmp,m,x1,d2y[2]);

      delete[] yytmp;
      delete[]  ytmp;
      delete[] dyytmp;
      delete[] dytmp;
  }

#endif

