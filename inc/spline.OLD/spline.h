#  ifndef _SPLINE_
#  define _SPLINE_

#  include <typd.h>
#  include <cmath>
#  include <cassert>

#  define SPLINE_NOGRAD 0.99e30

   inline void spline(REAL_ *x, REAL_ *y, INT_ n, REAL_ yp1, REAL_ ypn, REAL_ *y2 )
  {
      INT_ i,k;
      REAL_ p,qn,sig,un;
      REAL_ *u;

      u= new REAL_[n];

      if( yp1 >= SPLINE_NOGRAD )
     {
         y2[0]=u[0]=0.0;
     }
      else 
     {
         y2[0] = -0.5;
         u[0]=(3.0/(x[1]-x[0]))*((y[1]-y[0])/(x[1]-x[0])-yp1);
     }
      for(i=1;i<n-1;i++) 
     {
         sig=(x[i]-x[i-1])/(x[i+1]-x[i-1]);
         p=sig*y2[i-1]+2.0;
         y2[i]=(sig-1.0)/p;
         u[i]=(y[i+1]-y[i])/(x[i+1]-x[i]) - (y[i]-y[i-1])/(x[i]-x[i-1]);
         u[i]=(6.0*u[i]/(x[i+1]-x[i-1])-sig*u[i-1])/p;
     }
      if(ypn >= SPLINE_NOGRAD )
     {
         qn=un=0.0;
     }
      else 
     {
         qn=0.5;
         un=(3.0/(x[n-1]-x[n-2]))*(ypn-(y[n-1]-y[n-2])/(x[n-1]-x[n-2]));
     }
      y2[n-1]=(un-qn*u[n-2])/(qn*y2[n-2]+1.0);
      for(k=n-2;k>=0;k--)
     {
         y2[k]=y2[k]*y2[k+1]+u[k];
     }

      delete[] u; u= NULL; 
  };

   inline void spline(REAL_ *x, REAL_3 *y, INT_ n, REAL_3 yp1, REAL_3 ypn, REAL_3 *y2 )
  {
      INT_ i,j,k;
      REAL_ p,qn,sig,un;
      REAL_3 *u;

      u= new REAL_3[n];

      for( j=0;j<3;j++ )
     {
         if( yp1[j] >= SPLINE_NOGRAD )
        {
            y2[0][j]=u[0][j]=0.0;
        }
         else 
        {
            y2[0][j] = -0.5;
            u[0][j]=(3.0/(x[1]-x[0]))*((y[1][j]-y[0][j])/(x[1]-x[0])-yp1[j]);
        }
     }

      for(i=1;i<n-1;i++) 
     {
         sig=(x[i]-x[i-1])/(x[i+1]-x[i-1]);
         for( j=0;j<3;j++ )
        {
            p=sig*y2[i-1][j]+2.0;
            y2[i][j]=(sig-1.0)/p;
            u[i][j]=(y[i+1][j]-y[i][j])/(x[i+1]-x[i]) - (y[i][j]-y[i-1][j])/(x[i]-x[i-1]);
            u[i][j]=(6.0*u[i][j]/(x[i+1]-x[i-1])-sig*u[i-1][j])/p;
        }
     }
      for( j=0;j<3;j++ )
     {
         if(ypn[j] >= SPLINE_NOGRAD )
        {
            qn=un=0.0;
        }
         else 
        {
            qn=0.5;
            un=(3.0/(x[n-1]-x[n-2]))*(ypn[j]-(y[n-1][j]-y[n-2][j])/(x[n-1]-x[n-2]));
        }
         y2[n-1][j]=(un-qn*u[n-2][j])/(qn*y2[n-2][j]+1.0);
     }
      for(k=n-2;k>=0;k--)
     {
         y2[k][0]=y2[k][0]*y2[k+1][0]+u[k][0];
         y2[k][1]=y2[k][1]*y2[k+1][1]+u[k][1];
         y2[k][2]=y2[k][2]*y2[k+1][2]+u[k][2];
     }

      delete[] u; u= NULL; 
  };

// values, derivatives and second derivatives

   inline void splint(REAL_ *xa, REAL_ *ya, REAL_ *y2a, INT_ n, REAL_ x, REAL_ &y )
  {

      INT_ k0,k1,k;
      REAL_ h,b,a;

      k0=0;
      k1=n-1;

      while (k1-k0 > 1) 
     {
         k=(k1+k0) >> 1;
         if (xa[k] > x){ k1=k; }else{ k0=k; }
     }
      h=xa[k1]-xa[k0];

      assert( h > 0 );

      a=(xa[k1]-x)/h;
      b=(x-xa[k0])/h;

      y=a*ya[k0]+ b*ya[k1]+((a*a*a-a)*y2a[k0]+(b*b*b-b)*y2a[k1])*(h*h)/6.0;

      return;
  }

   inline void splint(REAL_ *xa, REAL_ *ya, REAL_ *y2a, INT_ n, REAL_ x, 
                      REAL_ &y, REAL_ &dy )
  {

      INT_ k0,k1,k;
      REAL_ h,b,a;
      REAL_ da,db;

      k0=0;
      k1=n-1;

      while (k1-k0 > 1) 
     {
         k=(k1+k0) >> 1;
         if (xa[k] > x){ k1=k; }else{ k0=k; }
     }
      h=xa[k1]-xa[k0];

      assert( h > 0 );

      a=(xa[k1]-x)/h;
      b=(x-xa[k0])/h;

      da= -1./h;
      db= -da;

      y=a*ya[k0]+ b*ya[k1]+((a*a*a-a)*y2a[k0]+(b*b*b-b)*y2a[k1])*(h*h)/6.0;
      dy=da*ya[k0]+ db*ya[k1]+((3*a*a*da-da)*y2a[k0]+(3*b*b*db-db)*y2a[k1])*(h*h)/6.0;

      return;
  }

   inline void splint(REAL_ *xa, REAL_ *ya, REAL_ *y2a, INT_ n, REAL_ x, 
                      REAL_ &y, REAL_ &dy, REAL_ &d2y )
  {

      INT_ k0,k1,k;
      REAL_ h,b,a;
      REAL_ da,db;

      k0=0;
      k1=n-1;

      while (k1-k0 > 1) 
     {
         k=(k1+k0) >> 1;
         if (xa[k] > x){ k1=k; }else{ k0=k; }
     }
      h=xa[k1]-xa[k0];

      assert( h > 0 );

      a=(xa[k1]-x)/h;
      b=(x-xa[k0])/h;

      da= -1./h;
      db= -da;

      y=a*ya[k0]+ b*ya[k1]+((a*a*a-a)*y2a[k0]+(b*b*b-b)*y2a[k1])*(h*h)/6.0;
      dy=da*ya[k0]+ db*ya[k1]+((3*a*a*da-da)*y2a[k0]+(3*b*b*db-db)*y2a[k1])*(h*h)/6.0;
      d2y=                     a*y2a[k0]+b*y2a[k1];

      return;
  }

// values, derivatives and second derivatives for REAL_3 values

   inline void splint(REAL_ *xa, REAL_3 *ya, REAL_3 *y2a, INT_ n, REAL_ x, REAL_3 y )
  {

      INT_ k0,k1,k;
      REAL_ h,b,a, a1,b1,w;

      k0=0;
      k1=n-1;

      while (k1-k0 > 1) 
     {
         k=(k1+k0) >> 1;
         if (xa[k] > x){ k1=k; }else{ k0=k; }
     }
      h=xa[k1]-xa[k0];

      assert( h > 0 );

      a=(xa[k1]-x)/h;
      b=(x-xa[k0])/h;

      a1= a*a*a-a;
      b1= b*b*b-b;

      w= h*h/6.;
      a1*= w;
      b1*= w;

      y[0]=a*ya[k0][0]+ b*ya[k1][0]+(a1*y2a[k0][0]+b1*y2a[k1][0]);
      y[1]=a*ya[k0][1]+ b*ya[k1][1]+(a1*y2a[k0][1]+b1*y2a[k1][1]);
      y[2]=a*ya[k0][2]+ b*ya[k1][2]+(a1*y2a[k0][2]+b1*y2a[k1][2]);

      return;
  }

   inline void splint(REAL_ *xa, REAL_3 *ya, REAL_3 *y2a, INT_ n, REAL_ x, 
                      REAL_3 y, REAL_3 dy )
  {

      INT_ k0,k1,k;
      REAL_ h,b,a, a1,b1,w, a2,b2;
      REAL_ da,db;

      k0=0;
      k1=n-1;

      while (k1-k0 > 1) 
     {
         k=(k1+k0) >> 1;
         if (xa[k] > x){ k1=k; }else{ k0=k; }
     }
      h=xa[k1]-xa[k0];

      assert( h > 0 );

      a=(xa[k1]-x)/h;
      b=(x-xa[k0])/h;

      da= -1./h;
      db= -da;

      a1= a*a*a-a;
      b1= b*b*b-b;

      a2= 3*a*a*da-da;
      b2= 3*b*b*db-db;

      w= h*h/6.;
      a2*= w;
      b2*= w;

      a1*= w;
      b1*= w;

      y[0]=  a*ya[k0][0]+  b*ya[k1][0]+(a1*y2a[k0][0]+b1*y2a[k1][0]);
      y[1]=  a*ya[k0][1]+  b*ya[k1][1]+(a1*y2a[k0][1]+b1*y2a[k1][1]);
      y[2]=  a*ya[k0][2]+  b*ya[k1][2]+(a1*y2a[k0][2]+b1*y2a[k1][2]);

      dy[0]=da*ya[k0][0]+ db*ya[k1][0]+(a2*y2a[k0][0]+b2*y2a[k1][0]);
      dy[1]=da*ya[k0][1]+ db*ya[k1][1]+(a2*y2a[k0][1]+b2*y2a[k1][1]);
      dy[2]=da*ya[k0][2]+ db*ya[k1][2]+(a2*y2a[k0][2]+b2*y2a[k1][2]);

      return;
  }

   inline void splint(REAL_ *xa, REAL_3 *ya, REAL_3 *y2a, INT_ n, REAL_ x, 
                      REAL_3 y, REAL_3 dy, REAL_3 d2y )
  {

      INT_ k0,k1,k;
      REAL_ h,b,a, a1,b1,w, a2,b2;
      REAL_ da,db;

      k0=0;
      k1=n-1;

      while (k1-k0 > 1) 
     {
         k=(k1+k0) >> 1;
         if (xa[k] > x){ k1=k; }else{ k0=k; }
     }
      h=xa[k1]-xa[k0];

      assert( h > 0 );

      a=(xa[k1]-x)/h;
      b=(x-xa[k0])/h;

      da= -1./h;
      db= -da;

      a1= a*a*a-a;
      b1= b*b*b-b;

      a2= 3*a*a*da-da;
      b2= 3*b*b*db-db;

      w= h*h/6.;
      a2*= w;
      b2*= w;

      a1*= w;
      b1*= w;

      y[0]=   a*ya[k0][0]+  b*ya[k1][0]+(a1*y2a[k0][0]+b1*y2a[k1][0]);
      y[1]=   a*ya[k0][1]+  b*ya[k1][1]+(a1*y2a[k0][1]+b1*y2a[k1][1]);
      y[2]=   a*ya[k0][2]+  b*ya[k1][2]+(a1*y2a[k0][2]+b1*y2a[k1][2]);

      dy[0]= da*ya[k0][0]+ db*ya[k1][0]+(a2*y2a[k0][0]+b2*y2a[k1][0]);
      dy[1]= da*ya[k0][1]+ db*ya[k1][1]+(a2*y2a[k0][1]+b2*y2a[k1][1]);
      dy[2]= da*ya[k0][2]+ db*ya[k1][2]+(a2*y2a[k0][2]+b2*y2a[k1][2]);

      d2y[0]=                            a *y2a[k0][0]+b *y2a[k1][0];
      d2y[1]=                            a *y2a[k0][1]+b *y2a[k1][1];
      d2y[2]=                            a *y2a[k0][2]+b *y2a[k1][2];

      return;
  }

#endif

