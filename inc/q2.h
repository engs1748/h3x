#  ifndef _Q2_
#  define _Q2_

#  include <typd.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   inline void q1( REAL_ x, REAL_2 q, REAL_2 dq )
  {
      q[0]= 1-x;
      q[1]=   x;

      dq[0]= -1;
      dq[1]=  1;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   inline void q11( REAL_ x, REAL_ y, REAL_4 q, REAL_4 dq[2] )
  {
      REAL_2   u,du;
      REAL_2   v,dv;

      q1( x, u,du );
      q1( y, v,dv );

      q[0]= u[0]*v[0];
      q[1]= u[1]*v[0];
      q[2]= u[0]*v[1];
      q[3]= u[1]*v[1];

      dq[0][0]= du[0]* v[0];
      dq[0][1]= du[1]* v[0];
      dq[0][2]= du[0]* v[1];
      dq[0][3]= du[1]* v[1];

      dq[1][0]=  u[0]*dv[0];
      dq[1][1]=  u[1]*dv[0];
      dq[1][2]=  u[0]*dv[1];
      dq[1][3]=  u[1]*dv[1];

      
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   inline void q111( REAL_ x, REAL_ y, REAL_ z, REAL_8 q, REAL_8 dq[3] )
  {
      REAL_2   u,du;
      REAL_2   v,dv;
      REAL_2   w,dw;

      q1( x, u,du );
      q1( y, v,dv );
      q1( z, w,dw );

      q[0]= u[0]*v[0]*w[0];
      q[1]= u[1]*v[0]*w[0];
      q[2]= u[0]*v[1]*w[0];
      q[3]= u[1]*v[1]*w[0];

      q[4]= u[0]*v[0]*w[1];
      q[5]= u[1]*v[0]*w[1];
      q[6]= u[0]*v[1]*w[1];
      q[7]= u[1]*v[1]*w[1];

      dq[0][0]= du[0]* v[0]* w[0];
      dq[0][1]= du[1]* v[0]* w[0];
      dq[0][2]= du[0]* v[1]* w[0];
      dq[0][3]= du[1]* v[1]* w[0];

      dq[0][4]= du[0]* v[0]* w[1];
      dq[0][5]= du[1]* v[0]* w[1];
      dq[0][6]= du[0]* v[1]* w[1];
      dq[0][7]= du[1]* v[1]* w[1];

      dq[1][0]=  u[0]*dv[0]* w[0];
      dq[1][1]=  u[1]*dv[0]* w[0];
      dq[1][2]=  u[0]*dv[1]* w[0];
      dq[1][3]=  u[1]*dv[1]* w[0];

      dq[1][4]=  u[0]*dv[0]* w[1];
      dq[1][5]=  u[1]*dv[0]* w[1];
      dq[1][6]=  u[0]*dv[1]* w[1];
      dq[1][7]=  u[1]*dv[1]* w[1];

      dq[2][0]=  u[0]* v[0]*dw[0];
      dq[2][1]=  u[1]* v[0]*dw[0];
      dq[2][2]=  u[0]* v[1]*dw[0];
      dq[2][3]=  u[1]* v[1]*dw[0];

      dq[2][4]=  u[0]* v[0]*dw[1];
      dq[2][5]=  u[1]* v[0]*dw[1];
      dq[2][6]=  u[0]* v[1]*dw[1];
      dq[2][7]=  u[1]* v[1]*dw[1];
      
  }

   inline void q2( REAL_ x, REAL_ *p )
  {
      p[0]= 2*(x-0.5)*  (x-1);
      p[1]=-4*        x*(x-1);
      p[2]= 2*(x-0.5)*x;

      return;
  };

   inline void q2( REAL_ x, REAL_ *p, REAL_ *dp )
  {
      p[0]= 2*(x-0.5)*  (x-1);
      p[1]=-4*        x*(x-1);
      p[2]= 2*(x-0.5)*x;

      dp[0]= 2*  (x-1) +2*(x-0.5);
      dp[1]=-4*  (x-1) -4* x     ;
      dp[2]= 2*x       +2*(x-0.5);

      return;
  };

   inline void q2( REAL_ x, REAL_ *p, REAL_ *dp, REAL_ *d2p )
  {
      p[0]= 2*(x-0.5)*  (x-1);
      p[1]=-4*        x*(x-1);
      p[2]= 2*(x-0.5)*x;

      dp[0]= 2*  (x-1) +2*(x-0.5);
      dp[1]=-4*  (x-1) -4* x     ;
      dp[2]= 2*x       +2*(x-0.5);

      d2p[0]= 4;
      d2p[1]=-8;
      d2p[2]= 4;


      return;
  };

   inline void ql2( REAL_ x, REAL_ *p )
  {
      if( x < 0.5 )
     {
         p[0]= 2*( 0.5-x );
         p[1]= 2*      x;
         p[2]= 0;
     }
      else
     {
         p[0]= 0;
         p[1]= 2*( 1-x );
         p[2]= 2*( x-0.5 );
     }

      return;
  };

   inline void q22( REAL_ x, REAL_ y, REAL_ *p )
  {
      REAL_ p0[3],p1[3];

      q2( x, p0 );
      q2( y, p1 );

      p[0+0]= p0[0]*p1[0];
      p[1+0]= p0[1]*p1[0];
      p[2+0]= p0[2]*p1[0];

      p[0+3]= p0[0]*p1[1];
      p[1+3]= p0[1]*p1[1];
      p[2+3]= p0[2]*p1[1];

      p[0+6]= p0[0]*p1[2];
      p[1+6]= p0[1]*p1[2];
      p[2+6]= p0[2]*p1[2];

      return;
  };

   inline void q22( REAL_ x, REAL_ y, REAL_ *p, REAL_2 *dp )
  {
      REAL_ p0[3],p1[3];
      REAL_ dp0[3],dp1[3];

      q2( x, p0,dp0 );
      q2( y, p1,dp1 );

      p[0+0]= p0[0]*p1[0];
      p[1+0]= p0[1]*p1[0];
      p[2+0]= p0[2]*p1[0];

      p[0+3]= p0[0]*p1[1];
      p[1+3]= p0[1]*p1[1];
      p[2+3]= p0[2]*p1[1];

      p[0+6]= p0[0]*p1[2];
      p[1+6]= p0[1]*p1[2];
      p[2+6]= p0[2]*p1[2];

      dp[0+0][0]= dp0[0]*p1[0];
      dp[1+0][0]= dp0[1]*p1[0];
      dp[2+0][0]= dp0[2]*p1[0];

      dp[0+3][0]= dp0[0]*p1[1];
      dp[1+3][0]= dp0[1]*p1[1];
      dp[2+3][0]= dp0[2]*p1[1];

      dp[0+6][0]= dp0[0]*p1[2];
      dp[1+6][0]= dp0[1]*p1[2];
      dp[2+6][0]= dp0[2]*p1[2];

      dp[0+0][1]= p0[0]*dp1[0];
      dp[1+0][1]= p0[1]*dp1[0];
      dp[2+0][1]= p0[2]*dp1[0];

      dp[0+3][1]= p0[0]*dp1[1];
      dp[1+3][1]= p0[1]*dp1[1];
      dp[2+3][1]= p0[2]*dp1[1];

      dp[0+6][1]= p0[0]*dp1[2];
      dp[1+6][1]= p0[1]*dp1[2];
      dp[2+6][1]= p0[2]*dp1[2];

      return;
  };

   inline void q22( REAL_ x, REAL_ y, REAL_ *p, REAL_2 *dp, REAL_3 *d2p )
  {
      REAL_ p0[3],p1[3];
      REAL_ dp0[3],dp1[3];
      REAL_ d2p0[3],d2p1[3];

      q2( x, p0,dp0,d2p0 );
      q2( y, p1,dp1,d2p1 );

      p[0+0]= p0[0]*p1[0];
      p[1+0]= p0[1]*p1[0];
      p[2+0]= p0[2]*p1[0];

      p[0+3]= p0[0]*p1[1];
      p[1+3]= p0[1]*p1[1];
      p[2+3]= p0[2]*p1[1];

      p[0+6]= p0[0]*p1[2];
      p[1+6]= p0[1]*p1[2];
      p[2+6]= p0[2]*p1[2];

      dp[0+0][0]= dp0[0]*p1[0];
      dp[1+0][0]= dp0[1]*p1[0];
      dp[2+0][0]= dp0[2]*p1[0];

      dp[0+3][0]= dp0[0]*p1[1];
      dp[1+3][0]= dp0[1]*p1[1];
      dp[2+3][0]= dp0[2]*p1[1];

      dp[0+6][0]= dp0[0]*p1[2];
      dp[1+6][0]= dp0[1]*p1[2];
      dp[2+6][0]= dp0[2]*p1[2];

      dp[0+0][1]= p0[0]*dp1[0];
      dp[1+0][1]= p0[1]*dp1[0];
      dp[2+0][1]= p0[2]*dp1[0];

      dp[0+3][1]= p0[0]*dp1[1];
      dp[1+3][1]= p0[1]*dp1[1];
      dp[2+3][1]= p0[2]*dp1[1];

      dp[0+6][1]= p0[0]*dp1[2];
      dp[1+6][1]= p0[1]*dp1[2];
      dp[2+6][1]= p0[2]*dp1[2];

      d2p[0+0][0]= d2p0[0]*p1[0];
      d2p[1+0][0]= d2p0[1]*p1[0];
      d2p[2+0][0]= d2p0[2]*p1[0];

      d2p[0+3][0]= d2p0[0]*p1[1];
      d2p[1+3][0]= d2p0[1]*p1[1];
      d2p[2+3][0]= d2p0[2]*p1[1];

      d2p[0+6][0]= d2p0[0]*p1[2];
      d2p[1+6][0]= d2p0[1]*p1[2];
      d2p[2+6][0]= d2p0[2]*p1[2];

      d2p[0+0][1]= p0[0]*d2p1[0];
      d2p[1+0][1]= p0[1]*d2p1[0];
      d2p[2+0][1]= p0[2]*d2p1[0];

      d2p[0+3][1]= p0[0]*d2p1[1];
      d2p[1+3][1]= p0[1]*d2p1[1];
      d2p[2+3][1]= p0[2]*d2p1[1];

      d2p[0+6][1]= p0[0]*d2p1[2];
      d2p[1+6][1]= p0[1]*d2p1[2];
      d2p[2+6][1]= p0[2]*d2p1[2];

      d2p[0+0][2]= dp0[0]*dp1[0];
      d2p[1+0][2]= dp0[1]*dp1[0];
      d2p[2+0][2]= dp0[2]*dp1[0];

      d2p[0+3][2]= dp0[0]*dp1[1];
      d2p[1+3][2]= dp0[1]*dp1[1];
      d2p[2+3][2]= dp0[2]*dp1[1];

      d2p[0+6][2]= dp0[0]*dp1[2];
      d2p[1+6][2]= dp0[1]*dp1[2];
      d2p[2+6][2]= dp0[2]*dp1[2];

      return;
  };

   inline void ql22( REAL_ x, REAL_ y, REAL_ *p )
  {
      REAL_ p0[3],p1[3];

      ql2( x, p0 );
      ql2( y, p1 );

      p[0+0]= p0[0]*p1[0];
      p[1+0]= p0[1]*p1[0];
      p[2+0]= p0[2]*p1[0];

      p[0+3]= p0[0]*p1[1];
      p[1+3]= p0[1]*p1[1];
      p[2+3]= p0[2]*p1[1];

      p[0+6]= p0[0]*p1[2];
      p[1+6]= p0[1]*p1[2];
      p[2+6]= p0[2]*p1[2];

      return;
  };

   inline void q222( REAL_ x, REAL_ y, REAL_ z, REAL_ *p )
  {
      REAL_ p0[3],p1[3],p2[3];

      q2( x, p0 );
      q2( y, p1 );
      q2( z, p2 );

      p[0+ 0+ 0]= p0[0]*p1[0]*p2[0];
      p[1+ 0+ 0]= p0[1]*p1[0]*p2[0];
      p[2+ 0+ 0]= p0[2]*p1[0]*p2[0];

      p[0+ 3+ 0]= p0[0]*p1[1]*p2[0];
      p[1+ 3+ 0]= p0[1]*p1[1]*p2[0];
      p[2+ 3+ 0]= p0[2]*p1[1]*p2[0];

      p[0+ 6+ 0]= p0[0]*p1[2]*p2[0];
      p[1+ 6+ 0]= p0[1]*p1[2]*p2[0];
      p[2+ 6+ 0]= p0[2]*p1[2]*p2[0];

      p[0+ 0+ 9]= p0[0]*p1[0]*p2[1];
      p[1+ 0+ 9]= p0[1]*p1[0]*p2[1];
      p[2+ 0+ 9]= p0[2]*p1[0]*p2[1];

      p[0+ 3+ 9]= p0[0]*p1[1]*p2[1];
      p[1+ 3+ 9]= p0[1]*p1[1]*p2[1];
      p[2+ 3+ 9]= p0[2]*p1[1]*p2[1];

      p[0+ 6+ 9]= p0[0]*p1[2]*p2[1];
      p[1+ 6+ 9]= p0[1]*p1[2]*p2[1];
      p[2+ 6+ 9]= p0[2]*p1[2]*p2[1];

      p[0+ 0+18]= p0[0]*p1[0]*p2[2];
      p[1+ 0+18]= p0[1]*p1[0]*p2[2];
      p[2+ 0+18]= p0[2]*p1[0]*p2[2];

      p[0+ 3+18]= p0[0]*p1[1]*p2[2];
      p[1+ 3+18]= p0[1]*p1[1]*p2[2];
      p[2+ 3+18]= p0[2]*p1[1]*p2[2];

      p[0+ 6+18]= p0[0]*p1[2]*p2[2];
      p[1+ 6+18]= p0[1]*p1[2]*p2[2];
      p[2+ 6+18]= p0[2]*p1[2]*p2[2];

      return;
  };

#  endif
