#ifndef _MTR_
#define _MTR_

# include <typd.h>
# include <m33.h>

  inline void ainv31( REAL_  a0, REAL_  a1, REAL_  a2, 
                      REAL_  b0, REAL_  b1, REAL_  b2, 
                      REAL_  c0, REAL_  c1, REAL_  c2, 
                      REAL_ &x0, REAL_ &x1, REAL_ &x2, REAL_ &x3, 
                      REAL_  f0, REAL_  f1, REAL_  f2, REAL_  f3 )
 {
     REAL_ d0,d1,d2;
     REAL_ e0,e1,e2;
     REAL_ d;

     d0= 1./a0;
     d1= 1./a1;
     d2= 1./a2;

     e0= b0*d0; 
     e1= b1*d1; 
     e2= b2*d2; 

     x0= f0*d0;
     x1= f1*d1;
     x2= f2*d2;

     d= c0*e0+ c1*e1+ c2*e2;
     d= -d;

     x3= f3- c0*x0- c1*x1- c2*x2;
     x3/= d;
 
     x0-= e0*x3;
     x1-= e1*x3;
     x2-= e2*x3;

     return;
 }

  inline void ainv32( REAL_ a0, REAL_ a1, REAL_ a2, 
                      REAL_ b0, REAL_ b1, REAL_ b2, 
                      REAL_ c0, REAL_ c1, REAL_ c2,
                      REAL_ l0, REAL_ l1, REAL_ l2,
                      REAL_ m0, REAL_ m1, REAL_ m2, 
                      REAL_ &x0, REAL_ &x1, REAL_ &x2, REAL_ &x3, REAL_ &x4, 
                      REAL_  r0, REAL_  r1, REAL_  r2, REAL_  r3, REAL_  r4 )
 {
     REAL_ d0,d1,d2;
     REAL_ s0,s1,s2;
     REAL_ t0,t1,t2;
     REAL_ u0,u1;
     REAL_ q0,q1,q2,q3;
     REAL_ d;

     d0= 1./a0;
     d1= 1./a1;
     d2= 1./a2;

     s0= b0*d0; 
     s1= b1*d1; 
     s2= b2*d2; 

     t0= c0*d0; 
     t1= c1*d1; 
     t2= c2*d2; 

     x0= r0*d0;
     x1= r1*d1;
     x2= r2*d2;

     q0= -( s0*l0+ s1*l1+ s2*l2 );
     q1= -( s0*m0+ s1*m1+ s2*m2 );

     q2= -( t0*l0+ t1*l1+ t2*l2 );
     q3= -( t0*m0+ t1*m1+ t2*m2 );

     u0= r3- ( l0*x0+ l1*x1+ l2*x2 );
     u1= r4- ( m0*x0+ m1*x1+ m2*x2 );

     d= q0*q3- q1*q2;
     d= 1./d;

     x3= u0*q3- u1*q2;
     x4=-u0*q1+ u1*q0;

     x3*= d;
     x4*= d;

     x0-= ( s0*x3+ t0*x4 );
     x1-= ( s1*x3+ t1*x4 );
     x2-= ( s2*x3+ t2*x4 );

     return;
 }

  inline void ainv33( REAL_ a0, REAL_ a1, REAL_ a2, 
                      REAL_ b0, REAL_ b1, REAL_ b2, 
                      REAL_ c0, REAL_ c1, REAL_ c2,
                      REAL_ d0, REAL_ d1, REAL_ d2,
                      REAL_ l0, REAL_ l1, REAL_ l2,
                      REAL_ m0, REAL_ m1, REAL_ m2, 
                      REAL_ n0, REAL_ n1, REAL_ n2, 
                      REAL_ &x0, REAL_ &x1, REAL_ &x2, REAL_ &x3, REAL_ &x4, REAL_ &x5, 
                      REAL_  r0, REAL_  r1, REAL_  r2, REAL_  r3, REAL_  r4, REAL_  r5 )
 {
     REAL_ f0,f1,f2;
     REAL_ s0,s1,s2;
     REAL_ t0,t1,t2;
     REAL_ u0,u1,u2;
     REAL_ v[3];
     REAL_ w[3];
     REAL_ q[9];
     REAL_ p[9];

     f0= 1./a0;
     f1= 1./a1;
     f2= 1./a2;

     s0= b0*f0; 
     s1= b1*f1; 
     s2= b2*f2; 

     t0= c0*f0; 
     t1= c1*f1; 
     t2= c2*f2; 

     u0= d0*f0; 
     u1= d1*f1; 
     u2= d2*f2; 

     x0= r0*f0;
     x1= r1*f1;
     x2= r2*f2;

     q[0]= -( s0*l0+ s1*l1+ s2*l2 );
     q[1]= -( s0*m0+ s1*m1+ s2*m2 );
     q[2]= -( s0*n0+ s1*n1+ s2*n2 );

     q[3]= -( t0*l0+ t1*l1+ t2*l2 );
     q[4]= -( t0*m0+ t1*m1+ t2*m2 );
     q[5]= -( t0*n0+ t1*n1+ t2*n2 );

     q[6]= -( u0*l0+ u1*l1+ u2*l2 );
     q[7]= -( u0*m0+ u1*m1+ u2*m2 );
     q[8]= -( u0*n0+ u1*n1+ u2*n2 );

     v[0]= r3- ( l0*x0+ l1*x1+ l2*x2 );
     v[1]= r4- ( m0*x0+ m1*x1+ m2*x2 );
     v[2]= r5- ( n0*x0+ n1*x1+ n2*x2 );

     inv330( q,p );
     dot33( p,v,w );

     x3=w[0];
     x4=w[1];
     x5=w[2];

     x0-= ( s0*x3+ t0*x4+ u0*x5 );
     x1-= ( s1*x3+ t1*x4+ u1*x5 );
     x2-= ( s2*x3+ t2*x4+ u2*x5 );

     return;
 }

#endif
