#  ifndef _PAR_
#  define _PAR_

#  include <mmap/mmap.h>
#  include <dir.h>
#  include <utils/proto.h>

#  include <cstdlib>
#  include <cstdio>

#  define   NCOM        4
#  define   NTASK       65536

   struct com_t
  {
      INT_               n; // number of entities
                   
      INT_            *idx; // absolute number of each entitiy
                   
      frme_t          *hfx; // frame for subscript transformation from entity to hex

      INT_            *ldx; // pointers to all hex touched by entity
      INT_2           *mdx;  
      INT_4          *list; // all hex touched by entity
                   
      INT_           *lsnd; // pointers to send list
      INT_4          *isnd; // send list

      INT_2           *ndx;
      INT_            *aux;

      com_t();
     ~com_t();

      INT_     rem( INT_ neig )
     { 
         INT_   k;
         INT_   ist=0,ien=0;

         k= ldx[0];

         if( k > 0 )
        {
            ien= mdx[k-1][1];

            k= ldx[neig-1];

            ist= ien;
            ien= mdx[k-1][1];
        } 
         return ien-ist;
     };

      INT_     loc()
     { 
         INT_ m= ldx[0];
         if( m > 0 ){ m= mdx[m-1][1]; }
         return m;
     }

      void     dims( INT_ l, INT_ m, INT_ p );
            
      void    reset( INT_ l, INT_ m, INT_ p );
            
      void     rcvl( INT_ neig, INT_ *c, INT_ *wc, INT_ *prm, INT_ *alias );
            
      void    fread( INT_ neig, FILE *f );
      void   fwrite( INT_ neig, FILE *f );
            
      void   printf( INT_ neig );
            
      void    start( INT_ neig, INT_ *ineg, struct hex_t *h, FILE *f );
            
      void     pick( struct msg_t &msg, INT_ &m, INT_ *t );
      void      all( struct msg_t &msg, INT_ &m, INT_ *t );

      void     task( INT_ i, INT_ &l, INT_ &m, INT_ **v );
      void     task( INT_ i, INT_ &l, INT_ &m, INT_ **v, INT_ *w, INT_ *u );
      void taskinfo( MMAP_   &var, INT_ a, frule_t &r, INT_ &c );
  };

   struct msg_t
  {
      INT_             tag;
      INT_            *dep;

      INT_               n;
      INT_               l;
      INT_            *col;

      MPI_Request    *sreq; 
      MPI_Request    *rreq; 

      MPI_Status    *sstat; 
      MPI_Status    *rstat; 

      msg_t();
     ~msg_t();


  };

   struct par_t
  {
      INT_    rank,size; 
      INT_      threads;

      INT_         neig;
      INT_        *ineg;
      INT_         tags;

      com_t         com[NCOM];

      FILE        *file;

      par_t();
     ~par_t();

      void fwritep( FILE *f );
      void  freadp( FILE *f );

      void   open( msg_t &msg, com_t &com );
      void  close( msg_t &msg );

      void malloc( msg_t &msg, struct MMAP_   &var, com_t &com );
      void   free( msg_t &msg );

      void   exchange( REAL_ *x, struct MMAP_   &var, com_t &com, msg_t &msg );
      void noexchange( REAL_ *x, struct MMAP_   &var, com_t &com, msg_t &msg );


      bool transit( com_t &com, msg_t &msg, INT_ &ntsk, INT_ *tasks, INT_ local );
      bool  locals( com_t &com, msg_t &msg, INT_ &ntsk, INT_ *tasks );
      bool     all( com_t &com, msg_t &msg, INT_ &ntsk, INT_ *tasks );

  };

#  endif
