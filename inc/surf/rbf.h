# ifndef _RBFSURF_
# define _RBFSURF_

# include <rbf/rbf_interp.h>
# include <surf/implicit.h>

/*
   implicit surface defined by a radial basis function interpolation of a pointcloud.
   see Carr et al, "Reconstruction and Representation of 3D Objects with Radial Basis Functions", SIGGRAPH 2001
*/

   class rbfsurf_c: public surf_c, public rbf_interp<3,vtx_t,REAL_>, public surfimplicit_t
  {
      private:
      // from surfimplicit
         REAL_ F(  vtx_t p0           );
         void dF(  vtx_t p0, REAL_ *d );
         void d2F( vtx_t p0, REAL_ *h );

      // from rbf_interp
         int  build_weights();
         void malloc();
         void free();

         int         m;
         vtx_t  *norms;
         REAL_   scale;
         REAL_ *scales;

         void offSurfacePoints();

      public:
         rbfsurf_c(){ norms=NULL; scales=NULL; }
        ~rbfsurf_c(){ free(); }

/** Return an integer indicating the type of the surface **/
         surf_e type(){ return surf_rbf; }

/** Automatically generate patches **/
         void autov( char **arg );

/** Copy constructor **/
         void copy( surf_c *var );

/** Compare the definition of two surfaces **/
         bool compare( surf_c *s );

/** Read surface parameters from an array of strings **/
         void read( char **arg );

/** 3D coordinates x, tangent vectors dx, and curvature tensor dx2 of the point at parametric coordinates s **/
         void interp( REAL_ *s, vtx_t &x                         );
         void interp( REAL_ *s, vtx_t &x, vtx_t  *dx             );
         void interp( REAL_ *s, vtx_t &x, vtx_t  *dx, vtx_t *dx2 );


/** Approximate project of 3D vertex v onto surface. y are the surface coordinates of the projection **/
         void aprj( vtx_t &w, REAL_ *y, INT_ j, aprj_t &stat );

/** Exact projection of 3D vertex v. y are the surface coordinates of the projection **/
         void  prj( vtx_t &w, REAL_ *y,         aprj_t &stat );

/** Update the boundary vertex at z with the relaxed displacement f projected onto the surface **/
         void updvtx( REAL_ *f, REAL_ *z, REAL_ rlx );

/** Construct surface from ASCII point+normal cloud file **/
         void freadPointCloud( char **arg );

/** Read/write to binary file **/
         void fwrite( FILE *f );
         void fread(  FILE *f );
  };

# endif
