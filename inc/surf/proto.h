#  ifndef _SURF_
#  define _SURF_

#  include <cstdlib>
#  include <cstdio>
#  include <cstring>
#  include <cassert>
#  include <cmath>

#  include <consts.h>
#  include <vsize.h>
#  include <typd.h>
#  include <m33.h>
#  include <vtx.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Sun 19 Aug 21:59:17 BST 2018
// Changes History -
// Next Change(s)  -

#  define SURF_TOL 1.e-9

   enum surf_e { surf_bad=-1, surf_cylinder, surf_plane, surf_bilinear, surf_biquadratic,
                 surf_sampled, surf_spline, surf_cspline, surf_num, surf_rbf };

/** constructor **/

   void newsurf( INT_ t, class surf_c **val );

   
   typedef vsize_t<surf_c*,32> vsurf_t;

   struct patch_t
  {
      INT_4        v;
      REAL_        x0[3];
      REAL_         z[9];
      REAL_         d[3];

      patch_t()
     { 
         memset( v,0,sizeof(INT_4)); 
         memset(x0,0,3*sizeof(REAL_)); 
         memset( z,0,9*sizeof(REAL_)); 
         memset( d,0,3*sizeof(REAL_));
     };
  };


   class surf_c
  {

      public:

         wsize_t<REAL_,3,32>   v;
         vsize_t<patch_t,32>   p;

         char             name[128];

         surf_c(){};
         virtual ~surf_c(){};

/** Return an integer indicating the type of the function **/
         virtual surf_e type(){ return surf_bad; };

/** Copy constructor **/
         virtual void copy( surf_c *var ){};

/** Copy constructor: creates a new instance of surf_c with the appropriate type **/
         void copy( surf_c **var )
        {
            assert( !(*var) );
            newsurf( type(),var );

            memcpy( (*var)->name,name, 128 );

         ((*var)->v).n= v.n;((*var)->v).resize(0.);
            patch_t dum;
         ((*var)->p).n= p.n;((*var)->p).resize(dum);

            for( INT_ i=0;i<v.n;i++ ){ for( INT_ j=0;j<2;j++ ){ ((*var)->v)[i][j]= v[i][j]; }}
            for( INT_ i=0;i<p.n;i++ ){ ((*var)->p)[i]= p[i]; }

            copy(*var);
            return;
        };

/** Compare the definition of two surfaces **/
         virtual bool compare( surf_c *s ){ return false; }

/** Read surface parameters from an array of strings **/
         virtual void read( char **arg ){};

/** 3D coordinates x of the point at parametric coordinates s **/
         virtual void interp( REAL_ *s, vtx_t &x                         ){ assert( false ); };

/** 3D coordinates x and tangent vectors dx of the point at parametric coordinates s **/
         virtual void interp( REAL_ *s, vtx_t &x, vtx_t  *dx             ){ assert( false ); };

/** 3D coordinates x, tangent vectors dx, and curvature tensor dx2 of the point at parametric coordinates s **/
         virtual void interp( REAL_ *s, vtx_t &x, vtx_t  *dx, vtx_t *dx2 ){ assert( false ); };


/** Test the surface in the patch with corners y0,y1,y2,y3 **/
         void test( REAL_b y0, REAL_b y1, REAL_b y2, REAL_b y3, char *f );

/** Test the surface in patch i **/
         void test( INT_ i, char *f );

/** Test the whole surface **/
         virtual void test( char **arg ){};


/** Add a named vertex **/
         virtual INT_ vtx( char **arg );

/** Add a named patch **/
         INT_ patch( char **arg );

/** Automatically generate patches **/
         virtual void autov( char **arg ){};

/** Approximate project of 3D vertex v on patch j. y are the surface coordinates of the projection **/
         virtual void aprj( vtx_t &w, REAL_ *y, INT_ j, aprj_t &stat )
        {
      
            vtx_t                     u[4];
      
            REAL_4                    q;
            REAL_2                    s;
      
            interp( v[p[j].v[0]], u[0] );
            interp( v[p[j].v[1]], u[1] );
            interp( v[p[j].v[2]], u[2] );
            interp( v[p[j].v[3]], u[3] );
      
            s[0]=0.5;
            s[1]=0.5;
      
          ::aprj( u[0],u[1],u[2],u[3], w,s, stat );
            q[0]= (1-s[0])*(1-s[1]);
            q[1]=    s[0] *(1-s[1]);
            q[2]= (1-s[0])*   s[1] ;
            q[3]=    s[0] *   s[1] ;
      
            y[0]= q[0]*v[p[j].v[0]][0]+ q[1]*v[p[j].v[1]][0]+ q[2]*v[p[j].v[2]][0]+ q[3]*v[p[j].v[3]][0];
            y[1]= q[0]*v[p[j].v[0]][1]+ q[1]*v[p[j].v[1]][1]+ q[2]*v[p[j].v[2]][1]+ q[3]*v[p[j].v[3]][1];
      
            interp( y, w );
         }

/** Approximate project of 3D vertex v on patch j. y are the surface coordinates of the projection **/
         void srch( vtx_t &v, REAL_ *y );

/** Exact projection of 3D vertex v. y are the surface coordinates of the projection **/
         virtual void  prj( vtx_t &x0, REAL_ *y,         aprj_t &stat )
        {
      
            REAL_   rlx;
            REAL_   err;
      
            REAL_    d;
            REAL_    b[2];
            REAL_    a[3];
      
            vtx_t   dx[3];
            vtx_t   d2x[3];
      
            vtx_t   x;
            vtx_t   r;
      
            INT_    j;
      
            rlx= 0.1;
      
            for( j=0;j<stat.mit;j++ )
           {
      
      // Bilinear shape function
               interp( y,x,dx,d2x );
      
               r= x-x0;
      
      // Distance
               d=  r*r;
               d*= 0.5;
      
      // Derivative of distance wrt parametric coordinates
      
               b[0]= r*dx[0];
               b[1]= r*dx[1];
      
      // Second derivatives
      
               a[0]=  dx[0]*dx[0]+ r*d2x[0];
               a[1]=  dx[0]*dx[1]+ r*d2x[2];
               a[2]=  dx[1]*dx[1]+ r*d2x[1];
      
      // LDL' factorization and solution
      
               ldl2( a,b );
      
      // Newton update
      
               y[0]-= rlx*b[0];
               y[1]-= rlx*b[1];
      
      // Convergence criterion
      
               err= fabs(b[0])+ fabs(b[1]);
               if( err < stat.tol ){ break; };
      
               rlx*= 1.1;
               rlx= fmin( rlx,1. );
      
      //       printf( "%2d % 9.3e % 9.3e % 9.3e % 9.3e % 9.3e\n", j, d, y[0],y[1], b[0],b[1] );
      
           }
            stat.a[0]= a[0];
            stat.a[1]= a[1];
            stat.a[2]= a[2];
      
            stat.x   = x;
      
            stat.b[0]= b[0];
            stat.b[1]= b[1];
      
            stat.d=    d;
      
            stat.err=  err;
            stat.it=   j;
      
      //    assert( stat.it < stat.mit );
      
            return;
        }

/** For frame of reference for patch p **/
         void  ref( INT_ p );

/** For frame of reference for patch p **/
         void  pnorm( INT_ p, vtx_t &x, vtx_t &n );

/** Update the boundary vertex at z with the relaxed displacement f projected onto the surface **/
         virtual void updvtx( REAL_ *f, REAL_ *z, REAL_ rlx )
        {
            // default update for parametric surface
            REAL_3   u,  q[2], dz;
            vtx_t    v, dv[2];

            interp( z, v,dv );

            qrf23( dv[0].x,dv[1].x, q[0],q[1], u );
            qrs23( q[0],q[1], u, dz, f );

            z[0]+= rlx*dz[0];
            z[1]+= rlx*dz[1];
            return;
        }


/** Write to binary file **/
         virtual void fwrite( FILE *f )
        {
          ::fwrite( name,128,sizeof(char), f );
          ::fwrite(&(v.n)     ,1,  sizeof(INT_),   f );
          ::fwrite( v.data() ,v.n,sizeof(REAL_b),   f );
          ::fwrite(&(p.n)     ,1,  sizeof(INT_),   f );
          ::fwrite( p.data() ,p.n,sizeof(patch_t),   f );
            return;
        }

/** Write to binary file **/
         virtual void fread( FILE *f )
        {
          ::fread( name,128,sizeof(char), f );
          ::fread( &(v.n)     ,1,  sizeof(INT_),   f );
            v.resize(-1);
          ::fread( v.data() ,v.n,sizeof(REAL_b),   f );
          ::fread(&(p.n)     ,1,  sizeof(INT_),   f );
            patch_t pdum;
            p.resize(pdum);
          ::fread( p.data() ,p.n,sizeof(patch_t),   f );
            return;
        }
  };

#   include <surf/cyl.h>
#   include <surf/bil.h>
#   include <surf/pla.h>
#   include <surf/biq.h>
#   include <surf/sam.h>
#   include <surf/spl.h>
#   include <surf/rbf.h>

   void fwrite( vsurf_t &var, FILE *f );
   void  fread( vsurf_t &var, FILE *f );

#  endif
