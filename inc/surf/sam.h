
#  ifndef _SAM_
#  define _SAM_

#  include <poly.h>
#  define SAM_QSIZE 512

/** sampled surfaces
    Automatically generated patch covers the interval [0,1]x[0,1]
**/

   class sampled_c: public surf_c
  {
      private:
         vsize_t<vtx_t,32>       x0;
         INT_                    n,m;
      public:
         sampled_c()
        { 
            n=0; 
            m=0; 
        };
         virtual surf_e type(){ return surf_sampled; };
         void read( char **arg );
         void copy( surf_c *var );
         bool compare( surf_c *var );


         void autov( char **arg );
         void interp( REAL_ *s, vtx_t  &x );
         void interp( REAL_ *s, vtx_t  &x, vtx_t  *dx );
         void interp( REAL_ *s, vtx_t  &x, vtx_t  *dx, vtx_t *dx2 );
         void test( char *name );

         void fread( FILE *f );
         void fwrite( FILE *f );
  };

#  endif
