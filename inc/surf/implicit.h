# ifndef _IMPSURF_
# define _IMPSURF_

#include <assert.h>
#include <vtx.h>
#include <consts.h>
#include <linalg.h>


// class for surface defined by implicit function
   struct surfimplicit_t
  {
      REAL_   accuracy; //uncertainty margin for projections onto surface

      surfimplicit_t(){accuracy=EPS;}
     ~surfimplicit_t(){}

   // returns distance from point p to closest point on surface
      REAL_ distance( vtx_t p0 );

   // calculate normal, gradient and curvature of surface at point closest to p0
      void tangent( vtx_t p0, vtx_t *t, bool proj, bool approx );

      void normal( vtx_t p0, vtx_t &n, bool proj, bool approx );

      void curvature( vtx_t p0, vtx_t *c, bool proj, bool approx );

   // q is projection of point p onto surface.
      void project( vtx_t p0, vtx_t &q0, bool approx );

   // implicit function (pseudo-distance function)
      virtual REAL_ F( vtx_t p0 ) = 0;

   // gradient of implicit function
      virtual void dF( vtx_t p0, REAL_ *d ) = 0;

   // second gradient (hessian) of implicit function
      virtual void d2F( vtx_t p0, REAL_ *h ) = 0;

   // right hand side for projection newton iterations
      void newton_rhs( vtx_t x0, REAL_ *nu, REAL_ *rhs, REAL_ *df );

   // jacobian matrix for projection newton iterations
      void newton_jac( vtx_t x0, REAL_ *nu, REAL_ *jac, REAL_ *df );
  };

# endif
