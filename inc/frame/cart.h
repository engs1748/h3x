#  ifndef _CART_
#  define _CART_

#  include <typd.h>
#  include <m33.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

/** A data structure to represent cartesian periodic boxes **/

   struct cart_t
  {

      private:

         INT_        n;              /** Number of periodic directions  **/
         REAL_3      x[3];           /** Periodic transformation vector **/
         REAL_3      y0[3];          /** Vectors orthogonal to the periodic directions - first vector **/
         REAL_3      y1[3];          /** Vectors orthogonal to the periodic directions - second vector **/
         REAL_       d[3];           /** Periodic distance (linear)     **/
         REAL_       v[3];           /** Frame speed       (linear)     **/

      public:

         cart_t()
        {
            n=0;            
            memset( x,-1,sizeof(REAL_3)*3 );
        };

        ~cart_t()
        {
            n=0;            
            memset( x,-1,sizeof(REAL_3)*3 );
        };

/** Build up the frame information from ASCII data **/

         void read( char **data )
        {
            INT_ k;
            INT_3 iprm;

            printf( "Assign data for frame %p\n", this );

            k= n++;
            assert( k<3 );
            
            sscanf( data[0],"%lf", x[k]+0  );
            sscanf( data[1],"%lf", x[k]+1  );
            sscanf( data[2],"%lf", x[k]+2  );
            sscanf( data[3],"%lf", d+k     );
            sscanf( data[4],"%lf", v+k     );

            sort3( x[0],iprm );
            if( fabs(x[k][iprm[0]]) > fabs(x[k][iprm[2]] ) ){ swap( iprm[0],iprm[2] ); }

            y0[k][iprm[0]]= x[k][iprm[2]];
            y0[k][iprm[1]]= 0;
            y0[k][iprm[2]]=-x[k][iprm[0]];

            y1[k][iprm[0]]= 0;
            y1[k][iprm[1]]= x[k][iprm[2]];
            y1[k][iprm[2]]=-x[k][iprm[1]];

            for( k=0;k<n;k++ )
           {
               printf( "%e %e %e\n", x[k][0],x[k][1],x[k][2] );
           }

            return;
        }

/** Reduce three-dimensional cartesian coordinates to a pair of transformed coordinates for
    the purpose of identifying periodic point pairs **/

         inline void redx( REAL_3 a, REAL_2 y, INT_ k )
        {
            y[0]= dot3( a,y0[k] );
            y[1]= dot3( a,y1[k] );
        }

/** Transform coordinates using the k-th transofmration vector with orientation s (+/-1) **/

         inline void prdx( REAL_3 a, REAL_3 b, INT_ k, INT_ s )
        {
            REAL_ c= s*d[k];
            b[0]= a[0]+c*x[k][0];
            b[1]= a[1]+c*x[k][1];
            b[2]= a[2]+c*x[k][2];
            return;
        };

         inline void prdx( vtx_t a, vtx_t &b, INT_ k, INT_ s )
        {
            REAL_ c= s*d[k];
            b[0]= a[0]+c*x[k][0];
            b[1]= a[1]+c*x[k][1];
            b[2]= a[2]+c*x[k][2];
            return;
        };

/** Transform vectors using the k-th transofmration vector with orientation s (+/-1) **/

         inline void prdv( REAL_3 a, REAL_3 b, INT_ k, INT_ s )
        {
        };

/** Transform tensors using the k-th transofmration vector with orientation s (+/-1) **/

         inline void prda( REAL_ *a, REAL_ *b, INT_ k, INT_ s )
        {
        };

 
  };

#  endif
