#  ifndef _DIR_
#  define _DIR_

#  include <misc.h>
#  include <cstdlib>

/** Data structure to handle i,j,k directions in the blocks of a multiblock grid.
    The data structure contains three 2-bit values for the directions of the
    i,j,k axis. **/

   struct dir_t
  {
      INT_         i:2;
      INT_         j:2;
      INT_         k:2;

      unsigned dum0:1;
      unsigned dum1:1;

      dir_t(){ i=0; j=0; k=0; dum0= 0; dum1= 0; };
      void init(){ i=0; j=0; k=0; dum0= 0; dum1= 0; };
      dir_t( INT_ l, INT_ m, INT_ n ){ i=l; j=m; k=n; dum0=0;dum1=0; };
      dir_t( bool s, INT_ l )
     {
         INT_ v[3]={0,0,0};
         INT_ m=1;
         if( !s ){ m=-m; };
         v[l]= m;
         i= v[0];
         j= v[1];
         k= v[2];

         dum0=0;
         dum1=0;
     }

       inline INT_ dir()
      {
          INT_ val;
          val= abs(j)+ 2*abs(k);
          return val;
      };

       inline bool sign()
      {
          bool val;
          val=        (i>>1);
          val= val || (j>>1);
          val= val || (k>>1);
          return !val;
      }

       void print()
      {
          printf( "[ %2d %2d %2d ]\n", i,j,k );
      }

       inline void vec( INT_ *v )
      {
          v[0]= max(0,i);
          v[1]= max(0,j);
          v[2]= max(0,k);
      }

       inline void svec( INT_ *v )
      {
          v[0]= i;
          v[1]= j;
          v[2]= k;
      }

  };

   inline dir_t operator+( dir_t a, dir_t b )
  {
      dir_t val;
      val.i= a.i+b.i;
      val.j= a.j+b.j;
      val.k= a.k+b.k;
      return val;
  };

   inline dir_t operator-( dir_t a, dir_t b )
  {
      dir_t val;
      val.i= a.i-b.i;
      val.j= a.j-b.j;
      val.k= a.k-b.k;
      return val;
  };

   inline bool operator==( dir_t d0, dir_t d1 )
  {
      bool b;
      b= true;
      if( d0.i != d1.i ){ b=false; return b; }
      if( d0.j != d1.j ){ b=false; return b; }
      if( d0.k != d1.k ){ b=false; return b; }
      return b;
  };

   inline dir_t operator!( dir_t b )
  {
      dir_t val;
      val.i= -b.i;
      val.j= -b.j;
      val.k= -b.k;
      return val;
  };

   inline dir_t abs( dir_t b )
  {
      dir_t val;
      val.i= ::abs(b.i);
      val.j= ::abs(b.j);
      val.k= ::abs(b.k);
      return val;
  };

   typedef dir_t frme_t[3];

   inline dir_t vec( dir_t a, dir_t b )
  {
      dir_t val;
      int u[3],v[3],w[3];

      a.svec(u);
      b.svec(v);

      w[0]= u[1]*v[2]- u[2]*v[1];
      w[1]= u[2]*v[0]- u[0]*v[2];
      w[2]= u[0]*v[1]- u[1]*v[0];

      val.i= w[0];
      val.j= w[1];
      val.k= w[2];
      
      return val; 
      
  }

   inline dir_t dmap( dir_t a, frme_t b, frme_t c )
  {
      INT_   idx[3];
      INT_   k; 
      dir_t val;

      k= b[0].dir(); idx[k]= 0;
      k= b[1].dir(); idx[k]= 1;
      k= b[2].dir(); idx[k]= 2;

      k= a.dir();
      k= idx[k];
      val= c[k];
      if( a.sign() != b[k].sign() ){ val= !val; };

      return val;

  };

   inline INT_ coor( dir_t i, dir_t j, dir_t k )
  {
      INT_ val;
      INT_  p,q,r;
      INT_  l,m,n;
      INT_  v[3];

      l= i.dir(); 
      m= j.dir(); 
      n= k.dir(); 

      p= i.sign();
      q= j.sign();
      r= k.sign();
      
      v[l]= p;
      v[m]= q;
      v[n]= r;

      val= v[0]+ 2*v[1]+ 4*v[2];

      return val;

  }

   struct frule_t
  {
      INT_3     f[3];
      INT_3     o;
      INT_3     s;

      frule_t(){ o[0]=0;o[1]=0;o[2]=0; memset( f,0,3*sizeof(INT_3) ); };
      frule_t( frme_t g, INT_ l, INT_ m, INT_ n )
     {
         o[0]= l-1;
         o[1]= m-1;
         o[2]= n-1;

         g[0].svec(f[0]);     
         g[1].svec(f[1]);     
         g[2].svec(f[2]);     

         for( INT_ i=0;i<3;i++ )
        {
           INT_ j=g[i].dir();
           s[i]= -1;
           if( g[i].sign() ){ o[j]= 0; s[i]=1; };
        }
     };

      inline void transform( INT_ i, INT_ j, INT_ k, INT_ &x,INT_ &y,INT_ &z )
     {
         x= o[0]+ f[0][0]*i+ f[1][0]*j+ f[2][0]*k;
         y= o[1]+ f[0][1]*i+ f[1][1]*j+ f[2][1]*k;
         z= o[2]+ f[0][2]*i+ f[1][2]*j+ f[2][2]*k;
     }

      inline void itransform( INT_ &i, INT_ &j, INT_ &k, INT_ x,INT_ y,INT_ z )
     {
         x-= o[0];
         y-= o[1];
         z-= o[2];

         i= f[0][0]*x+ f[0][1]*y+ f[0][2]*z;
         j= f[1][0]*x+ f[1][1]*y+ f[1][2]*z;
         k= f[2][0]*x+ f[2][1]*y+ f[2][2]*z;

/*       x= o[0]+ f[0][0]*i+ f[1][0]*j+ f[2][0]*k;
         y= o[1]+ f[0][1]*i+ f[1][1]*j+ f[2][1]*k;
         z= o[2]+ f[0][2]*i+ f[1][2]*j+ f[2][2]*k;*/
     }

      inline INT_ sign( INT_ i ){ return s[i]; };

  };

#  endif
