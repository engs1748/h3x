#  ifndef _POLY_
#  define _POLY_

   inline void lagr( INT_ n, REAL_ x, REAL_ *q )
  {
      INT_  j,k;
      REAL_ p=1./(n-1);
      REAL_ r,s,t,u;
      s= 0;
      for( k=0;k<n;k++ )
     {
         u= 1;

         r= 1;
         t= 0;
 
         for( j=0;j<n;j++ )
        {
            if( j != k )
           {
               r*=    (s- t);
               u*= (x-j*p);
           }
            t+= p;
        }

         q[k]=    u;

         r= 1./r;

         q[k]*=  r;

         s+= p;
     }
  };

   inline void lagr( INT_ n, REAL_ x, REAL_ *q, REAL_ *q1 )
  {
      INT_  i,j,k;
      REAL_ p=1./(n-1);
      REAL_ r,s,t,u,v,z;
      s= 0;
      for( k=0;k<n;k++ )
     {
         u= 1;
         z= 0;

         r= 1;
         t= 0;
 
         for( j=0;j<n;j++ )
        {
            if( j != k )
           {
               r*=    (s- t);
               u*= (x-j*p);
               v= 1;
               for( i=0;i<n;i++ )
              {
                  if( i != k && i != j )
                 {
                     v*= (x-i*p);
                 }
              }
               z+= v;
           }
            t+= p;
        }

         q[k]=    u;
         q1[k]=   z;

         r= 1./r;

         q[k]*=  r;
         q1[k]*= r;

         s+= p;
     }
  };

   inline void lagr( INT_ n, REAL_ x, REAL_ *q, REAL_ *q1, REAL_ *q2 )
  {
      INT_  i,j,h,k;
      REAL_ p=1./(n-1);
      REAL_ r,s,t,u,v,w,z,y;
      s= 0;
      for( k=0;k<n;k++ )
     {
         u= 1;
         z= 0;
         y= 0;

         r= 1;
         t= 0;
 
         for( j=0;j<n;j++ )
        {
            if( j != k )
           {
               r*=    (s- t);
               u*= (x-j*p);
               v= 1;
               for( i=0;i<n;i++ )
              {
                  if( i != k && i != j )
                 {
                     v*= (x-i*p);
                     w= 1; 
                     for( h=0;h<n;h++ ) 
                    { 
                        if( h != k && h != j && h!= i) 
                       { 
                           w*= (x-h*p); 
                       } 
                    } 
                     y+= w; 
                 }
              }
               z+= v;
           }
            t+= p;
        }

         q[k]=    u;
         q1[k]=   z;
         q2[k]=   y;

         r= 1./r;

         q[k]*=  r;
         q1[k]*= r;
         q2[k]*= r;

         s+= p;
     }
  };

#  endif
