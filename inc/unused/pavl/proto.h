#   ifndef _PAVL_
#   define _PAVL_

#   include <cassert>
#   include <cstdio>
#   include <typd.h>
#   include <utils/proto.h>
#   include <misc.h>

/**@ingroup sorting
  *@{
 **/

/**@defgroup avltree AVL Trees. 
  *@{
 **/

/** A node in a AVL tree. **/
    struct pavl_t
   {
       INT_            b;                /**< Balance factor. **/
       INT_            ic;               /**< Position with respect to parent: prn->ch[var->ic] points to var. **/
       INT_            val;              /**< Value **/
       INT_            ch[2];            /**< Left and right children (ch[0] and ch[1] respectively). **/
       INT_           prn;               /**< Parent. **/
   };

#   define   PAVL_TAB  "     "
#   define   PAVL_DLEN 100

/** AVL Tree class. AVL Trees are balanced binary search trees. During insertion and removal the tree is modified 
    to guarantee that the height of the sub-trees attached to each node differ by no more than one unit.
    The modifications are achieved by local permutation of the tree nodes, called rotation. Similar operations
    are performed during removal to keep the tree balanced. See D. Knuth, The Art of Computer Programming, 
    Volume 3, Sorting and Searching, Second Edition (1997), Addison Wesley, pp. 458-475.**/ 
    template <typename type> class pavl_c
   {
       protected:

          INT_             n;             /**< Number of nodes in the tree +1. **/
          INT_            nk;             /**< Number of keys  in the tree  **/
          INT_             m;             /**< Size of the storage for nodes in the tree . **/
          INT_            mk;             /**< Size of the storage for the keys  in the tree  **/
          INT_         *indx;             /**< Node index. For a given key k, indx[k] is the position of the
                                               corresponding node in buf.**/
          pavl_t        *buf;             /**< Node storage.**/

/** Performs the balancing operations corresponding to a node removal. The balancing start at node "node" and
    assumes that its ia child has been removed. (ia=1 means removal on the right, ia=-1 means removal on the left)
   @param                        node                    node to be rebalanced.
   @param                        ia                      index of the subtree where a change in height occurred.
 **/
          void rbalance( INT_ node, INT_ ia )
# include <pavl/protected/rbalance>
;

/** Destroy the node "node" and rearrange the storage accordingly.
   @param                        node                    node to be removed.
 **/
          void destroy( INT_ node )
# include <pavl/protected/destroy>
;

       public:        
          pavl_c()
         {
             n= 1;
             m= PAVL_DLEN;
             nk=0;
             mk= PAVL_DLEN;
             buf= new pavl_t[m];
             indx=new INT_[mk];
             setv( 0,mk, -1, indx );
             for( INT_ i=0;i<m;i++ )
            { 
                buf[i].ic   =   -1;
                buf[i].ch[0]=   -1;
                buf[i].ch[1]=   -1;
                buf[i].prn  =   -1;
                buf[i].val=     -1;
                buf[i].b=        0;
            }
         };

         ~pavl_c()
         {
             delete[] indx;
             delete[] buf;
             n=0;
             m=0;
             nk=0;
             mk=0;
         };

/**  Look up the key in in the tree and return the address of the corresponding node. If the key does not exist,
     creates a new node and inserts it in the tree, and perform balancing operations.
    @param                                  i     key label.
    @param                                  data  key storage
  **/
          bool insert( INT_ ikey, type *data )
# include <pavl/public/insert>
;

/**  Traverse the sub-tree starting at node "node" and prints it on standard output. If "node" is NULL on entry the 
     whole  tree is printed.
    @param                                  tab               indentation tab
    @param                                  node              node
  **/
          void  traverse( INT_ lev, INT_ node, type *data )
# include <pavl/public/traverse>
;

/**  Remove the tree node "node" and balances the tree. The node may be found by insert.
    @param                                  node     node to be removed.
  **/
          void  remove( INT_ ikey )
# include <pavl/public/remove>
;

/** Return the address of the smallest entry in the data >= the given key
   @param                                   node     node
 **/
          INT_         minsup( INT_ ikey )
# include <pavl/public/minsup>
;

/** Return the address of the largest entry in the data <= the given key
   @param                                   node     node
 **/
          INT_         maxinf( INT_ ikey )
# include <pavl/public/maxinf>
;

/** Notify the tree of a change in the name of a key.
   @param                                   ikey     old key identifier
   @param                                   jkey     new key identifier
 **/
          void         rename( INT_ ikey, INT_ jkey )
# include <pavl/public/rename>
;

   };

/**
  *@}
 **/

/**@
  *@}
 **/

#   endif
