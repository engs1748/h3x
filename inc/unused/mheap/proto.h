#  ifndef _M_HEAP_
#  define _M_HEAP_

#  include <typed.h>
#  include <utils/proto.h>

/**@ingroup sorting
  *@{
 **/

/**@defgroup miniheap Mini-heap
  *@{
 **/

/** Changes in size for heap storage reallocation
 **/
#   define MHEAP_DLEN 100

/** Mini-heap class. This class implements a min heap: extraction returns the element associated to the
    smallest key.
 **/

   template <typename type> class mheap_c
  {
      protected:
         INT_                      n;                          /**< Number of entries currently in the heap **/
         INT_                      m;                          /**< Physical size of the storage for the heap **/
         INT_                   *idx;                          /**< Heap index **/

      public:
         mheap_c()
        {
            n=0;
            m=MHEAP_DLEN;
            idx= new INT_[m];
            setv( 0,m, -1, idx );
        }

        ~mheap_c()
        {
            delete[] idx; idx= NULL;
            n=0;
            m=0;
        }

/** Insert a new entry in the heap. The new entry is inserted in the first available position at the bottom of the
    heap and then sifted up the heap to its correct position 
   @param                       k                   Key
   @param                       data                Data store
 **/
         void insert( INT_ k, type *data )
        {
            INT_ i,j;

            if( n == m )
           {
               realloc( &m,MHEAP_DLEN,&idx );
           }
            i=n++;
            idx[i]= k;
// sift new entry up
            while( i >0 )
           {
               j= i-1;
               j/= 2;
               if( data[idx[i]] < data[idx[j]] ) // if entry idx[i] is smaller than its parent then swap
              {  
                  swap( idx+j,idx+i ); 
              }
               else
              {
                  break;                         // the heap property is restored, stop.
              }
               i= j;
           }
        }

/** Extract the root of the heap. The root is replaced with the last entry, which is then sifted down the heap
    until it finds its correct position
   @param                       data                Data store
   @return                      They key corresponding to the heap root.
 **/
         INT_ extract( type *data )
        {
            INT_ i,j;
            INT_ val=-1;

            if( n > 0 )
           {
               i=0;
               n--;
               val= idx[i];
               idx[i]= idx[n];
               idx[n]= -1;
              
               if( n > 1 )
              {
// sift new root down
                  j=1;
                  do
                 {
                     if( j+1 < n )                    // find the smaller child
                    { 
                        if( data[idx[j]] > data[idx[j+1]] ){ j++; }; 
                    }
                     if( data[idx[i]] > data[idx[j]] )// if entry idx[i] is larger than its smaller child then swap
                    {  
                        swap( idx+j,idx+i ); 
                    }
                     else
                    {
                        break;                        // the heap property is restored, stop.
                    }
                     i= j;
                     j+= (j+1);
                 }while( j < n );
              }
           }
            return val;
        }
  };

/**
  *@}
 **/
/**
  *@}
 **/
#  endif
