#  ifndef _MSH_
#  define _MSH_

#  include <cstdlib>
#  include <cstdio>
#  include <cstring>
#  include <cassert>

#  include <typd.h>
#  include <vsize.h>
#  include <parse.h>

#  include <hmesh/hmesh.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Sat 18 Aug 11:08:35 BST 2018
// Changes History -
// Next Change(s)  -

   struct fref_t
  {
      INT_          n;
      INT_          f[6];
      bool          o[6];

      fref_t(){ n=0; f[0]=-1; o[0]=false;
                     f[1]=-1; o[1]=false;
                     f[2]=-1; o[2]=false;
                     f[3]=-1; o[3]=false;
                     f[4]=-1; o[4]=false;
                     f[5]=-1; o[5]=false; };

  };
   const fref_t zdum;

/** A data structure to hold msh grid data **/

   struct msh_t
  {
      public:

         wsize_t<REAL_,3,16>          xx;            /** Vertex coordinates **/

         wsize_t<INT_,2,16>           lf;            /** Face groups
                                                         0: last face in group
                                                         1: group (-1 for internal faces)
                                                      **/
         wsize_t<INT_,8,16>           fx;            /** Faces
                                                         0-3: vertices
                                                         4,5: hex
                                                         6,7: faces on hex
                                                      **/

         wsize_t<INT_,8,16>           hx;            /** Hex vertices 
                                                         0-7: vertices
                                                      **/
         vsize_t<fref_t,16>           hf;            /** Hex/face connectivity
                                                         0-5: faces on hex
                                                      **/

         void read( INT_ m, buf_t *buf );            /** Read from parsed file **/
         void hexs();                                /** Make hex **/

         void build( hmesh_t &var);                  /** Make hex mesh **/

      private:

         void readdm( INT_ m, buf_t *buf, INT_ &j ); /** Read dimensions **/
         void readvt( INT_ m, buf_t *buf, INT_ &j ); /** Read vertices **/
         void readhx( INT_ m, buf_t *buf, INT_ &j ); /** Read hex **/
         void readfc( INT_ m, buf_t *buf, INT_ &j ); /** Read faces **/
         void readbn( INT_ m, buf_t *buf, INT_ &j ); /** Read boundary labels **/

  };
  
#  endif

