#  ifndef _TRI_
#  define _TRI_

#  include <typd.h>
#  include <cmath>
#  include <cstdio>
#  include <cstdlib>
#  include <cstring>
#  include <cassert>
#  include <vsize.h>

#  define DLEN_T 8

   inline REAL_ angle( REAL_3 x, REAL_3 y, REAL_3 z )
  {
       REAL_3  u,v;
       REAL_   a,b,c;

       u[0]= y[0]-x[0];
       u[1]= y[1]-x[1];
       u[2]= y[2]-x[2];

       v[0]= z[0]-y[0];
       v[1]= z[1]-y[1];
       v[2]= z[2]-y[2];

       a=  u[0]*u[0];
       a+= u[1]*u[1];
       a+= u[2]*u[2];

       b=  u[0]*v[0];
       b+= u[1]*v[1];
       b+= u[2]*v[2];

       c=  v[0]*v[0];
       c+= v[1]*v[1];
       c+= v[2]*v[2];

       
       a*= c;
       a= sqrt(a);

       b/= a;

       return b;
       
  }

   inline void vex( REAL_3 x, REAL_3 y, REAL_3 z, REAL_3 w )
  {
       REAL_3  u,v;
       REAL_   a,c;

       u[0]= y[0]-x[0];
       u[1]= y[1]-x[1];
       u[2]= y[2]-x[2];

       v[0]= z[0]-y[0];
       v[1]= z[1]-y[1];
       v[2]= z[2]-y[2];

       a=  u[0]*u[0];
       a+= u[1]*u[1];
       a+= u[2]*u[2];

       c=  v[0]*v[0];
       c+= v[1]*v[1];
       c+= v[2]*v[2];

       w[0]= u[1]*v[2]- u[2]*v[1];
       w[1]= u[2]*v[0]- u[0]*v[2];
       w[2]= u[0]*v[1]- u[1]*v[0];
       
       a*= c;
       a= sqrt(a);
       a= 1./a;

       w[0]*=a;
       w[1]*=a;
       w[2]*=a;

       return;

  }
/** neig_t lists the nodes connected to one particular node by edges.
    Additionally it lists the triangles sharing those edges and the corresponding
    local edge. **/

   struct neig_t
  {
      INT_         v;
      INT_2        t;
      INT_2        j;

      neig_t()
     {
         v=-1;
         t[0]=-1;
         t[1]=-1;
         j[0]=-1;
         j[1]=-1;
     };

  };

   typedef vsize_t<neig_t,DLEN_T> vneig_t;

   struct vt_t
  {
      INT_          t;
      INT_          j;
 
      vt_t()
     {
         t=-1;  
         j=-1;  
     };
  };

   struct co_t
  {
      INT_         v;
      INT_         c;
      REAL_3       w;
      REAL_        s;

  };

   typedef vsize_t<co_t,DLEN_T> vco_t;

   struct tr_t
  {
      INT_3         v;
      INT_3         t;
      INT_3         j;

      tr_t()
     {
         v[0]=-1;
         v[1]=-1;
         v[2]=-1;

         t[0]=-1;
         t[1]=-1;
         t[2]=-1;

         j[0]=-1;
         j[1]=-1;
         j[2]=-1;
     };

  };

   struct tri_t
  {
      INT_                   n;
      INT_                   m;

      vt_t                  *v;
      tr_t                  *t;

      REAL_3                *x;
      REAL_2                *y;

      vsize_t<INT_,DLEN_T>   b;
      wsize_t<INT_,2,DLEN_T> l;

      vco_t                  c; 

      tri_t()
     {

         n=0;
         m=0;

         v=NULL;
         t=NULL;
         x=NULL;
         y=NULL;

     };

     ~tri_t()
     {
         n=0;
         m=0;
         free(v); v=NULL;
         free(t); t=NULL;
         free(x); x=NULL;
         free(y); y=NULL;

     };

      void  read( FILE *f );
      void check( FILE *f );

      void  neig( vneig_t *wrk );
      void  edge( vneig_t *wrk );

      void  corners();
      void   corner( INT_ p, INT_ q, INT_ r, INT_ s, INT_ t );

      void fan( INT_ p, INT_ o, vsize_t<INT_,DLEN_T> &list );
      void print( INT_ k, FILE *f );
  };


#  endif
