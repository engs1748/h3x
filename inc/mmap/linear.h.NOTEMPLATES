#  ifndef _LINEAR_
#  define _LINEAR_

   struct linear_t 
  {

      private:

         INT_             l;

         size_t           off;
         size_t           last;
         INT_             m;
         INT_             v;
         INT_             n[NDIM];
         INT_            n0[NDIM];
         INT_           n00;

         bool          verb;

      public:

         MPI_Datatype    *rtyp;
         MPI_Datatype    *styp;

         linear_t();
        ~linear_t();

         void cleanup( struct par_t &par );


         template<typename T> void memset( T *x, int val )
        {
            size_t len= size(); 
          ::memset( x,val,len*sizeof(T) );
        };

         template<typename T> void malloc( T **x )
        {
            size_t len= size(); 
           *x= new T[len]; 
            memset( *x,-1 );
        };

         template<typename T> void memcpy( T *src, T *dst )
        {
            size_t len= size()*sizeof(T); 
          ::memcpy( (void*)dst,(void*)src, len );
        };

         void dims(                                                      size_t &len, ... );
         void halo( linear_t &var, struct par_t &par, struct com_t &com, size_t &len, ... );

         void  fread( FILE *f );
         void fwrite( FILE *f );

         inline size_t   len(){ return off; };
         inline INT_  blocks(){ return n[l-1]; };

         inline INT_    idim(){ return n[0]; };
         inline INT_    jdim(){ return n[2]; };
         inline INT_    kdim(){ return n[3]; };

         void verbose(){ verb=!verb; }

         inline size_t lsize( INT_ p )
        { 
            assert( p <= l );
            size_t val;
            val= n[0];
            val*=v;
            for( INT_ i=2;i<p;i++ ){ val*= n[i]; };
            return val;
        };

         inline size_t bsize( INT_ p )
        { 
            assert( p <= l );
            size_t val;
            val= m;
            val*=v;
            for( INT_ i=2;i<p;i++ ){ val*= n[i]; };
            return val;
        };

         inline size_t size(){ return last; };

         inline size_t addr( INT_ i1, INT_ i0 )
        {
            size_t val;
            assert( l == 2 );

            val= i1;
            val*=m;
            val+=i0- n00;
            val+= off;
            return val;
        };

         inline size_t addr( INT_ i1, INT_ i0, INT_ i2 )
        {

            size_t val;
            assert( l == 3 );

            val=  i2-n0[2];
            val*=v;
            val+= i1;
            val*=m;
            val+=i0- n00;
            val+= off;
            return val;
        };

         inline size_t addr( INT_ i1, INT_ i0, INT_ i2, INT_ i3 )
        {

            size_t val;
            assert( l == 4 );

            val=  i3- n0[3];
            val*=n[2];
            val+= i2- n0[2];
            val*=v;
            val+= i1;
            val*=m;
            val+=i0- n00;

            val+=off;
            return val;
        };

         inline size_t addr( INT_ i1, INT_ i0, INT_ i2, INT_ i3, INT_ i4 )
        {

            size_t val;
            assert( l == 5 );

            val=  i4- n0[4];
            val*=n[3];
            val+= i3- n0[3];
            val*=n[2];
            val+= i2- n0[2];
            val*=v;
            val+= i1;
            val*=m;
            val+=i0- n00;

            val+=off;
            return val;
        };

         inline size_t addr( INT_ i1, INT_ i0, INT_ i2, INT_ i3, INT_ i4, INT_ i5 )
        {

            size_t val;
            assert( l == 6 );

            val=  i5- n0[5];
            val*=n[4];
            val+= i4- n0[4];
            val*=n[3];
            val+= i3- n0[3];
            val*=n[2];
            val+= i2- n0[2];
            val*=v;
            val+= i1;
            val*=m;
            val+=i0- n00;

            val+=off;
            return val;
        };

         inline size_t addr( INT_ i1, INT_ i0, INT_ i2, INT_ i3, INT_ i4, INT_ i5, INT_ i6  )
        {

            size_t val;
            assert( l == 7 );

            val=  i6- n0[6];
            val*=n[5];
            val+= i5- n0[5];
            val*=n[4];
            val+= i4- n0[4];
            val*=n[3];
            val+= i3- n0[3];
            val*=n[2];
            val+= i2- n0[2];
            val*=v;
            val+= i1;
            val*=m;
            val+=i0- n00;

            val+=off;
            return val;
        };

         inline void loadv( INT_ i0, INT_ i1, INT_ i2, INT_ i3, REAL_ *u, REAL_ *w )
        {
//place values of u at coordinates i* into buffer w
//i0:intent(in) : i coordinate
//i1:intent(in) : j coordinate
//i2:intent(in) : k coordinate
//i3:intent(in) : block coordinate
//u :intent(in) : global array of values (spatial coordinates?)
//w :intent(out): buffer of values at specified index to return

            size_t i= addr( 0,i0,i1,i2,i3 );
 
            for( INT_ j=0;j<v;j++ )
           {
               w[j]= u[i]; 
               i+=   m;
           }
        }

         inline void storev( INT_ i0, INT_ i1, INT_ i2, INT_ i3, REAL_ *u, REAL_ *w )
        {
            size_t i= addr( 0,i0,i1,i2,i3 );
 
            for( INT_ j=0;j<v;j++ )
           {
               u[i]= w[j];
               i+=   m;
           }
        }

         inline void addv( INT_ i0, INT_ i1, INT_ i2, INT_ i3, REAL_ *u, REAL_ *w )
        {
            size_t i= addr( 0,i0,i1,i2,i3 );
 
            for( INT_ j=0;j<v;j++ )
           {
               u[i]+= w[j];
               i+=   m;
           }
        }

         inline void addv( INT_ i0, INT_ i1, INT_ i2, REAL_ *u, REAL_ *w )
        {
            size_t i= addr( 0,i0,i1,i2 );
 
            for( INT_ j=0;j<v;j++ )
           {
               u[i]+= w[j];
               i+=   m;
           }
        }

         inline void fmav( INT_ i0, INT_ i1, INT_ i2, INT_ i3, REAL_ *u, REAL_ f, REAL_ *w )
        {
            size_t i= addr( 0,i0,i1,i2,i3 );
 
            for( INT_ j=0;j<v;j++ )
           {
               u[i]+= f*w[j];
               i+=   m;
           }
        }

         inline void subv( INT_ i0, INT_ i1, INT_ i2, INT_ i3, REAL_ *u, REAL_ *w )
        {
            size_t i= addr( 0,i0,i1,i2,i3 );
 
            for( INT_ j=0;j<v;j++ )
           {
               u[i]-= w[j];
               i+=   m;
           }
        }

         inline void loadv( INT_ i0, INT_ i1, INT_ i2, REAL_ *u, REAL_ *w )
        {
            size_t i= addr( 0,i0,i1,i2 );
 
            for( INT_ j=0;j<v;j++ )
           {
               w[j]= u[i]; 
               i+=   m;
           }
        }

         inline void loadv( INT_ i0, INT_ i1,  REAL_ *u, REAL_ *w )
        {
            size_t i= addr( 0,i0,i1 );
 
            for( INT_ j=0;j<v;j++ )
           {
               w[j]= u[i]; 
               i+=   m;
           }
        }

         inline void storev( INT_ i0, INT_ i1, INT_ i2, REAL_ *u, REAL_ *w )
        {
            size_t i= addr( 0,i0,i1,i2 );
 
            for( INT_ j=0;j<v;j++ )
           {
               u[i]= w[j];
               i+=   m;
           }
        }

         void sxpy( REAL_ *x, REAL_ a, REAL_ *y, REAL_ b, REAL_ *z );

  };

#  endif
