#  ifndef _MMAP_
#  define _MMAP_

#  define NDIM 8

#  include <mpi.h>
#  include <omp.h>
#  include <typd.h>
#  include <cassert>
#  include <cstdarg>
#  include <cstring>
#  include <typd.h>
#  include <misc.h>

#  include <mmap/linear.h>

#  define MMAP_  linear_t

#  endif
