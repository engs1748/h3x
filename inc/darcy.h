#  ifndef _DARCY_
#  define _DARCY_

#  include <cstdlib>
#  include <cstdio>
#  include <typd.h>

   struct darcy_t
  {
      REAL_       phi;
      REAL_       kd,kf;

      darcy_t(){ phi=1;kd=0;kf=0; };

      void fread( FILE *f )
     {
         REAL_ w;
         fscanf( f, "%lf %lf %lf",   &phi, &kd, &kf );

         w= phi;

         kd*= w;
         w*= phi;
         kf*= phi;
         
         printf( "% 9.3e % 9.3e % 9.3e\n", phi,kd,kf );
         return;
     };
  };

#  endif
