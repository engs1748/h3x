#  ifndef _LIMITERS_
#  define _LIMITERS_

#  define _ALPHA_ 0.05/16.

#  define  LIMITER kore1
#  define SLIMITER nolim
#  define VLIMITER nolim

   inline REAL_ nolim( REAL_ x, REAL_ y, REAL_ dum )
  {
      return (2*x+ y)/3;
  }

   inline REAL_ valbada( REAL_ r1, REAL_ r2, REAL_ dum )
  {
      REAL_ val;
      val= r1*r2;
      REAL_ eps= 1.e-32;
      if( val <= 0 )
     {
         val= 0;
     }
      else
     {
         val= ( r2*(r1*r1)+r1*(r2*r2) )/( r1*r1+ r2*r2+ eps );
     }
      return(val);
  };

   inline REAL_ minmod( REAL_ x, REAL_ y, REAL_ dum )
  {
      REAL_        z;

      if( x > 0.0 )
     {
        z= fmin( x,fmax(y,0.0) );
     }
      else
     {
        z= fmax( x,fmin(y,0.0) );
     }

      return z;

  };

   inline REAL_ vanleer( REAL_ a, REAL_ b )
  {
      return (b+fabs(b))/(a+fabs(b));
  }



   inline REAL_ cada( REAL_ x, REAL_ y, REAL_ t )
  {
      REAL_ gam= 1.5;
      const REAL_ c1= -2./7.;
      const REAL_ c2=  0.4;
      const REAL_ c3=  3*gam-2;
      REAL_ u,v,w,p,q;
 
      if( x > 0 )
     {
         u= x;
         v= y;
     }
      else
     {
         u=-x;
         v=-y;
     }
      p= (v+ 2.*u)/3.;
      q= fabs(x)+ fabs(y); 
      if( q > t )
     {

         if( v < -2*u )
        {
            w=0;
        }
         else
        {
            if( v < c1*u )
           {
              w= (v+2.*u)/3.;
           }
            else
           {
               if( v < 0 )
              {
                  w= -2.*v;
              }
               else
              {
                  if( v < c2*u )
                 {
                      w= 2.*v;
                 }
                  else
                 {
                     if( v < c3*u )
                    {
                        w= (v+2.*u)/3.;
                    }
                     else
                    {
                         w=gam*u;
                    }
                 }
              }
           }
        }
     }
      else
     {
         w= p;
     }
      if( x <= 0 ){ w= -w; };

      return w;
  }


   inline REAL_ cada1( REAL_ y, REAL_ x, REAL_ u )
  {
      REAL_              lim;
      REAL_              v,s;
      REAL_              r1,r2,r3,r4,r5;

      v= fabs(x)+ fabs(y);

      lim= 2*y+x;
      lim= lim/3.0;

      if( v > u )
     {
         s= 1;
         if( y < 0 ){ s=-s;}

          r1=   s*  lim;
          r2=  -s*x;
          r3= 2*s*x;
          r4=   r1;
          r5=1.5*fabs(y);
 
          lim= r5;

          if( r4 < lim ){ lim= r4; }
          if( r3 < lim ){ lim= r3; }
          if( r2 > lim ){ lim= r2; }
          if( r1 < lim ){ lim= r1; }

          if( 0.0 > lim ){ lim= 0.0; }

          lim= s*lim;

     }
      return lim;
  }

   inline REAL_ koren( REAL_ y, REAL_ x, REAL_ d )
  {
      REAL_              lim;
      REAL_              u,v,s;
      REAL_              a,b;

      a= 2.0;
      b= 1.1;

      u= y;
      v= x;

      s= 1;
      if( u < 0 )
     {
         s=-1;
         u= -u;
         v= -v;
     }
      lim= fmin( (u+2*v)/3.,a*u );
      lim= fmin( lim       ,b*v );
      lim= fmax( lim       ,0.   );

      lim= lim*s;
      
      return lim;
  }

   inline REAL_ kore1( REAL_ y, REAL_ x, REAL_ d )
  {
      REAL_              lim;
      REAL_              u,v,s;
      REAL_              a,b;

      a= 2.0;
      b= 1.1;

      lim= (y+2*x)/3.;

      if( fabs(x)+ fabs(y) > d )
     {

         u= y;
         v= x;
      
         s= 1;
         if( u < 0 )
        {
            s=-1;
            u= -u;
            v= -v;
        }
         lim= fmin( (u+2*v)/3.,a*u );
         lim= fmin( lim       ,b*v );
         lim= fmax( lim       ,0.   );
      
         lim= lim*s;
     }
      
      return lim;
  }

   inline REAL_ cada2( REAL_ y, REAL_ x, REAL_ u )
  {
      REAL_              lim;
      REAL_              v,s;
      REAL_              r1,r2,r3,r4,r5;

      v= fabs(x)+ fabs(y);

      lim= 2*y+x;
      lim= lim/3.0;

      if( v > u )
     {
         s= 1;
         if( y < 0 ){ s=-s;}

         r1=   s*  lim;
         r2=  -s*x;
         r3= 2*s*x;
         r4=   r1;
         r5=1.5*fabs(y);
 
         lim= r5;

         if( r4 < lim ){ lim= r4; }
         if( r3 < lim ){ lim= r3; }
         if( r2 > lim ){ lim= r2; }
         if( r1 < lim ){ lim= r1; }

         if( 0.0 > lim ){ lim= 0.0; }
         if( x*y < 0 ){ lim= 0.0; }

         lim= s*lim;

     }
      return lim;
  }

   inline void limdiff( REAL_ u, REAL_ v, REAL_ w,  REAL_ &x, REAL_ &y )
  {
      REAL_               b;

      b= _ALPHA_;

      x= LIMITER( v,u, b );
      y= LIMITER( v,w, b );

      return;
  }
   inline void slimdiff( REAL_ u, REAL_ v, REAL_ w,  REAL_ &x, REAL_ &y )
  {
      REAL_               b;

      b= _ALPHA_;

      x= SLIMITER( v,u, b );
      y= SLIMITER( v,w, b );

      return;
  }
   inline void vlimdiff( REAL_ u, REAL_ v, REAL_ w,  REAL_ &x, REAL_ &y )
  {
      REAL_               b;

      b= _ALPHA_;

      x= VLIMITER( v,u, b );
      y= VLIMITER( v,w, b );

      return;
  }

#  endif
