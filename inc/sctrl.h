#ifndef _SZECTR_
#define _SZECTR_

#  include <vsize.h>

   struct sctrl_t
  {
      REAL_             d;
      vsize_t<INT_,128> b;

      sctrl_t()
     {
         d=-1;
     }

      void fwrite( FILE *f )
     {
       ::fwrite( &d,        1,sizeof(REAL_),f );
       ::fwrite( &(b.n),    1,sizeof(INT_ ),f );
         if( b.n > 0 )
        {
          ::fwrite( b.data(),b.n,sizeof(INT_ ),f );
        }
         return;
     }
      void fread( FILE *f )
     {
         INT_ n;
       ::fread( &d,        1,sizeof(REAL_),f );
       ::fread( &n,        1,sizeof(INT_ ),f );
         if( n   > 0 )
        {
            b.n= n; b.resize(-1);
          ::fread( b.data(),b.n,sizeof(INT_ ),f );
        }
         return;
     }
  };

#endif
