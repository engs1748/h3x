#  ifndef _EXAMPLE_
#  define _EXAMPLE_

#  include <q3x/q3x.h>

   struct example_t: public q3x_t
  {
      void        tryq( double *x );
      void        trye( double *x );
      void        tryp( double *x );
      void        tryb( double *x );
      void        loops( REAL_ *wm, REAL_ *wf, REAL_ *wb, REAL_ *wc, REAL_ *v, REAL_ *r );

  };

#  endif
