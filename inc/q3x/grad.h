#  ifndef _GRAD_
#  define _GRAD_

   inline void wlsm( REAL_ *x0, REAL_ *x1, REAL_ *d )
  {
      REAL_ s[3];
      
      s[0]= x1[0]- x0[0];
      s[1]= x1[1]- x0[1];
      s[2]= x1[2]- x0[2];

      d[0]= s[0]*s[0];
      d[1]= s[1]*s[0];
      d[2]= s[2]*s[0];
      d[3]= s[1]*s[1];
      d[4]= s[2]*s[1];
      d[5]= s[2]*s[2];
  };

   inline void wlsv( INT_ n, REAL_ *x0, REAL_ *x1, REAL_ *v0, REAL_ *v1, REAL_ *d )
  {
      INT_  i,j;
      REAL_ s[3];
      REAL_ t;

      s[0]= x1[0]- x0[0];
      s[1]= x1[1]- x0[1];
      s[2]= x1[2]- x0[2];

      j=0;
      for( i=0;i<n;i++ )
     {
         t= v1[i]-v0[i];
         d[0+j]= s[0]*t;
         d[1+j]= s[1]*t;
         d[2+j]= s[2]*t;
         j+=3;
     }
  };


#  endif
