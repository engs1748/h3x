#  ifndef _GRAD_
#  define _GRAD_

#  include <m33.h>

   inline void  bias( INT_ n, REAL_ *x, REAL_ *w, REAL_ *x0, REAL_ *x1, REAL_ *v0, REAL_ *v1, REAL_ *dx0, REAL_ *dx1, REAL_ *dv0, REAL_ *dv1, REAL_ *y, REAL_ *z )
  {
      INT_  i,j;
      REAL_ s[3];
      REAL_ a,b,t,l,e,f;

      REAL_ u[6];
      REAL_ v[6];

      REAL_ p[3];
      REAL_ q[3];

      s[0]= x1[0]- x[0];
      s[1]= x1[1]- x[1];
      s[2]= x1[2]- x[2];
      b= s[0]*w[0]+ s[1]*w[1]+ s[2]*w[2];

      s[0]= x[0]- x0[0];
      s[1]= x[1]- x0[1];
      s[2]= x[2]- x0[2];
      a= s[0]*w[0]+ s[1]*w[1]+ s[2]*w[2];
      assert( a  > 0 );
      assert( b  > 0 );

      s[0]= x1[0]- x0[0];
      s[1]= x1[1]- x0[1];
      s[2]= x1[2]- x0[2];

      l= s[0]*w[0]+ s[1]*w[1]+ s[2]*w[2];
      l=1./l;

      u[0]= dx0[0]- s[0]*s[0];
      u[1]= dx0[1]- s[1]*s[0];
      u[2]= dx0[2]- s[2]*s[0];
      u[3]= dx0[3]- s[1]*s[1];
      u[4]= dx0[4]- s[2]*s[1];
      u[5]= dx0[5]- s[2]*s[2];

      v[0]= dx1[0]- s[0]*s[0];
      v[1]= dx1[1]- s[1]*s[0];
      v[2]= dx1[2]- s[2]*s[0];
      v[3]= dx1[3]- s[1]*s[1];
      v[4]= dx1[4]- s[2]*s[1];
      v[5]= dx1[5]- s[2]*s[2];

      ldl3f( u );
      ldl3f( v );

      j=0;
      for( i=0;i<n;i++ )
     {
         t= v1[i]-v0[i];

         p[0]= dv0[0+j]- s[0]*t;
         p[1]= dv0[1+j]- s[1]*t;
         p[2]= dv0[2+j]- s[2]*t;
         ldl3s( u,p );
         e= p[0]*w[0]+ p[1]*w[1]+ p[2]*w[2];

         q[0]= dv1[0+j]- s[0]*t;
         q[1]= dv1[1+j]- s[1]*t;
         q[2]= dv1[2+j]- s[2]*t;
         ldl3s( v,q );
         f= q[0]*w[0]+ q[1]*w[1]+ q[2]*w[2];

         t*= l;

         e= LIMITER( t,e,0. ); 
         f= LIMITER( t,f,0. ); 
         e*= a;
         f*= b;

         y[i]= v0[i]+ e;
         z[i]= v1[i]- f;

         j+=3;
     }

      return;
  }

   inline void wbias( INT_ n, REAL_ *x, REAL_ *w, REAL_ *x0, REAL_ *x1, REAL_ *dv, REAL_ *dx0, REAL_ *dx1, REAL_ *dv0, REAL_ *dv1, REAL_ *y, REAL_ *z )
  {
      INT_  i,j;
      REAL_ s[3];
      REAL_ a,b,t,l,e,f;

      REAL_ u[6];
      REAL_ v[6];

      REAL_ p[3];
      REAL_ q[3];

      s[0]= x1[0]- x[0];
      s[1]= x1[1]- x[1];
      s[2]= x1[2]- x[2];
      b= s[0]*w[0]+ s[1]*w[1]+ s[2]*w[2];

      s[0]= x[0]- x0[0];
      s[1]= x[1]- x0[1];
      s[2]= x[2]- x0[2];
      a= s[0]*w[0]+ s[1]*w[1]+ s[2]*w[2];
      assert( a  > 0 );
      assert( b  > 0 );

      s[0]= x1[0]- x0[0];
      s[1]= x1[1]- x0[1];
      s[2]= x1[2]- x0[2];

      l= s[0]*w[0]+ s[1]*w[1]+ s[2]*w[2];
      l=1./l;

      u[0]= dx0[0]- s[0]*s[0];
      u[1]= dx0[1]- s[1]*s[0];
      u[2]= dx0[2]- s[2]*s[0];
      u[3]= dx0[3]- s[1]*s[1];
      u[4]= dx0[4]- s[2]*s[1];
      u[5]= dx0[5]- s[2]*s[2];

      v[0]= dx1[0]- s[0]*s[0];
      v[1]= dx1[1]- s[1]*s[0];
      v[2]= dx1[2]- s[2]*s[0];
      v[3]= dx1[3]- s[1]*s[1];
      v[4]= dx1[4]- s[2]*s[1];
      v[5]= dx1[5]- s[2]*s[2];

      ldl3f( u );
      ldl3f( v );

      j=0;
      for( i=0;i<n;i++ )
     {
         t= dv[i];

         p[0]= dv0[0+j]- s[0]*t;
         p[1]= dv0[1+j]- s[1]*t;
         p[2]= dv0[2+j]- s[2]*t;
         ldl3s( u,p );
         e= p[0]*w[0]+ p[1]*w[1]+ p[2]*w[2];

         q[0]= dv1[0+j]- s[0]*t;
         q[1]= dv1[1+j]- s[1]*t;
         q[2]= dv1[2+j]- s[2]*t;
         ldl3s( v,q );
         f= q[0]*w[0]+ q[1]*w[1]+ q[2]*w[2];

         t*= l;

         e= LIMITER( t,e,0. ); 
         f= LIMITER( t,f,0. ); 
         e*= a;
         f*= b;

         y[i]=  e;
         z[i]= -f;

         j+=3;
     }

      return;
  }


#  endif
