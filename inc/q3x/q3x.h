#  ifndef _Q3X_
#  define _Q3X_

#  include <hmesh/hmesh.h>
#  include <gas/gas.h>
#  include <darcy.h>
#  include <par.h>
#  include <utils/proto.h>
#  include <sort/proto.h>
#  include <mtr.h>
#  include <limiters.h>

#  define   FREES   8
#  define   AWALLS 12
#  define   TWALLS 16
#  define   BTYPES 20
#  define   NMSG   32

#  define   MSIZE_ 13
#  define   WSIZE_  4

#  define   NCJG_  128

   struct cnjg_t
  {
      INT_         n;
      REAL_        z[NCJG_];
      REAL_       ft[NCJG_];
      REAL_       ff[NCJG_];

      FILE        *f;

      cnjg_t()
     {
         n=0;
       ::memset(  z,0,128*sizeof(REAL_) );
       ::memset( ft,0,128*sizeof(REAL_) );
       ::memset( ff,0,128*sizeof(REAL_) );

         f=NULL;
     };

      void fread( FILE *f )
     {
         fscanf( f, "%d",   &n );
         assert( n < NCJG_ );
         for( INT_ i=0;i<n;i++ )
        {
            fscanf( f, "%lf %lf %lf",   z+i, ff+i,ft+i );
        }
     };

      void memset( REAL_ *v, REAL_ a )
     {
       ::memset( v,a,NCJG_*sizeof(REAL_) );
     };

      void sxpy( REAL_ *v, REAL_ a, REAL_ *x, REAL_ b, REAL_ *y )
     {
         for( INT_ i=0;i<n;i++ )
        {
            v[i]= a*x[i]+ b*y[i];
        }
     };
 
  };

   struct fbnd_t 
  {
      INT_2       l[BTYPES];
      INT_       *q;
      INT_       *h;
      INT_       *b;
      INT_       *g;
      INT_       *n;
      INT_       *s;
      INT_       *p;
      INT_       *r;
      INT_       *i;
      frme_t     *f;

      fbnd_t() 
     {
         q= NULL;
         h= NULL;
         f= NULL;
         g= NULL;
         b= NULL;
         n= NULL;
         s= NULL;
         p= NULL;
         r= NULL;
         i= NULL;
     };

     ~fbnd_t() 
     {
        delete[]  q; q= NULL;
        delete[]  h; h= NULL;
        delete[]  f; f= NULL;
        delete[]  g; g= NULL;
        delete[]  b; b= NULL;
        delete[]  n; n= NULL;
        delete[]  s; s= NULL;
        delete[]  p; p= NULL;
        delete[]  r; r= NULL;
        delete[]  i; i= NULL;
     };
  };

   struct ctrl_t
  {
      INT_        nt;       // Number of time steps
      INT_        no;       // Residual Output frequency
      INT_        ns;       // Solution output frequency
      REAL_       dt;       // time step
      REAL_    sigma;       // CFL

      ctrl_t()
     {
         nt=0;
         no=0;
         ns=0;
         dt=0;
         sigma=0;
     };

      void fread( FILE *f )
     {
         fscanf( f, "%d",   &nt );
         fscanf( f, "%d",   &no );
         fscanf( f, "%lf",  &dt );
         fscanf( f, "%lf",  &sigma );
         fscanf( f, "%d",   &ns );
     };
  };

   struct q3x_t: public hmesh_t, par_t
  {

      ctrl_t      ctr;
      cnjg_t      cjg;

      INT_        bsize;

      MMAP_       so[NCOM]; // solution arrays
      MMAP_       co[NCOM]; // coordinates arrays


      MMAP_       cb;       // boundary coordinates 
      MMAP_       ci;       // inner boundary coordinates
                            //(? only needed for elliptic smoother)

      MMAP_       sb;       // boundary solution

      MMAP_       bw;       // boundary metrics
      MMAP_       cw;       // connection metrics
      MMAP_       mw[NCOM]; // volume metrics

      MMAP_       ld;       // left hand side diagonal
      MMAP_       cd[NCOM]; // left hand side diagonal (for elliptic smoothing)
      MMAP_       ca[NCOM]; // left hand side diagonal (for orthogonal smoothing)

      MMAP_       xd[NCOM]; // metric tensor for least square gradients
      MMAP_       sd[NCOM]; // least square gradients

      MMAP_       te[NCOM]; // Temperatures for conjugate solutions
      MMAP_       td[NCOM]; // Diagonal for conjugate solutions

      MMAP_       cj;       // temperature maps for conjugate heat transfer

      msg_t       msg[NMSG];   // messages

      fbnd_t      bn;

      GAS_        gas;

      INT_4      *be;
      INT_8      *bj;

      INT_       *lv;
      INT_2      *bv;

      darcy_t     da[32];

      bool        dbg;

      q3x_t(){ be= NULL; bv= NULL; lv= NULL; dbg= false; };
     ~q3x_t(){ delete[] be; be= NULL; delete[] bv; bv=NULL; delete[] lv; lv= NULL; };


// utilities

      void xyz( INT_ l, INT_ m, INT_ n, INT_ d, REAL_ *u, REAL_ *v, REAL_ *w );


// Pre-processing methods

      void        prep1( INT_ n, INT_ *c, INT_ *lc, INT_ *cprm, INT_ *hprm, INT_ *hid, INT_ *lb, INT_ *bprm, INT_ *qprm, 
                         INT_ j, INT_ *wc, INT_ *wh, INT_ *wq, INT_ *we, INT_ *wp );
      void        contq( INT_ j, INT_ n, INT_ *c, INT_ *lc, INT_ *cprm, INT_ *m0,  com_t &com );
      void        conte( INT_ j, INT_ n, INT_ *c, INT_ *lc, INT_ *cprm, INT_ *m0,  com_t &com );
      void        contp( INT_ j, INT_ n, INT_ *c, INT_ *lc, INT_ *cprm, INT_ *m0,  com_t &com );

      void        prepb( INT_ n, INT_ *c, INT_ *lc, INT_ *cprm, INT_ *hprm, INT_ *hid, INT_ *lb, INT_ *bprm, INT_ *qprm, 
                         INT_ j, INT_ *wc, INT_ *wh, INT_ *wq, INT_ *we, INT_ *wp, FILE *o );

      void        neigq( INT_ j, INT_ n, INT_ *c, INT_ *lc, INT_ *cprm, INT_ *m0, INT_ *m2, com_t &com );
      void        neige( INT_ j, INT_ n, INT_ *c, INT_ *lc, INT_ *cprm, INT_ *m0, INT_ *m2, com_t &com );
      void        neigp( INT_ j, INT_ n, INT_ *c, INT_ *lc, INT_ *cprm, INT_ *m0, INT_ *m2, com_t &com );

      void        prepq( INT_ *c, com_t &com );
      void        prepe( INT_ *c, com_t &com );
      void        prepp( INT_ *c, com_t &com );

      void        prep();

// partitioning

      void        part( INT_ n, INT_ m );
      void        scale( REAL_ n );

// run 

      void        start();
      void        stop();
      void        init( INT_ n, REAL_ *u );
      void        grid(      REAL_ *x, REAL_ *y, REAL_ *yi );
      void        grid(      REAL_ *x, REAL_ *y, REAL_ *yi, REAL_ *p );
      void        writegrid( REAL_ *x, REAL_ *y, REAL_ *yi, REAL_ *p );
      void        metrics( REAL_  *x, REAL_ *wm, REAL_ *wb, REAL_ *wc );
      void           grdx( REAL_ *wm, REAL_ *wb,REAL_ *dxdx );
      void           grdv( REAL_ *wm, REAL_ *wb,REAL_ *vb, REAL_ *v, REAL_ *dxdx, REAL_ *dvdx );
      void        binit( REAL_  *wb, REAL_ *vb0 );
      void        cinit( REAL_ *cf, REAL_ *ct );
      void        restart( REAL_  *v, REAL_ *vb, REAL_ *vc );
      void        save( REAL_ *v, REAL_ *vb, REAL_ *vc, INT_ k );
      void        run();
      void        rund();
      void        runrk();
      void        runab();
      void        runt();

      void        rht( REAL_ *x, REAL_ *t,REAL_ *r, REAL_ *d );
      void        rhcj( REAL_ *cf, REAL_ *rb, REAL_ *vc, REAL_  *rc );
      void        upcj( REAL_ *ct, REAL_ *vb, REAL_ *vc0, REAL_ *rc, REAL_ *vc, REAL_ dt );

      void        rhs(            REAL_ *wm, REAL_ *wf, REAL_ *wb, REAL_ *wc, REAL_ *vb0, REAL_ *vb, REAL_ *q,                           REAL_ *rb, REAL_ *r, REAL_ *d );

      void   darcyrhs(            REAL_ *wm, REAL_ *wf, REAL_ *wb, REAL_ *wc, REAL_ *dxdx,  REAL_ *vb0, REAL_ *vb, REAL_ *q,REAL_ *dqdx, REAL_ *rb, REAL_ *r, REAL_ *d );

      void       rhs3( REAL_  *x, REAL_ *wm,            REAL_ *wb, REAL_ *wc, REAL_ *dxdx,  REAL_ *vb0, REAL_ *vb, REAL_ *q,REAL_ *dqdx, REAL_ *rb, REAL_ *r, REAL_ *d );
      void       rhs4( REAL_  *x, REAL_ *wm,            REAL_ *wb, REAL_ *wc, REAL_ *dxdx,  REAL_ *vb0, REAL_ *vb, REAL_ *q,REAL_ *dqdx, REAL_ *rb, REAL_ *r, REAL_ *s, REAL_ *d );

      void        upd( REAL_ *wm, REAL_  *q, REAL_ *r, REAL_ *d, REAL_ sigma, REAL_ *res, bool flag );

      void        upd( REAL_ *wm, REAL_ *wb, REAL_ *bq0, REAL_ *q0, REAL_ *bq, REAL_  *q, REAL_ *br, REAL_ *r, REAL_ dt, REAL_ sigma, REAL_ *res, bool flag );

      void        chk( REAL_ *wm, REAL_ *wb, REAL_ *wc, REAL_ *v, REAL_ *r );

      void        delt( REAL_ *wm, REAL_ *lhd, REAL_ sigma, REAL_ &dt, REAL_ *sgm );
// mesh

      void           trln( INT_ n );
      void        stretch( REAL_ a, REAL_ b, INT_ n );
      void        refn( INT_ n );
      void        updl( INT_ n );

      void         smo( vsurf_t &su );

      void        smo0( REAL_ *y,REAL_ *dy, REAL_ *x, REAL_ *p, REAL_ *r, REAL_ *d, vsurf_t &su, REAL_ fct );
      void        smo1( REAL_ *y,REAL_ *dy, REAL_ *x,           REAL_ *r, REAL_ *d, vsurf_t &su );
      void        smob( REAL_ *y,REAL_ *dy, REAL_ *x,           REAL_ *r, REAL_ *d, vsurf_t &su, REAL_ rlx );
      void        smoe( REAL_ *y,REAL_ *dy, REAL_ *x,           REAL_ *r, REAL_ *d, vsurf_t &su );
      void        smov( REAL_ *y,REAL_ *dy, REAL_ *x,           REAL_ *r, REAL_ *d, vsurf_t &su );

      void        smoi( REAL_ *y,REAL_ *dy, REAL_ *x,           REAL_ *r, REAL_ *d, vsurf_t &su, REAL_ rlx );

      void        attr( REAL_ *x, REAL_ *p, REAL_ dst, REAL_ rlx,REAL_ &res );
      void        attr0( REAL_ *x, REAL_ *p, REAL_ dst, REAL_ rlx,REAL_ &res );
      void        attr1( REAL_ *x, REAL_ *p, REAL_ dst, REAL_ rlx,REAL_ &res );

// Postprocessing

      void        plot3d( REAL_ *x, FILE *f );
      void        tecplot3d( REAL_ *x, REAL_ *u, FILE *f );
      void        tecplot3dx( REAL_ *x, REAL_ *p, FILE *f );
      void        gnuplot3d( REAL_ *x, FILE *f );
      void        vtklegacy_block(    REAL_ *x,           FILE *f );
      void        vtklegacy_grid(     REAL_ *x, REAL_ *p, FILE *f );
      void        vtklegacy_solution( REAL_ *x, REAL_ *v, FILE *f );
      void        vtkxml_block(       REAL_ *x,           FILE *f );
      void        vtkxml_grid(        REAL_ *x, REAL_ *p, FILE *f );
      void        vtkxml_solution(    REAL_ *x, REAL_ *v, FILE *f );

// Visualize

      void        viz();
      void        vix();
      void        wco();
      void        vtk_block(    INT_ format );
      void        vtk_grid(     INT_ format );
      void        vtk_solution( INT_ format );
      void      ascii( INT_ n );

      void inline    vtkwritescalar( FILE *f, MMAP_ &mmap, REAL_ *v, INT_ hex, INT_ indx, INT_ size )
#include <q3x/inline/vtkwritescalar>
      void inline    vtkwritevector( FILE *f, MMAP_ &mmap, REAL_ *v, INT_ hex, INT_  len, INT_ size )
#include <q3x/inline/vtkwritevector>
      void inline    vtkwriteaux(    FILE *f, MMAP_ &mmap, REAL_ *v, INT_ hex, INT_ indx, INT_ size )
#include <q3x/inline/vtkwriteaux>
      void inline    vtkwriteint(    FILE *f,                        INT_ hex, INT_  val, INT_ size )
#include <q3x/inline/vtkwriteint>
      void inline    vtkwritereal(   FILE *f,                        INT_ hex, REAL_ val, INT_ size )
#include <q3x/inline/vtkwritereal>
      void inline    vtkwritecells(  FILE *f,                        INT_ off,            INT_ size, INT_ is_xml )
#include <q3x/inline/vtkwritecells>

// General vector operations

/** x= a*y+ b*z **/
//    void        sxpy( REAL_ *x, REAL_ a, REAL_ *y, REAL_ b, REAL_ *z );
  };

#  endif
