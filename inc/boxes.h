#  ifndef _BOXES_
#  define _BOXES_

#  include <cstdlib>
#  include <cstdio>
#  include <cassert>
#  include <cmath>

#  include <typd.h>
#  include <vsize.h>
#  include <utils/proto.h>

/** A cheap and cheerful data structure for nearest-neighbor computations in two space dimensions**/

   struct box_t
  {

      private:
         INT_2            n;          /** Number of intervals **/
         REAL_2          x0;          /** Lower-left corner **/
         REAL_2          x1;          /** Top-right corner **/

         REAL_2           d;          /** Interval length **/

         vsize_t<INT_,16>    p;       /** Keys, sorted by box **/
         vsize_t<INT_,16>    l;       /** Pointers to the end of each box **/
         vsize_t<INT_,16> indx;       /** Index to keys **/

      public:

/** Constructor **/

         box_t()
        {
            n[0]= 0;  n[1]= 0;
            x0[0]=0; x0[1]= 0;
            x1[0]=0; x1[1]= 0;
            d[0]= 0;  d[1]= 0;
        };

/** Constructor **/

         void build( INT_ a, INT_ b, REAL_2 y0, REAL_2 y1 )
        {
            assert( a>0 );
            assert( b>0 );
   
            assert( y1[0]>y0[0] );
            assert( y1[1]>y0[0] );
   
            n[0]= a;
            n[1]= b;
   
            l.n= n[0]*n[1]; l.resize(0);
   
            x0[0]= y0[0];
            x0[1]= y0[1];
   
            x1[0]= y1[0];
            x1[1]= y1[1];
   
            d[0]= x1[0]-x0[0]; d[0]= ((REAL_)n[0])/(d[0]);
            d[1]= x1[1]-x0[1]; d[1]= ((REAL_)n[1])/(d[1]);
        };

/** Destructor **/
 
        ~box_t()
        {
            n[0]= 0;  n[1]= 0;
            x0[0]=0; x0[1]= 0;
            x1[0]=0; x1[1]= 0;
            d[0]= 0;  d[1]= 0;
        };

/** Destructor **/

         void clear()
        {
            n[0]=0;
            n[1]=0;
            x0[0]=0; x0[1]=0;
            x1[0]=0; x1[1]=0;
            d[0]= 0; d[1]= 0;
            p.clear();
            p.clear();
            l.clear();
            indx.clear();
        }

/** Locate box containing x **/

         INT_ locate( REAL_2 x )
        {
            INT_  i,j,m;
   
            m= 1;
            j= 0;
   
            for( INT_ k=0;k<2;k++ )
           {
               REAL_ v;
   
               assert( (x[k] < x1[k]) && (x[k] > x0[k]) );
   
               v= x[k]-x0[k];
               v*=d[k];
   
               i= (INT_)floor(v);
   
               j+= i*m;
               m*= n[k];
   
           }
   
            return j;
            
        };

/** Insert new entry with key i and position x **/

         void insert( INT_ i, REAL_2 x )
        {
            assert( i > -1 );
            if( i < indx.n ){ if( indx[i] > -1 ){ return; } }
            
            INT_ k=locate( x );
   
            INT_ ist,ien;
           (p.n++); p.resize(-1);
   
            INT_ j= n[0]*n[1];
            ien= l[j-1]++;
   
            for( j=n[0]*n[1]-1;j>k;j-- )
           {
               ist= ien;
               ien= l[j-1]++; 
               p[ist]= p[ien];
           }
   
            p[ien]= i;
   
            indx.n= max( indx.n, i+1 );
            indx.resize(-1);
   
            indx[i]= ien;
            
            return;
        };

/** Remove key i - nice to have but not strictly needed **/

         void remove( INT_ i )
        {
            INT_ j,k,m;
            INT_ ist,ien;
   
            assert( i > 0 );
            assert( i < indx.n );
   
            j= indx[i];
            indx[i]=-1;
   
            assert( j > -1 );
            assert( j < p.n );
   
            k= bsearch(  j, n[0]*n[1], l.data() );
            
            for( m=k;m<n[0]*n[1];m++ )
           {
   
               ist= ien;
               ien= l[k]--; 
               i= p[ien];
               p[ist]= i;
   
               assert( indx[i] );
               indx[i]=ist;
               
           }
        };

/** Nearest neighbour computation **/

         void search( REAL_2 y, REAL_2 *x, INT_ &j, REAL_ &t )
        {
            INT_  i;
            REAL_ s;
            INT_ k;
   
/*          INT_ ist,ien;
            INT_ k=locate( y );*/
/*          ist= 0; if( k > 0 ){ ist= l[k-1]; };
            ien= l[k];*/
   
   
            j=-1;
            t=fabs(x1[0]-x0[0])+ fabs(x1[1]-x0[1]);
   
//          for( k=ist;k<ien;k++ )

            for( k=0;k<l[n[0]*n[1]-1];k++ )
           {
               i= p[k];
               s= fabs( y[0]-x[i][0] )+ fabs( y[1]-x[i][1] );
   
               if( s < t )
              {
                  t= s;
                  j= i;
              }
           }
   
            return;
        };

/** Check **/

         void check( REAL_2 *x )
        {
            INT_ i,j,k,h,m;
            INT_ ist,ien;
   
            REAL_2 y0,y1;
            k= 0;
            ien=0;
            for( j=0;j<n[1];j++ )
           {
               for( i=0;i<n[1];i++ )
              {
                  y0[0]= i; y0[0]/= d[0]; y0[0]+= x0[0];
                  y0[1]= j; y0[1]/= d[1]; y0[1]+= x0[1];
   
                  y1[0]= i+1; y1[0]/= d[0]; y1[0]+= x0[0];
                  y1[1]= j+1; y1[1]/= d[1]; y1[1]+= x0[1];
   
                  printf( "[%9.3f,%9.3f] x [%9.3f,%9.3f]\n", y0[0],y1[0], y0[1],y1[1] );
   
                  ist= ien;
                  ien= l[k];
                  for( h=ist;h<ien;h++ )
                 {
                     m= p[h]; 
   
                     printf( "%9.3f,%9.3f\n", x[m][0],x[m][1] );
                     assert( x[m][0] >= y0[0] && x[m][0] < y1[0] );
                     assert( x[m][1] >= y0[1] && x[m][1] < y1[1] );
                 }
                  k++;
              }
           }
        }

  };

#  endif
