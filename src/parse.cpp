#  include <cstdlib>
#  include <cstdio>
#  include <cassert>
#  include <cstring>

#  include <parse.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Sat 18 Aug 11:15:22 BST 2018
// Changes History
// Next Change(s)  -

/** Read a text file split in items by paris of brackets '()'. Each item becomes a string. **/

   void parse( FILE *f, INT_ &m, vsize_t<char,16> *buf )
  {

      vsize_t<INT_,16> stk;                                                                 /** Item stack **/

      INT_    j,l;

      size_t  n=0;
      char   *line=NULL;                                                                    /** Line read **/

      ssize_t len;
 
      m= 0;

      len= getline( &line,&n, f );
      while( len > 0 )
     {
         for( size_t i=0;i<strlen(line);i++ )
        {
            if( line[i]=='(' )                                                              /** New item starts **/
           { 

               l= stk.n; stk.append(-1);
               stk[l]=m; 
               assert( m < N );
               m++;
           }
            else
           {
               if( line[i]==')' )                                                           /** Item ends **/
              {
                  l= stk.n;
                  j= stk[l-1]; 
                 (stk.n)--; 

                  l= buf[j].n; buf[j].append('\0');                                         /** Close buffer for item just ended **/ 

              }
               else
              {
                  if( line[i] == '\n' )                                                     /** Ignore new line character if reading item **/
                 {
                     if( stk.n > 0 )
                    {
                        j= stk[stk.n-1]; 

                        l= buf[j].n; buf[j].append( '\0' ); buf[j][l]= ' ';
                    }
                 }
                  else                                                                     /** Add character to this item **/
                 {
                     j= stk[stk.n-1]; 
                     l= buf[j].n; buf[j].append( '\0' ); buf[j][l]= line[i];
                 }
              }
           }
        }
         len= getline( &line,&n, f );              /** Read next line **/

     }
      free( line ); line=NULL;
      return;
  }
  
