#  include <mmap/mmap.h>
#  include <par.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   linear_t::linear_t()
  {  
      l=    -1;
      m=     0;
      v=     0;
      off=   0;
      last=  0;
      n00=   0;
    ::memset( n,0,NDIM*sizeof(INT_) ); 
    ::memset(n0,0,NDIM*sizeof(INT_) ); 

      rtyp=  NULL;
      styp=  NULL;

      verb= false;
  };

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   linear_t::~linear_t()
  {  
      l= -1;
      m=  0;
    ::memset( n,0,NDIM*sizeof(size_t) ); 
    ::memset(n0,0,NDIM*sizeof(size_t) ); 

      delete[] rtyp; rtyp= NULL;
      delete[] styp; styp= NULL;

  };

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void linear_t::cleanup( par_t &par )
  {  
      for( INT_ k=1;k<par.neig;k++ )
     {
         if( styp[k] != MPI_DATATYPE_NULL )
        { 
            assert( rtyp[k] != MPI_DATATYPE_NULL );
            
            MPI_Type_free( styp+k );
            MPI_Type_free( rtyp+k );

            styp[k]= MPI_DATATYPE_NULL;
            rtyp[k]= MPI_DATATYPE_NULL;
        }
     }
      delete[] rtyp; rtyp= NULL;
      delete[] styp; styp= NULL;
  };

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void linear_t::dims( size_t &len, ... )
  {
      INT_     i,j,k;
      size_t    dlen;

      assert( n[0] == 0 );

      va_list   arg;
      va_start( arg,len );

// assign subscript ranges. Stop when a -ve value is encountered in the vararg list

      i= 0;
      while( (j=va_arg(arg,INT_))>0 )
     {
         n[i]= j;
         i++;
         assert( i< NDIM );
     }
      m= pad( n[0],VLEN );
      v= n[1];

      assert( n0[1]==0 );
// check for consistency

      if( l == -1 ){ l= i; }
      assert( l==i );

// compute offset for next group

      off= len;

      dlen= m;
      for( k=1;k<l;k++ ){ dlen*= n[k]; }

      len+= dlen;
      last= len;

      return;
  };

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void linear_t::halo( linear_t &var, par_t &par, com_t &com, size_t &len, ... )
  {

      INT_       i,j,k,h, p,q,r,s,t,  x,y,z;
      INT_     *length=NULL;
      INT_       *disp=NULL;
      INT_     ist,ien; 
      INT_     jst,jen; 
      size_t         w;
      size_t      dlen;

      assert( m == 0 );

      va_list   arg;
      va_start( arg,len );

// assign subscript ranges. Stop when a -ve value is encountered in the vararg list

      i= 0;
      while( (j=va_arg(arg,INT_))>0 )
     {
         n[i]= j;
         i++;
         assert( i< NDIM );
     }

// the last subscript range is the number of entities in the communication group

      n[i]= com.rem( par.neig );
      i++;
      assert( i< NDIM );
      m= pad( n[0],VLEN );
      v= n[1];
      assert( n0[1]==0 );

// check for consistency

      if( l == -1 ){ l= i; }
      assert( l==i );

// compute the offset for the next group

      dlen= m;
      for( k=1;k<l;k++ ){ dlen*= n[k]; }

      off= len;
      len+= dlen;
      last= len;

// Build the MPI datatype for receive - needed because of padding of the first dimension

// allocate workspaces

      assert( !styp );
      assert( !rtyp );

      rtyp= new MPI_Datatype[par.neig];
      styp= new MPI_Datatype[par.neig];

      for( k=0;k<par.neig;k++ )
     {    
         styp[k]= MPI_DATATYPE_NULL;
         rtyp[k]= MPI_DATATYPE_NULL;
     }

      h=   com.ldx[0];
      if( h > 0 )
     {
// find maximum length of derived data type 

        dlen= 0;
        ien=  0;
        jen=  0;
        for( k=0;k<par.neig;k++ )
       {
           ist=ien;
           ien=com.lsnd[k];

           dlen= ::max( (size_t)(ien-ist),dlen );

           INT_ h= com.ldx[k]; 

           jst=jen;
           jen= com.mdx[h-1][1];

           dlen= ::max( (size_t)(jen-jst),dlen );
       }
  
        for( k=0;k<l-1;k++ ){ dlen*= n[k]; };
  
        length= new INT_[dlen];
        disp  = new INT_[dlen];

         jen= com.mdx[h-1][1];

         n0[l-1]= jen;
         ien= com.lsnd[0];


         for( k=1;k<par.neig;k++ )
        {
            ist=ien;
            ien=com.lsnd[k];

            h= com.ldx[k]; 

            jst=jen;
            jen= com.mdx[h-1][1];

            assert( (ien>ist) == (jen>jst) );

            if( ien > ist )
           { 

// send types

               w= 0;
               for( j=ist;j<ien;j++ )
              {
                  s= com.isnd[j][3];
                  t= com.isnd[j][2];

                  assert( com.list[s][2] == t );

                  frule_t rule( com.hfx[s], var.idim(),var.jdim(),var.kdim() );

                  for( s=0;s<n[3];s++ )
                 {
                     for( r=0;r<n[2];r++ )
                    {
                        for( q=0;q<n[1];q++ ) 
                       {
                           for( p=0;p<n[0];p++ )
                          {

                              rule.transform( p,r,s, x,y,z );

                              length[w]=1;
                              disp[w]=  var.addr( q,x,y,z,t );
                              w++;
                          }
                       }
                    }
                 }
              }
               assert( w <= dlen );
               MPI_Type_indexed( w,length,disp, REAL_MPI, styp+k );
               MPI_Type_commit(  styp+k );

// receive types

               w=0;
               for( j=jst;j<jen;j++ )
              {
                  for( s=0;s<n[3];s++ )
                 {
                     for( r=0;r<n[2];r++ )
                    {
                        for( q=0;q<n[1];q++ )
                       {
                           for( p=0;p<n[0];p++ )
                          {
                              length[w]= 1;
                              disp[w]= addr(q,p,r,s,j);  
                              w++;
                          }
                       }
                    }
                 }
              }
               MPI_Type_indexed( w,length,disp, REAL_MPI, rtyp+k );
               MPI_Type_commit( rtyp+k );

           }
        }

// allocate request and status arrays

// done

         delete[] length;
         delete[] disp;
     }

      return;
  };

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void linear_t::fread( FILE *f )
  {
    ::fread( &l,   1,sizeof(INT_), f );         
    ::fread( &m,   1,sizeof(INT_), f );         
    ::fread(  n,NDIM,sizeof(INT_), f );         
      assert( m == pad( n[0],VLEN ) );
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void linear_t::fwrite( FILE *f )
  {
    ::fwrite( &l,   1,sizeof(INT_), f );         
    ::fwrite( &m,   1,sizeof(INT_), f );         
    ::fwrite(  n,NDIM,sizeof(INT_), f );         
  }

