#  include <msh.h>

   void msh_t::readdm( INT_ m, buf_t *buf, INT_ &j )
  {
      char                        *tok;
      INT_                         i0;
      tok= strtok( NULL," " ); sscanf( tok,"%x", &i0 ); 
      assert( i0==3 );
      return;
  }

   void msh_t::readvt( INT_ m, buf_t *buf, INT_ &j )
  {
      INT_                         ist,ien;
      INT_                         i;

      INT_                         i0,i1,i2,i3;
      double                       x0,x1,x2;

      char                        *tok;
      char                        *var;
      INT_                         flag; 

      j++;                      
      var= buf[j].data();
      tok= strtok( var," " );
      sscanf( tok,"%x", &flag ); 
      if( flag == 0 )           // global number
     {
         tok= strtok( NULL," " ); sscanf( tok,"%x", &i0 ); 
         tok= strtok( NULL," " ); sscanf( tok,"%x", &i1 ); 
         tok= strtok( NULL," " ); sscanf( tok,"%x", &i2 ); 
         tok= strtok( NULL," " ); sscanf( tok,"%x", &i3 ); 

         xx.n= i1; xx.resize(-999.);
         
         assert( i3 == 3 );
     }
      else
     {
         assert( flag > 0 );    // coordinates

         tok= strtok( NULL," " ); sscanf( tok,"%x", &ist); 
         tok= strtok( NULL," " ); sscanf( tok,"%x", &ien ); 
         tok= strtok( NULL," " ); sscanf( tok,"%x", &i2  ); 
         tok= strtok( NULL," " ); sscanf( tok,"%x", &i3 ); 

         ist--;
         assert( ien <= xx.n );

//       printf( "nodes: zone=%d ist=%d ien=%d\n", flag-1,ist,ien );

         assert( i2 == 1 ); // TGrid nodes 0: virtua, 1: ordinary, 2: boundary. ANSYS only uses 1
         assert( i3 == 3 );

         j++;
         var= buf[j].data();
         tok= strtok( var," "); 
         i= ist;
         while( tok )
        {
                                      sscanf( tok, "%lf", &x0 );
             tok= strtok( NULL," ");  sscanf( tok, "%lf", &x1 );
             tok= strtok( NULL," ");  sscanf( tok, "%lf", &x2 );

             xx[i][0]= x0;
             xx[i][1]= x1;
             xx[i][2]= x2;
             i++;

             tok= strtok( NULL," "); 
        }
         assert( i==ien );

     }
  }

   INT_ any( INT_ v, INT_ *w )
  {
      INT_ val= -1;
      for( INT_ i=0;i<4;i++ )
     {
         if( w[i]==v ){ val= i; break; };
     } 
      return val;
  }

   void share2( INT_ *a, INT_ *b, INT_ *w )
  {
      INT_ i,j,k;
      j= 0;
      for( i=0;i<4;i++ )
     {
         k= any( a[i],b ); if( k>-1 ){ w[j++]= a[i]; };
     }
      assert( j==2 );
      return;
  }

   INT_ share3( INT_ *a, INT_ *b, INT_ *c )
  {
      INT_4 d;
      INT_  val;

      share2( a,b, d );    
      if( any(d[0],c)>-1 )
     { 
         val= d[0]; 
     }
      else
     { 
         if( any(d[1],c)>-1 )
        { 
            val=d[1]; 
        }
         else
        { 
            assert( false ); 
        }
     }
      return val;
  }

   void msh_t::readhx( INT_ m, buf_t *buf, INT_ &j )
  {
      INT_                         ist,ien;

      INT_                         i0,i1,i2,i3;

      char                        *tok;
      char                        *var;

      INT_                         flag; 

      j++;                      
      var= buf[j].data();
      tok= strtok( var," " );
      sscanf( tok,"%x", &flag ); 
      if( flag == 0 )                       // global dimensions
     {
         tok= strtok( NULL," " ); sscanf( tok,"%x", &i0 ); 
         tok= strtok( NULL," " ); sscanf( tok,"%x", &i1 ); 
         tok= strtok( NULL," " ); sscanf( tok,"%x", &i2 ); 
         tok= strtok( NULL," " ); sscanf( tok,"%x", &i3 ); 

         hx.n= i1; hx.resize(-1);
         
     }
      if( flag > 0 )           
     {
         tok= strtok( NULL," " ); sscanf( tok,"%x", &ist  ); 
         tok= strtok( NULL," " ); sscanf( tok,"%x", &ien  ); 
         tok= strtok( NULL," " ); sscanf( tok,"%x", &i2   ); 
         tok= strtok( NULL," " ); sscanf( tok,"%x", &i3 ); 

         assert( ien <= hx.n );
         ist--;

         assert( i3==4 );
     }

      return;
  }

   void msh_t::readbn( INT_ m, buf_t *buf, INT_ &j )
  {

      char                        *tok;
      char                        *var;
      char                         typ[256];
      char                         lbl[256];

      INT_                         flag; 
 

      j++;                      
      var= buf[j].data();
      tok= strtok( var," " );
      sscanf( tok,"%x", &flag ); 

      assert( flag > 0 );
     {
         tok= strtok( NULL," " ); sscanf( tok,"%s",  typ   ); 
         tok= strtok( NULL," " ); sscanf( tok,"%s",  lbl   ); 

         printf( "group %3d %s %s\n", flag-3,typ,lbl );

     }

      return;
  }

   void msh_t::readfc( INT_ m, buf_t *buf, INT_ &j )
  {

      INT_                         ist,ien;
      INT_                         i,k;

      INT_                         i0,i1,i2,i3,i4,i5;

      char                        *tok;
      char                        *var;

      INT_                         flag; 


      j++;                      
      var= buf[j].data();
      tok= strtok( var," " );
      sscanf( tok,"%x", &flag ); 
      if( flag == 0 )                      // global dimensions          
     {
         tok= strtok( NULL," " ); sscanf( tok,"%x", &i1 ); 
         tok= strtok( NULL," " ); sscanf( tok,"%x", &i2 ); 
         tok= strtok( NULL," " ); sscanf( tok,"%x", &i3 ); 
         tok= strtok( NULL," " ); sscanf( tok,"%x", &i4 ); 
 
         fx.n= i2; fx.resize(-1);
 
     }
      else
     {
 
         assert( flag > 0 );              // face data
 
         tok= strtok( NULL," " ); sscanf( tok,"%x", &ist); 
         tok= strtok( NULL," " ); sscanf( tok,"%x", &ien); 
         tok= strtok( NULL," " ); sscanf( tok,"%x", &i3 ); 
         tok= strtok( NULL," " ); sscanf( tok,"%x", &i4 ); 
 
         assert( ien <= fx.n );
         assert( i4==4 );
         ist--;
 
         k= lf.n; lf.append(-1);
         lf[k][0]= ien;
         lf[k][1]= i3;
 
         printf( "quad:  zone=%d ist=%d ien=%d", flag-1,ist,ien );
         if( i3 == 2 ){ printf( " (inner)\n" ); }else{ printf( " (boundary)\n" ); };
 
         j++;
         var=buf[j].data();
         tok= strtok( var," "); 
         i= ist;
         while( tok )
        {
                                      sscanf( tok, "%x", &i0 );
             tok= strtok( NULL," ");  sscanf( tok, "%x", &i1 );
             tok= strtok( NULL," ");  sscanf( tok, "%x", &i2 );
             tok= strtok( NULL," ");  sscanf( tok, "%x", &i3 );
             tok= strtok( NULL," ");  sscanf( tok, "%x", &i4 );
             tok= strtok( NULL," ");  sscanf( tok, "%x", &i5 );
 
             fx[i][3]= --i0; assert( i0 < xx.n );
             fx[i][2]= --i1; assert( i1 < xx.n );
             fx[i][1]= --i2; assert( i2 < xx.n );
             fx[i][0]= --i3; assert( i3 < xx.n );
             fx[i][4]= --i4; assert( i4 < hx.n );
             fx[i][5]= -1;


             if( lf[k][1] == 2 ){ fx[i][5]= --i5;  assert( i5 < hx.n );};

             i++;
 
             tok= strtok( NULL," "); 
        }
         assert( i==ien );
     }
      return;

  }


   void msh_t::read( INT_ m, buf_t *buf )
  {
      INT_                         j;

      char   *tok;
      char   *var;
      INT_ flag; 

      j= 0;
      do 
     {
         if( buf[j].n > 1 )
        {
            var= buf[j].data(); 
            tok= strtok( var," " );
            sscanf( tok,"%d", &flag ); 
            
            if( strcmp( tok,"0" ) == 0 ) {}else{        
            if( strcmp( tok,"2" ) == 0 ) { readdm( m,buf,j ); }else{        
            if( strcmp( tok,"10") == 0 ) { readvt( m,buf,j ); }else{
            if( strcmp( tok,"12") == 0 ) { readhx( m,buf,j ); }else{
            if( strcmp( tok,"13") == 0 ) { readfc( m,buf,j ); }else{
            if( strcmp( tok,"39") == 0 ) { readbn( m,buf,j ); }}}}}}
        }
     }while( j++ < m );

      return;
  }

   bool no4( INT_ *a, INT_ *ia, INT_ *b, INT_ *ib )
  {
      INT_ n=0;
      if( ( a[ia[0]] != b[ib[0]] ) &&
          ( a[ia[0]] != b[ib[1]] ) &&
          ( a[ia[0]] != b[ib[2]] ) &&
          ( a[ia[0]] != b[ib[3]] ) ){ n++; };
      if( ( a[ia[1]] != b[ib[0]] ) &&
          ( a[ia[1]] != b[ib[1]] ) &&
          ( a[ia[1]] != b[ib[2]] ) &&
          ( a[ia[1]] != b[ib[3]] ) ){ n++; };
      if( ( a[ia[2]] != b[ib[0]] ) &&
          ( a[ia[2]] != b[ib[1]] ) &&
          ( a[ia[2]] != b[ib[2]] ) &&
          ( a[ia[2]] != b[ib[3]] ) ){ n++; };
      if( ( a[ia[3]] != b[ib[0]] ) &&
          ( a[ia[3]] != b[ib[1]] ) &&
          ( a[ia[3]] != b[ib[2]] ) &&
          ( a[ia[3]] != b[ib[3]] ) ){ n++; };
      return (n==4);
  }


   INT_ hase( INT_ p1, INT_ p2, INT_ *a )
  {
      INT_ val=-1;
      if( a[0]== p1 && a[1]== p2 ){ val=0; }else{
      if( a[1]== p1 && a[2]== p2 ){ val=1; }else{
      if( a[2]== p1 && a[3]== p2 ){ val=2; }else{
      if( a[3]== p1 && a[0]== p2 ){ val=3; }}}}
      return val;
  }

   void perm( INT_ *v, INT_ k )
  {
      v[0]=0;
      v[1]=1;
      v[2]=2;
      v[3]=3;
      for( INT_ j=0;j<k;j++ )
     {
         INT_ a;
         a=    v[0];
         v[0]= v[1];
         v[1]= v[2];
         v[2]= v[3];
         v[3]= a;
     }
  }
 

   void msh_t::hexs()
  {
      INT_                     i,j,k,h,l,m,n;
      bool                     o;
      INT_                     ist,ien;
      INT_                     wrk[6][4];
      INT_                     prm[6][4];
      INT_                     iprm[6];


      hf.n= hx.n; hf.resize( zdum );
      

      ien=0;
      for( k=0;k<lf.n;k++ )
     {
         ist= ien;
         ien= lf[k][0];

         n= 2; 
         if( lf[k][1]>= 3 ){ n=1; };
         for( h=ist;h<ien;h++ )
        {
            o=0;
            for( j=0;j<n;j++ )
           {
               i= fx[h][j+4];

               l= (hf[i].n)++;
               assert( l<6 );

               hf[i].f[l]= h;
               hf[i].o[l]= o;

               o=!o;
           }
        }
 
     }

      for( k=0;k<hx.n;k++ )
     { 

         assert( hf[k].n == 6 );  //should be       assert( hf[k].n==6 ); 
        {

/* loop over faces: at end of loop wrk[j][i] contains the i-th vertex of (candidate) face j.
   iprm is the permutation array sorting the faces on the hexahedron */

            for( j=0;j<6;j++ )
           {
               if( hf[k].o[j] )
              {
                  h= hf[k].f[j];
                  for( i=0;i<4;i++ ){ wrk[j][i]= fx[h][i]; };
              }
               else
              {
                  h= hf[k].f[j];
                  for( i=0;i<4;i++ ){ wrk[j][i]= fx[h][3-i]; };
              }
               sort4( wrk[j],prm[j] );
               iprm[j]= j;
           }

/* Identify face pairs that do not share vertices. These are opposite faces **/
            for( j=0;j<3;j++ )
           {
               l= iprm[2*j];
               for( h=2*j+1;h<6;h++ )
              {
                  m= iprm[h];
                  if( no4( wrk[l],prm[l], wrk[m],prm[m] ) )
                 { 
                     swap( iprm[2*j+1],iprm[h] );
                     break; 
                 };
              } 
           }


/* Identify faice pairs 0/1 ans 2/3 */
            l= iprm[0];
            for( j=2;j<6;j++ )
           {
               m= iprm[j]; 
               n= hase( wrk[l][1],wrk[l][0], wrk[m] );
               if( n > -1  )
              { 
                  perm( prm[m],n );
                  if( j%2 == 0 ){ j++; }else{ swap( iprm[j],iprm[j-1] ); };
                  m= iprm[j]; 
                  n= hase( wrk[l][3],wrk[l][2], wrk[m] );
                  assert( n > -1 );
                  perm( prm[m],n );
                  if( j == 5 )
                 {
                     swap( iprm[2],iprm[4] );
                     swap( iprm[3],iprm[5] );
                 }
                  break;
              } 
           }
            for( j=4;j<6;j++ )
           {
               m= iprm[j]; 
               n= hase( wrk[l][2],wrk[l][1], wrk[m] );
               if( n > -1 )
              { 
                  perm( prm[m],n );
                  if( j%2 == 0 ){ j++; }else{ swap( iprm[j],iprm[j-1] ); }; 
                  m= iprm[j]; 
                  n= hase( wrk[l][0],wrk[l][3], wrk[m] );
                  assert( n > -1 );
                  perm( prm[m],n );
                  break;
              } 
           }

/* Assemble the hexahedron */
            for( j=0;j<6;j++ )
           {
               m= iprm[j];
               l= hf[k].f[m];
               
               if( hf[k].o[m] == 0 )
              { 
                  assert( fx[l][4]== k ); 
                  assert( fx[l][6]==-1 ); 
                  fx[l][6]=j; 
              }
               else
              { 
                  assert( fx[l][5]== k ); 
                  assert( fx[l][7]==-1 ); 
                  fx[l][7]=j; 
              }
           }

// more elegant way didn't work, identify vertices by brute force
/*          m= iprm[0]; // perm( prm[m],0 );
            m= iprm[1]; // perm( prm[m],0 );

            l= iprm[0]; 
                        hx[k][0]= wrk[l][prm[l][0]];
                        hx[k][2]= wrk[l][prm[l][3]];
                        hx[k][4]= wrk[l][prm[l][1]];
                        hx[k][6]= wrk[l][prm[l][2]];

            m= iprm[2]; hx[k][1]= wrk[m][prm[m][2]];
            m= iprm[3]; hx[k][3]= wrk[m][prm[m][3]];
            m= iprm[2]; hx[k][5]= wrk[m][prm[m][3]];
            m= iprm[3]; hx[k][7]= wrk[m][prm[m][2]];*/

            hx[k][0]= share3( wrk[iprm[0]],wrk[iprm[2]],wrk[iprm[4]] );
            hx[k][1]= share3( wrk[iprm[1]],wrk[iprm[2]],wrk[iprm[4]] );

            hx[k][2]= share3( wrk[iprm[0]],wrk[iprm[3]],wrk[iprm[4]] );
            hx[k][3]= share3( wrk[iprm[1]],wrk[iprm[3]],wrk[iprm[4]] );

            hx[k][4]= share3( wrk[iprm[0]],wrk[iprm[2]],wrk[iprm[5]] );
            hx[k][5]= share3( wrk[iprm[1]],wrk[iprm[2]],wrk[iprm[5]] );

            hx[k][6]= share3( wrk[iprm[0]],wrk[iprm[3]],wrk[iprm[5]] );
            hx[k][7]= share3( wrk[iprm[1]],wrk[iprm[3]],wrk[iprm[5]] );


        }

     }

      return;
      
  }

   void msh_t::build( hmesh_t &var )
  {
      INT_ i,j;
      for( i=0;i<xx.n;i++ )
     {
         j=var.vx.append(-1);
         var.vx[j][0]= xx[i][0];
         var.vx[j][1]= xx[i][1];
         var.vx[j][2]= xx[i][2];
         j=var.vt.append(pdum);
         var.vt[j].id=j;
     }

      var.hx.n=hx.n; var.hx.resize(hdum);
      for( i=0;i<hx.n;i++ )
     {
         var.hex( i, hx[i][0], hx[i][1], hx[i][2], hx[i][3], hx[i][4], hx[i][5], hx[i][6], hx[i][7],
                     hx[i][0], hx[i][1], hx[i][2], hx[i][3], hx[i][4], hx[i][5], hx[i][6], hx[i][7] );
     }

      INT_ h=0;
      INT_ k=0;
      INT_ ist,ien;

      var.quads();

      ien=0;
      for( j=0;j<lf.n;j++ )
     {
         ist= ien;
         ien= lf[j][0];

         h= lf[j][1]-3; // lf[j][1]=2 are internal faces!
         if( h>-1 )
        { 
            printf( "adding group %d\n", k );
            for( i=ist;i<ien;i++ )
           {
               var.attach( k, fx[i][4],fx[i][6], -1,-1,-1 );
           }
            k++;
        } 
     }

      var.edges();
  }
