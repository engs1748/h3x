#  include <tri.h>

#  define TOL 1.e-9

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void tri_t::read( FILE *f )
  {
      INT_     i,j;

      fscanf( f,"%d %d", &m, &n );

      x= (REAL_3*)malloc( m*sizeof(REAL_3));
      y= (REAL_2*)malloc( m*sizeof(REAL_2));

      memset( x,-1,m*sizeof(REAL_3));
      memset( y,-1,m*sizeof(REAL_2));

      v= (vt_t*  )malloc( m*sizeof(vt_t  ));
      t= (tr_t*  )malloc( n*sizeof(tr_t  ));
 
      for( i=0;i<m;i++ )
     {
         fscanf( f,"%lf %lf %lf", x[i]+0,x[i]+1,x[i]+2 );
     }

      for( i=0;i<n;i++ )
     {
         fscanf( f,"%d %d %d", t[i].v+0,t[i].v+1,t[i].v+2 );
         j= t[i].v[0]; v[j].t= i; v[j].j= 0;
         j= t[i].v[1]; v[j].t= i; v[j].j= 1;
         j= t[i].v[2]; v[j].t= i; v[j].j= 2;
     }
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void tri_t::check( FILE *f )
  {
      int      i,j;

      for( i=0;i<n;i++ )
     {
                     fprintf( f, "\n" );
                     fprintf( f, "# %d %d %d\n", t[i].v[0],t[i].v[1],t[i].v[2] );
                     fprintf( f, "\n" );
         j= t[i].v[0]; fprintf( f, "%f %f %f\n", x[j][0], x[j][1], x[j][2] );
         j= t[i].v[1]; fprintf( f, "%f %f %f\n", x[j][0], x[j][1], x[j][2] );
         j= t[i].v[2]; fprintf( f, "%f %f %f\n", x[j][0], x[j][1], x[j][2] );
         j= t[i].v[0]; fprintf( f, "%f %f %f\n", x[j][0], x[j][1], x[j][2] );
     }

  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void tri_t::neig( vneig_t *wrk )
  {
      neig_t   dum;

      INT_2    iep[3]={ {0,1},{1,2},{2,0} };
      bool     f;
      INT_     o;

      INT_     i,j,h;

      INT_     a,e;

      for( i=0;i<n;i++ )
     {
         for( j=0;j<3;j++ )
        {
            a= t[i].v[ iep[j][0] ];
            e= t[i].v[ iep[j][1] ];
            o= 1;
            assert( a < m );
            assert( a > -1 );

            if( a>e )
           {
               swap( a,e );
               o= !o;
           }

            f= false;
            for( h=0;h<wrk[a].n;h++ )
           {
               if( wrk[a][h].v == e )
              {
                  f=true;
                  break;
              }
           }
            if( !f )
           { 
               h=(wrk[a].n)++; 
               wrk[a].resize(dum);
               wrk[a][h].v= e;
           }
            assert( wrk[a][h].t[o]== -1 );

            wrk[a][h].t[o]= i;
            wrk[a][h].j[o]= j;
            
        }
     }
      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void tri_t::edge( vneig_t *wrk )
  { 

      wsize_t<INT_,2,DLEN_T> wb;

      INT_2    iep[3]={ {0,1},{1,2},{2,0} };
      INT_     o;

      INT_     i,j,h,k;

      INT_     a,e, c,d, p,q, r,y,z;


      for( k=0;k<m;k++ )
     {
         for( h=0;h<wrk[k].n;h++ ) 
        {

            o= 0;
            for( i=0;i<2;i++ )
           {
               a= wrk[k][h].t[ o];
               e= wrk[k][h].t[!o];
                           
               c= wrk[k][h].j[ o];
               d= wrk[k][h].j[!o];
              
               if( a > -1 )
              {   
                  t[a].t[c]= e;
                  t[a].j[c]= d;
              } 
               else
              {
                  INT_ n=(wb.n)++;

                  wb.resize(-1);
                  wb[n][0]= e;
                  wb[n][1]= d;
                
              }
               o= !o;
           } 
        }  
     }

      i= 0;
      k= 0;
      z= -1;

      a= wb[i][0]; 
      e= wb[i][1]; 
      z= t[a].v[iep[e][0]];

      while( i<wb.n )
     {
         a= wb[i][0]; 
         e= wb[i][1]; 
         p= t[a].v[iep[e][0]];
         q= t[a].v[iep[e][1]];
         if( z ==-1 ){ z=p; };

         i= (b.n)++;
         b.resize(-1);
         b[i]= p;

         if( q == z )
        {
            k=(l.n)++;
            l.resize(-1);
            l[k][0]= i;
            l[k][1]= 0;
            z=-1;
        }

         for( j=i;j<wb.n;j++ )
        {
            r= wb[j][0]; 
            y= wb[j][1]; 
            c= t[r].v[iep[y][0]];
            d= t[r].v[iep[y][1]];

            if( c==q )
           {
               if( j > i )
              {
                  swap(wb[j][0],wb[i][0] );
                  swap(wb[j][1],wb[i][1] );
              }
               break;
           }
        }
     }

      return; 
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void tri_t::corner( INT_ p, INT_ q, INT_ r, INT_ s, INT_ t )
  {
      const REAL_ f=0.333;
      co_t     dum;
   
      REAL_ u= angle( x[p], x[q], x[r] );
      REAL_ v= angle( x[q], x[r], x[s] );
      REAL_ w= angle( x[r], x[s], x[t] );

      if( v < f*(u+w) )
     { 
         INT_ n=(c.n)++; 
         c.resize(dum);
         c[n].v= r;
         c[n].s= v;
         vex( x[q],x[r],x[s], c[n].w );
     };
  };

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void tri_t::corners()
  { 

      INT_     ist,ien;
      INT_     j,k;
      INT_     p,q,r,s,t;


      ien=0;
      for( k=0;k<l.n;k++ )
     {

         ist= ien;
         ien= l[k][0];

         p= b[ien-3];
         q= b[ien-2];
         r= b[ien-1];
         s= b[ist  ];
         t= b[ist+1];

         for( j=ist;j<ien-2;j++ )
        {

            p= q;
            q= r;
            r= s;
            s= t;
            t= b[j+2];

            corner( p,q,r,s,t );
        }

         p= q;
         q= r;
         r= s;
         s= t;
         t= b[ist];

         corner( p,q,r,s,t );

         p= q;
         q= r;
         r= s;
         s= t;
         t= b[ist+1];

         corner( p,q,r,s,t );

         l[k][1]= c.n;
     }

      return; 
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void tri_t::print( INT_ k, FILE *f )
  {
      INT_     a,b,c;

      a=t[k].v[0];
      b=t[k].v[1];
      c=t[k].v[2];

      fprintf( f,"\n" );
      fprintf( f,"# %d %d %d\n", a,b,c );
      fprintf( f,"\n" );
      fprintf( f,"%f %f %f \n", x[a][0], x[a][1], x[a][2] );
      fprintf( f,"%f %f %f \n", x[b][0], x[b][1], x[b][2] );
      fprintf( f,"%f %f %f \n", x[c][0], x[c][1], x[c][2] );
      fprintf( f,"%f %f %f \n", x[a][0], x[a][1], x[a][2] );
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void tri_t::fan( INT_ p, INT_ o, vsize_t<INT_,DLEN_T> &list )
  {

      INT_                   i,j,k,h;
      INT_                   n;
      wsize_t<INT_,2,DLEN_T> s;

      INT_2    ipe[3]={ {1,2},{2,0},{0,1} };
      INT_2    iep[3]={ {0,2},{1,0},{2,1} };

      i= v[p].t;
      j= v[p].j;
      j= iep[j][o];

     (s.n)++;
      s.resize(-1);
      s[0][0]= i;
      s[0][1]= j;

      n= (list.n)++;
      list.resize(-1);
      list[n]=i;

      while( s.n > 0 )
     {
    
         n= --(s.n);
         i= s[n][0];
         j= s[n][1];

         n= (list.n)++;
         list.resize(-1);
         list[n]=i;

         h= t[i].t[j];
         k= t[i].j[j];

         if( h > -1 && h != list[0] )
        {
            n= (s.n)++;
            s.resize(-1);
            s[n][0]= h;
            s[n][1]= ipe[k][o];
        }

     }

      return;

  }
