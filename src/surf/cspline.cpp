

#  include <surf/proto.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void cspline_c::fwrite( FILE *f )
  {
    ::fwrite( &m,           1, sizeof(INT_),  f ); 
    ::fwrite( &n,           1, sizeof(INT_),  f ); 
    ::fwrite( &t            1, sizeof(REAL_), f ); 
    ::fwrite( x0.data(),    m, sizeof(REAL_), f ); 
    ::fwrite( x1.data(),    n, sizeof(REAL_), f ); 
    ::fwrite(  y.data(),  m*n, sizeof(vtx_t), f ); 
    ::fwrite( y2.data(),  m*n, sizeof(vtx_t), f ); 
      surf_c::fwrite( f );
      return;
  }

   void cspline_c::fread( FILE *f )
  {
    ::fread( &m,           1,sizeof(INT_), f ); 
    ::fread( &n,           1,sizeof(INT_), f ); 
    ::fread( &t            1, sizeof(REAL_), f ); 
      assert( m < SAM_QSIZE );
      assert( n < SAM_QSIZE );
      x0.n=   m; x0.resize(-1.);
      x1.n=   n; x1.resize(-1.);
      y.n=  m*n;  y.resize(-1.);
      y2.n= m*n; y2.resize(-1.);
    ::fread( x0.data(),    m, sizeof(REAL_), f ); 
    ::fread( x1.data(),    n, sizeof(REAL_), f ); 
    ::fread(  y.data(),  m*n, sizeof(vtx_t), f ); 
    ::fread( y2.data(),  m*n, sizeof(vtx_t), f ); 
      surf_c::fread( f );
      return;
  }

   void cspline_c::read( char **arg )
  {
      vtx_t dum;
      REAL_3 v; 
      INT_  i,j,k,l;
      sscanf( arg[ 0],"%s", name );

      sscanf( arg[ 1],"%d",  &m );
      sscanf( arg[ 2],"%d",  &n );
      sscanf( arg[ 3],"%lf", &t );
      assert( m < SAM_QSIZE );
      assert( n < SAM_QSIZE );
      i=0;
      l=4;
      y.n=  m*n;   y.resize(-1.);
      y2.n= m*n;  y2.resize(-1.);
      for( k=0;k<n;k++ )
     {
         for( j=0;j<m;j++ )
        {
            sscanf( arg[l++], "%lf", v+0 );
            sscanf( arg[l++], "%lf", v+1 );
            sscanf( arg[l++], "%lf", v+2 );
            i=y.append(-1.);
            y[i][0]= v[0];
            y[i][1]= v[1];
            y[i][2]= v[2];
        }
     }

      x0.n= m; x0.resize(-1.);
      x1.n= n; x1.resize(-1.);
      for( i=0;i<m;i++ ){ x0[i]= ((REAL_)i)/((REAL_)(m-1)); };
      for( i=0;i<n;i++ ){ x1[i]= ((REAL_)i)/((REAL_)(n-1)); };

      spline( m,n,t,x0.data(),x1.data(), y.data(),y2.data() );

      autov( arg+l );
//    test( name );

      return;
  }


   void cspline_c::copy( surf_c *var )
  {
//    surf_c::copy( var );
      assert( (*var)->type() == surf_spline );
      cspline_c *wrk= (cspline_c*)var;

      wrk->m =  m;
      wrk->n =  n;
      wrk->t =  t;
      wrk->x0= x0;
      wrk->x1= x1;
      wrk->y=   y;
      wrk->y2= y2;

      return;
  }


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void cspline_c::interp( REAL_ *s, vtx_t &x )
  {

      splint( m,n, x0.data(), x1.data(), y.data(), y2.data(), s[0],s[1], x );

      return; 

  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void cspline_c::interp( REAL_ *s, vtx_t &x, vtx_t *dx )
  {
      splint( m,n, t, x0.data(), x1.data(), y.data(), y2.data(), s[0],s[1], x, dx );

      return; 


  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void cspline_c::interp( REAL_ *s, vtx_t &x, vtx_t *dx, vtx_t *d2x )
  {

      splint( m,n, t, x0.data(),x1.data(), y.data(),y2.data(), s[0],s[1], x,dx,d2x );

      return; 

  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void cspline_c::autov( char **arg )
  {
      v.insert(0,-BIG); v[0][0]= 0.00; v[0][1]= 0;
      v.insert(1,-BIG); v[1][0]= 0.25; v[1][1]= 0;
      v.insert(2,-BIG); v[2][0]= 0.50; v[2][1]= 0;
      v.insert(3,-BIG); v[3][0]= 0.75; v[3][1]= 0;
      v.insert(4,-BIG); v[4][0]= 1.00; v[4][1]= 0;

      v.insert(5,-BIG); v[5][0]= 0.00; v[5][1]= 1;
      v.insert(6,-BIG); v[6][0]= 0.25; v[6][1]= 1;
      v.insert(7,-BIG); v[7][0]= 0.50; v[7][1]= 1;
      v.insert(8,-BIG); v[8][0]= 0.75; v[8][1]= 1;
      v.insert(9,-BIG); v[9][0]= 1.00; v[9][1]= 1;

      patch_t pdum;
      p.insert(0,pdum);   p[0].v[0]=0; p[0].v[1]=1; p[0].v[2]=5; p[0].v[3]=6;
      p.insert(1,pdum);   p[1].v[0]=1; p[1].v[1]=2; p[1].v[2]=6; p[1].v[3]=7;
      p.insert(2,pdum);   p[2].v[0]=2; p[2].v[1]=3; p[2].v[2]=7; p[2].v[3]=8;
      p.insert(3,pdum);   p[3].v[0]=3; p[3].v[1]=4; p[3].v[2]=8; p[3].v[3]=9;
      ref(0);
      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -
 
   void cspline_c::test( char *name )
  {
      surf_c::test( v[0],v[1],v[2],v[3], name );
      return;
  }

