#  include <surf/proto.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void bilinear_c::fwrite( FILE *f )
  {
    ::fwrite( &x0,1,sizeof(vtx_t), f ); 
    ::fwrite( &x1,1,sizeof(vtx_t), f ); 
    ::fwrite( &x2,1,sizeof(vtx_t), f ); 
    ::fwrite( &x3,1,sizeof(vtx_t), f ); 
      surf_c::fwrite( f );
      return;
  }

   void bilinear_c::fread( FILE *f )
  {
    ::fread( &x0,1,sizeof(vtx_t), f ); 
    ::fread( &x1,1,sizeof(vtx_t), f ); 
    ::fread( &x2,1,sizeof(vtx_t), f ); 
    ::fread( &x3,1,sizeof(vtx_t), f ); 
      surf_c::fread( f );
      return;
  }

   void bilinear_c::read( char **arg )
  {
      sscanf( arg[ 0],"%s", name );

      sscanf( arg[ 1],"%lf", &(x0[0]) );
      sscanf( arg[ 2],"%lf", &(x0[1]) );
      sscanf( arg[ 3],"%lf", &(x0[2]) );

      sscanf( arg[ 4],"%lf", &(x1[0]) );
      sscanf( arg[ 5],"%lf", &(x1[1]) );
      sscanf( arg[ 6],"%lf", &(x1[2]) );

      sscanf( arg[ 7],"%lf", &(x2[0]) );
      sscanf( arg[ 8],"%lf", &(x2[1]) );
      sscanf( arg[ 9],"%lf", &(x2[2]) );

      sscanf( arg[10],"%lf", &(x3[0]) );
      sscanf( arg[11],"%lf", &(x3[1]) );
      sscanf( arg[12],"%lf", &(x3[2]) );

      autov( arg+13 );
//    test( name );


      return;
  }

   void bilinear_c::copy( surf_c *var )
  {
//    surf_c::copy( var );
      assert( var->type() == surf_bilinear );
      bilinear_c *wrk= (bilinear_c*)var;

      wrk->x0= x0;
      wrk->x1= x1;
      wrk->x2= x2;
      wrk->x3= x3;

      return;
  }
//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void bilinear_c::interp( REAL_ *s, vtx_t &x )
  {
      REAL_       u,v;
      REAL_4      q;

      u= s[0];
      v= s[1];

      q[0]= (1-u)*(1-v);
      q[1]=    u *(1-v);
      q[2]= (1-u)*   v;
      q[3]=    u *   v;

      x[0]=  q[0]*x0[0]+ q[1]*x1[0]+ q[2]*x2[0]+ q[3]*x3[0];
      x[1]=  q[0]*x0[1]+ q[1]*x1[1]+ q[2]*x2[1]+ q[3]*x3[1];
      x[2]=  q[0]*x0[2]+ q[1]*x1[2]+ q[2]*x2[2]+ q[3]*x3[2];

      return; 

  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void bilinear_c::interp( REAL_ *s, vtx_t &x, vtx_t *dx )
  {
      REAL_       u,v;
      REAL_4      q;

      REAL_4      dq[2];
 
      u= s[0];
      v= s[1];

      q[0]= (1-u)*(1-v);
      q[1]=    u *(1-v);
      q[2]= (1-u)*   v;
      q[3]=    u *   v;

      dq[0][0]= -(1-v);
      dq[0][1]=  (1-v);
      dq[0][2]=    -v;
      dq[0][3]=     v;

      dq[1][0]= -(1-u);
      dq[1][1]=    -u ;
      dq[1][2]=  (1-u);
      dq[1][3]=     u ;

      x[0]=  q[0]*x0[0]+ q[1]*x1[0]+ q[2]*x2[0]+ q[3]*x3[0];
      x[1]=  q[0]*x0[1]+ q[1]*x1[1]+ q[2]*x2[1]+ q[3]*x3[1];
      x[2]=  q[0]*x0[2]+ q[1]*x1[2]+ q[2]*x2[2]+ q[3]*x3[2];

      dx[0][0]=  dq[0][0]*x0[0]+ dq[0][1]*x1[0]+ dq[0][2]*x2[0]+ dq[0][3]*x3[0];
      dx[0][1]=  dq[0][0]*x0[1]+ dq[0][1]*x1[1]+ dq[0][2]*x2[1]+ dq[0][3]*x3[1];
      dx[0][2]=  dq[0][0]*x0[2]+ dq[0][1]*x1[2]+ dq[0][2]*x2[2]+ dq[0][3]*x3[2];

      dx[1][0]=  dq[1][0]*x0[0]+ dq[1][1]*x1[0]+ dq[1][2]*x2[0]+ dq[1][3]*x3[0];
      dx[1][1]=  dq[1][0]*x0[1]+ dq[1][1]*x1[1]+ dq[1][2]*x2[1]+ dq[1][3]*x3[1];
      dx[1][2]=  dq[1][0]*x0[2]+ dq[1][1]*x1[2]+ dq[1][2]*x2[2]+ dq[1][3]*x3[2];

      return; 

  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void bilinear_c::interp( REAL_ *s, vtx_t &x, vtx_t *dx, vtx_t *dx2 )
  {
      REAL_       u,v;
      REAL_4      q;

      REAL_4      dq[2];
      REAL_4      dq2[3];
 
      u= s[0];
      v= s[1];

      q[0]= (1-u)*(1-v);
      q[1]=    u *(1-v);
      q[2]= (1-u)*   v;
      q[3]=    u *   v;

      dq[0][0]= -(1-v);
      dq[0][1]=  (1-v);
      dq[0][2]=    -v;
      dq[0][3]=     v;

      dq[1][0]= -(1-u);
      dq[1][1]=    -u ;
      dq[1][2]=  (1-u);
      dq[1][3]=     u ;

      dq2[0][0]=    0;
      dq2[0][1]=    0;
      dq2[0][2]=    0;
      dq2[0][3]=    0;

      dq2[1][0]=    0;
      dq2[1][1]=    0;
      dq2[1][2]=    0;
      dq2[1][3]=    0;

      dq2[2][0]=    1;
      dq2[2][1]=   -1;
      dq2[2][2]=   -1;
      dq2[2][3]=    1;

      x[0]=  q[0]*x0[0]+ q[1]*x1[0]+ q[2]*x2[0]+ q[3]*x3[0];
      x[1]=  q[0]*x0[1]+ q[1]*x1[1]+ q[2]*x2[1]+ q[3]*x3[1];
      x[2]=  q[0]*x0[2]+ q[1]*x1[2]+ q[2]*x2[2]+ q[3]*x3[2];

      dx[0][0]=  dq[0][0]*x0[0]+ dq[0][1]*x1[0]+ dq[0][2]*x2[0]+ dq[0][3]*x3[0];
      dx[0][1]=  dq[0][0]*x0[1]+ dq[0][1]*x1[1]+ dq[0][2]*x2[1]+ dq[0][3]*x3[1];
      dx[0][2]=  dq[0][0]*x0[2]+ dq[0][1]*x1[2]+ dq[0][2]*x2[2]+ dq[0][3]*x3[2];

      dx[1][0]=  dq[1][0]*x0[0]+ dq[1][1]*x1[0]+ dq[1][2]*x2[0]+ dq[1][3]*x3[0];
      dx[1][1]=  dq[1][0]*x0[1]+ dq[1][1]*x1[1]+ dq[1][2]*x2[1]+ dq[1][3]*x3[1];
      dx[1][2]=  dq[1][0]*x0[2]+ dq[1][1]*x1[2]+ dq[1][2]*x2[2]+ dq[1][3]*x3[2];

      dx2[0][0]=  dq2[0][0]*x0[0]+ dq2[0][1]*x1[0]+ dq2[0][2]*x2[0]+ dq2[0][3]*x3[0];
      dx2[0][1]=  dq2[0][0]*x0[1]+ dq2[0][1]*x1[1]+ dq2[0][2]*x2[1]+ dq2[0][3]*x3[1];
      dx2[0][2]=  dq2[0][0]*x0[2]+ dq2[0][1]*x1[2]+ dq2[0][2]*x2[2]+ dq2[0][3]*x3[2];

      dx2[1][0]=  dq2[1][0]*x0[0]+ dq2[1][1]*x1[0]+ dq2[1][2]*x2[0]+ dq2[1][3]*x3[0];
      dx2[1][1]=  dq2[1][0]*x0[1]+ dq2[1][1]*x1[1]+ dq2[1][2]*x2[1]+ dq2[1][3]*x3[1];
      dx2[1][2]=  dq2[1][0]*x0[2]+ dq2[1][1]*x1[2]+ dq2[1][2]*x2[2]+ dq2[1][3]*x3[2];

      dx2[2][0]=  dq2[2][0]*x0[0]+ dq2[2][1]*x1[0]+ dq2[2][2]*x2[0]+ dq2[2][3]*x3[0];
      dx2[2][1]=  dq2[2][0]*x0[1]+ dq2[2][1]*x1[1]+ dq2[2][2]*x2[1]+ dq2[2][3]*x3[1];
      dx2[2][2]=  dq2[2][0]*x0[2]+ dq2[2][1]*x1[2]+ dq2[2][2]*x2[2]+ dq2[2][3]*x3[2];
      return; 

  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void bilinear_c::autov( char **arg )
  {
      v.insert(0,-BIG); v[0][0]= 0; v[0][1]=   0;
      v.insert(1,-BIG); v[1][0]= 1; v[1][1]=   0;
      v.insert(2,-BIG); v[2][0]= 0; v[2][1]=   1;
      v.insert(3,-BIG); v[3][0]= 1; v[3][1]=   1;

      patch_t pdum;
      p.insert(0,pdum);   p[0].v[0]=0; p[0].v[1]=1; p[0].v[2]=2; p[0].v[3]=3;
      ref(0);
      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -
 
   void bilinear_c::test( char *name )
  {
      surf_c::test( v[0],v[1],v[2],v[3], name );
      return;
  }

   bool bilinear_c::compare( surf_c *s )
  {
      REAL_ d=0;
//    assert( s->type() == surf_bilinear );
      if( s->type() != surf_bilinear ){ return false; };
      bilinear_c *w= (bilinear_c*)s;

      d= fmax( d, norminf( w->x0  - x0   ) );
      d= fmax( d, norminf( w->x1  - x1   ) );
      d= fmax( d, norminf( w->x2  - x2   ) );
      d= fmax( d, norminf( w->x3  - x3   ) );

      return (d<SURF_TOL);
  }
