
#  include <surf/proto.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void sampled_c::fwrite( FILE *f )
  {
    ::fwrite( &m,         1,sizeof(INT_), f ); 
    ::fwrite( &n,         1,sizeof(INT_), f ); 
    ::fwrite( x0.data(),n*m,sizeof(vtx_t), f ); 
      surf_c::fwrite( f );
      return;
  }

   void sampled_c::fread( FILE *f )
  {
    ::fread( &m,         1,sizeof(INT_), f ); 
    ::fread( &n,         1,sizeof(INT_), f ); 
      assert( m < SAM_QSIZE );
      assert( n < SAM_QSIZE );
      vtx_t dum;
      x0.n= n*m; x0.resize(dum);
    ::fread( x0.data(),n*m,sizeof(vtx_t), f ); 
      surf_c::fread( f );
      return;
  }

   void sampled_c::read( char **arg )
  {
      vtx_t dum;
      REAL_3 v; 
      INT_  i,j,k,l;
      sscanf( arg[ 0],"%s", name );

      sscanf( arg[ 1],"%d", &m );
      sscanf( arg[ 2],"%d", &n );
      assert( m < SAM_QSIZE );
      assert( n < SAM_QSIZE );
      i=0;
      l=3;
      for( k=0;k<n;k++ )
     {
         for( j=0;j<m;j++ )
        {
            sscanf( arg[l++], "%lf", v+0 );
            sscanf( arg[l++], "%lf", v+1 );
            sscanf( arg[l++], "%lf", v+2 );
            i=x0.append(dum);
            x0[i][0]= v[0];
            x0[i][1]= v[1];
            x0[i][2]= v[2];

            printf( "% 9.3e % 9.3e % 9.3e %d\n", v[0],v[1],v[2],i );
        }
     }

      REAL_2 s={0.3,0.3};
      vtx_t x;
      interp( s, x );

      autov( arg+l );
//    test( name );


      return;
  }

   void sampled_c::copy( surf_c *var )
  {
//    surf_c::copy( var );
      assert( var->type() == surf_sampled );
      sampled_c *wrk= (sampled_c*)var;

      wrk->m= m;
      wrk->n= n;
      wrk->x0= x0;

      return;
  }


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void sampled_c::interp( REAL_ *s, vtx_t &x )
  {

      REAL_       q[SAM_QSIZE];
      REAL_       p[SAM_QSIZE];
      INT_        i,j;
      vtx_t       u;

      lagr( m,s[0],p );
      lagr( n,s[1],q );

      x[0]=0;
      x[1]=0;
      x[2]=0;

      for( j=0;j<n;j++ )
     {
         for( i=0;i<m;i++ )
        {
            u= (p[i]*q[j])*x0[i+m*j]; x+= u;
        }
     }
      return; 

  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void sampled_c::interp( REAL_ *s, vtx_t &x, vtx_t *dx )
  {

      REAL_       q[128],p1[128];
      REAL_       p[128],q1[128];
      INT_        i,j;

      vtx_t       u;

      lagr( m,s[0],p,p1 );
      lagr( n,s[1],q,q1 );

      x[0]=0;
      x[1]=0;
      x[2]=0;

      dx[0][0]=0;
      dx[0][1]=0;
      dx[0][2]=0;
      dx[1][0]=0;
      dx[1][1]=0;
      dx[1][2]=0;

      for( j=0;j<n;j++ )
     {
         for( i=0;i<m;i++ )
        {
            u= p[i]*q[j] *x0[i+m*j]; x+= u;
                                   
            u= p1[i]*q[j]*x0[i+m*j]; dx[0]+= u;
            u= p[i]*q1[j]*x0[i+m*j]; dx[1]+= u;
        }
     }
      return; 


  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void sampled_c::interp( REAL_ *s, vtx_t &x, vtx_t *dx, vtx_t *dx2 )
  {

      REAL_       q[128],p1[128],p2[128];
      REAL_       p[128],q1[128],q2[128];
      INT_        i,j;
      vtx_t       u;

      lagr( m,s[0],p,p1,p2 );
      lagr( n,s[1],q,q1,q2 );

      x[0]=0;
      x[1]=0;
      x[2]=0;

      dx[0][0]=0;
      dx[0][1]=0;
      dx[0][2]=0;

      dx[1][0]=0;
      dx[1][1]=0;
      dx[1][2]=0;

      dx2[0][0]=0;
      dx2[0][1]=0;
      dx2[0][2]=0;

      dx2[1][0]=0;
      dx2[1][1]=0;
      dx2[1][2]=0;

      dx2[2][0]=0;
      dx2[2][1]=0;
      dx2[2][2]=0;

      for( j=0;j<n;j++ )
     {
         for( i=0;i<m;i++ )
        {
            u=  p[i]* q[j]*x0[i+m*j]; x+= u;
                                    
            u= p1[i]* q[j]*x0[i+m*j]; dx[0]+= u;
            u=  p[i]*q1[j]*x0[i+m*j]; dx[1]+= u;
                                    
            u= p2[i]* q[j]*x0[i+m*j];dx2[0]+= u;
            u=  p[i]*q2[j]*x0[i+m*j];dx2[1]+= u;
            u= p1[i]*q1[j]*x0[i+m*j];dx2[2]+= u;
        }
     }
      return; 

      return; 

  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void sampled_c::autov( char **arg )
  {
      v.insert(0,-BIG); v[0][0]= 0; v[0][1]=   0;
      v.insert(1,-BIG); v[1][0]= 1; v[1][1]=   0;
      v.insert(2,-BIG); v[2][0]= 0; v[2][1]=   1;
      v.insert(3,-BIG); v[3][0]= 1; v[3][1]=   1;

      patch_t pdum;
      p.insert(0,pdum);   p[0].v[0]=0; p[0].v[1]=1; p[0].v[2]=2; p[0].v[3]=3;
      ref(0);
      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -
 
   void sampled_c::test( char *name )
  {
      surf_c::test( v[0],v[1],v[2],v[3], name );
      return;
  }


   bool sampled_c::compare( surf_c *s )
  {
      REAL_ d=0;
//    assert( s->type() == surf_sampled );
      if( s->type() != surf_sampled ){ return false; };
      sampled_c *w= (sampled_c*)s;

      INT_ k=0;
      for( INT_ j=0;j<n;j++ )
     {
         for( INT_ i=0;i<m;i++ )
        {
            d= fmax( d, norminf( w->x0[k]  - x0[k]   ) );
            k++;
        }
     }

      return (d<SURF_TOL);
  }

