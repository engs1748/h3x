#  include <surf/proto.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void cyl_c::fwrite( FILE *f )
  {
    ::fwrite(  &x0, 1,sizeof(vtx_t), f ); 
    ::fwrite(  l+0, 1,sizeof(vtx_t), f ); 
    ::fwrite(  l+1, 1,sizeof(vtx_t), f ); 
    ::fwrite(  l+2, 1,sizeof(vtx_t), f ); 
      surf_c::fwrite( f );
      return;
  };

   void cyl_c::fread( FILE *f )
  {
    ::fread( &x0, 1,sizeof(vtx_t), f ); 
    ::fread( l+0, 1,sizeof(vtx_t), f ); 
    ::fread( l+1, 1,sizeof(vtx_t), f ); 
    ::fread( l+2, 1,sizeof(vtx_t), f ); 
      surf_c::fread( f );
      return;
  };

   void cyl_c::read( char **arg )
  {
      REAL_ v;

      sscanf( arg[ 0],"%s", name );

      sscanf( arg[ 1],"%lf", &(x0[0]) );
      sscanf( arg[ 2],"%lf", &(x0[1]) );
      sscanf( arg[ 3],"%lf", &(x0[2]) );

      sscanf( arg[ 4],"%lf", &(l[0][0]) );
      sscanf( arg[ 5],"%lf", &(l[0][1]) );
      sscanf( arg[ 6],"%lf", &(l[0][2]) );

      sscanf( arg[ 7],"%lf", &(l[2][0]) );
      sscanf( arg[ 8],"%lf", &(l[2][1]) );
      sscanf( arg[ 9],"%lf", &(l[2][2]) );

      sscanf( arg[10],"%lf", &r );
 

      l[1]=cross( l[2],l[0] );

      v= r/l2norm( l[0] );  l[0]*= v;
      v= r/l2norm( l[1] );  l[1]*= v;


      autov( arg+11 );

//    test( name );
      
      return;
  }

   void cyl_c::copy( surf_c *var )
  {
//    surf_c::copy( var );
      assert( var->type() == surf_cylinder );
      cyl_c *wrk= (cyl_c*)var;

      wrk->x0= x0;
      wrk->l[0]= l[0];
      wrk->l[1]= l[1];
      wrk->l[2]= l[2];

      return;
  }


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void cyl_c::interp( REAL_ *s, vtx_t &x )
  {
      REAL_       a,b;
      REAL_       u,v;
 
      a= s[0];
      b= s[1];

      u= cos(b);
      v= sin(b);

      x[0]=   x0[0];
      x[1]=   x0[1];
      x[2]=   x0[2];

      x[0]+=  l[0][0]*u;
      x[1]+=  l[0][1]*u;
      x[2]+=  l[0][2]*u;

      x[0]+=  l[1][0]*v;
      x[1]+=  l[1][1]*v;
      x[2]+=  l[1][2]*v;

      x[0]+=  l[2][0]*a;
      x[1]+=  l[2][1]*a;
      x[2]+=  l[2][2]*a;

      return; 

  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void cyl_c::interp( REAL_ *s, vtx_t &x, vtx_t *dx )
  {
      REAL_       a,b;
      REAL_       u,v;
 
      a= s[0];
      b= s[1];

      u= cos(b);
      v= sin(b);

      x[0]=   x0[0];
      x[1]=   x0[1];
      x[2]=   x0[2];

      x[0]+=  l[0][0]*u;
      x[1]+=  l[0][1]*u;
      x[2]+=  l[0][2]*u;

      x[0]+=  l[1][0]*v;
      x[1]+=  l[1][1]*v;
      x[2]+=  l[1][2]*v;

      x[0]+=  l[2][0]*a;
      x[1]+=  l[2][1]*a;
      x[2]+=  l[2][2]*a;

      dx[0][0]=  l[2][0];
      dx[0][1]=  l[2][1];
      dx[0][2]=  l[2][2];

      dx[1][0]= -l[0][0]*v;
      dx[1][1]= -l[0][1]*v;
      dx[1][2]= -l[0][2]*v;

      dx[1][0]+= l[1][0]*u;
      dx[1][1]+= l[1][1]*u;
      dx[1][2]+= l[1][2]*u;

      return; 

  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void cyl_c::interp( REAL_ *s, vtx_t &x, vtx_t *dx, vtx_t *dx2 )
  {
      REAL_       a,b;
      REAL_       u,v;
 
      a= s[0];
      b= s[1];

      u= cos(b);
      v= sin(b);

      x[0]=   x0[0];
      x[1]=   x0[1];
      x[2]=   x0[2];

      x[0]+=  l[0][0]*u;
      x[1]+=  l[0][1]*u;
      x[2]+=  l[0][2]*u;

      x[0]+=  l[1][0]*v;
      x[1]+=  l[1][1]*v;
      x[2]+=  l[1][2]*v;

      x[0]+=  l[2][0]*a;
      x[1]+=  l[2][1]*a;
      x[2]+=  l[2][2]*a;

      dx[0][0]=  l[2][0];
      dx[0][1]=  l[2][1];
      dx[0][2]=  l[2][2];

      dx[1][0]= -l[0][0]*v;
      dx[1][1]= -l[0][1]*v;
      dx[1][2]= -l[0][2]*v;

      dx[1][0]+= l[1][0]*u;
      dx[1][1]+= l[1][1]*u;
      dx[1][2]+= l[1][2]*u;

      dx2[0][0]=  0;
      dx2[0][1]=  0;
      dx2[0][2]=  0;

      dx2[1][0]= -l[0][0]*u;
      dx2[1][1]= -l[0][1]*u;
      dx2[1][2]= -l[0][2]*u;

      dx2[1][0]+=-l[1][0]*v;
      dx2[1][1]+=-l[1][1]*v;
      dx2[1][2]+=-l[1][2]*v;

      dx2[2][0]=  0;
      dx2[2][1]=  0;
      dx2[2][2]=  0;

      return; 

  }


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   INT_ cyl_c::vtx( char **arg )
  {
      INT_ i= surf_c::vtx( arg );
      v[i][1]*= M_PI;
      return i;
  };

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void cyl_c::autov( char **arg )
  {
      REAL_ a=-BIG,b=-BIG;
      REAL_ c=M_PI*0.25;

      sscanf( arg[0],"%lf", &a );
      sscanf( arg[1],"%lf", &b );

      v.insert(0,-BIG); v[0][0]= a; v[0][1]=   c;
      v.insert(1,-BIG); v[1][0]= a; v[1][1]= 3*c;
      v.insert(2,-BIG); v[2][0]= a; v[2][1]= 5*c;
      v.insert(3,-BIG); v[3][0]= a; v[3][1]= 7*c;
      v.insert(4,-BIG); v[4][0]= a; v[4][1]= 9*c;

      v.insert(5,-BIG); v[5][0]= b; v[5][1]=   c;
      v.insert(6,-BIG); v[6][0]= b; v[6][1]= 3*c;
      v.insert(7,-BIG); v[7][0]= b; v[7][1]= 5*c;
      v.insert(8,-BIG); v[8][0]= b; v[8][1]= 7*c;
      v.insert(9,-BIG); v[9][0]= b; v[9][1]= 9*c;

      patch_t pdum;
      p.insert(0,pdum);   p[0].v[0]= 2; p[0].v[1]= 1; p[0].v[2]= 5+2; p[0].v[3]= 5+1;
      p.insert(1,pdum);   p[1].v[0]= 3; p[1].v[1]= 4; p[1].v[2]= 5+3; p[1].v[3]= 5+4;
      p.insert(2,pdum);   p[2].v[0]= 2; p[2].v[1]= 3; p[2].v[2]= 5+2; p[2].v[3]= 5+3;
      p.insert(3,pdum);   p[3].v[0]= 1; p[3].v[1]= 0; p[3].v[2]= 5+1; p[3].v[3]= 5+0;
      ref(0);
      ref(1);
      ref(2);
      ref(3);
      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void cyl_c::test( char *name )
  {
      surf_c::test( v[0],v[4],v[5],v[9], name );
      return;
  }

   bool cyl_c::compare( surf_c *s )
  {
      REAL_ d=0;
//    assert( s->type() == surf_cylinder );
      if( s->type() != surf_cylinder ){ return false; };
      cyl_c *w= (cyl_c*)s;

      d= fmax( d, norminf( w->l[0]- l[0] ) );
      d= fmax( d, norminf( w->l[1]- l[1] ) );
      d= fmax( d, norminf( w->l[2]- l[2] ) );
      d= fmax( d, norminf( w->x0  - x0   ) );

      return (d<SURF_TOL);
  }
