

#  include <surf/proto.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void spline_c::fwrite( FILE *f )
  {
    ::fwrite( &m,           1, sizeof(INT_),  f ); 
    ::fwrite( &n,           1, sizeof(INT_),  f ); 
    ::fwrite( x0.data(),    m, sizeof(REAL_), f ); 
    ::fwrite( x1.data(),    n, sizeof(REAL_), f ); 
    ::fwrite(  y.data(),  m*n, sizeof(vtx_t), f ); 
    ::fwrite( y2.data(),  m*n, sizeof(vtx_t), f ); 
      surf_c::fwrite( f );
      return;
  }

   void spline_c::fread( FILE *f )
  {
    ::fread( &m,           1,sizeof(INT_), f ); 
    ::fread( &n,           1,sizeof(INT_), f ); 
      assert( m < SAM_QSIZE );
      assert( n < SAM_QSIZE );
      x0.n=   m; x0.resize(-1.);
      x1.n=   n; x1.resize(-1.);
      y.n=  m*n;  y.resize(-1.);
      y2.n= m*n; y2.resize(-1.);
    ::fread( x0.data(),    m, sizeof(REAL_), f ); 
    ::fread( x1.data(),    n, sizeof(REAL_), f ); 
    ::fread(  y.data(),  m*n, sizeof(vtx_t), f ); 
    ::fread( y2.data(),  m*n, sizeof(vtx_t), f ); 
      surf_c::fread( f );
      return;
  }

   void spline_c::read( char **arg )
  {
      vtx_t dum;
      REAL_3 v; 
      REAL_  u; 
      INT_  i,j,k,l;
      sscanf( arg[ 0],"%s", name );

      sscanf( arg[ 1],"%d", &m );
      sscanf( arg[ 2],"%d", &n );
      assert( m < SAM_QSIZE );
      assert( n < SAM_QSIZE );
      i=0;
      l=3;
      x0.n= m; x0.resize(-1.);
      x1.n= n; x1.resize(-1.);
      y.n=  m*n;   y.resize(-1.);
      y2.n= m*n;  y2.resize(-1.);
      for( i=0;i<m;i++ )
     { 
         sscanf( arg[l++], "%lf", &u ); 
         x0[i]= u;
     };
      for( i=0;i<n;i++ )
     { 
         sscanf( arg[l++], "%lf", &u ); 
         x1[i]= u;
     };
      i= 0;
      for( k=0;k<n;k++ )
     {
         for( j=0;j<m;j++ )
        {
            sscanf( arg[l++], "%lf", v+0 );
            sscanf( arg[l++], "%lf", v+1 );
            sscanf( arg[l++], "%lf", v+2 );
            y[i][0]= v[0];
            y[i][1]= v[1];
            y[i][2]= v[2];
            i++;
        }
     }

      spline( m,n,x0.data(),x1.data(), y.data(),y2.data() );

      autov( arg+l );
 
      printf( "testing %s\n", name );
      test( name );

      return;
  }


   void spline_c::copy( surf_c *var )
  {
//    surf_c::copy( var );
      assert( var->type() == surf_spline );
      spline_c *wrk= (spline_c*)var;

      wrk->m =  m;
      wrk->n =  n;
      wrk->x0= x0;
      wrk->x1= x1;
      wrk->y=   y;
      wrk->y2= y2;

      return;
  }


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void spline_c::interp( REAL_ *s, vtx_t &x )
  {

      splint( m,n, x0.data(), x1.data(), y.data(), y2.data(), s[0],s[1], x );

/*    lagr( m,s[0],p );
      lagr( n,s[1],q );

      x[0]=0;
      x[1]=0;
      x[2]=0;

      for( j=0;j<n;j++ )
     {
         for( i=0;i<m;i++ )
        {
            u= (p[i]*q[j])*x0[i+m*j]; x+= u;
        }
     }*/
      return; 

  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void spline_c::interp( REAL_ *s, vtx_t &x, vtx_t *dx )
  {
      splint( m,n, x0.data(), x1.data(), y.data(), y2.data(), s[0],s[1], x, dx );

      return; 


  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void spline_c::interp( REAL_ *s, vtx_t &x, vtx_t *dx, vtx_t *d2x )
  {

      splint( m,n, x0.data(),x1.data(), y.data(),y2.data(), s[0],s[1], x,dx,d2x );

      return; 

  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void spline_c::autov( char **arg )
  {
      v.insert(0,-BIG); v[0][0]= 0; v[0][1]=   0;
      v.insert(1,-BIG); v[1][0]= 1; v[1][1]=   0;
      v.insert(2,-BIG); v[2][0]= 0; v[2][1]=   1;
      v.insert(3,-BIG); v[3][0]= 1; v[3][1]=   1;

      patch_t pdum;
      p.insert(0,pdum);   p[0].v[0]=0; p[0].v[1]=1; p[0].v[2]=2; p[0].v[3]=3;
      ref(0);
      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -
 
   void spline_c::test( char *name )
  {
      surf_c::test( v[0],v[1],v[2],v[3], name );
      return;
  }

   bool spline_c::compare( surf_c *s )
  {
      REAL_ d=0;
//    assert( s->type() == surf_spline );
      if( s->type() != surf_spline ){ return false; };
      spline_c *w= (spline_c*)s;

      for( INT_ i=0;i<m;i++ )
     {
         d= fmax( d, norminf( w->x0[i]  - x0[i]   ) );
     }
      for( INT_ i=0;i<n;i++ )
     {
         d= fmax( d, norminf( w->x1[i]  - x1[i]   ) );
     }
      INT_ k=0;
      for( INT_ j=0;j<n;j++ )
     {
         for( INT_ i=0;i<m;i++ )
        {
            d= fmax( d, norminf( w->y[k]  - y[k]   ) );
            d= fmax( d, norminf( w->y2[k]  - y2[k]   ) );
            k++;
        }
     }
      return (d<SURF_TOL);
  }


