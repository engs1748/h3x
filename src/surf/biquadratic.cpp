
#  include <surf/proto.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void biquadratic_c::fwrite( FILE *f )
  {
    ::fwrite( &x0,1,sizeof(vtx_t), f ); 
    ::fwrite( &x1,1,sizeof(vtx_t), f ); 
    ::fwrite( &x2,1,sizeof(vtx_t), f ); 

    ::fwrite( &x3,1,sizeof(vtx_t), f ); 
    ::fwrite( &x4,1,sizeof(vtx_t), f ); 
    ::fwrite( &x5,1,sizeof(vtx_t), f ); 

    ::fwrite( &x6,1,sizeof(vtx_t), f ); 
    ::fwrite( &x7,1,sizeof(vtx_t), f ); 
    ::fwrite( &x8,1,sizeof(vtx_t), f ); 

      surf_c::fwrite( f );
      return;
  }

   void biquadratic_c::fread( FILE *f )
  {
    ::fread( &x0,1,sizeof(vtx_t), f ); 
    ::fread( &x1,1,sizeof(vtx_t), f ); 
    ::fread( &x2,1,sizeof(vtx_t), f ); 

    ::fread( &x3,1,sizeof(vtx_t), f ); 
    ::fread( &x4,1,sizeof(vtx_t), f ); 
    ::fread( &x5,1,sizeof(vtx_t), f ); 

    ::fread( &x6,1,sizeof(vtx_t), f ); 
    ::fread( &x7,1,sizeof(vtx_t), f ); 
    ::fread( &x8,1,sizeof(vtx_t), f ); 
      surf_c::fread( f );
      return;
  }

   void biquadratic_c::read( char **arg )
  {
      sscanf( arg[ 0],"%s", name );

      sscanf( arg[ 1],"%lf", &(x0[0]) );
      sscanf( arg[ 2],"%lf", &(x0[1]) );
      sscanf( arg[ 3],"%lf", &(x0[2]) );

      sscanf( arg[ 4],"%lf", &(x1[0]) );
      sscanf( arg[ 5],"%lf", &(x1[1]) );
      sscanf( arg[ 6],"%lf", &(x1[2]) );

      sscanf( arg[ 7],"%lf", &(x2[0]) );
      sscanf( arg[ 8],"%lf", &(x2[1]) );
      sscanf( arg[ 9],"%lf", &(x2[2]) );

      sscanf( arg[10],"%lf", &(x3[0]) );
      sscanf( arg[11],"%lf", &(x3[1]) );
      sscanf( arg[12],"%lf", &(x3[2]) );

      sscanf( arg[13],"%lf", &(x4[0]) );
      sscanf( arg[14],"%lf", &(x4[1]) );
      sscanf( arg[15],"%lf", &(x4[2]) );

      sscanf( arg[16],"%lf", &(x5[0]) );
      sscanf( arg[17],"%lf", &(x5[1]) );
      sscanf( arg[18],"%lf", &(x5[2]) );

      sscanf( arg[19],"%lf", &(x6[0]) );
      sscanf( arg[20],"%lf", &(x6[1]) );
      sscanf( arg[21],"%lf", &(x6[2]) );

      sscanf( arg[22],"%lf", &(x7[0]) );
      sscanf( arg[23],"%lf", &(x7[1]) );
      sscanf( arg[24],"%lf", &(x7[2]) );

      sscanf( arg[25],"%lf", &(x8[0]) );
      sscanf( arg[26],"%lf", &(x8[1]) );
      sscanf( arg[27],"%lf", &(x8[2]) );

      autov( arg+28 );
//    test( name );


      return;
  }


   void biquadratic_c::copy( surf_c *var )
  {
//    surf_c::copy( var );
      assert( var->type() == surf_biquadratic );
      biquadratic_c *wrk= (biquadratic_c*)var;

      wrk->x0= x0;
      wrk->x1= x1;
      wrk->x2= x2;
                 
      wrk->x3= x3;
      wrk->x4= x4;
      wrk->x5= x5;
                 
      wrk->x6= x6;
      wrk->x7= x7;
      wrk->x8= x8;

      return;
  }


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void biquadratic_c::interp( REAL_ *s, vtx_t &x )
  {
      REAL_       u,v;
      REAL_       q[9];

      REAL_3      f,g;

      u= s[0];
      v= s[1];

      f[0]= 2*    ( u-0.5 )*( u-1 );
      f[1]=-4* u *          ( u-1 );
      f[2]= 2* u *( u-0.5 );

      g[0]= 2*    ( v-0.5 )*( v-1 );
      g[1]=-4* v *          ( v-1 );
      g[2]= 2* v *( v-0.5 );

      q[0]= f[0]*g[0];
      q[1]= f[1]*g[0];
      q[2]= f[2]*g[0];

      q[3]= f[0]*g[1];
      q[4]= f[1]*g[1];
      q[5]= f[2]*g[1];

      q[6]= f[0]*g[2];
      q[7]= f[1]*g[2];
      q[8]= f[2]*g[2];

      x[0]=  q[0]*x0[0]+ q[1]*x1[0]+ q[2]*x2[0]+ q[3]*x3[0]+ q[4]*x4[0]+ q[5]*x5[0]+ q[6]*x6[0]+ q[7]*x7[0]+ q[8]*x8[0];
      x[1]=  q[0]*x0[1]+ q[1]*x1[1]+ q[2]*x2[1]+ q[3]*x3[1]+ q[4]*x4[1]+ q[5]*x5[1]+ q[6]*x6[1]+ q[7]*x7[1]+ q[8]*x8[1];
      x[2]=  q[0]*x0[2]+ q[1]*x1[2]+ q[2]*x2[2]+ q[3]*x3[2]+ q[4]*x4[2]+ q[5]*x5[2]+ q[6]*x6[2]+ q[7]*x7[2]+ q[8]*x8[2];

      return; 

  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void biquadratic_c::interp( REAL_ *s, vtx_t &x, vtx_t *dx )
  {
      REAL_       u,v;
      REAL_       q[9];

      REAL_       dq[2][9];
      REAL_3      f,g;
      REAL_3      df,dg;
 
      u= s[0];
      v= s[1];

      f[0]= 2*    ( u-0.5 )*( u-1 );
      f[1]=-4* u *          ( u-1 );
      f[2]= 2* u *( u-0.5 );

      g[0]= 2*    ( v-0.5 )*( v-1 );
      g[1]=-4* v *          ( v-1 );
      g[2]= 2* v *( v-0.5 );

      df[0]= 2*            ( u-1 ) +2*    ( u-0.5 );
      df[1]=-4*            ( u-1 ) -4* u;
      df[2]= 2* ( u-0.5 )          +2* u;

      dg[0]= 2*            ( v-1 ) +2*    ( v-0.5 );
      dg[1]=-4*            ( v-1 ) -4* v;
      dg[2]= 2* ( v-0.5 )          +2* v;

      q[0]= f[0]*g[0];
      q[1]= f[1]*g[0];
      q[2]= f[2]*g[0];

      q[3]= f[0]*g[1];
      q[4]= f[1]*g[1];
      q[5]= f[2]*g[1];

      q[6]= f[0]*g[2];
      q[7]= f[1]*g[2];
      q[8]= f[2]*g[2];

      dq[0][0]= df[0]*g[0];
      dq[0][1]= df[1]*g[0];
      dq[0][2]= df[2]*g[0];

      dq[0][3]= df[0]*g[1];
      dq[0][4]= df[1]*g[1];
      dq[0][5]= df[2]*g[1];

      dq[0][6]= df[0]*g[2];
      dq[0][7]= df[1]*g[2];
      dq[0][8]= df[2]*g[2];

      dq[1][0]= f[0]*dg[0];
      dq[1][1]= f[1]*dg[0];
      dq[1][2]= f[2]*dg[0];
                      
      dq[1][3]= f[0]*dg[1];
      dq[1][4]= f[1]*dg[1];
      dq[1][5]= f[2]*dg[1];
                      
      dq[1][6]= f[0]*dg[2];
      dq[1][7]= f[1]*dg[2];
      dq[1][8]= f[2]*dg[2];

      x[0]=  q[0]*x0[0]+ q[1]*x1[0]+ q[2]*x2[0]+ q[3]*x3[0]+ q[4]*x4[0]+ q[5]*x5[0]+ q[6]*x6[0]+ q[7]*x7[0]+ q[8]*x8[0];
      x[1]=  q[0]*x0[1]+ q[1]*x1[1]+ q[2]*x2[1]+ q[3]*x3[1]+ q[4]*x4[1]+ q[5]*x5[1]+ q[6]*x6[1]+ q[7]*x7[1]+ q[8]*x8[1];
      x[2]=  q[0]*x0[2]+ q[1]*x1[2]+ q[2]*x2[2]+ q[3]*x3[2]+ q[4]*x4[2]+ q[5]*x5[2]+ q[6]*x6[2]+ q[7]*x7[2]+ q[8]*x8[2];

      dx[0][0]=  dq[0][0]*x0[0]+ dq[0][1]*x1[0]+ dq[0][2]*x2[0]+ dq[0][3]*x3[0]+ dq[0][4]*x4[0]+ dq[0][5]*x5[0]+ dq[0][6]*x6[0]+ dq[0][7]*x7[0]+ dq[0][8]*x8[0];
      dx[0][1]=  dq[0][0]*x0[1]+ dq[0][1]*x1[1]+ dq[0][2]*x2[1]+ dq[0][3]*x3[1]+ dq[0][4]*x4[1]+ dq[0][5]*x5[1]+ dq[0][6]*x6[1]+ dq[0][7]*x7[1]+ dq[0][8]*x8[1];
      dx[0][2]=  dq[0][0]*x0[2]+ dq[0][1]*x1[2]+ dq[0][2]*x2[2]+ dq[0][3]*x3[2]+ dq[0][4]*x4[2]+ dq[0][5]*x5[2]+ dq[0][6]*x6[2]+ dq[0][7]*x7[2]+ dq[0][8]*x8[2];

      dx[1][0]=  dq[1][0]*x0[0]+ dq[1][1]*x1[0]+ dq[1][2]*x2[0]+ dq[1][3]*x3[0]+ dq[1][4]*x4[0]+ dq[1][5]*x5[0]+ dq[1][6]*x6[0]+ dq[1][7]*x7[0]+ dq[1][8]*x8[0];
      dx[1][1]=  dq[1][0]*x0[1]+ dq[1][1]*x1[1]+ dq[1][2]*x2[1]+ dq[1][3]*x3[1]+ dq[1][4]*x4[1]+ dq[1][5]*x5[1]+ dq[1][6]*x6[1]+ dq[1][7]*x7[1]+ dq[1][8]*x8[1];
      dx[1][2]=  dq[1][0]*x0[2]+ dq[1][1]*x1[2]+ dq[1][2]*x2[2]+ dq[1][3]*x3[2]+ dq[1][4]*x4[2]+ dq[1][5]*x5[2]+ dq[1][6]*x6[2]+ dq[1][7]*x7[2]+ dq[1][8]*x8[2];


      return; 

  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void biquadratic_c::interp( REAL_ *s, vtx_t &x, vtx_t *dx, vtx_t *dx2 )
  {
      REAL_       u,v;
      REAL_4      q;

      REAL_4      dq[2];
      REAL_4      dq2[3];
 
      REAL_3      f,g,df,dg,d2f,d2g;
      u= s[0];
      v= s[1];

      f[0]= 2*    ( u-0.5 )*( u-1 );
      f[1]=-4* u *          ( u-1 );
      f[2]= 2* u *( u-0.5 );

      g[0]= 2*    ( v-0.5 )*( v-1 );
      g[1]=-4* v *          ( v-1 );
      g[2]= 2* v *( v-0.5 );

      df[0]= 2*            ( u-1 ) +2*    ( u-0.5 );
      df[1]=-4*            ( u-1 ) -4* u;
      df[2]= 2* ( u-0.5 )          +2* u;

      dg[0]= 2*            ( v-1 ) +2*    ( v-0.5 );
      dg[1]=-4*            ( v-1 ) -4* v;
      dg[2]= 2* ( v-0.5 )          +2* v;

      d2f[0]= 2+2;
      d2f[1]=-4-4;
      d2f[2]= 2+2;

      d2g[0]= 2 +2;
      d2g[1]=-4 -4;
      d2g[2]= 2 +2;

      q[0]= f[0]*g[0];
      q[1]= f[1]*g[0];
      q[2]= f[2]*g[0];

      q[3]= f[0]*g[1];
      q[4]= f[1]*g[1];
      q[5]= f[2]*g[1];

      q[6]= f[0]*g[2];
      q[7]= f[1]*g[2];
      q[8]= f[2]*g[2];

      dq[0][0]= df[0]*g[0];
      dq[0][1]= df[1]*g[0];
      dq[0][2]= df[2]*g[0];

      dq[0][3]= df[0]*g[1];
      dq[0][4]= df[1]*g[1];
      dq[0][5]= df[2]*g[1];

      dq[0][6]= df[0]*g[2];
      dq[0][7]= df[1]*g[2];
      dq[0][8]= df[2]*g[2];

      dq[1][0]= f[0]*dg[0];
      dq[1][1]= f[1]*dg[0];
      dq[1][2]= f[2]*dg[0];
                      
      dq[1][3]= f[0]*dg[1];
      dq[1][4]= f[1]*dg[1];
      dq[1][5]= f[2]*dg[1];
                      
      dq[1][6]= f[0]*dg[2];
      dq[1][7]= f[1]*dg[2];
      dq[1][8]= f[2]*dg[2];

      dq2[0][0]= d2f[0]*g[0];
      dq2[0][1]= d2f[1]*g[0];
      dq2[0][2]= d2f[2]*g[0];

      dq2[0][3]= d2f[0]*g[1];
      dq2[0][4]= d2f[1]*g[1];
      dq2[0][5]= d2f[2]*g[1];

      dq2[0][6]= d2f[0]*g[2];
      dq2[0][7]= d2f[1]*g[2];
      dq2[0][8]= d2f[2]*g[2];

      dq2[1][0]= f[0]*d2g[0];
      dq2[1][1]= f[1]*d2g[0];
      dq2[1][2]= f[2]*d2g[0];

      dq2[1][3]= f[0]*d2g[1];
      dq2[1][4]= f[1]*d2g[1];
      dq2[1][5]= f[2]*d2g[1];

      dq2[1][6]= f[0]*d2g[2];
      dq2[1][7]= f[1]*d2g[2];
      dq2[1][8]= f[2]*d2g[2];

      dq2[2][0]= df[0]*dg[0];
      dq2[2][1]= df[1]*dg[0];
      dq2[2][2]= df[2]*dg[0];

      dq2[2][3]= df[0]*dg[1];
      dq2[2][4]= df[1]*dg[1];
      dq2[2][5]= df[2]*dg[1];

      dq2[2][6]= df[0]*dg[2];
      dq2[2][7]= df[1]*dg[2];
      dq2[2][8]= df[2]*dg[2];

      x[0]=  q[0]*x0[0]+ q[1]*x1[0]+ q[2]*x2[0]+ q[3]*x3[0]+ q[4]*x4[0]+ q[5]*x5[0]+ q[6]*x6[0]+ q[7]*x7[0]+ q[8]*x8[0];
      x[1]=  q[0]*x0[1]+ q[1]*x1[1]+ q[2]*x2[1]+ q[3]*x3[1]+ q[4]*x4[1]+ q[5]*x5[1]+ q[6]*x6[1]+ q[7]*x7[1]+ q[8]*x8[1];
      x[2]=  q[0]*x0[2]+ q[1]*x1[2]+ q[2]*x2[2]+ q[3]*x3[2]+ q[4]*x4[2]+ q[5]*x5[2]+ q[6]*x6[2]+ q[7]*x7[2]+ q[8]*x8[2];

      dx[0][0]=  dq[0][0]*x0[0]+ dq[0][1]*x1[0]+ dq[0][2]*x2[0]+ dq[0][3]*x3[0]+ dq[0][4]*x4[0]+ dq[0][5]*x5[0]+ dq[0][6]*x6[0]+ dq[0][7]*x7[0]+ dq[0][8]*x8[0];
      dx[0][1]=  dq[0][0]*x0[1]+ dq[0][1]*x1[1]+ dq[0][2]*x2[1]+ dq[0][3]*x3[1]+ dq[0][4]*x4[1]+ dq[0][5]*x5[1]+ dq[0][6]*x6[1]+ dq[0][7]*x7[1]+ dq[0][8]*x8[1];
      dx[0][2]=  dq[0][0]*x0[2]+ dq[0][1]*x1[2]+ dq[0][2]*x2[2]+ dq[0][3]*x3[2]+ dq[0][4]*x4[2]+ dq[0][5]*x5[2]+ dq[0][6]*x6[2]+ dq[0][7]*x7[2]+ dq[0][8]*x8[2];

      dx[1][0]=  dq[1][0]*x0[0]+ dq[1][1]*x1[0]+ dq[1][2]*x2[0]+ dq[1][3]*x3[0]+ dq[1][4]*x4[0]+ dq[1][5]*x5[0]+ dq[1][6]*x6[0]+ dq[1][7]*x7[0]+ dq[1][8]*x8[0];
      dx[1][1]=  dq[1][0]*x0[1]+ dq[1][1]*x1[1]+ dq[1][2]*x2[1]+ dq[1][3]*x3[1]+ dq[1][4]*x4[1]+ dq[1][5]*x5[1]+ dq[1][6]*x6[1]+ dq[1][7]*x7[1]+ dq[1][8]*x8[1];
      dx[1][2]=  dq[1][0]*x0[2]+ dq[1][1]*x1[2]+ dq[1][2]*x2[2]+ dq[1][3]*x3[2]+ dq[1][4]*x4[2]+ dq[1][5]*x5[2]+ dq[1][6]*x6[2]+ dq[1][7]*x7[2]+ dq[1][8]*x8[2];

      dx2[0][0]=  dq2[0][0]*x0[0]+ dq2[0][1]*x1[0]+ dq2[0][2]*x2[0]+ dq2[0][3]*x3[0]+ dq2[0][4]*x4[0]+ dq2[0][5]*x5[0]+ dq2[0][6]*x6[0]+ dq2[0][7]*x7[0]+ dq2[0][8]*x8[0];
      dx2[0][1]=  dq2[0][0]*x0[1]+ dq2[0][1]*x1[1]+ dq2[0][2]*x2[1]+ dq2[0][3]*x3[1]+ dq2[0][4]*x4[1]+ dq2[0][5]*x5[1]+ dq2[0][6]*x6[1]+ dq2[0][7]*x7[1]+ dq2[0][8]*x8[1];
      dx2[0][2]=  dq2[0][0]*x0[2]+ dq2[0][1]*x1[2]+ dq2[0][2]*x2[2]+ dq2[0][3]*x3[2]+ dq2[0][4]*x4[2]+ dq2[0][5]*x5[2]+ dq2[0][6]*x6[2]+ dq2[0][7]*x7[2]+ dq2[0][8]*x8[2];
         
      dx2[1][0]=  dq2[1][0]*x0[0]+ dq2[1][1]*x1[0]+ dq2[1][2]*x2[0]+ dq2[1][3]*x3[0]+ dq2[1][4]*x4[0]+ dq2[1][5]*x5[0]+ dq2[1][6]*x6[0]+ dq2[1][7]*x7[0]+ dq2[1][8]*x8[0];
      dx2[1][1]=  dq2[1][0]*x0[1]+ dq2[1][1]*x1[1]+ dq2[1][2]*x2[1]+ dq2[1][3]*x3[1]+ dq2[1][4]*x4[1]+ dq2[1][5]*x5[1]+ dq2[1][6]*x6[1]+ dq2[1][7]*x7[1]+ dq2[1][8]*x8[1];
      dx2[1][2]=  dq2[1][0]*x0[2]+ dq2[1][1]*x1[2]+ dq2[1][2]*x2[2]+ dq2[1][3]*x3[2]+ dq2[1][4]*x4[2]+ dq2[1][5]*x5[2]+ dq2[1][6]*x6[2]+ dq2[1][7]*x7[2]+ dq2[1][8]*x8[2];
         
      dx2[2][0]=  dq2[2][0]*x0[0]+ dq2[2][1]*x1[0]+ dq2[2][2]*x2[0]+ dq2[2][3]*x3[0]+ dq2[2][4]*x4[0]+ dq2[2][5]*x5[0]+ dq2[2][6]*x6[0]+ dq2[2][7]*x7[0]+ dq2[2][8]*x8[0];
      dx2[2][1]=  dq2[2][0]*x0[1]+ dq2[2][1]*x1[1]+ dq2[2][2]*x2[1]+ dq2[2][3]*x3[1]+ dq2[2][4]*x4[1]+ dq2[2][5]*x5[1]+ dq2[2][6]*x6[1]+ dq2[2][7]*x7[1]+ dq2[2][8]*x8[1];
      dx2[2][2]=  dq2[2][0]*x0[2]+ dq2[2][1]*x1[2]+ dq2[2][2]*x2[2]+ dq2[2][3]*x3[2]+ dq2[2][4]*x4[2]+ dq2[2][5]*x5[2]+ dq2[2][6]*x6[2]+ dq2[2][7]*x7[2]+ dq2[2][8]*x8[2];

      return; 

  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void biquadratic_c::autov( char **arg )
  {
      v.insert(0,-BIG); v[0][0]= 0; v[0][1]=   0;
      v.insert(1,-BIG); v[1][0]= 1; v[1][1]=   0;
      v.insert(2,-BIG); v[2][0]= 0; v[2][1]=   1;
      v.insert(3,-BIG); v[3][0]= 1; v[3][1]=   1;

      patch_t pdum;
      p.insert(0,pdum);   p[0].v[0]=0; p[0].v[1]=1; p[0].v[2]=2; p[0].v[3]=3;
      ref(0);
      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -
 
   void biquadratic_c::test( char *name )
  {
      surf_c::test( v[0],v[1],v[2],v[3], name );
      return;
  }


   bool biquadratic_c::compare( surf_c *s )
  {
      REAL_ d=0;
//    assert( s->type() == surf_biquadratic );
      if( s->type() != surf_biquadratic ){ return false; };
      biquadratic_c *w= (biquadratic_c*)s;

      d= fmax( d, norminf( w->x0  - x0   ) );
      d= fmax( d, norminf( w->x1  - x1   ) );
      d= fmax( d, norminf( w->x2  - x2   ) );

      d= fmax( d, norminf( w->x3  - x3   ) );
      d= fmax( d, norminf( w->x4  - x4   ) );
      d= fmax( d, norminf( w->x5  - x5   ) );

      d= fmax( d, norminf( w->x6  - x6   ) );
      d= fmax( d, norminf( w->x7  - x7   ) );
      d= fmax( d, norminf( w->x8  - x8   ) );

      return (d<SURF_TOL);
  }

