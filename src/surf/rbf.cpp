# include <surf/proto.h>

   REAL_ rbfsurf_c::F( vtx_t p0 ){ return interpolate( p0 ); }

   void rbfsurf_c::dF( vtx_t p0, REAL_ *d ){ derivative( p0, d ); }

   void rbfsurf_c::d2F( vtx_t p0, REAL_ *h ){ hessian( p0, h ); }

   void rbfsurf_c::malloc()
  {
      rbf_interp<3,vtx_t,REAL_>::malloc();
      if( n==3*m )
     {
         norms =new vtx_t [m];
         scales=new REAL_ [m];
     }
  }

   void rbfsurf_c::free()
  {
      rbf_interp<3,vtx_t,REAL_>::free();
      if( norms  ){ delete[] norms;   norms=NULL; }
      if( scales ){ delete[] scales; scales=NULL; }
  }

   void rbfsurf_c::copy( surf_c *var )
  {
      assert( var->type() == surf_rbf );
      rbfsurf_c *second = (rbfsurf_c*)var;

      second->n=n;
      second->m=m;
      second->accuracy=accuracy;
      second->scale=scale;

      second->malloc();
      delete[] second->norms;  second->norms =NULL;
      delete[] second->scales; second->scales=NULL;
      delete[] second->val;    second->val   =NULL;

      int i;
      for( i=0; i<n; i++ )
     {
         second->pt[i]=pt[i];
         second->w[ i]=w[ i];
     }
  }

   bool rbfsurf_c::compare( surf_c *s )
  {
      if( s->type() != surf_rbf ){ return false; }
      rbfsurf_c *other=(rbfsurf_c*)s;

      if( other->n != n ){ return false; }
      if( other->m != m ){ return false; }

      REAL_ d= other->accuracy - accuracy;
      for( int i=0; i<n; i++ )
     {
         d= fmax( d, norminf( other->pt[i] - pt[i] ) );
         d= fmax( d,          other->w[ i] - w[ i]   );
     }
      bool closeEnough = (d<SURF_TOL);
      return closeEnough;
  }

   void rbfsurf_c::offSurfacePoints()
  {
      int   i,k;

   // distance function increases along surface normal
      for( i=0; i<m; i++ )
     {
         k=3*i;
         val[k+1] =  scales[i];
         val[k+2] = -scales[i];

         pt[k+1] = pt[k] + scales[i]*norms[i];
         pt[k+2] = pt[k] - scales[i]*norms[i];
     }
  }

   int rbfsurf_c::build_weights()
  {
      int info=rbf_interp<3,vtx_t,REAL_>::build_weights();
      delete[] norms;   norms=NULL;
      delete[] scales; scales=NULL;
      return info;
  }

   void rbfsurf_c::freadPointCloud( char **arg )
  {
      int i,j,k;
      char     pointCloudFile[128];

      sscanf( arg[0],"%s", pointCloudFile);    // file with the point/normal cloud
      FILE *f=fopen( pointCloudFile, "r" );
      assert( f );

   // read dimensions and number of points in cloud
      int dims;
      fscanf( f, "%d", &dims );
      assert( dims==3 );

      fscanf( f, "%d", &m );
      n=3*m;

      malloc();

   // read points, normals and scales
      scale=0.;
      for( i=0; i<m; i++ )
     {
         k=3*i;
         for( j=0; j<3; j++ )
        {
            fscanf( f, " %lf ",    &pt[k][j] );
        }
         val[k]=0.;
         for( j=0; j<3; j++ )
        {
            fscanf( f, " %lf ", &norms[i][j] );
        }
         fscanf( f, " %lf ", &scales[i] );
         scale+=scales[i];
     }

      scale/=m;
      accuracy*=scale;

      offSurfacePoints();
      build_weights();

      fclose( f );

      return;
  }

   void rbfsurf_c::read( char **arg )
  {

      sscanf( arg[0],"%s", name );    // name of surface

      freadPointCloud( arg+1 );
      autov( arg+2 );   // create patches

      return;
  }

   void rbfsurf_c::fwrite( FILE *f )
  {
      rbf_interp<3,vtx_t,REAL_>::fwrite(   f );
    ::fwrite( &accuracy, 1, sizeof(REAL_), f );

      FILE *fextra=fopen( "rbfsurfbinary", "w" );
      rbf_interp<3,vtx_t,REAL_>::fwrite(   fextra );
    ::fwrite( &accuracy, 1, sizeof(REAL_), fextra );
      fclose( fextra );
  }

   void rbfsurf_c::fread( FILE *f )
  {
      rbf_interp<3,vtx_t,REAL_>::fread( f );
    ::fread( &accuracy, 1, sizeof(REAL_), f );
      delete[] norms;   norms=NULL;
      delete[] scales; scales=NULL;
  }

   void rbfsurf_c::interp( REAL_ *s, vtx_t &x )
  {
      bool approx=false;
      vtx_t x0(s[0],s[1],s[2]);
      project( x0, x, approx );
  }

   void rbfsurf_c::interp( REAL_ *s, vtx_t &x, vtx_t  *dx )
  {
      bool approx=false;
      bool proj =false;
      vtx_t x0(s[0],s[1],s[2]);
      project( x0,  x,       approx );
      tangent(  x, dx, proj, approx );
  }

   void rbfsurf_c::interp( REAL_ *s, vtx_t &x, vtx_t  *dx, vtx_t *dx2 )
  {
      bool approx=false;
      bool proj =false;
      vtx_t x0(s[0],s[1],s[2]);
      project(   x0,  x,        approx );
      tangent(    x, dx,  proj, approx );
      curvature(  x, dx2, proj, approx );
  }

   void rbfsurf_c::aprj( vtx_t &w, REAL_ *y, INT_ j, aprj_t &stat )
  {
      bool approx=true;
      project( w, w, approx );
      y[0]=w[0];
      y[1]=w[1];
      y[2]=w[2];
  }

   void rbfsurf_c::prj(  vtx_t &w, REAL_ *y,         aprj_t &stat )
  {
      bool approx=false;
      project( w, w, approx );
      y[0]=w[0];
      y[1]=w[1];
      y[2]=w[2];
  }

   void rbfsurf_c::autov( char **arg )
  {
      char  patchVerticesFile[128];

      sscanf( arg[0],"%s", patchVerticesFile );
      FILE *f=fopen( patchVerticesFile, "r"  );
      assert( f );

      int         i;
      int  npatches;
      int nvertices;
      fscanf( f, "%d", &nvertices );
      fscanf( f, "%d", &npatches  );

      for( i=0; i<nvertices; i++ )
     {
      // create new surface vertex (v array)
         v.insert( i, -BIG );

      // read coordinates of vertex
         fscanf( f, "%lf", &v[i][0] );
         fscanf( f, "%lf", &v[i][1] );
         fscanf( f, "%lf", &v[i][2] );
     }

      patch_t pdum;
      for( i=0; i<npatches; i++ )
     {
      // create new patch (p array)
         p.insert( i, pdum);

      // read vertex ids for patch
         fscanf( f, "%d", &p[i].v[0] );
         fscanf( f, "%d", &p[i].v[1] );
         fscanf( f, "%d", &p[i].v[2] );
         fscanf( f, "%d", &p[i].v[3] );

         ref( i );
     }
  }

   void rbfsurf_c::updvtx( REAL_ *f, REAL_ *z, REAL_ rlx )
  {
      bool     proj=true;
      bool  approx=false;

      vtx_t    n, x(z[0],z[1],z[2]), dz(f[0],f[1],f[2]), y;

   // project the update f onto the tangent plane
      normal( x, n, proj, approx );
      dz-=(dz*n)*n;

   // displace vertex and project onto surface
      x+=rlx*dz;
      project( x, x, approx );

      z[0]=y[0];
      z[1]=y[1];
      z[2]=y[2];
  }

