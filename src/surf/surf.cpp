#  include <surf/proto.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void newsurf( INT_ t, surf_c **val )
  {
      assert( !(*val) );
      switch(t)
     {
         case(surf_cylinder):
        {
          (*val)= new cyl_c();
            break;
        }
         case(surf_plane):
        {
          (*val)= new plane_c();
            break;
        }
         case(surf_bilinear):
        {
          (*val)= new bilinear_c();
            break;
        }
         case(surf_biquadratic):
        {
          (*val)= new biquadratic_c();
            break;
        }
         case(surf_sampled):
        {
          (*val)= new sampled_c();
            break;
        }
         case(surf_spline):
        {
          (*val)= new spline_c();
            break;
        }
         case(surf_cspline):
        {
          (*val)= new spline_c();
            break;
        }
         case(surf_rbf):
        {
          (*val)= new rbfsurf_c();
            break;
        }
         default:
        {
            assert( false );
        }
     }
      return;
  }

   void surf_c::test( REAL_b y0, REAL_b y1, REAL_b y2, REAL_b y3, char *name )
  {
      FILE *f;

      INT_   i,j;
      REAL_2 u;
      REAL_b w;
      REAL_4 q;
      vtx_t  x;
      vtx_t  dx[2];

      f= fopen( name,"w" );
      assert( f );

      for( j=0;j<64;j++ )
     {
         u[1]= j;
         u[1]/=63;

         fprintf( f,"\n" );
         for( i=0;i<64;i++ )
        {
            u[0]= i;
            u[0]/=63;

            q[0]= (1-u[0])*(1-u[1]); 
            q[1]=    u[0] *(1-u[1]); 
            q[2]= (1-u[0])*   u[1] ; 
            q[3]=    u[0] *   u[1] ; 

            w[0]= y0[0]*q[0]+ y1[0]*q[1]+ y2[0]*q[2]+ y3[0]*q[3];
            w[1]= y0[1]*q[0]+ y1[1]*q[1]+ y2[1]*q[2]+ y3[1]*q[3];

            interp( w, x,dx );
            fprintf( f, "% 12.5e % 12.5e % 12.5e % 12.5e % 12.5e\n", x[0],x[1],x[2], w[0],w[1] );
             
        } 
     } 
      fclose( f );
  }

   void surf_c::test( INT_ j, char *name )
  {
      test( v[p[j].v[0]],v[p[j].v[1]],v[p[j].v[2]],v[p[j].v[3]], name );
  }

// 07/05/19 jhc: changed to virtual to override in rbf surface
// void surf_c::aprj( vtx_t &w, REAL_ *y, INT_ j, aprj_t &stat )

   void surf_c::srch( vtx_t &w, REAL_ *y )
  {

      vtx_t                     f;
      REAL_                     d;
      REAL_                     e;

      REAL_b z;
      d= 999;
      for( INT_ k=0;k<16;k++ )
     {
         for( INT_ i=0;i<16;i++ )
        {
            z[0]= i; z[0]/= 15;
            z[1]= k; z[1]/= 15;

            interp( z,f );
            e= l2norm(f-w);
            if( e < d )
           { 
               y[0]= z[0];
               y[1]= z[1]; 
               d= e; 
           }
        }
     }
      interp( y, w );

      return;

   }

   INT_ surf_c::vtx( char **arg )
  {
      INT_ i;
      sscanf( arg[0],"%d", &i ); assert( i > -1 );
      v.insert(i,-BIG);
      sscanf( arg[1],"%lf", v[i]+0 );
      sscanf( arg[2],"%lf", v[i]+1 );
      return i;
  };

   INT_ surf_c::patch( char **arg )
  {
      INT_ i, i0,i1,i2,i3;

      sscanf( arg[0],"%d", &i ); assert( i > -1 );
      sscanf( arg[1],"%d", &i0); assert( i0> -1 );
      sscanf( arg[2],"%d", &i1); assert( i1> -1 );
      sscanf( arg[3],"%d", &i2); assert( i2> -1 );
      sscanf( arg[4],"%d", &i3); assert( i3> -1 );


      patch_t pdum;
      p.insert(i,pdum);
      p[i].v[0]= i0;
      p[i].v[1]= i1;
      p[i].v[2]= i2;
      p[i].v[3]= i3;
      ref(i);
      return i;
  }

   extern "C" void dsyev_( char *, char *, INT_ *, REAL_ *, INT_ *, REAL_ *, REAL_ *, INT_ *, INT_ * );

   void surf_c::ref( INT_ i )
  {
      vtx_t                     u[4];
      vtx_t                     f;
      vtx_t                    df[2];
      REAL_4                    q;
      REAL_4                   dq[3];
      REAL_                  work[8];
      REAL_                     r,s;
      REAL_3                    n;
      INT_                     ng;

      REAL_4                   yg,wg;
// compute metrics

/*    ng= 2; 
      yg[1]=  sqrt(3.)/3.;
      yg[0]= -yg[1];

      yg[0]+= 1;
      yg[1]+= 1;

      yg[0]*= 0.5;
      yg[1]*= 0.5;

      wg[0]= 0.5;
      wg[1]= 0.5;*/

      ng= 4;
      r= sqrt(6./5.);
      r*= 2;
      r/= 7.;
      s= 3./7.;
      
      yg[3]= s+r;
      yg[2]= s-r;
      yg[3]= sqrt(yg[3]); 
      yg[2]= sqrt(yg[2]); 
      yg[1]=-yg[2];
      yg[0]=-yg[3];
      yg[0]+= 1;
      yg[1]+= 1;
      yg[2]+= 1;
      yg[3]+= 1;
      yg[0]*= 0.5;
      yg[1]*= 0.5;
      yg[2]*= 0.5;
      yg[3]*= 0.5;
      r= sqrt(30.)/36.;
      wg[0]= 0.5+r;
      wg[1]= 0.5-r;
      wg[2]= wg[1];
      wg[3]= wg[0];
      wg[0]*= 0.5;
      wg[1]*= 0.5;
      wg[2]*= 0.5;
      wg[3]*= 0.5;
// boundary metrics

      interp( v[p[i].v[0]], u[0] );
      interp( v[p[i].v[1]], u[1] );
      interp( v[p[i].v[2]], u[2] );
      interp( v[p[i].v[3]], u[3] );

      memset( p[i].x0,0,3*sizeof(REAL_));
      memset( p[i].z,0,9*sizeof(REAL_));

      r= 0;
      for( INT_ k=0;k<ng;k++ )
     {
         for( INT_ j=0;j<ng;j++ )
        {
            q[0]= (1-yg[j])*(1-yg[k]);
            q[1]=    yg[j] *(1-yg[k]);
            q[2]= (1-yg[j])*   yg[k] ;
            q[3]=    yg[j] *   yg[k] ;
           
            dq[0][0]= -(1-yg[k]);
            dq[0][1]=  (1-yg[k]);
            dq[0][2]= -   yg[k] ;
            dq[0][3]=     yg[k] ;
           
            dq[1][0]= -(1-yg[j]);
            dq[1][1]= -   yg[j] ;
            dq[1][2]=  (1-yg[j]);
            dq[1][3]=     yg[j] ;

            f= q[0]*u[0]+ q[1]*u[1]+ q[2]*u[2]+ q[3]*u[3];
            df[0]= dq[0][0]*u[0]+ dq[0][1]*u[1]+ dq[0][2]*u[2]+ dq[0][3]*u[3];
            df[1]= dq[1][0]*u[0]+ dq[1][1]*u[1]+ dq[1][2]*u[2]+ dq[1][3]*u[3];
    
            cross3( df[0].x, df[1].x, n );
            REAL_ w= norm32(n); scal3( 1./w,n ); 

            w*= wg[j]*wg[k];

            p[i].x0[0]+= w*f[0];
            p[i].x0[1]+= w*f[1];
            p[i].x0[2]+= w*f[2];
                       
            r+= w;
        }
     }
      r= fmax(r,1.e-16);
      r= 1./r;
      p[i].x0[0]*= r;
      p[i].x0[1]*= r;
      p[i].x0[2]*= r;

      for( INT_ k=0;k<ng;k++ )
     {
         for( INT_ j=0;j<ng;j++ )
        {
            q[0]= (1-yg[j])*(1-yg[k]);
            q[1]=    yg[j] *(1-yg[k]);
            q[2]= (1-yg[j])*   yg[k] ;
            q[3]=    yg[j] *   yg[k] ;
           
            dq[0][0]= -(1-yg[k]);
            dq[0][1]=  (1-yg[k]);
            dq[0][2]= -   yg[k] ;
            dq[0][3]=     yg[k] ;
           
            dq[1][0]= -(1-yg[j]);
            dq[1][1]= -   yg[j] ;
            dq[1][2]=  (1-yg[j]);
            dq[1][3]=     yg[j] ;

            f= q[0]*u[0]+ q[1]*u[1]+ q[2]*u[2]+ q[3]*u[3];
            df[0]= dq[0][0]*u[0]+ dq[0][1]*u[1]+ dq[0][2]*u[2]+ dq[0][3]*u[3];
            df[1]= dq[1][0]*u[0]+ dq[1][1]*u[1]+ dq[1][2]*u[2]+ dq[1][3]*u[3];
    
            cross3( df[0].x,df[1].x, n );
            REAL_ w= norm32(n); scal3( 1./w,n ); 

            f[0]-= p[i].x0[0];
            f[1]-= p[i].x0[1];
            f[2]-= p[i].x0[2];

            w*= wg[j]*wg[k];

            p[i].z[0]+= w*f[0]*f[0];
            p[i].z[1]+= w*f[0]*f[1];
            p[i].z[2]+= w*f[0]*f[2];

            p[i].z[4]+= w*f[1]*f[1];
            p[i].z[5]+= w*f[1]*f[2];

            p[i].z[8]+= w*f[2]*f[2];
         
        }
     }
      p[i].z[0]*= r;
      p[i].z[1]*= r;
      p[i].z[2]*= r;

      p[i].z[4]*= r;
      p[i].z[5]*= r;

      p[i].z[8]*= r;

     {
         char jobz='v';
         char uplo='l';
         INT_ n=3;
         INT_ lda=3;
         INT_ lwork=8;
         INT_ info;
         dsyev_( &jobz,&uplo, &n,p[i].z,&lda, p[i].d,work, &lwork,&info );
     }

      p[i].d[0]= sqrt( fmax(0.,p[i].d[0]) );
      p[i].d[1]= sqrt( fmax(0.,p[i].d[1]) );
      p[i].d[2]= sqrt( fmax(0.,p[i].d[2]) );
  }

// 07/05/19 jhc: changed to virtual to override in rbf surface
// void surf_c::prj( vtx_t &x0, REAL_ *y, aprj_t &stat )

   void fread( vsurf_t &var, FILE *f )
  {
    ::fread( &(var.n),1,sizeof(INT_), f );
      var.resize(NULL);

      for( INT_ i=0;i<var.n;i++ )
     {
         surf_e t=surf_bad;
       ::fread( &t,1,sizeof(surf_e), f );

         surf_c *wrk= NULL;
         newsurf( t,&wrk ); var[i]=wrk;
         var[i]->fread( f );
     }
  }

   void fwrite( vsurf_t &var, FILE *f )
  {
    ::fwrite( &(var.n),1,sizeof(INT_), f );
      for( INT_ i=0;i<var.n;i++ )
     {
         surf_e t= var[i]->type();
       ::fwrite( &t,1,sizeof(surf_e), f );
         var[i]->fwrite( f );
     }
  }

   void surf_c::pnorm( INT_ j, vtx_t &x, vtx_t &n )
  {
      INT_ i0,i1,i2,i3;

      vtx_t d[2];

      i0= p[j].v[0];
      i1= p[j].v[1];
      i2= p[j].v[2];
      i3= p[j].v[3];

      REAL_b y;
      y[0]= 0.25*( v[i0][0]+ v[i1][0]+ v[i2][0]+ v[i3][0] );
      y[1]= 0.25*( v[i0][1]+ v[i1][1]+ v[i2][1]+ v[i3][1] );

      interp( y, x,d );

      n= cross( d[0],d[1] );
      n/= l2norm( n );
      
      return;
  }
