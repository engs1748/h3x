#  include <surf/proto.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void plane_c::fwrite( FILE *f )
  {
    ::fwrite( &x0,1,sizeof(vtx_t), f ); 
    ::fwrite( l+0,1,sizeof(vtx_t), f ); 
    ::fwrite( l+1,1,sizeof(vtx_t), f ); 
      surf_c::fwrite( f );
      return;
  }

   void plane_c::fread( FILE *f )
  {
    ::fread( &x0,1,sizeof(vtx_t), f ); 
    ::fread( l+0,1,sizeof(vtx_t), f ); 
    ::fread( l+1,1,sizeof(vtx_t), f ); 
      surf_c::fread( f );
      return;
  }

   void plane_c::read( char **arg )
  {
      sscanf( arg[ 0],"%s", name );

      sscanf( arg[ 1],"%lf", &(x0[0]) );
      sscanf( arg[ 2],"%lf", &(x0[1]) );
      sscanf( arg[ 3],"%lf", &(x0[2]) );

      sscanf( arg[ 4],"%lf", &(l[0][0]) );
      sscanf( arg[ 5],"%lf", &(l[0][1]) );
      sscanf( arg[ 6],"%lf", &(l[0][2]) );

      sscanf( arg[ 7],"%lf", &(l[1][0]) );
      sscanf( arg[ 8],"%lf", &(l[1][1]) );
      sscanf( arg[ 9],"%lf", &(l[1][2]) );

      autov( arg+10 );
//    test( name );


      return;
  }

   void plane_c::copy( surf_c *var )
  {
//    surf_c::copy( var );
      assert( var->type() == surf_plane );
      plane_c *wrk= (plane_c*)var;

      wrk->x0= x0;
      wrk->l[0]= l[0];
      wrk->l[1]= l[1];

      return;
  }


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void plane_c::interp( REAL_ *s, vtx_t &x )
  {
      REAL_       u,v;
 
      u= s[0];
      v= s[1];

      x[0]=   x0[0];
      x[1]=   x0[1];
      x[2]=   x0[2];

      x[0]+=  l[0][0]*u;
      x[1]+=  l[0][1]*u;
      x[2]+=  l[0][2]*u;

      x[0]+=  l[1][0]*v;
      x[1]+=  l[1][1]*v;
      x[2]+=  l[1][2]*v;

      return; 

  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void plane_c::interp( REAL_ *s, vtx_t &x, vtx_t *dx )
  {
      REAL_       u,v;
 
      u= s[0];
      v= s[1];

      x[0]=   x0[0];
      x[1]=   x0[1];
      x[2]=   x0[2];

      x[0]+=  l[0][0]*u;
      x[1]+=  l[0][1]*u;
      x[2]+=  l[0][2]*u;

      x[0]+=  l[1][0]*v;
      x[1]+=  l[1][1]*v;
      x[2]+=  l[1][2]*v;

      dx[0][0]=  l[0][0];
      dx[0][1]=  l[0][1];
      dx[0][2]=  l[0][2];
              
      dx[1][0]=  l[1][0];
      dx[1][1]=  l[1][1];
      dx[1][2]=  l[1][2];

      return; 

  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void plane_c::interp( REAL_ *s, vtx_t &x, vtx_t *dx, vtx_t *dx2 )
  {
      REAL_       u,v;
 
      u= s[0];
      v= s[1];

      x[0]=   x0[0];
      x[1]=   x0[1];
      x[2]=   x0[2];

      x[0]+=  l[0][0]*u;
      x[1]+=  l[0][1]*u;
      x[2]+=  l[0][2]*u;

      x[0]+=  l[1][0]*v;
      x[1]+=  l[1][1]*v;
      x[2]+=  l[1][2]*v;

      dx[0][0]=  l[0][0];
      dx[0][1]=  l[0][1];
      dx[0][2]=  l[0][2];
              
      dx[1][0]=  l[1][0];
      dx[1][1]=  l[1][1];
      dx[1][2]=  l[1][2];

      dx2[0][0]=  0;
      dx2[0][1]=  0;
      dx2[0][2]=  0;

      dx2[1][0]=  0;
      dx2[1][1]=  0;
      dx2[1][2]=  0;

      dx2[2][0]=  0;
      dx2[2][1]=  0;
      dx2[2][2]=  0;

      return; 

  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void plane_c::autov( char **arg )
  {
      v.insert(0,-BIG); v[0][0]= 0; v[0][1]=   0;
      v.insert(1,-BIG); v[1][0]= 1; v[1][1]=   0;
      v.insert(2,-BIG); v[2][0]= 0; v[2][1]=   1;
      v.insert(3,-BIG); v[3][0]= 1; v[3][1]=   1;

      patch_t pdum;
      p.insert(0,pdum);   p[0].v[0]=0; p[0].v[1]=1; p[0].v[2]=2; p[0].v[3]=3;
      ref(0);
      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -
 
   void plane_c::test( char *name )
  {
      surf_c::test( v[0],v[1],v[2],v[3], name );
      return;
  }

   bool plane_c::compare( surf_c *s )
  {
      REAL_ d=0;
//    assert( s->type() == surf_plane );
      if( s->type() != surf_plane ){ return false; };
      plane_c *w= (plane_c*)s;

      d= fmax( d, norminf( w->l[0]- l[0] ) );
      d= fmax( d, norminf( w->l[1]- l[1] ) );
      d= fmax( d, norminf( w->x0  - x0   ) );

      return (d<SURF_TOL);
  }
