
#include <misc.h>
#include <surf/implicit.h>

#include <cmath>

   // returns distance from point p to closest point on surface
   REAL_ surfimplicit_t::distance( vtx_t p0 )
  {
      bool approx=false;
      vtx_t      q;
      project( p0, q, approx );
      q = q-p0;
      return l2norm( q );
  }

   // calculate tangent vectors of surface at point closest to p0
   void surfimplicit_t::tangent( vtx_t p0, vtx_t *t, bool proj, bool approx )
  {
      INT_   i,j,k,m;
      INT_   dim=3;
      INT_   lwork=5*3;
      vtx_t  x,n,c[3];               // position, normal, curvature
      REAL_ tau[3*3];         // tangent tensor
      REAL_ wR[3], wI[3];     // real/imag parts of eigenvalues
      REAL_ vL[3*3], vR[3];   // left/right eigenvector arrays
      REAL_ wk[lwork];             // work array

      if( proj ){ project(  p0, x, approx ); }
      else{ x=p0; }
      proj=false;
      normal(    x, n, proj, approx );
      curvature( x, c, proj, approx );

      for( i=0; i<3; i++ )
     {
         for( j=0; j<3; j++ )
        {
            m=indx(i,j,3);
            tau[m] = c[i][j];

            for( k=0; k<3; k++ )
           {
               tau[m] += n[i]*n[k]*c[k][j];
           }
        }
     }

      char  jobL='V';
      char  jobR='N';
      INT_   info;
      INT_   ldvr=1;
           //  jobVL, jobVR,   N,   A,  LDA, WR, WI, VL, LDVL, VR,  LDVR, WORK,  LWORK,  INFO
      dgeev_( &jobL, &jobR, &dim, tau, &dim, wR, wI, vL, &dim, vR, &ldvr,   wk, &lwork, &info );
      assert( info==0 );

   // select largest eigenvalue eigenvectors
      INT_3 order;
      sort3( wR, order );

      for( i=0; i<2; i++ )
     {
         k=order[2-i];
         for( j=0; j<3; j++ )
        {
            t[i][j]=vL[j*3+k];
        }
         t[i] = cross( t[i], n );
         t[i]/= l2norm(t[i]);

     }
   // enforce consistent "positive" and "negative" side of surface
      vtx_t nt=cross(t[0], t[1] );
      if( nt*n < 0 ){ swap( t[0], t[1] ); }
  }

  // calculate normal of surface at point closest to p0
   void surfimplicit_t::normal( vtx_t p0, vtx_t &n, bool proj, bool approx )
  {
      INT_          i;
      vtx_t        x;
      REAL_     l=0;
      REAL_     d[3];

      if( proj ){ project( p0, x, approx ); }
      else{ x=p0; }
      dF( x, d );

      for( i=0; i<3; i++ ){ l+=d[i]*d[i]; }
      l=1./sqrt(l);

      for( i=0; i<3; i++ ){ n[i]=d[i]*l; }
  }

  // calculate curvature of surface at point closest to p0
   void surfimplicit_t::curvature( vtx_t p0, vtx_t *c, bool proj, bool approx )
  {
      vtx_t   x;
      REAL_  h[3*3];

      if( proj ){ project( p0, x, approx ); }
      else{ x=p0; }
      d2F( x, h );

      INT_ i,j;
      for( i=0; i<3; i++ )
     {
         for( j=0; j<3; j++ )
        {
            c[i][j] = h[indx(i,j,3)];
        }
     }
  }

  // q is projection of point p onto surface by newton iterations
  // if approx=true, will only do a single newton iteration. if approx=false, will iterate until convergence
   void surfimplicit_t::project( vtx_t p0, vtx_t &q0, bool approx )
  {
      REAL_ distance = fabs( F(p0) );
      if( distance < accuracy ){ q0=p0; return; }

      INT_           i=0;
      INT_           d=3+1;
      INT_           nrhs=1;
      INT_           info=0;
      REAL_        res=10000000.;             // newton iteration residual
      REAL_        rlx=0.5;               // relaxation factor
      REAL_        err=accuracy;    // convergence criteria

      INT_           ipiv[d];
      REAL_       jac[d*d];
      REAL_   nu[d],rhs[d];

      vtx_t        x0;
      REAL_ df[3];

   // initialise initial point x0 and solution vector nu
      x0=p0;
      for( i=0; i<3; i++ ){ nu[i]=p0[i]; }
      nu[3] = 0.;

      INT_ j=0;
      bool single_iteration=false;   //
   // newton iterations
      while( ( res>err ) && !single_iteration )
     {
      // construct rhs and jacobian
         newton_rhs( x0, nu, rhs, df );
         newton_jac( x0, nu, jac, df );

         for( i=0; i<d; i++ ){ ipiv[i]=0; }

      //          N   NRHS   A  LDA  IPIV   B  LDB,  info )
         dgesv_( &d, &nrhs, jac, &d, ipiv, rhs, &d, &info );
         assert( info==0 );

      // update solution vector
         for( i=0; i<d; i++ ){ nu[i]+=rlx*rhs[i]; }

      // calculate residual
         res = 0.;
         for( i=0; i<d; i++ ){ res+=rhs[i]*rhs[i]; }
         res=sqrt(res);

         rlx*=1.3;
         rlx=::fmin(rlx,1.0);

         j++;
         single_iteration = single_iteration || approx;
     }

   // assign to q
      for( i=0; i<3; i++ ){ q0[i]=nu[i]; }

      return;
  }

  // right hand side for projection newton iterations
   void surfimplicit_t::newton_rhs( vtx_t x0, REAL_ *nu, REAL_ *rhs, REAL_ *df )
  {
      INT_ i;

      vtx_t  x; // current position

      for( i=0; i<3; i++ ){ x[i]=nu[i]; }
      dF(  x, df );

      for( i=0; i<3; i++ )
     {
         rhs[i] = -( nu[3]*df[i] - ( x0[i] - nu[i] ) );
     }
      rhs[3] = -F( x );
  }

  // jacobian matrix for projection newton iterations
   void surfimplicit_t::newton_jac( vtx_t x0, REAL_ *nu, REAL_ *jac, REAL_ *df )
  {

      vtx_t              x;
      INT_         i,j,k,l;
      REAL_  h[3*3];   // hessian

      for( i=0; i<3; i++ ){ x[i]=nu[i]; }

      for( i=0; i<(3+1)*(3+1); i++ ){ jac[i]=0.; }

      d2F( x, h );

      for( i=0; i<3; i++ )
     {
         k = indx( 3, i,    3+1 );
         l = indx( i,    3, 3+1 );
         jac[ k ] = df[i];
         jac[ l ] = df[i];

         for( j=0; j<3; j++ )
        {
            k = indx( i,  j, 3+1 );
            l = indx( i,  j, 3   );
            jac[ k ]+= nu[3]*h[ l ];
        }

         k= indx( i,i, 3+1 );
         jac[ k ]+= 1.;
     }

      jac[(3+1)*(3+1)-1]=0.;

  }


