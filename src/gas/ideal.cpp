#  include <typd.h>
#  include <misc.h>
#  include <cmath>
#  include <cassert>
#  include <cstring>
#  include <gas/gas.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void idgas_t::init()
  { 
      gam=  1.4;
      wg= 0.0288;
      eps=  0.05;
//    nu= 1.8e-5;
      nu= 1.8e-3;
      pr=   0.7;

//    gam= g; 
      gam1= gam-1; 
      gam1=1./gam1; 

//    wg=w; 
      rg= runi/wg; 

// NONDIMENSIONALIZE
//    rg*= (300./10000.);

//    eps= e;
      assert( nvar() <= NVAR_ );

  };

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void idgas_t::varname( INT_ i, char *buf )
  {
      char names[5][2]={"u","v","w","t","p" };
      assert( i > -1 );
      assert( i < 5 );
      strncpy( buf,names[i], 2 );
  }
