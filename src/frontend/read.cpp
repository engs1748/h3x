
#  include <hmesh/hmesh.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:27:42 BST 2018
// Changes History
// Next Change(s)  -

   void read( INT_ l, INT_ m, char **arg, void *data )
  {
      FILE          *f;

      hmesh_t       *var;

      var=(((shmesh_t*)data)->msh)+l;

      assert( m == 1 );

      f= fopen( arg[0],"r" );
      assert( f );
      var->fread( f );
      var->trimb();
      fclose( f );

  }
