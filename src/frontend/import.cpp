
#  include <hmesh/hmesh.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:27:17 BST 2018
// Changes History -
// Next Change(s)  -

   void import( INT_ l, INT_ m, char **arg, void *data )
  {
      char           name[128];
      FILE          *f;

      hmesh_t       *var;

      assert( m == 1 );
      
      var=(((shmesh_t*)data)->msh)+l;

      sprintf( arg[0],"%s", name );
      f= fopen( name,"r" );
      var->read( f );
      fclose( f );

  }
