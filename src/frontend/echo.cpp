
#  include <hmesh/hmesh.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:24:12 BST 2018
// Changes History -
// Next Change(s)  -

   void echo( INT_ l, INT_ m, char **arg, void *data )
  {

      for( INT_ j=0;j<m;j++ )
     {
         printf( "%s ", arg[j] );
     }
      printf( "\n" );
      return;
  }

