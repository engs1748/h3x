#  include <hmesh/hmesh.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:26:41 BST 2018
// Changes History
// Next Change(s)  -

   void sbranch( INT_ l, INT_ m, char **arg, void *data )
  {
      INT_           k;

      hmesh_t       *var;
      REAL_3          v;
      REAL_           d;

      INT_           i0;
      INT_           flag;
      bool           b0;
      dir_t          a0;
      INT_         mask[6]={ -1,-1,-1, -1,-1,-1 };

      var=((hmesh_t*)data)+l;
//    assert( m == 3 );

      sscanf( arg[ 0],"%d", &i0 ); b0=(i0>0);
      sscanf( arg[ 1],"%d", &k  );

      sscanf( arg[ 2],"%lf", v+0 );             //direction
      sscanf( arg[ 3],"%lf", v+1 );             //direction
      sscanf( arg[ 4],"%lf", v+2 );             //direction
      sscanf( arg[ 5],"%lf",&d   );             //distance

      v[0]*= d;
      v[1]*= d;
      v[2]*= d;

      sscanf( arg[ 6],"%d", &flag );
      sscanf( arg[ 7],"%d", mask+0  );
      if( flag == 1 )
     {
         sscanf( arg[ 8],"%d", mask+1  );
         sscanf( arg[ 9],"%d", mask+2  );
         sscanf( arg[10],"%d", mask+3  );
         sscanf( arg[11],"%d", mask+4  );
     }

      i0= abs(i0); i0--; assert( i0>-1 && i0<3 );

      a0= dir_t(b0,i0); 

      var->sbranch( v, k,a0,mask,(flag==1) );

      mask[0]=-1;
      mask[1]=-1;
      mask[2]=-1;
      mask[3]=-1;
      mask[4]=-1;
      mask[5]=-1;
      var->del( k,mask );

  }

   void sbpos( INT_ l, INT_ m, char **arg, void *data )
  {
      INT_           k;

      hmesh_t       *var;
      INT_            g;
      REAL_           x,y,z,d;
      REAL_3          v;
      INT_           i0;
      INT_           flag;
      dir_t          a0;
      INT_         mask[6]={ -1,-1,-1, -1,-1,-1 };

      var=((hmesh_t*)data)+l;
//    assert( m == 5 );

      sscanf( arg[0],"%d", &g );
      sscanf( arg[1],"%lf",&x  );
      sscanf( arg[2],"%lf",&y  );
      sscanf( arg[3],"%lf",&z  );

      sscanf( arg[4],"%lf", v+0 );             //direction
      sscanf( arg[5],"%lf", v+1 );             //direction
      sscanf( arg[6],"%lf", v+2 );             //direction
      sscanf( arg[7],"%lf",&d   );             //distance

      v[0]*= d;
      v[1]*= d;
      v[2]*= d;

      sscanf( arg[8],"%d", &flag );
      sscanf( arg[9],"%d", mask+0  );
      if( flag == 1 )
     {
         sscanf( arg[10],"%d", mask+1  );
         sscanf( arg[11],"%d", mask+2  );
         sscanf( arg[12],"%d", mask+3  );
         sscanf( arg[13],"%d", mask+4  );
     }


      INT_ q=  var->search( g, vtx_t(x,y,z) );
      k= var->bz[g].bo[q].h;
      i0= var->bz[g].bo[q].q;
      a0= dir_t(i0%2,i0/2); 

      k= var->hx[k].id;
      var->sbranch( v, k,a0,mask,(flag==1) );

      mask[0]=-1;
      mask[1]=-1;
      mask[2]=-1;
      mask[3]=-1;
      mask[4]=-1;
      mask[5]=-1;
      var->del( k,mask );

  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:26:12 BST 2018
// Changes History -
// Next Change(s)  -

   void fbranch( INT_ l, INT_ m, char **arg, void *data )
  {
      INT_           k;
      INT_           i0;
      bool           b0;

      REAL_3         v;
      REAL_          d;
      hmesh_t       *var;
      INT_         mask[6]={ -1,-1,-1,-1,-1,-1 };

      dir_t          a0;

      var=((hmesh_t*)data)+l;
//    assert( m == 7 );

      sscanf( arg[ 0],"%d", &i0 ); b0=(i0>0);
      sscanf( arg[ 1],"%d", &k  );

      sscanf( arg[ 2],"%lf", v+0 );             //direction
      sscanf( arg[ 3],"%lf", v+1 );             //direction
      sscanf( arg[ 4],"%lf", v+2 );             //direction
      sscanf( arg[ 5],"%lf",&d   );             //distance


      sscanf( arg[ 6],"%d", mask+0 );
      sscanf( arg[ 7],"%d", mask+1 );
      sscanf( arg[ 8],"%d", mask+2 );

      sscanf( arg[ 9],"%d", mask+3 );
      sscanf( arg[10],"%d", mask+4 );

      i0= abs(i0); i0--; assert( i0>-1 && i0<3 ); 
      a0= dir_t(b0,i0); 

      assert( mask[0] > -1 ); 
      assert( mask[1] > -1 ); 
      assert( mask[2] > -1 ); 
      assert( mask[3] > -1 ); 
      assert( mask[4] > -1 ); 

      var->fbranch( v, k,a0,mask );
      var->del( k,mask );

      return;
  }

// Author          Joshua Hope-Collins <joshua.hope-collins@eng.ox.ac.uk>
// Created         Wed 31 Oct 13:00:00 GMT 2018
// Changes History -
// Next Change(s)  -

   void slot( INT_ l, INT_ m, char **arg, void *data )
  {

   INT_     n;              // width of slot
   INT_     k;              // hex to start
   INT_     b0, b1, b2, b3; // boundary groups
   INT_     i0, i1, i2;     // dummy for directions
   bool     d0, d1, d2;     // dummy for directions

   dir_t     a0, a1, a2;    // directions of laidback branch
   frme_t    f0;            // frame for a0-2
   hmesh_t  *var;           // mesh l

   var =((hmesh_t*)data)+l;
   assert( m == 9 );

   sscanf( arg[0],"%d", &i0 ); d0=(i0>0);  // direction out of hex
   sscanf( arg[1],"%d", &i1 ); d1=(i1>0);  // direction into   slot
   sscanf( arg[2],"%d", &i2 ); d2=(i2>0);  // direction across slot
   sscanf( arg[3],"%d", &k  );             //starting hex
   sscanf( arg[4],"%d", &n  );             //starting hex
   sscanf( arg[5],"%d", &b0 );             //boundary group
   sscanf( arg[6],"%d", &b1 );             //boundary group
   sscanf( arg[7],"%d", &b2 );             //boundary group
   sscanf( arg[8],"%d", &b3 );             //boundary group

   i0 = abs(i0); i0--; assert( i0>-1 && i0<3 );
   i1 = abs(i1); i1--; assert( i1>-1 && i1<3 );
   i2 = abs(i2); i2--; assert( i2>-1 && i2<3 );

   a0 = dir_t(d0,i0);
   a1 = dir_t(d1,i1);
   a2 = dir_t(d2,i2);

   f0[0] = a0;
   f0[1] = a1;
   f0[2] = a2;

   var->slot( k, f0, n, b0, b1, b2, b3 );

  }


// Author          Joshua Hope-Collins <joshua.hope-collins@eng.ox.ac.uk>
// Created         Fri 16 Nov 14:16:00 GMT 2018
// Changes History -
// Next Change(s)  -

   void lbranch( INT_ l, INT_ m, char **arg, void *data )
  {
   INT_  k;       // starting hex
   INT_  i0, i1;  // dummy for directions
   bool  d0, d1;  // dummy for directions
   INT_  b0, b1, b2, b3, b4;  // boundary groups of hole mouth and surface
   INT_  mask[5];

   dir_t    a0, a1;
   frme_t   f0;
   hmesh_t  *var;

   var = ((hmesh_t*)data)+l;
   assert( m == 8 );

   sscanf( arg[0],"%d", &i0 ); d0=(i0>0);  // direction out of hex
   sscanf( arg[1],"%d", &i1 ); d1=(i1>0);  // direction into laidback hole
   sscanf( arg[2],"%d", &k  );             // starting hex
   sscanf( arg[3],"%d", &b0 );             //boundary group
   sscanf( arg[4],"%d", &b1 );             //boundary group
   sscanf( arg[5],"%d", &b2 );             //boundary group
   sscanf( arg[6],"%d", &b3 );             //boundary group
   sscanf( arg[7],"%d", &b4 );             //boundary group

   mask[0]=b0;
   mask[1]=b1;
   mask[2]=b2;
   mask[3]=b3;
   mask[4]=b4;

   i0 = abs(i0); i0--; assert( i0>-1 && i0<3 );
   i1 = abs(i1); i1--; assert( i1>-1 && i1<3 );

   a0 = dir_t(d0,i0);
   a1 = dir_t(d1,i1);

   f0[0] = a0;
   f0[1] = a1;
   f0[2] = vec( a0, a1 );

   var->lbranch1( k, f0, mask );

   return;

  }

   void zbranch( INT_ l, INT_ m, char **arg, void *data )
  {
      INT_           k;

      hmesh_t       *var;
      REAL_3          v;
      REAL_           d;

      INT_           i0,i1;
      INT_           flag;
      bool           b0,b1;
      dir_t          a0,a1;
      INT_         mask[6]={ -1,-1,-1, -1,-1,-1 };

      var=((hmesh_t*)data)+l;

      sscanf( arg[ 0],"%d", &i0 ); b0=(i0>0);
      sscanf( arg[ 1],"%d", &i1 ); b1=(i1>0);
      sscanf( arg[ 2],"%d", &k  );

      sscanf( arg[ 3],"%lf", v+0 );             //direction
      sscanf( arg[ 4],"%lf", v+1 );             //direction
      sscanf( arg[ 5],"%lf", v+2 );             //direction
      sscanf( arg[ 6],"%lf",&d   );             //distance

      v[0]*= d;
      v[1]*= d;
      v[2]*= d;

      sscanf( arg[ 7],"%d", &flag   );

      sscanf( arg[ 8],"%d", mask+0  );
      sscanf( arg[ 9],"%d", mask+1  );
      sscanf( arg[10],"%d", mask+2  );
      sscanf( arg[11],"%d", mask+3  );
      sscanf( arg[12],"%d", mask+4  );

      i0= abs(i0); i0--; assert( i0>-1 && i0<3 );
      i1= abs(i1); i1--; assert( i1>-1 && i1<3 );

      a0= dir_t(b0,i0); 
      a1= dir_t(b1,i1); 

      var->zbranch( v, k,a0,a1,mask, (flag==1) );

  }
