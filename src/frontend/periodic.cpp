

#  include <hmesh/hmesh.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:22:50 BST 2018
// Changes History -
// Next Change(s)  -


   void periodic( INT_ l, INT_ m, char **arg, void *data )
  {

      hmesh_t              *var;
      INT_            i,j,k,h,n;

      var=(((shmesh_t*)data)->msh)+l;

      sscanf( arg[0],"%d", &i ); 
      sscanf( arg[1],"%d", &j );
      sscanf( arg[2],"%d", &k );
      sscanf( arg[3],"%d", &n );

      assert( i > -1 && i < var->fr.n );
 
      h= (var->pr.n)++; var->pr.resize(-1);

      var->pr[h][0]= i;
      var->pr[h][1]= j;
      var->pr[h][2]= k;
      var->pr[h][3]= n;

      return;
  }
