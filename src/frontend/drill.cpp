#  include <q3x/q3x.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:23:32 BST 2018
// Changes History -
// Next Change(s)  -

   void drill( INT_ l, INT_ m, char **arg, void *data)
  {
      hmesh_t                  *var;

      var=((hmesh_t*)data)+l;


      frme_t                     a0;
      wsize_t<dir_t,3,DLEN_H>     c(var->hx.n,ddum);
      INT_                    i0,i1;
      bool                    b0,b1;

      INT_                    i,j,k;

      INT_                        n;
      INT_                     mask[6];
      INT_                      wrk[6];
      VINT_                    list;

      VINT_2                   l1,l2;


      assert( m==9 );

      sscanf( arg[0],"%d", &i0 ); b0=(i0>0);
      sscanf( arg[1],"%d", &i1 ); b1=(i1>0);

      i0= abs(i0); i0--;
      i1= abs(i1); i1--;

      assert( i0 > -1 && i0 <3 );
      assert( i1 > -1 && i1 <3 );

      sscanf( arg[2],"%d", &k  );
      sscanf( arg[3],"%d", mask+0  );
      sscanf( arg[4],"%d", mask+1  );
      sscanf( arg[5],"%d", mask+2  );
      sscanf( arg[6],"%d", mask+3  );
      sscanf( arg[7],"%d", mask+4  );
      sscanf( arg[8],"%d", mask+5  );

      a0[0]= dir_t(b0,i0); 
      a0[1]= dir_t(b1,i1); 

//    var->point( 43,l1,l2 ); 
/*    printf( "\n" );
      for( INT_ m=0;m<l1.n;m++ )
     {
         printf( "%d %d %d \n", m,l1[m][0],l1[m][1] );
     }
      printf( "\n" );
      for( INT_ m=0;m<l2.n;m++ )
     {
         printf( "%d %d %d\n", m,l2[m][0],l2[m][1] );
     }*/
//    exit(0);

      var->drill( k,a0, list,c.data() );

      n=list.n;
      if( list[n-1]== list[0] && n > 1){ list.n--; };

       

      for( i=0;i<list.n;i++ ){ j= list[i]; list[i]= var->hx[j].id; };
      for( i=0;i<list.n;i++ )
     {
         j= list[i];
         k= var->indx[j];
/*       n= var->hx[k].face(!c[i][0]); wrk[n]= mask[0];
         n= var->hx[k].face( c[i][0]); wrk[n]= mask[1];
         n= var->hx[k].face(!c[i][1]); wrk[n]= mask[2];
         n= var->hx[k].face( c[i][1]); wrk[n]= mask[3];
         n= var->hx[k].face(!c[i][2]); wrk[n]= mask[4];
         n= var->hx[k].face( c[i][2]); wrk[n]= mask[5];*/
         n= face(!c[i][0]); wrk[n]= mask[0];
         n= face( c[i][0]); wrk[n]= mask[1];
         n= face(!c[i][1]); wrk[n]= mask[2];
         n= face( c[i][1]); wrk[n]= mask[3];
         n= face(!c[i][2]); wrk[n]= mask[4];
         n= face( c[i][2]); wrk[n]= mask[5];
         var->del( j,wrk );
     }


  }
