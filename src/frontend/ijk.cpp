#  include <q3x/q3x.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:21:53 BST 2018
// Changes History
// Next Change(s)  -

   void ijk( INT_ l, INT_ m, char **arg, void *data )
  {

      hmesh_t       *var;
      vsurf_t       *geo;
      INT_           dim[3]={0,0,0};

      INT_             j=0;
      INT_             i=0;
      INT_2        mask[6]={ {-1,-1},{-1,-1},{-1,-1},
                             {-1,-1},{-1,-1},{-1,-1} };


      var= (((shmesh_t*)data)->msh)+l;
      geo= (((shmesh_t*)data)->geo)+l;

      assert( m>=3 );
      for( i=0;i<3;i++ ){ sscanf( arg[i],"%d",dim+i ); };

 
      j= 0;
      if( m >= 13 )
     {
         for( INT_ k=0;k<6;k++ )
        { 
            sscanf( arg[i++],"%d",mask[j]+0 ); 
            sscanf( arg[i++],"%d",mask[j]+1 ); 

            j++;
        };
     }
      var->ijk( dim[0],dim[1],dim[2], mask,*geo ); 
   }
