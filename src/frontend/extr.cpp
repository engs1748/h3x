
#  include <q3x/q3x.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:23:32 BST 2018
// Changes History -
// Next Change(s)  -

   void ex0( INT_ l, INT_ m, char **arg, void *data)
  {

      hmesh_t                  *var;
      var=((hmesh_t*)data)+l;

      INT_                      i0,i1;
      bool                      b0,b1;
      dir_t                     d0,d1;
      INT_                      k0,l0,m0;
      INT_                      mask[6]={-1,-1,-1,-1,-1,-1};

      REAL_3                    v;

      sscanf( arg[ 0],"%d", &i0 ); b0=(i0>0);
      sscanf( arg[ 1],"%d", &i1 ); b1=(i1>0);
      sscanf( arg[ 2],"%d", &k0 );
      sscanf( arg[ 3],"%d", &l0 );
      sscanf( arg[ 4],"%d", &m0 );
      sscanf( arg[ 5],"%lf", v+0 );
      sscanf( arg[ 6],"%lf", v+1 );
      sscanf( arg[ 7],"%lf", v+2 );
      sscanf( arg[ 8],"%d", mask+0 );
      sscanf( arg[ 9],"%d", mask+1 );
      sscanf( arg[10],"%d", mask+2 );
      sscanf( arg[11],"%d", mask+3 );
      sscanf( arg[12],"%d", mask+4 );

      i0= abs(i0); i0--;
      i1= abs(i1); i1--;

      d0= dir_t(b0,i0); 
      d1= dir_t(b1,i1); 

      var->ex0( d0,d1, k0,l0,m0, v, mask );
      
  }

   void lref( INT_ l, INT_ m, char **arg, void *data)
  {

      hmesh_t                  *var;
      var=((hmesh_t*)data)+l;

      INT_                      i0,i1;
      bool                      b0,b1;
      dir_t                     d0,d1;
      INT_                      k0,l0,m0;

      sscanf( arg[ 0],"%d", &i0 ); b0=(i0>0);
      sscanf( arg[ 1],"%d", &i1 ); b1=(i1>0);
      sscanf( arg[ 2],"%d", &k0 );
      sscanf( arg[ 3],"%d", &l0 );
      sscanf( arg[ 4],"%d", &m0 );

      i0= abs(i0); i0--;
      i1= abs(i1); i1--;

      d0= dir_t(b0,i0); 
      d1= dir_t(b1,i1); 

      var->lref( d0,d1, k0,l0,m0 );
      
  }

