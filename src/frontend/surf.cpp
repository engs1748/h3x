
#  include <hmesh/hmesh.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:22:50 BST 2018
// Changes History -
// Next Change(s)  -


   void surf( INT_ l, INT_ m, char **arg, void *data )
  {

      vsurf_t        *var;
      INT_            k;

      surf_c         *wrk=NULL;

      var= (((shmesh_t*)data)->geo)+l;

      sscanf( arg[0],"%d", &k ); 
      newsurf( k,&wrk );
      wrk->read( arg+1 );

      k= (*var).append(NULL);
    (*var)[k]= wrk;

/*    char name[16];
      sprintf( name,"surface.%03d.dat", k );
      wrk->test( name );*/
    
      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:22:50 BST 2018
// Changes History -
// Next Change(s)  -


   void vertex( INT_ l, INT_ m, char **arg, void *data )
  {

      vsurf_t        *var;
      INT_            k;

      var= (((shmesh_t*)data)->geo)+l;
      assert( m > 1 );

      sscanf( arg[0],"%d", &k ); 

    (*var)[k]->vtx( arg+1 );

      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:22:50 BST 2018
// Changes History -
// Next Change(s)  -


   void patch( INT_ l, INT_ m, char **arg, void *data )
  {

      vsurf_t        *var;
      INT_            k;

      var= (((shmesh_t*)data)->geo)+l;
      assert( m > 1 );

      sscanf( arg[0],"%d", &k ); 

    (*var)[k]->patch( arg+1 );

      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:22:50 BST 2018
// Changes History -
// Next Change(s)  -


   void map( INT_ l, INT_ m, char **arg, void *data )
  {

      hmesh_t        *var= (((shmesh_t*)data)->msh)+l;
      vsurf_t        *geo= (((shmesh_t*)data)->geo)+l;

/*    for( INT_ j=0;j<var->nb;j++ )
     {
         var->bmap( j );
     }*/
      var->smap( *geo );

      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:22:50 BST 2018
// Changes History -
// Next Change(s)  -

   void geo( INT_ l, INT_ m, char **arg, void *data )
  {

      hmesh_t        *var=(((shmesh_t*)data)->msh)+l;
      vsurf_t        *geo=(((shmesh_t*)data)->geo)+l;
      INT_ g,s,p,b;
      INT_ flag;

      sscanf( arg[0],"%d", &g ); 
      sscanf( arg[1],"%d", &s ); 
      sscanf( arg[2],"%d", &p ); 
      sscanf( arg[3],"%d", &b ); 
      sscanf( arg[4],"%d", &flag ); 
      var->geo( g,s,p,b, *geo );
      var->bmap( g,(flag==1) );

      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:22:50 BST 2018
// Changes History -
// Next Change(s)  -

   void sze( INT_ l, INT_ m, char **arg, void *data )
  {

      hmesh_t        *var=((hmesh_t*)data)+l;
      INT_  g;
      REAL_ b;

      sscanf( arg[0],"%d", &g ); 
      sscanf( arg[1],"%lf", &b ); 
      var->bz[g].b= b;

      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:22:50 BST 2018
// Changes History -
// Next Change(s)  -

   void sct( INT_ l, INT_ m, char **arg, void *data )
  {

      hmesh_t        *var=(((shmesh_t*)data)->msh)+l;
      INT_  b;
      INT_  k,i,j;
      REAL_ d;

      sscanf( arg[0],"%d", &k ); 
      sscanf( arg[1],"%lf", &d ); 
      var->sc[k].d=d;
      i= 2;
      do
     {
         sscanf( arg[i++],"%d", &b ); 
         if( b > -1 ) 
        {
            bool flag=false;
            for( j=0;j<var->sc[k].b.n;j++ )
           {
               flag= ( var->sc[k].b[j]==b );
               if( flag ){ break; };
           }
            if( !flag )
           {
               j= var->sc[k].b.append(-1);
               var->sc[k].b[j]= b;
           }
        }
         
     }while( i < m);

      var->sctrl( k );

      return;
  }

   void box( INT_ l, INT_ m, char **arg, void *data )
  {

      vsurf_t        *var;
      INT_            k;

      REAL_3          x0,x1,x2,x3,x4,x5,x6,x7,x8;

      surf_c         *wrk=NULL;

      char          *line[24];
      char           name[3]="xx";

      var= (((shmesh_t*)data)->geo)+l;

      sscanf( arg[0],"%lf %lf %lf", x0+0,x0+1,x0+2 ); 
      sscanf( arg[1],"%lf %lf %lf", x1+0,x1+1,x1+2 ); 
      sscanf( arg[2],"%lf %lf %lf", x2+0,x2+1,x2+2 ); 
      sscanf( arg[3],"%lf %lf %lf", x3+0,x3+1,x3+2 ); 
      sscanf( arg[4],"%lf %lf %lf", x4+0,x4+1,x4+2 ); 
      sscanf( arg[5],"%lf %lf %lf", x5+0,x5+1,x5+2 ); 
      sscanf( arg[6],"%lf %lf %lf", x6+0,x6+1,x6+2 ); 
      sscanf( arg[7],"%lf %lf %lf", x7+0,x7+1,x7+2 ); 
      sscanf( arg[8],"%lf %lf %lf", x8+0,x8+1,x8+2 ); 


      line[0]= name;

      line[1]= arg[0];
      line[2]= arg[2];
      line[3]= arg[4];
      line[4]= arg[6];
      newsurf( 2,&wrk ); 
      wrk->read( line );
      k= (*var).append(NULL);
    (*var)[k]= wrk;

      line[1]= arg[1];
      line[2]= arg[3];
      line[3]= arg[5];
      line[4]= arg[7];
      newsurf( 2,&wrk ); 
      wrk->read( line );
      k= (*var).append(NULL);
    (*var)[k]= wrk;

      line[1]= arg[0];
      line[2]= arg[1];
      line[3]= arg[4];
      line[4]= arg[5];
      newsurf( 2,&wrk ); 
      wrk->read( line );
      k= (*var).append(NULL);
    (*var)[k]= wrk;

      line[1]= arg[2];
      line[2]= arg[3];
      line[3]= arg[6];
      line[4]= arg[7];
      newsurf( 2,&wrk ); 
      wrk->read( line );
      k= (*var).append(NULL);
    (*var)[k]= wrk;

      line[1]= arg[0];
      line[2]= arg[1];
      line[3]= arg[2];
      line[4]= arg[3];
      newsurf( 2,&wrk ); 
      wrk->read( line );
      k= (*var).append(NULL);
    (*var)[k]= wrk;

      line[1]= arg[4];
      line[2]= arg[5];
      line[3]= arg[6];
      line[4]= arg[7];
      newsurf( 2,&wrk ); 
      wrk->read( line );
      k= (*var).append(NULL);
    (*var)[k]= wrk;

/*    char name[16];
      sprintf( name,"surface.%03d.dat", k );
      wrk->test( name );*/
    
      return;
  }

   void film( INT_ l, INT_ m, char **arg, void *data )
  {

      vsurf_t        *var;
      INT_            k;

      surf_c         *wrk=NULL;

      var= (((shmesh_t*)data)->geo)+l;

      sscanf( arg[0],"%d", &k ); 
      newsurf( k,&wrk );
      wrk->read( arg+1 );

      k= (*var).append(NULL);
    (*var)[k]= wrk;

/*    char name[16];
      sprintf( name,"surface.%03d.dat", k );
      wrk->test( name );*/
    
      return;
  }
