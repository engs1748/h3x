# include <hmesh/hmesh.h>

   void vtk( INT_ l, INT_ m, char **arg, void *data )
  {
      hmesh_t  *var;

      var=((hmesh_t*)data)+l;
      assert( m == 1 );
      var->vtk( arg[0] );

      return;
  }
