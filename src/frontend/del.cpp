#  include <q3x/q3x.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:29:09 BST 2018
// Changes History
// Next Change(s)  -

   void del( INT_ l, INT_ m, char **arg, void *data)
  {

      hmesh_t       *var;

      INT_            k;
      INT_         mask[6];

      assert( m == 7 );

      var=((hmesh_t*)data)+l;

      sscanf( arg[0],"%d", &k ); 
      sscanf( arg[1],"%d", mask+0 );
      sscanf( arg[2],"%d", mask+1 );
      sscanf( arg[3],"%d", mask+2 );
      sscanf( arg[4],"%d", mask+3 );
      sscanf( arg[5],"%d", mask+4 );
      sscanf( arg[6],"%d", mask+5 );

      var->del( k,mask );

  }

   void bxdel( INT_ l, INT_ m, char **arg, void *data)
  {

      hmesh_t       *var;

      INT_             k;
      VINT_         list;
      REAL_3       x0,x1;

      INT_         mask[6];

      var=((hmesh_t*)data)+l;

      sscanf( arg[ 0],"%lf", x0+0 ); 
      sscanf( arg[ 1],"%lf", x1+0 ); 
      sscanf( arg[ 2],"%lf", x0+1 ); 
      sscanf( arg[ 3],"%lf", x1+1 ); 
      sscanf( arg[ 4],"%lf", x0+2 ); 
      sscanf( arg[ 5],"%lf", x1+2 ); 

      sscanf( arg[ 6],"%d", mask+0 );
      sscanf( arg[ 7],"%d", mask+1 );
      sscanf( arg[ 8],"%d", mask+2 );
      sscanf( arg[ 9],"%d", mask+3 );
      sscanf( arg[10],"%d", mask+4 );
      sscanf( arg[11],"%d", mask+5 );

      var->box( x0,x1, list );

      for( k=0;k<list.n;k++ )
     {
         var->del( list[k],mask );
     }

  }

   void rndel( INT_ l, INT_ m, char **arg, void *data)
  {

      hmesh_t       *var;

      INT_           k;
      VINT_         list;
      INT_       ist,ien;
      INT_          mask[6];

      var=((hmesh_t*)data)+l;

      sscanf( arg[ 0],"%d", &ist  ); 
      sscanf( arg[ 1],"%d", &ien  ); 
      sscanf( arg[ 2],"%d",  mask+0 );
      sscanf( arg[ 3],"%d",  mask+1 );
      sscanf( arg[ 4],"%d",  mask+2 );
      sscanf( arg[ 5],"%d",  mask+3 );
      sscanf( arg[ 6],"%d",  mask+4 );
      sscanf( arg[ 7],"%d",  mask+5 );

      if( ien == -1 && ist == -1 )
     {
         ist=0;
         ien= var->indx.n;
     }
      else
     {
         assert( ist > -1 );
         assert( ien < var->indx.n );
     }

      for( k=ist;k<ien;k++ )
     {
         var->del( k,mask );
     }

  }
