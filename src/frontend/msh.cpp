#  include <msh.h>

   void msh( INT_ l, INT_ m, char **arg, void *data )
  {

      hmesh_t                     *var;

      INT_                         n=0;
      buf_t                        buf[N];

      msh_t                        wrk;

      var=((hmesh_t*)data)+l;

      FILE *f=fopen( arg[0],"r" );
      parse( f, n,buf );
      fclose( f );
 
      wrk.read( n,buf );
      wrk.hexs(); 

      wrk.build( *var ); 

/*    char name[16];
      sprintf( name,"surface.%03d.dat", k );
      wrk->test( name );*/
    
      return;
  }

