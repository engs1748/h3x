
#  include <hmesh/hmesh.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:24:12 BST 2018
// Changes History -
// Next Change(s)  -

   void tunnel( INT_ l, INT_ m, char **arg, void *data )
  {

      hmesh_t        *var;

      frme_t           a0={ dir_t(0,1,0),dir_t(-1,0,0),dir_t(0,0,-1) };    

      VINT_        list;
      

      INT_        i,j,k;
      INT_        i0,i1;
      bool        b0,b1;
      INT_        mask[6]={ -1,-1,-1, -1,-1,-1 };

      var=(((shmesh_t*)data)->msh)+l;
      assert( m == 3 );

      sscanf( arg[0],"%d", &i0 ); b0=(i0>0);
      sscanf( arg[1],"%d", &i1 ); b1=(i1>0);
      sscanf( arg[2],"%d", &k  );

      i0= abs(i0); i0--;
      i1= abs(i1); i1--;

      assert( i0 > -1 && i0 < 3 );
      assert( i1 > -1 && i1 < 3 );

      a0[1]= dir_t(b0,i0); 
      a0[2]= dir_t(b1,i1); 

      var->tunnel( k,a0, list );

      for( i=0;i<list.n;i++ ){ j= list[i]; list[i]= var->hx[j].id; };
      for( i=0;i<list.n;i++ )
     {
         j= list[i];
         var->del( j,mask );
     }

      return;
  }

