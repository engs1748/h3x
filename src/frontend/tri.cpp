#  include <hmesh/hmesh.h>
#  include <tri.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:28:12 BST 2018
// Changes History -
// Next Change(s)  -

   void tri( INT_ l, INT_ m, char **arg, void *data )
  {
//    hmesh_t       *var;
      vneig_t       *wrk=NULL, dum;

//    var=((hmesh_t*)data)+l;

      INT_             n;
      INT_           j,k;

      tri_t          t[8];

      FILE *f=NULL;
      FILE *g=NULL;
      assert( m == 1 );

      f= fopen( arg[0],"r" );

      fscanf( f,"%d",&n );
      for( INT_ k=0;k<n;k++ )
     { 
         t[k].read(f); 
     }
      fclose(f);

      g= fopen( "c3.dat","w" );
      for( k=0;k<n;k++ )
     { 
         wrk= (vneig_t*)malloc((t[k].m)*sizeof(vneig_t));
         for( j=0;j<t[k].m;j++ ){ wrk[j]=dum; };

         t[k].neig( wrk );
         t[k].edge( wrk );
         t[k].corners();
         t[k].check( g );
         
         free(wrk); wrk=NULL;
     };
      fclose(g);

  }
