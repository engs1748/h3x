
#  include <q3x/q3x.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:21:53 BST 2018
// Changes History
// Next Change(s)  -

   void frame( INT_ l, INT_ m, char **arg, void *data )
  {

      hmesh_t         *var;
      INT_             k;

      var=(((shmesh_t*)data)->msh)+l;

      assert( m>1 );
      sscanf( arg[0],"%d",&k );
      var->frame( k, arg+1 ); 
   }

