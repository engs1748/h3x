#  include <hmesh/hmesh.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:22:50 BST 2018
// Changes History -
// Next Change(s)  -


   void corner( INT_ l, INT_ m, char **arg, void *data )
  {

      frme_t          a0;

      INT_            k;
      INT_           llst[3];
      VINT_          list;
      bool          b0,b1;
      INT_          i0,i1;

      hmesh_t        *var;
      
      var=((hmesh_t*)data)+l;
      assert( m == 3 );

      VINT_2                mark(var->vt.n,-1);

      sscanf( arg[0],"%d", &i0 ); b0=(i0<0);
      sscanf( arg[1],"%d", &i1 ); b1=(i1<0);

      i0= abs(i0); i0--;
      i1= abs(i1); i1--;

      assert( i0 > -1 && i0 <3 );
      assert( i1 > -1 && i1 <3 );

      sscanf( arg[2],"%d", &k  );

      a0[0]= dir_t(b0,i0); 
      a0[1]= dir_t(b1,i1); 

      var->corner( k,a0, llst,list ); 
      assert( llst[2]==list.n );

      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:22:50 BST 2018
// Changes History -
// Next Change(s)  -


   void collapse( INT_ l, INT_ m, char **arg, void *data )
  {

      dir_t           a0;

      INT_            k;
      VINT_          list;
      bool          b0;
      INT_          i0;

      hmesh_t        *var;
      
      var=((hmesh_t*)data)+l;
//    assert( m == 2 );

      sscanf( arg[0],"%d", &i0 ); b0=(i0<0);

      i0= abs(i0); i0--;

      assert( i0 > -1 && i0 <3 );

      sscanf( arg[1],"%d", &k  );

      a0= dir_t(b0,i0); 

      var->collapse( k,a0, list);

/*    for( i=0;i<list.n;i++ ){ j= list[i]; list[i]= var->hx[j].id; };
      for( i=0;i<list.n;i++ )
     {
         j= list[i];
         printf( "trying to remove hex %d\n", j );
         var->del( j,mask );
     }*/


  }
   void split( INT_ l, INT_ m, char **arg, void *data )
  {

      dir_t d0;
      INT_  i0, j0;

      bool  b0=true;

      hmesh_t  *var;

      var=((hmesh_t*)data)+l;

      assert( m== 2 );

      sscanf( arg[0], "%d", &i0 );

      sscanf( arg[1], "%d", &j0 );
      j0=abs(j0)-1;

      d0= dir_t( b0, j0 );

      var->split( i0, d0 );

   return;
  }
