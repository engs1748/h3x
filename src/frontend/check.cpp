

#  include <hmesh/hmesh.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:22:50 BST 2018
// Changes History -
// Next Change(s)  -


   void check( INT_ l, INT_ m, char **arg, void *data )
  {

      hmesh_t        *var;

      var=(((shmesh_t*)data)->msh)+l;
      var->check();
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:22:50 BST 2018
// Changes History -
// Next Change(s)  -


   void dump( INT_ l, INT_ m, char **arg, void *data )
  {

      hmesh_t        *var=(((shmesh_t*)data)->msh)+l;
      var->dump();
  }


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:22:50 BST 2018
// Changes History -
// Next Change(s)  -


   void tell( INT_ l, INT_ m, char **arg, void *data )
  {

      INT_            b;
      hmesh_t        *var;

      var=(((shmesh_t*)data)->msh)+l;

      printf( "\n" );
      printf( "--------------------------------------------\n" );
      printf( "hmesh %2d (%p) \n", l,data );
      printf( "--------------------------------------------\n" );
      printf( "No. of vertex locations:              %6d\n", var->vx.n );
      printf( "No. of vertices:                      %6d\n", var->vt.n );
      printf( "No. of hexahedra:                     %6d\n", var->hx.n );
      printf( "No. of boundary quads:                %6d\n", var->ng   );
      printf( "No. of quads:                         %6d\n", var->qv.n );
      printf( "No. of boundary groups:               %6d\n", var->nb   );
      for( b=0;b<var->nb;b++ )
     {
         if( var->bz[b].bo.n > 0 )
        {
            printf( "Group %4d (%4d %4d), no. of quads: %6d, no. of vertices %6d\n", b,var->bz[b].lbl[0],
                                                                  var->bz[b].lbl[1], 
                                                                  var->bz[b].bo.n,
                                                                  var->bz[b].bx.n );
        }
     }
      b= (MXGRP-1);
      printf( "No. of discarded quads:               %6d\n", var->bz[b].bo.n );
      INT_ n=0;;
      for( b=0;b<128;b++ )
     {
         if( var->sc[b].b.n > 0 ){ n++; };
     } 
      printf( "No. of size control groups            %6d\n", n         );
      for( b=0;b<128;b++ )
     {
         if( var->sc[b].b.n > 0 )
        {
            printf( "Group %4d (%9.3e),   no. of groups: %6d\n", b,var->sc[b].d,var->sc[b].b.n );
        }
     }
      printf( "--------------------------------------------\n" );
  }


