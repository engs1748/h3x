
#  include <hmesh/hmesh.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:28:46 BST 2018
// Changes History
// Next Change(s)  -

   void write( INT_ l, INT_ m, char **arg, void *data )
  {
      FILE          *f;
      hmesh_t       *var;
      vsurf_t       *geo;

      char          *buf;
      size_t         len;

      assert( m == 1 );
      
      var= (((shmesh_t*)data)->msh)+l;
      geo= (((shmesh_t*)data)->geo)+l;

      len= strlen(arg[0]);
      buf= (char*)malloc(len+5);

      memcpy(buf,arg[0],len+1);
      strcat(buf,".q3x");

      f= fopen( buf,"w" );
      var->fwrite( f );
      fclose( f );

      memcpy(buf,arg[0],len+1);
      strcat(buf,".g3x");

      f= fopen( buf,"w" );
      fwrite( *geo,f );
      fclose( f );

      free(buf);

      return;
  }


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:27:42 BST 2018
// Changes History
// Next Change(s)  -

   void read( INT_ l, INT_ m, char **arg, void *data )
  {
      FILE          *f;

      hmesh_t       *var;
      vsurf_t       *geo;

      char          *buf;
      size_t         len;

      var= (((shmesh_t*)data)->msh)+l;
      geo= (((shmesh_t*)data)->geo)+l;

      assert( m == 1 );

      var= (((shmesh_t*)data)->msh)+l;
      geo= (((shmesh_t*)data)->geo)+l;

      len= strlen(arg[0]);
      buf= (char*)malloc(len+5);

      memcpy(buf,arg[0],len+1);
      strcat(buf,".q3x");

      f= fopen( buf,"r" );
      var->fread( f );
      var->trimb();
      fclose( f );

      memcpy(buf,arg[0],len+1);
      strcat(buf,".g3x");

      f= fopen( buf,"r" );
      fread( *geo,f );
      fclose( f );

      free(buf);
  }
