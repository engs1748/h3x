
#  include <hmesh/hmesh.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:22:50 BST 2018
// Changes History -
// Next Change(s)  -


   void misc( INT_ l, INT_ m, char **arg, void *data )
  {

      hmesh_t        *var;
      vtx_t           p(-0.390748, 0.094750, 0.251311);
      
      var=((hmesh_t*)data)+l;

      printf("%d\n", var->search(9,p));
      
      return;

  }
