
#  include <hmesh/hmesh.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:22:50 BST 2018
// Changes History -
// Next Change(s)  -


   void group( INT_ l, INT_ m, char **arg, void *data )
  {

      hmesh_t          *var;
      INT_            i,j,k;


      var=(((shmesh_t*)data)->msh)+l;
      assert( m >= 3 );

      sscanf( arg[0],"%d", &i ); 
      sscanf( arg[1],"%d", &j );
      sscanf( arg[2],"%d", &k );

      assert( i > -1 );
      if( i < var->nb )
     {

/*       assert( var->bz[i].lbl[0] == -1 );
         assert( var->bz[i].lbl[1] == -1 );*/

         if( var->bz[i].lbl[0] > -1 )
        {
            printf( "Warnig: trying to reassign surface\n" );
        }
 
         var->bz[i].lbl[0]= j;
         var->bz[i].lbl[1]= k;
     }
      else
     {
         printf( "Warnig: trying to assign inexistent surface\n" );
     }

      return;
  }

   void maxgr( INT_ l,INT_ m, char **arg, void *data )
  {

      hmesh_t        *var=((hmesh_t*)data)+l;
      INT_            i;

      assert( m == 1 );
      sscanf( arg[0],"%d", &i ); 
      var->nb= max( var->nb,i );
      return;
  }

   void inter( INT_ l, INT_ m, char **arg, void *data )
  {

      INT_          a,b,c,d;
      INT_            s,p,r;
      REAL_               g;
      INT_                i;

      hmesh_t        *var= (((shmesh_t*)data)->msh)+l;
      vsurf_t        *geo= (((shmesh_t*)data)->geo)+l;

      sscanf( arg[0],"%d", &a ); 
      sscanf( arg[1],"%d", &b );
      sscanf( arg[2],"%d", &c );
      sscanf( arg[3],"%d", &d );

      sscanf( arg[4],"%d", &s );
      sscanf( arg[5],"%d", &p );
      sscanf( arg[6],"%d", &r );

      sscanf( arg[7],"%lf", &g );

      i= var->bi.append(ibdum);
      var->bi[i].z[0]= a;
      var->bi[i].z[1]= b;
      var->bi[i].z[2]= c;
      var->bi[i].z[3]= d;

      var->bi[i].s=    s;
      var->bi[i].p=    p;
      var->bi[i].r=    r;

      var->bi[i].d=    g;

      var->inter( i,*geo );

      return;
  }
