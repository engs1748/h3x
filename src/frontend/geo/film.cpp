#  include <cmath>
#  include <cstdlib>
#  include <cstdio>
#  include <cassert>

#  include <vtx.h>
#  include <spline/spline2.h>

   void film( REAL_ d, REAL_ h, INT_ &len, REAL_ *x, REAL_ *y, vtx_t *z,vtx_t*z2 )
  {
      INT_      i;

      INT_      n,m;
      INT_      l;

      REAL_     w;

      n= 12;

      w= 0.5*M_PI;
      w/= n;
      REAL_  dc= cos(w);
      REAL_  ds= sin(w);

      REAL_  c,s;

      l= 0;
      m=  (int)(2*n*d/(M_PI));

      c= 1;
      s= 0;
      for( i=0;i<n;i++ )
     {
         z[l++]= vtx_t(d+c,s,h);
         w= c;
         c= w*dc- s*ds;
         s= w*ds+ s*dc;
     }

      w= d/m;
      c= d;
      s= 1;
      for( i=0;i<2*m;i++ )
     {
         z[l++]= vtx_t(c,s,h);
         c-= w;
     }
      c= 0;
      s= 1;
      for( i=0;i<2*n;i++ )
     {
         z[l++]= vtx_t(-d+c,s,h);
         w= c;
         c= w*dc- s*ds;
         s= w*ds+ s*dc;
     }
      w= d/m;
      c=-d;
      s=-1;
      for( i=0;i<2*m;i++ )
     {
         z[l++]= vtx_t(c,s,h);
         c+= w;
     }
      c= 0;
      s=-1;
      for( i=0;i<n;i++ )
     {
         z[l++]= vtx_t(d+c,s,h);
         w= c;
         c= w*dc- s*ds;
         s= w*ds+ s*dc;
     }
      assert( 2*l < len );

      c= 1;
      s= 0;
      w= 2*M_PI;
      w/= l;
      dc= cos(w);
      ds= sin(w);
      for( i=0;i<l;i++ )
     {
         z[l+i]= vtx_t(c,s,0);
         w= c;
         c= w*dc- s*ds;
         s= w*ds+ s*dc;
     }
      len= l;

      for( i=0;i<l;i++ ){ x[i]= i; x[i]/= l; }
      spline( l,2, 1.,x,y, z,z2 );
  }

