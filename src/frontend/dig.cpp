#  include <q3x/q3x.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:23:32 BST 2018
// Changes History -
// Next Change(s)  -

   void di0( INT_ l, INT_ m, char **arg, void *data)
  {

      hmesh_t                  *var;
      var=((hmesh_t*)data)+l;

      INT_                      i0,i1;
      bool                      b0,b1;
      dir_t                     d0,d1;
      INT_                      k0;
      INT_                      mask[6]={-1,-1,-1,-1,-1,-1};

      REAL_3                    v;
      REAL_                     s;

      sscanf( arg[ 0],"%d", &i0 ); b0=(i0>0);
      sscanf( arg[ 1],"%d", &i1 ); b1=(i1>0);
      sscanf( arg[ 2],"%d", &k0 );
      sscanf( arg[ 3],"%lf", v+0 );
      sscanf( arg[ 4],"%lf", v+1 );
      sscanf( arg[ 5],"%lf", v+2 );
      sscanf( arg[ 6],"%d", mask+0 );
      sscanf( arg[ 7],"%d", mask+1 );
      sscanf( arg[ 8],"%d", mask+2 );
      sscanf( arg[ 9],"%d", mask+3 );
      sscanf( arg[10],"%d", mask+4 );
      sscanf( arg[11],"%lf", &s    );

      i0= abs(i0); i0--;
      i1= abs(i1); i1--;

      d0= dir_t(b0,i0); 
      d1= dir_t(b1,i1); 

      var->digb( d0,d1, k0, v, mask, s );
      
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:23:32 BST 2018
// Changes History -
// Next Change(s)  -

   void dx0( INT_ l, INT_ m, char **arg, void *data)
  {

      hmesh_t                  *var;
      var=((hmesh_t*)data)+l;

      INT_                      g;
      INT_                      i0,i1;
      bool                      b0,b1;
      dir_t                     d0,d1;
      INT_                      k0;
      INT_                      mask[6]={-1,-1,-1,-1,-1,-1};

      REAL_3                    v;
      REAL_3                    x;
      REAL_                     s;

      sscanf( arg[ 0],"%d", &i0 ); b0=(i0>0);
      sscanf( arg[ 1],"%d", &i1 ); b1=(i1>0);

      sscanf( arg[ 2],"%d", &g  ); 

      sscanf( arg[ 3],"%lf", x+0 );
      sscanf( arg[ 4],"%lf", x+1 );
      sscanf( arg[ 5],"%lf", x+2 );

      sscanf( arg[ 6],"%lf", v+0 );
      sscanf( arg[ 7],"%lf", v+1 );
      sscanf( arg[ 8],"%lf", v+2 );

      sscanf( arg[ 9],"%d", mask+0 );
      sscanf( arg[10],"%d", mask+1 );
      sscanf( arg[11],"%d", mask+2 );
      sscanf( arg[12],"%d", mask+3 );
      sscanf( arg[13],"%d", mask+4 );
      sscanf( arg[14],"%lf", &(s) );

      i0= abs(i0); i0--;
      i1= abs(i1); i1--;

      d0= dir_t(b0,i0); 
      d1= dir_t(b1,i1); 

      k0= var->search( g,vtx_t(x[0],x[1],x[2]) );
      k0= var->bz[g].bo[k0].h;
      k0= var->hx[k0].id;

      var->digb( d0,d1, k0, v, mask, s );
      
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:23:32 BST 2018
// Changes History -
// Next Change(s)  -

   void di6( INT_ l, INT_ m, char **arg, void *data)
  {

      hmesh_t                  *var;
      var=((hmesh_t*)data)+l;

      INT_                      i0,i1;
      bool                      b0,b1;
      dir_t                     d0,d1;
      INT_                      k0;
      INT_                      mask[6]={-1,-1,-1,-1,-1,-1};

      REAL_3                    v;
      REAL_                     d;

      sscanf( arg[ 0],"%d", &i0 ); b0=(i0>0);
      sscanf( arg[ 1],"%d", &i1 ); b1=(i1>0);
      sscanf( arg[ 2],"%d", &k0 );
      sscanf( arg[ 3],"%lf", v+0 );
      sscanf( arg[ 4],"%lf", v+1 );
      sscanf( arg[ 5],"%lf", v+2 );
      sscanf( arg[ 6],"%d", mask+0 );
      sscanf( arg[ 7],"%d", mask+1 );
      sscanf( arg[ 8],"%d", mask+2 );
      sscanf( arg[ 9],"%d", mask+3 );
      sscanf( arg[10],"%d", mask+4 );
      sscanf( arg[11],"%lf", &d );

      i0= abs(i0); i0--;
      i1= abs(i1); i1--;

      d0= dir_t(b0,i0); 
      d1= dir_t(b1,i1); 

      var->dig6( d0,d1, k0, v, mask, d );
      
  }
