#  include <q3x/q3x.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:29:09 BST 2018
// Changes History
// Next Change(s)  -

   void zone( INT_ l, INT_ m, char **arg, void *data)
  {

      hmesh_t       *var;

      INT_            k,z,r;

      assert( m == 7 );

      var=((hmesh_t*)data)+l;

      sscanf( arg[0],"%d", &k ); 
      sscanf( arg[1],"%d", &z );
      sscanf( arg[2],"%d", &r );

      k= var->indx[k];
      var->hx[k].iz[0]= z;
      var->hx[k].iz[1]= r;

  }

   void bxzone( INT_ l, INT_ m, char **arg, void *data)
  {

      hmesh_t       *var;

      INT_       h,k,z,r;
      VINT_         list;
      REAL_3       x0,x1;

      var=((hmesh_t*)data)+l;

      sscanf( arg[ 0],"%lf", x0+0 ); 
      sscanf( arg[ 1],"%lf", x1+0 ); 
      sscanf( arg[ 2],"%lf", x0+1 ); 
      sscanf( arg[ 3],"%lf", x1+1 ); 
      sscanf( arg[ 4],"%lf", x0+2 ); 
      sscanf( arg[ 5],"%lf", x1+2 ); 

      sscanf( arg[ 6],"%d", &z );
      sscanf( arg[ 7],"%d", &r );

      var->box( x0,x1, list );

      for( k=0;k<list.n;k++ )
     {
         h= var->indx[list[k]];
         var->hx[h].iz[0]= z;
         var->hx[h].iz[1]= r;
     }

  }

   void rnzone( INT_ l, INT_ m, char **arg, void *data)
  {

      hmesh_t       *var;

      INT_       h,k,z,r;
      VINT_         list;
      INT_       ist,ien;

      var=((hmesh_t*)data)+l;

      sscanf( arg[ 0],"%d", &ist  ); 
      sscanf( arg[ 1],"%d", &ien  ); 
      sscanf( arg[ 2],"%d", &z );
      sscanf( arg[ 3],"%d", &r );

      if( ist > ien )
     {
         ien= var->indx.n- ien;
         ist= var->indx.n- ist;

     }
      ist= max( 0,ist );
      ien= min( var->indx.n,ien );


      for( k=ist;k<ien;k++ )
     {
         h= var->indx[k];
         if( (h > -1) && (h < var->hx.n) ){ var->hx[h].iz[0]= z; }
         if( (h > -1) && (h < var->hx.n) ){ var->hx[h].iz[1]= r; }
     }

  }
