#  include <hmesh/hmesh.h>

   void mul( INT_ l, INT_ m,  char **arg, void *data )
  {
      hmesh_t *var;

      var= ((hmesh_t*)data)+l;

      INT_            f,d,n;
      INT_            mask[MXGRP];

      sscanf( arg[0],"%d", &f );
      sscanf( arg[1],"%d", &d );
      sscanf( arg[2],"%d", &n );

      for( INT_ i=0;i<var->nb;i++ ){ sscanf( arg[i+3],"%d",mask+i); }
      var->mul( f,d,n, mask );
  }

   void add( INT_ l, INT_ m,  char **arg, void *data )
  {
      hmesh_t *v1,*v2;

      INT_            k;
      INT_            mask[MXGRP];

      sscanf( arg[0],"%d", &k );

      v1= ((hmesh_t*)data)+l;
      v2= ((hmesh_t*)data)+k;

      for( INT_ i=0;i<v2->nb;i++ ){ sscanf( arg[i+1],"%d",mask+i); }
      v1->add( (*v2), mask );
  }

   void adf( INT_ l, INT_ m,  char **arg, void *data )
  {
      hmesh_t *v1,*v2;
      vsurf_t *s1,*s2;
      VINT_2   list;

      INT_            k,j,n;
      INT_            mask[MXGRP];

      sscanf( arg[0],"%d", &k );

      v1= ((hmesh_t*)data)+l;
      v2= ((hmesh_t*)data)+k;

      s1= (((shmesh_t*)data)->geo)+l;
      s2= (((shmesh_t*)data)->geo)+k;

      sscanf( arg[1],"%d", &n );
      list.n=n; list.resize(-1); 
      j=2;
      for( INT_ i=0;i<n;i++ ){ sscanf( arg[j++],"%d", list[i]+0 ); sscanf( arg[j++],"%d", list[i]+1 ); }

      for( INT_ i=0;i<v2->nb;i++ ){ sscanf( arg[j++],"%d",mask+i); }
      v1->add( (*v2), list,mask, *s1,*s2 );
  }
