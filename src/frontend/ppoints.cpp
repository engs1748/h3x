
#  include <hmesh/hmesh.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:22:50 BST 2018
// Changes History -
// Next Change(s)  -


   void periodic( INT_ m, char **arg, void *data )
  {

      hmesh_t        *var=(hmesh_t*)data;

      var->periodic();

      return;
  }
