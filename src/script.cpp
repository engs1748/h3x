#  include <lang.h>
#  include <q3x/q3x.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void      ijk( int l, int m, char **arg, void *data );
   void      di0( int l, int m, char **arg, void *data );
   void      dx0( int l, int m, char **arg, void *data );
   void      di6( int l, int m, char **arg, void *data );
   void      ex0( int l, int m, char **arg, void *data );
   void    drill( int l, int m, char **arg, void *data );
   void   tunnel( int l, int m, char **arg, void *data );
   void  sbranch( int l, int m, char **arg, void *data );
   void  lbranch( int l, int m, char **arg, void *data );
   void  zbranch( int l, int m, char **arg, void *data );
   void    sbpos( int l, int m, char **arg, void *data );
   void  fbranch( int l, int m, char **arg, void *data );
   void     slot( int l, int m, char **arg, void *data );
   void     pass( int l, int m, char **arg, void *data );
   void      del( int l, int m, char **arg, void *data );
   void    bxdel( int l, int m, char **arg, void *data );
   void    rndel( int l, int m, char **arg, void *data );
   void   corner( int l, int m, char **arg, void *data );
   void    split( int l, int m, char **arg, void *data );
   void collapse( int l, int m, char **arg, void *data );
   void     lref( int l, int m, char **arg, void *data );
   void     topo( int l, int m, char **arg, void *data );
   void    check( int l, int m, char **arg, void *data );

   void    write( int l, int m, char **arg, void *data );
   void     read( int l, int m, char **arg, void *data );

   void      tri( int l, int m, char **arg, void *data );
   void    frame( int l, int m, char **arg, void *data );

   void     zone( int l, int m, char **arg, void *data );
   void   bxzone( int l, int m, char **arg, void *data );
   void   rnzone( int l, int m, char **arg, void *data );
   void    inter( int l, int m, char **arg, void *data );
   void    group( int l, int m, char **arg, void *data );
   void    maxgr( int l, int m, char **arg, void *data );
   void periodic( int l, int m, char **arg, void *data );

   void      tec( int l, int m, char **arg, void *data );
   void      vtk( int l, int m, char **arg, void *data );
   void      gnp( int l, int m, char **arg, void *data );
   void     btec( int l, int m, char **arg, void *data );
   void     tell( int l, int m, char **arg, void *data );
   void     dump( int l, int m, char **arg, void *data );

   void     echo( int l, int m, char **arg, void *data );

   void     surf( int l, int m, char **arg, void *data );
   void   vertex( int l, int m, char **arg, void *data );
   void    patch( int l, int m, char **arg, void *data );
   void      map( int l, int m, char **arg, void *data );
   void      geo( int l, int m, char **arg, void *data );
   void      sze( int l, int m, char **arg, void *data );
   void      sct( int l, int m, char **arg, void *data );

   void      mul( int l, int m, char **arg, void *data );
   void      add( int l, int m, char **arg, void *data );
   void      adf( int l, int m, char **arg, void *data );

   void     misc( int l, int m, char **arg, void *data );
   void      msh( int l, int m, char **arg, void *data );


   void script( char *name ) 
  {
      lang_t var;
      shmesh_t  data;

      var.command( "ijk",      &(ijk),      "L M N [s0 s1 s2 s3 s4 s5]");
      var.command( "msh",      &(msh),      "no help");

      var.command( "di0",      &(di0),      "no help");
      var.command( "dx0",      &(dx0),      "no help");
      var.command( "di6",      &(di6),      "no help");
      var.command( "ex0",      &(ex0),      "no help");
      var.command( "lre",      &(lref),     "no help");

      var.command( "drill",    &(drill),    "no help");
      var.command( "tunnel",   &(tunnel),   "no help");
      var.command( "sbranch",  &(sbranch),  "no help");
      var.command( "lbranch",  &(lbranch),  "no help");
      var.command( "sbpos",    &(sbpos),    "no help");
      var.command( "slot",     &(slot),     "no help");
      var.command( "fbranch",  &(fbranch),  "no help");
      var.command( "zbranch",  &(zbranch),  "no help");
      var.command( "pass",     &(pass),     "no help");
      var.command( "del",      &(del),      "no help");
      var.command( "bxdel",    &(bxdel),    "no help");
      var.command( "rndel",    &(rndel),    "no help");

      var.command( "corner",   &(corner),   "no help");
      var.command( "split",    &(split),    "no help");
      var.command( "collapse", &(collapse), "no help");
      var.command( "topo",     &(topo),     "no help");
      var.command( "check",    &(check),    "no help");

      var.command( "write",    &(write),    "no help");
      var.command( "read",     &(read),     "no help");

      var.command( "tri",      &(tri),      "no help");
      var.command( "frame",    &(frame),    "no help");
      var.command( "zone",     &(zone),     "no help");
      var.command( "bxzone",   &(bxzone),   "no help");
      var.command( "rnzone",   &(rnzone),   "no help");

      var.command( "inter",    &(inter),    "no help");
      var.command( "group",    &(group),    "no help");
      var.command( "maxgr",    &(maxgr),    "no help");
      var.command( "periodic", &(periodic), "no help");

      var.command( "tec",      &(tec),      "no help");
      var.command( "vtk",      &(vtk),      "no help");
      var.command( "gnp",      &(gnp),      "no help");
      var.command( "btec",     &(btec),     "no help");
      var.command( "tell",     &(tell),     "no help");
      var.command( "dump",     &(dump),     "no help");

      var.command( "surf",     &(surf),     "no help");
      var.command( "vertex",   &(vertex),   "no help");
      var.command( "patch",    &(patch),    "no help");

      var.command( "map",      &(map),      "no help");
      var.command( "geo",      &(geo),      "no help");
      var.command( "sze",      &(sze),      "no help");
      var.command( "sct",      &(sct),      "no help");

      var.command( "mul",      &(mul),      "no help");
      var.command( "add",      &(add),      "no help");
      var.command( "adf",      &(adf),      "no help");

      var.command( "misc",     &(misc),     "no help");
      var.command( "echo",     &(echo),     "no help");

      FILE *f=NULL;
      f= fopen( name,"r" );
      assert( f );
      var.read( f );
      fclose( f );

      for( INT_ i=0;i<var.line.n;i++ )
     {
         var.execute( i,(void *)(&data) );
     }
 
  }

