
#  include <utils/proto.h>


/*3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
         1         2         3         4         5         6         7         8         9         0         1         2

   Author          Luca di Mare <l.di.mare@ic.ac.uk>
   Created         Thu Jul 15 13:27:09 BST 2010
   Changes History -
   Next Change(s)  -
 */

// generate cumulative counts

   void accml( INT_ n, INT_ *ia )
  {
      INT_ i;
      for( i=1;i<n;i++ )
     {
         ia[i]+= ia[i-1];
     }
  }

/*3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
         1         2         3         4         5         6         7         8         9         0         1         2

   Author          Luca di Mare <l.di.mare@ic.ac.uk>
   Created         Thu Jul 15 13:27:09 BST 2010
   Changes History -
   Next Change(s)  -
 */

   void stophere()
  {
      bool val=true;
      while( val )
     {

     }
  }

/*3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
         1         2         3         4         5         6         7         8         9         0         1         2

   Author          Luca di Mare <l.di.mare@ic.ac.uk>
   Created         Thu Jul 15 13:27:09 BST 2010
   Changes History -
   Next Change(s)  -
 */

   bool isnan( REAL_ var )
  {
      bool val;
      val= (!(var>=0)) && (!(var<=0));
      return val;
  }

/*3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
         1         2         3         4         5         6         7         8         9         0         1         2

   Author          Luca di Mare <l.di.mare@ic.ac.uk>
   Created         Thu Jul 15 13:27:09 BST 2010
   Changes History -
   Next Change(s)  -
 */

   void concatp( INT_ n, INT_ *ilst[], INT_ *iprm, INT_ *m, INT_ *lprm )
  {
      INT_  i,j;
      INT_  i0;

     (*m)= 0;
      if( n > 0 )
     {
         for( i=0;i<n-1;i++ )
        {
            for( j=i+1;j<n;j++ )
           {
               if( ilst[0][iprm[j]] == ilst[1][iprm[i]] )
              {
                  if( j != i+1 )
                 {
                     swap( iprm+j,iprm+(i+1) );
                 }
                  break;
              }
           }
        }
         i0= ilst[1][iprm[0]];
         for( i=1;i<n;i++ )
        {
            lprm[(*m)]= i;
            if( ilst[0][iprm[i]] != i0 ){ (*m)++; }
            i0= ilst[1][iprm[i]];
        }
         lprm[(*m)]= i;
       (*m)++;

     }
  }

/*3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
         1         2         3         4         5         6         7         8         9         0         1         2

   Author          Luca di Mare <l.di.mare@ic.ac.uk>
   Created         Thu Jul 15 13:27:09 BST 2010
   Changes History -
   Next Change(s)  -
 */

   INT_ reducible( INT_ n, INT_ *ilst[], INT_ *iprm, INT_ m, INT_ *lprm )
  {
      INT_ i0,i1;
      INT_ i,j;
      INT_ val;

      val= -1;
      for( i=m-1;i>=1;i-- )
     {
         for( j=0;j<i;j++ )
        {
            if( j != i )
           {
               i0= 0; if( j > 0 ){ i0= lprm[j-1]; };
               i1= lprm[i]-1;

               if( ilst[0][iprm[i0]] == ilst[1][iprm[i1]] )
              {
                  val= i;
                  break;
              } 
           }
        }
         if( val != -1 ){ break; };
     }

      return val;
  }

/*3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
         1         2         3         4         5         6         7         8         9         0         1         2

   Author          Luca di Mare <l.di.mare@ic.ac.uk>
   Created         Thu Jul 15 13:27:09 BST 2010
   Changes History -
   Next Change(s)  -
 */

   void concatg( INT_ n, INT_ *ilst[], INT_ *iprm, INT_ *m, INT_ *lprm )
  {
      INT_  i,j;
    (*m)= 0;
      if( n > 0 )
     {
         identv( n,iprm );
         do
        {
            concatp( n,ilst,iprm, m,lprm );
            j= reducible( n,ilst,iprm, *m,lprm );
            if( j != -1 )
           {
               assert( j > 0 );
               assert( j < n );
               i= lprm[j-1];
               swap( iprm+i,iprm+0 );
           }
        }while( j != -1 );
     }
  }

/*3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
         1         2         3         4         5         6         7         8         9         0         1         2

   Author          Luca di Mare <l.di.mare@ic.ac.uk>
   Created         Thu Jul 15 13:27:09 BST 2010
   Changes History -
   Next Change(s)  -
 */


   void match( INT_ n, INT_ *ilst0[], INT_ *ilst1[], INT_ *iprm )
  {
      INT_ i,j;
      INT_ i0,i1;
      INT_ j0,j1;

      identv( n,iprm );
      for( i=0;i<n;i++ )
     {
         i0=ilst0[0][i]; 
         i1=ilst0[1][i]; 
         for( j=i;j<n;j++ )
        {
            j0=ilst1[1][j]; 
            j1=ilst1[0][j]; 
            if( i0 == j0 && i1 == j1 )
           {
               if( i != j )
              {
                  swap( iprm+i,iprm+j );
              }
               break;
           }
        }
     }
  } 

