#  include <q3x/q3x.h>

   void metis( q3x_t &var )
  {
      FILE *f;
      INT_ n=0;

// count edges
      for( INT_ i=0;i<var.hx.n;i++ )
     {
         for( INT_ j=0;j<6;j++ )
        {
            INT_ q= var.hx[i].q[j].q;
            bool o= var.hx[i].q[j].o;
            assert( q > -1 );

            INT_ k= var.qv[q].h[!o];
            if( k > i ){ n++; }
            
        }
     }

      f= fopen( "graph.dat","w" );
      fprintf( f,"%d %d\n", var.hx.n, n );
      for( INT_ i=0;i<var.hx.n;i++ )
     {
         for( INT_ j=0;j<6;j++ )
        {
            INT_ q= var.hx[i].q[j].q;
            bool o= var.hx[i].q[j].o;
            assert( q > -1 );

            INT_ k= var.qv[q].h[!o];
            if( k > -1 ){ fprintf( f,"%d ", k+1 ); }
            
        }
         fprintf( f,"\n" );
     }
      fclose( f );
  }
