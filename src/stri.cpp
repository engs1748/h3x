#  include <tri/tri.cpp>

   void stri( )
  {
      INT_           n,m;
      INT_           j,k;
      INT_           list[128];


      tri_t          t[8];
      neig_t         wrk[4*N3];
      dir_t          a0=dir_t(0,0,1);

      REAL_3         z[32];

      FILE *f=NULL;
      FILE *g=NULL;

      f= fopen( "stri.dat","r" );

      fscanf( f,"%d",&n );
      for( INT_ k=0;k<n;k++ )
     { 
         t[k].read(f); 
     }
      fclose(f);

      g= fopen( "c3.dat","w" );
      m= 0;
      for( k=0;k<n;k++ )
     { 
         for( j=0;j<4*N3;j++ ){ wrk[j].reset(); };
         t[k].neig( wrk );
         t[k].edge( wrk );
         t[k].corners( m,z );
         t[k].check( g );
     };
      fclose(g);

      printf( "corners\n" );
      for( k=0;k<m;k++ )
     {
         printf( "[%2d] % 9.3f % 9.3f % 9.3f\n", k,z[k][0],z[k][1],z[k][2] );
     }
 
      return; 
  }
