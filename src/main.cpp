#  include <q3x/q3x.h>
#  include <sys/types.h>
#  include <sys/stat.h>

#  define ARG(a,b)         {b=true; if(a++==argc-1){ b=false; break; }}
#  define TEST(x)          if(x){ b=false;break;}

   void tests();
   void script( char * );
   void  metis( q3x_t & );

   void msg()
  {
      printf( "                                Q3X                               \n" );
      printf( "                                                                  \n" );
      printf( "                                2018                              \n" );
      printf( "                       University of Oxford                       \n" );
      printf( "                     High Performance Hex CFD                     \n" );
      printf( "                           Luca di Mare                           \n" );
      printf( "                     luca.dimare@eng.ox.ac.uk                     \n" );
      printf( "                                                                  \n" );
      printf( "                                                                  \n" );
      printf( "Usage:a.out [options]                                             \n" );
      printf( "Options are:                                                      \n" );
      printf( "-a  file N     : output mesh as ASCII                             \n" );
      printf( "-c             : output wall coordinates for modal conjugate model\n" );
      printf( "-d             : make directory tree                              \n" );
      printf( "-e       N     : smoothing.                                       \n" );
      printf( "-j  file N [V] : initialize.                                      \n" );
      printf( "-m  file       : dump graph for METIS partitioning                \n" );
      printf( "-p  file N M   : partition on N ranks with method M               \n" );
      printf( "                 (M=0  axial, M=1 manual ).                       \n" );
      printf( "-q  file       : preprocess.                                      \n" );
      printf( "-r0 file N     : run steady.                                      \n" );
      printf( "-r3 file N     : run third order RK                               \n" );
      printf( "-ra file N     : run third order Adams-Bashforth+Krank-Nicholson  \n" );
      printf( "-rt file N     : run temperature.                                 \n" );
      printf( "-s  file       : run script file.                                 \n" );
      printf( "-u  file N     : mesh by trilinear interpolation.                 \n" );
      printf( "-t  file N     : refine by a factor N (grid only for now).        \n" );
      printf( "-n  file N     : update layout.                                   \n" );
      printf( "-v  file N     : visualize.                                       \n" );
      printf( "-x  file N     : visualize (grid only)                            \n" );
      printf( "-vtk -f -d     : visualize (vtk file )                            \n" );
      printf( "                (f=l  legacy format, f=x  xml format )            \n" );
      printf( "                (d=b  block layout, d=x  grid, d=v  solution).    \n" );
  }

   int main( int argc, char **argv )
  {
      MPI_Init( &argc,&argv );
      int        rank;
      MPI_Comm_rank( MPI_COMM_WORLD,&rank );

      q3x_t            var;
      vsurf_t          geo;
      char             buf[1024];
      INT_             a;    
      bool             b= false;    


      if( rank == 0 ){ msg(); }

      if( argc >= 2 )
     {
       
         a=1;
         do
        {
            if( strcmp( argv[a],"-a" ) == 0 ) 
           {
               INT_  n= 0;
               b=    true;
               ARG(a,b); 
               if( b )
              {
                  strcpy( buf,argv[a] );
                  strcat( buf,".q3x" ); 
                  
                  ARG(a,b); sscanf( argv[a],"%d",&n );
                  if( b )
                 { 
                     FILE *f= fopen( buf,"r" );
                     if( f )
                    {
                        var.fread( f ); fclose( f );
                        var.ascii( n );
                    }
                 }
                  goto esc;
              }
           }
            TEST(b);

// Mesh smoothing
            if( strcmp( argv[a],"-c" ) == 0 ) 
           {
               var.wco();
               goto esc;
           }

            if( strcmp( argv[a],"-e" ) == 0 ) 
           {
               b=true;
               INT_ n=1;
               ARG(a,b); 
               if( b )
              {
                  strcpy( buf,argv[a] );
                  strcat( buf,".g3x" ); 
                  FILE *f= fopen( buf,"r" );
                  fread( geo,f );
                  fclose( f );

                  ARG(a,b); sscanf( argv[a],"%d", &n );
                  if( b ) 
                 {

                     var.threads=n;
                     var.smo(geo);
                 }
              }
               goto esc;
           }
            TEST(b);

// Graph for METIS

            if( strcmp( argv[a],"-m" ) == 0 ) 
           {
               b= true;
               ARG(a,b);
               if( b )
              {
                  strcpy( buf,argv[a] );
                  strcat( buf,".q3x" ); 
                  FILE *f= fopen( buf,"r" );
                  if( f )
                 {
                     var.fread( f ); fclose( f );
                     metis( var );
                 }
                  goto esc;
              }

           }
            TEST(b);

// Initialize

            if( strcmp( argv[a],"-j" ) == 0 ) 
           {
               INT_ n=0,i=0;
               REAL_ v[32];
               ARG(a,b); 
               if( b )
              {
                  strcpy( buf,argv[a] );
                  strcat( buf,".q3x" ); 
                  FILE *f= fopen( buf,"r" );

                  ARG(a,b); sscanf( argv[a],"%d", &n );
                  do
                 { 
                     ARG(a,b); 
                 }while( sscanf( argv[a],"%lf", v+(i++) ) || i < argc );

                  if( f )
                 {
                     var.fread( f ); fclose( f );
                     var.init( n,v ); 
                 }
                  goto esc;
              }
           }
            TEST(b);
            

// Partition

            if( strcmp( argv[a],"-p" ) == 0 ) 
           {
               INT_  n= 0;
               INT_  m=-1;
               b= true;
       
               ARG(a,b); 
               if( b )
              {
                  strcpy( buf,argv[a] );
                  strcat( buf,".q3x" ); 
                  ARG(a,b); sscanf( argv[a],"%d", &n );
                  ARG(a,b); sscanf( argv[a],"%d", &m );
                  if( b )
                 {
                     TEST(n== 0);
                     TEST(m==-1);
       
                     FILE *f= fopen( buf,"r" );
                     if( f )
                    {
                        var.fread( f ); fclose( f );
                        var.part( n,m );
                    }
                 }
                  goto esc;
              }
           }
            TEST(b);

// Preprocess

            if( strcmp( argv[a],"-q" ) == 0 ) 
           {
               b= true;
               ARG(a,b); 
               if( b )
              {
                  strcpy( buf,argv[a] );
                  strcat( buf,".q3x" ); 
                  if( b )
                 { 
                     FILE *f= fopen( buf,"r" );
                     if( f )
                    {
                        var.fread( f ); fclose( f );
                        var.prep();
                    }
                 }
                  goto esc;
                  
              }
           }
            TEST(b);

// Run

            if( strcmp( argv[a],"-r0" ) == 0 ) 
           {
               b=true;
               INT_ n=1;
               ARG(a,b); sscanf( argv[a],"%d", &n );
               if( b ) 
              {
                  var.threads=n;
                  var.run();
              }
               goto esc;
           }
            TEST(b);

            if( strcmp( argv[a],"-rd" ) == 0 ) 
           {
               b=true;
               INT_ n=1;
               ARG(a,b); sscanf( argv[a],"%d", &n );
               if( b ) 
              {
                  var.threads=n;
                  var.rund();
              }
               goto esc;
           }
            TEST(b);

            if( strcmp( argv[a],"-r3" ) == 0 ) 
           {
               b=true;
               INT_ n=1;
               ARG(a,b); sscanf( argv[a],"%d", &n );
               if( b ) 
              {
                  var.threads=n;
                  var.runrk();
              }
               goto esc;
           }
            TEST(b);

            if( strcmp( argv[a],"-ra" ) == 0 ) 
           {
               assert( false );
               b=true;
               INT_ n=1;
               ARG(a,b); sscanf( argv[a],"%d", &n );
               if( b ) 
              {
                  var.threads=n;
                  var.runab();
              }
               goto esc;
           }
            TEST(b);

            if( strcmp( argv[a],"-rt" ) == 0 ) 
           {
               b=true;
               INT_ n=1;
               ARG(a,b); sscanf( argv[a],"%d", &n );
               if( b ) 
              {
                  var.threads=n;
                  var.runt();
              }
               goto esc;
           }
            TEST(b);
            
// Run script

            if( strcmp( argv[a],"-s" ) == 0 ) 
           {
               b= true;
               ARG(a,b); strcpy( buf,argv[a] );
               if( b ){ script( buf ); }
               goto esc;
           }
            TEST(b);
            

// Trilinear grid

            if( strcmp( argv[a],"-u" ) == 0 ) 
           {
               INT_  n= 0;
               b=    true;
               ARG(a,b); 
               if( b )
              {
                  strcpy( buf,argv[a] );
                  strcat( buf,".q3x" ); 
                  
                  ARG(a,b); sscanf( argv[a],"%d",&n );
                  if( b )
                 { 
                     FILE *f= fopen( buf,"r" );
                     if( f )
                    {
                        var.fread( f ); fclose( f );
                        var.trln( n );
                    }
                 }
                  goto esc;
              }
           }
            TEST(b);

// Trilinear grid

            if( strcmp( argv[a],"-t" ) == 0 ) 
           {
               INT_ n= 0;
               b=    true;
               ARG(a,b); 
               if( b )
              {
                  strcpy( buf,argv[a] );
                  strcat( buf,".q3x" ); 
                  ARG(a,b); sscanf( argv[a],"%d",&n );
                  if( b )
                 { 
                     FILE *f= fopen( buf,"r" );
                     if( f )
                    {
                        var.fread( f ); fclose( f );
                        var.refn( n );
                    }
                 }
                  goto esc;
              }
           }
            TEST(b);

// Update layout
            if( strcmp( argv[a],"-n" ) == 0 ) 
           {
               INT_  n= 0;
               b=    true;

               ARG(a,b); 
               if( b )
              {
                  strcpy( buf,argv[a] );
                  strcat( buf,".q3x" ); 
                  ARG(a,b); sscanf( argv[a],"%d",&n );
                  if( b )
                 { 
                     FILE *f= fopen( buf,"r" );
                     if( f )
                    {
                        var.fread( f ); fclose( f );
                        var.updl( n );
                
                        f= fopen( buf,"w" );
                        var.fwrite( f );
                        fclose( f );
                
                    }
                 }
              }
               goto esc;
           }
            TEST(b);

// Visualise

            if( strcmp( argv[a],"-v" ) == 0 ) 
           {
               var.viz();
               goto esc;
           }

            if( strcmp( argv[a],"-x" ) == 0 ) 
           {
               var.vix();
               goto esc;
           }

            if( strcmp( argv[a],"-d" ) == 0 ) 
           {
               mkdir( "log",0700 );
               mkdir( "prep",0700 );
               mkdir( "grid",0700 );
               mkdir( "restart_init",0700 );
               mkdir( "restart_new",0700 );
               mkdir( "restart",0700 );
               mkdir( "tec",0700 );
               mkdir( "vtk",0700 );
               mkdir( "wco",0700 );
               mkdir( "wco/x",0700 );
               mkdir( "wco/t",0700 );
               goto esc;
           }
       
            if( strcmp( argv[a], "-vtk" ) == 0 )
           {
               ARG(a,b);

               INT_ format;

               if(      strcmp( argv[a], "-l" ) == 0 ){ format=0; }
               else if( strcmp( argv[a], "-x" ) == 0 ){ format=1; }
               else{ printf( "\nSpecify -l or -x for legacy or xml vtk file format\n" ); goto esc; }

               ARG(a,b);

               if( strcmp( argv[a], "-b" ) == 0 )
              {
                  var.vtk_block( format );
                  printf( "\nBlock layout written to vtk file\n" );
              }
               else if( strcmp( argv[a], "-x" ) == 0 )
              {
                  var.vtk_grid( format );
                  printf( "\nMesh written to vtk file\n" );
              }
               else if( strcmp( argv[a], "-v" ) == 0 )
              {
                  var.vtk_solution( format );
                  printf( "\nMesh and solution written to vtk file\n" );
              }
               else
              {
                  printf( "\nSpecify -b or -x or -v to write block layout, grid or solution\n" );
              }
               goto esc;
           }

        }while( ++a < argc );
     }

 esc:
      MPI_Finalize();
  }
