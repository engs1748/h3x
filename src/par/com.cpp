#  include <par.h>
#  include <hex/hex.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:37:07 BST 2018
// Changes History
// Next Change(s)  -

   com_t::com_t()
  {

      n=    0;

       idx= NULL;

       ldx= NULL;
       mdx= NULL;
      list= NULL;

      lsnd= NULL;
      isnd= NULL;
      
       hfx= NULL;

       ndx= NULL;
       aux= NULL;

  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:37:07 BST 2018
// Changes History
// Next Change(s)  -

   com_t::~com_t()
  {
     
      n=0;

      delete[]    ldx; ldx=    NULL;
      delete[]    mdx; mdx=    NULL;
      delete[]   list; list=   NULL;

      delete[]    idx; idx=    NULL;
                               
      delete[]   lsnd; lsnd=   NULL;
      delete[]   isnd; isnd=   NULL;
                               

      delete[]    hfx; hfx=    NULL;

      delete[]    ndx; ndx=    NULL;
      delete[]    aux; aux=    NULL;

  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:37:07 BST 2018
// Changes History
// Next Change(s)  -

   void com_t::dims( INT_ l, INT_ m, INT_ p )
  {
       idx= new  INT_[l];

       ldx= new  INT_ [p];
       mdx= new  INT_2[l*p];
      list= new  INT_4[m];

       hfx= new frme_t[m];

      reset( l,m,p );
      
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:37:07 BST 2018
// Changes History
// Next Change(s)  -

   void com_t::reset( INT_ l, INT_ m, INT_ p )
  {

      memset(  idx, 0,l*  sizeof( INT_ ) );

      memset(  ldx, 0,  p*sizeof( INT_ ) );
      memset(  mdx, 0,l*p*sizeof( INT_2) );
      memset( list,-1,  m*sizeof( INT_4) );

      memset(  hfx, 0,  m*sizeof(frme_t) );

      n= 0;

  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:37:07 BST 2018
// Changes History
// Next Change(s)  -

   void com_t::rcvl( INT_ neig, INT_ *c, INT_ *wc, INT_ *prm, INT_ *alias )
  {
      INT_     a,i,j,k;
      INT_     ist,ien; //,dst,den;
      INT_     jst,jen; //,dst,den;

      ien= 0;
      jen= 0;
      for( k=0;k<neig;k++ )
     {
         jst= jen;
         jen= ldx[k];

         for( j=jst;j<jen;j++ )
        {

            ist= ien; 
            ien= mdx[j][1];
            for( i=ist;i<ien;i++ )
           {
               a= list[i][0];
               list[i][2]= prm[a];
               list[i][0]= alias[a];
           }
        }
     }

      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:37:07 BST 2018
// Changes History
// Next Change(s)  -

   void com_t::fwrite( INT_ neig, FILE *f )
  {
      INT_ m;

    ::fwrite(   &n,        1,sizeof( INT_ ),f );

    ::fwrite(  idx,        n,sizeof( INT_ ),f );
    ::fwrite(  ldx,     neig,sizeof( INT_ ),f );

      m= neig;
      m= ldx[m-1];

      if( m > 0 )
     {

       ::fwrite(  mdx,        m,sizeof( INT_2),f );

         m= mdx[m-1][1];
       ::fwrite( list,        m,sizeof( INT_4),f );
       ::fwrite(  hfx,        m,sizeof(frme_t),f );

     }

  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:37:07 BST 2018
// Changes History
// Next Change(s)  -

   void com_t::fread( INT_ neig, FILE *f )
  {
      INT_ m;

      assert( !(idx) );
      assert( !(ldx) );
      assert( !(mdx) );
      assert( !(list) );
      assert( !(hfx) );

    ::fread(   &n,        1,sizeof( INT_ ),f );

      idx= new INT_[n];
      ldx= new INT_[neig];

    ::fread(  idx,          n,sizeof( INT_ ),f );
    ::fread(  ldx,       neig,sizeof( INT_ ),f );


      m= neig;
      m= ldx[m-1];

      if( m > 0 )
     {
         mdx= new INT_2[m];
       ::fread(  mdx,        m,sizeof( INT_2),f );
   
         m= mdx[m-1][1];
   
         list= new  INT_4[m];
         hfx=  new frme_t[m];
   
       ::fread( list,        m,sizeof(  INT_4),f );
       ::fread(  hfx,        m,sizeof( frme_t),f );
     }

  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:37:07 BST 2018
// Changes History
// Next Change(s)  -

   void com_t::printf( INT_ neig )
  {
      INT_ i,j,k;
      INT_ ist,ien;
      INT_ jst,jen;

    ::printf( "# of connections %4d\n", n );
    ::printf( "correspondence between connections and hex:\n" );

      ien= 0;
      jen= 0;
      for( k=0;k<neig;k++ )
     {

         jst= jen;
         jen= ldx[k];

         if( jen > jst )
        {
          ::printf( "neighbor %2d:\n",k );
        
           for( j=jst;j<jen;j++ )
          {
            ::printf( "connection %4d (%4d): ",mdx[j][0], idx[ mdx[j][0] ] );
              ist= ien;
              ien= mdx[j][1];
              for( i=ist;i<ien;i++ )
             {
               ::printf( "%4d (%4d,%4d) ",list[i][0],list[i][1],list[i][2] );
             }
            ::printf( "\n" );
           }
        }
     }

      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:37:07 BST 2018
// Changes History
// Next Change(s)  -

   void com_t::start( INT_ neig, INT_* ineg, hex_t *h, FILE *f )
  {
      MPI_Request            *srq;
      MPI_Request            *rrq;
      MPI_Status          *status;

      INT_              i,j,k,l,m;
      INT_        ist,ien,jst,jen;
      INT_                   *buf;


      assert( !lsnd );
      lsnd=    new INT_[neig];
      memset( lsnd,0,neig*sizeof(INT_) );

      l=ldx[0];
      if( l == 0 ){ return; }

      buf=    new INT_[neig];
      status= new MPI_Status[neig];
      srq=    new MPI_Request[neig];
      rrq=    new MPI_Request[neig];

      ien=mdx[l-1][1];

      srq[0]= MPI_REQUEST_NULL;
      rrq[0]= MPI_REQUEST_NULL;

// exchange the number of entries to communicate

      for( k=1;k<neig;k++ )
     {
         l= ldx[k];

         ist= ien;
         ien= mdx[l-1][1];
          
         buf[k]= ien- ist;

         MPI_Isend(  buf+k,1,MPI_INT,ineg[k],1, MPI_COMM_WORLD, srq+k ); 
         MPI_Irecv( lsnd+k,1,MPI_INT,ineg[k],1, MPI_COMM_WORLD, rrq+k ); 

     }

      MPI_Waitall( neig,srq, status );
      MPI_Waitall( neig,rrq, status );

      accml( neig,lsnd );

      assert( !isnd );
      isnd   = new INT_4[lsnd[neig-1]];

// exchange entries to communicate

      l=ldx[0];
      ien=mdx[l-1][1];
      jen=0;
      srq[0]= MPI_REQUEST_NULL;
      rrq[0]= MPI_REQUEST_NULL;
      for( k=1;k<neig;k++ )
     {
         l= ldx[k];

         ist= ien;
         ien= mdx[l-1][1];

         jst= jen;
         jen= lsnd[k];
          
         if( k > 0 )
        {

            assert( ((jen-jst )>0) == ((ien-ist)>0) );

            MPI_Isend(  list+ist,4*(ien-ist),MPI_INT,ineg[k],0, MPI_COMM_WORLD, srq+k ); 
            MPI_Irecv(  isnd+jst,4*(jen-jst),MPI_INT,ineg[k],0, MPI_COMM_WORLD, rrq+k ); 
        }
         else
        {
            srq[k+0]= MPI_REQUEST_NULL;
            rrq[k+0]= MPI_REQUEST_NULL;
        }
     }

      MPI_Waitall( neig,srq, status );
      MPI_Waitall( neig,rrq, status );

// identify the entries in the send list with local connections

      ien= 0;
      l= ldx[0];
      for( k=0;k<neig;k++ )
     {
         ist= ien;
         ien= lsnd[k];
         for( i=ist;i<ien;i++ )
        {

            assert( isnd[i][0] == h[isnd[i][2]].id );

            bool flag= false;
            for( int j=0;j<mdx[l-1][1];j++ )
           {
               if( (isnd[i][0] == list[j][0]) && (isnd[i][1] == list[j][1]) )
              {
                  isnd[i][3]= j;
                  flag= true;
                  break;
              }
           }
            assert( flag );
        }
     }

      delete[] buf; buf= NULL;
      delete[] srq; srq= NULL;
      delete[] rrq; rrq= NULL;

      delete[] status; status= NULL;

// create auxiliary lists

      l= neig;
      l= ldx[l-1];
      l= mdx[l-1][1];

      assert( !ndx ); ndx= new INT_2[n];
      assert( !aux ); aux= new INT_ [l];

      memset( ndx,0,n*sizeof(INT_2) );
      
      jen=0;
      ien=0;
      for( k=0;k<neig;k++ )
     {

         jst= jen;
         jen= ldx[k];

         for( j=jst;j<jen;j++ )
        {
            ist= ien;            
            ien= mdx[j][1];

            l=   mdx[j][0];
            ndx[l][1]+= (ien-ist);
        }
     }

      for( i=1;i<n;i++ ){ ndx[i][1]+= ndx[i-1][1]; }
      for( i=n-1;i>0;i-- ){ ndx[i][1]= ndx[i-1][1]; }
      ndx[0][1]= 0;

      ien=0;
      jen= 0;
      k= 0;

      jst= jen;
      jen= ldx[k];

      for( j=jst;j<jen;j++ )
     {
         ist= ien;            
         ien= mdx[j][1];
      
         l= mdx[j][0];
         m= ndx[l][1];
        
         for( i=ist;i<ien;i++ )
        {
            aux[m]= i;
            m++; 
        }
         ndx[l][0]= m;
         ndx[l][1]= m;
     }

      for( k=1;k<neig;k++ )
     {

         jst= jen;
         jen= ldx[k];

         for( j=jst;j<jen;j++ )
        {
            ist= ien;            
            ien= mdx[j][1];
         
            l= mdx[j][0];
            m= ndx[l][1];
           
            for( i=ist;i<ien;i++ )
           {
               aux[m]= i;
               m++; 
           }
            ndx[l][1]= m;
        }
     }

      return;
   }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:37:07 BST 2018
// Changes History
// Next Change(s)  -

   void com_t::task( INT_ i, INT_ &l, INT_ &m, INT_ **v )
  {

      INT_ ist,ien,loc;

      ist= 0;
      if( i > 0 ) ist= ndx[i-1][1];

      loc= ndx[i][0];
      ien= ndx[i][1];

    (*v)= aux+ist;
      l=  ien-ist;
      m=  loc-ist;

      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:37:07 BST 2018
// Changes History
// Next Change(s)  -

   void com_t::task( INT_ i, INT_ &l, INT_ &m, INT_ **v, INT_ *w, INT_ *u )
  {

      INT_ ist,ien,loc;

      ist= 0;
      if( i > 0 ) ist= ndx[i-1][1];

      loc= ndx[i][0];
      ien= ndx[i][1];

    (*v)= aux+ist;
      l=  ien-ist;
      m=  loc-ist;

      for( INT_ j=ist;j<ien;j++ )
     {
         INT_ k= list[j][3];
         w[k]= j;
         u[k]= idx[i];
     }

      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:37:07 BST 2018
// Changes History
// Next Change(s)  -

   void com_t::taskinfo( MMAP_   &var, INT_ a, frule_t &r, INT_ &c )
  {
      c= list[a][2];
      r= frule_t( hfx[a], var.idim(), var.jdim(), var.kdim() );
      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:37:07 BST 2018
// Changes History
// Next Change(s)  -

   void com_t::pick( msg_t &msg, INT_ &m, INT_ *t )
  {
      INT_     jst,jen;
      INT_     i,j,k,l;

      m= 0;

      jen= 0;
      for( i=0;i<n;i++ )
     {
         jst= jen;
         jen= ndx[i][0];
         if( msg.dep[i]==0 )
        {
            l=0;
            for( j=jst;j<jen;j++ )
           {
               k= aux[j];
               k= list[k][2];
               assert( k < msg.n );
               if( msg.col[k] < msg.l ){ l++; }; 
           }
            if( l == ( jen-jst ) )
           {
               for( j=jst;j<jen;j++ )
              {
                  k= aux[j];
                  k= list[k][2];
                  assert( k < msg.n );
                  msg.col[k]= msg.l;
              }
               msg.dep[i]=-1;
               assert( m < NTASK );
               t[m++]= i;
           }
        }   
         jen= ndx[i][1];
     }

    (msg.l)++;
     return;
  }
