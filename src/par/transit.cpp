#  include <par.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:39:30 BST 2018
// Changes History
// Next Change(s)  -

   bool par_t::transit( com_t &com, msg_t &msg, INT_ &n, INT_ *task, INT_ local )
  {

      INT_        i,j,k;
      INT_      jst,jen;
      INT_         flag;
      MPI_Status   stat;
      bool          val;


// decide whether to include local connectivity entries or not

      if( local == 0 )
     {
         jen= 0;
     }
      else
     {
         assert( local == 1 );
         jen= com.ldx[0];
     }
      
      n=   0;
      val= false;

      for( k=local;k<neig;k++ )
     {

         jst= jen;
         jen= com.ldx[k];

// check if this request has already been handled

         if( msg.rreq[k] == MPI_REQUEST_NULL )
        {
            for( j=jst;j<jen;j++ )
           { 
               i= com.mdx[j][0];
// can pick tasks if all dependencies satisfied but some entries not handled
               if( msg.dep[i] == 0 ){ val=true; };
           }
        }
         else
        {
// can pick tasks if not all dependencies satisfied
            val= true;
            MPI_Test( msg.rreq+k, &flag,&stat );

            if( flag )
           {
               MPI_Wait( msg.rreq+k,&stat );
               msg.rreq[k]= MPI_REQUEST_NULL;

               for( j=jst;j<jen;j++ )
              { 
                  i= com.mdx[j][0];
                  msg.dep[i]--; 
              }
           }
        }
     }

      if( val )
     {

// pick thread-safe tasks (in fact these are too safe because more than one connection could
// be handled on a block without causing race conditions, e.g. two opposite faces).

         com.pick( msg, n,task );

     }
      else
     {
         close( msg ); 
     }
 

      return val;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:39:30 BST 2018
// Changes History
// Next Change(s)  -

   bool par_t::locals( com_t &com, msg_t &msg, INT_ &n, INT_ *task )
  {
      bool val;
      com.pick( msg,n,task );

      val= (n>0);
      if( !val ){ msg.l=0; };

      return val;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:39:30 BST 2018
// Changes History
// Next Change(s)  -

   bool par_t::all( com_t &com, msg_t &msg, INT_ &n, INT_ *task )
  {
      bool val;
      com.pick( msg,n,task );

      val= (n>0);
      if( !val ){ msg.l=0; };

      return val;
  }
