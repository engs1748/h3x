#  include <par.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   par_t::par_t()
  {
      MPI_Comm_rank( MPI_COMM_WORLD, &rank );
      MPI_Comm_size( MPI_COMM_WORLD, &size );

      ineg=    NULL;
       file=   NULL;
      tags=       0;
  };

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void par_t::fwritep( FILE *f )
  {

// neighboring ranks
    ::fwrite( &neig,   1,sizeof(INT_), f );
    ::fwrite(  ineg,neig,sizeof(INT_), f );

// connectivity

      com[0].fwrite( neig,f ); 
      com[1].fwrite( neig,f );
      com[2].fwrite( neig,f );

  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void par_t::freadp( FILE *f )
  {

// neighboring ranks
    ::fread( &neig,   1,sizeof(INT_), f );

      ineg= new INT_[neig];
    ::fread(  ineg,neig,sizeof(INT_), f );

// connectivity
      com[0].fread( neig,f );
      com[1].fread( neig,f );
      com[2].fread( neig,f );

  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   par_t::~par_t()
  {
      
      delete[] ineg;    ineg=     NULL;
      if( file ){ fclose( file ); }

  };

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void par_t::open( msg_t &msg, com_t &com )
  {
      INT_     i,j,k;
      INT_     jst,jen;

      assert( msg.tag == -1 ); 

      msg.tag= tags++;
      for( INT_ i=0;i<neig;i++ )
     {
         msg.sreq[i]= MPI_REQUEST_NULL;
         msg.rreq[i]= MPI_REQUEST_NULL;
     }

      k=0;
      msg.l=   k;

      memset( msg.col,k-1,msg.n*sizeof(INT_) );
      memset( msg.dep,       0,com.n*sizeof(INT_) );

      jen=com.ldx[0];
      for( k=1;k<neig;k++ )
     {
         jst=jen; 
         jen= com.ldx[k];
         for( j=jst;j<jen;j++ )
        {
            i  = com.mdx[j][0];
            msg.dep[i]++;
        }
     }

      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void par_t::close( msg_t &msg )
  {
      assert( msg.tag == --tags );
      msg.tag= -1;

      MPI_Waitall( neig,msg.sreq, msg.sstat );

      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void par_t::malloc( msg_t &msg, MMAP_   &var, com_t &com )
  {
      assert( msg.tag == -1 ); 

      assert(  !msg.sreq ); msg.sreq=  new MPI_Request[neig];
      assert(  !msg.rreq ); msg.rreq=  new MPI_Request[neig];

      assert( !msg.sstat ); msg.sstat= new MPI_Status[neig];
      assert( !msg.rstat ); msg.rstat= new MPI_Status[neig];

      assert( !msg.dep  );   msg.dep =  new INT_[com.n];

      msg.n= var.blocks();
      assert( !msg.col  );   msg.col  = new INT_[msg.n];

      for( INT_ i=0;i<neig;i++ )
     {
         msg.sreq[i]= MPI_REQUEST_NULL;
         msg.rreq[i]= MPI_REQUEST_NULL;

     }

      memset( msg.dep  , 0,com.n*sizeof(INT_) );
      memset( msg.col  ,-1,msg.n*sizeof(INT_) );
      msg.l= 0;

      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void par_t::free( msg_t &msg )
  {
      assert( (msg.tag==-1) );

      delete[] msg.sreq;  msg.sreq =  NULL;
      delete[] msg.rreq;  msg.rreq =  NULL;
                      
      delete[] msg.sstat; msg.sstat=  NULL;
      delete[] msg.rstat; msg.rstat=  NULL;

      delete[] msg.dep;   msg.dep  =  NULL;
      delete[] msg.col;   msg.col  =  NULL;

      return;
  }
