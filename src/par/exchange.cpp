#  include <par.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void par_t::exchange( REAL_ *x, MMAP_   &var, com_t &com, msg_t &msg )
  {
      INT_       ist,ien;
      INT_       jst,jen;
      INT_       h,k;

      open( msg,com );

      h=   com.ldx[0];
      if( h == 0 ){ return; };

      ien= com.mdx[h-1][1];
      jen= com.lsnd[0];
 
     
      for( k=1;k<neig;k++ )
     {
         ist= ien;
         jst= jen;

         h= com.ldx[k];

         ien= com.mdx[h-1][1];
         jen= com.lsnd[k];

         assert( ((jen-jst)>0) == ((ien-ist)>0) );

         if( ien > ist )
        {
            assert( (  var.styp[k]!=MPI_DATATYPE_NULL) );

            MPI_Isend( x, 1,var.styp[k], ineg[k], msg.tag, MPI_COMM_WORLD, msg.sreq+k );
            MPI_Irecv( x, 1,var.rtyp[k], ineg[k], msg.tag, MPI_COMM_WORLD, msg.rreq+k );

        }
     }

      return;

  }

   void par_t::noexchange( REAL_ *x, MMAP_   &var, com_t &com, msg_t &msg )
  {
      open( msg,com );
      close( msg );
      return;

  }
