#  include <par.h>
//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   msg_t::msg_t()
  {
      tag=-1;
      n=    0;
 
      dep=  NULL;

      sreq= NULL;
      rreq= NULL;

      sstat=NULL;
      rstat=NULL;

      col  =NULL;
  };

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   msg_t::~msg_t()
  {
      tag=-1;
      n=    0;

      delete[]  dep;  dep= NULL;
      delete[]  col;  col= NULL;

      delete[]  sreq;  sreq= NULL;
      delete[]  rreq;  rreq= NULL;
                      
      delete[] sstat; sstat= NULL;
      delete[] rstat; rstat= NULL;

  };

