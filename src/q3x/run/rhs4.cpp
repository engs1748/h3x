
#  include <q3x/q3x.h>
#  include <q3x/inline/fmetric0>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::rhs4( REAL_ *x, REAL_ *wm, REAL_ *wb, REAL_ *wc, REAL_ *dxdx,
                    REAL_ *vb0, REAL_ *vb, 
                    REAL_  *v, REAL_ *dvdx, REAL_ *rb, REAL_  *r, REAL_ *a, REAL_ *lhd )
  {

      INT_    ntsk;
      INT_    tasks[NTASK];

      REAL_3   x0,x1,x2,x3,x4,x5,x6,x7;

      REAL_    q0[  NVAR_], q1[  NVAR_], q2[  NVAR_], qi[  NVAR_], qj[  NVAR_], qk[  NVAR_];

      REAL_   q0x[3*NVAR_],q1x[3*NVAR_],qix[3*NVAR_],qjx[3*NVAR_],qkx[3*NVAR_];

      REAL_   x0x[      6],x1x[      6],xix[      6],xjx[      6],xkx[      6];

      REAL_    v0[  NVAR_], v1[  NVAR_], vi[  NVAR_], vj[  NVAR_], vk[  NVAR_];
      REAL_    u0[  NVAR_],              ui[  NVAR_], uj[  NVAR_], uk[  NVAR_];

      REAL_    w0[ WSIZE_] ,w1[ WSIZE_] ,wi[ WSIZE_] ,wj[ WSIZE_] ,wk[ WSIZE_];
      REAL_    w[MSIZE_];

      REAL_2  d0,di,dj,dk;

      INT_    c,d, e,f,g, i,j,k, l,m,n, s,t,o,u;
      INT_    ist,ien;
      frule_t t0,t1,t2;



// compute metrics

      exchange(  dvdx, sd[1], com[0], msg[0] );

      so[1].memset(   r,0 );
      so[1].memset(   a,0 );
         sb.memset(  rb,0 );
         ld.memset( lhd,0 );

// boundary loop -find way to do with openmp

      ien= 0;
      for( k=0;k<FREES;k++ )
     {
         ist= ien;
         ien= bn.l[k][0];
         for( INT_ t=ist;t<ien;t++ )
        {
    
            t0= frule_t( bn.f[t], bsize,bsize,bsize );
            c= bn.h[t];
            if( c > -1 )
           {  
               for( j=0;j<bsize;j++ )
              {
                  for( i=0;i<bsize;i++ )
                 {
                     t0.transform( i,j,0, l,m,n );

                     mw[0].loadv(l,m,n,  c,  wm, w0  );
                     so[0].loadv(l,m,n,  c,   v, q0  );
                     xd[0].loadv(l,m,n,  c,dxdx, x0x );
                     sd[0].loadv(l,m,n,  c,dvdx, q0x );
                        bw.loadv(i,j,    t,  wb, w   );
                        sb.loadv(i,j,    t,  vb, q1  );
                        sb.loadv(i,j,    t, vb0, q2  );

                     gas.fluxb( w+0, w+3, 0., w0, q0,q1,q2, x0x,q0x, v0,v1, d0 );
             
                     so[0].subv(l,m,n,c,   r,     v0 );
                        ld.addv(l,m,n,c, lhd,     d0 );

                        sb.storev(i,j,  t,  rb,     v1 );
             
                 }
              }
           }
        }
     }
      for( k=FREES;k<FREES+1;k++ )
     {
         ist= ien;
         ien= bn.l[k][0];
         for( INT_ t=ist;t<ien;t++ )
        {
    
            t0= frule_t( bn.f[t], bsize,bsize,bsize );
            c= bn.h[t];
            if( c > -1 )
           {   

               for( j=0;j<bsize;j++ )
              {
                  for( i=0;i<bsize;i++ )
                 {
                     t0.transform( i,j,0, l,m,n );

                     mw[0].loadv(l,m,n,  c,  wm, w0  );
                     so[0].loadv(l,m,n,  c,   v, q0  );
                     xd[0].loadv(l,m,n,  c,dxdx, x0x );
                     sd[0].loadv(l,m,n,  c,dvdx, q0x );
                        bw.loadv(i,j,    t,  wb, w   );
                        sb.loadv(i,j,    t,  vb, q1  );
                        sb.loadv(i,j,    t, vb0, q2  );

                     gas.fluxw( w+0, w+3, 0., w0, q0,q1,q2, x0x,q0x, v0,u0,v1, d0 );
             
                     so[0].subv(l,m,n,c,   r,     v0 );
                     so[0].subv(l,m,n,c,   a,     u0 );

                        ld.addv(l,m,n,c, lhd,     d0 );

                        sb.storev(i,j,  t,  rb,     v1 );


                     if( l==0 && m==0 && n==7 && hx[c].id == 13 )
                    {
//                      printf( "% 2d % 9.3e % 9.3e % 9.3e % 9.3e % 9.3e\n", t,q1[0],q1[1],q1[2],q1[3],q1[4] );
                    }
                 }
              }
           }
        }
     }
      for( k=FREES+1;k<AWALLS;k++ )
     {
         ist= ien;
         ien= bn.l[k][0];
         for( INT_ t=ist;t<ien;t++ )
        {
    
            t0= frule_t( bn.f[t], bsize,bsize,bsize );
            c= bn.h[t];
            if( c > -1 )
           {   
               for( j=0;j<bsize;j++ )
              {
                  for( i=0;i<bsize;i++ )
                 {

                     t0.transform( i,j,0, l,m,n );

                     mw[0].loadv(l,m,n,  c,  wm, w0  );
                     so[0].loadv(l,m,n,  c,   v, q0  );
                     xd[0].loadv(l,m,n,  c,dxdx, x0x );
                     sd[0].loadv(l,m,n,  c,dvdx, q0x );
                        bw.loadv(i,j,    t,  wb, w   );
                        sb.loadv(i,j,    t,  vb, q1  );
                        sb.loadv(i,j,    t, vb0, q2  );

                     gas.fluxa( w+0, w+3, 0., w0, q0,q1,q2, x0x,q0x, v0,u0,v1, d0 );
             
                     so[0].subv(l,m,n,c,   r,     v0 );
                     so[0].subv(l,m,n,c,   a,     u0 );

                        ld.addv(l,m,n,c, lhd,     d0 );

                        sb.storev(i,j,  t,  rb,     v1 );
             
                 }
              }
           }
        }
     }
      for( k=AWALLS;k<TWALLS;k++ )
     {
         ist= ien;
         ien= bn.l[k][0];
         for( INT_ t=ist;t<ien;t++ )
        {
    
            t0= frule_t( bn.f[t], bsize,bsize,bsize );
            c= bn.h[t];
            if( c > -1 )
           {   
               for( j=0;j<bsize;j++ )
              {
                  for( i=0;i<bsize;i++ )
                 {

                     t0.transform( i,j,0, l,m,n );

                     mw[0].loadv(l,m,n,  c,  wm, w0  );
                     so[0].loadv(l,m,n,  c,   v, q0  );
                     xd[0].loadv(l,m,n,  c,dxdx, x0x );
                     sd[0].loadv(l,m,n,  c,dvdx, q0x );
                        bw.loadv(i,j,    t,  wb, w   );
                        sb.loadv(i,j,    t,  vb, q1  );
                        sb.loadv(i,j,    t, vb0, q2  );

                     gas.fluxt( w+0, w+3, 0., w0, q0,q1,q2, x0x,q0x, v0,u0,vi, d0 );

                     so[0].subv(l,m,n,c,   r,     v0 );
                     so[0].subv(l,m,n,c,   a,     u0 );

                        ld.addv(l,m,n,c, lhd,     d0 );

                        sb.storev(i,j,  t,  rb,     vi );
             
                 }
              }
           }
        }
     }

#pragma omp parallel for private(  t,  i,  j,  k,  w, \
                                  w0, wi, wj, wk, \
                                      x1, x2, x3, \
                                  x4, x5, x6, x7, \
                                  x0x,xix,xjx,xkx,\
                                  q0, qi, qj, qk, \
                                  q0x,qix,qjx,qkx,\
                                  u0, ui, uj, uk, \
                                  v0, vi, vj, vk, \
                                  di, dj, dk, d0 ) schedule(dynamic)
 

      for( t=0;t<hx.n;t++ )
     {

         for( k=0;k<bsize-1;k++ )
        {
            for( j=0;j<bsize-1;j++ )
           {
               for( i=0;i<bsize-1;i++ )
              {

                  co[0].loadv(i+1,j,  k,  t, x,x1 );
                  co[0].loadv(i,  j+1,k,  t, x,x2 );
                  co[0].loadv(i+1,j+1,k,  t, x,x3 );

                  co[0].loadv(i,  j,  k+1,t, x,x4 );
                  co[0].loadv(i+1,j,  k+1,t, x,x5 );
                  co[0].loadv(i,  j+1,k+1,t, x,x6 );
                  co[0].loadv(i+1,j+1,k+1,t, x,x7 );

                  mw[0].loadv(i,  j,  k,  t,wm,w0  );
                  mw[0].loadv(i+1,j,  k,  t,wm,wi  );
                  mw[0].loadv(i,  j+1,k,  t,wm,wj  );
                  mw[0].loadv(i,  j,  k+1,t,wm,wk  );

                  so[0].loadv(i,  j,  k,  t,v, q0 );
                  so[0].loadv(i+1,j,  k,  t,v, qi );
                  so[0].loadv(i,  j+1,k,  t,v, qj );
                  so[0].loadv(i,  j,  k+1,t,v, qk );

                  xd[0].loadv(i,  j,  k,  t,dxdx, x0x );
                  xd[0].loadv(i+1,j,  k,  t,dxdx, xix );
                  xd[0].loadv(i,  j+1,k,  t,dxdx, xjx );
                  xd[0].loadv(i,  j,  k+1,t,dxdx, xkx );

                  sd[0].loadv(i,  j,  k,  t,dvdx, q0x );
                  sd[0].loadv(i+1,j,  k,  t,dvdx, qix );
                  sd[0].loadv(i,  j+1,k,  t,dvdx, qjx );
                  sd[0].loadv(i,  j,  k+1,t,dvdx, qkx );

                  fmetric0( 0.5,0.5, x1,x3,x5,x7, w+0, w+3, w[6], w+7, w+10 );
                  gas.flux2( w+0,w+3, 0., w0,wi, q0,qi, x0x,xix, q0x,qix, vi,ui,di );
                  
                  fmetric0( 0.5,0.5, x3,x2,x7,x6, w+0, w+3, w[6], w+7, w+10 );
                  gas.flux2( w+0,w+3, 0., w0,wj, q0,qj, x0x,xjx, q0x,qjx, vj,uj,dj );

                  fmetric0( 0.5,0.5, x4,x5,x6,x7, w+0, w+3, w[6], w+7, w+10 );
                  gas.flux2( w+0,w+3, 0., w0,wk, q0,qk, x0x,xkx, q0x,qkx, vk,uk,dk );

                  u0[0]= ui[0]+uj[0]+uk[0];
                  u0[1]= ui[1]+uj[1]+uk[1];
                  u0[2]= ui[2]+uj[2]+uk[2];
                  u0[3]= ui[3]+uj[3]+uk[3];
                  u0[4]= ui[4]+uj[4]+uk[4];

                  v0[0]= vi[0]+vj[0]+vk[0];
                  v0[1]= vi[1]+vj[1]+vk[1];
                  v0[2]= vi[2]+vj[2]+vk[2];
                  v0[3]= vi[3]+vj[3]+vk[3];
                  v0[4]= vi[4]+vj[4]+vk[4];

                  d0[0]= di[0]+dj[0]+dk[0];
                  d0[1]= di[1]+dj[1]+dk[1];

                  so[0].subv(i,  j,  k,  t,  r, v0 );
                  so[0].addv(i+1,j,  k,  t,  r, vi );
                  so[0].addv(i,  j+1,k,  t,  r, vj );
                  so[0].addv(i,  j,  k+1,t,  r, vk );

                  so[0].subv(i,  j,  k,  t,  a, u0 );
                  so[0].addv(i+1,j,  k,  t,  a, ui );
                  so[0].addv(i,  j+1,k,  t,  a, uj );
                  so[0].addv(i,  j,  k+1,t,  a, uk );

                     ld.addv(i,  j,  k,  t,lhd, d0 );
                     ld.addv(i+1,j,  k,  t,lhd, di );
                     ld.addv(i,  j+1,k,  t,lhd, dj );
                     ld.addv(i,  j,  k+1,t,lhd, dk );

              }
           }
        }

         i=bsize-1;
         for( k=0;k<bsize-1;k++ )
        {
            for( j=0;j<bsize-1;j++ )
           {
               co[0].loadv(i,  j+1,k,  t, x,x2 );
               co[0].loadv(i+1,j+1,k,  t, x,x3 );

               co[0].loadv(i,  j,  k+1,t, x,x4 );
               co[0].loadv(i+1,j,  k+1,t, x,x5 );
               co[0].loadv(i,  j+1,k+1,t, x,x6 );
               co[0].loadv(i+1,j+1,k+1,t, x,x7 );

               mw[0].loadv(i,  j,  k,  t,wm,w0  );
               mw[0].loadv(i,  j+1,k,  t,wm,wj  );
               mw[0].loadv(i,  j,  k+1,t,wm,wk  );

               so[0].loadv(i,  j,  k,  t,v, q0 );
               so[0].loadv(i,  j+1,k,  t,v, qj );
               so[0].loadv(i,  j,  k+1,t,v, qk );

               xd[0].loadv(i,  j,  k,  t,dxdx, x0x );
               xd[0].loadv(i,  j+1,k,  t,dxdx, xjx );
               xd[0].loadv(i,  j,  k+1,t,dxdx, xkx );

               sd[0].loadv(i,  j,  k,  t,dvdx, q0x );
               sd[0].loadv(i,  j+1,k,  t,dvdx, qjx );
               sd[0].loadv(i,  j,  k+1,t,dvdx, qkx );

               fmetric0( 0.5,0.5, x3,x2,x7,x6, w+0, w+3, w[6], w+7, w+10 );
               gas.flux2( w+0,w+3, 0., w0,wj, q0,qj, x0x,xjx, q0x,qjx, vj,uj,dj );

               fmetric0( 0.5,0.5, x4,x5,x6,x7, w+0, w+3, w[6], w+7, w+10 );
               gas.flux2( w+0,w+3, 0., w0,wk, q0,qk, x0x,xkx, q0x,qkx, vk,uk,dk );

               u0[0]= uj[0]+uk[0];
               u0[1]= uj[1]+uk[1];
               u0[2]= uj[2]+uk[2];
               u0[3]= uj[3]+uk[3];
               u0[4]= uj[4]+uk[4];

               v0[0]= vj[0]+vk[0];
               v0[1]= vj[1]+vk[1];
               v0[2]= vj[2]+vk[2];
               v0[3]= vj[3]+vk[3];
               v0[4]= vj[4]+vk[4];

               d0[0]= dj[0]+dk[0];
               d0[1]= dj[1]+dk[1];

               so[0].subv(i,  j,  k,  t,  r, v0 );
               so[0].addv(i,  j+1,k,  t,  r, vj );
               so[0].addv(i,  j,  k+1,t,  r, vk );

               so[0].subv(i,  j,  k,  t,  a, u0 );
               so[0].addv(i,  j+1,k,  t,  a, uj );
               so[0].addv(i,  j,  k+1,t,  a, uk );

                  ld.addv(i,  j,  k,  t,lhd, d0 );
                  ld.addv(i,  j+1,k,  t,lhd, dj );
                  ld.addv(i,  j,  k+1,t,lhd, dk );

           }
        }

         j=bsize-1;
         for( k=0;k<bsize-1;k++ )
        {
            for( i=0;i<bsize-1;i++ )
           {

               co[0].loadv(i+1,j,  k,  t, x,x1 );
               co[0].loadv(i+1,j+1,k,  t, x,x3 );

               co[0].loadv(i,  j,  k+1,t, x,x4 );
               co[0].loadv(i+1,j,  k+1,t, x,x5 );
               co[0].loadv(i,  j+1,k+1,t, x,x6 );
               co[0].loadv(i+1,j+1,k+1,t, x,x7 );

               mw[0].loadv(i,  j,  k,  t,wm,w0  );
               mw[0].loadv(i+1,j,  k,  t,wm,wi  );
               mw[0].loadv(i,  j,  k+1,t,wm,wk  );

               so[0].loadv(i,  j,  k,  t,v, q0 );
               so[0].loadv(i+1,j,  k,  t,v, qi );
               so[0].loadv(i,  j,  k+1,t,v, qk );
               xd[0].loadv(i,  j,  k,  t,dxdx, x0x );
               xd[0].loadv(i+1,j,  k,  t,dxdx, xix );
               xd[0].loadv(i,  j,  k+1,t,dxdx, xkx );

               sd[0].loadv(i,  j,  k,  t,dvdx, q0x );
               sd[0].loadv(i+1,j,  k,  t,dvdx, qix );
               sd[0].loadv(i,  j,  k+1,t,dvdx, qkx );

               fmetric0( 0.5,0.5, x1,x3,x5,x7, w+0, w+3, w[6], w+7, w+10 );
               gas.flux2( w+0,w+3, 0., w0,wi, q0,qi, x0x,xix, q0x,qix, vi,ui,di );
               
               fmetric0( 0.5,0.5, x4,x5,x6,x7, w+0, w+3, w[6], w+7, w+10 );
               gas.flux2( w+0,w+3, 0., w0,wk, q0,qk, x0x,xkx, q0x,qkx, vk,uk,dk );

               u0[0]= ui[0]+uk[0];
               u0[1]= ui[1]+uk[1];
               u0[2]= ui[2]+uk[2];
               u0[3]= ui[3]+uk[3];
               u0[4]= ui[4]+uk[4];

               v0[0]= vi[0]+vk[0];
               v0[1]= vi[1]+vk[1];
               v0[2]= vi[2]+vk[2];
               v0[3]= vi[3]+vk[3];
               v0[4]= vi[4]+vk[4];

               d0[0]= di[0]+dk[0];
               d0[1]= di[1]+dk[1];

               so[0].subv(i,  j,  k,  t,  r, v0 );
               so[0].addv(i+1,j,  k,  t,  r, vi );
               so[0].addv(i,  j,  k+1,t,  r, vk );

               so[0].subv(i,  j,  k,  t,  a, u0 );
               so[0].addv(i+1,j,  k,  t,  a, ui );
               so[0].addv(i,  j,  k+1,t,  a, uk );

                  ld.addv(i,  j,  k,  t,lhd, d0 );
                  ld.addv(i+1,j,  k,  t,lhd, di );
                  ld.addv(i,  j,  k+1,t,lhd, dk );

           }
        }

         k=bsize-1;
         for( j=0;j<bsize-1;j++ )
        {
            for( i=0;i<bsize-1;i++ )
           {
               co[0].loadv(i+1,j,  k,  t, x,x1 );
               co[0].loadv(i,  j+1,k,  t, x,x2 );
               co[0].loadv(i+1,j+1,k,  t, x,x3 );

               co[0].loadv(i+1,j,  k+1,t, x,x5 );
               co[0].loadv(i,  j+1,k+1,t, x,x6 );
               co[0].loadv(i+1,j+1,k+1,t, x,x7 );

               mw[0].loadv(i,  j,  k,  t,wm,w0  );
               mw[0].loadv(i+1,j,  k,  t,wm,wi  );
               mw[0].loadv(i,  j+1,k,  t,wm,wj  );

               so[0].loadv(i,  j,  k,  t,v, q0 );
               so[0].loadv(i+1,j,  k,  t,v, qi );
               so[0].loadv(i,  j+1,k,  t,v, qj );

               xd[0].loadv(i,  j,  k,  t,dxdx, x0x );
               xd[0].loadv(i+1,j,  k,  t,dxdx, xix );
               xd[0].loadv(i,  j+1,k,  t,dxdx, xjx );

               sd[0].loadv(i,  j,  k,  t,dvdx, q0x );
               sd[0].loadv(i+1,j,  k,  t,dvdx, qix );
               sd[0].loadv(i,  j+1,k,  t,dvdx, qjx );

               fmetric0( 0.5,0.5, x1,x3,x5,x7, w+0, w+3, w[6], w+7, w+10 );
               gas.flux2( w+0,w+3, 0., w0,wi, q0,qi, x0x,xix, q0x,qix, vi,ui,di );
          
               fmetric0( 0.5,0.5, x3,x2,x7,x6, w+0, w+3, w[6], w+7, w+10 );
               gas.flux2( w+0,w+3, 0., w0,wj, q0,qj, x0x,xjx, q0x,qjx, vj,uj,dj );

               u0[0]= ui[0]+uj[0];
               u0[1]= ui[1]+uj[1];
               u0[2]= ui[2]+uj[2];
               u0[3]= ui[3]+uj[3];
               u0[4]= ui[4]+uj[4];

               v0[0]= vi[0]+vj[0];
               v0[1]= vi[1]+vj[1];
               v0[2]= vi[2]+vj[2];
               v0[3]= vi[3]+vj[3];
               v0[4]= vi[4]+vj[4];

               d0[0]= di[0]+dj[0];
               d0[1]= di[1]+dj[1];

               so[0].subv(i,  j,  k,  t,  r, v0 );
               so[0].addv(i+1,j,  k,  t,  r, vi );
               so[0].addv(i,  j+1,k,  t,  r, vj );

               so[0].subv(i,  j,  k,  t,  a, u0 );
               so[0].addv(i+1,j,  k,  t,  a, ui );
               so[0].addv(i,  j+1,k,  t,  a, uj );

                  ld.addv(i,  j,  k,  t,lhd, d0 );
                  ld.addv(i+1,j,  k,  t,lhd, di );
                  ld.addv(i,  j+1,k,  t,lhd, dj );

           }
        }

         k=bsize-1;
         j=bsize-1;
         for( i=0;i<bsize-1;i++ )
        {
            co[0].loadv(i+1,j,  k,  t, x,x1 );
            co[0].loadv(i+1,j+1,k,  t, x,x3 );

            co[0].loadv(i+1,j,  k+1,t, x,x5 );
            co[0].loadv(i+1,j+1,k+1,t, x,x7 );

            mw[0].loadv(i,  j,  k,  t,wm,w0  );
            mw[0].loadv(i+1,j,  k,  t,wm,wi  );

            so[0].loadv(i,  j,  k,  t,v, q0 );
            so[0].loadv(i+1,j,  k,  t,v, qi );
            xd[0].loadv(i,  j,  k,  t,dxdx, x0x );
            xd[0].loadv(i+1,j,  k,  t,dxdx, xix );

            sd[0].loadv(i,  j,  k,  t,dvdx, q0x );
            sd[0].loadv(i+1,j,  k,  t,dvdx, qix );

            fmetric0( 0.5,0.5, x1,x3,x5,x7, w+0, w+3, w[6], w+7, w+10 );
            gas.flux2( w+0,w+3, 0., w0,wi, q0,qi, x0x,xix, q0x,qix, vi,ui,di );

            so[0].subv(i,  j,  k,  t,  r, vi );
            so[0].addv(i+1,j,  k,  t,  r, vi );

            so[0].subv(i,  j,  k,  t,  a, ui );
            so[0].addv(i+1,j,  k,  t,  a, ui );

               ld.addv(i,  j,  k,  t,lhd, di );
               ld.addv(i+1,j,  k,  t,lhd, di );


        }

         k=bsize-1;
         i=bsize-1;
         for( j=0;j<bsize-1;j++ )
        {
            co[0].loadv(i,  j+1,k,  t, x,x2 );
            co[0].loadv(i+1,j+1,k,  t, x,x3 );

            co[0].loadv(i,  j+1,k+1,t, x,x6 );
            co[0].loadv(i+1,j+1,k+1,t, x,x7 );

            mw[0].loadv(i,  j,  k,  t,wm,w0  );
            mw[0].loadv(i,  j+1,k,  t,wm,wj  );

            so[0].loadv(i,  j,  k,  t,v, q0 );
            so[0].loadv(i,  j+1,k,  t,v, qj );
            xd[0].loadv(i,  j,  k,  t,dxdx, x0x );
            xd[0].loadv(i,  j+1,k,  t,dxdx, xjx );

            sd[0].loadv(i,  j,  k,  t,dvdx, q0x );
            sd[0].loadv(i,  j+1,k,  t,dvdx, qjx );

            fmetric0( 0.5,0.5, x3,x2,x7,x6, w+0, w+3, w[6], w+7, w+10 );
            gas.flux2( w+0,w+3, 0., w0,wj, q0,qj, x0x,xjx, q0x,qjx, vj,uj,dj );

            so[0].subv(i,  j,  k,  t,  r, vj );
            so[0].addv(i,  j+1,k,  t,  r, vj );

            so[0].subv(i,  j,  k,  t,  a, uj );
            so[0].addv(i,  j+1,k,  t,  a, uj );

               ld.addv(i,  j,  k,  t,lhd, dj );
               ld.addv(i,  j+1,k,  t,lhd, dj );


        }

         j=bsize-1;
         i=bsize-1;
         for( k=0;k<bsize-1;k++ )
        {

            co[0].loadv(i,  j,  k+1,t, x,x4 );
            co[0].loadv(i+1,j,  k+1,t, x,x5 );
            co[0].loadv(i,  j+1,k+1,t, x,x6 );
            co[0].loadv(i+1,j+1,k+1,t, x,x7 );

            mw[0].loadv(i,  j,  k,  t,wm,w0  );
            mw[0].loadv(i,  j,  k+1,t,wm,wk  );

            so[0].loadv(i,  j,  k,  t,v, q0 );
            so[0].loadv(i,  j,  k+1,t,v, qk );
            xd[0].loadv(i,  j,  k,  t,dxdx, x0x );
            xd[0].loadv(i,  j,  k+1,t,dxdx, xkx );

            sd[0].loadv(i,  j,  k,  t,dvdx, q0x );
            sd[0].loadv(i,  j,  k+1,t,dvdx, qkx );

            fmetric0( 0.5,0.5, x4,x5,x6,x7, w+0, w+3, w[6], w+7, w+10 );
            gas.flux2( w+0,w+3, 0., w0,wk, q0,qk, x0x,xkx, q0x,qkx, vk,uk,dk );

            so[0].subv(i,  j,  k,  t,  r, vk );
            so[0].addv(i,  j,  k+1,t,  r, vk );

            so[0].subv(i,  j,  k,  t,  a, uk );
            so[0].addv(i,  j,  k+1,t,  a, uk );

               ld.addv(i,  j,  k,  t,lhd, dk );
               ld.addv(i,  j,  k+1,t,lhd, dk );

        }


     }

// handle interfaces with only local data dependencies
       
      while( locals( com[0],msg[0], ntsk,tasks ) )
     {

#pragma omp parallel for private(t,c,d,e,f,g,i,j,l,m,n,s,u,o,t0,t1,t2,w,  \
                                                                        w0, w1,\
                                                                   x0,x1,x2,x3,\
                                                                       x0x,x1x,\
                                                                        q0, q1,\
                                                                       q0x,q1x,\
                                                                        u0, v0, d0) schedule(dynamic)
         for( INT_ t=0;t<ntsk;t++ )
        {
            INT_    num,loc;
            INT_   *idx;
    
            com[0].task( tasks[t], num,loc, &idx );

            com[0].taskinfo( sd[0], idx[0], t0,c );
            com[0].taskinfo( sd[0], idx[1], t1,d );

            s= idx[0],
            t2= frule_t( com[0].hfx[s], bsize+1,bsize+1,bsize+1 );
            assert( c == com[0].list[s][2] );
            u=-1;
            o= com[0].list[s][1];
            if( hx[c].q[o].o ){ u=-u; };

            for( j=0;j<bsize;j++ )
           {
               for( i=0;i<bsize;i++ )
              {

                  t2.transform(   i,  j,0, l,m,n ); co[0].loadv( l,m,n,c,x,x0 );
                  t2.transform( 1+i,  j,0, l,m,n ); co[0].loadv( l,m,n,c,x,x1 );
                  t2.transform(   i,1+j,0, l,m,n ); co[0].loadv( l,m,n,c,x,x2 );
                  t2.transform( 1+i,1+j,0, l,m,n ); co[0].loadv( l,m,n,c,x,x3 );

                  t0.transform( i,j,0, l,m,n );
                  t1.transform( i,j,0, e,f,g );

                  mw[0].loadv(l,m,n,c,wm,w0  );
                  mw[0].loadv(e,f,g,d,wm,w1  );

                  so[0].loadv(l,m,n,  c,v,q0 );
                  so[0].loadv(e,f,g,  d,v,q1 );

                  xd[0].loadv(l,m,n,  c,dxdx, x0x );
                  xd[0].loadv(e,f,g,  d,dxdx, x1x );

                  sd[0].loadv(l,m,n,  c,dvdx, q0x );
                  sd[0].loadv(e,f,g,  d,dvdx, q1x );

                  fmetric0( 0.5,0.5, x0,x1,x2,x3, w+0, w+3, w[6], w+7, w+10 );

                  w[3]*= u;
                  w[4]*= u;
                  w[5]*= u;
    
                  gas.flux2( w+0,w+3, 0., w0,w1, q0,q1, x0x,x1x, q0x,q1x, v0,u0,d0 );

                  so[0].subv(l,m,n,c,  r, v0 );
                  so[0].addv(e,f,g,d,  r, v0 );

                  so[0].subv(l,m,n,c,  a, u0 );
                  so[0].addv(e,f,g,d,  a, u0 );

                     ld.addv(l,m,n,c,lhd, d0 );
                     ld.addv(e,f,g,d,lhd, d0 );

              }
           }
        }
     }

// handle interfaces with data dependencies on remote ranks

      while( transit( com[0],msg[0], ntsk,tasks, 1 ) )
     {
#pragma omp parallel for private(u,s,o,t,c,d,i,j,l,m,n,t0,t2,w,  \
                                                               w0, w1,\
                                                          x0,x1,x2,x3,\
                                                              x0x,x1x,\
                                                               q0, q1,\
                                                              q0x,q1x,\
                                                               u0, v0, d0) schedule(dynamic)
         for( INT_ t=0;t<ntsk;t++ )
        {
            INT_    num,loc;
            INT_   *idx;
    
            com[0].task( tasks[t], num,loc, &idx );
            com[0].taskinfo( sd[0], idx[0], t0,c );

            d= idx[1];

            s= idx[0],
            t2= frule_t( com[0].hfx[s], bsize+1,bsize+1,bsize+1 );
            assert( c == com[0].list[s][2] );
            u=-1;
            o= com[0].list[s][1];
            if( hx[c].q[o].o ){ u=-u; };
    
            for( j=0;j<bsize;j++ )
           {
               for( i=0;i<bsize;i++ )
              {
                  t2.transform(   i,  j,0, l,m,n ); co[0].loadv( l,m,n,c,x,x0 );
                  t2.transform( 1+i,  j,0, l,m,n ); co[0].loadv( l,m,n,c,x,x1 );
                  t2.transform(   i,1+j,0, l,m,n ); co[0].loadv( l,m,n,c,x,x2 );
                  t2.transform( 1+i,1+j,0, l,m,n ); co[0].loadv( l,m,n,c,x,x3 );
    
                  t0.transform( i,j,0, l,m,n );

                  mw[0].loadv(l,m,n,c,wm,w0  );
                  mw[1].loadv(i,j,0,d,wm,w1  );

                  so[0].loadv(l,m,n,     c,v, q0 );
                  so[1].loadv(i,j,0,     d,v, q1 );

                  xd[0].loadv(l,m,n,  c,dxdx, x0x );
                  xd[1].loadv(i,j,0,  d,dxdx, x1x );

                  sd[0].loadv(l,m,n,  c,dvdx, q0x );
                  sd[1].loadv(i,j,0,  d,dvdx, q1x );

                  fmetric0( 0.5,0.5, x0,x1,x2,x3, w+0, w+3, w[6], w+7, w+10 );

                  w[3]*= u;
                  w[4]*= u;
                  w[5]*= u;
    
                  gas.flux2( w+0,w+3, 0., w0,w1, q0,q1, x0x,x1x, q0x,q1x, v0,u0,d0 );

                  so[0].subv(l,m,n,c,  r, v0 );

                  so[0].subv(l,m,n,c,  a, u0 );

                     ld.addv(l,m,n,c,lhd, d0 );


              }
           }
        }
     }

/*    for( t= 0;t<hx.n;t++ )
     {
         printf( "\n" );
         printf( "%2d\n", hx[t].id ); 
         printf( "\n" );
         for( k=0;k<bsize;k++ )
        {
            for( j=0;j<bsize;j++ )
           {
               for( i=0;i<bsize;i++ )
              {
               so[0].loadv(i,  j,  k,  t,r, v0 );
               printf( " %2d %2d %2d % 9.3e % 9.3e % 9.3e % 9.3e % 9.3e\n",  
                       i,j,k, v0[0],v0[1],v0[2],v0[3],v0[4] );
              }
           }
        }
     }*/

      return;
  }
