
#  include <q3x/q3x.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::start()
  {

      size_t         len;

      INT_     j,k,l,m,n; 
      INT_       ist,ien;
      char          name[512];
      FILE            *f;

// open log file

      sprintf( name, "log/%06d.dat", rank );
      assert( !file );
      file= fopen( name,"w" );

// input section

      sprintf( name,"prep/%06d.bin", rank );

      FILE *o;
      o= fopen( name,"r" );

      assert( o );

// read message lists, dependencies and connection lists

    ::fread( &k,1,sizeof(INT_),o );
      assert( k == size );

      freadp( o );

      qv.n= com[0].n; qv.resize( qdum );
      eg.n= com[1].n; eg.resize( edum );
      vt.n= com[2].n; vt.resize( pdum );

      for( k=0;k<qv.n;k++ ){ qv[k].fread( o ); }
      for( k=0;k<eg.n;k++ ){ eg[k].fread( o ); }
      for( k=0;k<vt.n;k++ ){ vt[k].fread( o ); }

    ::fread( &hx.n,1,sizeof(INT_), o ); 
      hx.resize(hdum);
      for( k=0;k<hx.n;k++ )
     { 
         hx[k].fread( o ); 
         assert( hx[k].iz[0] > -1 ); 
     }

// read boundaries

      for( k=0;k<BTYPES;k++ ){ ::fread( bn.l[k],1,sizeof(INT_2),o ); }

      m= bn.l[BTYPES-1][0];
      bn.f= new frme_t[m];

      bn.q= new  INT_ [m];
      bn.h= new  INT_ [m];
      bn.g= new  INT_ [m];
      bn.b= new  INT_ [m];
      bn.n= new  INT_ [m];
      bn.s= new  INT_ [m];
      bn.p= new  INT_ [m];
      bn.r= new  INT_ [m];
      bn.i= new  INT_ [m];

      INT_12 buf;
      ien= 0;
      for( k=0;k<BTYPES;k++ )
     {
         ist= ien;
         ien= bn.l[k][0];

         for( j=ist;j<ien;j++ )
        {

          ::fread( buf,1,sizeof(INT_12),o );

            bn.h[j]= buf[0];
            bn.q[j]= buf[1];
            bn.g[j]= buf[2];
            bn.b[j]= buf[3]; 
            bn.n[j]= buf[4];
            bn.s[j]= buf[5];
            bn.p[j]= buf[6];
            bn.r[j]= buf[7];
            bn.i[j]= buf[8];

          ::fread( bn.f[j],1,sizeof(frme_t),o );

        }
     }
      nb= m;

// exahedra


      be= new INT_4[ com[1].n ];
    ::fread( be,com[1].n,sizeof(INT_4), o );

      lv= new INT_[ com[2].n ];
    ::fread( lv,com[2].n,sizeof(INT_), o );

      bv= new INT_2[ lv[com[2].n-1] ];
    ::fread( bv,lv[com[2].n-1],sizeof(INT_2), o );

    ::fread( &ni,1,sizeof(INT_),o );
      bj= new INT_8[ni];
      for( INT_ i=0;i<ni;i++ ){ ::fread( bj[i],1,sizeof(INT_8),o ); }

      fclose( o );

// done reading.

// start communications

      com[0].start( neig,ineg, hx.data(),file );
      com[1].start( neig,ineg, hx.data(),file );
      com[2].start( neig,ineg, hx.data(),file );

// scan grids

      k= 0;
      j= hx[k].id;
      MPI_File fh; 
      MPI_Status status;
      char fname[1024];

      sprintf( fname, "grid/h");
      MPI_File_open(MPI_COMM_WORLD, fname, MPI_MODE_RDONLY, MPI_INFO_NULL, &fh);
      assert( fh );
      MPI_File_read_at(fh, 0, &l, 1, INT_MPI, &status);
      MPI_Barrier( MPI_COMM_WORLD );
      MPI_File_close( &fh );

      f= fopen( "conjugate.dat","r" ); 
      assert( f );
      cjg.fread( f );
      fclose( f );

// create memory maps for coordinates, solution etc.

      bsize=l;
      m= com[0].loc();
      n= gas.nvar();

// Cell-centred fields

      len= 0; so[0].dims(                       len, l,     n,l,l, hx.n, -1 );
              so[1].halo( so[0], *this, com[0], len, l,     n,l,1,       -1 ); // solution arrays

      len= 0; mw[0].dims(                       len, l,WSIZE_,l,l, hx.n, -1 ); 
              mw[1].halo( mw[0], *this, com[0], len, l,WSIZE_,l,1,       -1 ); // volume and cell centre coordinates 

      len= 0; xd[0].dims(                       len, l,     6,l,l, hx.n, -1 ); 
              xd[1].halo( xd[0], *this, com[0], len, l,     6,l,1,       -1 ); // metric tensors for least squares

      len= 0; sd[0].dims(                       len, l,XDIM*n,l,l, hx.n, -1 ); 
              sd[1].halo( sd[0], *this, com[0], len, l,XDIM*n,l,1,       -1 ); // least square gradients

      len= 0;    ld.dims(                       len, l,     2,l,l, hx.n, -1 ); // LHS diagonal ( viscous and inviscid contributions separate)

      len= 0;    sb.dims(                       len, l,     n,l,     nb, -1 ); // boundary values
      len= 0;    bw.dims(                       len, l,MSIZE_,l,     nb, -1 ); // boundray faces
      len= 0;    cw.dims(                       len, l,MSIZE_,l,      m, -1 ); // connection faces

      if( cjg.n > 0 )
     {
          
         ist= bn.l[AWALLS-1][0];
         ien= bn.l[TWALLS-1][0];
         len= 0;    cj.dims(                    len, l, cjg.n,l,ien-ist,-1 ); // temperature eigenmodes for conjugate heat transfer
     }

// Node-centred fields

      l++;

      len= 0; co[0].dims(                       len, l,XDIM,l,l, hx.n, -1 ); // blocks
              co[1].halo( co[0], *this, com[0], len, l,XDIM,l,2,       -1 ); // faces
              co[2].halo( co[0], *this, com[1], len, l,XDIM,2,2,       -1 ); // edges
              co[3].halo( co[0], *this, com[2], len, 2,XDIM,2,2,       -1 ); // vertices

      len= 0;    cb.dims(                       len, l,BDIM,l,     nb, -1 );
      len= 0;    ci.dims(                       len, l,BDIM,l,     ni, -1 );

      len= 0; cd[0].dims(                       len, l,   1,l,l, hx.n, -1 ); // blocks
              cd[1].halo( cd[0], *this, com[0], len, l,   1,l,2,       -1 ); // faces
              cd[2].halo( cd[0], *this, com[1], len, l,   1,2,2,       -1 ); // edges
              cd[3].halo( cd[0], *this, com[2], len, 2,   1,2,2,       -1 ); // vertices

      len= 0; ca[0].dims(                       len, l,   9,l,l, hx.n, -1 ); // blocks
              ca[1].halo( ca[0], *this, com[0], len, l,   9,l,2,       -1 ); // faces
              ca[2].halo( ca[0], *this, com[1], len, l,   9,2,2,       -1 ); // edges
              ca[3].halo( ca[0], *this, com[2], len, 2,   9,2,2,       -1 ); // vertices

      len= 0; te[0].dims(                       len, l,   1,l,l, hx.n, -1 ); // blocks
              te[1].halo( te[0], *this, com[0], len, l,   1,l,2,       -1 ); // faces
              te[2].halo( te[0], *this, com[1], len, l,   1,2,2,       -1 ); // edges
              te[3].halo( te[0], *this, com[2], len, 2,   1,2,2,       -1 ); // vertices

      len= 0; td[0].dims(                       len, l,   1,l,l, hx.n, -1 ); // blocks
              td[1].halo( td[0], *this, com[0], len, l,   1,l,2,       -1 ); // faces
              td[2].halo( td[0], *this, com[1], len, l,   1,2,2,       -1 ); // edges
              td[3].halo( td[0], *this, com[2], len, 2,   1,2,2,       -1 ); // vertices

// prepare message spaces

      malloc( msg[ 0],so[0],com[0] );

      malloc( msg[ 1],co[0],com[0] );
      malloc( msg[ 2],co[0],com[1] );
      malloc( msg[ 3],co[0],com[2] );

      malloc( msg[ 4],ca[0],com[0] );
      malloc( msg[ 5],ca[0],com[1] );
      malloc( msg[ 6],ca[0],com[2] );

      malloc( msg[ 7],te[0],com[0] );
      malloc( msg[ 8],te[0],com[1] );
      malloc( msg[ 9],te[0],com[2] );

      malloc( msg[10],td[0],com[0] );
      malloc( msg[11],td[0],com[1] );
      malloc( msg[12],td[0],com[2] );

//    omp_set_num_threads( threads );

//    omp_set_num_threads( threads );
/*    printf( "Warning: no OMP_SET_NUM_THREADS!!!! %d\n", threads );*/
//    assert( threads<=1 );

// read run parameters

      f= fopen( "controls.dat","r" ); 
      assert( f );
      ctr.fread( f );
      fclose( f );

// block data

      f= fopen( "properties.dat","r" ); 
      assert( f );
      fscanf( f, "%d", &n );
      for( l=0;l<n;l++ )
     {
         da[l].fread( f );
     }
      fclose( f );

      return;
  }
