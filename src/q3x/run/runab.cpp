
#  include <q3x/q3x.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::runab()
  {

      REAL_          *v[3]={NULL,NULL,NULL};
      REAL_          *r[4]={NULL,NULL,NULL};
      REAL_          *s[4]={NULL,NULL,NULL};

      REAL_          *x=NULL,*d=NULL;
      REAL_          *y=NULL;
      REAL_          *yi=NULL;

      REAL_          *wb=NULL;
      REAL_          *vb[3]={NULL,NULL,NULL};
      REAL_          *rb[4]={NULL,NULL,NULL};
      REAL_         *vb0=NULL;
      REAL_          *wc=NULL;
      REAL_          *wm=NULL;

      REAL_        *dxdx=NULL;
      REAL_        *dvdx=NULL;

      REAL_           dt;
      REAL_2         sgm;
      REAL_          res[NVAR_+1];

      bool          flag;

      double         stime,etime;
      

      gas.init();

      start();

      co[3].malloc( &x  );
         cb.malloc( &y  );
         ci.malloc( &yi );

      mw[1].malloc( &wm ); 
         bw.malloc( &wb ); 
         cw.malloc( &wc ); 

      so[1].malloc( r+0 );
      so[1].malloc( r+1 );
      so[1].malloc( r+2 );
      so[1].malloc( r+3 );

      so[1].malloc( s+0 );
      so[1].malloc( s+1 );
      so[1].malloc( s+2 );
      so[1].malloc( s+3 );

      so[1].malloc( v+0 );
      so[1].malloc( v+1 );
      so[1].malloc( v+2 );

         ld.malloc( &d  );

         sb.malloc(  vb+0 );
         sb.malloc(  vb+1 );
         sb.malloc(  vb+2 );

         sb.malloc(  rb+0 );
         sb.malloc(  rb+1 );
         sb.malloc(  rb+2 );
         sb.malloc(  rb+3 );

         sb.malloc( &vb0);

      xd[1].malloc( &dxdx );
      sd[1].malloc( &dvdx );

      grid( x,y,yi );

      metrics( x, wm,wb,wc );
      grdx( wm,wb, dxdx );

      chk( wm,wb,wc, x,r[0] );

      binit( wb, vb0 );
      restart( v[0], vb[0], NULL );

      stime=MPI_Wtime();
      dt= -1;

      grdv( wm,wb, vb[0],v[0], dxdx,dvdx );
      rhs4( x, wm,wb,wc, dxdx, vb0, vb[0],v[0],dvdx, rb[0],r[0],s[0],d );
//    rhs3( x, wm,wb,wc, dxdx, vb0, vb[0],v[0],dvdx, rb[0],r[0],d );

      so[1].memcpy(  r[0], r[1] ); 
      so[1].memcpy(  r[0], r[2] ); 

         sb.memcpy( rb[0],rb[1] ); 
         sb.memcpy( rb[0],rb[2] ); 

      so[1].memcpy(  s[0], s[1] ); 
      so[1].memcpy(  s[0], s[2] ); 

      for( INT_ it=0;it<ctr.nt;it++ )
     {
         flag= ( it%ctr.no==0 );

         so[1].memset(   r[0],0 );
         so[1].memset(   s[0],0 );
            sb.memset(   rb[0],0 );
 
         grdv( wm,wb, vb[0],v[0], dxdx,dvdx );

         rhs4( x, wm,wb,wc, dxdx, vb0, vb[0],v[0],dvdx, rb[0],r[0],s[0],d );
//       rhs3( x, wm,wb,wc, dxdx, vb0, vb[0],v[0],dvdx, rb[0],r[0],d );

         delt( wm,  d,  ctr.sigma,  dt, sgm );

         so[1].memset(   r[3],0. );
            sb.memset(  rb[3],0. );

         so[0].sxpy(  r[3], 0.0, r[3],  23./12., r[0] ); 
         so[0].sxpy(  r[3], 1.0, r[3], -16./12., r[1] ); 
         so[0].sxpy(  r[3], 1.0, r[3],   5./12., r[2] ); 
                     
         so[0].sxpy(  r[3], 1.0, r[3],  23./12., s[0] ); 
         so[0].sxpy(  r[3], 1.0, r[3], -16./12., s[1] ); 
         so[0].sxpy(  r[3], 1.0, r[3],   5./12., s[2] ); 

            sb.sxpy( rb[3], 0.0,rb[3],  23./12.,rb[0] );
            sb.sxpy( rb[3], 1.0,rb[3], -16./12.,rb[1] );
            sb.sxpy( rb[3], 1.0,rb[3],   5./12.,rb[2] );

         upd( wm,wb, vb[0],v[0], vb[0],v[0], rb[3],r[3], dt, ctr.sigma, res, flag );

         so[1].memcpy(  r[1], r[2] ); 
         so[1].memcpy(  r[0], r[1] ); 

         so[1].memcpy(  s[1], s[2] ); 
         so[1].memcpy(  s[0], s[1] ); 
  
            sb.memcpy( rb[1],rb[2] ); 
            sb.memcpy( rb[0],rb[1] ); 
  

         if( flag )
        { 
            if( rank == 0 )
           {
               printf( "%9d %9.3e %9.3e %9.3e %9.3e %9.3e %9.3e %9.3e %9.3e \n", it, dt, sgm[0]*dt, sgm[1]*dt, res[0],res[1],res[2],res[3],res[4] );
           }
        } 

         if( it%ctr.ns == 0 )
        {
            save( v[0], vb[0], NULL, it );
        }
     }

      save( v[0], vb[0], NULL, -1 );

      etime=MPI_Wtime();

      if( rank==0 )
     {
         printf( "elapsed time %9.3e\n", etime-stime );
     }

      delete[] x;
      delete[] y;
      delete[] yi;
      delete[] v[0];
      delete[] v[1];
      delete[] v[2];
      delete[] vb[0];
      delete[] vb[1];
      delete[] vb[2];
      delete[] r[0];
      delete[] r[1];
      delete[] r[2];
      delete[] r[3];
      delete[] rb[0];
      delete[] rb[1];
      delete[] rb[2];
      delete[] rb[3];
      delete[] d;
      delete[] wm;
      delete[] wb;
      delete[] wc;

      delete[] dxdx;
      delete[] dvdx;

      stop();

      return;
  }
