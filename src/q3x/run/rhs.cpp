
#  include <q3x/q3x.h>
#  include <q3x/inline/fmetric0>

   void q3x_t::rhs( REAL_ *x, REAL_ *wm, REAL_ *wb, REAL_ *wc, 
                    REAL_ *vb0, REAL_ *vb,
                    REAL_  *v, REAL_ *rb, REAL_  *r, REAL_ *lhd )
  {

      INT_     ntsk;
      INT_     tasks[NTASK];
      REAL_3   x1,x2,x3,x4,x5,x6,x7;
      REAL_    q0[NVAR_],q1[NVAR_],qi[NVAR_],qj[NVAR_],qk[NVAR_];
      REAL_    f0[NVAR_],fi[NVAR_],fj[NVAR_],fk[NVAR_];
      REAL_2   l0,li,lj,lk;
      REAL_    w[MSIZE_];
      INT_     c,d, e,f,g, i,j,k, l,m,n, t;
      INT_     ist,ien;
      frule_t  t0,t1;

      REAL_   qb[NVAR_];

      exchange(  v, so[1], com[0], msg[0] );

      so[1].memset(   r,0 );
         ld.memset( lhd,0 );

// boundary loop -find way to do with openmp

      ien= 0;
      for( k=0;k<FREES;k++ )
     {
         ist= ien;
         ien= bn.l[k][0];
         for( INT_ t=ist;t<ien;t++ )
        {
    
            t0= frule_t( bn.f[t], bsize,bsize,bsize );
            c= bn.h[t];
            if( c > -1 )
           {  
               for( j=0;j<bsize;j++ )
              {
                  for( i=0;i<bsize;i++ )
                 {
                     t0.transform( i,j,0, l,m,n );
                     so[0].loadv(l,m,n,  c, v,q0 );
                        bw.loadv(i,j,    t,wb, w );
                        sb.loadv(i,j,    t,vb0,qb );
             
                     gas.IFLUX( q0,qb, w+3,0., f0,l0 );
             
                     so[0].subv(l,m,n,c,   r, f0 );
                        ld.addv(l,m,n,c, lhd, l0 );
             
                 }
              }
           }
        }
     }
      for( k=FREES;k<BTYPES;k++ )
     {
         ist= ien;
         ien= bn.l[k][0];
         for( INT_ t=ist;t<ien;t++ )
        {
    
            t0= frule_t( bn.f[t], bsize,bsize,bsize );
            c= bn.h[t];
            if( c > -1 )
           {   
               for( j=0;j<bsize;j++ )
              {
                  for( i=0;i<bsize;i++ )
                 {
                     t0.transform( i,j,0, l,m,n );
                     so[0].loadv(l,m,n,  c, v,q0 );
                        bw.loadv(i,j,    t,wb, w );
             
                     gas.WFLUX( q0, w+3,0., f0,l0 );
             
                     so[0].subv(l,m,n,c,   r, f0 );
                        ld.addv(l,m,n,c, lhd, l0 );
             
                 }
              }
           }
        }
     }

#pragma omp parallel for private(t,i,j,k,q0,qi,qj,qk,w,f0,fi,fj,fk,l0,li,lj,lk,\
                                 x1,x2,x3,x4,x5,x6,x7) schedule(dynamic)
      for( t=0;t<hx.n;t++ )
     {

         for( k=0;k<bsize-1;k++ )
        {
            for( j=0;j<bsize-1;j++ )
           {
               for( i=0;i<bsize-1;i++ )
              {
                  co[0].loadv(i+1,j,  k,  t, x,x1 );
                  co[0].loadv(i,  j+1,k,  t, x,x2 );
                  co[0].loadv(i+1,j+1,k,  t, x,x3 );

                  co[0].loadv(i,  j,  k+1,t, x,x4 );
                  co[0].loadv(i+1,j,  k+1,t, x,x5 );
                  co[0].loadv(i,  j+1,k+1,t, x,x6 );
                  co[0].loadv(i+1,j+1,k+1,t, x,x7 );

                  so[0].loadv(i,  j,  k,  t,v, q0 );
                  so[0].loadv(i+1,j,  k,  t,v, qi );
                  so[0].loadv(i,  j+1,k,  t,v, qj );
                  so[0].loadv(i,  j,  k+1,t,v, qk );

                  fmetric0( 0.5,0.5, x1,x3,x5,x7, w+0, w+3, w[6], w+7, w+10 );
                  gas.IFLUX( q0,qi, w+3,0., fi,li );

                  fmetric0( 0.5,0.5, x3,x2,x7,x6, w+0, w+3, w[6], w+7, w+10 );
                  gas.IFLUX( q0,qj, w+3,0., fj,lj );

                  fmetric0( 0.5,0.5, x4,x5,x6,x7, w+0, w+3, w[6], w+7, w+10 );
                  gas.IFLUX( q0,qk, w+3,0., fk,lk );

                  f0[0]= fi[0]+fj[0]+fk[0];
                  f0[1]= fi[1]+fj[1]+fk[1];
                  f0[2]= fi[2]+fj[2]+fk[2];
                  f0[3]= fi[3]+fj[3]+fk[3];
                  f0[4]= fi[4]+fj[4]+fk[4];

                  so[0].subv(i,  j,  k,  t,  r, f0 );
                  so[0].addv(i+1,j,  k,  t,  r, fi );
                  so[0].addv(i,  j+1,k,  t,  r, fj );
                  so[0].addv(i,  j,  k+1,t,  r, fk );

                  l0[0]= li[0]+ lj[0]+ lk[0];
                  l0[1]= li[1]+ lj[1]+ lk[1];
                     ld.addv(i,  j,  k,  t,lhd, l0 );
                     ld.addv(i+1,j,  k,  t,lhd, li );
                     ld.addv(i,  j+1,k,  t,lhd, lj );
                     ld.addv(i,  j,  k+1,t,lhd, lk );

              }
           }
        }

         i=bsize-1;
         for( k=0;k<bsize-1;k++ )
        {
            for( j=0;j<bsize-1;j++ )
           {

               co[0].loadv(i,  j+1,k,  t, x,x2 );
               co[0].loadv(i+1,j+1,k,  t, x,x3 );

               co[0].loadv(i,  j,  k+1,t, x,x4 );
               co[0].loadv(i+1,j,  k+1,t, x,x5 );
               co[0].loadv(i,  j+1,k+1,t, x,x6 );
               co[0].loadv(i+1,j+1,k+1,t, x,x7 );

               so[0].loadv(i,  j,  k,  t,v, q0 );
               so[0].loadv(i,  j+1,k,  t,v, qj );
               so[0].loadv(i,  j,  k+1,t,v, qk );

               fmetric0( 0.5,0.5, x3,x2,x7,x6, w+0, w+3, w[6], w+7, w+10 );
               gas.IFLUX( q0,qj, w+3,0., fj,lj );

               fmetric0( 0.5,0.5, x4,x5,x6,x7, w+0, w+3, w[6], w+7, w+10 );
               gas.IFLUX( q0,qk, w+3,0., fk,lk );

               f0[0]= fj[0]+fk[0];
               f0[1]= fj[1]+fk[1];
               f0[2]= fj[2]+fk[2];
               f0[3]= fj[3]+fk[3];
               f0[4]= fj[4]+fk[4];

               so[0].subv(i,  j,  k,  t,  r, f0 );
               so[0].addv(i,  j+1,k,  t,  r, fj );
               so[0].addv(i,  j,  k+1,t,  r, fk );

               l0[0]= lj[0]+ lk[0];
               l0[1]= lj[1]+ lk[1];

                  ld.addv(i,  j,  k,  t,lhd, l0 );
                  ld.addv(i,  j+1,k,  t,lhd, lj );
                  ld.addv(i,  j,  k+1,t,lhd, lk );

           }
        }

         j=bsize-1;
         for( k=0;k<bsize-1;k++ )
        {
            for( i=0;i<bsize-1;i++ )
           {
               co[0].loadv(i+1,j,  k,  t, x,x1 );
               co[0].loadv(i+1,j+1,k,  t, x,x3 );

               co[0].loadv(i,  j,  k+1,t, x,x4 );
               co[0].loadv(i+1,j,  k+1,t, x,x5 );
               co[0].loadv(i,  j+1,k+1,t, x,x6 );
               co[0].loadv(i+1,j+1,k+1,t, x,x7 );

               so[0].loadv(i,  j,  k,  t,v, q0 );
               so[0].loadv(i+1,j,  k,  t,v, qi );
               so[0].loadv(i,  j,  k+1,t,v, qk );

               fmetric0( 0.5,0.5, x1,x3,x5,x7, w+0, w+3, w[6], w+7, w+10 );
               gas.IFLUX( q0,qi, w+3,0., fi,li );

               fmetric0( 0.5,0.5, x4,x5,x6,x7, w+0, w+3, w[6], w+7, w+10 );
               gas.IFLUX( q0,qk, w+3,0., fk,lk );

               f0[0]= fi[0]+fk[0];
               f0[1]= fi[1]+fk[1];
               f0[2]= fi[2]+fk[2];
               f0[3]= fi[3]+fk[3];
               f0[4]= fi[4]+fk[4];

               so[0].subv(i,  j,  k,  t,  r, f0 );
               so[0].addv(i+1,j,  k,  t,  r, fi );
               so[0].addv(i,  j,  k+1,t,  r, fk );

               l0[0]= li[0]+ lk[0];
               l0[1]= li[1]+ lk[1];
                  ld.addv(i,  j,  k,  t,lhd, l0 );
                  ld.addv(i+1,j,  k,  t,lhd, li );
                  ld.addv(i,  j,  k+1,t,lhd, lk );

           }
        }

         k=bsize-1;
         for( j=0;j<bsize-1;j++ )
        {
            for( i=0;i<bsize-1;i++ )
           {
               co[0].loadv(i+1,j,  k,  t, x,x1 );
               co[0].loadv(i,  j+1,k,  t, x,x2 );
               co[0].loadv(i+1,j+1,k,  t, x,x3 );

               co[0].loadv(i+1,j,  k+1,t, x,x5 );
               co[0].loadv(i,  j+1,k+1,t, x,x6 );
               co[0].loadv(i+1,j+1,k+1,t, x,x7 );

               so[0].loadv(i,  j,  k,  t,v, q0 );
               so[0].loadv(i+1,j,  k,  t,v, qi );
               so[0].loadv(i,  j+1,k,  t,v, qj );


               fmetric0( 0.5,0.5, x1,x3,x5,x7, w+0, w+3, w[6], w+7, w+10 );
               gas.IFLUX( q0,qi, w+3,0., fi,li );

               fmetric0( 0.5,0.5, x3,x2,x7,x6, w+0, w+3, w[6], w+7, w+10 );
               gas.IFLUX( q0,qj, w+3,0., fj,lj );

               f0[0]= fi[0]+fj[0];
               f0[1]= fi[1]+fj[1];
               f0[2]= fi[2]+fj[2];
               f0[3]= fi[3]+fj[3];
               f0[4]= fi[4]+fj[4];

               so[0].subv(i,  j,  k,  t,  r, f0 );
               so[0].addv(i+1,j,  k,  t,  r, fi );
               so[0].addv(i,  j+1,k,  t,  r, fj );

               l0[0]= li[0]+ lj[0];
               l0[1]= li[1]+ lj[1];
                  ld.addv(i,  j,  k,  t,lhd, l0 );
                  ld.addv(i+1,j,  k,  t,lhd, li );
                  ld.addv(i,  j+1,k,  t,lhd, lj );

           }
        }

         k=bsize-1;
         j=bsize-1;
         for( i=0;i<bsize-1;i++ )
        {
            co[0].loadv(i+1,j,  k,  t, x,x1 );
            co[0].loadv(i+1,j+1,k,  t, x,x3 );

            co[0].loadv(i+1,j,  k+1,t, x,x5 );
            co[0].loadv(i+1,j+1,k+1,t, x,x7 );

            so[0].loadv(i,  j,  k,  t,v, q0 );
            so[0].loadv(i+1,j,  k,  t,v, qi );

            fmetric0( 0.5,0.5, x1,x3,x5,x7, w+0, w+3, w[6], w+7, w+10 );
            gas.IFLUX( q0,qi, w+3,0., f0,l0 );

            so[0].subv(i,  j,  k,  t,  r,f0 );
            so[0].addv(i+1,j,  k,  t,  r,f0 );

               ld.addv(i,  j,  k,  t,lhd, l0 );
               ld.addv(i+1,j,  k,  t,lhd, l0 );

        }

         k=bsize-1;
         i=bsize-1;
         for( j=0;j<bsize-1;j++ )
        {
            co[0].loadv(i,  j+1,k,  t, x,x2 );
            co[0].loadv(i+1,j+1,k,  t, x,x3 );

            co[0].loadv(i,  j+1,k+1,t, x,x6 );
            co[0].loadv(i+1,j+1,k+1,t, x,x7 );

            so[0].loadv(i,  j,  k,  t,v, q0 );
            so[0].loadv(i,  j+1,k,  t,v, qj );

            fmetric0( 0.5,0.5, x3,x2,x7,x6, w+0, w+3, w[6], w+7, w+10 );
            gas.IFLUX( q0,qj, w+3,0., f0,l0 );

            so[0].subv(i,  j,  k,  t,  r,f0 );
            so[0].addv(i,  j+1,k,  t,  r,f0 );

               ld.addv(i,  j,  k,  t,lhd,l0 );
               ld.addv(i,  j+1,k,  t,lhd,l0 );

        }

         j=bsize-1;
         i=bsize-1;
         for( k=0;k<bsize-1;k++ )
        {

            co[0].loadv(i,  j,  k+1,t, x,x4 );
            co[0].loadv(i+1,j,  k+1,t, x,x5 );
            co[0].loadv(i,  j+1,k+1,t, x,x6 );
            co[0].loadv(i+1,j+1,k+1,t, x,x7 );

            so[0].loadv(i,  j,  k,  t,v, q0 );
            so[0].loadv(i,  j,  k+1,t,v, qk );

            fmetric0( 0.5,0.5, x4,x5,x6,x7, w+0, w+3, w[6], w+7, w+10 );
            gas.IFLUX( q0,qk, w+3,0., f0,l0 );

            so[0].subv(i,  j,  k,  t,  r,f0 );
            so[0].addv(i,  j,  k+1,t,  r,f0 );

               ld.addv(i,  j,  k,  t,lhd,l0 );
               ld.addv(i,  j,  k+1,t,lhd,l0 );

        }

     }

// handle interfaces with only local data dependencies
       
      while( locals( com[0],msg[0], ntsk,tasks ) )
     {

#pragma omp parallel for private(t,c,d,e,f,g,i,j,l,m,n,t0,t1,q0,q1,w,f0,l0) schedule(dynamic)
         for( INT_ t=0;t<ntsk;t++ )
        {
            INT_    num,loc;
            INT_   *idx;
    
            com[0].task( tasks[t], num,loc, &idx );
            com[0].taskinfo( so[0], idx[0], t0,c );
            com[0].taskinfo( so[0], idx[1], t1,d );
    
            for( j=0;j<bsize;j++ )
           {
               for( i=0;i<bsize;i++ )
              {
                  t0.transform( i,j,0, l,m,n );
                  t1.transform( i,j,0, e,f,g );

                  so[0].loadv(l,m,n,  c,v,q0 );
                  so[0].loadv(e,f,g,  d,v,q1 );
                     cw.loadv(i,j,  idx[0], wc,w );
    
                  gas.IFLUX( q0,q1, w+3,0., f0,l0 );

                  so[0].subv(l,m,n,c,  r,f0 );
                  so[0].addv(e,f,g,d,  r,f0 );

                     ld.addv(l,m,n,c,lhd,l0 );
                     ld.addv(e,f,g,d,lhd,l0 );

              }
           }
        }
     }

// handle interfaces with data dependencies on remote ranks

      while( transit( com[0],msg[0], ntsk,tasks, 1 ) )
     {
#pragma omp parallel for private(t,c,d,i,j,l,m,n,t0,q0,q1,w,f0,l0) schedule(dynamic)
         for( INT_ t=0;t<ntsk;t++ )
        {
            INT_    num,loc;
            INT_   *idx;
    
            com[0].task( tasks[t], num,loc, &idx );
            com[0].taskinfo( so[0], idx[0], t0,c );
            d= idx[1];
    
            for( j=0;j<bsize;j++ )
           {
               for( i=0;i<bsize;i++ )
              {
    
                  t0.transform( i,j,0, l,m,n );

                  so[0].loadv(l,m,n,     c,v,q0 );
                  so[1].loadv(i,j,0,     d,v,q1 );
                     cw.loadv(i,j,  idx[0], wc,w );
    
                  gas.IFLUX( q0,q1, w+3,0., f0,l0 );

                  so[0].subv(l,m,n,c,  r, f0 );
                     ld.addv(l,m,n,c,lhd, l0 );

              }
           }
        }
     }

      return;
  }
