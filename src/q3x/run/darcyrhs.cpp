
#  include <q3x/q3x.h>

   void q3x_t::darcyrhs( REAL_ *wm, REAL_ *wf, REAL_ *wb, REAL_ *wc, REAL_ *dxdx,
                    REAL_ *vb0, REAL_ *vb, 
                    REAL_  *v, REAL_ *dvdx, REAL_ *rb, REAL_  *r, REAL_ *lhd )
  {

      INT_    ntsk;
      INT_    tasks[NTASK];

      REAL_   f0[NVAR_],fi[NVAR_],fj[NVAR_],fk[NVAR_],f1[NVAR_];
      REAL_   g0[NVAR_],gi[NVAR_],gj[NVAR_],gk[NVAR_];

      REAL_     q2[ NVAR_], q3[  NVAR_], g2[  NVAR_], g3[  NVAR_];

      REAL_    q0[  NVAR_], q1[  NVAR_], qi[  NVAR_], qj[  NVAR_], qk[  NVAR_];

      REAL_   q0x[3*NVAR_],q1x[3*NVAR_],qix[3*NVAR_],qjx[3*NVAR_],qkx[3*NVAR_];
      REAL_   x0x[      6],x1x[      6],xix[      6],xjx[      6],xkx[      6];

      REAL_   w0[3*8] ,w1[3*8] ,wi[3*8] ,wj[3*8] ,wk[3*8];

      REAL_   l0,li,lj,lk,l1, d0,di,dj,dk;
      REAL_   phi;
      REAL_   aa;
      REAL_   kd;
      REAL_   kf;
      REAL_   w[1+3+3*8];
      REAL_   vol[1+3];
      INT_    c,d, e,f,g, i,j,k, l,m,n, t, z,y;
      INT_    ist,ien;
      frule_t t0,t1;

      REAL_   qb[NVAR_];

      exchange(  v, so[1], com[0], msg[0] );

      so[1].memset(   r,0 );
         ld.memset( lhd,0 );

// boundary loop -find way to do with openmp

      ien= 0;
      for( k=0;k<FREES;k++ )
     {
         ist= ien;
         ien= bn.l[k][0];
         for( INT_ t=ist;t<ien;t++ )
        {
    
            t0= frule_t( bn.f[t], bsize,bsize,bsize );
            c= bn.h[t];
            if( c > -1 )
           {  

               z= hx[c].iz[0];
               phi= da[z].phi;

               for( j=0;j<bsize;j++ )
              {
                  for( i=0;i<bsize;i++ )
                 {
                     t0.transform( i,j,0, l,m,n );
                     mw[0].loadv(l,m,n,  c,wm,w0  );
                     so[0].loadv(l,m,n,  c, v,q0 );


                        bw.loadv(i,j,    t,wb, w );
                        sb.loadv(i,j,    t,vb0,qb );
             
//                   printf( "%2d %6d %2d %2d % 9.3e (i)\n", k,t, i,j, (w0[0]-w[0])*w[3]+ (w0[1]-w[1])*w[4]+ (w0[2]-w[2])*w[5] );

//                   gas.IFLUX( q0,qb, w+3,0., f0,l0 );
             
                     so[0].fmav(l,m,n,c,   r, -phi,f0 );
                        ld.addv(l,m,n,c, lhd,&l0 );
             
                 }
              }
           }
        }
     }
      for( k=FREES;k<FREES+1;k++ )
     {
         ist= ien;
         ien= bn.l[k][0];
         for( INT_ t=ist;t<ien;t++ )
        {
    
            t0= frule_t( bn.f[t], bsize,bsize,bsize );
            c= bn.h[t];
            if( c > -1 )
           {   
               z= hx[c].iz[0];
               phi= da[z].phi;

               for( j=0;j<bsize;j++ )
              {
                  for( i=0;i<bsize;i++ )
                 {
                     t0.transform( i,j,0, l,m,n );
                     mw[0].loadv(l,m,n,  c,wm,w0  );

                     so[0].loadv(l,m,n,  c, v,q0 );
                        bw.loadv(i,j,    t,wb, w );
             
//                   printf( "%2d %6d %2d %2d % 9.3e (w)\n", k,t, i,j, (w0[0]-w[0])*w[3]+ (w0[1]-w[1])*w[4]+ (w0[2]-w[2])*w[5] );

//                   gas.WFLUX( q0, w+3,0., f0,l0 );
             
                     so[0].fmav(l,m,n,c,   r, -phi,f0 );
                        ld.addv(l,m,n,c, lhd,&l0 );
             
                 }
              }
           }
        }
     }
      for( k=FREES+1;k<BTYPES;k++ )
     {
         ist= ien;
         ien= bn.l[k][0];
         for( INT_ t=ist;t<ien;t++ )
        {
    
            t0= frule_t( bn.f[t], bsize,bsize,bsize );
            c= bn.h[t];
            if( c > -1 )
           {   
               z= hx[c].iz[0];
               phi= da[z].phi;

               for( j=0;j<bsize;j++ )
              {
                  for( i=0;i<bsize;i++ )
                 {
                     t0.transform( i,j,0, l,m,n );

                     mw[0].loadv(l,m,n,  c,wm,w0  );
                     so[0].loadv(l,m,n,  c, v,q0 );

                        bw.loadv(i,j,    t,wb, w );
             
//                   printf( "%2d %6d %2d %2d % 9.3e (f)\n", k,t, i,j, (w0[0]-w[0])*w[3]+ (w0[1]-w[1])*w[4]+ (w0[2]-w[2])*w[5] );

//                   gas.FFLUX( w0,q0, w, f0,l0 );
             
                     so[0].fmav(l,m,n,c,   r, -phi,f0 );
                        ld.addv(l,m,n,c, lhd,&l0 );
             
                 }
              }
           }
        }
     }

#pragma omp parallel for private(t,i,j,k,q0,qi,qj,qk,w,w0,wi,wj,wk,x0x,xix,xjx,xkx,q0x,qix,qjx,qkx,f0,fi,fj,fk,g0,gi,gj,gk,l0,li,lj,lk,d0,di,dj,dk,z,phi,kd,kf,vol,f1,l1) schedule(dynamic)
      for( t=0;t<hx.n;t++ )
     {

         z= hx[t].iz[0];
         phi= da[z].phi;
         kd= da[z].kd;
         kf= da[z].kf;

         for( k=0;k<bsize-1;k++ )
        {
            for( j=0;j<bsize-1;j++ )
           {
               for( i=0;i<bsize-1;i++ )
              {

                  mw[0].loadv(i,  j,  k,  t,wm,w0  );
                  mw[0].loadv(i+1,j,  k,  t,wm,wi  );
                  mw[0].loadv(i,  j+1,k,  t,wm,wj  );
                  mw[0].loadv(i,  j,  k+1,t,wm,wk  );

                  so[0].loadv(i,  j,  k,  t,v, q0 );
                  so[0].loadv(i+1,j,  k,  t,v, qi );
                  so[0].loadv(i,  j+1,k,  t,v, qj );
                  so[0].loadv(i,  j,  k+1,t,v, qk );

                  xd[0].loadv(i,  j,  k,  t,dxdx, x0x );
                  xd[0].loadv(i+1,j,  k,  t,dxdx, xix );
                  xd[0].loadv(i,  j+1,k,  t,dxdx, xjx );
                  xd[0].loadv(i,  j,  k+1,t,dxdx, xkx );

                  sd[0].loadv(i,  j,  k,  t,dvdx, q0x );
                  sd[0].loadv(i+1,j,  k,  t,dvdx, qix );
                  sd[0].loadv(i,  j+1,k,  t,dvdx, qjx );
                  sd[0].loadv(i,  j,  k+1,t,dvdx, qkx );

//                   fw.loadv(i,  j,  k,  t,wf,w  );

//                gas.IFLUX( q0,qi, w+I+3,0., fi,li );
//                gas.IFLUX( q0,qj, w+J+3,0., fj,lj );
//                gas.IFLUX( q0,qk, w+K+3,0., fk,lk );
//
//                gas.VFLUX( w0,x0x,q0,q0x, wi,xix,qi,qix, w+I, gi,di );
//                gas.VFLUX( w0,x0x,q0,q0x, wj,xjx,qj,qjx, w+J, gj,dj );
//                gas.VFLUX( w0,x0x,q0,q0x, wk,xkx,qk,qkx, w+K, gk,dk );

                  gas.DARCS( q0,w0[3], kd,kf, f1,l1 );

                  fi[0]+= gi[0];
                  fi[1]+= gi[1];
                  fi[2]+= gi[2];
                  fi[3]+= gi[3];
                  fi[4]+= gi[4];

                  fj[0]+= gj[0];
                  fj[1]+= gj[1];
                  fj[2]+= gj[2];
                  fj[3]+= gj[3];
                  fj[4]+= gj[4];

                  fk[0]+= gk[0];
                  fk[1]+= gk[1];
                  fk[2]+= gk[2];
                  fk[3]+= gk[3];
                  fk[4]+= gk[4];

                  li+= di;
                  lj+= dj;
                  lk+= dk;

                  f0[0]= fi[0]+fj[0]+fk[0]+f1[0];
                  f0[1]= fi[1]+fj[1]+fk[1]+f1[1];
                  f0[2]= fi[2]+fj[2]+fk[2]+f1[2];
                  f0[3]= fi[3]+fj[3]+fk[3]+f1[3];
                  f0[4]= fi[4]+fj[4]+fk[4]+f1[4];

                  so[0].fmav(i,  j,  k,  t,  r, -phi,f0 );
                  so[0].fmav(i+1,j,  k,  t,  r,  phi,fi );
                  so[0].fmav(i,  j+1,k,  t,  r,  phi,fj );
                  so[0].fmav(i,  j,  k+1,t,  r,  phi,fk );

                  l0= li+ lj+ lk+ l1;
                     ld.addv(i,  j,  k,  t,lhd,&l0 );
                     ld.addv(i+1,j,  k,  t,lhd,&li );
                     ld.addv(i,  j+1,k,  t,lhd,&lj );
                     ld.addv(i,  j,  k+1,t,lhd,&lk );

              }
           }
        }

         i=bsize-1;
         for( k=0;k<bsize-1;k++ )
        {
            for( j=0;j<bsize-1;j++ )
           {

               mw[0].loadv(i,  j,  k,  t,wm,w0  );
               mw[0].loadv(i,  j+1,k,  t,wm,wj  );
               mw[0].loadv(i,  j,  k+1,t,wm,wk  );

               so[0].loadv(i,  j,  k,  t,v, q0 );
               so[0].loadv(i,  j+1,k,  t,v, qj );
               so[0].loadv(i,  j,  k+1,t,v, qk );

               xd[0].loadv(i,  j,  k,  t,dxdx, x0x );
               xd[0].loadv(i,  j+1,k,  t,dxdx, xjx );
               xd[0].loadv(i,  j,  k+1,t,dxdx, xkx );

               sd[0].loadv(i,  j,  k,  t,dvdx, q0x );
               sd[0].loadv(i,  j+1,k,  t,dvdx, qjx );
               sd[0].loadv(i,  j,  k+1,t,dvdx, qkx );

//                fw.loadv(i,  j,  k,  t,wf,w );

//             gas.IFLUX( q0,qj, w+J+3,0., fj,lj );
//             gas.IFLUX( q0,qk, w+K+3,0., fk,lk );
//
//             gas.VFLUX( w0,x0x,q0,q0x, wj,xjx,qj,qjx, w+J, gj,dj );
//             gas.VFLUX( w0,x0x,q0,q0x, wk,xkx,qk,qkx, w+K, gk,dk );

               gas.DARCS( q0,w0[3], kd,kf, f1,l1 );

               fj[0]+= gj[0];
               fj[1]+= gj[1];
               fj[2]+= gj[2];
               fj[3]+= gj[3];
               fj[4]+= gj[4];

               fk[0]+= gk[0];
               fk[1]+= gk[1];
               fk[2]+= gk[2];
               fk[3]+= gk[3];
               fk[4]+= gk[4];

               lj+= dj;
               lk+= dk;

               f0[0]= fj[0]+fk[0]+f1[0];
               f0[1]= fj[1]+fk[1]+f1[1];
               f0[2]= fj[2]+fk[2]+f1[2];
               f0[3]= fj[3]+fk[3]+f1[3];
               f0[4]= fj[4]+fk[4]+f1[4];

               so[0].fmav(i,  j,  k,  t,  r, -phi,f0 );
               so[0].fmav(i,  j+1,k,  t,  r,  phi,fj );
               so[0].fmav(i,  j,  k+1,t,  r,  phi,fk );

               l0= lj+ lk+ l1;
                  ld.addv(i,  j,  k,  t,lhd,&l0 );
                  ld.addv(i,  j+1,k,  t,lhd,&lj );
                  ld.addv(i,  j,  k+1,t,lhd,&lk );

           }
        }

         j=bsize-1;
         for( k=0;k<bsize-1;k++ )
        {
            for( i=0;i<bsize-1;i++ )
           {
               mw[0].loadv(i,  j,  k,  t,wm,w0  );
               mw[0].loadv(i+1,j,  k,  t,wm,wi  );
               mw[0].loadv(i,  j,  k+1,t,wm,wk  );

               so[0].loadv(i,  j,  k,  t,v, q0 );
               so[0].loadv(i+1,j,  k,  t,v, qi );
               so[0].loadv(i,  j,  k+1,t,v, qk );

               xd[0].loadv(i,  j,  k,  t,dxdx, x0x );
               xd[0].loadv(i+1,j,  k,  t,dxdx, xix );
               xd[0].loadv(i,  j,  k+1,t,dxdx, xkx );

               sd[0].loadv(i,  j,  k,  t,dvdx, q0x );
               sd[0].loadv(i+1,j,  k,  t,dvdx, qix );
               sd[0].loadv(i,  j,  k+1,t,dvdx, qkx );

//                fw.loadv(i,  j,  k,  t,wf,w );

//             gas.IFLUX( q0,qi, w+I+3,0., fi,li );
//             gas.IFLUX( q0,qk, w+K+3,0., fk,lk );
//
//             gas.VFLUX( w0,x0x,q0,q0x, wi,xix,qi,qix, w+I, gi,di );
//             gas.VFLUX( w0,x0x,q0,q0x, wk,xkx,qk,qkx, w+K, gk,dk );

               gas.DARCS( q0,w0[3], kd,kf, f1,l1 );

               fi[0]+= gi[0];
               fi[1]+= gi[1];
               fi[2]+= gi[2];
               fi[3]+= gi[3];
               fi[4]+= gi[4];

               fk[0]+= gk[0];
               fk[1]+= gk[1];
               fk[2]+= gk[2];
               fk[3]+= gk[3];
               fk[4]+= gk[4];

               li+= di;
               lk+= dk;

               f0[0]= fi[0]+fk[0]+f1[0];
               f0[1]= fi[1]+fk[1]+f1[1];
               f0[2]= fi[2]+fk[2]+f1[2];
               f0[3]= fi[3]+fk[3]+f1[3];
               f0[4]= fi[4]+fk[4]+f1[4];

               so[0].fmav(i,  j,  k,  t,  r, -phi,f0 );
               so[0].fmav(i+1,j,  k,  t,  r,  phi,fi );
               so[0].fmav(i,  j,  k+1,t,  r,  phi,fk );

               l0= li+ lk+ l1;
                  ld.addv(i,  j,  k,  t,lhd,&l0 );
                  ld.addv(i+1,j,  k,  t,lhd,&li );
                  ld.addv(i,  j,  k+1,t,lhd,&lk );

           }
        }

         k=bsize-1;
         for( j=0;j<bsize-1;j++ )
        {
            for( i=0;i<bsize-1;i++ )
           {

               mw[0].loadv(i,  j,  k,  t,wm,w0  );
               mw[0].loadv(i+1,j,  k,  t,wm,wi  );
               mw[0].loadv(i,  j+1,k,  t,wm,wj  );

               so[0].loadv(i,  j,  k,  t,v, q0 );
               so[0].loadv(i+1,j,  k,  t,v, qi );
               so[0].loadv(i,  j+1,k,  t,v, qj );

               xd[0].loadv(i,  j,  k,  t,dxdx, x0x );
               xd[0].loadv(i+1,j,  k,  t,dxdx, xix );
               xd[0].loadv(i,  j+1,k,  t,dxdx, xjx );

               sd[0].loadv(i,  j,  k,  t,dvdx, q0x );
               sd[0].loadv(i+1,j,  k,  t,dvdx, qix );
               sd[0].loadv(i,  j+1,k,  t,dvdx, qjx );

//                fw.loadv(i,  j,  k,  t,wf,w );
               mw[0].loadv(i,  j,  k,  t,wm,vol);


//             gas.IFLUX( q0,qi, w+I+3,0., fi,li );
//             gas.IFLUX( q0,qj, w+J+3,0., fj,lj );

//             gas.VFLUX( w0,x0x,q0,q0x, wi,xix,qi,qix, w+I, gi,di );
//             gas.VFLUX( w0,x0x,q0,q0x, wj,xjx,qj,qjx, w+J, gj,dj );

               gas.DARCS( q0,vol[3], kd,kf, f1,l1 );

               fi[0]+= gi[0];
               fi[1]+= gi[1];
               fi[2]+= gi[2];
               fi[3]+= gi[3];
               fi[4]+= gi[4];

               fj[0]+= gj[0];
               fj[1]+= gj[1];
               fj[2]+= gj[2];
               fj[3]+= gj[3];
               fj[4]+= gj[4];

               li+= di;
               lj+= dj;

               f0[0]= fi[0]+fj[0]+f1[0];
               f0[1]= fi[1]+fj[1]+f1[1];
               f0[2]= fi[2]+fj[2]+f1[2];
               f0[3]= fi[3]+fj[3]+f1[3];
               f0[4]= fi[4]+fj[4]+f1[4];

               so[0].fmav(i,  j,  k,  t,  r, -phi,f0 );
               so[0].fmav(i+1,j,  k,  t,  r,  phi,fi );
               so[0].fmav(i,  j+1,k,  t,  r,  phi,fj );

               l0= li+ lj+ l1;
                  ld.addv(i,  j,  k,  t,lhd,&l0 );
                  ld.addv(i+1,j,  k,  t,lhd,&li );
                  ld.addv(i,  j+1,k,  t,lhd,&lj );

           }
        }

         k=bsize-1;
         j=bsize-1;
         for( i=0;i<bsize-1;i++ )
        {
            mw[0].loadv(i,  j,  k,  t,wm,w0  );
            mw[0].loadv(i+1,j,  k,  t,wm,wi  );

            so[0].loadv(i,  j,  k,  t,v, q0 );
            so[0].loadv(i+1,j,  k,  t,v, qi );

            xd[0].loadv(i,  j,  k,  t,dxdx, x0x );
            xd[0].loadv(i+1,j,  k,  t,dxdx, xix );

            sd[0].loadv(i,  j,  k,  t,dvdx, q0x );
            sd[0].loadv(i+1,j,  k,  t,dvdx, qix );

//             fw.loadv(i,  j,  k,  t,wf,w );

//          gas.IFLUX( q0,qi, w+I+3,0., fi,li );
//          gas.VFLUX( w0,x0x,q0,q0x, wi,xix,qi,qix, w+I, gi,di );
            gas.DARCS( q0,w0[3], kd,kf, f1,l1 );

            fi[0]+= gi[0];
            fi[1]+= gi[1];
            fi[2]+= gi[2];
            fi[3]+= gi[3];
            fi[4]+= gi[4];

            li+= di;

            f0[0]= fi[0]+f1[0];
            f0[1]= fi[1]+f1[1];
            f0[2]= fi[2]+f1[2];
            f0[3]= fi[3]+f1[3];
            f0[4]= fi[4]+f1[4];

            so[0].fmav(i,  j,  k,  t,  r,-phi,f0 );
            so[0].fmav(i+1,j,  k,  t,  r, phi,fi );

            l0=    li+ l1;
               ld.addv(i,  j,  k,  t,lhd,&l0 );
               ld.addv(i+1,j,  k,  t,lhd,&li );

        }

         k=bsize-1;
         i=bsize-1;
         for( j=0;j<bsize-1;j++ )
        {
            mw[0].loadv(i,  j,  k,  t,wm,w0  );
            mw[0].loadv(i,  j+1,k,  t,wm,wj  );

            so[0].loadv(i,  j,  k,  t,v, q0 );
            so[0].loadv(i,  j+1,k,  t,v, qj );

            xd[0].loadv(i,  j,  k,  t,dxdx, x0x );
            xd[0].loadv(i,  j+1,k,  t,dxdx, xjx );

            sd[0].loadv(i,  j,  k,  t,dvdx, q0x );
            sd[0].loadv(i,  j+1,k,  t,dvdx, qjx );

//             fw.loadv(i,  j,  k,  t,wf,w );

//          gas.IFLUX( q0,qj, w+J+3,0., fj,lj );
//          gas.VFLUX( w0,x0x,q0,q0x, wj,xjx,qj,qjx, w+J, gj,dj );
            gas.DARCS( q0,w0[3], kd,kf, f1,l1 );

            fj[0]+= gj[0];
            fj[1]+= gj[1];
            fj[2]+= gj[2];
            fj[3]+= gj[3];
            fj[4]+= gj[4];
 
            lj+= dj;

            f0[0]= fj[0]+f1[0];
            f0[1]= fj[1]+f1[1];
            f0[2]= fj[2]+f1[2];
            f0[3]= fj[3]+f1[3];
            f0[4]= fj[4]+f1[4];

            so[0].fmav(i,  j,  k,  t,  r,-phi,f0 );
            so[0].fmav(i,  j+1,k,  t,  r, phi,fj );

            l0=    lj+ l1;
               ld.addv(i,  j,  k,  t,lhd,&l0 );
               ld.addv(i,  j+1,k,  t,lhd,&lj );

        }

         j=bsize-1;
         i=bsize-1;
         for( k=0;k<bsize-1;k++ )
        {
            mw[0].loadv(i,  j,  k,  t,wm,w0  );
            mw[0].loadv(i,  j,  k+1,t,wm,wk  );

            so[0].loadv(i,  j,  k,  t,v, q0 );
            so[0].loadv(i,  j,  k+1,t,v, qk );

            xd[0].loadv(i,  j,  k,  t,dxdx, x0x );
            xd[0].loadv(i,  j,  k+1,t,dxdx, xkx );

            sd[0].loadv(i,  j,  k,  t,dvdx, q0x );
            sd[0].loadv(i,  j,  k+1,t,dvdx, qkx );

//             fw.loadv(i,  j,  k,  t,wf,w );

//          gas.IFLUX( q0,qk, w+K+3,0., fk,lk );
//          gas.VFLUX( w0,x0x,q0,q0x, wk,xkx,qk,qkx, w+K, gk,dk );
            gas.DARCS( q0,w0[3], kd,kf, f1,l1 );

            fk[0]+= gk[0];
            fk[1]+= gk[1];
            fk[2]+= gk[2];
            fk[3]+= gk[3];
            fk[4]+= gk[4];

            lk+= dk;

            f0[0]= fk[0]+f1[0];
            f0[1]= fk[1]+f1[1];
            f0[2]= fk[2]+f1[2];
            f0[3]= fk[3]+f1[3];
            f0[4]= fk[4]+f1[4];

            so[0].fmav(i,  j,  k,  t,  r,-phi,f0 );
            so[0].fmav(i,  j,  k+1,t,  r, phi,fk );

            l0=    lk+ l1;

               ld.addv(i,  j,  k,  t,lhd,&l0 );
               ld.addv(i,  j,  k+1,t,lhd,&lk );

        }

         k=bsize-1;
         so[0].loadv(i,  j,  k,  t,v, q0 );
         mw[0].loadv(i,  j,  k,  t,wm,w0  );
           gas.DARCS( q0,w0[3], kd,kf, f1,l1 );

         so[0].fmav(i,  j,  k,  t,  r,-phi,f1 );
            ld.addv(i,  j,  k,  t,lhd,&l1 );
     }

// handle interfaces with only local data dependencies
       
      while( locals( com[0],msg[0], ntsk,tasks ) )
     {

#pragma omp parallel for private(t,c,d,e,f,g,i,j,l,m,n,y,z,t0,t1,w0,w1,x0x,x1x,q0x,q1x,q0,q1,w,g0,d0,f0,l0,phi,q2,q3,g2,g3,aa) schedule(dynamic)
         for( INT_ t=0;t<ntsk;t++ )
        {
            INT_    num,loc;
            INT_   *idx;
    
            com[0].task( tasks[t], num,loc, &idx );
            com[0].taskinfo( so[0], idx[0], t0,c );
            com[0].taskinfo( so[0], idx[1], t1,d );

            y= hx[c].iz[0];
            z= hx[d].iz[0];

            if( fabs( da[y].phi- da[z].phi ) < 1.e-6 )
           {

               phi=0.5*( da[y].phi+ da[z].phi );
    
               for( j=0;j<bsize;j++ )
              {
                  for( i=0;i<bsize;i++ )
                 {
                     t0.transform( i,j,0, l,m,n );
                     t1.transform( i,j,0, e,f,g );

                     mw[0].loadv(l,m,n,  c,wm,w0  );
                     mw[0].loadv(e,f,g,  d,wm,w1  );
   
                     xd[0].loadv(l,m,n,  c,dxdx, x0x );
                     xd[0].loadv(e,f,g,  d,dxdx, x1x );
   
                     sd[0].loadv(l,m,n,  c,dvdx, q0x );
                     sd[0].loadv(e,f,g,  d,dvdx, q1x );
             
                     so[0].loadv(l,m,n,  c,v,q0 );
                     so[0].loadv(e,f,g,  d,v,q1 );
                        cw.loadv(i,j,  idx[0], wc,w );
             
//                   gas.IFLUX( q0,q1, w+3,0., f0,l0 );
//                   gas.VFLUX( w0,x0x,q0,q0x, w1,x1x,q1,q1x, w, g0,d0 );

                     f0[0]+= g0[0];
                     f0[1]+= g0[1];
                     f0[2]+= g0[2];
                     f0[3]+= g0[3];
                     f0[4]+= g0[4];

                     l0+= d0;
             
                     so[0].fmav(l,m,n,c,  r,-phi,f0 );
                     so[0].fmav(e,f,g,d,  r, phi,f0 );
             
                        ld.addv(l,m,n,c,lhd,&l0 );
                        ld.addv(e,f,g,d,lhd,&l0 );
             
                 }
              }
           }
            else
           {

               for( j=0;j<bsize;j++ )
              {
                  for( i=0;i<bsize;i++ )
                 {
                     t0.transform( i,j,0, l,m,n );
                     t1.transform( i,j,0, e,f,g );

                     mw[0].loadv(l,m,n,  c,wm,w0  );
                     mw[0].loadv(e,f,g,  d,wm,w1  );
   
                     xd[0].loadv(l,m,n,  c,dxdx, x0x );
                     xd[0].loadv(e,f,g,  d,dxdx, x1x );
   
                     sd[0].loadv(l,m,n,  c,dvdx, q0x );
                     sd[0].loadv(e,f,g,  d,dvdx, q1x );
             
                     so[0].loadv(l,m,n,  c,v,q0 );
                     so[0].loadv(e,f,g,  d,v,q1 );

                        cw.loadv(i,j,  idx[0], wc,w );

                     gas.DARCF( w+3, q0,q1, da[y].phi,da[z].phi, q2,q3, aa,g2,g3 );
//                   gas.IFLUX( q2,q3, w+3,0., f0,l0 );
             
                     so[0].fmav(l,m,n,c,  r,-aa,f0 );
                     so[0].subv(l,m,n,c,  r,    g2 );

                     so[0].fmav(e,f,g,d,  r, aa,f0 );
                     so[0].addv(e,f,g,d,  r,    g3 );
             
                        ld.addv(l,m,n,c,lhd,&l0 );
                        ld.addv(e,f,g,d,lhd,&l0 );
             
                 }
              }
           }
        }
     }

// handle interfaces with data dependencies on remote ranks

      while( transit( com[0],msg[0], ntsk,tasks, 1 ) )
     {
#pragma omp parallel for private(t,c,d,i,j,l,m,n,t0,q0,q1,w,f0,l0) schedule(dynamic)
         for( INT_ t=0;t<ntsk;t++ )
        {
            assert( false );
            INT_    num,loc;
            INT_   *idx;
    
            com[0].task( tasks[t], num,loc, &idx );
            com[0].taskinfo( so[0], idx[0], t0,c );
            d= idx[1];
    
            for( j=0;j<bsize;j++ )
           {
               for( i=0;i<bsize;i++ )
              {
    
                  t0.transform( i,j,0, l,m,n );

                  so[0].loadv(l,m,n,     c,v,q0 );
                  so[1].loadv(i,j,0,     d,v,q1 );
                     cw.loadv(i,j,  idx[0], wc,w );
    
//                gas.IFLUX( q0,q1, w+3,0., f0,l0 );

                  so[0].subv(l,m,n,c,  r, f0 );
                     ld.addv(l,m,n,c,lhd,&l0 );

              }
           }
        }
     }

      return;
  }
