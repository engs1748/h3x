

#  include <q3x/q3x.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::rund()
  {

      REAL_          *v=NULL,*r=NULL;
      REAL_          *x=NULL,*d=NULL;
      REAL_          *y=NULL;
      REAL_          *yi=NULL;

      REAL_          *wb=NULL;
      REAL_          *vb=NULL;
      REAL_          *rb=NULL;
      REAL_         *vb0=NULL;
      REAL_          *wc=NULL;
      REAL_          *wm=NULL;
      REAL_          *wf=NULL;

      REAL_        *dxdx=NULL;
      REAL_        *dvdx=NULL;

      REAL_        sigma=1;
      REAL_          res[NVAR_+1];

      bool          flag;

      double         stime,etime;

      gas.init();

      start();

      co[3].malloc( &x  );
         cb.malloc( &y  );
         ci.malloc( &yi  );

      mw[1].malloc( &wm ); 
         bw.malloc( &wb ); 
         cw.malloc( &wc ); 

      so[1].malloc( &r  );

      so[1].malloc( &v  );

         ld.malloc( &d  );

         sb.malloc( &vb );
         sb.malloc( &rb );

         sb.malloc( &vb0);

      xd[1].malloc( &dxdx );
      sd[1].malloc( &dvdx );

      grid( x,y,yi );

      metrics( x, wm,wb,wc );
      grdx( wm,wb, dxdx );
      chk( wm,wb,wc, x,r );

      binit( wb, vb0 );
      restart( v, vb, NULL );
      stime=MPI_Wtime();

      for( INT_ it=0;it<ctr.nt;it++ )
     {
         flag= ( it%ctr.no==0 );
 
         grdv( wm,wb, vb,v, dxdx,dvdx );

         dbg = (it == 1 );

//            rhs( wm,wf,wb,wc, dxdx, vb0,vb, v,dvdx, rb,r,d );
//            rhs( wm,wf,wb,wc,       vb0,vb, v,      rb,r,d ); // first order

         darcyrhs( wm,wf,wb,wc, dxdx, vb0,vb, v,dvdx, rb,r,d ); // darcy

         upd( wm,          v,r,d, sigma, res, flag );

         if( flag )
        { 
            if( rank == 0 )
           {
               printf( "%9d %12.6e %12.6e %12.6e %12.6e %12.6e \n", it, res[0],res[1],res[2],res[3],res[4] );
           }
        } 
         if( it%ctr.ns == 0 )
        {
            save( v, vb, NULL, it );
        }
     }

      save( v, vb, NULL, -1 );

      etime=MPI_Wtime();

      if( rank==0 )
     {
         printf( "elapsed time %9.3e\n", etime-stime );
     }

      delete[] x;
      delete[] y;
      delete[] yi;
      delete[] v;
      delete[] vb;
      delete[] r;
      delete[] rb;
      delete[] d;
      delete[] wm;
      delete[] wb;
      delete[] wc;

      delete[] dxdx;
      delete[] dvdx;

      stop();

      return;
  }
