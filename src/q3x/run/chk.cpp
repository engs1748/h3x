#  include <q3x/q3x.h>
#  include <q3x/inline/fmetric0>


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::chk( REAL_ *wm, REAL_ *wb, REAL_ *wc, REAL_ *x, REAL_ *r )
  {

      INT_    ntsk;
      INT_  tasks[NTASK];
      INT_    jdx[32],kdx[32];
      REAL_   w[MSIZE_];

      REAL_   r0[NVAR_];
      REAL_   ri[NVAR_];
      REAL_   rj[NVAR_];
      REAL_   rk[NVAR_];
      REAL_  res[NVAR_];
      REAL_ lres[NVAR_];

      INT_      c,d, e,f,g, i,j,k, l,m,n, t;
      INT_      ist,ien;
      frule_t   t0,t1;
      INT_      num,loc;
      INT_     *idx;

      REAL_3    x1,x2,x3,x4,x5,x6,x7;

      memset( r,0,so[1].size()*sizeof(REAL_) );

      exchange(  x, co[1], com[0], msg[1] );

// boundary loop -find way to do with openmp
 
      ien= 0;
      for( k=0;k<BTYPES;k++ )
     {
         ist= ien;
         ien= bn.l[k][0];
         for( INT_ t=ist;t<ien;t++ )
        {
            t0= frule_t( bn.f[t], bsize,bsize,bsize );
            c= bn.h[t];

            for( j=0;j<bsize;j++ )
           {
               for( i=0;i<bsize;i++ )
              {
                  t0.transform( i,j,0, l,m,n );
  
                  bw.loadv(i,j,t,  wb,w );
  
                  r0[0]= w[3]*w[6];
                  r0[1]= w[4]*w[6];
                  r0[2]= w[5]*w[6];
                  r0[3]= 0;
                  r0[4]= 0;

                  so[0].subv(l,m,n,c, r,r0 );
    
              }
           }
        }
     }

#pragma omp parallel for private(t,i,j,k,w,ri,rj,rk,r0,x1,x2,x3,x4,x5,x6,x7)

      for( t=0;t<hx.n;t++ )
     {

         for( k=0;k<bsize-1;k++ )
        {
            for( j=0;j<bsize-1;j++ )
           {
               for( i=0;i<bsize-1;i++ )
              {
                  co[0].loadv(i+1,j,  k,  t, x,x1 );
                  co[0].loadv(i,  j+1,k,  t, x,x2 );
                  co[0].loadv(i+1,j+1,k,  t, x,x3 );

                  co[0].loadv(i,  j,  k+1,t, x,x4 );
                  co[0].loadv(i+1,j,  k+1,t, x,x5 );
                  co[0].loadv(i,  j+1,k+1,t, x,x6 );
                  co[0].loadv(i+1,j+1,k+1,t, x,x7 );

                  fmetric0( 0.5,0.5, x1,x3,x5,x7, w+0, w+3, w[6], w+7, w+10 );
                  

                  ri[0]= w[3]*w[6];
                  ri[1]= w[4]*w[6];
                  ri[2]= w[5]*w[6];
                  ri[3]= 0;      
                  ri[4]= 0;      
                                   
                  fmetric0( 0.5,0.5, x3,x2,x7,x6, w+0, w+3, w[6], w+7, w+10 );

                                   
                  rj[0]= w[3]*w[6];
                  rj[1]= w[4]*w[6];
                  rj[2]= w[5]*w[6];
                  rj[3]= 0;      
                  rj[4]= 0;      
                                   
                                   
                  fmetric0( 0.5,0.5, x4,x5,x6,x7, w+0, w+3, w[6], w+7, w+10 );

                  rk[0]= w[3]*w[6];
                  rk[1]= w[4]*w[6];
                  rk[2]= w[5]*w[6];
                  rk[3]= 0;
                  rk[4]= 0;

                  r0[0]= ri[0]+rj[0]+rk[0];
                  r0[1]= ri[1]+rj[1]+rk[1];
                  r0[2]= ri[2]+rj[2]+rk[2];
                  r0[3]= 0;
                  r0[4]= 0;

                  so[0].subv(i,  j,  k,  t,r,r0 );

                  so[0].addv(i+1,j,  k,  t,r,ri );
                  so[0].addv(i,  j+1,k,  t,r,rj );
                  so[0].addv(i,  j,  k+1,t,r,rk );

              }
           }
        }

         i= bsize-1;
         for( k=0;k<bsize-1;k++ )
        {
            for( j=0;j<bsize-1;j++ )
           {
 
                  co[0].loadv(i,  j+1,k,  t, x,x2 );
                  co[0].loadv(i+1,j+1,k,  t, x,x3 );

                  co[0].loadv(i,  j,  k+1,t, x,x4 );
                  co[0].loadv(i+1,j,  k+1,t, x,x5 );
                  co[0].loadv(i,  j+1,k+1,t, x,x6 );
                  co[0].loadv(i+1,j+1,k+1,t, x,x7 );

                  fmetric0( 0.5,0.5, x3,x2,x7,x6, w+0, w+3, w[6], w+7, w+10 );

                                   
                  rj[0]= w[3]*w[6];
                  rj[1]= w[4]*w[6];
                  rj[2]= w[5]*w[6];
                  rj[3]= 0;      
                  rj[4]= 0;      
                                   
                                   
                  fmetric0( 0.5,0.5, x4,x5,x6,x7, w+0, w+3, w[6], w+7, w+10 );

                  rk[0]= w[3]*w[6];
                  rk[1]= w[4]*w[6];
                  rk[2]= w[5]*w[6];
                  rk[3]= 0;
                  rk[4]= 0;

                  r0[0]= rj[0]+rk[0];
                  r0[1]= rj[1]+rk[1];
                  r0[2]= rj[2]+rk[2];
                  r0[3]= 0;
                  r0[4]= 0;

                  so[0].subv(i,  j,  k,  t,r,r0 );

                  so[0].addv(i,  j+1,k,  t,r,rj );
                  so[0].addv(i,  j,  k+1,t,r,rk );

           }
        }

         j= bsize-1;
         for( k=0;k<bsize-1;k++ )
        {
            for( i=0;i<bsize-1;i++ )
           {
               co[0].loadv(i+1,j,  k,  t, x,x1 );
               co[0].loadv(i+1,j+1,k,  t, x,x3 );

               co[0].loadv(i,  j,  k+1,t, x,x4 );
               co[0].loadv(i+1,j,  k+1,t, x,x5 );
               co[0].loadv(i,  j+1,k+1,t, x,x6 );
               co[0].loadv(i+1,j+1,k+1,t, x,x7 );

               fmetric0( 0.5,0.5, x1,x3,x5,x7, w+0, w+3, w[6], w+7, w+10 );
               

               ri[0]= w[3]*w[6];
               ri[1]= w[4]*w[6];
               ri[2]= w[5]*w[6];
               ri[3]= 0;      
               ri[4]= 0;      
                                
               fmetric0( 0.5,0.5, x4,x5,x6,x7, w+0, w+3, w[6], w+7, w+10 );

               rk[0]= w[3]*w[6];
               rk[1]= w[4]*w[6];
               rk[2]= w[5]*w[6];
               rk[3]= 0;
               rk[4]= 0;

               r0[0]= ri[0]+rk[0];
               r0[1]= ri[1]+rk[1];
               r0[2]= ri[2]+rk[2];
               r0[3]= 0;
               r0[4]= 0;

               so[0].subv(i,  j,  k,  t,r,r0 );

               so[0].addv(i+1,j,  k,  t,r,ri );
               so[0].addv(i,  j,  k+1,t,r,rk );

           }
        }

         k=bsize-1;
         for( j=0;j<bsize-1;j++ )
        {
            for( i=0;i<bsize-1;i++ )
           {

               co[0].loadv(i+1,j,  k,  t, x,x1 );
               co[0].loadv(i,  j+1,k,  t, x,x2 );
               co[0].loadv(i+1,j+1,k,  t, x,x3 );

               co[0].loadv(i+1,j,  k+1,t, x,x5 );
               co[0].loadv(i,  j+1,k+1,t, x,x6 );
               co[0].loadv(i+1,j+1,k+1,t, x,x7 );

               fmetric0( 0.5,0.5, x1,x3,x5,x7, w+0, w+3, w[6], w+7, w+10 );
               

               ri[0]= w[3]*w[6];
               ri[1]= w[4]*w[6];
               ri[2]= w[5]*w[6];
               ri[3]= 0;      
               ri[4]= 0;      
                                
               fmetric0( 0.5,0.5, x3,x2,x7,x6, w+0, w+3, w[6], w+7, w+10 );

                                
               rj[0]= w[3]*w[6];
               rj[1]= w[4]*w[6];
               rj[2]= w[5]*w[6];
               rj[3]= 0;      
               rj[4]= 0;      
                                
                                
               r0[0]= ri[0]+rj[0];
               r0[1]= ri[1]+rj[1];
               r0[2]= ri[2]+rj[2];
               r0[3]= 0;
               r0[4]= 0;

               so[0].subv(i,  j,  k,  t,r,r0 );

               so[0].addv(i+1,j,  k,  t,r,ri );
               so[0].addv(i,  j+1,k,  t,r,rj );

           }
        }

         k=bsize-1;
         j=bsize-1;
         for( i=0;i<bsize-1;i++ )
        {
            co[0].loadv(i+1,j,  k,  t, x,x1 );
            co[0].loadv(i+1,j+1,k,  t, x,x3 );

            co[0].loadv(i+1,j,  k+1,t, x,x5 );
            co[0].loadv(i+1,j+1,k+1,t, x,x7 );

            fmetric0( 0.5,0.5, x1,x3,x5,x7, w+0, w+3, w[6], w+7, w+10 );
            
            r0[0]= w[3]*w[6];
            r0[1]= w[4]*w[6];
            r0[2]= w[5]*w[6];
            r0[3]= 0;      
            r0[4]= 0;      
                             
            so[0].subv(i,  j,  k,  t,r,r0 );
            so[0].addv(i+1,j,  k,  t,r,r0 );

        }

         i= bsize-1;
         k= bsize-1;
         for( j=0;j<bsize-1;j++ )
        {
            co[0].loadv(i,  j+1,k,  t, x,x2 );
            co[0].loadv(i+1,j+1,k,  t, x,x3 );

            co[0].loadv(i,  j+1,k+1,t, x,x6 );
            co[0].loadv(i+1,j+1,k+1,t, x,x7 );

            fmetric0( 0.5,0.5, x3,x2,x7,x6, w+0, w+3, w[6], w+7, w+10 );

                             
            r0[0]= w[3]*w[6];
            r0[1]= w[4]*w[6];
            r0[2]= w[5]*w[6];
            r0[3]= 0;      
            r0[4]= 0;      
                             
                             
            so[0].subv(i,  j,  k,  t,r,r0 );
            so[0].addv(i,  j+1,k,  t,r,r0 );

        }

         j= bsize-1;
         i= bsize-1;
         for( k=0;k<bsize-1;k++ )
        {

            co[0].loadv(i,  j,  k+1,t, x,x4 );
            co[0].loadv(i+1,j,  k+1,t, x,x5 );
            co[0].loadv(i,  j+1,k+1,t, x,x6 );
            co[0].loadv(i+1,j+1,k+1,t, x,x7 );

            fmetric0( 0.5,0.5, x4,x5,x6,x7, w+0, w+3, w[6], w+7, w+10 );

            r0[0]= w[3]*w[6];
            r0[1]= w[4]*w[6];
            r0[2]= w[5]*w[6];
            r0[3]= 0;
            r0[4]= 0;

            so[0].subv(i,  j,  k,  t,r,r0 );
            so[0].addv(i,  j,  k+1,t,r,r0 );

        }

// here last cell for volume terms if needed

         i= bsize-1;
         j= bsize-1;
         k= bsize-1;
     }

// interfaces with local data dependencies
       
      while( locals( com[0],msg[1], ntsk,tasks ) )
     {
  
#pragma omp parallel for private(t,num,loc,idx,jdx,kdx,t0,t1,c,d,e,f,g,i,j,l,m,n,w,r0)
         for( INT_ t=0;t<ntsk;t++ )
        {
    
            com[0].task( tasks[t], num,loc, &idx,jdx,kdx );
            com[0].taskinfo( so[0], idx[0], t0,c );
            com[0].taskinfo( so[0], idx[1], t1,d );

/*          fprintf( file, "#----\n" );
            fprintf( file, "#(l) kdx=[%2d %2d] idx=[%2d %2d] list=[%2d %2d %2d %2d] \n", 
                     kdx[0],kdx[1],  idx[0],idx[1],  
                     com[0].list[idx[0]][0], com[0].list[idx[0]][1], 
                     com[0].list[idx[0]][2], com[0].list[idx[0]][3] );
            fprintf( file, "#(l) kdx=[%2d %2d] idx=[%2d %2d] list=[%2d %2d %2d %2d] \n", 
                     kdx[0],kdx[1],  idx[0],idx[1],  
                     com[0].list[idx[1]][0], com[0].list[idx[1]][1], 
                     com[0].list[idx[1]][2], com[0].list[idx[1]][3] );*/
    
            for( j=0;j<bsize;j++ )
           {
               for( i=0;i<bsize;i++ )
              {
                  cw.loadv( i,j,idx[0], wc,w );
                  r0[0]= w[3]*w[6];
                  r0[1]= w[4]*w[6];
                  r0[2]= w[5]*w[6];
                  r0[3]= 0;
                  r0[4]= 0;
  
                  t0.transform( i,j,0, l,m,n );
                  t1.transform( i,j,0, e,f,g );
    
                  so[0].subv(l,m,n,c,r,r0 );
                  so[0].addv(e,f,g,d,r,r0 );
              }
           }
        }
     }

// interfaces with data dependencies on remote ranks

      while( transit( com[0],msg[1], ntsk,tasks, 1 ) )
     {
#pragma omp parallel for private(t,num,loc,idx,jdx,kdx,t0,t1,c,d,e,f,g,i,j,l,m,n,w,r0)
         for( INT_ t=0;t<ntsk;t++ )
        {
    
            com[0].task( tasks[t], num,loc, &idx,jdx,kdx );
            com[0].taskinfo( so[0], idx[0], t0,c );

/*          fprintf( file, "#----\n" );
            fprintf( file, "#(r) kdx=[%2d %2d] idx=[%2d %2d] list=[%2d %2d %2d %2d] \n", 
                     kdx[0],-1    ,  idx[0],-1    ,  
                     com[0].list[idx[0]][0], com[0].list[idx[0]][1], 
                     com[0].list[idx[0]][2], com[0].list[idx[0]][3] );*/
    
            for( j=0;j<bsize;j++ )
           {
               for( i=0;i<bsize;i++ )
              {
    
                  cw.loadv( i,j,idx[0], wc,w );
                  r0[0]= w[3]*w[6];
                  r0[1]= w[4]*w[6];
                  r0[2]= w[5]*w[6];
                  r0[3]= 0;
                  r0[4]= 0;

                  t0.transform( i,j,0, l,m,n );
    
                  so[0].subv(l,m,n,c,r,r0 );

              }
           }
        }
     }


      memset( lres,0,NVAR_*sizeof(REAL_) );

#pragma omp parallel for private( t,i,j,k,r0,res )
      for( t=0;t<hx.n;t++ )
     {
         memset(  res,0,NVAR_*sizeof(REAL_) );
         for( k=0;k<bsize;k++ )
        {
            for( j=0;j<bsize;j++ )
           {
               for( i=0;i<bsize;i++ )
              {
                  so[0].loadv( i,j,k,t, r,r0 ); 
                  res[0]+= fabs(r0[0]);
                  res[1]+= fabs(r0[1]);
                  res[2]+= fabs(r0[2]);
              }
           }
        }
#pragma omp critical
        {
            lres[0]+= res[0];
            lres[1]+= res[1];
            lres[2]+= res[2];
            lres[3]+= (bsize*bsize*bsize);
        }
     }

      memset( res,0,NVAR_*sizeof(REAL_) );
      MPI_Reduce( lres,res,4,REAL_MPI,MPI_SUM,0,MPI_COMM_WORLD);
         fprintf( file,"Metrics residuals: %9.3e %9.3e %9.3e %9.3e\n", lres[0],lres[1],lres[2] , lres[3]);

      if( rank == 0 )
     {
         res[3]= 1./res[3];
         res[0]*= res[3];
         res[1]*= res[3];
         res[2]*= res[3];

         printf( "Metrics residuals: %9.3e %9.3e %9.3e\n", res[0],res[1],res[2] );
     }

/*    for( t=0;t<hx.n;t++ )
     {
         fprintf( file," \n" );
         fprintf( file,"#after remote face loop\n" );
         fprintf( file,"#%2d %2d \n", t,hx[t].id );
         fprintf( file," \n" );
         FILE *f;
         char name[32];
         sprintf( name,"metrics.%06d", hx[t].id );
         f= fopen( name,"w" );
         for( k=0;k<bsize;k++ )
        {
            for( j=0;j<bsize;j++ )
           {
               for( i=0;i<bsize;i++ )
              {
                  so[0].loadv(i,j,k,t,r, r0 );
                  fprintf( f," % 9.3e % 9.3e % 9.3e \n", r0[0],r0[1],r0[2] );

              }
           }
        }
         fclose(f );
     }*/


      return;
  }
