
#  include <q3x/q3x.h>
#  include <m33.h>

#  include <q3x/inline/vmetric>
#  include <q3x/inline/fmetric>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::metrics( REAL_ *x, REAL_ *wm, REAL_ *wb, REAL_ *wc )
  {
      REAL_3   x0,x1,x2,x3,x4,x5,x6,x7;
      REAL_2   yg,wg;

      REAL_    v[MSIZE_];

//    REAL_    u[3*16];

      INT_     c,d,i,j,k,l,m,n,o,s,t; 
      INT_     ist,ien;
      frule_t  r;

// compute metrics

      yg[1]=  sqrt(3.)/3.;
      yg[0]= -yg[1];

      yg[0]+= 1;
      yg[1]+= 1;

      yg[0]*= 0.5;
      yg[1]*= 0.5;

      wg[0]= 0.5;
      wg[1]= 0.5;

// boundary metrics

      ien= 0;
      for( s=0;s<BTYPES;s++ )
     {
         ist= ien;
         ien= bn.l[s][0];

         for( t=ist;t<ien;t++ )
        {

            r= frule_t( bn.f[t], bsize+1,bsize+1,bsize+1 );
            c= bn.h[t];
            o= bn.q[t];
            o= hx[c].q[o].o;

            d=-1;
            if( o ){ d=-d; };

            for( j=0;j<bsize;j++ )
           {
               for( i=0;i<bsize;i++ )
              {
                  r.transform(   i,  j,0, l,m,n ); co[0].loadv( l,m,n,c,x,x0 );
                  r.transform( 1+i,  j,0, l,m,n ); co[0].loadv( l,m,n,c,x,x1 );
                  r.transform(   i,1+j,0, l,m,n ); co[0].loadv( l,m,n,c,x,x2 );
                  r.transform( 1+i,1+j,0, l,m,n ); co[0].loadv( l,m,n,c,x,x3 );

                  fmetric( 2, yg,wg, x0,x1,x2,x3, v+0 ,v+ 3,v[6],v+ 7,v+10 );
                  v[3]*= d;
                  v[4]*= d;
                  v[5]*= d;

                  bw.storev( i,j,t,wb,v );

              }
           }
/*          bw.loadv( 0,0,t,(*wb),v );
            fprintf( file,"#(b) %2d %2d %2d %2d %1d % 9.3e % 9.3e % 9.3e\n", t,c, bn.q[t],d,o, v[3],v[4],v[5] );*/
        }
     }

// internal metrics
      for( c=0;c<hx.n;c++ )
     {
         for( k=0;k<bsize;k++ )
        {
            for( j=0;j<bsize;j++ )
           {
               for( i=0;i<bsize;i++ )
              {
                  co[0].loadv(   i,  j,  k,c,x,x0 );
                  co[0].loadv( 1+i,  j,  k,c,x,x1 );

                  co[0].loadv(   i,1+j,  k,c,x,x2 );
                  co[0].loadv( 1+i,1+j,  k,c,x,x3 );

                  co[0].loadv(   i,  j,1+k,c,x,x4 );
                  co[0].loadv( 1+i,  j,1+k,c,x,x5 );

                  co[0].loadv(   i,1+j,1+k,c,x,x6 );
                  co[0].loadv( 1+i,1+j,1+k,c,x,x7 );

                  vmetric( 2, yg,wg, x0,x1,x2,x3,x4,x5,x6,x7, v+0,v[3] );

                  mw[0].storev( i,j,k,c,wm,v );

              }
           }
        }
/*       mw[0].loadv(   0,0,0,c,(*wm),v );
         fprintf(file,"#(i) %2d %2d         % 9.3e % 9.3e % 9.3e\n",c,h[c].id,u[I+3],u[I+3],u[I+3]);
         fprintf(file,"#(j) %2d %2d         % 9.3e % 9.3e % 9.3e\n",c,h[c].id,u[J+4],u[J+4],u[J+4]);
         fprintf(file,"#(k) %2d %2d         % 9.3e % 9.3e % 9.3e\n",c,h[c].id,u[K+5],u[K+5],u[K+5]);*/

     }


// connections

      ien= 0;
      for( s=0;s<com[0].ldx[0];s++ )
     {
         ist= ien;

         ien= com[0].mdx[s][1];
//       fprintf( file,"#----------------------------------------\n" );
         

         for( t=ist;t<ien;t++ )
        {
    
            r= frule_t( com[0].hfx[t], bsize+1,bsize+1,bsize+1 );
            c= com[0].list[t][2];
            d=-1;
            o= com[0].list[t][1];
            o= hx[c].q[o].o;
            if( o ){ d=-d; };

            for( j=0;j<bsize;j++ )
           {
               for( i=0;i<bsize;i++ )
              {
                  r.transform(   i,  j,0, l,m,n ); co[0].loadv( l,m,n,c,x,x0 );
                  r.transform( 1+i,  j,0, l,m,n ); co[0].loadv( l,m,n,c,x,x1 );
                  r.transform(   i,1+j,0, l,m,n ); co[0].loadv( l,m,n,c,x,x2 );
                  r.transform( 1+i,1+j,0, l,m,n ); co[0].loadv( l,m,n,c,x,x3 );
         
                  fmetric( 2, yg,wg, x0,x1,x2,x3, v+0 ,v+ 3,v[6],v+ 7,v+10 );
                  v[3]*= d;
                  v[4]*= d;
                  v[5]*= d;
         
                  
                  cw.storev( i,j,t,wc,v );
              }
           }
/*          cw.loadv(   0,0,t,(*wc),v );
            fprintf( file,"#(c) %2d %2d %2d %2d %1d % 9.3e % 9.3e % 9.3e\n", t,c, com[0].list[t][1], d,o, v[3],v[4],v[5] );*/
        }
     }

      return;
  }
