
#  include <q3x/q3x.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 19:46:09 BST 2018
// Changes History
// Next Change(s)  -

   void q3x_t::stop()
  {


      so[1].cleanup( *this );

      mw[1].cleanup( *this );

      xd[1].cleanup( *this );

      sd[1].cleanup( *this );

      co[1].cleanup( *this );
      co[2].cleanup( *this );
      co[3].cleanup( *this );

      free( msg[0] );
      free( msg[1] );
      free( msg[2] );
      free( msg[3] );

      return;
  }
