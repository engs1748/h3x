#  include <q3x/q3x.h>
#  include <q2.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::rht( REAL_ *x, REAL_ *v, REAL_ *r, REAL_ *g )
  {

      INT_       ntsk;
      INT_       tasks[NTASK];

      INT_       b,c,d;
      INT_       i,j,k, l,m,n, s,t;
      INT_       ist,ien;
      frule_t    r0,r1,r2,r3,r4,r5;
      frule_t    rr;
      INT_       num,loc;
      INT_      *idx;

      REAL_      v0,v1, v2,v3, v4,v5, v6,v7;

      REAL_      f000;
      REAL_      a000;

      REAL_      a;

      REAL_      f;
      REAL_8     w0,w1;

// MER added
      INT_       ic, jc, kc;
      REAL_2     wg, yg;
      REAL_3     x0,x1,x2,x3,x4,x5,x6,x7;
      REAL_9     dzdx,dxdz;
      REAL_3     dvdx;

      REAL_8     p;
      REAL_8     dpdz[3];
      REAL_8     dpdx[3];
      REAL_      dc;

      REAL_8     w;

////


// compute metrics

      yg[1]=  sqrt(3.)/3.;
      yg[0]= -yg[1];

      yg[0]+= 1;
      yg[1]+= 1;

      yg[0]*= 0.5;
      yg[1]*= 0.5;

      wg[0]= 0.5;
      wg[1]= 0.5;

// update loop

      REAL_      wrk0[1024];
      REAL_      wrk1[1024];

      assert( bsize < 1024 );

      te[3].memset(   r,0 );
      td[3].memset(   g,0 );

      exchange(  v, te[1], com[0], msg[1] );
      exchange(  v, te[2], com[1], msg[2] );
      exchange(  v, te[3], com[2], msg[3] );

// Boundary loop

      ien= 0;
      for( b=0;b<1;b++ )
     {
         ist= ien;
         ien= bn.l[b][0];

         for( t=ist;t<ien;t++ )
        {


            r0= frule_t( bn.f[t], bsize+1,bsize+1,bsize+1 );
            c= bn.h[t];
            s= bn.s[t];

//          printf( "\n" );
//          printf( "%2d %2d\n", hx[c].id,s );
            if( c > -1 )
           {

               k= 0;
               for( j=1;j<bsize;j++ )
              {
                  for( i=1;i<bsize;i++ )
                 {
                     r0.transform( i  ,j  ,0, l,m,n  ); 

                     te[0].loadv( l,m,n,c, v,&v0 );
                     te[0].loadv( l,m,n,c, r,&f000 );

// Here only homogeneous Dirichlet boundary conditions for now.
// Add other boundaries later
                     f= v0-0;
                     a= 1;

                     te[0].addv( l,m,n,c, r,&f );
                     td[0].addv( l,m,n,c, g,&a );
             
                 }
              }
           }
        }
     }

      for( t=0;t<hx.n;t++ )
     {
         for( k=0;k<bsize;k++ )
        {
            for( j=0;j<bsize;j++ )
           {
               for( i=0;i<bsize;i++ )
              {
                  te[0].loadv( i  ,j  ,k  ,t, v,&v0 );
                  te[0].loadv( i+1,j  ,k  ,t, v,&v1 );

                  te[0].loadv( i  ,j+1,k  ,t, v,&v2 );
                  te[0].loadv( i+1,j+1,k  ,t, v,&v3 );

                  te[0].loadv( i  ,j  ,k+1,t, v,&v4 );
                  te[0].loadv( i+1,j  ,k+1,t, v,&v5 );

                  te[0].loadv( i  ,j+1,k+1,t, v,&v6 );
                  te[0].loadv( i+1,j+1,k+1,t, v,&v7 );

                  co[0].loadv(   i,  j,  k,t,x,x0 );
                  co[0].loadv( 1+i,  j,  k,t,x,x1 );

                  co[0].loadv(   i,1+j,  k,t,x,x2 );
                  co[0].loadv( 1+i,1+j,  k,t,x,x3 );

                  co[0].loadv(   i,  j,1+k,t,x,x4 );
                  co[0].loadv( 1+i,  j,1+k,t,x,x5 );

                  co[0].loadv(   i,1+j,1+k,t,x,x6 );
                  co[0].loadv( 1+i,1+j,1+k,t,x,x7 );

// HERE MER TO WRITE THE LOOP WITH GAUSS LEGENDRE INTEGRATION FOR FINITE ELEMENT RESIDUAL
// USE AS EXAMPLE THE LOOP USED TO EVALUATE METRIC COEFFICIENTS
                  memset( w,0,sizeof(REAL_8));

                  for( kc=0;kc<=1;kc++)
                 { 
                     for( jc=0;jc<=1;jc++)
                    {
                        for( ic=0;ic<=1;ic++)
                       {

                           q111(yg[ic],yg[jc],yg[kc], p,dpdz);
        
                           x1[0+0]= p[0]*x0[0]+ p[1]*x1[0]+ p[2]*x2[0]+ p[3]*x3[0]+ p[4]*x4[0]+ p[5]*x5[0]+ p[6]*x6[0]+ p[7]*x7[0];
                           x1[0+1]= p[0]*x0[1]+ p[1]*x1[1]+ p[2]*x2[1]+ p[3]*x3[1]+ p[4]*x4[1]+ p[5]*x5[1]+ p[6]*x6[1]+ p[7]*x7[1];
                           x1[0+2]= p[0]*x0[2]+ p[1]*x1[2]+ p[2]*x2[2]+ p[3]*x3[2]+ p[4]*x4[2]+ p[5]*x5[2]+ p[6]*x6[2]+ p[7]*x7[2];

                           dxdz[0+0]= dpdz[0][0]*x0[0]+ dpdz[0][1]*x1[0]+ dpdz[0][2]*x2[0]+ dpdz[0][3]*x3[0]+ dpdz[0][4]*x4[0]+ dpdz[0][5]*x5[0]+ dpdz[0][6]*x6[0]+ dpdz[0][7]*x7[0];
                           dxdz[0+1]= dpdz[0][0]*x0[1]+ dpdz[0][1]*x1[1]+ dpdz[0][2]*x2[1]+ dpdz[0][3]*x3[1]+ dpdz[0][4]*x4[1]+ dpdz[0][5]*x5[1]+ dpdz[0][6]*x6[1]+ dpdz[0][7]*x7[1];
                           dxdz[0+2]= dpdz[0][0]*x0[2]+ dpdz[0][1]*x1[2]+ dpdz[0][2]*x2[2]+ dpdz[0][3]*x3[2]+ dpdz[0][4]*x4[2]+ dpdz[0][5]*x5[2]+ dpdz[0][6]*x6[2]+ dpdz[0][7]*x7[2];
   
                           dxdz[3+0]= dpdz[1][0]*x0[0]+ dpdz[1][1]*x1[0]+ dpdz[1][2]*x2[0]+ dpdz[1][3]*x3[0]+ dpdz[1][4]*x4[0]+ dpdz[1][5]*x5[0]+ dpdz[1][6]*x6[0]+ dpdz[1][7]*x7[0];
                           dxdz[3+1]= dpdz[1][0]*x0[1]+ dpdz[1][1]*x1[1]+ dpdz[1][2]*x2[1]+ dpdz[1][3]*x3[1]+ dpdz[1][4]*x4[1]+ dpdz[1][5]*x5[1]+ dpdz[1][6]*x6[1]+ dpdz[1][7]*x7[1];
                           dxdz[3+2]= dpdz[1][0]*x0[2]+ dpdz[1][1]*x1[2]+ dpdz[1][2]*x2[2]+ dpdz[1][3]*x3[2]+ dpdz[1][4]*x4[2]+ dpdz[1][5]*x5[2]+ dpdz[1][6]*x6[2]+ dpdz[1][7]*x7[2];
   
                           dxdz[6+0]= dpdz[2][0]*x0[0]+ dpdz[2][1]*x1[0]+ dpdz[2][2]*x2[0]+ dpdz[2][3]*x3[0]+ dpdz[2][4]*x4[0]+ dpdz[2][5]*x5[0]+ dpdz[2][6]*x6[0]+ dpdz[2][7]*x7[0];
                           dxdz[6+1]= dpdz[2][0]*x0[1]+ dpdz[2][1]*x1[1]+ dpdz[2][2]*x2[1]+ dpdz[2][3]*x3[1]+ dpdz[2][4]*x4[1]+ dpdz[2][5]*x5[1]+ dpdz[2][6]*x6[1]+ dpdz[2][7]*x7[1];
                           dxdz[6+2]= dpdz[2][0]*x0[2]+ dpdz[2][1]*x1[2]+ dpdz[2][2]*x2[2]+ dpdz[2][3]*x3[2]+ dpdz[2][4]*x4[2]+ dpdz[2][5]*x5[2]+ dpdz[2][6]*x6[2]+ dpdz[2][7]*x7[2];

                           dc= wg[ic];
                           dc*=wg[jc];
                           dc*=wg[kc];

                           dc*= det( dxdz );

                           inv33( dxdz, dzdx );

                           dot33( dzdx, dpdz[0], dpdx[0] );
                           dot33( dzdx, dpdz[1], dpdx[1] );
                           dot33( dzdx, dpdz[2], dpdx[2] );
                           dot33( dzdx, dpdz[3], dpdx[3] );
                           dot33( dzdx, dpdz[4], dpdx[4] );
                           dot33( dzdx, dpdz[5], dpdx[5] );
                           dot33( dzdx, dpdz[6], dpdx[6] );
                           dot33( dzdx, dpdz[7], dpdx[7] );

                           dvdx[0]= dpdx[0][0]*v0+ dpdx[0][1]*v1+ dpdx[0][2]*v2+ dpdx[0][3]*v3+ dpdx[0][4]*v4+ dpdx[0][5]*v5+ dpdx[0][6]*v6+ dpdx[0][7]*v7;
                           dvdx[1]= dpdx[1][0]*v0+ dpdx[1][1]*v1+ dpdx[1][2]*v2+ dpdx[1][3]*v3+ dpdx[1][4]*v4+ dpdx[1][5]*v5+ dpdx[1][6]*v6+ dpdx[1][7]*v7;
                           dvdx[2]= dpdx[2][0]*v0+ dpdx[2][1]*v1+ dpdx[2][2]*v2+ dpdx[2][3]*v3+ dpdx[2][4]*v4+ dpdx[2][5]*v5+ dpdx[2][6]*v6+ dpdx[2][7]*v7;

                           w0[0]+= dc*dot3( dvdx,dpdx[0] );
                           w0[1]+= dc*dot3( dvdx,dpdx[1] );
                           w0[2]+= dc*dot3( dvdx,dpdx[2] );
                           w0[3]+= dc*dot3( dvdx,dpdx[3] );
                           w0[4]+= dc*dot3( dvdx,dpdx[4] );
                           w0[5]+= dc*dot3( dvdx,dpdx[5] );
                           w0[6]+= dc*dot3( dvdx,dpdx[6] );
                           w0[7]+= dc*dot3( dvdx,dpdx[7] );

                           w0[0]-= dc*dot3( x1,x1 );
                           w0[1]-= dc*dot3( x1,x1 );
                           w0[2]-= dc*dot3( x1,x1 );
                           w0[3]-= dc*dot3( x1,x1 );
                           w0[4]-= dc*dot3( x1,x1 );
                           w0[5]-= dc*dot3( x1,x1 );
                           w0[6]-= dc*dot3( x1,x1 );
                           w0[7]-= dc*dot3( x1,x1 );

                           w1[0]+= dc*dot3( dpdx[0],dpdx[0] );
                           w1[1]+= dc*dot3( dpdx[1],dpdx[1] );
                           w1[2]+= dc*dot3( dpdx[2],dpdx[2] );
                           w1[3]+= dc*dot3( dpdx[3],dpdx[3] );
                           w1[4]+= dc*dot3( dpdx[4],dpdx[4] );
                           w1[5]+= dc*dot3( dpdx[5],dpdx[5] );
                           w1[6]+= dc*dot3( dpdx[6],dpdx[6] );
                           w1[7]+= dc*dot3( dpdx[7],dpdx[7] );
 

                       }
                    }
                 }
                  te[0].addv( i  ,j  ,k  ,t, r,w0+0 );
                  te[0].addv( i+1,j  ,k  ,t, r,w0+1 );

                  te[0].addv( i  ,j+1,k  ,t, r,w0+2 );
                  te[0].addv( i+1,j+1,k  ,t, r,w0+3 );

                  te[0].addv( i  ,j  ,k+1,t, r,w0+4 );
                  te[0].addv( i+1,j  ,k+1,t, r,w0+5 );

                  te[0].addv( i  ,j+1,k+1,t, r,w0+6 );
                  te[0].addv( i+1,j+1,k+1,t, r,w0+7 );

                  td[0].addv( i  ,j  ,k  ,t, g,w1+0 );
                  td[0].addv( i+1,j  ,k  ,t, g,w1+1 );

                  td[0].addv( i  ,j+1,k  ,t, g,w1+2 );
                  td[0].addv( i+1,j+1,k  ,t, g,w1+3 );

                  td[0].addv( i  ,j  ,k+1,t, g,w1+4 );
                  td[0].addv( i+1,j  ,k+1,t, g,w1+5 );

                  td[0].addv( i  ,j+1,k+1,t, g,w1+6 );
                  td[0].addv( i+1,j+1,k+1,t, g,w1+7 );

              }
           }
        }
     }

// handle interfaces with only local data dependencies

      while( transit( com[2],msg[3], ntsk,tasks, 0 ) )
     {
         for( t=0;t<ntsk;t++ )
        {
            com[2].task( tasks[t], num,loc, &idx );

            assert( num >= loc );

            f=0;
            a=0;

            for( s=0;s<loc;s++ )
           { 
               com[2].taskinfo( te[0], idx[s], r0,c );
             
               r0.transform( 0,0,0, l,m,n ); te[0].loadv( l,m,n, c, r,&f000 );
               r0.transform( 0,0,0, l,m,n ); td[0].loadv( l,m,n, c, g,&a000 );

               f+= f000;
               a+= a000;
         
           }

            for( s=loc;s<num;s++ )
           { 
               c= idx[s];

               te[3].loadv( 0,0,0, c, r,&f000 );
               td[3].loadv( 0,0,0, c, g,&a000 );

               f+= f000;
               a+= a000;

           }

            for( s=0;s<loc;s++ )
           { 
               com[2].taskinfo( te[0], idx[s], r0,c );
             
               r0.transform( 0,0,0,   l,m,n ); 

               co[0].storev( l,m,n,c, r,&f ); 
               cd[0].storev( l,m,n,c, g,&a );

           }
        }
     }

// handle interfaces with only local data dependencies

      while( transit( com[1],msg[2], ntsk,tasks, 0 ) )
     {
         for( t=0;t<ntsk;t++ )
        {
            memset( wrk0,0,1024*sizeof(REAL_) );
            memset( wrk1,0,1024*sizeof(REAL_) ); 

            com[1].task( tasks[t], num,loc, &idx );
            assert( num >= loc );

            for( s=0;s<loc;s++ )
           { 
               com[1].taskinfo( te[0], idx[s], r0,c );
          
               d= idx[s];
             
               for( i=1;i<bsize;i++ )
              {
          
                  r0.transform( i,  0,0, l,m,n ); 

                  te[0].loadv( l,m,n, c, r,&f000 );
                  td[0].loadv( l,m,n, c, g,&a000 );


                  wrk0[i]+= f000;
                  wrk1[i]+= a000;
          
              }
           }
          
            for( s=loc;s<num;s++ )
           { 
               c= idx[s];
             
               com[1].taskinfo( te[0], idx[s], r0,b );
          
               for( i=1;i<bsize;i++ )
              {

                  te[2].loadv(   i,0,0, c, r,&f000 );
                  td[2].loadv(   i,0,0, c, g,&a000 );

                  wrk0[i]+= f000;
                  wrk1[i]+= a000;

              }
           }
          
            for( s=0;s<loc;s++ )
           { 
               com[1].taskinfo( te[0], idx[s], r0,c );
          
               d= idx[s];
             
               for( i=1;i<bsize;i++ )
              {
          
                  r0.transform( i,0,0, l,m,n ); 

                  te[0].storev( l,m,n,c, r, wrk0+i ); 
                  td[0].storev( l,m,n,c, g, wrk1+i  );
          
              }
           }
        }
     }

      while( locals( com[0],msg[1], ntsk,tasks ) )
     {
         for( t=0;t<ntsk;t++ )
        {
    
            com[0].task( tasks[t], num,loc, &idx );
            com[0].taskinfo( te[0], idx[0], r0,c );
            com[0].taskinfo( te[0], idx[1], r1,d );

            assert( num == 2 );
            assert( loc == 2 );

            for( j=1;j<bsize;j++ )
           {
               for( i=1;i<bsize;i++ )
              {


                  r1.transform( i  ,j  ,0, l,m,n );  

                  te[0].loadv( l,m,n,d, r,&f   );
                  td[0].loadv( l,m,n,d, g,&a   );

                  r0.transform( i  ,j  ,0, l,m,n  ); 

                  te[0].loadv( l,m,n,c, r,&f000  );
                  td[0].loadv( l,m,n,c, g,&a000  );

                  f+= f000;
                  a+= a000;

                  r0.transform( i,j,0, l,m,n  ); 

                  te[0].storev( l,m,n,c, r,&f    );
                  td[0].storev( l,m,n,c, g,&a    );


                  r1.transform( i,j,0, l,m,n  ); 

                  te[0].storev( l,m,n,d, r,&f );
                  td[0].storev( l,m,n,d, g,&a );

              }
           }
        }
     }

// handle interfaces with data dependencies on remote ranks

      while( transit( com[0],msg[1], ntsk,tasks, 1 ) )
     {
         for( t=0;t<ntsk;t++ )
        {
    
            com[0].task( tasks[t], num,loc, &idx );
            com[0].taskinfo( te[0], idx[0], r0,c );
            b= idx[1];
    
            assert( num == 2 );
            assert( loc == 1 );
    
            for( j=1;j<bsize;j++ )
           {
               for( i=1;i<bsize;i++ )
              {
                  te[1].loadv( i  ,j  ,0,b, r,&f );
                  td[1].loadv( i  ,j  ,0,b, g,&a );

                  r0.transform( i  ,j  ,0, l,m,n  ); 

                  te[0].loadv( l,m,n,c, r,&f000 );
                  td[0].loadv( l,m,n,c, g,&a000 );

                  f+= f000;
                  a+= a000;

                  te[0].storev( l,m,n,c, r,&f );
                  td[0].storev( l,m,n,c, g,&a );

              }
           }
        }
     }

      return;
  }

