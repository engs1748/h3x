
#  include <q3x/q3x.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -


   void q3x_t::upd( REAL_ *wm, REAL_ *wb, 
                    REAL_ *bv0, REAL_ *v0, REAL_ *bv, REAL_ *v, REAL_ *br, REAL_ *r, REAL_ dt, REAL_ sigma, REAL_ *res, bool flag )
  {

      REAL_    q[NVAR_];
      REAL_   q0[NVAR_];
      REAL_   q1[NVAR_];
      REAL_   f0[NVAR_];
      REAL_   f1[NVAR_];

      REAL_   w0[WSIZE_];
      REAL_   w1[MSIZE_];

      REAL_  lres[NVAR_+1];
      REAL_  vres[NVAR_+1];
      REAL_   l0;
      INT_    i,j,k, c,t;
      frule_t t0;
      INT_    ist,ien;

      memset( lres,0,(NVAR_+1)*sizeof(REAL_));


#pragma omp parallel for private(t,i,j,k,q0,q1,f0,w0,l0,vres) schedule(dynamic)

      for( t=0;t<hx.n;t++ )
     {

//       if( hx[t].id == 37 ){ printf( "\n" ); }

         memset(  vres,0,(NVAR_+1)*sizeof(REAL_));

         for( k=0;k<bsize;k++ )
        {
            for( j=0;j<bsize;j++ )
           {
               for( i=0;i<bsize;i++ )
              {
                  so[0].loadv(i,j,k,t, v0, q0 );
                  so[0].loadv(i,j,k,t,  r, f0 );
                  mw[0].loadv(i,j,k,t, wm, w0 );

                  l0= dt/w0[3];

                  vres[0]+= fabs(f0[0]*l0);
                  vres[1]+= fabs(f0[1]*l0);
                  vres[2]+= fabs(f0[2]*l0);
                  vres[3]+= fabs(f0[3]*l0);
                  vres[4]+= fabs(f0[4]*l0);

                  gas.updte1( q0,l0,f0,q1 );
                  so[0].storev(i,j,k,t,  v,q1 );

/*                if( hx[t].id == 37 && i == 0 )
                 {
                     printf( "%2d %2d %2d % 9.3e % 9.3e % 9.3e % 9.3e % 9.3e % 9.3e % 9.3e % 9.3e % 9.3e % 9.3e % 9.3e % 9.3e % 9.3e\n", 
                              i,j,k, w0[0],w0[1],w0[2], q0[0], q0[1], q0[2], q0[3], q0[4], q1[0], q1[1], q1[2], q1[3], q1[4] );
                 }*/
              }
           }
        }

         if( flag )
        {
#pragma omp critical
           {
               lres[0]+= vres[0];
               lres[1]+= vres[1];
               lres[2]+= vres[2];
               lres[3]+= vres[3];
               lres[4]+= vres[4];
               lres[5]+= (bsize*bsize*bsize);
           }
        }

     }
      if( flag )
     {
         memset(  res,0,(NVAR_+1)*sizeof(REAL_));
         MPI_Reduce( lres,res, gas.nvar()+1,REAL_MPI,MPI_SUM,0,
                     MPI_COMM_WORLD);
         if( rank == 0 ) 
        {
            res[5]=  1./res[5];
            res[0]*= res[5];
            res[1]*= res[5];
            res[2]*= res[5];
            res[3]*= res[5];
            res[4]*= res[5];
        }
     }

      ien= 0;
      for( k=0;k<FREES;k++ )
     {
         ist= ien;
         ien= bn.l[k][0];
         for( INT_ t=ist;t<ien;t++ )
        {
    
            t0= frule_t( bn.f[t], bsize,bsize,bsize );
            c= bn.h[t];
            if( c > -1 )
           {  
               for( j=0;j<bsize;j++ )
              {
                  for( i=0;i<bsize;i++ )
                 {

                        bw.loadv(i,j,    t, wb, w1 );
                        sb.loadv(i,j,    t,bv0, q1 );
                        sb.loadv(i,j,    t, br, f1 );

                        q[0]= q1[0] +f1[0]*dt;
                        q[1]= q1[1] +f1[1]*dt;
                        q[2]= q1[2] +f1[2]*dt;
                        q[3]= q1[3] +f1[3]*dt;
                        q[4]= q1[4] +f1[4]*dt;
                        sb.storev( i,j,    t, bv, q );

                 }
              }
           }
        }
     }

      return;
  }
   void q3x_t::upd( REAL_ *wm, 
                    REAL_ *v, REAL_ *r, REAL_ *lhd, REAL_ sigma,
                    REAL_ *res, bool flag )
  {

      REAL_   q0[NVAR_];
      REAL_   f0[NVAR_];
      REAL_   g0[NVAR_];
      REAL_  lres[NVAR_+1];
      REAL_  vres[NVAR_+1];
      REAL_   l0;
      INT_    i,j,k, t;

      REAL_   s;

      s= 1./sigma;
      s++;

      memset( lres,0,(NVAR_+1)*sizeof(REAL_));

//pragma omp parallel for private(t,i,j,k,q0,f0,g0,l0,vres) schedule(dynamic)

      for( t=0;t<hx.n;t++ )
     {
         memset(  vres,0,(NVAR_+1)*sizeof(REAL_));

         for( k=0;k<bsize;k++ )
        {
            for( j=0;j<bsize;j++ )
           {
               for( i=0;i<bsize;i++ )
              {
                  so[0].loadv(i,j,k,t,  v, q0 );
                  so[0].loadv(i,j,k,t,  r, f0 );
                     ld.loadv(i,j,k,t,lhd,&l0  );

                  l0*= s;

                  l0= 1./l0;

                  vres[0]+= fabs(f0[0]*l0);
                  vres[1]+= fabs(f0[1]*l0);
                  vres[2]+= fabs(f0[2]*l0);
                  vres[3]+= fabs(f0[3]*l0);
                  vres[4]+= fabs(f0[4]*l0);

                  gas.DELTA( q0,f0,g0 );
//                printf( "%4d %4d %4d %4d %12.5e %12.5e %12.5e %12.5e %12.5e %12.5e \n", t,i,j,k, l0, g0[0],g0[1],g0[2],g0[3],g0[4] );
                  so[0].fmav(i,j,k,t,  v,l0,g0 );

              }
           }
        }

         if( flag )
        {
//pragma omp critical
           {
               lres[0]+= vres[0];
               lres[1]+= vres[1];
               lres[2]+= vres[2];
               lres[3]+= vres[3];
               lres[4]+= vres[4];
               lres[5]+= (bsize*bsize*bsize);
           }
        }

     }
      if( flag )
     {
         memset(  res,0,(NVAR_+1)*sizeof(REAL_));
         MPI_Reduce( lres,res, gas.nvar()+1,REAL_MPI,MPI_SUM,0,
                     MPI_COMM_WORLD);
         if( rank == 0 ) 
        {
            res[5]=  1./res[5];
            res[0]*= res[5];
            res[1]*= res[5];
            res[2]*= res[5];
            res[3]*= res[5];
            res[4]*= res[5];
        }
     }

      return;
  }
