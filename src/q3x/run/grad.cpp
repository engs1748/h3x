
#  include <q3x/q3x.h>
#  include <q3x/grad.h>
#  include <m33.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::grdx( REAL_ *wm, REAL_ *wb, REAL_ *dxdx )
  {

      INT_    ntsk;
      INT_  tasks[NTASK];

      REAL_   w0[MSIZE_];
      REAL_   w1[MSIZE_];

      REAL_   wi[WSIZE_];
      REAL_   wj[WSIZE_];
      REAL_   wk[WSIZE_];

      REAL_   r0[6];
      REAL_   ri[6];
      REAL_   rj[6];
      REAL_   rk[6];
      REAL_   eps=0.;

      INT_      c,d, e,f,g, i,j,k, l,m,n, t;
      INT_      ist,ien;
      frule_t   t0,t1;


      xd[0].memset( dxdx, 0 );
      exchange( wm, mw[1], com[0], msg[0] );

// boundary loop -find way to do with openmp

      ien= 0;
      for( k=0;k<FREES;k++ )
     {
         ist= ien;
         ien= bn.l[k][0];
         for( INT_ t=ist;t<ien;t++ )
        {
    
            t0= frule_t( bn.f[t], bsize,bsize,bsize );
            c= bn.h[t];
            if( c > -1 )
           {  
               for( j=0;j<bsize;j++ )
              {
                  for( i=0;i<bsize;i++ )
                 {
                     t0.transform( i,j,0, l,m,n );
                     mw[0].loadv(l,  m,  n,  c,wm, w0 );
                        bw.loadv(i,j,        t,wb, w1 );

                     w1[0]= 2*w1[0]- w0[0];
                     w1[1]= 2*w1[1]- w0[1];
                     w1[2]= 2*w1[2]- w0[2];

                     wlsm( w0,w1, r0 );
                     xd[0].addv( l,  m,  n,  c,dxdx,r0 );
                 }
              }
           }
        }
     }
      for( k=FREES+1;k<BTYPES;k++ )
     {
         ist= ien;
         ien= bn.l[k][0];
         for( INT_ t=ist;t<ien;t++ )
        {
    
            t0= frule_t( bn.f[t], bsize,bsize,bsize );
            c= bn.h[t];
            if( c > -1 )
           {  
               for( j=0;j<bsize;j++ )
              {
                  for( i=0;i<bsize;i++ )
                 {
                     t0.transform( i,j,0, l,m,n );
                     mw[0].loadv(l,  m,  n,  c,wm, w0 );
                        bw.loadv(i,j,        t,wb, w1 );

                     wlsm( w0,w1, r0 );
                     xd[0].addv( l,  m,  n,  c,dxdx,r0 );
                 }
              }
           }
        }
     }

#pragma omp parallel for private(t,i,j,k,w0,wi,wj,wk,ri,rj,rk,r0)

      for( t=0;t<hx.n;t++ )
     {

         for( k=0;k<bsize-1;k++ )
        {
            for( j=0;j<bsize-1;j++ )
           {
               for( i=0;i<bsize-1;i++ )
              {
                  mw[0].loadv(i,  j,  k,  t,wm,  w0 );
                  mw[0].loadv(i+1,j,  k,  t,wm,  wi );
                  mw[0].loadv(i,  j+1,k,  t,wm,  wj );
                  mw[0].loadv(i,  j,  k+1,t,wm,  wk );

                  wlsm( w0,wi, ri );
                  wlsm( w0,wj, rj );
                  wlsm( w0,wk, rk );

                  r0[0]= ri[0]+ rj[0]+ rk[0]+ eps;
                  r0[1]= ri[1]+ rj[1]+ rk[1];
                  r0[2]= ri[2]+ rj[2]+ rk[2];
                  r0[3]= ri[3]+ rj[3]+ rk[3]+ eps;
                  r0[4]= ri[4]+ rj[4]+ rk[4];
                  r0[5]= ri[5]+ rj[5]+ rk[5]+ eps;

                  xd[0].addv( i,  j,  k,  t,dxdx,r0 );
                  xd[0].addv( i+1,j,  k,  t,dxdx,ri );
                  xd[0].addv( i,  j+1,k,  t,dxdx,rj );
                  xd[0].addv( i,  j,  k+1,t,dxdx,rk );

              }
           }
        }

         i= bsize-1;
         for( k=0;k<bsize-1;k++ )
        {
            for( j=0;j<bsize-1;j++ )
           {
               mw[0].loadv(i,  j,  k,  t,wm,  w0 );
               mw[0].loadv(i,  j+1,k,  t,wm,  wj );
               mw[0].loadv(i,  j,  k+1,t,wm,  wk );

               wlsm( w0,wj, rj );
               wlsm( w0,wk, rk );

               r0[0]= rj[0]+ rk[0]+ eps;
               r0[1]= rj[1]+ rk[1];
               r0[2]= rj[2]+ rk[2];
               r0[3]= rj[3]+ rk[3]+ eps;
               r0[4]= rj[4]+ rk[4];
               r0[5]= rj[5]+ rk[5]+ eps;

               xd[0].addv( i,  j,  k,  t,dxdx,r0 );
               xd[0].addv( i,  j+1,k,  t,dxdx,rj );
               xd[0].addv( i,  j,  k+1,t,dxdx,rk );

           }
        }

         j= bsize-1;
         for( k=0;k<bsize-1;k++ )
        {
            for( i=0;i<bsize-1;i++ )
           {
               mw[0].loadv(i,  j,  k,  t,wm,  w0 );
               mw[0].loadv(i+1,j,  k,  t,wm,  wi );
               mw[0].loadv(i,  j,  k+1,t,wm,  wk );

               wlsm( w0,wi, ri );
               wlsm( w0,wk, rk );

               r0[0]= ri[0]+ rk[0]+ eps;
               r0[1]= ri[1]+ rk[1];
               r0[2]= ri[2]+ rk[2];
               r0[3]= ri[3]+ rk[3]+ eps;
               r0[4]= ri[4]+ rk[4];
               r0[5]= ri[5]+ rk[5]+ eps;

               xd[0].addv( i,  j,  k,  t,dxdx,r0 );
               xd[0].addv( i+1,j,  k,  t,dxdx,ri );
               xd[0].addv( i,  j,  k+1,t,dxdx,rk );

           }
        }

         k=bsize-1;
         for( j=0;j<bsize-1;j++ )
        {
            for( i=0;i<bsize-1;i++ )
           {

               mw[0].loadv(i,  j,  k,  t,wm,  w0 );
               mw[0].loadv(i+1,j,  k,  t,wm,  wi );
               mw[0].loadv(i,  j+1,k,  t,wm,  wj );

               wlsm( w0,wi, ri );
               wlsm( w0,wj, rj );

               r0[0]= ri[0]+ rj[0]+ eps;
               r0[1]= ri[1]+ rj[1];
               r0[2]= ri[2]+ rj[2];
               r0[3]= ri[3]+ rj[3]+ eps;
               r0[4]= ri[4]+ rj[4];
               r0[5]= ri[5]+ rj[5]+ eps;

               xd[0].addv( i,  j,  k,  t,dxdx,r0 );
               xd[0].addv( i+1,j,  k,  t,dxdx,ri );
               xd[0].addv( i,  j+1,k,  t,dxdx,rj );
           }
        }

         k=bsize-1;
         j=bsize-1;
         for( i=0;i<bsize-1;i++ )
        {
            mw[0].loadv(i,  j,  k,  t,wm,  w0 );
            mw[0].loadv(i+1,j,  k,  t,wm,  wi );

            wlsm( w0,wi, ri );

            r0[0]= ri[0]+ eps;
            r0[1]= ri[1];
            r0[2]= ri[2];
            r0[3]= ri[3]+ eps;
            r0[4]= ri[4];
            r0[5]= ri[5]+ eps;

            xd[0].addv( i,  j,  k,  t,dxdx,r0 );
            xd[0].addv( i+1,j,  k,  t,dxdx,ri );

        }

         i= bsize-1;
         k= bsize-1;
         for( j=0;j<bsize-1;j++ )
        {
            mw[0].loadv(i,  j,  k,  t,wm,  w0 );
            mw[0].loadv(i,  j+1,k,  t,wm,  wj );

            wlsm( w0,wj, rj );

            r0[0]= rj[0]+ eps;
            r0[1]= rj[1];
            r0[2]= rj[2];
            r0[3]= rj[3]+ eps;
            r0[4]= rj[4];
            r0[5]= rj[5]+ eps;

            xd[0].addv( i,  j,  k,  t,dxdx,r0 );
            xd[0].addv( i,  j+1,k,  t,dxdx,rj );

        }

         j= bsize-1;
         i= bsize-1;
         for( k=0;k<bsize-1;k++ )
        {
            mw[0].loadv(i,  j,  k,  t,wm,  w0 );
            mw[0].loadv(i,  j,  k+1,t,wm,  wk );

            wlsm( w0,wk, rk );

            r0[0]= rk[0]+ eps;
            r0[1]= rk[1];
            r0[2]= rk[2];
            r0[3]= rk[3]+ eps;
            r0[4]= rk[4];
            r0[5]= rk[5]+ eps;

            xd[0].addv( i,  j,  k,  t,dxdx,r0 );
            xd[0].addv( i,  j,  k+1,t,dxdx,rk );

        }

// here last cell for volume terms if needed

         i= bsize-1;
         j= bsize-1;
         k= bsize-1;

         r0[0]= eps;
         r0[1]= 0;
         r0[2]= 0;
         r0[3]= eps;
         r0[4]= 0;
         r0[5]= eps;

         xd[0].addv( i,  j,  k,  t,dxdx,r0 );

     }

// interfaces with local data dependencies
       
      while( locals( com[0],msg[0], ntsk,tasks ) )
     {
  
#pragma omp parallel for private(t,t0,t1,c,d,e,f,g,i,j,l,m,n,w0,w1,r0)

         for( INT_ t=0;t<ntsk;t++ )
        {
            INT_      num,loc;
            INT_     *idx;
    
            com[0].task( tasks[t], num,loc, &idx );
            com[0].taskinfo( mw[0], idx[0], t0,c );
            com[0].taskinfo( mw[0], idx[1], t1,d );

            for( j=0;j<bsize;j++ )
           {
               for( i=0;i<bsize;i++ )
              {
                  t0.transform( i,j,0, l,m,n );
                  t1.transform( i,j,0, e,f,g );

                  mw[0].loadv(l,m,n,c, wm,  w0 );
                  mw[0].loadv(e,f,g,d, wm,  w1 );

                  wlsm( w0,w1, r0 );
    
                  xd[0].addv( l,m,n,c, dxdx,r0 );
                  xd[0].addv( e,f,g,d, dxdx,r0 );
              }
           }
        }
     }

// interfaces with data dependencies on remote ranks

      while( transit( com[0],msg[0], ntsk,tasks, 1 ) )
     {

#pragma omp parallel for private(t,t0,c,d,e,f,g,i,j,l,m,n,w0,w1,r0)

         for( INT_ t=0;t<ntsk;t++ )
        {
            INT_      num,loc;
            INT_     *idx;
    
            com[0].task( tasks[t], num,loc, &idx );
            com[0].taskinfo( mw[0], idx[0], t0,c );
            d= idx[1];

            for( j=0;j<bsize;j++ )
           {
               for( i=0;i<bsize;i++ )
              {

                  t0.transform( i,j,0, l,m,n );

                  mw[0].loadv(l,m,n,c, wm,  w0 );
                  mw[1].loadv(i,j,0,d, wm,  w1 );

                  wlsm( w0,w1, r0 );
    
                  xd[0].addv( l,m,n,c, dxdx,r0 );
    
              }
           }
        }
     }

/*    printf( "\n" );
      printf( "dxdx\n" );
      printf( "\n" );
      for( t=0;t<hx.n;t++ )
     {
         for( k=0;k<bsize;k++ )
        {
            for( j=0;j<bsize;j++ )
           {
               for( i=0;i<bsize;i++ )
              {
                  xd[0].loadv( i,j,k,t, dxdx, w0 );
                  printf( " % 9.3e % 9.3e % 9.3e % 9.3e % 9.3e % 9.3e \n", w0[ 0], w0[ 1], w0[ 2], w0[ 3], w0[ 4], w0[ 5] );
              }
               printf( "\n" );
           }
            printf( "\n" );
        }
     }*/

// now exchange the metric tensors 

      exchange( dxdx, xd[1], com[0], msg[0] );
      while( transit( com[0],msg[0], ntsk,tasks, 0 ) ){};

      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::grdv( REAL_ *wm, REAL_ *wb, REAL_ *vb, REAL_ *v, REAL_ *dxdx, REAL_ *dvdx )
  {

      INT_    ntsk;
      INT_  tasks[NTASK];
      REAL_   w0[MSIZE_];
      REAL_   w1[MSIZE_];
      REAL_   wi[WSIZE_];
      REAL_   wj[WSIZE_];
      REAL_   wk[WSIZE_];

      REAL_   r0[NVAR_*3];
      REAL_   ri[NVAR_*3];
      REAL_   rj[NVAR_*3];
      REAL_   rk[NVAR_*3];

      REAL_   v0[NVAR_];
      REAL_   v1[NVAR_];
      REAL_   vi[NVAR_];
      REAL_   vj[NVAR_];
      REAL_   vk[NVAR_];

      INT_      c,d, e,f,g, i,j,k, l,m,n, o,o3,t;
      INT_      ist,ien;
      frule_t   t0,t1;

      o= gas.nvar();
      o3=3*o;

      sd[0].memset( dvdx, 0 );
      exchange(  v, so[1], com[0], msg[0] );

// boundary loop -find way to do with openmp

      ien= 0;
      for( k=0;k<FREES;k++ )
     {
         ist= ien;
         ien= bn.l[k][0];
         for( INT_ t=ist;t<ien;t++ )
        {
    
            t0= frule_t( bn.f[t], bsize,bsize,bsize );
            c= bn.h[t];
            if( c > -1 )
           {  
               for( j=0;j<bsize;j++ )
              {
                  for( i=0;i<bsize;i++ )
                 {
                     t0.transform( i,j,0, l,m,n );
                     mw[0].loadv(l,  m,  n,  c,wm,  w0 );
                     so[0].loadv(l,  m,  n,  c, v,  v0 );
                        bw.loadv(i,j,    t,wb, w1 );
                        sb.loadv(i,j,    t,vb, v1 );

                        w1[0]= 2*w1[0]- w0[0];
                        w1[1]= 2*w1[1]- w0[1];
                        w1[2]= 2*w1[2]- w0[2];

                        wlsv( o, w0,w1, v0,v1, r0 );
                        sd[0].addv( l,  m,  n,  c,dvdx,r0 );

                 }
              }
           }
        }
     }


#pragma omp parallel for private(t,i,j,k,w0,wi,wj,wk,v0,vi,vj,vk,ri,rj,rk,r0)

      for( t=0;t<hx.n;t++ )
     {

         for( k=0;k<bsize-1;k++ )
        {
            for( j=0;j<bsize-1;j++ )
           {
               for( i=0;i<bsize-1;i++ )
              {
                  mw[0].loadv(i,  j,  k,  t,wm,  w0 );
                  mw[0].loadv(i+1,j,  k,  t,wm,  wi );
                  mw[0].loadv(i,  j+1,k,  t,wm,  wj );
                  mw[0].loadv(i,  j,  k+1,t,wm,  wk );


                  so[0].loadv(i,  j,  k,  t, v,  v0 );
                  so[0].loadv(i+1,j,  k,  t, v,  vi );
                  so[0].loadv(i,  j+1,k,  t, v,  vj );
                  so[0].loadv(i,  j,  k+1,t, v,  vk );

                  wlsv( o, w0,wi, v0,vi, ri );
                  wlsv( o, w0,wj, v0,vj, rj );
                  wlsv( o, w0,wk, v0,vk, rk );

                  for( INT_ r=0;r<o3;r++ ){ r0[   r]= ri[   r]+ rj[   r]+ rk[   r]; }

                  sd[0].addv( i,  j,  k,  t,dvdx,r0 );
                  sd[0].addv( i+1,j,  k,  t,dvdx,ri );
                  sd[0].addv( i,  j+1,k,  t,dvdx,rj );
                  sd[0].addv( i,  j,  k+1,t,dvdx,rk );

              }
           }
        }

         i= bsize-1;
         for( k=0;k<bsize-1;k++ )
        {
            for( j=0;j<bsize-1;j++ )
           {
               mw[0].loadv(i,  j,  k,  t,wm,  w0 );
               mw[0].loadv(i,  j+1,k,  t,wm,  wj );
               mw[0].loadv(i,  j,  k+1,t,wm,  wk );

               so[0].loadv(i,  j,  k,  t, v,  v0 );
               so[0].loadv(i,  j+1,k,  t, v,  vj );
               so[0].loadv(i,  j,  k+1,t, v,  vk );

               wlsv( o, w0,wj, v0,vj, rj );
               wlsv( o, w0,wk, v0,vk, rk );

               for( INT_ r=0;r<o3;r++ ){ r0[   r]= rj[   r]+ rk[   r]; } 

               sd[0].addv( i,  j,  k,  t,dvdx,r0 );
               sd[0].addv( i,  j+1,k,  t,dvdx,rj );
               sd[0].addv( i,  j,  k+1,t,dvdx,rk );

           }
        }

         j= bsize-1;
         for( k=0;k<bsize-1;k++ )
        {
            for( i=0;i<bsize-1;i++ )
           {
               mw[0].loadv(i,  j,  k,  t,wm,  w0 );
               mw[0].loadv(i+1,j,  k,  t,wm,  wi );
               mw[0].loadv(i,  j,  k+1,t,wm,  wk );

               so[0].loadv(i,  j,  k,  t, v,  v0 );
               so[0].loadv(i+1,j,  k,  t, v,  vi );
               so[0].loadv(i,  j,  k+1,t, v,  vk );

               wlsv( o, w0,wi, v0,vi, ri );
               wlsv( o, w0,wk, v0,vk, rk );

               for( INT_ r=0;r<o3;r++ ){ r0[   r]= ri[   r]+ rk[   r]; }

               sd[0].addv( i,  j,  k,  t,dvdx,r0 );
               sd[0].addv( i+1,j,  k,  t,dvdx,ri );
               sd[0].addv( i,  j,  k+1,t,dvdx,rk );

           }
        }

         k=bsize-1;
         for( j=0;j<bsize-1;j++ )
        {
            for( i=0;i<bsize-1;i++ )
           {

               mw[0].loadv(i,  j,  k,  t,wm,  w0 );
               mw[0].loadv(i+1,j,  k,  t,wm,  wi );
               mw[0].loadv(i,  j+1,k,  t,wm,  wj );

               so[0].loadv(i,  j,  k,  t, v,  v0 );
               so[0].loadv(i+1,j,  k,  t, v,  vi );
               so[0].loadv(i,  j+1,k,  t, v,  vj );

               wlsv( o, w0,wi, v0,vi, ri );
               wlsv( o, w0,wj, v0,vj, rj );

               for( INT_ r=0;r<o3;r++ ){ r0[r]= ri[   r]+ rj[   r]; }

               sd[0].addv( i,  j,  k,  t,dvdx,r0 );
               sd[0].addv( i+1,j,  k,  t,dvdx,ri );
               sd[0].addv( i,  j+1,k,  t,dvdx,rj );
           }
        }

         k=bsize-1;
         j=bsize-1;
         for( i=0;i<bsize-1;i++ )
        {
            mw[0].loadv(i,  j,  k,  t,wm,  w0 );
            mw[0].loadv(i+1,j,  k,  t,wm,  wi );

            so[0].loadv(i,  j,  k,  t, v,  v0 );
            so[0].loadv(i+1,j,  k,  t, v,  vi );

            wlsv( o, w0,wi, v0,vi, r0 );

            sd[0].addv( i,  j,  k,  t,dvdx,r0 );
            sd[0].addv( i+1,j,  k,  t,dvdx,r0 );

        }

         i= bsize-1;
         k= bsize-1;
         for( j=0;j<bsize-1;j++ )
        {
            mw[0].loadv(i,  j,  k,  t,wm,  w0 );
            mw[0].loadv(i,  j+1,k,  t,wm,  wj );

            so[0].loadv(i,  j,  k,  t, v,  v0 );
            so[0].loadv(i,  j+1,k,  t, v,  vj );

            wlsv( o, w0,wj, v0,vj, r0 );

            sd[0].addv( i,  j,  k,  t,dvdx,r0 );
            sd[0].addv( i,  j+1,k,  t,dvdx,r0 );

        }

         j= bsize-1;
         i= bsize-1;
         for( k=0;k<bsize-1;k++ )
        {
            mw[0].loadv(i,  j,  k,  t,wm,  w0 );
            mw[0].loadv(i,  j,  k+1,t,wm,  wk );

            so[0].loadv(i,  j,  k,  t, v,  v0 );
            so[0].loadv(i,  j,  k+1,t, v,  vk );

            wlsv( o, w0,wk, v0,vk, r0 );

            sd[0].addv( i,  j,  k,  t,dvdx,r0 );
            sd[0].addv( i,  j,  k+1,t,dvdx,r0 );

        }

// here last cell for volume terms if needed

         i= bsize-1;
         j= bsize-1;
         k= bsize-1;
     }

// interfaces with local data dependencies
       
      while( locals( com[0],msg[0], ntsk,tasks ) )
     {
  
#pragma omp parallel for private(t,t0,t1,c,d,e,f,g,i,j,l,m,n,w0,w1,v0,v1,r0)

         for( INT_ t=0;t<ntsk;t++ )
        {
            INT_      num,loc;
            INT_     *idx;
    
            com[0].task( tasks[t], num,loc, &idx );
            com[0].taskinfo( mw[0], idx[0], t0,c );
            com[0].taskinfo( mw[0], idx[1], t1,d );

            for( j=0;j<bsize;j++ )
           {
               for( i=0;i<bsize;i++ )
              {
                  t0.transform( i,j,0, l,m,n );
                  t1.transform( i,j,0, e,f,g );

                  mw[0].loadv(l,m,n,c, wm,  w0 );
                  mw[0].loadv(e,f,g,d, wm,  w1 );

                  so[0].loadv(l,m,n,c,  v,  v0 );
                  so[0].loadv(e,f,g,d,  v,  v1 );

                  wlsv( o, w0,w1, v0,v1, r0 );
    
                  sd[0].addv( l,m,n,c, dvdx,r0 );
                  sd[0].addv( e,f,g,d, dvdx,r0 );
              }
           }
        }
     }

// interfaces with data dependencies on remote ranks

      while( transit( com[0],msg[0], ntsk,tasks, 1 ) )
     {

#pragma omp parallel for private(t,t0,c,d,e,f,g,i,j,l,m,n,w0,w1,v0,v1,r0)

         for( INT_ t=0;t<ntsk;t++ )
        {
            INT_      num,loc;
            INT_     *idx;
    
            com[0].task( tasks[t], num,loc, &idx );
            com[0].taskinfo( mw[0], idx[0], t0,c );
            d= idx[1];

            for( j=0;j<bsize;j++ )
           {
               for( i=0;i<bsize;i++ )
              {

                  t0.transform( i,j,0, l,m,n );

                  mw[0].loadv(l,m,n,c, wm,  w0 );
                  mw[1].loadv(i,j,0,d, wm,  w1 );

                  so[0].loadv(l,m,n,c,  v,  v0 );
                  so[1].loadv(i,j,0,d,  v,  v1 );

                  wlsv( o, w0,w1, v0,v1, r0 );
    
                  sd[0].addv( l,m,n,c, dvdx,r0 );
    
              }
           }
        }
     }

      return;
  }
