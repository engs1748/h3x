#  include <q3x/q3x.h>

   
//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::binit( REAL_ *wb, REAL_ *vb )
  {
      REAL_    v[  16];

      wsize_t<REAL_,NVAR_+3,32> v0;
      VINT_    lb;

      INT_     c,b,d,i,j,k,n,o,s,t; 
      INT_     ist,ien, jst,jen, jmin;
      REAL_    dmin;
      frule_t  r;

// read boundary data

      FILE    *f=NULL;
      f= fopen( "bdata.dat","r" ); assert(f );
      fscanf( f,"%d", &n ); lb.n=n; lb.resize(-1);
      for( i=0;i<lb.n;i++ )
     {
         fscanf( f,"%d", &(lb[i])); 
     }
      n= lb.n;

      if( n > 0 )
     {
         v0.n= lb[n-1];
         v0.resize(0.);
         for( i=0;i<lb[n-1];i++ )
        {
            for( j=0;j<(3+NVAR_);j++ ){ fscanf( f, "%lf", v0[i]+j ); } //printf ("%e ",v0[i][j] ); }
        }
     }

// assign boundary values

      ien= 0;
      for( s=0;s<FREES;s++ )
     {
         ist= ien;
         ien= bn.l[s][0];


         for( t=ist;t<ien;t++ )
        {
    
            r= frule_t( bn.f[t], bsize+1,bsize+1,bsize+1 );
            c= bn.h[t];
            o= bn.q[t];
            o= hx[c].q[o].o;

            b= bn.b[t];
            jst= 0;
            jen= lb[b];
            if( b > 0 ){ jst= lb[b-1]; };

            d=-1;
            if( o ){ d=-d; };

            for( j=0;j<bsize;j++ )
           {
               for( i=0;i<bsize;i++ )
              {

                  bw.loadv( i,j,t,wb,v );

                  jmin=-1;
                  dmin= 999;

                  for( k=jst;k<jen;k++ )
                 {
                     REAL_ d= abs(v[0]- v0[k][0])+   
                              abs(v[1]- v0[k][1])+   
                              abs(v[2]- v0[k][2]);

                     if( d < dmin )
                    {
                        dmin= d;
                        jmin= k;
                    }                     
                 }
                  sb.storev( i,j,t,vb,v0[jmin]+3 );

              }
           }
        }
     }
      for( s=AWALLS;s<TWALLS;s++ )
     {
         ist= ien;
         ien= bn.l[s][0];


         for( t=ist;t<ien;t++ )
        {
    
            r= frule_t( bn.f[t], bsize+1,bsize+1,bsize+1 );
            c= bn.h[t];
            o= bn.q[t];
            o= hx[c].q[o].o;

            b= bn.b[t];
            jst= 0;
            jen= lb[b];
            if( b > 0 ){ jst= lb[b-1]; };

            d=-1;
            if( o ){ d=-d; };

            for( j=0;j<bsize;j++ )
           {
               for( i=0;i<bsize;i++ )
              {

                  bw.loadv( i,j,t,wb,v );

                  jmin=-1;
                  dmin= 999;

                  for( k=jst;k<jen;k++ )
                 {
                     REAL_ d= abs(v[0]- v0[k][0])+   
                              abs(v[1]- v0[k][1])+   
                              abs(v[2]- v0[k][2]);

                     if( d < dmin )
                    {
                        dmin= d;
                        jmin= k;
                    }                     
                 }
                  sb.storev( i,j,t,vb,v0[jmin]+3 );

              }
           }
        }
     }



      return;
  }


   void q3x_t::cinit( REAL_ *cf, REAL_ *ct )
  {
      INT_   ist,ien, i,j,k,c, t0;
      char   name[1024];
      FILE  *f;
      REAL_  buf[NCJG_];
      char *line=NULL;
      char  *tok=NULL;
      size_t n=0;

      ien= bn.l[AWALLS-1][0];
      t0= ien;
      for( k=AWALLS;k<TWALLS;k++ )
     {
         ist= ien;
         ien= bn.l[k][0];
         for( INT_ t=ist;t<ien;t++ )
        {
    
            c= bn.h[t];
            if( c > -1 )
           {   

               sprintf( name,"wco/t/%06d", bn.i[t] );
               f= fopen( name,"r" );
               assert( f );
               for( j=0;j<bsize;j++ )
              {
                  for( i=0;i<bsize;i++ )
                 {
                     getline( &line,&n, f );

                     tok= strtok( line," " );
                     for( INT_ l=0;l<cjg.n;l++ )
                    { 
                        sscanf( tok,"%lf", buf+l ); 
                        tok= strtok( NULL," " );
                    };
                     cj.storev(i,j, t-t0,  cf, buf   );
                     for( INT_ l=0;l<cjg.n;l++ )
                    { 
                        sscanf( tok,"%lf", buf+l ); 
                        tok= strtok( NULL," " );
                    };
                     cj.storev(i,j, t-t0,  ct, buf   );
                 }
              }
               fclose( f );
           }
        }
     }
    ::free( line );

  }
