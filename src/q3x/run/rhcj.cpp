#  include <q3x/q3x.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::rhcj( REAL_ *cf, REAL_ *rb, REAL_ *vc, REAL_ *rc )
  {

      REAL_    v0[NVAR_];
      REAL_    v1[NCJG_];
      REAL_    v2[NCJG_];

      INT_    c, i,j,k, l, t0;
      INT_    ist,ien;


      for( l=0;l<cjg.n;l++ ){ rc[l]=0. ; v2[l]= 0.; rc[l]= 0; };

      if( cjg.n <= 0 ){ return; };

      ien= bn.l[AWALLS-1][0];
      t0= ien;
      for( k=AWALLS;k<TWALLS;k++ )
     {
         ist= ien;
         ien= bn.l[k][0];
         for( INT_ t=ist;t<ien;t++ )
        {
    
            c= bn.h[t];
            if( c > -1 )
           {   
               for( j=0;j<bsize;j++ )
              {
                  for( i=0;i<bsize;i++ )
                 {

                        sb.loadv(i,j,  t,  rb,   v0 ); // note for isothermal walls boundary residual is the wall flux
                        cj.loadv(i,j,t-t0, cf,   v1 ); // note for isothermal walls boundary residual is the wall flux

                        for( l=0;l<cjg.n;l++ )
                       {
                           v2[l]+= cjg.ff[l]*v1[l]*v0[4];
                       }
                 }
              }
           }
        }
     }

      MPI_Allreduce( v2,rc, cjg.n, REAL_MPI, MPI_SUM, MPI_COMM_WORLD );

      for( l=0;l<cjg.n;l++ )
     {
         rc[l]+= cjg.z[l]*vc[l];
     }

      return;
  }

   void q3x_t::upcj( REAL_ *ct, REAL_ *vb, REAL_ *vc0, REAL_ *rc, REAL_ *vc, REAL_ dt )
  {

      REAL_    v0[  NVAR_];
      REAL_    v1[  NCJG_];

      INT_    c, i,j,k, l, t0;
      INT_    ist,ien;

      if( cjg.n <= 0 ){ return; }

      for( l=0;l<cjg.n;l++ ){ vc[l]= vc0[l]+ dt*rc[l]; }
      if( rank == 0 )
     {
         for( l=0;l<cjg.n;l++ )
        {
            fprintf( cjg.f, "%12.5e ", vc[l] );
        }
         fprintf( cjg.f, "\n" );
     }


      ien= bn.l[AWALLS-1][0];
      t0= ien;
      for( k=AWALLS;k<TWALLS;k++ )
     {
         ist= ien;
         ien= bn.l[k][0];
         for( INT_ t=ist;t<ien;t++ )
        {
 
            c= bn.h[t];
            if( c > -1 )
           {   
               for( j=0;j<bsize;j++ )
              {
                  for( i=0;i<bsize;i++ )
                 {

                        cj.loadv(i,j,t-t0, ct,v1 ); // note for isothermal walls boundary residual is the wall flux

                        v0[0]=0;
                        v0[1]=0;
                        v0[2]=0;
                        v0[3]=0;
                        v0[4]=0;
             
                        for( l=0;l<cjg.n;l++ )
                       {
                           v0[3]+= cjg.ft[l]*v1[l];
                       }
                        sb.storev(i,j,  t,  vb,   v0 ); 
                 }
              }
           }
        }
     }

      return;
  }
