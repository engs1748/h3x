
#  include <q3x/q3x.h>
#  include <q3x/bias.h>

#  define BIAS bias

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::rhs( REAL_ *wm, REAL_ *wf, REAL_ *wb, REAL_ *wc, REAL_ *dxdx,
                    REAL_ *vb0, REAL_ *vb, 
                    REAL_  *v, REAL_ *dvdx, REAL_ *rb, REAL_  *r, REAL_ *lhd )
  {

      INT_    ntsk;
      INT_    tasks[NTASK];

      REAL_    q0[  NVAR_], q1[  NVAR_], qi[  NVAR_], qj[  NVAR_], qk[  NVAR_];
      REAL_    u0[  NVAR_], u1[  NVAR_], ui[  NVAR_], uj[  NVAR_], uk[  NVAR_];

      REAL_   q0x[3*NVAR_],q1x[3*NVAR_],qix[3*NVAR_],qjx[3*NVAR_],qkx[3*NVAR_];
      REAL_   x0x[      6],x1x[      6],xix[      6],xjx[      6],xkx[      6];

      REAL_    f0[  NVAR_], fi[  NVAR_], fj[  NVAR_], fk[  NVAR_];
      REAL_    g0[  NVAR_], gi[  NVAR_], gj[  NVAR_], gk[  NVAR_];
      REAL_   w0[MSIZE_] ,w1[MSIZE_] ,wi[MSIZE_] ,wj[MSIZE_] ,wk[MSIZE_];
      REAL_   w[1+3+MSIZE_];

      REAL_   l0,li,lj,lk, d0,di,dj,dk;
      INT_    c,d, e,f,g, i,j,k, l,m,n, t,o;
      INT_    ist,ien;
      frule_t t0,t1;

      REAL_   qb[NVAR_];

      exchange(  dvdx, sd[1], com[0], msg[0] );

      so[1].memset(   r,0 );
         ld.memset( lhd,0 );
      o= gas.nvar();

// boundary loop -find way to do with openmp

      ien= 0;
      for( k=0;k<FREES;k++ )
     {
         ist= ien;
         ien= bn.l[k][0];
         for( INT_ t=ist;t<ien;t++ )
        {
    
            t0= frule_t( bn.f[t], bsize,bsize,bsize );
            c= bn.h[t];
            if( c > -1 )
           {  
               for( j=0;j<bsize;j++ )
              {
                  for( i=0;i<bsize;i++ )
                 {
                     t0.transform( i,j,0, l,m,n );
                     mw[0].loadv(l,m,n,  c,wm,w0  );
                     so[0].loadv(l,m,n,  c, v,q0 );


                        bw.loadv(i,j,    t,wb, w );
                        sb.loadv(i,j,    t,vb0,qb );
             
                     gas.IFLUX( q0,qb, w+3,0., f0,l0 );
             
                     so[0].fmav(l,m,n,c,   r, -1.,f0 );
                        ld.addv(l,m,n,c, lhd,&l0 );
             
                 }
              }
           }
        }
     }
      for( k=FREES;k<FREES+1;k++ )
     {
         ist= ien;
         ien= bn.l[k][0];
         for( INT_ t=ist;t<ien;t++ )
        {
    
            t0= frule_t( bn.f[t], bsize,bsize,bsize );
            c= bn.h[t];
            if( c > -1 )
           {   
               for( j=0;j<bsize;j++ )
              {
                  for( i=0;i<bsize;i++ )
                 {
                     t0.transform( i,j,0, l,m,n );
                     mw[0].loadv(l,m,n,  c,wm,w0  );

                     so[0].loadv(l,m,n,  c, v,q0 );
                        bw.loadv(i,j,    t,wb, w );
             
                     gas.WFLUX( q0, w+3,0., f0,l0 );
             
                     so[0].fmav(l,m,n,c,   r, -1.,f0 );
                        ld.addv(l,m,n,c, lhd,&l0 );
             
                 }
              }
           }
        }
     }
      for( k=FREES+1;k<BTYPES;k++ )
     {
         ist= ien;
         ien= bn.l[k][0];
         for( INT_ t=ist;t<ien;t++ )
        {
    
            t0= frule_t( bn.f[t], bsize,bsize,bsize );
            c= bn.h[t];
            if( c > -1 )
           {   
               for( j=0;j<bsize;j++ )
              {
                  for( i=0;i<bsize;i++ )
                 {
                     t0.transform( i,j,0, l,m,n );

                     mw[0].loadv(l,m,n,  c,wm,w0  );
                     so[0].loadv(l,m,n,  c, v,q0 );

                        bw.loadv(i,j,    t,wb, w );
             
                     gas.FFLUX( w0,q0, w, f0,l0 );
             
                     so[0].fmav(l,m,n,c,   r, -1.,f0 );
                        ld.addv(l,m,n,c, lhd,&l0 );
             
                 }
              }
           }
        }
     }


#pragma omp parallel for private( t,  i,  j,  k,  w, \
                                 w0, wi, wj, wk, \
                                 u0, ui, uj, uk, \
                                 q0, qi, qj, qk, \
                                 x0x,xix,xjx,xkx,\
                                 q0x,qix,qjx,qkx,\
                                 f0, fi, fj, fk, \
                                 gi, gj, gk, l0, \
                                 li, lj, lk, \
                                 di, dj, dk ) schedule(dynamic)

      for( t=0;t<hx.n;t++ )
     {

         for( k=0;k<bsize-1;k++ )
        {
            for( j=0;j<bsize-1;j++ )
           {
               for( i=0;i<bsize-1;i++ )
              {

                  mw[0].loadv(i,  j,  k,  t,wm,w0  );
                  mw[0].loadv(i+1,j,  k,  t,wm,wi  );
                  mw[0].loadv(i,  j+1,k,  t,wm,wj  );
                  mw[0].loadv(i,  j,  k+1,t,wm,wk  );

                  so[0].loadv(i,  j,  k,  t,v, q0 );
                  so[0].loadv(i+1,j,  k,  t,v, qi );
                  so[0].loadv(i,  j+1,k,  t,v, qj );
                  so[0].loadv(i,  j,  k+1,t,v, qk );

                  xd[0].loadv(i,  j,  k,  t,dxdx, x0x );
                  xd[0].loadv(i+1,j,  k,  t,dxdx, xix );
                  xd[0].loadv(i,  j+1,k,  t,dxdx, xjx );
                  xd[0].loadv(i,  j,  k+1,t,dxdx, xkx );

                  sd[0].loadv(i,  j,  k,  t,dvdx, q0x );
                  sd[0].loadv(i+1,j,  k,  t,dvdx, qix );
                  sd[0].loadv(i,  j+1,k,  t,dvdx, qjx );
                  sd[0].loadv(i,  j,  k+1,t,dvdx, qkx );

//                   fw.loadv(i,  j,  k,  t,wf,w  );

                  BIAS( o, w+I+0,w+I+3, w0,wi, q0,qi, x0x,xix, q0x,qix, u0,ui );
                  gas.IFLUX( u0,ui, w+I+3,0., fi,li );

                  BIAS( o, w+J+0,w+J+3, w0,wj, q0,qj, x0x,xjx, q0x,qjx, u0,uj );
                  gas.IFLUX( u0,uj, w+J+3,0., fj,lj );

                  BIAS( o, w+K+0,w+K+3, w0,wk, q0,qk, x0x,xkx, q0x,qkx, u0,uk );
                  gas.IFLUX( u0,uk, w+K+3,0., fk,lk );

                  gas.VFLUX( w0,x0x,q0,q0x, wi,xix,qi,qix, w+I, gi,di );
                  gas.VFLUX( w0,x0x,q0,q0x, wj,xjx,qj,qjx, w+J, gj,dj );
                  gas.VFLUX( w0,x0x,q0,q0x, wk,xkx,qk,qkx, w+K, gk,dk );

                  fi[0]+= gi[0];
                  fi[1]+= gi[1];
                  fi[2]+= gi[2];
                  fi[3]+= gi[3];
                  fi[4]+= gi[4];

                  fj[0]+= gj[0];
                  fj[1]+= gj[1];
                  fj[2]+= gj[2];
                  fj[3]+= gj[3];
                  fj[4]+= gj[4];

                  fk[0]+= gk[0];
                  fk[1]+= gk[1];
                  fk[2]+= gk[2];
                  fk[3]+= gk[3];
                  fk[4]+= gk[4];

                  li+= di;
                  lj+= dj;
                  lk+= dk;

                  f0[0]= fi[0]+fj[0]+fk[0];
                  f0[1]= fi[1]+fj[1]+fk[1];
                  f0[2]= fi[2]+fj[2]+fk[2];
                  f0[3]= fi[3]+fj[3]+fk[3];
                  f0[4]= fi[4]+fj[4]+fk[4];

                  so[0].subv(i,  j,  k,  t,  r, f0 );
                  so[0].addv(i+1,j,  k,  t,  r, fi );
                  so[0].addv(i,  j+1,k,  t,  r, fj );
                  so[0].addv(i,  j,  k+1,t,  r, fk );

                  l0= li+ lj+ lk;
                     ld.addv(i,  j,  k,  t,lhd,&l0 );
                     ld.addv(i+1,j,  k,  t,lhd,&li );
                     ld.addv(i,  j+1,k,  t,lhd,&lj );
                     ld.addv(i,  j,  k+1,t,lhd,&lk );

              }
           }
        }

         i=bsize-1;
         for( k=0;k<bsize-1;k++ )
        {
            for( j=0;j<bsize-1;j++ )
           {
               mw[0].loadv(i,  j,  k,  t,wm,w0  );
               mw[0].loadv(i,  j+1,k,  t,wm,wj  );
               mw[0].loadv(i,  j,  k+1,t,wm,wk  );

               so[0].loadv(i,  j,  k,  t,v, q0 );
               so[0].loadv(i,  j+1,k,  t,v, qj );
               so[0].loadv(i,  j,  k+1,t,v, qk );
               xd[0].loadv(i,  j,  k,  t,dxdx, x0x );
               xd[0].loadv(i,  j+1,k,  t,dxdx, xjx );
               xd[0].loadv(i,  j,  k+1,t,dxdx, xkx );

               sd[0].loadv(i,  j,  k,  t,dvdx, q0x );
               sd[0].loadv(i,  j+1,k,  t,dvdx, qjx );
               sd[0].loadv(i,  j,  k+1,t,dvdx, qkx );
//             fw.loadv(i,  j,  k,  t,wf,w );

               BIAS( o, w+J+0,w+J+3, w0,wj, q0,qj, x0x,xjx, q0x,qjx, u0,uj );
               gas.IFLUX( u0,uj, w+J+3,0., fj,lj );

               BIAS( o, w+K+0,w+K+3, w0,wk, q0,qk, x0x,xkx, q0x,qkx, u0,uk );
               gas.IFLUX( u0,uk, w+K+3,0., fk,lk );

               gas.VFLUX( w0,x0x,q0,q0x, wj,xjx,qj,qjx, w+J, gj,dj );
               gas.VFLUX( w0,x0x,q0,q0x, wk,xkx,qk,qkx, w+K, gk,dk );

               fj[0]+= gj[0];
               fj[1]+= gj[1];
               fj[2]+= gj[2];
               fj[3]+= gj[3];
               fj[4]+= gj[4];

               fk[0]+= gk[0];
               fk[1]+= gk[1];
               fk[2]+= gk[2];
               fk[3]+= gk[3];
               fk[4]+= gk[4];

               lj+= dj;
               lk+= dk;

               f0[0]= fj[0]+fk[0];
               f0[1]= fj[1]+fk[1];
               f0[2]= fj[2]+fk[2];
               f0[3]= fj[3]+fk[3];
               f0[4]= fj[4]+fk[4];

               so[0].subv(i,  j,  k,  t,  r, f0 );
               so[0].addv(i,  j+1,k,  t,  r, fj );
               so[0].addv(i,  j,  k+1,t,  r, fk );

               l0= lj+ lk;
                  ld.addv(i,  j,  k,  t,lhd,&l0 );
                  ld.addv(i,  j+1,k,  t,lhd,&lj );
                  ld.addv(i,  j,  k+1,t,lhd,&lk );

           }
        }

         j=bsize-1;
         for( k=0;k<bsize-1;k++ )
        {
            for( i=0;i<bsize-1;i++ )
           {
               mw[0].loadv(i,  j,  k,  t,wm,w0  );
               mw[0].loadv(i+1,j,  k,  t,wm,wi  );
               mw[0].loadv(i,  j,  k+1,t,wm,wk  );

               so[0].loadv(i,  j,  k,  t,v, q0 );
               so[0].loadv(i+1,j,  k,  t,v, qi );
               so[0].loadv(i,  j,  k+1,t,v, qk );
               xd[0].loadv(i,  j,  k,  t,dxdx, x0x );
               xd[0].loadv(i+1,j,  k,  t,dxdx, xix );
               xd[0].loadv(i,  j,  k+1,t,dxdx, xkx );

               sd[0].loadv(i,  j,  k,  t,dvdx, q0x );
               sd[0].loadv(i+1,j,  k,  t,dvdx, qix );
               sd[0].loadv(i,  j,  k+1,t,dvdx, qkx );
//             fw.loadv(i,  j,  k,  t,wf,w );

               BIAS( o, w+I+0,w+I+3, w0,wi, q0,qi, x0x,xix, q0x,qix, u0,ui );
               gas.IFLUX( u0,ui, w+I+3,0., fi,li );

               BIAS( o, w+K+0,w+K+3, w0,wk, q0,qk, x0x,xkx, q0x,qkx, u0,uk );
               gas.IFLUX( u0,uk, w+K+3,0., fk,lk );

               gas.VFLUX( w0,x0x,q0,q0x, wi,xix,qi,qix, w+I, gi,di );
               gas.VFLUX( w0,x0x,q0,q0x, wk,xkx,qk,qkx, w+K, gk,dk );

               fi[0]+= gi[0];
               fi[1]+= gi[1];
               fi[2]+= gi[2];
               fi[3]+= gi[3];
               fi[4]+= gi[4];

               fk[0]+= gk[0];
               fk[1]+= gk[1];
               fk[2]+= gk[2];
               fk[3]+= gk[3];
               fk[4]+= gk[4];

               li+= di;
               lk+= dk;

               f0[0]= fi[0]+fk[0];
               f0[1]= fi[1]+fk[1];
               f0[2]= fi[2]+fk[2];
               f0[3]= fi[3]+fk[3];
               f0[4]= fi[4]+fk[4];

               so[0].subv(i,  j,  k,  t,  r, f0 );
               so[0].addv(i+1,j,  k,  t,  r, fi );
               so[0].addv(i,  j,  k+1,t,  r, fk );

               l0= li+ lk;
                  ld.addv(i,  j,  k,  t,lhd,&l0 );
                  ld.addv(i+1,j,  k,  t,lhd,&li );
                  ld.addv(i,  j,  k+1,t,lhd,&lk );

           }
        }

         k=bsize-1;
         for( j=0;j<bsize-1;j++ )
        {
            for( i=0;i<bsize-1;i++ )
           {
               mw[0].loadv(i,  j,  k,  t,wm,w0  );
               mw[0].loadv(i+1,j,  k,  t,wm,wi  );
               mw[0].loadv(i,  j+1,k,  t,wm,wj  );

               so[0].loadv(i,  j,  k,  t,v, q0 );
               so[0].loadv(i+1,j,  k,  t,v, qi );
               so[0].loadv(i,  j+1,k,  t,v, qj );

               xd[0].loadv(i,  j,  k,  t,dxdx, x0x );
               xd[0].loadv(i+1,j,  k,  t,dxdx, xix );
               xd[0].loadv(i,  j+1,k,  t,dxdx, xjx );

               sd[0].loadv(i,  j,  k,  t,dvdx, q0x );
               sd[0].loadv(i+1,j,  k,  t,dvdx, qix );
               sd[0].loadv(i,  j+1,k,  t,dvdx, qjx );
//             fw.loadv(i,  j,  k,  t,wf,w );

               BIAS( o, w+I+0,w+I+3, w0,wi, q0,qi, x0x,xix, q0x,qix, u0,ui );
               gas.IFLUX( u0,ui, w+I+3,0., fi,li );

               BIAS( o, w+J+0,w+J+3, w0,wj, q0,qj, x0x,xjx, q0x,qjx, u0,uj );
               gas.IFLUX( u0,uj, w+J+3,0., fj,lj );

               gas.VFLUX( w0,x0x,q0,q0x, wi,xix,qi,qix, w+I, gi,di );
               gas.VFLUX( w0,x0x,q0,q0x, wj,xjx,qj,qjx, w+J, gj,dj );

               fi[0]+= gi[0];
               fi[1]+= gi[1];
               fi[2]+= gi[2];
               fi[3]+= gi[3];
               fi[4]+= gi[4];

               fj[0]+= gj[0];
               fj[1]+= gj[1];
               fj[2]+= gj[2];
               fj[3]+= gj[3];
               fj[4]+= gj[4];

               li+= di;
               lj+= dj;

               f0[0]= fi[0]+fj[0];
               f0[1]= fi[1]+fj[1];
               f0[2]= fi[2]+fj[2];
               f0[3]= fi[3]+fj[3];
               f0[4]= fi[4]+fj[4];

               so[0].subv(i,  j,  k,  t,  r, f0 );
               so[0].addv(i+1,j,  k,  t,  r, fi );
               so[0].addv(i,  j+1,k,  t,  r, fj );

               l0= li+ lj;
                  ld.addv(i,  j,  k,  t,lhd,&l0 );
                  ld.addv(i+1,j,  k,  t,lhd,&li );
                  ld.addv(i,  j+1,k,  t,lhd,&lj );

           }
        }

         k=bsize-1;
         j=bsize-1;
         for( i=0;i<bsize-1;i++ )
        {
            mw[0].loadv(i,  j,  k,  t,wm,w0  );
            mw[0].loadv(i+1,j,  k,  t,wm,wi  );

            so[0].loadv(i,  j,  k,  t,v, q0 );
            so[0].loadv(i+1,j,  k,  t,v, qi );
            xd[0].loadv(i,  j,  k,  t,dxdx, x0x );
            xd[0].loadv(i+1,j,  k,  t,dxdx, xix );

            sd[0].loadv(i,  j,  k,  t,dvdx, q0x );
            sd[0].loadv(i+1,j,  k,  t,dvdx, qix );
//             fw.loadv(i,  j,  k,  t,wf,w );
   
            BIAS( o, w+I+0,w+I+3, w0,wi, q0,qi, x0x,xix, q0x,qix, u0,ui );
            gas.IFLUX( u0,ui, w+I+3,0., fi,li );
            gas.VFLUX( w0,x0x,q0,q0x, wi,xix,qi,qix, w+I, gi,di );

            fi[0]+= gi[0];
            fi[1]+= gi[1];
            fi[2]+= gi[2];
            fi[3]+= gi[3];
            fi[4]+= gi[4];

            li+= di;

            so[0].subv(i,  j,  k,  t,  r,fi );
            so[0].addv(i+1,j,  k,  t,  r,fi );

            l0= li;
               ld.addv(i,  j,  k,  t,lhd,&l0 );
               ld.addv(i+1,j,  k,  t,lhd,&li );

        }

         k=bsize-1;
         i=bsize-1;
         for( j=0;j<bsize-1;j++ )
        {
            mw[0].loadv(i,  j,  k,  t,wm,w0  );
            mw[0].loadv(i,  j+1,k,  t,wm,wj  );

            so[0].loadv(i,  j,  k,  t,v, q0 );
            so[0].loadv(i,  j+1,k,  t,v, qj );
            xd[0].loadv(i,  j,  k,  t,dxdx, x0x );
            xd[0].loadv(i,  j+1,k,  t,dxdx, xjx );

            sd[0].loadv(i,  j,  k,  t,dvdx, q0x );
            sd[0].loadv(i,  j+1,k,  t,dvdx, qjx );
//             fw.loadv(i,  j,  k,  t,wf,w );

            BIAS( o, w+J+0,w+J+3, w0,wj, q0,qj, x0x,xjx, q0x,qjx, u0,uj );
            gas.IFLUX( u0,uj, w+J+3,0., fj,lj );
            gas.VFLUX( w0,x0x,q0,q0x, wj,xjx,qj,qjx, w+J, gj,dj );

            fj[0]+= gj[0];
            fj[1]+= gj[1];
            fj[2]+= gj[2];
            fj[3]+= gj[3];
            fj[4]+= gj[4];

            lj+= dj;

            so[0].subv(i,  j,  k,  t,  r,fj );
            so[0].addv(i,  j+1,k,  t,  r,fj );

            l0= lj;
               ld.addv(i,  j,  k,  t,lhd,&l0 );
               ld.addv(i,  j+1,k,  t,lhd,&lj );

        }

         j=bsize-1;
         i=bsize-1;
         for( k=0;k<bsize-1;k++ )
        {
            mw[0].loadv(i,  j,  k,  t,wm,w0  );
            mw[0].loadv(i,  j,  k+1,t,wm,wk  );

            so[0].loadv(i,  j,  k,  t,v, q0 );
            so[0].loadv(i,  j,  k+1,t,v, qk );
            xd[0].loadv(i,  j,  k,  t,dxdx, x0x );
            xd[0].loadv(i,  j,  k+1,t,dxdx, xkx );

            sd[0].loadv(i,  j,  k,  t,dvdx, q0x );
            sd[0].loadv(i,  j,  k+1,t,dvdx, qkx );
//             fw.loadv(i,  j,  k,  t,wf,w );

            BIAS( o, w+K+0,w+K+3, w0,wk, q0,qk, x0x,xkx, q0x,qkx, u0,uk );
            gas.IFLUX( u0,uk, w+K+3,0., fk,lk );
            gas.VFLUX( w0,x0x,q0,q0x, wk,xkx,qk,qkx, w+K, gk,dk );

            fk[0]+= gk[0];
            fk[1]+= gk[1];
            fk[2]+= gk[2];
            fk[3]+= gk[3];
            fk[4]+= gk[4];

            lk+= dk;

            so[0].subv(i,  j,  k,  t,  r,fk );
            so[0].addv(i,  j,  k+1,t,  r,fk );

            l0= lk;
               ld.addv(i,  j,  k,  t,lhd,&l0 );
               ld.addv(i,  j,  k+1,t,lhd,&lk );

        }

     }

/*    if( dbg )
     {
         printf( "\n" );
         printf( "rhs - internals\n" );
         printf( "\n" );
         for( t=0;t<hx.n;t++ )
        {
            for( k=0;k<bsize;k++ )
           {
               for( j=0;j<bsize;j++ )
              {
                  for( i=0;i<bsize;i++ )
                 {
                     so[0].loadv(l,m,n,t,   r, f0 );
                     printf( " %4d %4d %4d %4d % 12.5e % 12.5e % 12.5e % 12.5e % 12.5e\n", t,i,j,k, 
f0[0]   ,
f0[1]   ,
f0[2]   ,
f0[3]   ,
f0[4]    );
                 }
              }
           }
        }
         printf( "\n" );
     }*/

// handle interfaces with only local data dependencies
       
      while( locals( com[0],msg[0], ntsk,tasks ) )
     {

#pragma omp parallel for private(t,c,d,e,f,g,i,j,l,m,n,t0,t1,w, w0, w1,\
                                                                q0, q1,\
                                                               x0x,x1x,\
                                                               q0x,q1x,\
                                                                u0, u1,\
                                                                 f0,g0,l0,d0) schedule(dynamic)
         for( INT_ t=0;t<ntsk;t++ )
        {
            INT_    num,loc;
            INT_   *idx;
    
            com[0].task( tasks[t], num,loc, &idx );
            com[0].taskinfo( sd[0], idx[0], t0,c );
            com[0].taskinfo( sd[0], idx[1], t1,d );
    
            for( j=0;j<bsize;j++ )
           {
               for( i=0;i<bsize;i++ )
              {
                  t0.transform( i,j,0, l,m,n );
                  t1.transform( i,j,0, e,f,g );

                  mw[0].loadv(l,m,n,c,wm,w0  );
                  mw[0].loadv(e,f,g,d,wm,w1  );

                  so[0].loadv(l,m,n,  c,v,q0 );
                  so[0].loadv(e,f,g,  d,v,q1 );

                  xd[0].loadv(l,m,n,  c,dxdx, x0x );
                  xd[0].loadv(e,f,g,  d,dxdx, x1x );

                  sd[0].loadv(l,m,n,  c,dvdx, q0x );
                  sd[0].loadv(e,f,g,  d,dvdx, q1x );

                     cw.loadv(i,j,  idx[0], wc,w );
    
                  BIAS( o, w+0,w+3, w0,w1, q0,q1, x0x,x1x, q0x,q1x, u0,u1 );
                  gas.IFLUX( u0,u1, w+3,0., f0,l0 );
                  gas.VFLUX( w0,x0x,q0,q0x, w1,x1x,q1,q1x, w, g0,d0 );

                  f0[0]+= g0[0];
                  f0[1]+= g0[1];
                  f0[2]+= g0[2];
                  f0[3]+= g0[3];
                  f0[4]+= g0[4];

                  l0+= d0;

                  so[0].subv(l,m,n,c,  r,f0 );
                  so[0].addv(e,f,g,d,  r,f0 );

                     ld.addv(l,m,n,c,lhd,&l0 );
                     ld.addv(e,f,g,d,lhd,&l0 );

              }
           }
        }
     }

// handle interfaces with data dependencies on remote ranks

      while( transit( com[0],msg[0], ntsk,tasks, 1 ) )
     {
#pragma omp parallel for private(t,c,d,i,j,l,m,n,t0,w, w0, w1,\
                                                       q0, q1,\
                                                      x0x,x1x,\
                                                      q0x,q1x,\
                                                        u0,u1,\
                                                        f0,g0,l0,d0) schedule(dynamic)
         for( INT_ t=0;t<ntsk;t++ )
        {
            INT_    num,loc;
            INT_   *idx;
    
            com[0].task( tasks[t], num,loc, &idx );
            com[0].taskinfo( sd[0], idx[0], t0,c );
            d= idx[1];
    
            for( j=0;j<bsize;j++ )
           {
               for( i=0;i<bsize;i++ )
              {
    
                  t0.transform( i,j,0, l,m,n );

                  mw[0].loadv(l,m,n,c,wm,w0  );
                  mw[1].loadv(i,j,0,d,wm,w1  );

                  so[0].loadv(l,m,n,     c,v, q0 );
                  so[1].loadv(i,j,0,     d,v, q1 );

                  xd[0].loadv(l,m,n,  c,dxdx, x0x );
                  xd[1].loadv(i,j,0,  d,dxdx, x1x );

                  sd[0].loadv(l,m,n,  c,dvdx, q0x );
                  sd[1].loadv(i,j,0,  d,dvdx, q1x );

                     cw.loadv(i,j,  idx[0], wc,w );
    
                  BIAS( o, w+0,w+3, w0,w1, q0,q1, x0x,x1x, q0x,q1x, u0,u1 );
                  gas.IFLUX( u0,u1, w+3,0., f0,l0 );
                  gas.VFLUX( w0,x0x,q0,q0x, w1,x1x,q1,q1x, w, g0,d0 );

                  f0[0]+= g0[0];
                  f0[1]+= g0[1];
                  f0[2]+= g0[2];
                  f0[3]+= g0[3];
                  f0[4]+= g0[4];

                  l0+= d0;

                  so[0].subv(l,m,n,c,  r, f0 );
                     ld.addv(l,m,n,c,lhd,&l0 );

              }
           }
        }
     }

      return;
  }
