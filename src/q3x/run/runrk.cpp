
#  include <q3x/q3x.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::runrk()
  {
      REAL_           vc[3][NCJG_];
      REAL_           rc[4][NCJG_];

      REAL_          *v[3]={NULL,NULL,NULL};
      REAL_          *r[4]={NULL,NULL,NULL};

      REAL_          *x=NULL,*d=NULL;
      REAL_          *y=NULL;
      REAL_          *yi=NULL;

      REAL_          *wb=NULL;
      REAL_          *vb[3]={NULL,NULL,NULL};
      REAL_          *rb[4]={NULL,NULL,NULL};
      REAL_         *vb0=NULL;
      REAL_          *wc=NULL;
      REAL_          *wm=NULL;

      REAL_          *cf=NULL;
      REAL_          *ct=NULL;

      REAL_        *dxdx=NULL;
      REAL_        *dvdx=NULL;

      REAL_           dt;
      REAL_2         sgm;
      REAL_          res[NVAR_+1];

      bool          flag;

      double         stime,etime;
      const REAL_3   alp[3]={{1,0,0},{0.25,0.25,0},{1./6.,1./6,2./3.}};
      

      gas.init();

      start();

      co[3].malloc( &x  );
         cb.malloc( &y  );
         ci.malloc( &yi );

      mw[1].malloc( &wm ); 
         bw.malloc( &wb ); 
         cw.malloc( &wc ); 

      so[1].malloc( r+0 );
      so[1].malloc( r+1 );
      so[1].malloc( r+2 );
      so[1].malloc( r+3 );

      so[1].malloc( v+0 );
      so[1].malloc( v+1 );
      so[1].malloc( v+2 );

         ld.malloc( &d  );

         sb.malloc(  vb+0 );
         sb.malloc(  vb+1 );
         sb.malloc(  vb+2 );

         sb.malloc(  rb+0 );
         sb.malloc(  rb+1 );
         sb.malloc(  rb+2 );
         sb.malloc(  rb+3 );

         sb.malloc( &vb0);

      xd[1].malloc( &dxdx );
      sd[1].malloc( &dvdx );

      grid( x,y,yi );

      metrics( x, wm,wb,wc );
      grdx( wm,wb, dxdx );

      chk( wm,wb,wc, x,r[0] );

      binit( wb, vb0 );
      if( cjg.n > 0 )
     {
         cj.malloc(  &cf);
         cj.malloc(  &ct);
         cinit( cf,ct );

         if( rank == 0 )
        {
            cjg.f= fopen( "cjg_history.dat","w" );
            assert( cjg.f );
        }
     }
      restart( v[0], vb[0], vc[0] );

      sb.memcpy( vb[0],vb[1] );
      sb.memcpy( vb[0],vb[2] );

      stime=MPI_Wtime();
      dt= -1;

      for( INT_ it=0;it<ctr.nt;it++ )
     {
         flag= ( it%ctr.no==0 );

         so[1].memset(   r[0],0 );
         so[1].memset(   r[1],0 );
         so[1].memset(   r[2],0 );
         so[1].memset(   r[3],0 );

         sb.memset(   rb[0],0 );
         sb.memset(   rb[1],0 );
         sb.memset(   rb[2],0 );
         sb.memset(   rb[3],0 );

         cjg.memset( rc[0],0 );
         cjg.memset( rc[1],0 );
         cjg.memset( rc[2],0 );
         cjg.memset( rc[3],0 );
 
         grdv( wm,wb, vb[0],v[0], dxdx,dvdx );
         rhs3( x, wm,wb,wc, dxdx, vb0, vb[0],v[0],dvdx, rb[0],r[0],d );
         rhcj( cf, rb[0],vc[0],rc[0] );
         delt( wm,  d,  ctr.sigma,  dt, sgm );
         so[0].sxpy( r[3], 0.0,r[3], alp[0][0],r[0] ); sb.sxpy( rb[3], 0.0,rb[3], alp[0][0],rb[0] );
         cjg.sxpy( rc[3],0.0,rc[3],alp[0][0],rc[0] );
         upd( wm,wb, vb[0],v[0], vb[1],v[1], rb[3],r[3], dt, ctr.sigma, res, flag );
         upcj( ct, vb[1], vc[0], rc[3], vc[1], dt );



         grdv( wm,wb, vb[1],v[1], dxdx,dvdx );
         rhs3( x, wm,wb,wc, dxdx, vb0,vb[1], v[1],dvdx, rb[1],r[1],d );
         rhcj( cf, rb[1],vc[1],rc[1] );
         so[0].sxpy( r[3], 0.0,r[3], alp[1][0],r[0] ); sb.sxpy( rb[3], 0.0,rb[3], alp[1][0],rb[0] );
         so[0].sxpy( r[3], 1.0,r[3], alp[1][1],r[1] ); sb.sxpy( rb[3], 1.0,rb[3], alp[1][1],rb[1] );
         cjg.sxpy( rc[3],0.0,rc[3],alp[1][0],rc[0] );
         cjg.sxpy( rc[3],1.0,rc[3],alp[1][1],rc[1] );
         upd( wm,wb, vb[1],v[1], vb[2],v[2], rb[3],r[3], dt, ctr.sigma, res, flag );
         upcj( ct, vb[2], vc[1], rc[3], vc[2], dt );


         grdv( wm,wb, vb[2],v[2], dxdx,dvdx );
         rhs3( x, wm,wb,wc, dxdx, vb0,vb[1], v[2],dvdx, rb[2],r[2],d );
         rhcj( cf, rb[2],vc[2],rc[2] );
         so[0].sxpy( r[3], 0.0,r[3], alp[2][0],r[0] ); sb.sxpy( rb[3], 0.0,rb[3], alp[2][0],rb[0] );
         so[0].sxpy( r[3], 1.0,r[3], alp[2][1],r[1] ); sb.sxpy( rb[3], 1.0,rb[3], alp[2][1],rb[1] );
         so[0].sxpy( r[3], 1.0,r[3], alp[2][2],r[2] ); sb.sxpy( rb[3], 1.0,rb[3], alp[2][2],rb[2] );
         cjg.sxpy( rc[3],0.0,rc[3],alp[2][0],rc[0] );
         cjg.sxpy( rc[3],1.0,rc[3],alp[2][1],rc[1] );
         cjg.sxpy( rc[3],1.0,rc[3],alp[2][2],rc[2] );
         upd( wm,wb, vb[2],v[2], vb[0],v[0], rb[3],r[3], dt, ctr.sigma, res, flag );
         upcj( ct, vb[0], vc[2], rc[3], vc[0], dt );

         if( flag )
        { 
            if( rank == 0 )
           {
//             printf( "%9d %9.3e %9.3e %9.3e %9.3e %9.3e %9.3e %9.3e %9.3e %9.3e %9.3e %9.3e %9.3e \n", it, dt, sgm[0]*dt, sgm[1]*dt, res[0],res[1],res[2],res[3],res[4], vc[0][0],vc[0][1],vc[0][2],vc[0][3] );

               printf( "%9d %9.3e %9.3e %9.3e %9.3e %9.3e %9.3e %9.3e %9.3e \n", it, dt, sgm[0]*dt, sgm[1]*dt, res[0],res[1],res[2],res[3],res[4]  );
           }
        } 

         if( it%ctr.ns == 0 )
        {
            save( v[0], vb[0], vc[0], it );
        }
     }

      save( v[0], vb[0], vc[0], -1 );

      etime=MPI_Wtime();

      if( rank==0 )
     {
         printf( "elapsed time %9.3e\n", etime-stime );
     }

      delete[] x;
      delete[] y;
      delete[] yi;
      delete[] v[0];
      delete[] v[1];
      delete[] v[2];
      delete[] vb[0];
      delete[] vb[1];
      delete[] vb[2];
      delete[] r[0];
      delete[] r[1];
      delete[] r[2];
      delete[] r[3];
      delete[] rb[0];
      delete[] rb[1];
      delete[] rb[2];
      delete[] rb[3];
      delete[] d;
      delete[] wm;
      delete[] wb;
      delete[] wc;

      delete[] dxdx;
      delete[] dvdx;

      delete[] cf;
      delete[] ct;

      if( cjg.n > 0 )
     {
         if( rank == 0 )
        {
            fclose( cjg.f );
        }
     }

      stop();

      return;
  }
