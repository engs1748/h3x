
#  include <q3x/q3x.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::runt()
  {

      REAL_          *v=NULL,*r=NULL;
      REAL_          *x=NULL,*d=NULL;
      REAL_          *y=NULL;
      REAL_          *yi=NULL;

      REAL_          *vb=NULL;
//    REAL_         *vb0=NULL;

//    REAL_        sigma=1;
      REAL_          res[1];

      bool          flag;

      double         stime,etime;

      gas.init();

      start();

      co[3].malloc( &x  );
         cb.malloc( &y  );
         ci.malloc( &yi  );

      te[3].malloc( &v  );
      te[3].malloc( &r  );

      td[3].malloc( &v  );
      td[3].malloc( &r  );

      grid( x,y,yi );

// HERE WE NEED TO DECIDE HOW TO KEEP THE TWO FIELDS SEPARATE
//    restart( v, vb );
      stime=MPI_Wtime();

      res[0]=0;

      for( INT_ it=0;it<ctr.nt;it++ )
     {
         flag= ( it%ctr.no==0 );
 
         rht( x, v, r,d );

         for( INT_ t=0;t<hx.n;t++ )
        {
            for( INT_ k=0;k<bsize+1;k++ )
           {
               for( INT_ j=0;j<bsize+1;j++ )
              {
                  for( INT_ i=0;i<bsize+1;i++ )
                 {
                     REAL_          f,g;

                     te[0].loadv( i,  j,  k, t, r, &f );
                     td[0].loadv( i,  j,  k, t, d, &g );


                     f/= g;
                     res[0]+= fabs(f);


                     co[0].subv( i,j,k,t, v,&f );

                 }
              }
           }
        }

         if( flag )
        { 
            if( rank == 0 )
           {
               printf( "%9d %12.6e \n", it, res[0] );
           }
        } 
     }

// DECIDE HOW TO KEEP FIELDS SEPARATE
//    save( v, vb, -1 );

      etime=MPI_Wtime();

      if( rank==0 )
     {
         printf( "elapsed time %9.3e\n", etime-stime );
     }

      delete[] x;
      delete[] y;
      delete[] yi;
      delete[] v;
      delete[] vb;
      delete[] r;
      delete[] d;

      stop();

      return;
  }
