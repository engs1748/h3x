
#  include <q3x/q3x.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::delt( REAL_ *wm, REAL_ *lhd, REAL_ sigma, REAL_ &dt, REAL_ *sgm )
  {

      REAL_   v0[8];
      REAL_2  l0;
      INT_    i,j,k, t;

      REAL_   s;
      REAL_2  t0,t1;

      t1[0]=-1;
      t1[1]=-1;
       
//pragma omp parallel for private(t,i,j,k,v0,l0,s,t0) schedule(dynamic)

      for( t=0;t<hx.n;t++ )
     {

         t0[0]=-1;
         t0[1]=-1;
         for( k=0;k<bsize;k++ )
        {
            for( j=0;j<bsize;j++ )
           {
               for( i=0;i<bsize;i++ )
              {

                     ld.loadv(i,j,k,t,lhd, l0  );
                  mw[0].loadv(i,j,k,t,wm,v0 );

                  s=1./v0[3];
                  t0[0]= fmax( l0[0]*s, t0[0] );
                  t0[1]= fmax( l0[1]*s, t0[1] );

              }
           }
        }

//pragma omp critical
        {
           t1[0]= fmax( t0[0],t1[0] );
           t1[1]= fmax( t0[1],t1[1] );
        }

     }

      MPI_Allreduce( &(t1),&(t0), 2,REAL_MPI, MPI_MAX, MPI_COMM_WORLD );
      s= 1./( t0[0]+t0[1] );

     if( dt < 0 )
     {
         dt= s*sigma;  // this is the original statement
     }
      else
     {
         if( (dt/s < 0.8*sigma) || ( dt/s > 1.2*sigma )  )
        {
            dt= s*sigma;
        }
     }
      sgm[0]= t0[0];
      sgm[1]= t0[1];

      

      return;
  }

