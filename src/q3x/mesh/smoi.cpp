#  include <q3x/q3x.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

#  include <q3x/inline/ellp>
#  include <q3x/inline/ellb>

   void q3x_t::smoi( REAL_ *y, REAL_ *dy, REAL_ *x, REAL_ *r, REAL_ *g, vsurf_t &su, REAL_ rlx )
  {


      INT_       b,c,d;
      INT_       i,j, l,m,n, s,t;
      INT_       num,loc;
      frule_t    r0,r1,r2,r3,r4,r5;
      frule_t    rr;

      INT_      *idx;


      REAL_3     f;
      REAL_b     z;
      REAL_      ww;

      REAL_3     q[3],w;
      REAL_3     u;
      REAL_b     dz;

      vtx_t      v;
      vtx_t      dv[2];

// update loop

//    return;

      assert( bsize < 1024 );

// conformity at vertices
      for( b=0;b<ni;b++ )
     {
         s= bj[b][3];
         t= bj[b][2];

         com[0].task( t, num,loc, &idx );
         com[0].taskinfo( co[0], idx[0], r0,c );
         com[0].taskinfo( co[0], idx[1], r1,d );

         assert( num == 2 );
         assert( loc == 2 );

         for( j=0;j<bsize+1;j++ )
        {
            for( i=0;i<bsize+1;i++ )
           {
               ci.loadv( i,j, b,  y, z );
               su[s]->interp( z, v,dv );
               cross3( dv[0].x,dv[1].x, w ); 
               ww= norm32( w ); scal3( 1./ww,w );

               r0.transform( i,j,0, l,m,n );
             
               co[0].loadv(l,m,n,c,   x, u );
               co[0].loadv(l,m,n,c,   r, f );
             
               u[0]= v[0]-u[0]-f[0];
               u[1]= v[1]-u[1]-f[1];
               u[2]= v[2]-u[2]-f[2];
             
               ww= dot3( u,w );
               f[0]+= ww*w[0];
               f[1]+= ww*w[1];
               f[2]+= ww*w[2];

               co[0].storev( l,m,n,c, r,f );
//             printf( "% 9.3e % 9.3e % 9.3e\n", f[0],f[1],f[2] ); 

               r1.transform( i,j,0, l,m,n );
             
               co[0].loadv(l,m,n,d,   x, u );
               co[0].loadv(l,m,n,d,   r, f );
             
               u[0]= v[0]-u[0]-f[0];
               u[1]= v[1]-u[1]-f[1];
               u[2]= v[2]-u[2]-f[2];
             
               ww= dot3( u,w );
               f[0]+= ww*w[0];
               f[1]+= ww*w[1];
               f[2]+= ww*w[2];

               co[0].storev( l,m,n,d, r,f );
//             printf( "% 9.3e % 9.3e % 9.3e\n", f[0],f[1],f[2] ); 
           }
        }

         for( j=0;j<bsize+1;j++ )
        {
            for( i=0;i<bsize+1;i++ )
           {
       
               r0.transform( i,j,0, l,m,n );
       
               co[0].loadv(l,m,n,c,   x, u );
               co[0].loadv(l,m,n,c,   r, f );
       
               ci.loadv( i,j, b,  y, z );
       
               su[s]->interp( z, v,dv );

               f[0]+= u[0]- v[0];
               f[1]+= u[1]- v[1];
               f[2]+= u[2]- v[2];

               qrf23( dv[0].x,dv[1].x, q[0],q[1], u );
               qrs23( q[0],q[1], u, dz, f );

               z[0]+= rlx*dz[0];
               z[1]+= rlx*dz[1];

               ci.storev( i,j,  b, y,z );
//             printf( "% 9.3e % 9.3e\n", z[0],z[1] ); 
           }
        }

     }



      return;
  }


