
#  include <q3x/q3x.h>


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::scale( REAL_ f )
  {
      MMAP_       co0;
      MMAP_       cb0;
      INT_         i,j,k,l,m;
      REAL_       *x0;
      REAL_       *p0;
      REAL_       *y0;

      REAL_x       xm;
      REAL_b       ym;

      size_t dlen;
      size_t len=0;

      MPI_File fh0, fh1;
      MPI_Status status;
      MPI_Offset off0, off1;
      MPI_Offset offlen;


      MPI_File_open(MPI_COMM_WORLD, "grid/h", MPI_MODE_RDONLY, MPI_INFO_NULL, &fh0); assert( fh0 );

      MPI_File_open(MPI_COMM_WORLD, "grid_scal/h", MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh1);
      assert( fh1 );

      off0= 0; 
      MPI_File_read_at(fh0, off0,    &m, 1, INT_MPI, &status); off0+= sizeof( INT_ );
      MPI_File_read_at(fh0, off0, &dlen, 1,LONG_MPI, &status); off0+= sizeof(size_t );

      bsize=m;

      len=0; co0.dims( len, m+1,  XDIM,m+1,  m+1,  -1 ); assert( len== dlen );

      off1= 0; 
      MPI_File_write_at(fh1, off1,   &m, 1, INT_MPI, &status); off1+= sizeof( INT_ );
      MPI_File_write_at(fh1, off1,&dlen, 1,LONG_MPI, &status); off1+= sizeof(size_t );

      assert( off1 == off0 );

      co0.malloc( &x0 );
      co0.malloc( &p0 );

      offlen= 2*len*sizeof(REAL_);

      for( l=0;l<hx.n;l++ )
     {

         off1= off0+ offlen*hx[l].id;

         MPI_File_read_at(fh0, off1, x0, len, REAL_MPI, &status); off1+= len*sizeof(REAL_);
         MPI_File_read_at(fh0, off1, p0, len, REAL_MPI, &status); 

         for( k=0; k<m+1;k++ )
        {
            for( j=0; j<m+1;j++ )
           {
               for( i=0; i<m+1;i++ )
              {

                  co0.loadv( i,  j,  k,   x0, xm );      

                  xm[0]*= f;
                  xm[1]*= f;
                  xm[2]*= f;

                  co0.storev( i,  j,  k,   x0, xm );      

              }
           }
        }
         off1= off0+ offlen*hx[l].id;

         MPI_File_write_at(fh1, off1, x0, len, REAL_MPI, &status); off1+= len*sizeof(REAL_);
         MPI_File_write_at(fh1, off1, p0, len, REAL_MPI, &status);

     }

      MPI_Barrier( MPI_COMM_WORLD );
      MPI_File_close( &fh0 );
      MPI_File_close( &fh1 );

      delete[] x0; x0= NULL;
      delete[] p0; p0= NULL;


      MPI_File_open(MPI_COMM_WORLD, "grid/b", MPI_MODE_RDONLY, MPI_INFO_NULL, &fh0); assert( fh0 );

      MPI_File_open(MPI_COMM_WORLD, "grid_scal/b", MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh1);

      off0= 0; 
      MPI_File_read_at(fh0, off0,    &m, 1, INT_MPI, &status); off0+= sizeof( INT_ );
      MPI_File_read_at(fh0, off0, &dlen, 1,LONG_MPI, &status); off0+= sizeof(size_t );

      assert( m == bsize );

      len=0; cb0.dims( len, m+1,  BDIM,m+1,   -1 ); assert( len== dlen );

      off1= 0; 
      MPI_File_write_at(fh1, off1,    &m, 1, INT_MPI, &status); off1+= sizeof( INT_ );
      MPI_File_write_at(fh1, off1, &dlen, 1,LONG_MPI, &status); off1+= sizeof(size_t );

      assert( off1==off0 ); 

      cb0.malloc( &y0 );
      offlen= len*sizeof(REAL_);

      for( INT_ p=0;p<BTYPES;p++ )
     {
         for( INT_ g=0;g<nb;g++ )
        {
            if( bz[g].lbl[0] == p && bz[g].bo.n > 0 )
           { 
               for( k=0;k<bz[g].bo.n;k++ )
              {

                  MPI_File_read_at(fh0, off0, y0, len, REAL_MPI, &status); 

/* In principle we shouldn't do anything here for parametric surfaces, but we should for
   RBF surfaces. Solution is to ask the surface to manipulate the surface coordinates 
                  for( j=0; j<m;j++ )
                 {
                     for( i=0; i<m;i++ )
                    {
                        cb0.loadv( i,  j,   y0, ym[0]);
                    }
                 }*/

                  MPI_File_write_at(fh1, off0, y0, len, REAL_MPI, &status); 
                  off0+= offlen;
              }
           } 
        }
     }
      MPI_Barrier( MPI_COMM_WORLD );
      MPI_File_close( &fh0 );
      MPI_File_close( &fh1 );


      MPI_File_open(MPI_COMM_WORLD, "grid/i", MPI_MODE_RDONLY, MPI_INFO_NULL, &fh0); assert( fh0 );

      MPI_File_open(MPI_COMM_WORLD, "grid_scal/i", MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh1);

      off0= 0; 
      MPI_File_read_at(fh0, off0,    &m, 1, INT_MPI, &status); off0+= sizeof( INT_ );
      MPI_File_read_at(fh0, off0, &dlen, 1,LONG_MPI, &status); off0+= sizeof(size_t );

      assert( m == bsize );
      assert( dlen==len );

      off1= 0; 
      MPI_File_write_at(fh1, off1,    &m, 1, INT_MPI, &status); off1+= sizeof( INT_ );
      MPI_File_write_at(fh1, off1, &dlen, 1,LONG_MPI, &status); off1+= sizeof(size_t );

      assert( off1==off0 ); 

      for( INT_ g=0;g<bi.n;g++ )
     {
 
         for( k=0;k<bi[g].bo.n;k++ )
        {

            MPI_File_read_at(fh0, off0, y0, len, REAL_MPI, &status); 

            for( j=0; j<m;j++ )
           {
               for( i=0; i<m;i++ )
              {

                  cb0.loadv( i,  j,   y0, ym );
// see comment above
              }
           }

            MPI_File_write_at(fh1, off1, y0, len, REAL_MPI, &status); 
            off0+= offlen;
        }
     }

      MPI_Barrier( MPI_COMM_WORLD );
      MPI_File_close( &fh0 );
      MPI_File_close( &fh1 );

      delete[] y0; y0= NULL;

  }
