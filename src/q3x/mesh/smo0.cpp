#  include <q3x/q3x.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

#  include <q3x/inline/ellp>
#  include <q3x/inline/ellb>

   void q3x_t::smo0( REAL_ *y, REAL_ *dy, REAL_ *x, REAL_ *p, REAL_ *r, REAL_ *g, vsurf_t &su, REAL_ fct )
  {

      INT_       ntsk;
      INT_       tasks[NTASK];

      INT_       b,c,d;
      INT_       i,j,k, l,m,n, s,t;
      INT_       ist,ien;
      frule_t    r0,r1,r2,r3,r4,r5;
      frule_t    rr;
      INT_       num,loc;
      INT_      *idx;

      REAL_x     x000,x100,x200, x010,x110,x210, x020,x120,x220;
      REAL_x     x001,x101,x201, x011,x111,x211, x021,x121,x221;
      REAL_x     y001,y101,y201, y011,y111,y211, y021,y121,y221;
      REAL_x                          z111;
      REAL_x                          p111,r111;
      REAL_x                          q111;
      REAL_x                          s111;
      REAL_x     x002,x102,x202, x012,x112,x212, x022,x122,x222;
      REAL_x                     y012,y112,y212;

      REAL_      a;


      REAL_x     f;
      REAL_b     z;

      vtx_t      v;
      vtx_t      v1;

      REAL_3     f0;

// update loop

      REAL_3     wrk0[1024];
      REAL_      wrk1[1024];

      assert( bsize < 1024 );

      co[3].memset(   r,0 );
      cd[3].memset(   g,0 );

      cb.memset(   dy,0 );

      exchange(  x, co[1], com[0], msg[1] );
      exchange(  x, co[2], com[1], msg[2] );
      exchange(  x, co[3], com[2], msg[3] );

// Boundary looe

      ien= 0;
      for( b=0;b<BTYPES;b++ )
     {
         ist= ien;
         ien= bn.l[b][0];

         for( t=ist;t<ien;t++ )
        {


            r0= frule_t( bn.f[t], bsize+1,bsize+1,bsize+1 );
            c= bn.h[t];
            s= bn.s[t];

//          printf( "\n" );
//          printf( "%2d %2d\n", hx[c].id,s );
            if( c > -1 )
           {

               k= 0;
               for( j=1;j<bsize;j++ )
              {
                  for( i=1;i<bsize;i++ )
                 {
                     r0.transform( i-1,j-1,0, l,m,n  ); co[0].loadv( l,m,n,c, x,x001 );
                     r0.transform( i  ,j-1,0, l,m,n  ); co[0].loadv( l,m,n,c, x,x101 );
                     r0.transform( i+1,j-1,0, l,m,n  ); co[0].loadv( l,m,n,c, x,x201 );

                     r0.transform( i-1,j  ,0, l,m,n  ); co[0].loadv( l,m,n,c, x,x011 );
                     r0.transform( i  ,j  ,0, l,m,n  ); co[0].loadv( l,m,n,c, x,x111 );
                     r0.transform( i+1,j  ,0, l,m,n  ); co[0].loadv( l,m,n,c, x,x211 );

                     r0.transform( i-1,j+1,0, l,m,n  ); co[0].loadv( l,m,n,c, x,x021 );
                     r0.transform( i  ,j+1,0, l,m,n  ); co[0].loadv( l,m,n,c, x,x121 );
                     r0.transform( i+1,j+1,0, l,m,n  ); co[0].loadv( l,m,n,c, x,x221 );
                                                                                    
                     r0.transform( i-1,j-1,1, l,m,n  ); co[0].loadv( l,m,n,c, x,x002 );
                     r0.transform( i  ,j-1,1, l,m,n  ); co[0].loadv( l,m,n,c, x,x102 );
                     r0.transform( i+1,j-1,1, l,m,n  ); co[0].loadv( l,m,n,c, x,x202 );

                     r0.transform( i-1,j  ,1, l,m,n  ); co[0].loadv( l,m,n,c, x,x012 );
                     r0.transform( i  ,j  ,1, l,m,n  ); co[0].loadv( l,m,n,c, x,x112 );
                     r0.transform( i+1,j  ,1, l,m,n  ); co[0].loadv( l,m,n,c, x,x212 );

                     r0.transform( i-1,j+1,1, l,m,n  ); co[0].loadv( l,m,n,c, x,x022 );
                     r0.transform( i  ,j+1,1, l,m,n  ); co[0].loadv( l,m,n,c, x,x122 );
                     r0.transform( i+1,j+1,1, l,m,n  ); co[0].loadv( l,m,n,c, x,x222 );

                     r0.transform( i  ,j  ,1, l,m,n  ); co[0].loadv( l,m,n,c, p,q111 );

                     p111[0]= r0.f[0][0]*q111[0]+ r0.f[0][1]*q111[1]+ r0.f[0][2]*q111[2];
                     p111[1]= r0.f[1][0]*q111[0]+ r0.f[1][1]*q111[1]+ r0.f[1][2]*q111[2];
                     p111[2]= 0;
             
                     cb.loadv( i,j,  t, y,z );
             
                     vtx_t  v;
                     vtx_t  dv[2];
                     REAL_3   q[3],w;
                     su[s]->interp( z, v,dv );

                     qrf23( dv[0].x, dv[1].x, q[0], q[1], w );

                     cross3( q[0],q[1], q[2] );
                     REAL_ d= norm32( q[2] );
                     scal3( 1./d, q[2] );

                     ellb(  x001, x101, x201, x011, x111, x211, x021, x121, x221,
                            x002, x102, x202, x012, x112, x212, x022, x122, x222, 
                            fct*p111[0],fct*p111[1],fct*p111[2], f, a, q );

                     r0.transform( i,j,0, l,m,n  );

                     co[0].storev( l,m,n,c, r,f );
                     cd[0].storev( l,m,n,c, g,&a );
             
                 }
              }
           }
        }
     }

// here OPENMP loop to handle blocks
//    printf( "\n" );
//    printf( "blocks\n" );

      for( t=0;t<hx.n;t++ )
     {
//       printf( "\n" );
//       printf( "%2d\n", hx[t].id );

         for( k=1;k<bsize;k++ )
        {
            for( j=1;j<bsize;j++ )
           {
               for( i=1;i<bsize;i++ )
              {
                  co[0].loadv( i-1,j-1,k-1,t, x,x000 );
                  co[0].loadv( i  ,j-1,k-1,t, x,x100 );
                  co[0].loadv( i+1,j-1,k-1,t, x,x200 );
                                                    
                  co[0].loadv( i-1,j  ,k-1,t, x,x010 );
                  co[0].loadv( i  ,j  ,k-1,t, x,x110 );
                  co[0].loadv( i+1,j  ,k-1,t, x,x210 );
                                                    
                  co[0].loadv( i-1,j+1,k-1,t, x,x020 );
                  co[0].loadv( i  ,j+1,k-1,t, x,x120 );
                  co[0].loadv( i+1,j+1,k-1,t, x,x220 );
                                                    
                  co[0].loadv( i-1,j-1,k  ,t, x,x001 );
                  co[0].loadv( i  ,j-1,k  ,t, x,x101 );
                  co[0].loadv( i+1,j-1,k  ,t, x,x201 );
                                                    
                  co[0].loadv( i-1,j  ,k  ,t, x,x011 );
                  co[0].loadv( i  ,j  ,k  ,t, x,x111 );
                  co[0].loadv( i+1,j  ,k  ,t, x,x211 );
                                                    
                  co[0].loadv( i-1,j+1,k  ,t, x,x021 );
                  co[0].loadv( i  ,j+1,k  ,t, x,x121 );
                  co[0].loadv( i+1,j+1,k  ,t, x,x221 );
                                                    
                  co[0].loadv( i-1,j-1,k+1,t, x,x002 );
                  co[0].loadv( i  ,j-1,k+1,t, x,x102 );
                  co[0].loadv( i+1,j-1,k+1,t, x,x202 );
                                                    
                  co[0].loadv( i-1,j  ,k+1,t, x,x012 );
                  co[0].loadv( i  ,j  ,k+1,t, x,x112 );
                  co[0].loadv( i+1,j  ,k+1,t, x,x212 );
                                                    
                  co[0].loadv( i-1,j+1,k+1,t, x,x022 );
                  co[0].loadv( i  ,j+1,k+1,t, x,x122 );
                  co[0].loadv( i+1,j+1,k+1,t, x,x222 );

                  co[0].loadv( i  ,j  ,k  ,t, p,p111 );

                  ellp(  x000,x100,x200, x010,x110,x210, x020,x120,x220, 
                         x001,x101,x201, x011,x111,x211, x021,x121,x221, 
                         x002,x102,x202, x012,x112,x212, x022,x122,x222, fct*p111[0],
                                                                         fct*p111[1],
                                                                         fct*p111[2], f, a );

                  co[0].storev( i,j,k,t, r,f );
                  cd[0].storev( i,j,k,t, g,&a );

              }
           }
        }
     }
// handle interfaces with only local data dependencies

      while( transit( com[2],msg[3], ntsk,tasks, 0 ) )
     {
         for( t=0;t<ntsk;t++ )
        {
            com[2].task( tasks[t], num,loc, &idx );

            assert( num >= loc );

            f[0]=0;
            f[1]=0;
            f[2]=0;

            a=0;

            for( s=0;s<loc;s++ )
           { 
               com[2].taskinfo( co[0], idx[s], r0,c );
             
               r0.transform( 1,0,0, l,m,n ); co[0].loadv( l,m,n, c, x,x100 );
               r0.transform( 0,1,0, l,m,n ); co[0].loadv( l,m,n, c, x,x010 );
               r0.transform( 1,1,0, l,m,n ); co[0].loadv( l,m,n, c, x,x110 );
               r0.transform( 0,0,1, l,m,n ); co[0].loadv( l,m,n, c, x,x001 );
               r0.transform( 1,0,1, l,m,n ); co[0].loadv( l,m,n, c, x,x101 );
               r0.transform( 0,1,1, l,m,n ); co[0].loadv( l,m,n, c, x,x011 );
               r0.transform( 1,1,1, l,m,n ); co[0].loadv( l,m,n, c, x,x111 );

               f[0]+= x100[0]+ x010[0]+ x110[0]+ x001[0]+ x101[0]+ x011[0]+ x111[0];
               f[1]+= x100[1]+ x010[1]+ x110[1]+ x001[1]+ x101[1]+ x011[1]+ x111[1];
               f[2]+= x100[2]+ x010[2]+ x110[2]+ x001[2]+ x101[2]+ x011[2]+ x111[2];

               a+= 7;
         
           }

            for( s=loc;s<num;s++ )
           { 
               c= idx[s];
             
               co[3].loadv( 1,0,0, c, x,x100 );
               co[3].loadv( 0,1,0, c, x,x010 );
               co[3].loadv( 1,1,0, c, x,x110 );
               co[3].loadv( 0,0,1, c, x,x001 );
               co[3].loadv( 1,0,1, c, x,x101 );
               co[3].loadv( 0,1,1, c, x,x011 );
               co[3].loadv( 1,1,1, c, x,x111 );

               f[0]+= x100[0]+ x010[0]+ x110[0]+ x001[0]+ x101[0]+ x011[0]+ x111[0];
               f[1]+= x100[1]+ x010[1]+ x110[1]+ x001[1]+ x101[1]+ x011[1]+ x111[1];
               f[2]+= x100[2]+ x010[2]+ x110[2]+ x001[2]+ x101[2]+ x011[2]+ x111[2];

               a+= 7;

           }

            for( s=0;s<loc;s++ )
           { 
               com[2].taskinfo( co[0], idx[s], r0,c );
             
               r0.transform( 0,0,0,   l,m,n ); co[0].loadv( l,m,n, c, x,x000 );

               f0[0]= f[0]- 7*num*x000[0];
               f0[1]= f[1]- 7*num*x000[1];
               f0[2]= f[2]- 7*num*x000[2];
               
               f0[0]/= (7*num);
               f0[1]/= (7*num);
               f0[2]/= (7*num);

               co[0].storev( l,m,n,c, r,f0 ); 
               cd[0].storev( l,m,n,c, g,&a );

           }
        }
     }

      while( transit( com[1],msg[2], ntsk,tasks, 0 ) )
     {
         for( t=0;t<ntsk;t++ )
        {
            com[1].task( tasks[t], num,loc, &idx );
            assert( num >= loc );

            memset( wrk0,0,1024*sizeof(REAL_3) );
            memset( wrk1,0,1024*sizeof(REAL_) ); 

            INT_ e,e0,e1, i0,j0,k0, i1,j1,k1, b0,b1, s0,s1, mm0,mm1;
            INT_ j2,k2, j3,k3;
            frule_t r2,r3;
            vtx_t v0,v1;

            e= tasks[t];

            b0= be[e][0];
            b1= be[e][2];

            bool flag= false;
            flag= ( num==2 && b0 > -1 && b1 > -1 );

            if( flag )
           { 
               s0= bn.s[b0];
               s1= bn.s[b1];
               mm0= bn.r[b0];
               mm1= bn.r[b1];
               flag = ( s0 == s1 && abs(mm0-mm1)<=1 );
           }

//          flag= false;
            if( flag )
           {
               REAL_b     y0;
               REAL_b     y1;


               assert( num==loc ); // no work in parallel for now

               e0= be[e][1];
               e1= be[e][3];

               s0= bn.s[b0];
               s1= bn.s[b1];

               mm0= bn.r[b0];
               mm1= bn.r[b1];

               com[1].taskinfo( co[0], e0, r0,c );
               com[1].taskinfo( co[0], e1, r1,d );

//             bool fflag= ( ( hx[c].id == 55 ) && ( hx[d].id == 60 ) ) ||
//                         ( ( hx[c].id == 60 ) && ( hx[d].id == 55 ) ) ;
//&& ( s0 == 10 );
               bool fflag= false;

               r2= frule_t( bn.f[b0], bsize+1,bsize+1,bsize+1 );
               r3= frule_t( bn.f[b1], bsize+1,bsize+1,bsize+1 );

               j=0;
               k=0;


               r0.transform( 1,1,0, l,m,n );   r2.itransform( i0,j0,k0, l ,m ,n  );
               if( k0 == 0 ){ j2=1;k2=0; }else{ j2=0;k2=1; };

               r1.transform( 1,1,0, l,m,n );   r3.itransform( i1,j1,k1, l ,m ,n  );
               if( k1 == 0 ){ j3=1;k3=0; }else{ j3=0;k3=1; };

               for( i=1;i<bsize;i++ )
              {

                                                                                    if( fflag ){  printf( "\n" );                         }
                  r0.transform( i-1,j2,k2,l,m,n  ); co[0].loadv( l,m,n,c, x,x001 ); if( fflag ){  printf( "x001 %2d %2d %2d %2d % 9.3e % 9.3e % 9.3e \n", l,m,n,hx[c].id, x001[0],x001[1],x001[2] ); }
                  r0.transform( i  ,j2,k2,l,m,n  ); co[0].loadv( l,m,n,c, x,x101 ); if( fflag ){  printf( "x101 %2d %2d %2d %2d % 9.3e % 9.3e % 9.3e \n", l,m,n,hx[c].id, x101[0],x101[1],x101[2] ); }
                  r0.transform( i+1,j2,k2,l,m,n  ); co[0].loadv( l,m,n,c, x,x201 ); if( fflag ){  printf( "x201 %2d %2d %2d %2d % 9.3e % 9.3e % 9.3e \n", l,m,n,hx[c].id, x201[0],x201[1],x201[2] ); }
                                                                                                                
                  r0.transform( i-1, 0, 0,l,m,n  ); co[0].loadv( l,m,n,c, x,x011 ); if( fflag ){  printf( "x011 %2d %2d %2d %2d % 9.3e % 9.3e % 9.3e \n", l,m,n,hx[c].id, x011[0],x011[1],x011[2] ); }
                  r0.transform( i  , 0, 0,l,m,n  ); co[0].loadv( l,m,n,c, x,x111 ); if( fflag ){  printf( "x111 %2d %2d %2d %2d % 9.3e % 9.3e % 9.3e \n", l,m,n,hx[c].id, x111[0],x111[1],x111[2] ); }
                  r0.transform( i+1, 0, 0,l,m,n  ); co[0].loadv( l,m,n,c, x,x211 ); if( fflag ){  printf( "x211 %2d %2d %2d %2d % 9.3e % 9.3e % 9.3e \n", l,m,n,hx[c].id, x211[0],x211[1],x211[2] ); }
                                                                                                                
                  r0.transform( i-1, 1, 1,l,m,n  ); co[0].loadv( l,m,n,c, x,x002 ); if( fflag ){  printf( "x002 %2d %2d %2d %2d % 9.3e % 9.3e % 9.3e \n", l,m,n,hx[c].id, x002[0],x002[1],x002[2] ); }
                  r0.transform( i  , 1, 1,l,m,n  ); co[0].loadv( l,m,n,c, x,x102 ); if( fflag ){  printf( "x102 %2d %2d %2d %2d % 9.3e % 9.3e % 9.3e \n", l,m,n,hx[c].id, x102[0],x102[1],x102[2] ); }
                  r0.transform( i+1, 1, 1,l,m,n  ); co[0].loadv( l,m,n,c, x,x202 ); if( fflag ){  printf( "x202 %2d %2d %2d %2d % 9.3e % 9.3e % 9.3e \n", l,m,n,hx[c].id, x202[0],x202[1],x202[2] ); }
                                                                                                                
                  r0.transform( i  , 1, 1,l,m,n  ); co[0].loadv( l,m,n,c, p,q111 ); if( fflag ){  printf( "q111 %2d %2d %2d %2d % 9.3e % 9.3e % 9.3e \n", l,m,n,hx[c].id, q111[0],q111[1],q111[2] ); }                           
                                                                                                                
                  r0.transform( i-1,k2,j2,l,m,n  ); co[0].loadv( l,m,n,c, x,x012 ); if( fflag ){  printf( "x012 %2d %2d %2d %2d % 9.3e % 9.3e % 9.3e \n", l,m,n,hx[c].id, x012[0],x012[1],x012[2] ); }
                  r0.transform( i  ,k2,j2,l,m,n  ); co[0].loadv( l,m,n,c, x,x112 ); if( fflag ){  printf( "x112 %2d %2d %2d %2d % 9.3e % 9.3e % 9.3e \n", l,m,n,hx[c].id, x112[0],x112[1],x112[2] ); }
                  r0.transform( i+1,k2,j2,l,m,n  ); co[0].loadv( l,m,n,c, x,x212 ); if( fflag ){  printf( "x212 %2d %2d %2d %2d % 9.3e % 9.3e % 9.3e \n", l,m,n,hx[c].id, x212[0],x212[1],x212[2] ); }
                                                                                                                
                  r1.transform( i-1,j3,k3,l,m,n  ); co[0].loadv( l,m,n,d, x,x021 ); if( fflag ){  printf( "x021 %2d %2d %2d %2d % 9.3e % 9.3e % 9.3e \n", l,m,n,hx[d].id, x021[0],x021[1],x021[2] ); }
                  r1.transform( i  ,j3,k3,l,m,n  ); co[0].loadv( l,m,n,d, x,x121 ); if( fflag ){  printf( "x121 %2d %2d %2d %2d % 9.3e % 9.3e % 9.3e \n", l,m,n,hx[d].id, x121[0],x121[1],x121[2] ); }
                  r1.transform( i+1,j3,k3,l,m,n  ); co[0].loadv( l,m,n,d, x,x221 ); if( fflag ){  printf( "x221 %2d %2d %2d %2d % 9.3e % 9.3e % 9.3e \n", l,m,n,hx[d].id, x221[0],x221[1],x221[2] ); }
                                                                                                                
                  r1.transform( i-1, 0, 0,l,m,n  ); co[0].loadv( l,m,n,d, x,y011 ); if( fflag ){  printf( "x011 %2d %2d %2d %2d % 9.3e % 9.3e % 9.3e \n", l,m,n,hx[d].id, x011[0],x011[1],x011[2] ); }
                  r1.transform( i  , 0, 0,l,m,n  ); co[0].loadv( l,m,n,d, x,y111 ); if( fflag ){  printf( "x111 %2d %2d %2d %2d % 9.3e % 9.3e % 9.3e \n", l,m,n,hx[d].id, x111[0],x111[1],x111[2] ); }
                  r1.transform( i+1, 0, 0,l,m,n  ); co[0].loadv( l,m,n,d, x,y211 ); if( fflag ){  printf( "x211 %2d %2d %2d %2d % 9.3e % 9.3e % 9.3e \n", l,m,n,hx[d].id, x211[0],x211[1],x211[2] ); }
                                                                                                                
                  r1.transform( i-1, 1, 1,l,m,n  ); co[0].loadv( l,m,n,d, x,x022 ); if( fflag ){  printf( "x022 %2d %2d %2d %2d % 9.3e % 9.3e % 9.3e \n", l,m,n,hx[d].id, x022[0],x022[1],x022[2] ); }
                  r1.transform( i  , 1, 1,l,m,n  ); co[0].loadv( l,m,n,d, x,x122 ); if( fflag ){  printf( "x122 %2d %2d %2d %2d % 9.3e % 9.3e % 9.3e \n", l,m,n,hx[d].id, x122[0],x122[1],x122[2] ); }
                  r1.transform( i+1, 1, 1,l,m,n  ); co[0].loadv( l,m,n,d, x,x222 ); if( fflag ){  printf( "x222 %2d %2d %2d %2d % 9.3e % 9.3e % 9.3e \n", l,m,n,hx[d].id, x222[0],x222[1],x222[2] ); }
                                                                                                                
                  r1.transform( i-1,k3,j3,l,m,n  ); co[0].loadv( l,m,n,d, x,y012 ); if( fflag ){  printf( "x012 %2d %2d %2d %2d % 9.3e % 9.3e % 9.3e \n", l,m,n,hx[d].id, x012[0],x012[1],x012[2] ); }
                  r1.transform( i  ,k3,j3,l,m,n  ); co[0].loadv( l,m,n,d, x,y112 ); if( fflag ){  printf( "x112 %2d %2d %2d %2d % 9.3e % 9.3e % 9.3e \n", l,m,n,hx[d].id, x112[0],x112[1],x112[2] ); }
                  r1.transform( i+1,k3,j3,l,m,n  ); co[0].loadv( l,m,n,d, x,y212 ); if( fflag ){  printf( "x212 %2d %2d %2d %2d % 9.3e % 9.3e % 9.3e \n", l,m,n,hx[d].id, x212[0],x212[1],x212[2] ); }

                  r1.transform( i  , 1, 1,l,m,n  ); co[0].loadv( l,m,n,d, p,r111 ); if( fflag ){  printf( "r111 %2d %2d %2d %2d % 9.3e % 9.3e % 9.3e \n", l,m,n,hx[d].id, r111[0],r111[1],r111[2] ); }                           

                  x011[0]+= y011[0]; x011[0]*= 0.5;
                  x011[1]+= y011[1]; x011[1]*= 0.5;
                  x011[2]+= y011[2]; x011[2]*= 0.5;

                  x111[0]+= y111[0]; x111[0]*= 0.5;
                  x111[1]+= y111[1]; x111[1]*= 0.5;
                  x111[2]+= y111[2]; x111[2]*= 0.5;

                  x211[0]+= y211[0]; x211[0]*= 0.5;
                  x211[1]+= y211[1]; x211[1]*= 0.5;
                  x211[2]+= y211[2]; x211[2]*= 0.5;

                  x012[0]+= y012[0]; x012[0]*= 0.5;
                  x012[1]+= y012[1]; x012[1]*= 0.5;
                  x012[2]+= y012[2]; x012[2]*= 0.5;

                  x112[0]+= y112[0]; x112[0]*= 0.5;
                  x112[1]+= y112[1]; x112[1]*= 0.5;
                  x112[2]+= y112[2]; x112[2]*= 0.5;

                  x212[0]+= y212[0]; x212[0]*= 0.5;
                  x212[1]+= y212[1]; x212[1]*= 0.5;
                  x212[2]+= y212[2]; x212[2]*= 0.5;
                                                                                 
                  r0.transform( i,  j,  k,   l ,m ,n  ); r2.itransform( i0,j0,k0, l ,m ,n  ); assert( k0==0 );
                  r1.transform( i,  j,  k,   l ,m ,n  ); r3.itransform( i1,j1,k1, l ,m ,n  ); assert( k1==0 );

                  cb.loadv( i0,j0,  b0,  y, y0 );
                  cb.loadv( i1,j1,  b1,  y, y1 );

                  y0[0]+= y1[0];
                  y0[1]+= y1[1];

                  y0[0]*= 0.5;
                  y0[1]*= 0.5;

                  vtx_t  v;
                  vtx_t  dv[2];
                  REAL_3   q[3],w;
                  REAL_    ww;

                  su[s0]->interp( y0, v,dv );
                  qrf23( dv[0].x, dv[1].x, q[0], q[1], w );

                  cross3( q[0],q[1], q[2] );
                  ww= norm32( q[2] );
                  scal3( 1./ww, q[2] );

          
                  p111[0]=  r0.f[0][0]*q111[0]+ r0.f[0][1]*q111[1]+ r0.f[0][2]*q111[2];
//                p111[1]=  r0.f[1][0]*q111[0]+ r0.f[1][1]*q111[1]+ r0.f[1][2]*q111[2];

                  p111[0]+= r1.f[0][0]*r111[0]+ r1.f[0][1]*r111[1]+ r1.f[0][2]*r111[2];
//                p111[1]+= r1.f[1][0]*r111[0]+ r1.f[1][1]*r111[1]+ r1.f[1][2]*r111[2];

                  p111[0]*= 0.5;
                  p111[1]=  0;
                  p111[2]=  0;

                                                                                    if( fflag ){  printf( "p111                 % 9.3e % 9.3e % 9.3e \n",                 p111[0],p111[1],p111[2] ); }                           

                  ellb(  x001, x101, x201, x011, x111, x211, x021, x121, x221, 
                         x002, x102, x202, x012, x112, x212, x022, x122, x222, fct*p111[0],fct*p111[1],fct*p111[2],f, a, q );
          
                  r0.transform( i,j,k, l,m,n  ); 
                  assert( l >= 0 && l < bsize+1 );
                  assert( m >= 0 && m < bsize+1 );
                  assert( n >= 0 && n < bsize+1 );

                  co[0].storev( l,m,n,c, r,f );
                  cd[0].storev( l,m,n,c, g,&a );

                  r1.transform( i,j,k, l,m,n  ); 
                  assert( l >= 0 && l < bsize+1 );
                  assert( m >= 0 && m < bsize+1 );
                  assert( n >= 0 && n < bsize+1 );

                  co[0].storev( l,m,n,d, r,f );
                  cd[0].storev( l,m,n,d, g,&a );
              }


           }
            else
           {

               for( s=0;s<loc;s++ )
              { 
                  com[1].taskinfo( co[0], idx[s], r0,c );
             
                  d= idx[s];
                
                  for( i=1;i<bsize;i++ )
                 {
/*                   r0.transform( i+1,0,0, l,m,n ); co[0].loadv( l,m,n, c, x,x200 );
                     r0.transform( i-1,0,0, l,m,n ); co[0].loadv( l,m,n, c, x,x000 );*/
             
                     r0.transform( i,  0,1, l,m,n ); co[0].loadv( l,m,n, c, x,x101 );
                     r0.transform( i,  1,0, l,m,n ); co[0].loadv( l,m,n, c, x,x110 );
                     r0.transform( i,  1,1, l,m,n ); co[0].loadv( l,m,n, c, x,x111 );

                     wrk0[i][0]+= x111[0]+ x110[0]+ x101[0]; 
                     wrk0[i][1]+= x111[1]+ x110[1]+ x101[1]; 
                     wrk0[i][2]+= x111[2]+ x110[2]+ x101[2]; 

                     wrk1[i]+= 3;
             
                 }
              }
             
               for( s=loc;s<num;s++ )
              { 
                  c= idx[s];
                
                  com[1].taskinfo( co[0], idx[s], r0,b );
             
                  for( i=1;i<bsize;i++ )
                 {
/*                    co[2].loadv( i+1,0,0, c, x,x200 );
                     co[2].loadv( i-1,0,0, c, x,x000 );*/

                     co[2].loadv(   i,0,1, c, x,x101 );
                     co[2].loadv(   i,1,0, c, x,x110 );
                     co[2].loadv(   i,1,1, c, x,x111 );

                     wrk0[i][0]+= x111[0]+ x110[0]+ x101[0]; 
                     wrk0[i][1]+= x111[1]+ x110[1]+ x101[1]; 
                     wrk0[i][2]+= x111[2]+ x110[2]+ x101[2]; 

                     wrk1[i]+= 3;
             
                 }
              }
             
               for( s=0;s<loc;s++ )
              { 
                  com[1].taskinfo( co[0], idx[s], r0,c );
             
                  d= idx[s];
                
                  for( i=1;i<bsize;i++ )
                 {
             
                     r0.transform( i,0,0, l,m,n ); co[0].loadv( l,m,n, c, x,x000 );
             
                     f[0]= wrk0[i][0]- 3*num*x000[0];
                     f[1]= wrk0[i][1]- 3*num*x000[1];
                     f[2]= wrk0[i][2]- 3*num*x000[2];

                     f[0]/= (3*num);
                     f[1]/= (3*num);
                     f[2]/= (3*num);
             
                     co[0].storev( l,m,n,c, r, f ); 
                     cd[0].storev( l,m,n,c, g, wrk1+i  );
             
                 }
              }
           }
        }
     }

      REAL_ mdif=-1;
      while( locals( com[0],msg[1], ntsk,tasks ) )
     {
         for( t=0;t<ntsk;t++ )
        {
    
            com[0].task( tasks[t], num,loc, &idx );
            com[0].taskinfo( co[0], idx[0], r0,c );
            com[0].taskinfo( co[0], idx[1], r1,d );

            assert( num == 2 );
            assert( loc == 2 );

            for( j=1;j<bsize;j++ )
           {
               for( i=1;i<bsize;i++ )
              {

                  r1.transform( i-1,j-1,1, l,m,n );  co[0].loadv( l,m,n,d, x,x000 );
                  r1.transform( i  ,j-1,1, l,m,n );  co[0].loadv( l,m,n,d, x,x100 );
                  r1.transform( i+1,j-1,1, l,m,n );  co[0].loadv( l,m,n,d, x,x200 );

                  r1.transform( i-1,j  ,1, l,m,n );  co[0].loadv( l,m,n,d, x,x010 );
                  r1.transform( i  ,j  ,1, l,m,n );  co[0].loadv( l,m,n,d, x,x110 );
                  r1.transform( i+1,j  ,1, l,m,n );  co[0].loadv( l,m,n,d, x,x210 );

                  r1.transform( i-1,j+1,1, l,m,n );  co[0].loadv( l,m,n,d, x,x020 );
                  r1.transform( i  ,j+1,1, l,m,n );  co[0].loadv( l,m,n,d, x,x120 );
                  r1.transform( i+1,j+1,1, l,m,n );  co[0].loadv( l,m,n,d, x,x220 );

                  r1.transform( i  ,j  ,1, l,m,n );  co[0].loadv( l,m,n,d, p,r111 );

                  r1.transform( i-1,j-1,0, l,m,n );  co[0].loadv( l,m,n,d, x,y001 );
                  r1.transform( i  ,j-1,0, l,m,n );  co[0].loadv( l,m,n,d, x,y101 );
                  r1.transform( i+1,j-1,0, l,m,n );  co[0].loadv( l,m,n,d, x,y201 );

                  r1.transform( i-1,j  ,0, l,m,n );  co[0].loadv( l,m,n,d, x,y011 );
                  r1.transform( i  ,j  ,0, l,m,n );  co[0].loadv( l,m,n,d, x,y111 );


                  r1.transform( i+1,j  ,0, l,m,n );  co[0].loadv( l,m,n,d, x,y211 );

                  r1.transform( i-1,j+1,0, l,m,n );  co[0].loadv( l,m,n,d, x,y021 );
                  r1.transform( i  ,j+1,0, l,m,n );  co[0].loadv( l,m,n,d, x,y121 );
                  r1.transform( i+1,j+1,0, l,m,n );  co[0].loadv( l,m,n,d, x,y221 );
                                                                                 
                  r0.transform( i-1,j-1,0, l,m,n  ); co[0].loadv( l,m,n,c, x,x001 );
                  r0.transform( i  ,j-1,0, l,m,n  ); co[0].loadv( l,m,n,c, x,x101 );
                  r0.transform( i+1,j-1,0, l,m,n  ); co[0].loadv( l,m,n,c, x,x201 );

                  r0.transform( i-1,j  ,0, l,m,n  ); co[0].loadv( l,m,n,c, x,x011 );
                  r0.transform( i  ,j  ,0, l,m,n  ); co[0].loadv( l,m,n,c, x,x111 );
                  r0.transform( i+1,j  ,0, l,m,n  ); co[0].loadv( l,m,n,c, x,x211 );


                  r0.transform( i-1,j+1,0, l,m,n  ); co[0].loadv( l,m,n,c, x,x021 );
                  r0.transform( i  ,j+1,0, l,m,n  ); co[0].loadv( l,m,n,c, x,x121 );
                  r0.transform( i+1,j+1,0, l,m,n  ); co[0].loadv( l,m,n,c, x,x221 );

                  r0.transform( i-1,j-1,1, l,m,n  ); co[0].loadv( l,m,n,c, x,x002 );
                  r0.transform( i  ,j-1,1, l,m,n  ); co[0].loadv( l,m,n,c, x,x102 );
                  r0.transform( i+1,j-1,1, l,m,n  ); co[0].loadv( l,m,n,c, x,x202 );

                  r0.transform( i-1,j  ,1, l,m,n  ); co[0].loadv( l,m,n,c, x,x012 );
                  r0.transform( i  ,j  ,1, l,m,n  ); co[0].loadv( l,m,n,c, x,x112 );
                  r0.transform( i+1,j  ,1, l,m,n  ); co[0].loadv( l,m,n,c, x,x212 );

                  r0.transform( i  ,j  ,1, l,m,n  ); co[0].loadv( l,m,n,c, p,s111 );

                  r0.transform( i-1,j+1,1, l,m,n  ); co[0].loadv( l,m,n,c, x,x022 );
                  r0.transform( i  ,j+1,1, l,m,n  ); co[0].loadv( l,m,n,c, x,x122 );
                  r0.transform( i+1,j+1,1, l,m,n  ); co[0].loadv( l,m,n,c, x,x222 );

                  REAL_ dif= fabs(y001[0]-x001[0])+ fabs(y001[1]-x001[1])+ fabs(y001[2]-x001[2])+
                             fabs(y101[0]-x101[0])+ fabs(y101[1]-x101[1])+ fabs(y101[2]-x101[2])+
                             fabs(y201[0]-x201[0])+ fabs(y201[1]-x201[1])+ fabs(y201[2]-x201[2])+
                             fabs(y011[0]-x011[0])+ fabs(y011[1]-x011[1])+ fabs(y011[2]-x011[2])+
                             fabs(y111[0]-x111[0])+ fabs(y111[1]-x111[1])+ fabs(y111[2]-x111[2])+
                             fabs(y211[0]-x211[0])+ fabs(y211[1]-x211[1])+ fabs(y211[2]-x211[2])+
                             fabs(y021[0]-x021[0])+ fabs(y021[1]-x021[1])+ fabs(y021[2]-x021[2])+
                             fabs(y121[0]-x121[0])+ fabs(y121[1]-x121[1])+ fabs(y121[2]-x121[2])+
                             fabs(y221[0]-x221[0])+ fabs(y221[1]-x221[1])+ fabs(y221[2]-x221[2]);

                  mdif= fmax( dif,mdif );
//                assert( dif < 1.e-12 );

                  x001[0]+= y001[0]; x001[0]*= 0.5;
                  x001[1]+= y001[1]; x001[1]*= 0.5;
                  x001[2]+= y001[2]; x001[2]*= 0.5;

                  x101[0]+= y101[0]; x101[0]*= 0.5;
                  x101[1]+= y101[1]; x101[1]*= 0.5;
                  x101[2]+= y101[2]; x101[2]*= 0.5;

                  x201[0]+= y201[0]; x201[0]*= 0.5;
                  x201[1]+= y201[1]; x201[1]*= 0.5;
                  x201[2]+= y201[2]; x201[2]*= 0.5;
                                  
                  x011[0]+= y011[0]; x011[0]*= 0.5;
                  x011[1]+= y011[1]; x011[1]*= 0.5;
                  x011[2]+= y011[2]; x011[2]*= 0.5;

                  z111[0]=  x111[0]; 
                  z111[1]=  x111[1]; 
                  z111[2]=  x111[2]; 

                  z111[0]+= y111[0]; z111[0]*= 0.5;
                  z111[1]+= y111[1]; z111[1]*= 0.5;
                  z111[2]+= y111[2]; z111[2]*= 0.5;

                  x211[0]+= y211[0]; x211[0]*= 0.5;
                  x211[1]+= y211[1]; x211[1]*= 0.5;
                  x211[2]+= y211[2]; x211[2]*= 0.5;
                                  
                  x021[0]+= y021[0]; x021[0]*= 0.5;
                  x021[1]+= y021[1]; x021[1]*= 0.5;
                  x021[2]+= y021[2]; x021[2]*= 0.5;

                  x121[0]+= y121[0]; x121[0]*= 0.5;
                  x121[1]+= y121[1]; x121[1]*= 0.5;
                  x121[2]+= y121[2]; x121[2]*= 0.5;

                  x221[0]+= y221[0]; x221[0]*= 0.5;
                  x221[1]+= y221[1]; x221[1]*= 0.5;
                  x221[2]+= y221[2]; x221[2]*= 0.5;


                  p111[0]= r0.f[0][0]*s111[0]+ r0.f[0][1]*s111[1]+ r0.f[0][2]*s111[2];
                  p111[1]= r0.f[1][0]*s111[0]+ r0.f[1][1]*s111[1]+ r0.f[1][2]*s111[2];
//                p111[2]= r0.f[2][0]*s111[0]+ r0.f[2][1]*s111[1]+ r0.f[2][2]*s111[2];

                  q111[0]= r1.f[0][0]*r111[0]+ r1.f[0][1]*r111[1]+ r1.f[0][2]*r111[2];
                  q111[1]= r1.f[1][0]*r111[0]+ r1.f[1][1]*r111[1]+ r1.f[1][2]*r111[2];
//                q111[2]= r1.f[2][0]*r111[0]+ r1.f[2][1]*r111[1]+ r1.f[2][2]*r111[2];

//                assert( ( fabs( p111[0]-q111[0] )+ fabs( p111[1]-q111[1] ) ) < 1.e-12 );
//                p111[2]-= q111[2];
//                p111[2]/= 2;

                  p111[0]+= q111[0];
                  p111[1]+= q111[1];

                  p111[0]*= 0.5;
                  p111[1]*= 0.5;
                  p111[2]= 0;


                                                                                 
                  ellp( x000,x100,x200, x010,x110,x210, x020,x120,x220, 
                        x001,x101,x201, x011,z111,x211, x021,x121,x221, 
                        x002,x102,x202, x012,x112,x212, x022,x122,x222, fct*p111[0],fct*p111[1],fct*p111[2], f,a );

                  x111[0]= f[0]+ z111[0]- x111[0];
                  x111[1]= f[1]+ z111[1]- x111[1];
                  x111[2]= f[2]+ z111[2]- x111[2];

                  r0.transform( i,j,0, l,m,n  ); co[0].storev( l,m,n,c, r,x111 );
                                                 cd[0].storev( l,m,n,c, g,&a );

                  y111[0]= f[0]+ z111[0]- y111[0];
                  y111[1]= f[1]+ z111[1]- y111[1];
                  y111[2]= f[2]+ z111[2]- y111[2];

                  r1.transform( i,j,0, l,m,n  ); co[0].storev( l,m,n,d, r,y111 );
                                                 cd[0].storev( l,m,n,d, g,&a );

              }
           }
        }
     }

// handle interfaces with data dependencies on remote ranks

      while( transit( com[0],msg[1], ntsk,tasks, 1 ) )
     {
         for( t=0;t<ntsk;t++ )
        {
    
            com[0].task( tasks[t], num,loc, &idx );
            com[0].taskinfo( co[0], idx[0], r0,c );
            b= idx[1];
    
            assert( num == 2 );
            assert( loc == 1 );
    
            for( j=1;j<bsize;j++ )
           {
               for( i=1;i<bsize;i++ )
              {
                  co[1].loadv( i-1,j-1,1,b, x,x000 );
                  co[1].loadv( i  ,j-1,1,b, x,x100 );
                  co[1].loadv( i+1,j-1,1,b, x,x200 );

                  co[1].loadv( i-1,j  ,1,b, x,x010 );
                  co[1].loadv( i  ,j  ,1,b, x,x110 );
                  co[1].loadv( i+1,j  ,1,b, x,x210 );

                  co[1].loadv( i-1,j+1,1,b, x,x020 );
                  co[1].loadv( i  ,j+1,1,b, x,x120 );
                  co[1].loadv( i+1,j+1,1,b, x,x220 );

                  r0.transform( i-1,j-1,0, l,m,n  ); co[0].loadv( l,m,n,c, x,x001 );
                  r0.transform( i  ,j-1,0, l,m,n  ); co[0].loadv( l,m,n,c, x,x101 );
                  r0.transform( i+1,j-1,0, l,m,n  ); co[0].loadv( l,m,n,c, x,x201 );

                  r0.transform( i-1,j  ,0, l,m,n  ); co[0].loadv( l,m,n,c, x,x011 );
                  r0.transform( i  ,j  ,0, l,m,n  ); co[0].loadv( l,m,n,c, x,x111 );
                  r0.transform( i+1,j  ,0, l,m,n  ); co[0].loadv( l,m,n,c, x,x211 );

                  r0.transform( i-1,j+1,0, l,m,n  ); co[0].loadv( l,m,n,c, x,x021 );
                  r0.transform( i  ,j+1,0, l,m,n  ); co[0].loadv( l,m,n,c, x,x121 );
                  r0.transform( i+1,j+1,0, l,m,n  ); co[0].loadv( l,m,n,c, x,x221 );
                                                                                 
                  r0.transform( i-1,j-1,1, l,m,n  ); co[0].loadv( l,m,n,c, x,x002 );
                  r0.transform( i  ,j-1,1, l,m,n  ); co[0].loadv( l,m,n,c, x,x102 );
                  r0.transform( i+1,j-1,1, l,m,n  ); co[0].loadv( l,m,n,c, x,x202 );

                  r0.transform( i-1,j  ,1, l,m,n  ); co[0].loadv( l,m,n,c, x,x012 );
                  r0.transform( i  ,j  ,1, l,m,n  ); co[0].loadv( l,m,n,c, x,x112 );
                  r0.transform( i+1,j  ,1, l,m,n  ); co[0].loadv( l,m,n,c, x,x212 );

                  r0.transform( i-1,j+1,1, l,m,n  ); co[0].loadv( l,m,n,c, x,x022 );
                  r0.transform( i  ,j+1,1, l,m,n  ); co[0].loadv( l,m,n,c, x,x122 );
                  r0.transform( i+1,j+1,1, l,m,n  ); co[0].loadv( l,m,n,c, x,x222 );

                  ellp(  x000,x100,x200, x010,x110,x210, x020,x120,x220, 
                         x001,x101,x201, x011,x111,x211, x021,x121,x221, 
                         x002,x102,x202, x012,x112,x212, x022,x122,x222, 0,0,0, f, a );

                  r0.transform( i,j,0, l,m,n  ); co[0].storev( l,m,n,c, r,f );
                                                 cd[0].storev( l,m,n,c, g,&a );

              }
           }
        }
     }

      return;
  }


