
#  include <q3x/q3x.h>


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::refn( INT_  n )
  {
      MMAP_      co0,co1;
      MMAP_      cb0,cb1;
      INT_         i,j,k,l,m;
      INT_         p,q,r;
      REAL_       *x0;
      REAL_       *p0;
      REAL_       *x1;
      REAL_       *p1;
      REAL_       *y0;
      REAL_       *y1;

      REAL_        xi[3];

      REAL_x       xm[8];
      REAL_x       pm[8];
      REAL_b       ym[4];

      REAL_x       xn;
      REAL_x       pn;
      REAL_b       yn;
      REAL_        w[8];

      size_t len=0,dlen;
      size_t len0,len1;

      MPI_File fh0, fh1;
      MPI_Status status;
      MPI_Offset off0, off1, off2;
      MPI_Offset offlen0,offlen1;


      MPI_File_open(MPI_COMM_WORLD, "grid/h", MPI_MODE_RDONLY, MPI_INFO_NULL, &fh0); assert( fh0 );

      MPI_File_open(MPI_COMM_WORLD, "grid_refn/h", MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh1);
      assert( fh1 );

      off0= 0; 
      MPI_File_read_at(fh0, off0,    &m, 1, INT_MPI, &status); off0+= sizeof( INT_ );
      MPI_File_read_at(fh0, off0, &dlen, 1,LONG_MPI, &status); off0+= sizeof(size_t );

      bsize=m;

      len0=0; co0.dims( len0, m+1,  XDIM,m+1,  m+1,  -1 ); assert( len0== dlen );
      len1=0; co1.dims( len1, m*n+1,XDIM,m*n+1,m*n+1,-1 );

      off2= 0; 
      l= m*n;
      MPI_File_write_at(fh1, off2,    &l, 1, INT_MPI, &status); off2+= sizeof( INT_ );
      MPI_File_write_at(fh1, off2, &len1, 1,LONG_MPI, &status); off2+= sizeof(size_t );

      assert( off2 == off0 );

      co0.malloc( &x0 );
      co0.malloc( &p0 );

      co1.malloc( &x1 );
      co1.malloc( &p1 );

      offlen0= 2*len0*sizeof(REAL_);
      offlen1= 2*len1*sizeof(REAL_);

      for( l=0;l<hx.n;l++ )
     {

         off0= off2+ offlen0*hx[l].id;
         off1= off2+ offlen1*hx[l].id;

         MPI_File_read_at(fh0, off0, x0, len, REAL_MPI, &status); off0+= len0*sizeof(REAL_);
         MPI_File_read_at(fh0, off0, p0, len, REAL_MPI, &status); 

         co1.memset( x1,0. );
         co1.memset( p1,0. );

         for( k=0; k<m;k++ )
        {
            for( j=0; j<m;j++ )
           {
               for( i=0; i<m;i++ )
              {

                  co0.loadv( i,  j,  k,   x0, xm[0]);      
                  co0.loadv( i+1,j,  k,   x0, xm[1]);      
                  co0.loadv( i,  j+1,k,   x0, xm[2]);      
                  co0.loadv( i+1,j+1,k,   x0, xm[3]);      
                  co0.loadv( i,  j,  k+1, x0, xm[4]);      
                  co0.loadv( i+1,j,  k+1, x0, xm[5]);      
                  co0.loadv( i,  j+1,k+1, x0, xm[6]);      
                  co0.loadv( i+1,j+1,k+1, x0, xm[7]);      

                  co0.loadv( i,  j,  k,   p0, pm[0]);      
                  co0.loadv( i+1,j,  k,   p0, pm[1]);      
                  co0.loadv( i,  j+1,k,   p0, pm[2]);      
                  co0.loadv( i+1,j+1,k,   p0, pm[3]);      
                  co0.loadv( i,  j,  k+1, p0, pm[4]);      
                  co0.loadv( i+1,j,  k+1, p0, pm[5]);      
                  co0.loadv( i,  j+1,k+1, p0, pm[6]);      
                  co0.loadv( i+1,j+1,k+1, p0, pm[7]);      


                  for( r=0;r<n+1;r++ )
                 {
                     for( q=0;q<n+1;q++ )
                    {
                        for( p=0;p<n+1;p++ )
                       {
                           xi[0]= ((REAL_)p)/((REAL_)n);
                           xi[1]= ((REAL_)q)/((REAL_)n);
                           xi[2]= ((REAL_)r)/((REAL_)n);

                           w[0]= (1-xi[0])*(1-xi[1])*(1-xi[2]);
                           w[1]=    xi[0] *(1-xi[1])*(1-xi[2]);
                           w[2]= (1-xi[0])*   xi[1] *(1-xi[2]);
                           w[3]=    xi[0] *   xi[1] *(1-xi[2]);
                           w[4]= (1-xi[0])*(1-xi[1])*   xi[2] ;
                           w[5]=    xi[0] *(1-xi[1])*   xi[2] ;
                           w[6]= (1-xi[0])*   xi[1] *   xi[2] ;
                           w[7]=    xi[0] *   xi[1] *   xi[2] ;

                           xn[0]= w[0]*xm[0][0]+ w[1]*xm[1][0]+ w[2]*xm[2][0]+ w[3]*xm[3][0]+ 
                                  w[4]*xm[4][0]+ w[5]*xm[5][0]+ w[6]*xm[6][0]+ w[7]*xm[7][0];
                           xn[1]= w[0]*xm[0][1]+ w[1]*xm[1][1]+ w[2]*xm[2][1]+ w[3]*xm[3][1]+ 
                                  w[4]*xm[4][1]+ w[5]*xm[5][1]+ w[6]*xm[6][1]+ w[7]*xm[7][1];
                           xn[2]= w[0]*xm[0][2]+ w[1]*xm[1][2]+ w[2]*xm[2][2]+ w[3]*xm[3][2]+ 
                                  w[4]*xm[4][2]+ w[5]*xm[5][2]+ w[6]*xm[6][2]+ w[7]*xm[7][2];

                           pn[0]= w[0]*pm[0][0]+ w[1]*pm[1][0]+ w[2]*pm[2][0]+ w[3]*pm[3][0]+ 
                                  w[4]*pm[4][0]+ w[5]*pm[5][0]+ w[6]*pm[6][0]+ w[7]*pm[7][0];
                           pn[1]= w[0]*pm[0][1]+ w[1]*pm[1][1]+ w[2]*pm[2][1]+ w[3]*pm[3][1]+ 
                                  w[4]*pm[4][1]+ w[5]*pm[5][1]+ w[6]*pm[6][1]+ w[7]*pm[7][1];
                           pn[2]= w[0]*pm[0][2]+ w[1]*pm[1][2]+ w[2]*pm[2][2]+ w[3]*pm[3][2]+ 
                                  w[4]*pm[4][2]+ w[5]*pm[5][2]+ w[6]*pm[6][2]+ w[7]*pm[7][2];

                           co1.storev( p+n*i,q+n*j,r+n*k, x1, xn );
                           co1.storev( p+n*i,q+n*j,r+n*k, p1, pn );
                       }
                    }
                 }
          
            

              }
           }

        }

          MPI_File_write_at(fh1, off1, x1, len1, REAL_MPI, &status); off1+= len1*sizeof(REAL_);
          MPI_File_write_at(fh1, off1, p1, len1, REAL_MPI, &status);

     }

      MPI_Barrier( MPI_COMM_WORLD );
      MPI_File_close( &fh0 );
      MPI_File_close( &fh1 );

      delete[] x0; x0= NULL;
      delete[] x1; x1= NULL;
      delete[] p0; p0= NULL;
      delete[] p1; p1= NULL;

      MPI_File_open(MPI_COMM_WORLD, "grid/b", MPI_MODE_RDONLY, MPI_INFO_NULL, &fh0); assert( fh0 );

      MPI_File_open(MPI_COMM_WORLD, "grid_refn/b", MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh1);

      off0= 0; 
      MPI_File_read_at(fh0, off0,    &m, 1, INT_MPI, &status); off0+= sizeof( INT_ );
      MPI_File_read_at(fh0, off0, &dlen, 1,LONG_MPI, &status); off0+= sizeof(size_t );

      assert( m == bsize );

      len0=0; cb0.dims( len0, m+1,  BDIM,m+1,   -1 ); assert( len0== dlen );
      len1=0; cb1.dims( len1, m*n+1,BDIM,m*n+1, -1 );

      off1= 0; 
      l= m*n;
      MPI_File_write_at(fh1, off1,    &l, 1, INT_MPI, &status); off1+= sizeof( INT_ );
      MPI_File_write_at(fh1, off1, &len1, 1,LONG_MPI, &status); off1+= sizeof(size_t );

      assert( off1==off0 ); 

      cb0.malloc( &y0 );
      cb1.malloc( &y1 );

      offlen0= len0*sizeof(REAL_);
      offlen1= len1*sizeof(REAL_);

      for( INT_ p=0;p<BTYPES;p++ )
     {
         for( INT_ g=0;g<nb;g++ )
        {
            if( bz[g].lbl[0] == p && bz[g].bo.n > 0 )
           { 
               for( k=0;k<bz[g].bo.n;k++ )
              {

                  MPI_File_read_at(fh0, off0, y0, len, REAL_MPI, &status); off0+= offlen0;
  

                  for( j=0; j<m;j++ )
                 {
                     for( i=0; i<m;i++ )
                    {

                        cb0.loadv( i,  j,   y0, ym[0]);
                        cb0.loadv( i+1,j,   y0, ym[1]);
                        cb0.loadv( i,  j+1, y0, ym[2]);
                        cb0.loadv( i+1,j+1, y0, ym[3]);


                        for( q=0;q<n+1;q++ )
                       {
                           for( p=0;p<n+1;p++ )
                          {

                              xi[0]= ((REAL_)p)/((REAL_)n);
                              xi[1]= ((REAL_)q)/((REAL_)n);
                             
                              w[0]= (1-xi[0])*(1-xi[1]);
                              w[1]=    xi[0] *(1-xi[1]);
                              w[2]= (1-xi[0])*   xi[1] ;
                              w[3]=    xi[0] *   xi[1] ;
                             
                              yn[0]= w[0]*ym[0][0]+ w[1]*ym[1][0]+ w[2]*ym[2][0]+ w[3]*ym[3][0];
                              yn[1]= w[0]*ym[0][1]+ w[1]*ym[1][1]+ w[2]*ym[2][1]+ w[3]*ym[3][1];
                              yn[2]= w[0]*ym[0][2]+ w[1]*ym[1][2]+ w[2]*ym[2][2]+ w[3]*ym[3][2];

                              cb1.storev( p+n*i,q+n*j, y1, yn );
                          }
                       }

                    }
                 }

                  MPI_File_write_at(fh1, off1, y1, len1, REAL_MPI, &status); off1+= offlen1;
              }
           } 
        }
     }
      MPI_Barrier( MPI_COMM_WORLD );
      MPI_File_close( &fh0 );
      MPI_File_close( &fh1 );


      MPI_File_open(MPI_COMM_WORLD, "grid/i", MPI_MODE_RDONLY, MPI_INFO_NULL, &fh0); assert( fh0 );

      MPI_File_open(MPI_COMM_WORLD, "grid_refn/i", MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh1);

      off0= 0; 
      MPI_File_read_at(fh0, off0,    &m, 1, INT_MPI, &status); off0+= sizeof( INT_ );
      MPI_File_read_at(fh0, off0, &dlen, 1,LONG_MPI, &status); off0+= sizeof(size_t );

      assert( m == bsize );
      assert( dlen==len0 );

      off1= 0; 
      l= m*n;
      MPI_File_write_at(fh1, off1,    &l, 1, INT_MPI, &status); off1+= sizeof( INT_ );
      MPI_File_write_at(fh1, off1, &len1, 1,LONG_MPI, &status); off1+= sizeof(size_t );

      assert( off1==off0 ); 

      for( INT_ g=0;g<bi.n;g++ )
     {
 
         for( k=0;k<bi[g].bo.n;k++ )
        {

            MPI_File_read_at(fh0, off0, y0, len, REAL_MPI, &status); off0+= offlen0;

            for( j=0; j<m;j++ )
           {
               for( i=0; i<m;i++ )
              {

                  cb0.loadv( i,  j,   y0, ym[0]);
                  cb0.loadv( i+1,j,   y0, ym[1]);
                  cb0.loadv( i,  j+1, y0, ym[2]);
                  cb0.loadv( i+1,j+1, y0, ym[3]);

                  for( q=0;q<n+1;q++ )
                 {
                     for( p=0;p<n+1;p++ )
                    {

                        xi[0]= ((REAL_)p)/((REAL_)n);
                        xi[1]= ((REAL_)q)/((REAL_)n);
                       
                        w[0]= (1-xi[0])*(1-xi[1]);
                        w[1]=    xi[0] *(1-xi[1]);
                        w[2]= (1-xi[0])*   xi[1] ;
                        w[3]=    xi[0] *   xi[1] ;
                       
                        yn[0]= w[0]*ym[0][0]+ w[1]*ym[1][0]+ w[2]*ym[2][0]+ w[3]*ym[3][0];
                        yn[1]= w[0]*ym[0][1]+ w[1]*ym[1][1]+ w[2]*ym[2][1]+ w[3]*ym[3][1];
                        yn[2]= w[0]*ym[0][2]+ w[1]*ym[1][2]+ w[2]*ym[2][2]+ w[3]*ym[3][2];

                        cb1.storev( p+n*i,q+n*j, y1, yn );
                    }
                 }

              }
           }

            MPI_File_write_at(fh1, off1, y1, len1, REAL_MPI, &status); off1+= offlen1;
        }
     }

      MPI_Barrier( MPI_COMM_WORLD );
      MPI_File_close( &fh0 );
      MPI_File_close( &fh1 );

      delete[] y0; y0= NULL;
      delete[] y1; y1= NULL;

  }
