
#  include <q3x/q3x.h>


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::stretch( REAL_ a, REAL_ b, INT_ n )
  {
      MMAP_      coo;
      MMAP_      cob;
      INT_         i,j,k,l;
      INT_         i2;
      REAL_       *x;

      REAL_        tol=1.e-9;

         FILE *f=NULL;
         char  name[512];

      size_t len=0;
      coo.dims( len, n+1,3,n+1,n+1, -1 );

      x= new REAL_[len];

      for( l=0;l<hx.n;l++ )
     {
         memset( x,0,len*sizeof(REAL_) );

         sprintf( name, "grid/%06d", hx[l].id );
         f= fopen( name, "r" );
         assert(f);
         INT_ nn;
       ::fread( &nn,       1,sizeof(INT_), f );
         assert( nn == n );
       ::fread( &len,      1,sizeof(size_t), f );
       ::fread(  x,len,sizeof(REAL_),f );
         fclose( f );

         for( k=0; k<n+1;k++ )
        {
            for( j=0; j<n+1;j++ )
           {
               for( i=0; i<n+1;i++ )
              {
                  i2= coo.addr(2,i,j,k);

                  REAL_ z=  x[i2];
          
                  if( z > a+tol && z < b-tol )
                 {
                     z= (z-a)/(b-a);

                     z*= -M_PI;
                     z= cos(z); 

                     z+=1;
                     z*=0.5;

                     z*= (b-a);
                     z+= a;
 
                     x[i2]= z;
                 }
                  

              }
           }
        }

         sprintf( name, "grid_str/%06d", hx[l].id );
         f= fopen( name, "w" );
         assert(f);
       ::fwrite( &n,        1,sizeof(INT_), f );
       ::fwrite( &len,      1,sizeof(size_t), f );
       ::fwrite(  x,len,sizeof(REAL_),f );
         fclose( f );

     }

      delete[] x; x= NULL;

  }
