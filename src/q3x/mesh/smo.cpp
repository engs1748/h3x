#  include <q3x/q3x.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::smo( vsurf_t &su )
  {

      REAL_          *x=NULL, *d=NULL, *r=NULL, *p=NULL;
      REAL_          *y=NULL, *dy=NULL;
      REAL_          *yi=NULL,*dyi=NULL;
      INT_           it;
      INT_           g,j,b;
      INT_           ien,ist;

      double         stime,etime;

                      REAL_          rlx=0.5;
                      REAL_        omega=0.5;
                      REAL_          fct;
                      REAL_          sze;

      INT_            nit;
      REAL_           s0,s1,ds;
      REAL_           f0,f1,df;
      INT_            rest;

      gas.init();

      start();

     {
         FILE *f= fopen( "mcontrols.dat","r" );
         assert(f );
         fscanf( f, "%d", &nit );
         fscanf( f, "%lf %lf %lf", &s0,&s1,&ds );
         fscanf( f, "%lf %lf %lf", &f0,&f1,&df );
         fscanf( f, "%d         ", &rest       );
         fscanf( f, "%lf        ", &rlx        );
         fscanf( f, "%lf        ", &omega      );
//       fscanf( f, "%lf        ", &save       );
         fclose( f );
     }

      co[3].malloc( &x  );
      co[3].malloc( &p  );
      co[3].malloc( &r  );
         cb.malloc( &y  );
         cb.malloc( &dy  );

         ci.malloc( &yi  );
         ci.malloc( &dyi  );

      cd[3].malloc( &d  );

      grid( x,y,yi, p );

      if( rest == 1 ){ co[3].memset( p,0  ); };

      stime=MPI_Wtime();

      fct= f0;
      sze= s0;

      for( it=0;it<nit;it++ )
     {
         REAL_4  res={0,0,0,0};
         REAL_4 lres={0,0,0,0};

         if( bsize > 2 ){ attr0( x,p, sze, omega, lres[3] ); }

         smo0( y,dy, x,p, r, d, su, fct );
         smov( y,dy, x,   r, d, su );
         smoe( y,dy, x,   r, d, su );
         smob( y,dy, x,   r, d, su, rlx );

         smoi( yi,dyi, x, r, d, su, rlx );


         for( INT_ t=0;t<hx.n;t++ )
        {
            for( INT_ k=0;k<bsize+1;k++ )
           {
               for( INT_ j=0;j<bsize+1;j++ )
              {
                  for( INT_ i=0;i<bsize+1;i++ )
                 {
                     REAL_3         f;
                   
                     co[0].loadv( i,  j,  k, t, r, f );


                     lres[0]+= fabs(f[0]);
                     lres[1]+= fabs(f[1]);
                     lres[2]+= fabs(f[2]);

                     f[0]*= rlx;
                     f[1]*= rlx;
                     f[2]*= rlx;

                     co[0].addv( i,j,k,t, x,f );

                 }
              }
           }
        }
         MPI_Allreduce( lres,res,4,REAL_MPI,MPI_SUM,MPI_COMM_WORLD);
         if( rank == 0 ){ printf( "%4d % 9.3e % 9.3e % 9.3e % 9.3e % 9.3e % 9.3e\n", it,res[0],res[1],res[2],res[3], sze,fct ); }

         fct+= df; fct= fmin( f1,fct );
         sze/= ds; sze= fmax( s1,sze );
     }

//    writegrid( x,y,yi, p );

      size_t len= co[0].bsize(4);
      size_t off;

      MPI_File fh;
      MPI_Status status;
      MPI_Offset off0,off1, offlen;
      char fname[1024];

      sprintf( fname, "grid/h");
      MPI_File_open(MPI_COMM_WORLD, fname, MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);
      assert( fh );

      off0=0;
      MPI_File_write_at(fh, off0, &bsize, 1,  INT_MPI, &status); off0+= sizeof(INT_);
      MPI_File_write_at(fh, off0,   &len, 1, LONG_MPI, &status); off0+= sizeof(size_t);

      offlen= len*sizeof(REAL_);
      offlen*=2;

      for( INT_ l=0;l<hx.n;l++ )
     {
         off= co[0].addr( 0,0,0,0,l );

         off1= off0+ offlen*hx[l].id;

         MPI_File_write_at(fh, off1, x+off, len, REAL_MPI, &status); off1+= len*sizeof(REAL_);
         MPI_File_write_at(fh, off1, p+off, len, REAL_MPI, &status);
     }
      MPI_Barrier( MPI_COMM_WORLD );
      MPI_File_close( &fh );


      len= cb.bsize(3);

      sprintf( fname, "grid/b");
      MPI_File_open(MPI_COMM_WORLD, fname, MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);
      assert( fh );

      off0=0;
      MPI_File_write_at(fh, off0, &bsize, 1,  INT_MPI, &status); off0+= sizeof(INT_);
      MPI_File_write_at(fh, off0,   &len, 1, LONG_MPI, &status); off0+= sizeof(size_t);

      offlen= len*sizeof(REAL_);

      ien=0;
      for( INT_ k=0;k<BTYPES;k++ )
     {   
         ist=ien;
         ien= bn.l[k][0];
         for( j=ist;j<ien;j++ )
        {

            INT_ i= bn.i[j];

            off= cb.addr( 0,0,0,j );
            off1= off0+ i*offlen;

            MPI_File_write_at(fh, off1, y+off, len, REAL_MPI, &status);
        }
     }  
      MPI_Barrier( MPI_COMM_WORLD );
      MPI_File_close( &fh );


      len= ci.bsize(3);

      sprintf( fname, "grid/i");
      MPI_File_open(MPI_COMM_WORLD, fname, MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);
      assert( fh );

      off0=0;
      MPI_File_write_at(fh, off0, &bsize, 1,  INT_MPI, &status); off0+= sizeof(INT_);
      MPI_File_write_at(fh, off0,   &len, 1, LONG_MPI, &status); off0+= sizeof(size_t);

      offlen= len*sizeof(REAL_);

      ien=0;
      for( INT_ k=0;k<ni;k++ )
     {
         g= bj[k][4];
         b= bj[k][0];
         off= ci.addr( 0,0,0,k );

         off1= off0;
         for (INT_ i=0;i<g;i++){ off1+= offlen*bi[i].bo.n; };
         off1+= b*offlen;

         MPI_File_write_at(fh, off1, yi+off, len, REAL_MPI, &status);

     }
      MPI_Barrier( MPI_COMM_WORLD );
      MPI_File_close( &fh );


      etime=MPI_Wtime();

      if( rank==0 )
     {
         printf( "elapsed time %9.3e\n", etime-stime );
     }

      delete[] x;
      delete[] y;
      delete[] yi;
      delete[] dy;
      delete[] dyi;
      delete[] r;
      delete[] d;
      delete[] p;

      stop();

      return;
  }
