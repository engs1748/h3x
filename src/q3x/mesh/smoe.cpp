#  include <q3x/q3x.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

#  include <q3x/inline/ellp>
#  include <q3x/inline/ellb>

   void q3x_t::smoe( REAL_ *y, REAL_ *dy, REAL_ *x, REAL_ *r, REAL_ *g, vsurf_t &su )
  {

      INT_       ntsk;
      INT_       tasks[NTASK];

      INT_       c;
      INT_       i, l,m,n, s,t;
      frule_t    r0,r1,r2,r3,r4,r5;
      frule_t    rr;
      INT_       num,loc;
      INT_      *idx;

      REAL_3     f;

      REAL_3     w;
      REAL_3     u;

      vtx_t      v;
      REAL_b     y0;
      REAL_b     y1;

      vtx_t      v0, dv0[2];
      vtx_t      v1, dv1[2];

      REAL_3     u2;

// update loop

      assert( bsize < 1024 );

      INT_ e;
      INT_ b0,b1;
      INT_ e0,e1;
      INT_ h0,h1;
      INT_ s0,s1;
      INT_ i0,j0,k0, i1,j1,k1, l0,m0,n0, l1,m1,n1;
      INT_ mm0;
      INT_ mm1;

      REAL_3 wn0[1024];
      REAL_3 wn1[1024];
      REAL_3 wx0[1024];
      REAL_3 wx1[1024];

// conformity along edges

      noexchange(  x, co[2],  com[1], msg[2] );
      while( locals( com[1],msg[2], ntsk,tasks ) )
     {
         for( t=0;t<ntsk;t++ )
        {
            com[1].task( tasks[t], num,loc, &idx );
            assert( num >= loc );

            e= tasks[t];


            b0= be[e][0];
            e0= be[e][1];
            b1= be[e][2];
            e1= be[e][3];


            assert( b0 < nb );
            assert( b1 < nb );

            if( b0 > -1 )
           {

               assert( b1 > -1 );
               s0= bn.s[b0];
               s1= bn.s[b1];
               mm0= bn.r[b0];
               mm1= bn.r[b1];
               if( (s0 != s1) || (abs(mm0-mm1)>1) )
              {
                  r0= frule_t( bn.f[b0], bsize+1,bsize+1,bsize+1 );
                  r1= frule_t( bn.f[b1], bsize+1,bsize+1,bsize+1 );

                  com[1].taskinfo( co[0], e0, r2,h0 );
                  com[1].taskinfo( co[0], e1, r3,h1 );

                  for( i=1;i<bsize;i++ )
                 {
                     r2.transform( i,0,0,   l0,m0,n0 ); r0.itransform( i0,j0,k0, l0,m0,n0 );
                     r3.transform( i,0,0,   l1,m1,n1 ); r1.itransform( i1,j1,k1, l1,m1,n1 );
                     assert( k0==0 );
                     assert( k1==0 );

                     cb.loadv( i0,j0,  b0,  y, y0 );
                     cb.loadv( i1,j1,  b1,  y, y1 );
                
                     su[s0]->interp( y0, v0,dv0 );
                     su[s1]->interp( y1, v1,dv1 );

                     cross3( dv0[0].x,dv0[1].x, wn0[i] );
                     cross3( dv1[0].x,dv1[1].x, wn1[i] );

                     wx0[i][0]= v0[0];
                     wx0[i][1]= v0[1];
                     wx0[i][2]= v0[2];

                     wx1[i][0]= v1[0];
                     wx1[i][1]= v1[1];
                     wx1[i][2]= v1[2];
                 }

                  for( s=0;s<loc;s++ )
                 {
                     com[1].taskinfo( co[0], idx[s], rr,c );

                     for( i=1;i<bsize;i++ )
                    {
                   
                        rr.transform( i,0,0, l,m,n  ); 
                   
                   
                        co[0].loadv(l,m,n, c,   x, u );
                        co[0].loadv(l,m,n, c,   r, f );
                        cross3( wn0[i],wn1[i], u2 );

                        w[0]= wn0[i][0]*( wx0[i][0]-u[0] )+ wn0[i][1]*( wx0[i][1]-u[1] )+ wn0[i][2]*( wx0[i][2]-u[2] );
                        w[1]= wn1[i][0]*( wx1[i][0]-u[0] )+ wn1[i][1]*( wx1[i][1]-u[1] )+ wn1[i][2]*( wx1[i][2]-u[2] );
                        w[2]= u2[0]*f[0]+ u2[1]*f[1]+ u2[2]*f[2];
                
                        REAL_ mm[9],m1[9];
                        mm[0+0]= wn0[i][0];
                        mm[1+0]= wn1[i][0];
                        mm[2+0]=  u2[0];
                        mm[0+3]= wn0[i][1];
                        mm[1+3]= wn1[i][1];
                        mm[2+3]=  u2[1];
                        mm[0+6]= wn0[i][2];
                        mm[1+6]= wn1[i][2];
                        mm[2+6]=  u2[2];
                        inv330( mm, m1 );
                        dot33( m1, w,f );

                        co[0].storev(l,m,n, c,   r, f );

                    }
                 }
              }
               else
              { 
                  if( s0 == s1 )
                 {

                     r0= frule_t( bn.f[b0], bsize+1,bsize+1,bsize+1 );
                     r1= frule_t( bn.f[b1], bsize+1,bsize+1,bsize+1 );

                     com[1].taskinfo( co[0], e0, r2,h0 );
                     com[1].taskinfo( co[0], e1, r3,h1 );

                     for( i=1;i<bsize;i++ )
                    {
                        r2.transform( i,0,0,   l0,m0,n0 ); r0.itransform( i0,j0,k0, l0,m0,n0 );
                        r3.transform( i,0,0,   l1,m1,n1 ); r1.itransform( i1,j1,k1, l1,m1,n1 );
                        assert( k0==0 );
                        assert( k1==0 );

                        cb.loadv( i0,j0,  b0,  y, y0 );
                        cb.loadv( i1,j1,  b1,  y, y1 );

                        su[s0]->interp( y0, v0,dv0 );
                        su[s0]->interp( y1, v1,dv1 );

                        cross3( dv0[0].x,dv0[1].x, u );
                        cross3( dv1[0].x,dv1[1].x, f );
                        wn0[i][0]= 0.5*( f[0]+u[0] );
                        wn0[i][1]= 0.5*( f[1]+u[1] );
                        wn0[i][2]= 0.5*( f[2]+u[2] );
                        REAL_ ww= norm32( wn0[i] ); scal3( 1./ww, wn0[i] );

                        wx0[i][0]= 0.5*(v0[0]+v1[0]);
                        wx0[i][1]= 0.5*(v0[1]+v1[1]);
                        wx0[i][2]= 0.5*(v0[2]+v1[2]);

                    }

 
                     for( s=0;s<loc;s++ )
                    {
                        com[1].taskinfo( co[0], idx[s], rr,c );

                        for( i=1;i<bsize;i++ )
                       {
                      
                           rr.transform( i,0,0, l,m,n  ); 
                      
                           co[0].loadv(l,m,n, c,   x, u );
                           co[0].loadv(l,m,n, c,   r, f );

                           REAL_ ww= wn0[i][0]*( wx0[i][0]- u[0]- f[0] )+
                                     wn0[i][1]*( wx0[i][1]- u[1]- f[1] )+
                                     wn0[i][2]*( wx0[i][2]- u[2]- f[2] );

                           f[0]+= ww*wn0[i][0];
                           f[1]+= ww*wn0[i][1];
                           f[2]+= ww*wn0[i][2];

                           co[0].storev(l,m,n, c,   r, f );

                       }
                    }
                 }
              }  
           }
        }
     }


      return;
  }


