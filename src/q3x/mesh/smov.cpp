#  include <q3x/q3x.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

#  include <q3x/inline/ellp>
#  include <q3x/inline/ellb>

   void q3x_t::smov( REAL_ *y, REAL_ *dy, REAL_ *x, REAL_ *r, REAL_ *g, vsurf_t &su )
  {

      INT_       ntsk;
      INT_       tasks[NTASK];

      INT_       c;
      INT_       d;
      INT_       i,j,k, l,m,n, t,p,o;
      INT_       ist,ien;
      frule_t    r0,r1,r2,r3,r4,r5;
      frule_t    rr;
      frule_t    rb[32];
      frule_t    rv[32];
      INT_       num,loc;
      INT_      *idx;

      REAL_x     f;

      REAL_x     u;

      vtx_t      v0, v1, v2;
      vtx_t      dv0[2],dv1[2],dv2[2];

      INT_       b[32];
      INT_       e[32];
      INT_       h[32];
      INT_2      z[32];
      INT_       s[32];
      INT_       q[32];
      INT_ i0,j0,k0, i1,j1,k1, l0,m0,n0, l1,m1,n1;
      INT_ i2,j2,k2, l2,m2,n2;
      INT_ b0,b1,b2, s0,s1,s2;
      INT_ p0,p1,p2;

      REAL_x     wn0,wn1,wn2;
      REAL_x     wx0,wx1,wx2;
      REAL_x     w;
      REAL_b     y0,y1,y2;

// update loop

      assert( bsize < 1024 );

// conformity at vertices

      noexchange(  x, co[3],  com[2], msg[3] );
      while( locals( com[2],msg[3], ntsk,tasks ) )
     {
         for( t=0;t<ntsk;t++ )
        {
            com[2].task( tasks[t], num,loc, &idx );
            assert( num >= loc );

            d= tasks[t];

            ien= lv[d];
            ist= 0;
            if( d > 0 ){ ist=lv[d-1]; };


// collect information about tasks, surfaces, boundary patches etc.
            if( ien > ist )
           {

            i= 0;
            l= 0;
         
            for( j=ist;j<ien;j++ )
           {
               b[i]= bv[j][0];
               e[i]= bv[j][1];
               rb[i]= frule_t( bn.f[ b[i] ], bsize+1,bsize+1,bsize+1 );

               com[2].taskinfo( co[0],e[i], rv[i], h[i] );

               k= bn.s[ b[i] ]; // surface
               o= bn.r[ b[i] ]; // branch

               bool flag= false;
               for( m=0;m<l;m++ )
              {
                  if( z[m][0] == k && abs( z[m][1] - o )<2 ){ flag= true; break; };
              }
               if( !flag )
              {
                  m= l;
                  z[m][0]= k;
                  z[m][1]= o;
                  s[l++]= k;  
                   
                  
              }
//             printf( "locat surfce %d in local list (%d,%d)\n", k,l,m );
               g[i]= m;         // patch i is attached to surface m (in the local list)
               q[m]= i;         // surface m in the local list is represented by patch q
               i++;
     
           }
            assert( l > 0 );

            if( l >= 3 )
           {
// l >= 3: find intersection between three surfaces, now we assume they are independent

               s0= s[0]; // surface 0
               s1= s[1]; // surface 1
               s2= s[2]; // surface 2

               p0= q[0]; // surface 0
               p1= q[1]; // surface 1
               p2= q[2]; // surface 2

               b0= b[p0]; // one patch in the local list on surface 0
               b1= b[p1]; // one patch in the local list on surface 1
               b2= b[p2]; // one patch in the local list on surface 2
               
               rv[p0].transform( 0,0,0,   l0 ,m0 ,n0  ); rb[p0].itransform( i0,j0,k0, l0,m0,n0 ); assert( k0==0 );
               rv[p1].transform( 0,0,0,   l1 ,m1 ,n1  ); rb[p1].itransform( i1,j1,k1, l1,m1,n1 ); assert( k1==0 );
               rv[p2].transform( 0,0,0,   l2 ,m2 ,n2  ); rb[p2].itransform( i2,j2,k2, l2,m2,n2 ); assert( k2==0 );

               cb.loadv( i0,j0,  b0,  y, y0 );
               cb.loadv( i1,j1,  b1,  y, y1 );
               cb.loadv( i2,j2,  b2,  y, y2 );

               su[s0]->interp( y0, v0,dv0 );
               su[s1]->interp( y1, v1,dv1 );
               su[s2]->interp( y2, v2,dv2 );

               cross3( dv0[0].x,dv0[1].x, wn0 );
               cross3( dv1[0].x,dv1[1].x, wn1 );
               cross3( dv2[0].x,dv2[1].x, wn2 );

               wx0[0]= v0[0];
               wx0[1]= v0[1];
               wx0[2]= v0[2];

               wx1[0]= v1[0];
               wx1[1]= v1[1];
               wx1[2]= v1[2];

               wx2[0]= v2[0];
               wx2[1]= v2[1];
               wx2[2]= v2[2];
             
               REAL_9 mm,mm1;
               mm[0+0]= wn0[0];
               mm[1+0]= wn1[0];
               mm[2+0]= wn2[0];
               mm[0+3]= wn0[1];
               mm[1+3]= wn1[1];
               mm[2+3]= wn2[1];
               mm[0+6]= wn0[2];
               mm[1+6]= wn1[2];
               mm[2+6]= wn2[2];

               inv330( mm,mm1 );
//             assert( l == 3 );

               for( p=0;p<loc;p++ )
              {
                  com[2].taskinfo( co[0], idx[p], rr,c );
          
                  rr.transform( 0,0,0,  l,m,n  ); co[0].loadv(l,m,n, c,   x, u );
             
                  f[0]= wn0[0]*( wx0[0]-u[0] )+ wn0[1]*( wx0[1]-u[1] )+ wn0[2]*( wx0[2]-u[2] );
                  f[1]= wn1[0]*( wx1[0]-u[0] )+ wn1[1]*( wx1[1]-u[1] )+ wn1[2]*( wx1[2]-u[2] );
                  f[2]= wn2[0]*( wx2[0]-u[0] )+ wn2[1]*( wx2[1]-u[1] )+ wn2[2]*( wx2[2]-u[2] );


                  dot33( mm1, f, u );                                    

                  co[0].storev(l,m,n, c,   r,u );
                
              }
           }
            else
           {
               if( l == 2 )
              {
                  s0= s[0]; // surface 0
                  s1= s[1]; // surface 1
   
                  p0= q[0]; // surface 0
                  p1= q[1]; // surface 1
   
                  b0= b[p0]; // one patch in the local list on surface 0
                  b1= b[p1]; // one patch in the local list on surface 1

                  rv[p0].transform( 0,0,0,   l0 ,m0 ,n0   ); rb[p0].itransform( i0,j0,k0, l0,m0,n0 ); assert( k0==0 );
                  rv[p1].transform( 0,0,0,   l1 ,m1 ,n1   ); rb[p1].itransform( i1,j1,k1, l1,m1,n1 ); assert( k1==0 );

                  cb.loadv( i0,j0,  b0,  y, y0 );
                  cb.loadv( i1,j1,  b1,  y, y1 );

                  su[s0]->interp( y0, v0,dv0 );
                  su[s1]->interp( y1, v1,dv1 );

                  cross3( dv0[0].x,dv0[1].x, wn0 );
                  cross3( dv1[0].x,dv1[1].x, wn1 );

                  wx0[0]= v0[0];
                  wx0[1]= v0[1];
                  wx0[2]= v0[2];

                  wx1[0]= v1[0];
                  wx1[1]= v1[1];
                  wx1[2]= v1[2];

                  cross3( wn0,wn1, wn2 );
             
                  REAL_9 mm,mm1;
                  mm[0+0]= wn0[0];
                  mm[1+0]= wn1[0];
                  mm[2+0]= wn2[0];

                  mm[0+3]= wn0[1];
                  mm[1+3]= wn1[1];
                  mm[2+3]= wn2[1];

                  mm[0+6]= wn0[2];
                  mm[1+6]= wn1[2];
                  mm[2+6]= wn2[2];

                  inv330( mm,mm1 );

                  for( p=0;p<loc;p++ )
                 {
                     com[2].taskinfo( co[0], idx[p], rr,c );
          
                     rr.transform( 0,0,0, l,m,n  ); 
                   
                     co[0].loadv(l,m,n, c,   x, u );
                     co[0].loadv(l,m,n, c,   r, w );
             
                     f[0]= wn0[0]*( wx0[0]-u[0] )+ wn0[1]*( wx0[1]-u[1] )+ wn0[2]*( wx0[2]-u[2] );
                     f[1]= wn1[0]*( wx1[0]-u[0] )+ wn1[1]*( wx1[1]-u[1] )+ wn1[2]*( wx1[2]-u[2] );
                     f[2]= wn2[0]*  w[0]  +        wn2[1]*  w[1]  +        wn2[2]*  w[2]  ;

                     dot33( mm1, f, u );
          
                     co[0].storev(l,m,n, c,   r,u );
                   
                 }
              }
               else
              {
                  if( l == 1 )
                 {
                     s0= s[0]; // surface 0
     
                     p0= q[0]; // surface 0
     
                     b0= b[p0]; // one patch in the local list on surface 0
                     
                     rv[p0].transform( 0,0,0,   l0 ,m0 ,n0   ); rb[p0].itransform( i0,j0,k0, l0,m0,n0 ); assert( k0==0 );

                     cb.loadv( i0,j0,  b0,  y, y0 );

                     su[s0]->interp( y0, v0,dv0 );
                     cross3( dv0[0].x,dv0[1].x, wn0 );
                     wx0[0]= v0[0];
                     wx0[1]= v0[1];
                     wx0[2]= v0[2];

                     REAL_ ww= norm32( wn0 ); scal3( 1./ww,wn0 );

                     for( p=0;p<loc;p++ )
                    {
                        com[2].taskinfo( co[0], idx[p], rr,c );
          
                        rr.transform( 0,0,0, l,m,n  ); 
                      
                        co[0].loadv(l,m,n, c,   x, u );
                        co[0].loadv(l,m,n, c,   r, w );
             
                        ww= ( wx0[0]- u[0]- w[0] )*wn0[0]+ ( wx0[1]- u[1]- w[1] )*wn0[1]+ ( wx0[2]- u[2]- w[2] )*wn0[2];

                        f[0]= w[0]+ ww*wn0[0];
                        f[1]= w[1]+ ww*wn0[1];
                        f[2]= w[2]+ ww*wn0[2];

                        co[0].storev(l,m,n, c,   r,f );

                    }
                 }
              }
           }
           }
        }
     }

      return;
  }


