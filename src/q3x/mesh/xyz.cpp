

#  include <q3x/q3x.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::xyz( INT_ l, INT_ m, INT_ n, INT_ d, REAL_ *u,
                                                    REAL_ *v,
                                                    REAL_ *w )
  {

//    ijk( l,m,n );
      assert( false );

      MMAP_      coo;
      INT_         i,j,k;
      INT_         e,f,g;
      INT_         b;
      INT_         i0,i1,i2;
      REAL_       *x;

      bsize= d;

      size_t len=0;
      coo.dims( len, bsize+1,3,bsize+1,bsize+1, -1 );

      x= new REAL_[len];

      b= 0;

      for( g=0;g<n;g++ )
     {
         for( f=0;f<m;f++ )
        {
            for( e=0;e<l;e++ )
           {
               memset( x,0,len*sizeof(REAL_) );

               for( k=0; k<n+1;k++ )
              {
                  for( j=0; j<n+1;j++ )
                 {
                     for( i=0; i<n+1;i++ )
                    {
                        i0= coo.addr(0,i,j,k);
                        i1= coo.addr(1,i,j,k);
                        i2= coo.addr(2,i,j,k);

                        x[i0]= u[i+bsize*e];
                        x[i1]= v[j+bsize*f];
                        x[i2]= w[k+bsize*g];
                
                    }
                 }
              }

               assert( hx[b].id == b );

               FILE *f;
               char  name[512];
               sprintf( name, "grid/%06d.bin", b );
               f= fopen( name, "w" );
             ::fwrite( &bsize,    1,sizeof(INT_), f );
             ::fwrite( &len,      1,sizeof(size_t), f );
             ::fwrite(  x,len,sizeof(REAL_),f );
               fclose( f );

               b++;

           }
        }
     }

      delete[] x; x= NULL;
  }
