
#  include <q3x/q3x.h>
#  include <q2.h>


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::ascii( INT_ nn )
  {
      MMAP_      coo;
      MMAP_      cid;
      MMAP_      cob;
      INT_         i,j,h,d,k,l,m,n, u,v,w, q;
      INT_         n0;
      INT_       l0,l1,l2,l3,l4,l5,l6,l7;

      REAL_       *x; // coordinates
      REAL_       *p; // control functions

      REAL_        x0[3];
      REAL_3      *xa;

      INT_        *id= NULL;
      frme_t       g;

      FILE *f=NULL;
      char  name[512];

      sprintf( name, "grid/h/%06d", hx[0].id );
      f= fopen( name, "r" );
      assert(f);
    ::fread( &n,        1,sizeof(INT_), f );
      fclose( f );
      assert( n%nn == 0 );

      bsize= n; 

      if( qv.n==0 ){ assert( false ); quads(); };
      if( eg.n==0 ){ edges(); };

      size_t len=0;
      len=0; cid.dims( len, bsize/nn+1,1,bsize/nn+1,bsize/nn+1, hx.n, -1 );

      len=0; coo.dims( len, bsize+1,3,bsize+1,bsize+1,  -1 );


      coo.malloc( &x );
      coo.malloc( &p );

      cid.malloc( &id );


// number nodes

      n= 0;

      for( l=0;l<hx.n;l++ )
     {
         for( k=1;k<bsize/nn;k++ )
        {
            for( j=1;j<bsize/nn;j++ )
           {
               for( i=1;i<bsize/nn;i++ )
              {
                  cid.storev( i,j,k,l, id,&n );
                  n++;   
              }
           }
        }
     }


      for( l=0;l<qv.n;l++ )
     {
         if( qv[l].h[0] > -1 || qv[l].h[1] > -1 )
        {
            n0= n;
            for( k=0;k<2;k++ )
           {
               h= qv[l].h[k];
               d= qv[l].q[k];
               if( d > -1 )
              {
                  hx[h].qdir( d,g );
                  g[2]= abs(g[2]);
                  if( d%2 ){ g[2]=!g[2]; };
          
                  frule_t ff= frule_t( g,bsize/nn+1,bsize/nn+1,bsize/nn+1 );
          
                  for( j=1;j<bsize/nn;j++ )
                 {
                     for( i=1;i<bsize/nn;i++ )
                    {
                        ff.transform( i,j,0, u,v,w );
          
                  assert( u >= 0 && u <= bsize/nn+1 );
                  assert( v >= 0 && v <= bsize/nn+1 );
                  assert( w >= 0 && w <= bsize/nn+1 );
          
//                      printf( "any of all this (faces)\n" );
          
                        n= n0+ (i-1)+(bsize/nn-1)*(j-1);
          
                        cid.storev( u,v,w,h, id,&n );
                    }
                 }
              }
           }
            n= n0+ (bsize/nn-1)*(bsize/nn-1);
        }
     }

      for( l=0;l<eg.n;l++ )
     {
         VINT_2 list;

         list.n= 0;
         edge( l, list );

         if( list.n > 0 )
        { 

            n0= n;
            for( k=0;k<list.n;k++ )
           {
               h= list[k][0];
               d= list[k][1];
               if( h > -1 )
              {
                  hx[h].edir( d,g );
          
                  frule_t ff= frule_t( g,bsize/nn+1,bsize/nn+1,bsize/nn+1 );
          
                  for( i=1;i<bsize/nn;i++ )
                 {
                     ff.transform( i,0,0, u,v,w );
          
                  assert( u >= 0 && u <= bsize/nn+1 );
                  assert( v >= 0 && v <= bsize/nn+1 );
                  assert( w >= 0 && w <= bsize/nn+1 );
          
                     n= n0+(i-1);
//                   printf( "edge %d %d %d %d %d\n", u,v,w,h, n ); 
//                   printf( "any of all this (edges)\n" );
          
                     cid.storev( u,v,w,h, id,&n );
                 }
              }
           }
            n= n0+ (bsize/nn-1);
        }
     }

      for( l=0;l<vt.n;l++ )
     {
         if( vt[l].h > -1 )
        {
            VINT_2 qist;
            VINT_2 hist;
          
            hist.n= 0;
            qist.n= 0;
            point( l, hist,qist );
          
            n0= n;
            for( k=0;k<hist.n;k++ )
           {
               h= hist[k][0];
               d= hist[k][1];
               if( h > -1 )
              {
                  hx[h].pdir( d,g );
          
                  frule_t ff= frule_t( g,bsize/nn+1,bsize/nn+1,bsize/nn+1 );
          
                  ff.transform( 0,0,0, u,v,w );
                  n= n0;
          
                  assert( u >= 0 && u <= bsize/nn+1 );
                  assert( v >= 0 && v <= bsize/nn+1 );
                  assert( w >= 0 && w <= bsize/nn+1 );
          
//                printf( "vrtx %d %d %d %d %d\n", u,v,w,h, n ); 
          
                  cid.storev( u,v,w,h, id,&n );
              }
           }
            n= n0+1;
        }
     }

// number of boundary faces
      q= 0;
      for( k=0;k<nb;k++ )
     {
         if( (bz[k].lbl[0] >= AWALLS) && (bz[k].lbl[0] < TWALLS) )
        {
            for( l=0;l<bz[k].bo.n;l++ )
           {
               h= bz[k].bo[l].h;
               d= bz[k].bo[l].q;
               if( h > -1 )
              {
               
                  q++;
              }
           }
        }
     }


// read nodes
      xa= new REAL_3[n];

      for( h=0;h<hx.n;h++ )
     {
// now build control functions

         sprintf( name, "grid/h/%06d", hx[h].id );
         f= fopen( name, "r" );
         assert(f);
       ::fread( &m,        1,sizeof(INT_), f );
         assert( m == bsize );
       ::fread( &len,      1,sizeof(size_t), f );

       ::fread(  x,len,sizeof(REAL_),f );
       ::fread(  p,len,sizeof(REAL_),f );
         fclose( f );

         for( k=0;k<bsize/nn+1;k++ )
        {
            for( j=0;j<bsize/nn+1;j++ )
           {
               for( i=0;i<bsize/nn+1;i++ )
              {
                  cid.loadv( i,j,k,h, id, &l );
                  assert( l >=0 && l < n );

                  assert( i*nn >= 0 && i*nn < bsize+1 );
                  assert( j*nn >= 0 && j*nn < bsize+1 );
                  assert( k*nn >= 0 && k*nn < bsize+1 );

                  coo.loadv( i*nn,j*nn,k*nn,    x,  x0 );

                  xa[l][0]= x0[0];
                  xa[l][1]= x0[1];
                  xa[l][2]= x0[2];
                  
              }
           }
        }
     }

      f= fopen( "ascii.dat","w" );

      fprintf( f, "%d %d %d\n", n, hx.n*(bsize/nn)*(bsize/nn)*(bsize/nn),
                         q*(bsize/nn)*(bsize/nn) );
      for( i=0;i<n;i++ )
     {
         fprintf( f,"% 14.7e % 14.7e % 14.7e\n", 
                  xa[i][0],xa[i][1],xa[i][2] );
     }
      for( h=0;h<hx.n;h++ )
     {
         for( k=0;k<bsize/nn;k++ )
        {
            for( j=0;j<bsize/nn;j++ )
           {
               for( i=0;i<bsize/nn;i++ )
              {
                 
                  cid.loadv(   i,  j,  k,h, id, &l0 );
                  cid.loadv( 1+i,  j,  k,h, id, &l1 );
                  cid.loadv(   i,1+j,  k,h, id, &l2 );
                  cid.loadv( 1+i,1+j,  k,h, id, &l3 );
                  cid.loadv(   i,  j,1+k,h, id, &l4 );
                  cid.loadv( 1+i,  j,1+k,h, id, &l5 );
                  cid.loadv(   i,1+j,1+k,h, id, &l6 );
                  cid.loadv( 1+i,1+j,1+k,h, id, &l7 );

                  fprintf( f, "%d %d %d %d %d %d %d %d\n",
                           l0,l1,l2,l3,l4,l5,l6,l7 );
              }
           }
        }
     }
     

      for( k=0;k<nb;k++ )
     {
         if( (bz[k].lbl[0] >= AWALLS) && (bz[k].lbl[0] < TWALLS) )
        {
            for( l=0;l<bz[k].bo.n;l++ )
           {
               h= bz[k].bo[l].h;
               d= bz[k].bo[l].q;
               if( h > -1 )
              {
                  hx[h].qdir( d,g );
                  g[2]= abs(g[2]);
                  if( d%2 ){ g[2]=!g[2]; };
          
                  frule_t ff= frule_t( g,bsize/nn+1,bsize/nn+1,bsize/nn+1 );
               
                  for( j=0;j<bsize/nn;j++ )
                 {
                     for( i=0;i<bsize/nn;i++ )
                    {
   ff.transform(   i,  j,0, u,v,w );cid.loadv( u,v,w,h, id, &l0 );
   ff.transform( 1+i,  j,0, u,v,w );cid.loadv( u,v,w,h, id, &l1 );
   ff.transform(   i,1+j,0, u,v,w );cid.loadv( u,v,w,h, id, &l2 );
   ff.transform( 1+i,1+j,0, u,v,w );cid.loadv( u,v,w,h, id, &l3 );
                  
                  fprintf( f, "%d %d %d %d\n",  l0,l1,l2,l3 );
                  
                  
               
                    }
                 }
              }
           }

        }
     }
      fclose( f );
      
      return;
  }
