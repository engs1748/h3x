
#include<q3x/q3x.h>
#include<q3x/inline/setp3>

   void q3x_t::attr0( REAL_ *x, REAL_ *p, REAL_ dst, REAL_ omega, REAL_ &res )
  {
      res=0;
      if( dst == 0 ){ return; }

      REAL_3     x000,x100,x200, x010,x110,x210, x020,x120,x220;
      REAL_3     x001,x101,x201, x011,x111,x211, x021,x121,x221;
      REAL_3     x002,x102,x202, x012,x112,x212, x022,x122,x222;
      REAL_3     q[8];
      REAL_3     dp;
      REAL_3     f;
      REAL_      u,v,w;
      res=0;

      INT_       i,j,k,t;

      for( t=0;t<hx.n;t++ )
     {

         i=1;
         j=1;
         k=1;

         co[0].loadv( i-1,j-1,k-1,t, x,x000 );
         co[0].loadv( i  ,j-1,k-1,t, x,x100 );
         co[0].loadv( i+1,j-1,k-1,t, x,x200 );
                                           
         co[0].loadv( i-1,j  ,k-1,t, x,x010 );
         co[0].loadv( i  ,j  ,k-1,t, x,x110 );
         co[0].loadv( i+1,j  ,k-1,t, x,x210 );
                                           
         co[0].loadv( i-1,j+1,k-1,t, x,x020 );
         co[0].loadv( i  ,j+1,k-1,t, x,x120 );
         co[0].loadv( i+1,j+1,k-1,t, x,x220 );
                                           
         co[0].loadv( i-1,j-1,k  ,t, x,x001 );
         co[0].loadv( i  ,j-1,k  ,t, x,x101 );
         co[0].loadv( i+1,j-1,k  ,t, x,x201 );
                                           
         co[0].loadv( i-1,j  ,k  ,t, x,x011 );
         co[0].loadv( i  ,j  ,k  ,t, x,x111 );
         co[0].loadv( i+1,j  ,k  ,t, x,x211 );
                                           
         co[0].loadv( i-1,j+1,k  ,t, x,x021 );
         co[0].loadv( i  ,j+1,k  ,t, x,x121 );
         co[0].loadv( i+1,j+1,k  ,t, x,x221 );
                                           
         co[0].loadv( i-1,j-1,k+1,t, x,x002 );
         co[0].loadv( i  ,j-1,k+1,t, x,x102 );
         co[0].loadv( i+1,j-1,k+1,t, x,x202 );
                                           
         co[0].loadv( i-1,j  ,k+1,t, x,x012 );
         co[0].loadv( i  ,j  ,k+1,t, x,x112 );
         co[0].loadv( i+1,j  ,k+1,t, x,x212 );
                                           
         co[0].loadv( i-1,j+1,k+1,t, x,x022 );
         co[0].loadv( i  ,j+1,k+1,t, x,x122 );
         co[0].loadv( i+1,j+1,k+1,t, x,x222 );

         co[0].loadv( i  ,j  ,k  ,t, p,q[0] );

         setp3(  x000,  x100,  x200,  x010,  x110,  x210,  x020,  x120,  x220,
                 x001,  x101,  x201,  x011,  x111,  x211,  x021,  x121,  x221,
                 x002,  x102,  x202,  x012,  x112,  x212,  x022,  x122,  x222,  q[0],  dp, 0,0,0, hx[t].d[0][0], hx[t].d[0][1], hx[t].d[0][2], dst );

         res+= fabs( dp[0] );
         res+= fabs( dp[1] );
         res+= fabs( dp[2] );

         q[0][0]+= omega*dp[0];
         q[0][1]+= omega*dp[1];
         q[0][2]+= omega*dp[2];


         i=bsize-1;
         j=1;
         k=1;

         co[0].loadv( i-1,j-1,k-1,t, x,x000 );
         co[0].loadv( i  ,j-1,k-1,t, x,x100 );
         co[0].loadv( i+1,j-1,k-1,t, x,x200 );
                                           
         co[0].loadv( i-1,j  ,k-1,t, x,x010 );
         co[0].loadv( i  ,j  ,k-1,t, x,x110 );
         co[0].loadv( i+1,j  ,k-1,t, x,x210 );
                                           
         co[0].loadv( i-1,j+1,k-1,t, x,x020 );
         co[0].loadv( i  ,j+1,k-1,t, x,x120 );
         co[0].loadv( i+1,j+1,k-1,t, x,x220 );
                                           
         co[0].loadv( i-1,j-1,k  ,t, x,x001 );
         co[0].loadv( i  ,j-1,k  ,t, x,x101 );
         co[0].loadv( i+1,j-1,k  ,t, x,x201 );
                                           
         co[0].loadv( i-1,j  ,k  ,t, x,x011 );
         co[0].loadv( i  ,j  ,k  ,t, x,x111 );
         co[0].loadv( i+1,j  ,k  ,t, x,x211 );
                                           
         co[0].loadv( i-1,j+1,k  ,t, x,x021 );
         co[0].loadv( i  ,j+1,k  ,t, x,x121 );
         co[0].loadv( i+1,j+1,k  ,t, x,x221 );
                                           
         co[0].loadv( i-1,j-1,k+1,t, x,x002 );
         co[0].loadv( i  ,j-1,k+1,t, x,x102 );
         co[0].loadv( i+1,j-1,k+1,t, x,x202 );
                                           
         co[0].loadv( i-1,j  ,k+1,t, x,x012 );
         co[0].loadv( i  ,j  ,k+1,t, x,x112 );
         co[0].loadv( i+1,j  ,k+1,t, x,x212 );
                                           
         co[0].loadv( i-1,j+1,k+1,t, x,x022 );
         co[0].loadv( i  ,j+1,k+1,t, x,x122 );
         co[0].loadv( i+1,j+1,k+1,t, x,x222 );

         co[0].loadv( i  ,j  ,k  ,t, p,q[1] );

         setp3(  x000,  x100,  x200,  x010,  x110,  x210,  x020,  x120,  x220,
                 x001,  x101,  x201,  x011,  x111,  x211,  x021,  x121,  x221,
                 x002,  x102,  x202,  x012,  x112,  x212,  x022,  x122,  x222,  q[1],  dp, 1,0,0, hx[t].d[1][0], hx[t].d[1][1], hx[t].d[1][2], dst );

         res+= fabs( dp[0] );
         res+= fabs( dp[1] );
         res+= fabs( dp[2] );

         q[1][0]+= omega*dp[0];
         q[1][1]+= omega*dp[1];
         q[1][2]+= omega*dp[2];

         i=1;
         j=bsize-1;
         k=1;

         co[0].loadv( i-1,j-1,k-1,t, x,x000 );
         co[0].loadv( i  ,j-1,k-1,t, x,x100 );
         co[0].loadv( i+1,j-1,k-1,t, x,x200 );
                                           
         co[0].loadv( i-1,j  ,k-1,t, x,x010 );
         co[0].loadv( i  ,j  ,k-1,t, x,x110 );
         co[0].loadv( i+1,j  ,k-1,t, x,x210 );
                                           
         co[0].loadv( i-1,j+1,k-1,t, x,x020 );
         co[0].loadv( i  ,j+1,k-1,t, x,x120 );
         co[0].loadv( i+1,j+1,k-1,t, x,x220 );
                                           
         co[0].loadv( i-1,j-1,k  ,t, x,x001 );
         co[0].loadv( i  ,j-1,k  ,t, x,x101 );
         co[0].loadv( i+1,j-1,k  ,t, x,x201 );
                                           
         co[0].loadv( i-1,j  ,k  ,t, x,x011 );
         co[0].loadv( i  ,j  ,k  ,t, x,x111 );
         co[0].loadv( i+1,j  ,k  ,t, x,x211 );
                                           
         co[0].loadv( i-1,j+1,k  ,t, x,x021 );
         co[0].loadv( i  ,j+1,k  ,t, x,x121 );
         co[0].loadv( i+1,j+1,k  ,t, x,x221 );
                                           
         co[0].loadv( i-1,j-1,k+1,t, x,x002 );
         co[0].loadv( i  ,j-1,k+1,t, x,x102 );
         co[0].loadv( i+1,j-1,k+1,t, x,x202 );
                                           
         co[0].loadv( i-1,j  ,k+1,t, x,x012 );
         co[0].loadv( i  ,j  ,k+1,t, x,x112 );
         co[0].loadv( i+1,j  ,k+1,t, x,x212 );
                                           
         co[0].loadv( i-1,j+1,k+1,t, x,x022 );
         co[0].loadv( i  ,j+1,k+1,t, x,x122 );
         co[0].loadv( i+1,j+1,k+1,t, x,x222 );

         co[0].loadv( i  ,j  ,k  ,t, p,q[2] );

         setp3(  x000,  x100,  x200,  x010,  x110,  x210,  x020,  x120,  x220,
                 x001,  x101,  x201,  x011,  x111,  x211,  x021,  x121,  x221,
                 x002,  x102,  x202,  x012,  x112,  x212,  x022,  x122,  x222,  q[2],  dp, 0,1,0, hx[t].d[2][0], hx[t].d[2][1], hx[t].d[2][2], dst );

         res+= fabs( dp[0] );
         res+= fabs( dp[1] );
         res+= fabs( dp[2] );

         q[2][0]+= omega*dp[0];
         q[2][1]+= omega*dp[1];
         q[2][2]+= omega*dp[2];

         i=bsize-1;
         j=bsize-1;
         k=1;

         co[0].loadv( i-1,j-1,k-1,t, x,x000 );
         co[0].loadv( i  ,j-1,k-1,t, x,x100 );
         co[0].loadv( i+1,j-1,k-1,t, x,x200 );
                                           
         co[0].loadv( i-1,j  ,k-1,t, x,x010 );
         co[0].loadv( i  ,j  ,k-1,t, x,x110 );
         co[0].loadv( i+1,j  ,k-1,t, x,x210 );
                                           
         co[0].loadv( i-1,j+1,k-1,t, x,x020 );
         co[0].loadv( i  ,j+1,k-1,t, x,x120 );
         co[0].loadv( i+1,j+1,k-1,t, x,x220 );
                                           
         co[0].loadv( i-1,j-1,k  ,t, x,x001 );
         co[0].loadv( i  ,j-1,k  ,t, x,x101 );
         co[0].loadv( i+1,j-1,k  ,t, x,x201 );
                                           
         co[0].loadv( i-1,j  ,k  ,t, x,x011 );
         co[0].loadv( i  ,j  ,k  ,t, x,x111 );
         co[0].loadv( i+1,j  ,k  ,t, x,x211 );
                                           
         co[0].loadv( i-1,j+1,k  ,t, x,x021 );
         co[0].loadv( i  ,j+1,k  ,t, x,x121 );
         co[0].loadv( i+1,j+1,k  ,t, x,x221 );
                                           
         co[0].loadv( i-1,j-1,k+1,t, x,x002 );
         co[0].loadv( i  ,j-1,k+1,t, x,x102 );
         co[0].loadv( i+1,j-1,k+1,t, x,x202 );
                                           
         co[0].loadv( i-1,j  ,k+1,t, x,x012 );
         co[0].loadv( i  ,j  ,k+1,t, x,x112 );
         co[0].loadv( i+1,j  ,k+1,t, x,x212 );
                                           
         co[0].loadv( i-1,j+1,k+1,t, x,x022 );
         co[0].loadv( i  ,j+1,k+1,t, x,x122 );
         co[0].loadv( i+1,j+1,k+1,t, x,x222 );

         co[0].loadv( i  ,j  ,k  ,t, p,q[3] );

         setp3(  x000,  x100,  x200,  x010,  x110,  x210,  x020,  x120,  x220,
                 x001,  x101,  x201,  x011,  x111,  x211,  x021,  x121,  x221,
                 x002,  x102,  x202,  x012,  x112,  x212,  x022,  x122,  x222,  q[3],  dp, 1,1,0, hx[t].d[3][0], hx[t].d[3][1], hx[t].d[3][2], dst );

         res+= fabs( dp[0] );
         res+= fabs( dp[1] );
         res+= fabs( dp[2] );

         q[3][0]+= omega*dp[0];
         q[3][1]+= omega*dp[1];
         q[3][2]+= omega*dp[2];

         i=1;
         j=1;
         k=bsize-1;

         co[0].loadv( i-1,j-1,k-1,t, x,x000 );
         co[0].loadv( i  ,j-1,k-1,t, x,x100 );
         co[0].loadv( i+1,j-1,k-1,t, x,x200 );
                                           
         co[0].loadv( i-1,j  ,k-1,t, x,x010 );
         co[0].loadv( i  ,j  ,k-1,t, x,x110 );
         co[0].loadv( i+1,j  ,k-1,t, x,x210 );
                                           
         co[0].loadv( i-1,j+1,k-1,t, x,x020 );
         co[0].loadv( i  ,j+1,k-1,t, x,x120 );
         co[0].loadv( i+1,j+1,k-1,t, x,x220 );
                                           
         co[0].loadv( i-1,j-1,k  ,t, x,x001 );
         co[0].loadv( i  ,j-1,k  ,t, x,x101 );
         co[0].loadv( i+1,j-1,k  ,t, x,x201 );
                                           
         co[0].loadv( i-1,j  ,k  ,t, x,x011 );
         co[0].loadv( i  ,j  ,k  ,t, x,x111 );
         co[0].loadv( i+1,j  ,k  ,t, x,x211 );
                                           
         co[0].loadv( i-1,j+1,k  ,t, x,x021 );
         co[0].loadv( i  ,j+1,k  ,t, x,x121 );
         co[0].loadv( i+1,j+1,k  ,t, x,x221 );
                                           
         co[0].loadv( i-1,j-1,k+1,t, x,x002 );
         co[0].loadv( i  ,j-1,k+1,t, x,x102 );
         co[0].loadv( i+1,j-1,k+1,t, x,x202 );
                                           
         co[0].loadv( i-1,j  ,k+1,t, x,x012 );
         co[0].loadv( i  ,j  ,k+1,t, x,x112 );
         co[0].loadv( i+1,j  ,k+1,t, x,x212 );
                                           
         co[0].loadv( i-1,j+1,k+1,t, x,x022 );
         co[0].loadv( i  ,j+1,k+1,t, x,x122 );
         co[0].loadv( i+1,j+1,k+1,t, x,x222 );

         co[0].loadv( i  ,j  ,k  ,t, p,q[4] );

         setp3(  x000,  x100,  x200,  x010,  x110,  x210,  x020,  x120,  x220,
                 x001,  x101,  x201,  x011,  x111,  x211,  x021,  x121,  x221,
                 x002,  x102,  x202,  x012,  x112,  x212,  x022,  x122,  x222,  q[4],  dp, 0,0,1, hx[t].d[4][0], hx[t].d[4][1], hx[t].d[4][2], dst );

         res+= fabs( dp[0] );
         res+= fabs( dp[1] );
         res+= fabs( dp[2] );

         q[4][0]+= omega*dp[0];
         q[4][1]+= omega*dp[1];
         q[4][2]+= omega*dp[2];

         i=bsize-1;
         j=1;
         k=bsize-1;

         co[0].loadv( i-1,j-1,k-1,t, x,x000 );
         co[0].loadv( i  ,j-1,k-1,t, x,x100 );
         co[0].loadv( i+1,j-1,k-1,t, x,x200 );
                                           
         co[0].loadv( i-1,j  ,k-1,t, x,x010 );
         co[0].loadv( i  ,j  ,k-1,t, x,x110 );
         co[0].loadv( i+1,j  ,k-1,t, x,x210 );
                                           
         co[0].loadv( i-1,j+1,k-1,t, x,x020 );
         co[0].loadv( i  ,j+1,k-1,t, x,x120 );
         co[0].loadv( i+1,j+1,k-1,t, x,x220 );
                                           
         co[0].loadv( i-1,j-1,k  ,t, x,x001 );
         co[0].loadv( i  ,j-1,k  ,t, x,x101 );
         co[0].loadv( i+1,j-1,k  ,t, x,x201 );
                                           
         co[0].loadv( i-1,j  ,k  ,t, x,x011 );
         co[0].loadv( i  ,j  ,k  ,t, x,x111 );
         co[0].loadv( i+1,j  ,k  ,t, x,x211 );
                                           
         co[0].loadv( i-1,j+1,k  ,t, x,x021 );
         co[0].loadv( i  ,j+1,k  ,t, x,x121 );
         co[0].loadv( i+1,j+1,k  ,t, x,x221 );
                                           
         co[0].loadv( i-1,j-1,k+1,t, x,x002 );
         co[0].loadv( i  ,j-1,k+1,t, x,x102 );
         co[0].loadv( i+1,j-1,k+1,t, x,x202 );
                                           
         co[0].loadv( i-1,j  ,k+1,t, x,x012 );
         co[0].loadv( i  ,j  ,k+1,t, x,x112 );
         co[0].loadv( i+1,j  ,k+1,t, x,x212 );
                                           
         co[0].loadv( i-1,j+1,k+1,t, x,x022 );
         co[0].loadv( i  ,j+1,k+1,t, x,x122 );
         co[0].loadv( i+1,j+1,k+1,t, x,x222 );

         co[0].loadv( i  ,j  ,k  ,t, p,q[5] );

         setp3(  x000,  x100,  x200,  x010,  x110,  x210,  x020,  x120,  x220,
                 x001,  x101,  x201,  x011,  x111,  x211,  x021,  x121,  x221,
                 x002,  x102,  x202,  x012,  x112,  x212,  x022,  x122,  x222,  q[5],  dp, 1,0,1, hx[t].d[5][0], hx[t].d[5][1], hx[t].d[5][2], dst );

         res+= fabs( dp[0] );
         res+= fabs( dp[1] );
         res+= fabs( dp[2] );

         q[5][0]+= omega*dp[0];
         q[5][1]+= omega*dp[1];
         q[5][2]+= omega*dp[2];

         i=1;
         j=bsize-1;
         k=bsize-1;

         co[0].loadv( i-1,j-1,k-1,t, x,x000 );
         co[0].loadv( i  ,j-1,k-1,t, x,x100 );
         co[0].loadv( i+1,j-1,k-1,t, x,x200 );
                                           
         co[0].loadv( i-1,j  ,k-1,t, x,x010 );
         co[0].loadv( i  ,j  ,k-1,t, x,x110 );
         co[0].loadv( i+1,j  ,k-1,t, x,x210 );
                                           
         co[0].loadv( i-1,j+1,k-1,t, x,x020 );
         co[0].loadv( i  ,j+1,k-1,t, x,x120 );
         co[0].loadv( i+1,j+1,k-1,t, x,x220 );
                                           
         co[0].loadv( i-1,j-1,k  ,t, x,x001 );
         co[0].loadv( i  ,j-1,k  ,t, x,x101 );
         co[0].loadv( i+1,j-1,k  ,t, x,x201 );
                                           
         co[0].loadv( i-1,j  ,k  ,t, x,x011 );
         co[0].loadv( i  ,j  ,k  ,t, x,x111 );
         co[0].loadv( i+1,j  ,k  ,t, x,x211 );
                                           
         co[0].loadv( i-1,j+1,k  ,t, x,x021 );
         co[0].loadv( i  ,j+1,k  ,t, x,x121 );
         co[0].loadv( i+1,j+1,k  ,t, x,x221 );
                                           
         co[0].loadv( i-1,j-1,k+1,t, x,x002 );
         co[0].loadv( i  ,j-1,k+1,t, x,x102 );
         co[0].loadv( i+1,j-1,k+1,t, x,x202 );
                                           
         co[0].loadv( i-1,j  ,k+1,t, x,x012 );
         co[0].loadv( i  ,j  ,k+1,t, x,x112 );
         co[0].loadv( i+1,j  ,k+1,t, x,x212 );
                                           
         co[0].loadv( i-1,j+1,k+1,t, x,x022 );
         co[0].loadv( i  ,j+1,k+1,t, x,x122 );
         co[0].loadv( i+1,j+1,k+1,t, x,x222 );

         co[0].loadv( i  ,j  ,k  ,t, p,q[6] );

         setp3(  x000,  x100,  x200,  x010,  x110,  x210,  x020,  x120,  x220,
                 x001,  x101,  x201,  x011,  x111,  x211,  x021,  x121,  x221,
                 x002,  x102,  x202,  x012,  x112,  x212,  x022,  x122,  x222,  q[6],  dp, 0,1,1, hx[t].d[6][0], hx[t].d[6][1], hx[t].d[6][2], dst );

         res+= fabs( dp[0] );
         res+= fabs( dp[1] );
         res+= fabs( dp[2] );

         q[6][0]+= omega*dp[0];
         q[6][1]+= omega*dp[1];
         q[6][2]+= omega*dp[2];

         i=bsize-1;
         j=bsize-1;
         k=bsize-1;

         co[0].loadv( i-1,j-1,k-1,t, x,x000 );
         co[0].loadv( i  ,j-1,k-1,t, x,x100 );
         co[0].loadv( i+1,j-1,k-1,t, x,x200 );
                                           
         co[0].loadv( i-1,j  ,k-1,t, x,x010 );
         co[0].loadv( i  ,j  ,k-1,t, x,x110 );
         co[0].loadv( i+1,j  ,k-1,t, x,x210 );
                                           
         co[0].loadv( i-1,j+1,k-1,t, x,x020 );
         co[0].loadv( i  ,j+1,k-1,t, x,x120 );
         co[0].loadv( i+1,j+1,k-1,t, x,x220 );
                                           
         co[0].loadv( i-1,j-1,k  ,t, x,x001 );
         co[0].loadv( i  ,j-1,k  ,t, x,x101 );
         co[0].loadv( i+1,j-1,k  ,t, x,x201 );
                                           
         co[0].loadv( i-1,j  ,k  ,t, x,x011 );
         co[0].loadv( i  ,j  ,k  ,t, x,x111 );
         co[0].loadv( i+1,j  ,k  ,t, x,x211 );
                                           
         co[0].loadv( i-1,j+1,k  ,t, x,x021 );
         co[0].loadv( i  ,j+1,k  ,t, x,x121 );
         co[0].loadv( i+1,j+1,k  ,t, x,x221 );
                                           
         co[0].loadv( i-1,j-1,k+1,t, x,x002 );
         co[0].loadv( i  ,j-1,k+1,t, x,x102 );
         co[0].loadv( i+1,j-1,k+1,t, x,x202 );
                                           
         co[0].loadv( i-1,j  ,k+1,t, x,x012 );
         co[0].loadv( i  ,j  ,k+1,t, x,x112 );
         co[0].loadv( i+1,j  ,k+1,t, x,x212 );
                                           
         co[0].loadv( i-1,j+1,k+1,t, x,x022 );
         co[0].loadv( i  ,j+1,k+1,t, x,x122 );
         co[0].loadv( i+1,j+1,k+1,t, x,x222 );

         co[0].loadv( i  ,j  ,k  ,t, p,q[7] );

         setp3(  x000,  x100,  x200,  x010,  x110,  x210,  x020,  x120,  x220,
                 x001,  x101,  x201,  x011,  x111,  x211,  x021,  x121,  x221,
                 x002,  x102,  x202,  x012,  x112,  x212,  x022,  x122,  x222,  q[7],  dp, 1,1,1, hx[t].d[7][0], hx[t].d[7][1], hx[t].d[7][2], dst );

         res+= fabs( dp[0] );
         res+= fabs( dp[1] );
         res+= fabs( dp[2] );

         q[7][0]+= omega*dp[0];
         q[7][1]+= omega*dp[1];
         q[7][2]+= omega*dp[2];


         for( k=1;k<bsize;k++ )
        {
            for( j=1;j<bsize;j++ )
           {
               for( i=1;i<bsize;i++ )
              {
                  u= (REAL_)(i-1)/(bsize-2);     
                  v= (REAL_)(j-1)/(bsize-2);     
                  w= (REAL_)(k-1)/(bsize-2);     

                  f[0]= (1-u)*(1-v)*(1-w)*q[0][0]+
                           u *(1-v)*(1-w)*q[1][0]+
                        (1-u)*   v *(1-w)*q[2][0]+
                           u *   v *(1-w)*q[3][0]+
                        (1-u)*(1-v)*   w *q[4][0]+
                           u *(1-v)*   w *q[5][0]+
                        (1-u)*   v *   w *q[6][0]+
                           u *   v *   w *q[7][0];

                  f[1]= (1-u)*(1-v)*(1-w)*q[0][1]+
                           u *(1-v)*(1-w)*q[1][1]+
                        (1-u)*   v *(1-w)*q[2][1]+
                           u *   v *(1-w)*q[3][1]+
                        (1-u)*(1-v)*   w *q[4][1]+
                           u *(1-v)*   w *q[5][1]+
                        (1-u)*   v *   w *q[6][1]+
                           u *   v *   w *q[7][1];

                  f[2]= (1-u)*(1-v)*(1-w)*q[0][2]+
                           u *(1-v)*(1-w)*q[1][2]+
                        (1-u)*   v *(1-w)*q[2][2]+
                           u *   v *(1-w)*q[3][2]+
                        (1-u)*(1-v)*   w *q[4][2]+
                           u *(1-v)*   w *q[5][2]+
                        (1-u)*   v *   w *q[6][2]+
                           u *   v *   w *q[7][2];

                  co[0].storev( i  ,j  ,k  ,t, p,f );

              }
           }
        }

     }

      return;
  }    
