
#  include <q3x/q3x.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

#  include <q3x/inline/orth>
#  include <q3x/inline/stra>
#  include <q3x/inline/leng>

   void q3x_t::ort0( REAL_ *y, REAL_ *dy, REAL_ *x, REAL_ *r, REAL_ *g )
  {

      INT_       ntsk;
      INT_       tasks[NTASK];

      INT_       b,c,d;
      INT_       i,j,k, l,m,n, s,t;
      INT_       ist,ien;
      frule_t    r0,r1;
      frule_t    rr;
      INT_       num,loc;
      INT_      *idx;

      REAL_3     x000,x100, x010,x110;
      REAL_3     x001,x101, x011,x111;

      REAL_      a;
      REAL_      l0=0.008;


      REAL_3     f[3];
      REAL_9     df[3];


// update loop

      REAL_3     wrk0[1024];
      REAL_9     wrk1[1024];

      assert( bsize < 1024 );

      co[3].memset(   r,0 );
      ca[3].memset(   g,0 );

      cb.memset(   dy,0 );

// Boundary loop

      ien= 0;
      for( b=0;b<BTYPES;b++ )
     {
         ist= ien;
         ien= bn.l[b][0];

         for( t=ist;t<ien;t++ )
        {


            r0= frule_t( bn.f[t], bsize+1,bsize+1,bsize+1 );
            c= bn.h[t];
            s= bn.s[t];

            if( c > -1 )
           {

               k= 0;
               for( j=0;j<bsize;j++ )
              {
                  for( i=0;i<bsize;i++ )
                 {
                     r0.transform( i  ,j  ,0, l,m,n  ); co[0].loadv( l,m,n,c, x,x000 );
                     r0.transform( i+1,j  ,0, l,m,n  ); co[0].loadv( l,m,n,c, x,x100 );

                     r0.transform( i  ,j+1,0, l,m,n  ); co[0].loadv( l,m,n,c, x,x010 );
                     r0.transform( i+1,j+1,0, l,m,n  ); co[0].loadv( l,m,n,c, x,x110 );

                     orth( x010, x000, x100, a, f, df ); 
                     r0.transform( i,  j+1,0, l,m,n ); co[0].addv( l,m,n,c, r,f[0] ); ca[0].addv( l,m,n,c ,g,df[0] ); 
                     r0.transform( i,  j  ,0, l,m,n ); co[0].addv( l,m,n,c, r,f[1] ); ca[0].addv( l,m,n,c ,g,df[1] ); 
                     r0.transform( i+1,j  ,0, l,m,n ); co[0].addv( l,m,n,c, r,f[2] ); ca[0].addv( l,m,n,c ,g,df[2] );

                     orth( x000, x100, x110, a, f, df ); 
                     r0.transform( i,  j  ,0, l,m,n ); co[0].addv( l,m,n,c, r,f[0] ); ca[0].addv( l,m,n,c ,g,df[0] ); 
                     r0.transform( i+1,j  ,0, l,m,n ); co[0].addv( l,m,n,c, r,f[1] ); ca[0].addv( l,m,n,c ,g,df[1] );
                     r0.transform( i+1,j+1,0, l,m,n ); co[0].addv( l,m,n,c, r,f[2] ); ca[0].addv( l,m,n,c ,g,df[2] ); 

                     orth( x100, x110, x010, a, f, df ); 
                     r0.transform( i+1,j  ,0, l,m,n ); co[0].addv( l,m,n,c, r,f[0] ); ca[0].addv( l,m,n,c ,g,df[0] );
                     r0.transform( i+1,j+1,0, l,m,n ); co[0].addv( l,m,n,c, r,f[1] ); ca[0].addv( l,m,n,c ,g,df[1] ); 
                     r0.transform( i,  j+1,0, l,m,n ); co[0].addv( l,m,n,c, r,f[2] ); ca[0].addv( l,m,n,c ,g,df[2] ); 

                     orth( x110, x010, x000, a, f, df ); 
                     r0.transform( i+1,j+1,0, l,m,n ); co[0].addv( l,m,n,c, r,f[0] ); ca[0].addv( l,m,n,c ,g,df[0] ); 
                     r0.transform( i,  j+1,0, l,m,n ); co[0].addv( l,m,n,c, r,f[1] ); ca[0].addv( l,m,n,c ,g,df[1] ); 
                     r0.transform( i  ,j  ,0, l,m,n ); co[0].addv( l,m,n,c, r,f[2] ); ca[0].addv( l,m,n,c ,g,df[2] );

                     leng( x000,x100, l0, a,f,df );
                     r0.transform( i,  j  ,0, l,m,n ); co[0].addv( l,m,n,c, r,f[0] ); ca[0].addv( l,m,n,c ,g,df[0] ); 
                     r0.transform( i+1,j  ,0, l,m,n ); co[0].addv( l,m,n,c, r,f[1] ); ca[0].addv( l,m,n,c ,g,df[1] );

                     leng( x100,x110, l0, a,f,df );
                     r0.transform( i+1,j  ,0, l,m,n ); co[0].addv( l,m,n,c, r,f[0] ); ca[0].addv( l,m,n,c ,g,df[0] ); 
                     r0.transform( i+1,j+1,0, l,m,n ); co[0].addv( l,m,n,c, r,f[1] ); ca[0].addv( l,m,n,c ,g,df[1] );

                     leng( x110,x010, l0, a,f,df );
                     r0.transform( i+1,j+1,0, l,m,n ); co[0].addv( l,m,n,c, r,f[0] ); ca[0].addv( l,m,n,c ,g,df[0] ); 
                     r0.transform( i  ,j+1,0, l,m,n ); co[0].addv( l,m,n,c, r,f[1] ); ca[0].addv( l,m,n,c ,g,df[1] );

                     leng( x010,x000, l0, a,f,df );
                     r0.transform( i  ,j+1,0, l,m,n ); co[0].addv( l,m,n,c, r,f[0] ); ca[0].addv( l,m,n,c ,g,df[0] );
                     r0.transform( i  ,j  ,0, l,m,n ); co[0].addv( l,m,n,c, r,f[1] ); ca[0].addv( l,m,n,c ,g,df[1] ); 

             
                 }
              }
           }
        }
     }

      for( t=0;t<hx.n;t++ )
     {

         for( k=0;k<bsize;k++ )
        {
            for( j=0;j<bsize;j++ )
           {
               for( i=0;i<bsize;i++ )
              {
                  co[0].loadv( i  ,j  ,k  ,t, x,x000 );
                  co[0].loadv( i+1,j  ,k  ,t, x,x100 );
                                                    
                  co[0].loadv( i  ,j+1,k  ,t, x,x010 );
                  co[0].loadv( i+1,j+1,k  ,t, x,x110 );
                                                    
                  co[0].loadv( i  ,j  ,k+1,t, x,x001 );
                  co[0].loadv( i+1,j  ,k+1,t, x,x101 );

                  co[0].loadv( i  ,j+1,k+1,t, x,x011 );
                  co[0].loadv( i+1,j+1,k+1,t, x,x111 );

// 0

                  orth( x010, x000, x100, a, f, df ); 
                  co[0].addv( i  ,j+1,k  , t,r, f[0] ); ca[0].addv( i  ,j+1,k  , t,g, df[0] ); 
                  co[0].addv( i  ,j  ,k  , t,r, f[1] ); ca[0].addv( i  ,j  ,k  , t,g, df[1] ); 
                  co[0].addv( i+1,j  ,k  , t,r, f[2] ); ca[0].addv( i+1,j  ,k  , t,g, df[2] );

                  orth( x010, x000, x001, a, f, df ); 
                  co[0].addv( i  ,j+1,k  , t,r, f[0] ); ca[0].addv( i  ,j+1,k  , t,g, df[0] ); 
                  co[0].addv( i  ,j  ,k  , t,r, f[1] ); ca[0].addv( i  ,j  ,k  , t,g, df[1] ); 
                  co[0].addv( i  ,j  ,k+1, t,r, f[2] ); ca[0].addv( i  ,j  ,k+1, t,g, df[2] );

                  orth( x001, x000, x100, a, f, df ); 
                  co[0].addv( i  ,j  ,k+1, t,r, f[0] ); ca[0].addv( i  ,j  ,k+1, t,g, df[0] ); 
                  co[0].addv( i  ,j  ,k  , t,r, f[1] ); ca[0].addv( i  ,j  ,k  , t,g, df[1] ); 
                  co[0].addv( i+1,j  ,k  , t,r, f[2] ); ca[0].addv( i+1,j  ,k  , t,g, df[2] );

// 1

                  orth( x000, x100, x110, a, f, df ); 
                  co[0].addv( i  ,j  ,k  , t,r, f[0] ); ca[0].addv( i  ,j  ,k  , t,g, df[0] ); 
                  co[0].addv( i+1,j  ,k  , t,r, f[1] ); ca[0].addv( i+1,j  ,k  , t,g, df[1] ); 
                  co[0].addv( i+1,j+1,k  , t,r, f[2] ); ca[0].addv( i+1,j+1,k  , t,g, df[2] );

                  orth( x000, x100, x101, a, f, df ); 
                  co[0].addv( i  ,j  ,k  , t,r, f[0] ); ca[0].addv( i  ,j  ,k  , t,g, df[0] ); 
                  co[0].addv( i+1,j  ,k  , t,r, f[1] ); ca[0].addv( i+1,j  ,k  , t,g, df[1] ); 
                  co[0].addv( i+1,j  ,k+1, t,r, f[2] ); ca[0].addv( i+1,j  ,k+1, t,g, df[2] );

                  orth( x101, x100, x110, a, f, df ); 
                  co[0].addv( i+1,j  ,k+1, t,r, f[0] ); ca[0].addv( i+1,j  ,k+1, t,g, df[0] ); 
                  co[0].addv( i+1,j  ,k  , t,r, f[1] ); ca[0].addv( i+1,j  ,k  , t,g, df[1] ); 
                  co[0].addv( i+1,j+1,k  , t,r, f[2] ); ca[0].addv( i+1,j+1,k  , t,g, df[2] );

// 2

                  orth( x000, x010, x110, a, f, df ); 
                  co[0].addv( i  ,j  ,k  , t,r, f[0] ); ca[0].addv( i  ,j  ,k  , t,g, df[0] ); 
                  co[0].addv( i  ,j+1,k  , t,r, f[1] ); ca[0].addv( i  ,j+1,k  , t,g, df[1] ); 
                  co[0].addv( i+1,j+1,k  , t,r, f[2] ); ca[0].addv( i+1,j+1,k  , t,g, df[2] );

                  orth( x000, x010, x011, a, f, df ); 
                  co[0].addv( i  ,j  ,k  , t,r, f[0] ); ca[0].addv( i  ,j  ,k  , t,g, df[0] ); 
                  co[0].addv( i  ,j+1,k  , t,r, f[1] ); ca[0].addv( i  ,j+1,k  , t,g, df[1] ); 
                  co[0].addv( i  ,j+1,k+1, t,r, f[2] ); ca[0].addv( i  ,j+1,k+1, t,g, df[2] );

                  orth( x011, x010, x110, a, f, df ); 
                  co[0].addv( i  ,j+1,k+1, t,r, f[0] ); ca[0].addv( i  ,j+1,k+1, t,g, df[0] ); 
                  co[0].addv( i  ,j+1,k  , t,r, f[1] ); ca[0].addv( i  ,j+1,k  , t,g, df[1] ); 
                  co[0].addv( i+1,j+1,k  , t,r, f[2] ); ca[0].addv( i+1,j+1,k  , t,g, df[2] );

// 3

                  orth( x010, x110, x100, a, f, df ); 
                  co[0].addv( i  ,j+1,k  , t,r, f[0] ); ca[0].addv( i  ,j+1,k  , t,g, df[0] ); 
                  co[0].addv( i+1,j+1,k  , t,r, f[1] ); ca[0].addv( i+1,j+1,k  , t,g, df[1] ); 
                  co[0].addv( i+1,j  ,k  , t,r, f[2] ); ca[0].addv( i+1,j  ,k  , t,g, df[2] );

                  orth( x010, x110, x111, a, f, df ); 
                  co[0].addv( i  ,j+1,k  , t,r, f[0] ); ca[0].addv( i  ,j+1,k  , t,g, df[0] ); 
                  co[0].addv( i+1,j+1,k  , t,r, f[1] ); ca[0].addv( i+1,j+1,k  , t,g, df[1] ); 
                  co[0].addv( i+1,j+1,k+1, t,r, f[2] ); ca[0].addv( i+1,j+1,k+1, t,g, df[2] );

                  orth( x111, x110, x100, a, f, df ); 
                  co[0].addv( i+1,j+1,k+1, t,r, f[0] ); ca[0].addv( i+1,j+1,k+1, t,g, df[0] ); 
                  co[0].addv( i+1,j+1,k  , t,r, f[1] ); ca[0].addv( i+1,j+1,k  , t,g, df[1] ); 
                  co[0].addv( i+1,j  ,k  , t,r, f[2] ); ca[0].addv( i+1,j  ,k  , t,g, df[2] );

// 4

                  orth( x011, x001, x101, a, f, df ); 
                  co[0].addv( i  ,j+1,k+1, t,r, f[0] ); ca[0].addv( i  ,j+1,k+1, t,g, df[0] ); 
                  co[0].addv( i  ,j  ,k+1, t,r, f[1] ); ca[0].addv( i  ,j  ,k+1, t,g, df[1] ); 
                  co[0].addv( i+1,j  ,k+1, t,r, f[2] ); ca[0].addv( i+1,j  ,k+1, t,g, df[2] );

                  orth( x011, x001, x000, a, f, df ); 
                  co[0].addv( i  ,j+1,k+1, t,r, f[0] ); ca[0].addv( i  ,j+1,k+1, t,g, df[0] ); 
                  co[0].addv( i  ,j  ,k+1, t,r, f[1] ); ca[0].addv( i  ,j  ,k+1, t,g, df[1] ); 
                  co[0].addv( i  ,j  ,k  , t,r, f[2] ); ca[0].addv( i  ,j  ,k  , t,g, df[2] );

                  orth( x000, x001, x101, a, f, df ); 
                  co[0].addv( i  ,j  ,k  , t,r, f[0] ); ca[0].addv( i  ,j  ,k  , t,g, df[0] ); 
                  co[0].addv( i  ,j  ,k+1, t,r, f[1] ); ca[0].addv( i  ,j  ,k+1, t,g, df[1] ); 
                  co[0].addv( i+1,j  ,k+1, t,r, f[2] ); ca[0].addv( i+1,j  ,k+1, t,g, df[2] );
                                        
// 5

                  orth( x001, x101, x111, a, f, df ); 
                  co[0].addv( i  ,j  ,k+1, t,r, f[0] ); ca[0].addv( i  ,j  ,k+1, t,g, df[0] ); 
                  co[0].addv( i+1,j  ,k+1, t,r, f[1] ); ca[0].addv( i+1,j  ,k+1, t,g, df[1] ); 
                  co[0].addv( i+1,j+1,k+1, t,r, f[2] ); ca[0].addv( i+1,j+1,k+1, t,g, df[2] );

                  orth( x001, x101, x100, a, f, df ); 
                  co[0].addv( i  ,j  ,k+1, t,r, f[0] ); ca[0].addv( i  ,j  ,k+1, t,g, df[0] ); 
                  co[0].addv( i+1,j  ,k+1, t,r, f[1] ); ca[0].addv( i+1,j  ,k+1, t,g, df[1] ); 
                  co[0].addv( i+1,j  ,k  , t,r, f[2] ); ca[0].addv( i+1,j  ,k  , t,g, df[2] );

                  orth( x100, x101, x111, a, f, df ); 
                  co[0].addv( i+1,j  ,k  , t,r, f[0] ); ca[0].addv( i+1,j  ,k  , t,g, df[0] ); 
                  co[0].addv( i+1,j  ,k+1, t,r, f[1] ); ca[0].addv( i+1,j  ,k+1, t,g, df[1] ); 
                  co[0].addv( i+1,j+1,k+1, t,r, f[2] ); ca[0].addv( i+1,j+1,k+1, t,g, df[2] );

// 6

                                        
                  orth( x001, x011, x111, a, f, df ); 
                  co[0].addv( i  ,j  ,k+1, t,r, f[0] ); ca[0].addv( i  ,j  ,k+1, t,g, df[0] ); 
                  co[0].addv( i  ,j+1,k+1, t,r, f[1] ); ca[0].addv( i  ,j+1,k+1, t,g, df[1] ); 
                  co[0].addv( i+1,j+1,k+1, t,r, f[2] ); ca[0].addv( i+1,j+1,k+1, t,g, df[2] );

                  orth( x001, x011, x010, a, f, df ); 
                  co[0].addv( i  ,j  ,k+1, t,r, f[0] ); ca[0].addv( i  ,j  ,k+1, t,g, df[0] ); 
                  co[0].addv( i  ,j+1,k+1, t,r, f[1] ); ca[0].addv( i  ,j+1,k+1, t,g, df[1] ); 
                  co[0].addv( i  ,j+1,k  , t,r, f[2] ); ca[0].addv( i  ,j+1,k  , t,g, df[2] );

                  orth( x010, x011, x111, a, f, df ); 
                  co[0].addv( i  ,j+1,k  , t,r, f[0] ); ca[0].addv( i  ,j+1,k  , t,g, df[0] ); 
                  co[0].addv( i  ,j+1,k+1, t,r, f[1] ); ca[0].addv( i  ,j+1,k+1, t,g, df[1] ); 
                  co[0].addv( i+1,j+1,k+1, t,r, f[2] ); ca[0].addv( i+1,j+1,k+1, t,g, df[2] );

// 7
                                        
                  orth( x011, x111, x101, a, f, df ); 
                  co[0].addv( i  ,j+1,k+1, t,r, f[0] ); ca[0].addv( i  ,j+1,k+1, t,g, df[0] ); 
                  co[0].addv( i+1,j+1,k+1, t,r, f[1] ); ca[0].addv( i+1,j+1,k+1, t,g, df[1] ); 
                  co[0].addv( i+1,j  ,k+1, t,r, f[2] ); ca[0].addv( i+1,j  ,k+1, t,g, df[2] );

                  orth( x011, x111, x110, a, f, df ); 
                  co[0].addv( i  ,j+1,k+1, t,r, f[0] ); ca[0].addv( i  ,j+1,k+1, t,g, df[0] ); 
                  co[0].addv( i+1,j+1,k+1, t,r, f[1] ); ca[0].addv( i+1,j+1,k+1, t,g, df[1] ); 
                  co[0].addv( i+1,j+1,k  , t,r, f[2] ); ca[0].addv( i+1,j+1,k  , t,g, df[2] );

                  orth( x110, x111, x101, a, f, df ); 
                  co[0].addv( i+1,j+1,k  , t,r, f[0] ); ca[0].addv( i+1,j+1,k  , t,g, df[0] ); 
                  co[0].addv( i+1,j+1,k+1, t,r, f[1] ); ca[0].addv( i+1,j+1,k+1, t,g, df[1] ); 
                  co[0].addv( i+1,j  ,k+1, t,r, f[2] ); ca[0].addv( i+1,j  ,k+1, t,g, df[2] );
                  

                  leng( x000,x100, l0, a,f,df );
                  co[0].addv(  i,  j  ,k,  t, r,f[0] ); ca[0].addv( i,  j  ,k,   t,g,df[0] ); 
                  co[0].addv(  i+1,j  ,k,  t, r,f[1] ); ca[0].addv( i+1,j  ,k,   t,g,df[1] );

                  leng( x100,x110, l0, a,f,df );
                  co[0].addv(  i+1,j  ,k,  t, r,f[0] ); ca[0].addv( i+1,j  ,k,   t,g,df[0] ); 
                  co[0].addv(  i+1,j+1,k,  t, r,f[1] ); ca[0].addv( i+1,j+1,k,   t,g,df[1] );

                  leng( x110,x010, l0, a,f,df );
                  co[0].addv(  i+1,j+1,k,  t, r,f[0] ); ca[0].addv( i+1,j+1,k,   t,g,df[0] ); 
                  co[0].addv(  i  ,j+1,k,  t, r,f[1] ); ca[0].addv( i  ,j+1,k,   t,g,df[1] );

                  leng( x010,x000, l0, a,f,df );
                  co[0].addv(  i  ,j+1,k,  t, r,f[0] ); ca[0].addv( i  ,j+1,k,   t,g,df[0] );
                  co[0].addv(  i  ,j  ,k,  t, r,f[1] ); ca[0].addv( i  ,j  ,k,   t,g,df[1] ); 

                  leng( x001,x101, l0, a,f,df );
                  co[0].addv(  i,  j  ,k+1,t, r,f[0] ); ca[0].addv( i,  j  ,k+1, t,g,df[0] ); 
                  co[0].addv(  i+1,j  ,k+1,t, r,f[1] ); ca[0].addv( i+1,j  ,k+1, t,g,df[1] );

                  leng( x101,x111, l0, a,f,df );
                  co[0].addv(  i+1,j  ,k+1,t, r,f[0] ); ca[0].addv( i+1,j  ,k+1, t,g,df[0] ); 
                  co[0].addv(  i+1,j+1,k+1,t, r,f[1] ); ca[0].addv( i+1,j+1,k+1, t,g,df[1] );

                  leng( x111,x011, l0, a,f,df );
                  co[0].addv(  i+1,j+1,k+1,t, r,f[0] ); ca[0].addv( i+1,j+1,k+1, t,g,df[0] ); 
                  co[0].addv(  i  ,j+1,k+1,t, r,f[1] ); ca[0].addv( i  ,j+1,k+1, t,g,df[1] );

                  leng( x011,x001, l0, a,f,df );
                  co[0].addv(  i  ,j+1,k+1,t, r,f[0] ); ca[0].addv( i  ,j+1,k+1, t,g,df[0] );
                  co[0].addv(  i  ,j  ,k+1,t, r,f[1] ); ca[0].addv( i  ,j  ,k+1, t,g,df[1] ); 

                  leng( x000,x001, l0, a,f,df );
                  co[0].addv(  i,  j  ,k,  t, r,f[0] ); ca[0].addv( i,  j  ,k,   t,g,df[0] ); 
                  co[0].addv(  i,  j  ,k+1,t, r,f[1] ); ca[0].addv( i,  j  ,k+1, t,g,df[1] ); 

                  leng( x100,x101, l0, a,f,df );
                  co[0].addv(  i+1,j  ,k,  t, r,f[0] ); ca[0].addv( i+1,j  ,k,   t,g,df[0] ); 
                  co[0].addv(  i+1,j  ,k+1,t, r,f[1] ); ca[0].addv( i+1,j  ,k+1, t,g,df[1] ); 

                  leng( x110,x111, l0, a,f,df );
                  co[0].addv(  i+1,j+1,k,  t, r,f[0] ); ca[0].addv( i+1,j+1,k,   t,g,df[0] ); 
                  co[0].addv(  i+1,j+1,k+1,t, r,f[1] ); ca[0].addv( i+1,j+1,k+1, t,g,df[1] ); 

                  leng( x010,x011, l0, a,f,df );
                  co[0].addv(  i  ,j+1,k,  t, r,f[0] ); ca[0].addv( i  ,j+1,k,   t,g,df[0] );
                  co[0].addv(  i  ,j+1,k+1,t, r,f[1] ); ca[0].addv( i  ,j+1,k+1, t,g,df[1] );
              }
           }
        }

/*       for( k=0;k<bsize+1;k++ )
        {
            for( j=0;j<bsize+1;j++ )
           {
               for( i=1;i<bsize;i++ )
              {
                  co[0].loadv( i-1,j  ,k  ,t, x,x011 );
                  co[0].loadv( i  ,j  ,k  ,t, x,x111 );
                  co[0].loadv( i+1,j  ,k  ,t, x,x211 );
                                                    
                  stra( x011, x111, x211, a, f, df ); 

                  co[0].addv( i-1,j  ,k  ,t, r,f[0] ); ca[0].addv( i-1,j  ,k  ,t, g,df[0] );
                  co[0].addv( i  ,j  ,k  ,t, r,f[1] ); ca[0].addv( i  ,j  ,k  ,t, g,df[1] );
                  co[0].addv( i+1,j  ,k  ,t, r,f[2] ); ca[0].addv( i+1,j  ,k  ,t, g,df[2] );
                  

              }
           }
        }

         for( k=0;k<bsize+1;k++ )
        {
            for( j=1;j<bsize;j++ )
           {
               for( i=0;i<bsize+1;i++ )
              {
                  co[0].loadv( i  ,j-1,k  ,t, x,x101 );
                  co[0].loadv( i  ,j  ,k  ,t, x,x111 );
                  co[0].loadv( i  ,j+1,k  ,t, x,x121 );
                                                    
                  stra( x101, x111, x121, a, f, df ); 

                  co[0].addv( i  ,j-1,k  ,t, r,f[0] ); ca[0].addv( i  ,j-1,k  ,t, g,df[0] );
                  co[0].addv( i  ,j  ,k  ,t, r,f[1] ); ca[0].addv( i  ,j  ,k  ,t, g,df[1] );
                  co[0].addv( i  ,j+1,k  ,t, r,f[2] ); ca[0].addv( i  ,j+1,k  ,t, g,df[2] );
              }
           }
        }
         for( k=1;k<bsize;k++ )
        {
            for( j=0;j<bsize+1;j++ )
           {
               for( i=0;i<bsize+1;i++ )
              {
                  co[0].loadv( i  ,j  ,k-1,t, x,x110 );
                  co[0].loadv( i  ,j  ,k  ,t, x,x111 );
                  co[0].loadv( i  ,j  ,k+1,t, x,x112 );
                                                    
                  stra( x110, x111, x112, a, f, df ); 

                  co[0].addv( i  ,j  ,k-1,t, r,f[0] ); ca[0].addv( i  ,j  ,k-1,t, g,df[0] );
                  co[0].addv( i  ,j  ,k  ,t, r,f[1] ); ca[0].addv( i  ,j  ,k  ,t, g,df[1] );
                  co[0].addv( i  ,j  ,k+1,t, r,f[2] ); ca[0].addv( i  ,j  ,k+1,t, g,df[2] );
              }
           }
        }*/
     }

      exchange(  g, ca[1], com[0], msg[4] );
      exchange(  r, co[1], com[0], msg[1] );

      exchange(  g, ca[2], com[1], msg[5] );
      exchange(  r, co[2], com[1], msg[2] );

      exchange(  g, ca[3], com[2], msg[6] );
      exchange(  r, co[3], com[2], msg[3] );

// rescale blocks by jacobian


// handle interfaces with only local data dependencies

      while( transit( com[2],msg[3], ntsk,tasks, 0 ) )
     {
         for( t=0;t<ntsk;t++ )
        {
            com[2].task( tasks[t], num,loc, &idx );

            assert( num >= loc );

            wrk0[0][0]=0;
            wrk0[0][1]=0;
            wrk0[0][2]=0;

            for( s=0;s<loc;s++ )
           { 
               com[2].taskinfo( co[0], idx[s], r0,c );
             
               r0.transform( 0,0,0, l,m,n ); co[0].loadv( l,m,n, c, r,f[0] );
               wrk0[0][0]+= f[0][0];
               wrk0[0][1]+= f[0][1];
               wrk0[0][2]+= f[0][2];

           }

            for( s=loc;s<num;s++ )
           { 
               c= idx[s];
             
               co[3].loadv( 0,0,0, c, r,f[0] );

               wrk0[0][0]+= f[0][0];
               wrk0[0][1]+= f[0][1];
               wrk0[0][2]+= f[0][2];

           }

            for( s=0;s<loc;s++ )
           { 
               com[2].taskinfo( co[0], idx[s], r0,c );
               r0.transform( 0,0,0,   l,m,n ); co[0].storev( l,m,n,c, r,wrk0[0] ); 

           }
        }
     }

      while( transit( com[2],msg[6], ntsk,tasks, 0 ) )
     {
         for( t=0;t<ntsk;t++ )
        {
            com[2].task( tasks[t], num,loc, &idx );

            assert( num >= loc );

            memset( wrk1[0],0,9*sizeof(REAL_) );

            for( s=0;s<loc;s++ )
           { 
               com[2].taskinfo( co[0], idx[s], r0,c );
             
               r0.transform( 0,0,0, l,m,n ); ca[0].loadv( l,m,n, c, g,df[1] );

               wrk1[0][0+0]+= df[1][0+0];
               wrk1[0][1+0]+= df[1][1+0];
               wrk1[0][2+0]+= df[1][2+0];
               wrk1[0][0+3]+= df[1][0+3];
               wrk1[0][1+3]+= df[1][1+3];
               wrk1[0][2+3]+= df[1][2+3];
               wrk1[0][0+6]+= df[1][0+6];
               wrk1[0][1+6]+= df[1][1+6];
               wrk1[0][2+6]+= df[1][2+6];
           }

            for( s=loc;s<num;s++ )
           { 
               c= idx[s];
             
               ca[3].loadv( 0,0,0, c, g,df[1] );

               wrk1[0][0+0]+= df[1][0+0];
               wrk1[0][1+0]+= df[1][1+0];
               wrk1[0][2+0]+= df[1][2+0];
               wrk1[0][0+3]+= df[1][0+2];
               wrk1[0][1+3]+= df[1][1+3];
               wrk1[0][2+3]+= df[1][2+3];
               wrk1[0][0+6]+= df[1][0+6];
               wrk1[0][1+6]+= df[1][1+6];
               wrk1[0][2+6]+= df[1][2+6];

           }

            for( s=0;s<loc;s++ )
           { 
               com[2].taskinfo( co[0], idx[s], r0,c );
               r0.transform( 0,0,0,   l,m,n ); ca[0].storev( l,m,n,c, g,wrk1[0] );
           }
        }
     }

      while( transit( com[1],msg[2], ntsk,tasks, 0 ) )
     {
         for( t=0;t<ntsk;t++ )
        {
            com[1].task( tasks[t], num,loc, &idx );
            assert( num >= loc );

            memset( wrk0,0,1024*sizeof(REAL_3) );

            for( s=0;s<loc;s++ )
           { 
               com[1].taskinfo( co[0], idx[s], r0,c );
          
               d= idx[s];
             
               for( i=1;i<bsize;i++ )
              {
          
                  r0.transform( i,0,0, l,m,n ); co[0].loadv( l,m,n, c, r,f[0] );

                  wrk0[i][0]+= f[0][0];
                  wrk0[i][1]+= f[0][1];
                  wrk0[i][2]+= f[0][2];

              }
           }
          
            for( s=loc;s<num;s++ )
           { 
               c= idx[s];
             
               com[1].taskinfo( co[0], idx[s], r0,b );
          
               for( i=1;i<bsize;i++ )
              {
                  co[2].loadv( i,0,0, c, r,f[0] );

                  wrk0[i][0]+= f[0][0];
                  wrk0[i][1]+= f[0][1];
                  wrk0[i][2]+= f[0][2];

              }
           }
          
            for( s=0;s<loc;s++ )
           { 
               com[1].taskinfo( co[0], idx[s], r0,c );
          
               d= idx[s];
             
               for( i=1;i<bsize;i++ )
              {
          
                  r0.transform( i,0,0, l,m,n ); co[0].storev( l,m,n, c, r,wrk0[i] );
          
              }
           }
        }
     }

      while( transit( com[1],msg[5], ntsk,tasks, 0 ) )
     {
         for( t=0;t<ntsk;t++ )
        {
            com[1].task( tasks[t], num,loc, &idx );
            assert( num >= loc );

            memset( wrk1,0,1024*sizeof(REAL_9) ); 

            for( s=0;s<loc;s++ )
           { 
               com[1].taskinfo( co[0], idx[s], r0,c );
          
               d= idx[s];
             
               for( i=1;i<bsize;i++ )
              {
          
                  r0.transform( i,0,0, l,m,n ); ca[0].loadv( l,m,n, c, g,df[1] );

                  wrk1[i][0+0]+= df[1][0+0];
                  wrk1[i][1+0]+= df[1][1+0];
                  wrk1[i][2+0]+= df[1][2+0];
                  wrk1[i][0+3]+= df[1][0+3];
                  wrk1[i][1+3]+= df[1][1+3];
                  wrk1[i][2+3]+= df[1][2+3];
                  wrk1[i][0+6]+= df[1][0+6];
                  wrk1[i][1+6]+= df[1][1+6];
                  wrk1[i][2+6]+= df[1][2+6];
          
              }
           }
          
            for( s=loc;s<num;s++ )
           { 
               c= idx[s];
             
               com[1].taskinfo( co[0], idx[s], r0,b );
          
               for( i=1;i<bsize;i++ )
              {
                  ca[2].loadv( i,0,0, c, g,df[1] );

                  wrk1[i][0+0]+= df[1][0+0];
                  wrk1[i][1+0]+= df[1][1+0];
                  wrk1[i][2+0]+= df[1][2+0];
                  wrk1[i][0+3]+= df[1][0+3];
                  wrk1[i][1+3]+= df[1][1+3];
                  wrk1[i][2+3]+= df[1][2+3];
                  wrk1[i][0+6]+= df[1][0+6];
                  wrk1[i][1+6]+= df[1][1+6];
                  wrk1[i][2+6]+= df[1][2+6];

              }
           }
          
            for( s=0;s<loc;s++ )
           { 
               com[1].taskinfo( co[0], idx[s], r0,c );
          
               d= idx[s];
             
               for( i=1;i<bsize;i++ )
              {
          
                  r0.transform( i,0,0, l,m,n ); ca[0].storev( l,m,n, c, g,wrk1[i] );
          
              }
           }
        }
     }

      while( locals( com[0],msg[1], ntsk,tasks ) )
     {
         for( t=0;t<ntsk;t++ )
        {
    
            com[0].task( tasks[t], num,loc, &idx );
            com[0].taskinfo( co[0], idx[0], r0,c );
            com[0].taskinfo( co[0], idx[1], r1,d );

            assert( num == 2 );
            assert( loc == 2 );

            for( j=1;j<bsize;j++ )
           {
               for( i=1;i<bsize;i++ )
              {

                  r1.transform( i,j,0, l,m,n );   co[0].loadv( l,m,n,d, r,f[0] );
                  r0.transform( i,j,0, l,m,n );   co[0].loadv( l,m,n,c, r,f[1] );

                  f[0][0]+= f[1][0];
                  f[0][1]+= f[1][1];
                  f[0][2]+= f[1][2];

                  r0.transform( i,j,0, l,m,n  ); co[0].storev( l,m,n,c, r,f[0] );
                  r1.transform( i,j,0, l,m,n  ); co[0].storev( l,m,n,d, r,f[0] );

              }
           }
        }
     }

      while( locals( com[0],msg[4], ntsk,tasks ) )
     {
         for( t=0;t<ntsk;t++ )
        {
    
            com[0].task( tasks[t], num,loc, &idx );
            com[0].taskinfo( co[0], idx[0], r0,c );
            com[0].taskinfo( co[0], idx[1], r1,d );

            assert( num == 2 );
            assert( loc == 2 );

            for( j=1;j<bsize;j++ )
           {
               for( i=1;i<bsize;i++ )
              {

                  r1.transform( i,j,0, l,m,n );  ca[0].loadv( l,m,n,d, g,df[0] );
                  r0.transform( i,j,0, l,m,n  ); ca[0].loadv( l,m,n,c, g,df[1] );

                  df[0][0+0]+= df[1][0+0];
                  df[0][1+0]+= df[1][1+0];
                  df[0][2+0]+= df[1][2+0];
                  df[0][0+3]+= df[1][0+3];
                  df[0][1+3]+= df[1][1+3];
                  df[0][2+3]+= df[1][2+3];
                  df[0][0+6]+= df[1][0+6];
                  df[0][1+6]+= df[1][1+6];
                  df[0][2+6]+= df[1][2+6];

                  r0.transform( i,j,0, l,m,n  ); ca[0].storev( l,m,n,c, g,df[0] );
                  r1.transform( i,j,0, l,m,n  ); ca[0].storev( l,m,n,d, g,df[0] );

              }
           }
        }
     }

// handle interfaces with data dependencies on remote ranks

      while( transit( com[0],msg[1], ntsk,tasks, 1 ) )
     {
         for( t=0;t<ntsk;t++ )
        {
    
            com[0].task( tasks[t], num,loc, &idx );
            com[0].taskinfo( co[0], idx[0], r0,c );
            b= idx[1];
    
            assert( num == 2 );
            assert( loc == 1 );
    
            for( j=1;j<bsize;j++ )
           {
               for( i=1;i<bsize;i++ )
              {

                                                  co[1].loadv( l,m,n,b, r,f[0]  );
                  r0.transform( i,j,0, l,m,n  );  co[0].loadv( l,m,n,c, r,f[1] );

                  f[0][0]+= f[1][0];
                  f[0][1]+= f[1][1];
                  f[0][2]+= f[1][2];

                  r0.transform( i,j,0, l,m,n  ); co[0].storev( l,m,n,c, r,f[0] );


              }
           }
        }
     }

      while( transit( com[0],msg[4], ntsk,tasks, 1 ) )
     {
         for( t=0;t<ntsk;t++ )
        {
    
            com[0].task( tasks[t], num,loc, &idx );
            com[0].taskinfo( co[0], idx[0], r0,c );
            b= idx[1];
    
            assert( num == 2 );
            assert( loc == 1 );
    
            for( j=1;j<bsize;j++ )
           {
               for( i=1;i<bsize;i++ )
              {

                                                  ca[1].loadv( l,m,n,b, g,df[0] );
                  r0.transform( i,j,0, l,m,n  );  ca[0].loadv( l,m,n,c, g,df[1] );

                  df[0][0+0]+= df[1][0+0];
                  df[0][1+0]+= df[1][1+0];
                  df[0][2+0]+= df[1][2+0];
                  df[0][0+3]+= df[1][0+3];
                  df[0][1+3]+= df[1][1+3];
                  df[0][2+3]+= df[1][2+3];
                  df[0][0+6]+= df[1][0+6];
                  df[0][1+6]+= df[1][1+6];
                  df[0][2+6]+= df[1][2+6];

                  r0.transform( i,j,0, l,m,n  ); ca[0].storev( l,m,n,c, g,df[0] );

              }
           }
        }
     }

      for( t=0;t<hx.n;t++ )
     {

         for( k=0;k<bsize+1;k++ )
        {
            for( j=0;j<bsize+1;j++ )
           {
               for( i=0;i<bsize+1;i++ )
              {
                  co[0].loadv( i,j,k,t, r,f[0] );
                  ca[0].loadv( i,j,k,t, g,df[0] );

/*                inv330( df[0],df[1] );
                  dot33( df[1],f[0],f[0] );*/

                  REAL_ d= abs(df[0][0])+ abs(df[0][1])+ abs(df[0][2])+
                           abs(df[0][3])+ abs(df[0][4])+ abs(df[0][5])+
                           abs(df[0][6])+ abs(df[0][7])+ abs(df[0][8]);

                  d= 1./d;
                  f[0][0]*= d;
                  f[0][1]*= d;
                  f[0][2]*= d;

                  co[0].storev( i,j,k,t, r,f[0] );
                                                    
              }
           }
        }
     }

      return;
  }


