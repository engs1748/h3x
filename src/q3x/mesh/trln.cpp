
#  include <q3x/q3x.h>
#  include <q2.h>


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::trln( INT_ n )
  {
      MMAP_      coo;
      MMAP_      cob;
      INT_         i,j,k,l;

      REAL_       *x; // coordinates
      REAL_       *p; // control functions

      REAL_       *y; // surface coordinates

      REAL_        xi[3];

      REAL_x       x0[8];
      REAL_b       y0[4];

      REAL_        w[8];


      MPI_File     fh;
      MPI_Status   status;
      MPI_Offset   offlen,off0,off1;

      size_t len=0;
      coo.dims( len, n+1,XDIM,n+1,n+1, -1 );

      x= new REAL_[len];
      p= new REAL_[len];

      MPI_File_open(MPI_COMM_WORLD, "grid/h", MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh); 
      assert( fh );

      off0=0;
      MPI_File_write_at(fh, off0, &n, 1,    INT_MPI, &status); off0+= sizeof(INT_);
      MPI_File_write_at(fh, off0, &len, 1, LONG_MPI, &status); off0+= sizeof(size_t);

      offlen=  2*len*sizeof(REAL_);

      for( l=0;l<hx.n;l++ )
     {
         coo.memset( x,0. );
         coo.memset( p,0. );
  
         for( k=0;k<8;k++ )
        {
            j= hx[l].v[k];
            x0[k][0]= vx[j][0];
            x0[k][1]= vx[j][1];
            x0[k][2]= vx[j][2];
        }


         REAL_x z;

         for( k=0; k<n+1;k++ )
        {
            for( j=0; j<n+1;j++ )
           {
               for( i=0; i<n+1;i++ )
              {
          
                  xi[0]= ((REAL_)i)/((REAL_)n);
                  xi[1]= ((REAL_)j)/((REAL_)n);
                  xi[2]= ((REAL_)k)/((REAL_)n);
            
                  w[0]= (1-xi[0])*(1-xi[1])*(1-xi[2]);
                  w[1]=    xi[0] *(1-xi[1])*(1-xi[2]);
                  w[2]= (1-xi[0])*   xi[1] *(1-xi[2]);
                  w[3]=    xi[0] *   xi[1] *(1-xi[2]);
                  w[4]= (1-xi[0])*(1-xi[1])*   xi[2] ;
                  w[5]=    xi[0] *(1-xi[1])*   xi[2] ;
                  w[6]= (1-xi[0])*   xi[1] *   xi[2] ;
                  w[7]=    xi[0] *   xi[1] *   xi[2] ;
 
                  REAL_ ww[27];
                  q222( xi[0],xi[1],xi[2], ww );

                  z[0]= w[0]*x0[0][0]+ w[1]*x0[1][0]+ w[2]*x0[2][0]+ w[3]*x0[3][0]+ w[4]*x0[4][0]+ w[5]*x0[5][0]+ w[6]*x0[6][0]+ w[7]*x0[7][0];
                  z[1]= w[0]*x0[0][1]+ w[1]*x0[1][1]+ w[2]*x0[2][1]+ w[3]*x0[3][1]+ w[4]*x0[4][1]+ w[5]*x0[5][1]+ w[6]*x0[6][1]+ w[7]*x0[7][1];
                  z[2]= w[0]*x0[0][2]+ w[1]*x0[1][2]+ w[2]*x0[2][2]+ w[3]*x0[3][2]+ w[4]*x0[4][2]+ w[5]*x0[5][2]+ w[6]*x0[6][2]+ w[7]*x0[7][2];

                  coo.storev( i,j,k, x,z );

              }
           }
        }

// now build control functions

          off1= off0+ offlen* hx[l].id;

          MPI_File_write_at(fh, off1, x, len,  REAL_MPI, &status); off1+= len*sizeof(REAL_);
          MPI_File_write_at(fh, off1, p, len,  REAL_MPI, &status);

     }
      MPI_Barrier( MPI_COMM_WORLD );
      MPI_File_close( &fh );

      delete[] x; x= NULL;
      delete[] p; p= NULL;

      len=0;
      cob.dims( len, n+1,BDIM,n+1,     -1 );
      y= new REAL_[len];

      offlen= len*sizeof(REAL_);

      MPI_File_open(MPI_COMM_WORLD, "grid/b", MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh); 
      assert( fh );

      off0= 0;
      MPI_File_write_at(fh, off0, &n, 1,    INT_MPI, &status); off0+= sizeof(INT_);
      MPI_File_write_at(fh, off0, &len, 1, LONG_MPI, &status); off0+= sizeof(size_t);

      for( INT_ p=0;p<BTYPES;p++ )
     {
         for( INT_ g=0;g<nb;g++ )
        {
            if( bz[g].lbl[0]== p && bz[g].bo.n > 0 )
           {
               for( k=0;k<bz[g].bo.n;k++ )
              {
                  memset( y,0,len*sizeof(REAL_) );

                  INT_ h= bz[g].bo[k].h;
                  INT_ q= bz[g].bo[k].q;

                  INT_ r= hx[h].q[q].r;
                  bool o= hx[h].q[q].o;

                  INT_4 buf[2];
                  INT_4 bvf[2];
                  hx[h].pack( q, buf[0],buf[1] );
                  hx[h].npack( q, bvf[0],bvf[1] );

                  j= bz[g].bo[k].v[0]; j= bz[g].bx[j].id; assert( j == bvf[1][0] );
                  j= bz[g].bo[k].v[1]; j= bz[g].bx[j].id; assert( j == bvf[1][1] );
                  j= bz[g].bo[k].v[2]; j= bz[g].bx[j].id; assert( j == bvf[1][2] );
                  j= bz[g].bo[k].v[3]; j= bz[g].bx[j].id; assert( j == bvf[1][3] );

                  for(i=0;i<4;i++ )
                 {

                     j= mqp[o][r][i];
                     j= bz[g].bo[k].v[j];

                     y0[i][0]= bz[g].bx[j].y[0];
                     y0[i][1]= bz[g].bx[j].y[1];
                     y0[i][2]= bz[g].bx[j].y[2];
                 } 

                  REAL_b z;

                  for( j=0; j<n+1;j++ )
                 {
                     for( i=0; i<n+1;i++ )
                    {
                
                        xi[0]= ((REAL_)i)/((REAL_)n);
                        xi[1]= ((REAL_)j)/((REAL_)n);
                  
                        w[0]= (1-xi[0])*(1-xi[1]);
                        w[1]=    xi[0] *(1-xi[1]);
                        w[2]= (1-xi[0])*   xi[1] ;
                        w[3]=    xi[0] *   xi[1] ;

                        z[0]= w[0]*y0[0][0]+ w[1]*y0[1][0]+ w[2]*y0[3][0]+ w[3]*y0[2][0];
                        z[1]= w[0]*y0[0][1]+ w[1]*y0[1][1]+ w[2]*y0[3][1]+ w[3]*y0[2][1];
                        z[2]= w[0]*y0[0][2]+ w[1]*y0[1][2]+ w[2]*y0[3][2]+ w[3]*y0[2][2];

                        cob.storev( i,j, y, z );


                    }
                 }

                  MPI_File_write_at(fh, off0, y, len,  REAL_MPI, &status);
                  off0+= offlen;
              }
           }
        }
     }
      MPI_Barrier( MPI_COMM_WORLD );
      MPI_File_close( &fh );

      MPI_File_open(MPI_COMM_WORLD, "grid/i", MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);
      assert( fh );

      off0= 0;
      MPI_File_write_at(fh, off0,   &n, 1,  INT_MPI, &status); off0+= sizeof(INT_);
      MPI_File_write_at(fh, off0, &len, 1, LONG_MPI, &status); off0+= sizeof(size_t);

      for( INT_ g=0;g<bi.n;g++ )
     {
 
         for( k=0;k<bi[g].bo.n;k++ )
        {
            memset( y,0,len*sizeof(REAL_) );

            INT_ h= bi[g].bo[k].h[0];
            INT_ q= bi[g].bo[k].q[0];

            INT_ r= hx[h].q[q].r;
            bool o= hx[h].q[q].o;

            INT_4 buf[2];
            INT_4 bvf[2];
            hx[h].pack( q, buf[0],buf[1] );
            hx[h].npack( q, bvf[0],bvf[1] );

            j= bi[g].bo[k].v[0]; j= bi[g].bx[j].id; assert( j == bvf[1][0] );
            j= bi[g].bo[k].v[1]; j= bi[g].bx[j].id; assert( j == bvf[1][1] );
            j= bi[g].bo[k].v[2]; j= bi[g].bx[j].id; assert( j == bvf[1][2] );
            j= bi[g].bo[k].v[3]; j= bi[g].bx[j].id; assert( j == bvf[1][3] );

            for(i=0;i<4;i++ )
           {

               j= mqp[o][r][i];
               j= bi[g].bo[k].v[j];

               y0[i][0]= bi[g].bx[j].y[0];
               y0[i][1]= bi[g].bx[j].y[1];
               y0[i][2]= bi[g].bx[j].y[2];
           } 

            REAL_b z;

            for( j=0; j<n+1;j++ )
           {
               for( i=0; i<n+1;i++ )
              {
      
                  xi[0]= ((REAL_)i)/((REAL_)n);
                  xi[1]= ((REAL_)j)/((REAL_)n);
            
                  w[0]= (1-xi[0])*(1-xi[1]);
                  w[1]=    xi[0] *(1-xi[1]);
                  w[2]= (1-xi[0])*   xi[1] ;
                  w[3]=    xi[0] *   xi[1] ;

                  z[0]= w[0]*y0[0][0]+ w[1]*y0[1][0]+ w[2]*y0[3][0]+ w[3]*y0[2][0];
                  z[1]= w[0]*y0[0][1]+ w[1]*y0[1][1]+ w[2]*y0[3][1]+ w[3]*y0[2][1];
                  z[2]= w[0]*y0[0][2]+ w[1]*y0[1][2]+ w[2]*y0[3][2]+ w[3]*y0[2][2];

                  cob.storev( i,j, y,z );

              }
           }

            MPI_File_write_at(fh, off0, y, len, REAL_MPI, &status);
            off0+= offlen;
        }
     }
      MPI_Barrier( MPI_COMM_WORLD );
      MPI_File_close( &fh );

      delete[] y; y= NULL;

  }
