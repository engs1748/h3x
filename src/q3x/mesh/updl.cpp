
#  include <q3x/q3x.h>


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::updl( INT_ n )
  {
      MMAP_      coo;
      MMAP_      cob;
      INT_         i,j,k,l;
      REAL_       *x;
      REAL_       *y;

      REAL_x       x000, x100, x010, x110, x001, x101, x011, x111;
      REAL_b       y0[4];


      MPI_File fh; 
      MPI_Status status;
      MPI_Offset off0,off1, offlen;

      size_t len=0;
      size_t dlen;
      coo.dims( len, n+1,XDIM,n+1,n+1, -1 );

      x= new REAL_[len];

      MPI_File_open(MPI_COMM_WORLD, "grid/h", MPI_MODE_RDONLY, MPI_INFO_NULL, &fh);
      assert( fh );

      off0= 0;
      MPI_File_read_at(fh, off0,    &l, 1,  INT_MPI, &status); off0+= sizeof(INT_);
      MPI_File_read_at(fh, off0, &dlen, 1, LONG_MPI, &status); off0+= sizeof(size_t);

      assert( n == l );
      assert( dlen == len );

      offlen= dlen*sizeof(REAL_);
      offlen*=2;

      for( l=0;l<hx.n;l++ )
     {
         memset( x,0,len*sizeof(REAL_) );

         off1= off0+ offlen*hx[l].id;

         MPI_File_read_at(fh, off1, x, len, REAL_MPI, &status);

         coo.loadv(0,0,0,x,x000);
         coo.loadv(n,0,0,x,x100);
         coo.loadv(0,n,0,x,x010);
         coo.loadv(n,n,0,x,x110);
         coo.loadv(0,0,n,x,x001);
         coo.loadv(n,0,n,x,x101);
         coo.loadv(0,n,n,x,x011);
         coo.loadv(n,n,n,x,x111);
 
         i=hx[l].v[0];  vx[i][0]=x000[0];vx[i][1]=x000[1];vx[i][2]=x000[2];
         i=hx[l].v[1];  vx[i][0]=x100[0];vx[i][1]=x100[1];vx[i][2]=x100[2];
         i=hx[l].v[2];  vx[i][0]=x010[0];vx[i][1]=x010[1];vx[i][2]=x010[2];
         i=hx[l].v[3];  vx[i][0]=x110[0];vx[i][1]=x110[1];vx[i][2]=x110[2];
         i=hx[l].v[4];  vx[i][0]=x001[0];vx[i][1]=x001[1];vx[i][2]=x001[2];
         i=hx[l].v[5];  vx[i][0]=x101[0];vx[i][1]=x101[1];vx[i][2]=x101[2];
         i=hx[l].v[6];  vx[i][0]=x011[0];vx[i][1]=x011[1];vx[i][2]=x011[2];
         i=hx[l].v[7];  vx[i][0]=x111[0];vx[i][1]=x111[1];vx[i][2]=x111[2];

     }
      MPI_Barrier( MPI_COMM_WORLD );
      MPI_File_close( &fh );

      delete[] x; x= NULL;

      len=0;
      cob.dims( len, n+1,BDIM,n+1,     -1 );
      y= new REAL_[len];

      off0= 0;

      MPI_File_open(MPI_COMM_WORLD, "grid/b", MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);
      assert( fh );

      off0= 0;
      MPI_File_read_at(fh, off0, &l, 1, INT_MPI, &status); off0+= sizeof( INT_ );
      MPI_File_read_at(fh, off0, &dlen, 1, LONG_MPI, &status); off0+= sizeof( size_t );

      assert( l == n );
      assert( dlen == len );

      offlen= dlen*sizeof(REAL_);

      for( INT_ p=0;p<BTYPES;p++ )
     {
         for( INT_ g=0;g<nb;g++ )
        {
            if( bz[g].lbl[0]==p && bz[g].bo.n > 0 )
           {
               for( k=0;k<bz[g].bo.n;k++ )
              {
                  memset( y,0,len*sizeof(REAL_) );
             
                  MPI_File_read_at(fh, off0, y, dlen, REAL_MPI, &status); off0+= offlen;
             
                  cob.loadv(0,0,y,y0[0]);
                  cob.loadv(n,0,y,y0[1]);
                  cob.loadv(n,n,y,y0[2]);
                  cob.loadv(0,n,y,y0[3]);
             
                  INT_ h= bz[g].bo[k].h;
                  INT_ q= bz[g].bo[k].q;
             
                  INT_ r= hx[h].q[q].r;
                  bool o= hx[h].q[q].o;
             
                  INT_4 buf[2];
                  INT_4 bvf[2];
                  hx[h].pack( q, buf[0],buf[1] );
                  hx[h].npack( q, bvf[0],bvf[1] );
             
                  j= bz[g].bo[k].v[0]; j= bz[g].bx[j].id; assert( j == bvf[1][0] );
                  j= bz[g].bo[k].v[1]; j= bz[g].bx[j].id; assert( j == bvf[1][1] );
                  j= bz[g].bo[k].v[2]; j= bz[g].bx[j].id; assert( j == bvf[1][2] );
                  j= bz[g].bo[k].v[3]; j= bz[g].bx[j].id; assert( j == bvf[1][3] );
             
                  for(i=0;i<4;i++ )
                 {
             
                     j= mqp[o][r][i];
                     j= bz[g].bo[k].v[j];
             
                     y0[i][0]= bz[g].bx[j].y[0];
                     y0[i][1]= bz[g].bx[j].y[1];
                     y0[i][2]= bz[g].bx[j].y[2];
                 }
              }
           }
        }
     }
      MPI_Barrier( MPI_COMM_WORLD );
      MPI_File_close( &fh );


      MPI_File_open(MPI_COMM_WORLD, "grid/i", MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);

      assert( fh );

      off0= 0;
      MPI_File_read_at(fh, off0, &l, 1, INT_MPI, &status); off0+= sizeof( INT_ );
      MPI_File_read_at(fh, off0, &dlen, 1, LONG_MPI, &status); off0+= sizeof( size_t );

      assert( l == n );
      assert( dlen == len );

      offlen= dlen*sizeof(REAL_);

      for( INT_ g=0;g<bi.n;g++ )
     {
         for( k=0;k<bi[g].bo.n;k++ )
        {
            memset( y,0,len*sizeof(REAL_) );

            MPI_File_write_at(fh, off0, y, len, REAL_MPI, &status); off0+= offlen;
 
            cob.loadv(0,0,y,y0[0]);
            cob.loadv(n,0,y,y0[1]);
            cob.loadv(n,n,y,y0[2]);
            cob.loadv(0,n,y,y0[3]);
 
            INT_ h= bi[g].bo[k].h[0];
            INT_ q= bi[g].bo[k].q[0];

            INT_ r= hx[h].q[q].r;
            bool o= hx[h].q[q].o;

            INT_4 buf[2];
            INT_4 bvf[2];
            hx[h].pack( q, buf[0],buf[1] );
            hx[h].npack( q, bvf[0],bvf[1] );

            j= bi[g].bo[k].v[0]; j= bi[g].bx[j].id; assert( j == bvf[1][0] );
            j= bi[g].bo[k].v[1]; j= bi[g].bx[j].id; assert( j == bvf[1][1] );
            j= bi[g].bo[k].v[2]; j= bi[g].bx[j].id; assert( j == bvf[1][2] );
            j= bi[g].bo[k].v[3]; j= bi[g].bx[j].id; assert( j == bvf[1][3] );

            for(i=0;i<4;i++ )
           {

               j= mqp[o][r][i];
               j= bi[g].bo[k].v[j];

               y0[i][0]= bi[g].bx[j].y[0];
               y0[i][1]= bi[g].bx[j].y[1];
               y0[i][2]= bi[g].bx[j].y[2];
           }
        }
     }
      MPI_Barrier( MPI_COMM_WORLD );
      MPI_File_close( &fh );

      delete[] y; y= NULL;

  }
