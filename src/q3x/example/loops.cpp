#  include <q3x/example.h>


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void example_t::loops( REAL_ *wm, REAL_ *wf, REAL_ *wb, REAL_ *wc, REAL_ *v, REAL_ *r )
  {

      INT_    ntsk;
      INT_  tasks[16384];
      INT_    jdx[32],kdx[32];
      REAL_   w[1+3+3*8];
      REAL_   z[1+3+3*8];
      INT_    c,d, e,f,g, i,j,k, l,m,n, t;
      INT_    ist,ien;
      frule_t r0,r1;
      INT_    num,loc;
      INT_   *idx;

      memset( r,0,so[1].size()*sizeof(REAL_) );

      exchange(  v, so[1], com[0], msg[0] );

// boundary loop -find way to do with openmp
 
      ien= 0;
      for( k=0;k<BTYPES;k++ )
     {
         ist= ien;
         ien= bn.l[k][0];
         for( INT_ t=ist;t<ien;t++ )
        {
            r0= frule_t( bn.f[t], bsize,bsize,bsize );
            c= bn.h[t];

            bw.loadv(0,0,t,  wb,w );
             
            for( j=0;j<bsize;j++ )
           {
               for( i=0;i<bsize;i++ )
              {
                  r0.transform( i,j,0, l,m,n );
  
  
                  bw.loadv(i,j,t,  wb,w );
  
                  z[0]= w[3]*w[6];
                  z[1]= w[4]*w[6];
                  z[2]= w[5]*w[6];
                  z[3]= 0;
                  z[4]= 0;

//                fprintf( file,"%1d %1d %1d %1d % 9.3e % 9.3e % 9.3e % 9.3e\n", l,m,n,c, w[3],w[4],w[5],w[6] ); 
                  so[0].subv(l,m,n,c, r,z );
    
              }
           }
        }
     }

//pragma omp parallel for private(t,i,j,k,ql,qr,s,lmax)
      for( t=0;t<hx.n;t++ )
     {

         for( k=0;k<bsize;k++ )
        {
            for( j=0;j<bsize;j++ )
           {
               for( i=0;i<bsize-1;i++ )
              {
                  fw.loadv(i,j,k,t,wf,w );
                  z[0]= w[I+3]*w[I+6];
                  z[1]= w[I+4]*w[I+6];
                  z[2]= w[I+5]*w[I+6];
                  z[3]= 0;
                  z[4]= 0;

                  so[0].subv(i,  j,  k,  t,r,z );
                  so[0].addv(i+1,j,  k,  t,r,z );

              }
           }
        }
         for( k=0;k<bsize;k++ )
        {
            for( j=0;j<bsize-1;j++ )
           {
               for( i=0;i<bsize;i++ )
              {
                  fw.loadv(i,j,k,t,wf,w );
                  z[0]= w[J+3]*w[J+6];
                  z[1]= w[J+4]*w[J+6];
                  z[2]= w[J+5]*w[J+6];
                  z[3]= 0;
                  z[4]= 0;

                  so[0].subv(i,  j,  k,  t,r,z );
                  so[0].addv(i,  j+1,k,  t,r,z );

              }
           }
        }
         for( k=0;k<bsize-1;k++ )
        {
            for( j=0;j<bsize;j++ )
           {
               for( i=0;i<bsize;i++ )
              {
                  fw.loadv(i,j,k,t,wf,w );
                  z[0]= w[K+3]*w[K+6];
                  z[1]= w[K+4]*w[K+6];
                  z[2]= w[K+5]*w[K+6];
                  z[3]= 0;
                  z[4]= 0;

                  so[0].subv(i,  j,  k,  t,r,z );
                  so[0].addv(i,  j,  k+1,t,r,z );
              }
           }
        }
     }

/*    for( t=0;t<hx.n;t++ )
     {
         fprintf( file,"after inner loop \n" );
         fprintf( file," \n" );
         fprintf( file,"%2d %2d \n", t,hx[t].id );
         fprintf( file," \n" );
         for( k=0;k<bsize;k++ )
        {
            for( j=0;j<bsize;j++ )
           {
               for( i=0;i<bsize;i++ )
              {
                  so[0].loadv(i,j,k,t,r, z );
                  fprintf( file," % 9.3e % 9.3e % 9.3e \n", z[0],z[1],z[2] );

              }
           }
        }
     }*/

// handle interfaces with only local data dependencies
       
      while( locals( com[0],msg[0], ntsk,tasks ) )
     {
  
//pragma omp parallel for private(t,c,d,e,z,g,i,j,l,m,n,r0,r1,idx,num,loc,ql,qr,s,lmax)
         for( INT_ t=0;t<ntsk;t++ )
        {
    
            com[0].task( tasks[t], num,loc, &idx,jdx,kdx );
            com[0].taskinfo( so[0], idx[0], r0,c );
            com[0].taskinfo( so[0], idx[1], r1,d );
            fprintf( file, "#----\n" );
            fprintf( file, "#(l) kdx=[%2d %2d] idx=[%2d %2d] list=[%2d %2d %2d %2d] \n", 
                     kdx[0],kdx[1],  idx[0],idx[1],  
                     com[0].list[idx[0]][0], com[0].list[idx[0]][1], 
                     com[0].list[idx[0]][2], com[0].list[idx[0]][3] );
            fprintf( file, "#(l) kdx=[%2d %2d] idx=[%2d %2d] list=[%2d %2d %2d %2d] \n", 
                     kdx[0],kdx[1],  idx[0],idx[1],  
                     com[0].list[idx[1]][0], com[0].list[idx[1]][1], 
                     com[0].list[idx[1]][2], com[0].list[idx[1]][3] );
    
            for( j=0;j<bsize;j++ )
           {
               for( i=0;i<bsize;i++ )
              {
                  cw.loadv( i,j,idx[0], wc,w );
                  z[0]= w[3]*w[6];
                  z[1]= w[4]*w[6];
                  z[2]= w[5]*w[6];
                  z[3]= 0;
                  z[4]= 0;
  
                  r0.transform( i,j,0, l,m,n );
                  r1.transform( i,j,0, e,f,g );
//                fprintf( file,"%1d %1d %1d %1d %1d % 9.3e % 9.3e % 9.3e % 9.3e\n", l,m,n,c,d, w[3],w[4],w[5],w[6] ); 
    
                  so[0].subv(l,m,n,c,r,z );
                  so[0].addv(e,f,g,d,r,z );
              }
           }
        }
     }

/*    for( t=0;t<hx.n;t++ )
     {
         fprintf( file,"after local face loop \n" );
         fprintf( file," \n" );
         fprintf( file,"%2d %2d \n", t,hx[t].id );
         fprintf( file," \n" );
         for( k=0;k<bsize;k++ )
        {
            for( j=0;j<bsize;j++ )
           {
               for( i=0;i<bsize;i++ )
              {
                  so[0].loadv(i,j,k,t,r, z );
                  fprintf( file," % 9.3e % 9.3e % 9.3e \n", z[0],z[1],z[2] );

              }
           }
        }
     }*/

// handle interfaces with data dependencies on remote ranks
/*    fprintf( file, "\n" );
      fprintf( file, "# remote dependency loops\n" );
      fprintf( file, "\n" );*/

      while( transit( com[0],msg[0], ntsk,tasks, 1 ) )
     {
//pragma omp parallel for private(t,c,d,i,j,l,m,n,r0,idx,num,loc,ql,qr,s,lmax)
         for( INT_ t=0;t<ntsk;t++ )
        {
    
            com[0].task( tasks[t], num,loc, &idx,jdx,kdx );
            com[0].taskinfo( so[0], idx[0], r0,c );

            fprintf( file, "#----\n" );
            fprintf( file, "#(r) kdx=[%2d %2d] idx=[%2d %2d] list=[%2d %2d %2d %2d] \n", 
                     kdx[0],-1    ,  idx[0],-1    ,  
                     com[0].list[idx[0]][0], com[0].list[idx[0]][1], 
                     com[0].list[idx[0]][2], com[0].list[idx[0]][3] );
    
            for( j=0;j<bsize;j++ )
           {
               for( i=0;i<bsize;i++ )
              {
    
                  cw.loadv( i,j,idx[0], wc,w );
                  z[0]= w[3]*w[6];
                  z[1]= w[4]*w[6];
                  z[2]= w[5]*w[6];
                  z[3]= 0;
                  z[4]= 0;

                  r0.transform( i,j,0, l,m,n );
    
                  so[0].subv(l,m,n,c,r,z );

//                fprintf( file,"%1d %1d %1d %1d % 9.3e % 9.3e % 9.3e % 9.3e\n", l,m,n,c, w[3],w[4],w[5],w[6] ); 
    
              }
           }
        }
     }

      for( t=0;t<hx.n;t++ )
     {
         fprintf( file," \n" );
         fprintf( file,"#after remote face loop\n" );
         fprintf( file,"#%2d %2d \n", t,hx[t].id );
         fprintf( file," \n" );
         for( k=0;k<bsize;k++ )
        {
            for( j=0;j<bsize;j++ )
           {
               for( i=0;i<bsize;i++ )
              {
                  so[0].loadv(i,j,k,t,r, z );
                  fprintf( file," % 9.3e % 9.3e % 9.3e \n", z[0],z[1],z[2] );

              }
           }
        }
     }


      return;
  }
