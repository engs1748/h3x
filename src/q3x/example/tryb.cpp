
#  include <q3x/example.h>


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void example_t::tryb( double *x )
  {

      INT_    c,g;
      INT_    i,j,k, l,m,n;
      INT_    ist,ien;
      frule_t r;
    
      fprintf( file, "# tryb %4d %4d \n", nb,ng );

      ien= 0;
      for( g=0;g<BTYPES;g++ )
     {
         fprintf( file, "# faces %4d \n", bo[g].n );

         ist= ien;
         ien= bn.l[g][1];

         for( INT_ t=ist;t<ien;t++ )
        {
    
            r= frule_t( bn.f[t], bsize+1,bsize+1,bsize+1 );
            c= bn.h[t];
             
/*          fprintf( file, "# %4d %4d \n", c,g );
            fprintf( file, "# %4d %4d \n", b[g].n );*/
    
            k= 0;
            for( j=0;j<bsize+1;j++ )
           {
               for( i=0;i<bsize+1;i++ )
              {
                  r.transform( i,j,k, l,m,n );
                  size_t w0= co[0].addr( 0,l,m,n, c );
                  size_t w1= co[0].addr( 1,l,m,n, c );
                  size_t w2= co[0].addr( 2,l,m,n, c );
    
                  fprintf( file, " % 9.4e % 9.4e % 9.4e\n", x[w0],x[w1],x[w2] );
    
              }
           }
        }
     }

      return;
  }
