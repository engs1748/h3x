

#  include <q3x/example.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void example_t::trye( double *x )
  {
      INT_  ntsk;
      INT_  tasks[16384];

      INT_    c;
      INT_    i,j,k, l,m,n, r,s;
      frule_t r0,r1;
      INT_    num,loc;
      INT_   *idx;

      msg_t msg;

      malloc( msg,co[0],com[1] );

      exchange(  x, co[2], com[1], msg);

      while( transit( com[1],msg, ntsk,tasks, 0 ) )
     {
         for( r=0;r<ntsk;r++ )
        {
            fprintf( file, "\n" );
            fprintf( file, "# task %4d: %4d\n", r,tasks[r] );
            fprintf( file, "\n" );
            com[1].task( tasks[r], num,loc, &idx );
            assert( num >= loc );

            for( s=0;s<loc;s++ )
           { 
               com[1].taskinfo( co[0], idx[s], r0,c );
             
               fprintf( file, "# \n" );
             
               k= 0;
               j= 0;
               for( i=0;i<bsize+1;i++ )
              {
                  r0.transform( i,j,k, l,m,n );
                  size_t w0= co[0].addr( 0,l,m,n, c );
                  size_t w1= co[0].addr( 1,l,m,n, c );
                  size_t w2= co[0].addr( 2,l,m,n, c );
          
                  fprintf( file, " % 9.4e % 9.4e % 9.4e\n", x[w0],x[w1],x[w2] );
          
              }
           }

            for( s=loc;s<num;s++ )
           { 
               c= idx[s];
             
               fprintf( file, "# \n" );
             
               k= 0;
               j= 0;
               for( i=0;i<bsize+1;i++ )
              {
                  size_t w0= co[2].addr( 0,i,j,k, c );
                  size_t w1= co[2].addr( 1,i,j,k, c );
                  size_t w2= co[2].addr( 2,i,j,k, c );
          
                  fprintf( file, " % 9.4e % 9.4e % 9.4e\n", x[w0],x[w1],x[w2] );
          
              }
           }
        }
     }

      free( msg );

      return;
  }
