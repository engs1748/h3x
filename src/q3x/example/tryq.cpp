


#  include <q3x/example.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void example_t::tryq( double *x )
  {

      msg_t msg;

      malloc( msg,co[0],com[0] );

      INT_  ntsk;
      INT_  tasks[16384];

      exchange(  x, co[1], com[0], msg );

// here OPENMP loop to handle blocks


// handle interfaces with only local data dependencies
     {
         INT_    b,c,d;
         INT_    i,j,k, l,m,n;
         frule_t r0,r1;
         INT_    num,loc;
         INT_   *idx;
       
         while( locals( com[0],msg, ntsk,tasks ) )
        {
            for( INT_ t=0;t<ntsk;t++ )
           {
       
               com[0].task( tasks[t], num,loc, &idx );
               com[0].taskinfo( co[0], idx[0], r0,c );
               com[0].taskinfo( co[0], idx[1], r1,d );
               assert( num == 2 );
               assert( loc == 2 );
       
               fprintf( file, "# %4d %4d \n", c,d );
       
               k= 0;
               for( j=0;j<bsize+1;j++ )
              {
                  for( i=0;i<bsize+1;i++ )
                 {
                     r0.transform( i,j,k, l,m,n );
                     size_t w0= co[0].addr( 0,l,m,n, c );
                     size_t w1= co[0].addr( 1,l,m,n, c );
                     size_t w2= co[0].addr( 2,l,m,n, c );
       
                     r1.transform( i,j,k, l,m,n );
                     size_t v0= co[0].addr( 0,l,m,n, d );
                     size_t v1= co[0].addr( 1,l,m,n, d );
                     size_t v2= co[0].addr( 2,l,m,n, d );
       
                     fprintf( file, "\n" );
                     fprintf( file, "#\n" );
                     fprintf( file, "\n" );
                     fprintf( file, " % 9.4e % 9.4e % 9.4e\n", x[v0],x[v1],x[v2] );
                     fprintf( file, " % 9.4e % 9.4e % 9.4e\n", x[w0],x[w1],x[w2] );
       
                 }
              }
           }
        }

// handle interfaces with data dependencies on remote ranks

         while( transit( com[0],msg, ntsk,tasks, 1 ) )
        {
            for( INT_ t=0;t<ntsk;t++ )
           {
       
               com[0].task( tasks[t], num,loc, &idx );
               com[0].taskinfo( co[0], idx[0], r0,c );
               b= idx[1];
       
               assert( num == 2 );
               assert( loc == 1 );
       
               fprintf( file, "# %4d %4d \n", c,b );
       
               k= 0;
               for( j=0;j<bsize+1;j++ )
              {
                  for( i=0;i<bsize+1;i++ )
                 {
                     size_t w0= co[1].addr( 0,i,j,k, b );
                     size_t w1= co[1].addr( 1,i,j,k, b );
                     size_t w2= co[1].addr( 2,i,j,k, b );
       
                     r0.transform( i,j,k, l,m,n );
                     size_t v0= co[0].addr( 0,l,m,n, c );
                     size_t v1= co[0].addr( 1,l,m,n, c );
                     size_t v2= co[0].addr( 2,l,m,n, c );
       
                     fprintf( file, "\n" );
                     fprintf( file, "#\n" );
                     fprintf( file, "\n" );
                     fprintf( file, " % 9.4e % 9.4e % 9.4e\n", x[v0],x[v1],x[v2] );
                     fprintf( file, " % 9.4e % 9.4e % 9.4e\n", x[w0],x[w1],x[w2] );
       
                 }
              }
           }
        }
     }

      free( msg );

      return;
  }
