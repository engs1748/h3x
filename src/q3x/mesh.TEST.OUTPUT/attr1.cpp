
#include<q3x/q3x.h>
#include<q2.h>
#include<q3x/inline/setp3>

   void q3x_t::attr1( REAL_ *x, REAL_ *p, REAL_ dst, REAL_ omega, REAL_ &res )
  {
      res=0;
      if( dst == 0 ){ return; }

      REAL_3     x000,x100,x200, x010,x110,x210, x020,x120,x220;
      REAL_3     x001,x101,x201, x011,x111,x211, x021,x121,x221;
      REAL_3     x002,x102,x202, x012,x112,x212, x022,x122,x222;
      REAL_3     q[8+12+6];
      REAL_3     dp;
      REAL_3     f;
      REAL_      u,v,w;
      res=0;

      INT_       i,j,k,t;

      for( t=0;t<hx.n;t++ )
     {

// corners

         i=1;
         j=1;
         k=1;

         co[0].loadv( i-1,j-1,k-1,t, x,x000 );
         co[0].loadv( i  ,j-1,k-1,t, x,x100 );
         co[0].loadv( i+1,j-1,k-1,t, x,x200 );
                                           
         co[0].loadv( i-1,j  ,k-1,t, x,x010 );
         co[0].loadv( i  ,j  ,k-1,t, x,x110 );
         co[0].loadv( i+1,j  ,k-1,t, x,x210 );
                                           
         co[0].loadv( i-1,j+1,k-1,t, x,x020 );
         co[0].loadv( i  ,j+1,k-1,t, x,x120 );
         co[0].loadv( i+1,j+1,k-1,t, x,x220 );
                                           
         co[0].loadv( i-1,j-1,k  ,t, x,x001 );
         co[0].loadv( i  ,j-1,k  ,t, x,x101 );
         co[0].loadv( i+1,j-1,k  ,t, x,x201 );
                                           
         co[0].loadv( i-1,j  ,k  ,t, x,x011 );
         co[0].loadv( i  ,j  ,k  ,t, x,x111 );
         co[0].loadv( i+1,j  ,k  ,t, x,x211 );
                                           
         co[0].loadv( i-1,j+1,k  ,t, x,x021 );
         co[0].loadv( i  ,j+1,k  ,t, x,x121 );
         co[0].loadv( i+1,j+1,k  ,t, x,x221 );
                                           
         co[0].loadv( i-1,j-1,k+1,t, x,x002 );
         co[0].loadv( i  ,j-1,k+1,t, x,x102 );
         co[0].loadv( i+1,j-1,k+1,t, x,x202 );
                                           
         co[0].loadv( i-1,j  ,k+1,t, x,x012 );
         co[0].loadv( i  ,j  ,k+1,t, x,x112 );
         co[0].loadv( i+1,j  ,k+1,t, x,x212 );
                                           
         co[0].loadv( i-1,j+1,k+1,t, x,x022 );
         co[0].loadv( i  ,j+1,k+1,t, x,x122 );
         co[0].loadv( i+1,j+1,k+1,t, x,x222 );

         co[0].loadv( i  ,j  ,k  ,t, p,q[0] );

         setp3(  x000,  x100,  x200,  x010,  x110,  x210,  x020,  x120,  x220,
                 x001,  x101,  x201,  x011,  x111,  x211,  x021,  x121,  x221,
                 x002,  x102,  x202,  x012,  x112,  x212,  x022,  x122,  x222,  q[0],  dp, 0,0,0, hx[t].d[0][0], hx[t].d[0][1], hx[t].d[0][2], dst );

         res+= fabs( dp[0] );
         res+= fabs( dp[1] );
         res+= fabs( dp[2] );

         q[0][0]+= omega*dp[0];
         q[0][1]+= omega*dp[1];
         q[0][2]+= omega*dp[2];


         i=bsize-1;
         j=1;
         k=1;

         co[0].loadv( i-1,j-1,k-1,t, x,x000 );
         co[0].loadv( i  ,j-1,k-1,t, x,x100 );
         co[0].loadv( i+1,j-1,k-1,t, x,x200 );
                                           
         co[0].loadv( i-1,j  ,k-1,t, x,x010 );
         co[0].loadv( i  ,j  ,k-1,t, x,x110 );
         co[0].loadv( i+1,j  ,k-1,t, x,x210 );
                                           
         co[0].loadv( i-1,j+1,k-1,t, x,x020 );
         co[0].loadv( i  ,j+1,k-1,t, x,x120 );
         co[0].loadv( i+1,j+1,k-1,t, x,x220 );
                                           
         co[0].loadv( i-1,j-1,k  ,t, x,x001 );
         co[0].loadv( i  ,j-1,k  ,t, x,x101 );
         co[0].loadv( i+1,j-1,k  ,t, x,x201 );
                                           
         co[0].loadv( i-1,j  ,k  ,t, x,x011 );
         co[0].loadv( i  ,j  ,k  ,t, x,x111 );
         co[0].loadv( i+1,j  ,k  ,t, x,x211 );
                                           
         co[0].loadv( i-1,j+1,k  ,t, x,x021 );
         co[0].loadv( i  ,j+1,k  ,t, x,x121 );
         co[0].loadv( i+1,j+1,k  ,t, x,x221 );
                                           
         co[0].loadv( i-1,j-1,k+1,t, x,x002 );
         co[0].loadv( i  ,j-1,k+1,t, x,x102 );
         co[0].loadv( i+1,j-1,k+1,t, x,x202 );
                                           
         co[0].loadv( i-1,j  ,k+1,t, x,x012 );
         co[0].loadv( i  ,j  ,k+1,t, x,x112 );
         co[0].loadv( i+1,j  ,k+1,t, x,x212 );
                                           
         co[0].loadv( i-1,j+1,k+1,t, x,x022 );
         co[0].loadv( i  ,j+1,k+1,t, x,x122 );
         co[0].loadv( i+1,j+1,k+1,t, x,x222 );

         co[0].loadv( i  ,j  ,k  ,t, p,q[1] );

         setp3(  x000,  x100,  x200,  x010,  x110,  x210,  x020,  x120,  x220,
                 x001,  x101,  x201,  x011,  x111,  x211,  x021,  x121,  x221,
                 x002,  x102,  x202,  x012,  x112,  x212,  x022,  x122,  x222,  q[1],  dp, 1,0,0, hx[t].d[1][0], hx[t].d[1][1], hx[t].d[1][2], dst );

         res+= fabs( dp[0] );
         res+= fabs( dp[1] );
         res+= fabs( dp[2] );

         q[1][0]+= omega*dp[0];
         q[1][1]+= omega*dp[1];
         q[1][2]+= omega*dp[2];

         i=1;
         j=bsize-1;
         k=1;

         co[0].loadv( i-1,j-1,k-1,t, x,x000 );
         co[0].loadv( i  ,j-1,k-1,t, x,x100 );
         co[0].loadv( i+1,j-1,k-1,t, x,x200 );
                                           
         co[0].loadv( i-1,j  ,k-1,t, x,x010 );
         co[0].loadv( i  ,j  ,k-1,t, x,x110 );
         co[0].loadv( i+1,j  ,k-1,t, x,x210 );
                                           
         co[0].loadv( i-1,j+1,k-1,t, x,x020 );
         co[0].loadv( i  ,j+1,k-1,t, x,x120 );
         co[0].loadv( i+1,j+1,k-1,t, x,x220 );
                                           
         co[0].loadv( i-1,j-1,k  ,t, x,x001 );
         co[0].loadv( i  ,j-1,k  ,t, x,x101 );
         co[0].loadv( i+1,j-1,k  ,t, x,x201 );
                                           
         co[0].loadv( i-1,j  ,k  ,t, x,x011 );
         co[0].loadv( i  ,j  ,k  ,t, x,x111 );
         co[0].loadv( i+1,j  ,k  ,t, x,x211 );
                                           
         co[0].loadv( i-1,j+1,k  ,t, x,x021 );
         co[0].loadv( i  ,j+1,k  ,t, x,x121 );
         co[0].loadv( i+1,j+1,k  ,t, x,x221 );
                                           
         co[0].loadv( i-1,j-1,k+1,t, x,x002 );
         co[0].loadv( i  ,j-1,k+1,t, x,x102 );
         co[0].loadv( i+1,j-1,k+1,t, x,x202 );
                                           
         co[0].loadv( i-1,j  ,k+1,t, x,x012 );
         co[0].loadv( i  ,j  ,k+1,t, x,x112 );
         co[0].loadv( i+1,j  ,k+1,t, x,x212 );
                                           
         co[0].loadv( i-1,j+1,k+1,t, x,x022 );
         co[0].loadv( i  ,j+1,k+1,t, x,x122 );
         co[0].loadv( i+1,j+1,k+1,t, x,x222 );

         co[0].loadv( i  ,j  ,k  ,t, p,q[2] );

         setp3(  x000,  x100,  x200,  x010,  x110,  x210,  x020,  x120,  x220,
                 x001,  x101,  x201,  x011,  x111,  x211,  x021,  x121,  x221,
                 x002,  x102,  x202,  x012,  x112,  x212,  x022,  x122,  x222,  q[2],  dp, 0,1,0, hx[t].d[2][0], hx[t].d[2][1], hx[t].d[2][2], dst );

         res+= fabs( dp[0] );
         res+= fabs( dp[1] );
         res+= fabs( dp[2] );

         q[2][0]+= omega*dp[0];
         q[2][1]+= omega*dp[1];
         q[2][2]+= omega*dp[2];

         i=bsize-1;
         j=bsize-1;
         k=1;

         co[0].loadv( i-1,j-1,k-1,t, x,x000 );
         co[0].loadv( i  ,j-1,k-1,t, x,x100 );
         co[0].loadv( i+1,j-1,k-1,t, x,x200 );
                                           
         co[0].loadv( i-1,j  ,k-1,t, x,x010 );
         co[0].loadv( i  ,j  ,k-1,t, x,x110 );
         co[0].loadv( i+1,j  ,k-1,t, x,x210 );
                                           
         co[0].loadv( i-1,j+1,k-1,t, x,x020 );
         co[0].loadv( i  ,j+1,k-1,t, x,x120 );
         co[0].loadv( i+1,j+1,k-1,t, x,x220 );
                                           
         co[0].loadv( i-1,j-1,k  ,t, x,x001 );
         co[0].loadv( i  ,j-1,k  ,t, x,x101 );
         co[0].loadv( i+1,j-1,k  ,t, x,x201 );
                                           
         co[0].loadv( i-1,j  ,k  ,t, x,x011 );
         co[0].loadv( i  ,j  ,k  ,t, x,x111 );
         co[0].loadv( i+1,j  ,k  ,t, x,x211 );
                                           
         co[0].loadv( i-1,j+1,k  ,t, x,x021 );
         co[0].loadv( i  ,j+1,k  ,t, x,x121 );
         co[0].loadv( i+1,j+1,k  ,t, x,x221 );
                                           
         co[0].loadv( i-1,j-1,k+1,t, x,x002 );
         co[0].loadv( i  ,j-1,k+1,t, x,x102 );
         co[0].loadv( i+1,j-1,k+1,t, x,x202 );
                                           
         co[0].loadv( i-1,j  ,k+1,t, x,x012 );
         co[0].loadv( i  ,j  ,k+1,t, x,x112 );
         co[0].loadv( i+1,j  ,k+1,t, x,x212 );
                                           
         co[0].loadv( i-1,j+1,k+1,t, x,x022 );
         co[0].loadv( i  ,j+1,k+1,t, x,x122 );
         co[0].loadv( i+1,j+1,k+1,t, x,x222 );

         co[0].loadv( i  ,j  ,k  ,t, p,q[3] );

         setp3(  x000,  x100,  x200,  x010,  x110,  x210,  x020,  x120,  x220,
                 x001,  x101,  x201,  x011,  x111,  x211,  x021,  x121,  x221,
                 x002,  x102,  x202,  x012,  x112,  x212,  x022,  x122,  x222,  q[3],  dp, 1,1,0, hx[t].d[3][0], hx[t].d[3][1], hx[t].d[3][2], dst );

         res+= fabs( dp[0] );
         res+= fabs( dp[1] );
         res+= fabs( dp[2] );

         q[3][0]+= omega*dp[0];
         q[3][1]+= omega*dp[1];
         q[3][2]+= omega*dp[2];

         i=1;
         j=1;
         k=bsize-1;

         co[0].loadv( i-1,j-1,k-1,t, x,x000 );
         co[0].loadv( i  ,j-1,k-1,t, x,x100 );
         co[0].loadv( i+1,j-1,k-1,t, x,x200 );
                                           
         co[0].loadv( i-1,j  ,k-1,t, x,x010 );
         co[0].loadv( i  ,j  ,k-1,t, x,x110 );
         co[0].loadv( i+1,j  ,k-1,t, x,x210 );
                                           
         co[0].loadv( i-1,j+1,k-1,t, x,x020 );
         co[0].loadv( i  ,j+1,k-1,t, x,x120 );
         co[0].loadv( i+1,j+1,k-1,t, x,x220 );
                                           
         co[0].loadv( i-1,j-1,k  ,t, x,x001 );
         co[0].loadv( i  ,j-1,k  ,t, x,x101 );
         co[0].loadv( i+1,j-1,k  ,t, x,x201 );
                                           
         co[0].loadv( i-1,j  ,k  ,t, x,x011 );
         co[0].loadv( i  ,j  ,k  ,t, x,x111 );
         co[0].loadv( i+1,j  ,k  ,t, x,x211 );
                                           
         co[0].loadv( i-1,j+1,k  ,t, x,x021 );
         co[0].loadv( i  ,j+1,k  ,t, x,x121 );
         co[0].loadv( i+1,j+1,k  ,t, x,x221 );
                                           
         co[0].loadv( i-1,j-1,k+1,t, x,x002 );
         co[0].loadv( i  ,j-1,k+1,t, x,x102 );
         co[0].loadv( i+1,j-1,k+1,t, x,x202 );
                                           
         co[0].loadv( i-1,j  ,k+1,t, x,x012 );
         co[0].loadv( i  ,j  ,k+1,t, x,x112 );
         co[0].loadv( i+1,j  ,k+1,t, x,x212 );
                                           
         co[0].loadv( i-1,j+1,k+1,t, x,x022 );
         co[0].loadv( i  ,j+1,k+1,t, x,x122 );
         co[0].loadv( i+1,j+1,k+1,t, x,x222 );

         co[0].loadv( i  ,j  ,k  ,t, p,q[4] );

         setp3(  x000,  x100,  x200,  x010,  x110,  x210,  x020,  x120,  x220,
                 x001,  x101,  x201,  x011,  x111,  x211,  x021,  x121,  x221,
                 x002,  x102,  x202,  x012,  x112,  x212,  x022,  x122,  x222,  q[4],  dp, 0,0,1, hx[t].d[4][0], hx[t].d[4][1], hx[t].d[4][2], dst );

         res+= fabs( dp[0] );
         res+= fabs( dp[1] );
         res+= fabs( dp[2] );

         q[4][0]+= omega*dp[0];
         q[4][1]+= omega*dp[1];
         q[4][2]+= omega*dp[2];

         i=bsize-1;
         j=1;
         k=bsize-1;

         co[0].loadv( i-1,j-1,k-1,t, x,x000 );
         co[0].loadv( i  ,j-1,k-1,t, x,x100 );
         co[0].loadv( i+1,j-1,k-1,t, x,x200 );
                                           
         co[0].loadv( i-1,j  ,k-1,t, x,x010 );
         co[0].loadv( i  ,j  ,k-1,t, x,x110 );
         co[0].loadv( i+1,j  ,k-1,t, x,x210 );
                                           
         co[0].loadv( i-1,j+1,k-1,t, x,x020 );
         co[0].loadv( i  ,j+1,k-1,t, x,x120 );
         co[0].loadv( i+1,j+1,k-1,t, x,x220 );
                                           
         co[0].loadv( i-1,j-1,k  ,t, x,x001 );
         co[0].loadv( i  ,j-1,k  ,t, x,x101 );
         co[0].loadv( i+1,j-1,k  ,t, x,x201 );
                                           
         co[0].loadv( i-1,j  ,k  ,t, x,x011 );
         co[0].loadv( i  ,j  ,k  ,t, x,x111 );
         co[0].loadv( i+1,j  ,k  ,t, x,x211 );
                                           
         co[0].loadv( i-1,j+1,k  ,t, x,x021 );
         co[0].loadv( i  ,j+1,k  ,t, x,x121 );
         co[0].loadv( i+1,j+1,k  ,t, x,x221 );
                                           
         co[0].loadv( i-1,j-1,k+1,t, x,x002 );
         co[0].loadv( i  ,j-1,k+1,t, x,x102 );
         co[0].loadv( i+1,j-1,k+1,t, x,x202 );
                                           
         co[0].loadv( i-1,j  ,k+1,t, x,x012 );
         co[0].loadv( i  ,j  ,k+1,t, x,x112 );
         co[0].loadv( i+1,j  ,k+1,t, x,x212 );
                                           
         co[0].loadv( i-1,j+1,k+1,t, x,x022 );
         co[0].loadv( i  ,j+1,k+1,t, x,x122 );
         co[0].loadv( i+1,j+1,k+1,t, x,x222 );

         co[0].loadv( i  ,j  ,k  ,t, p,q[5] );

         setp3(  x000,  x100,  x200,  x010,  x110,  x210,  x020,  x120,  x220,
                 x001,  x101,  x201,  x011,  x111,  x211,  x021,  x121,  x221,
                 x002,  x102,  x202,  x012,  x112,  x212,  x022,  x122,  x222,  q[5],  dp, 1,0,1, hx[t].d[5][0], hx[t].d[5][1], hx[t].d[5][2], dst );

         res+= fabs( dp[0] );
         res+= fabs( dp[1] );
         res+= fabs( dp[2] );

         q[5][0]+= omega*dp[0];
         q[5][1]+= omega*dp[1];
         q[5][2]+= omega*dp[2];

         i=1;
         j=bsize-1;
         k=bsize-1;

         co[0].loadv( i-1,j-1,k-1,t, x,x000 );
         co[0].loadv( i  ,j-1,k-1,t, x,x100 );
         co[0].loadv( i+1,j-1,k-1,t, x,x200 );
                                           
         co[0].loadv( i-1,j  ,k-1,t, x,x010 );
         co[0].loadv( i  ,j  ,k-1,t, x,x110 );
         co[0].loadv( i+1,j  ,k-1,t, x,x210 );
                                           
         co[0].loadv( i-1,j+1,k-1,t, x,x020 );
         co[0].loadv( i  ,j+1,k-1,t, x,x120 );
         co[0].loadv( i+1,j+1,k-1,t, x,x220 );
                                           
         co[0].loadv( i-1,j-1,k  ,t, x,x001 );
         co[0].loadv( i  ,j-1,k  ,t, x,x101 );
         co[0].loadv( i+1,j-1,k  ,t, x,x201 );
                                           
         co[0].loadv( i-1,j  ,k  ,t, x,x011 );
         co[0].loadv( i  ,j  ,k  ,t, x,x111 );
         co[0].loadv( i+1,j  ,k  ,t, x,x211 );
                                           
         co[0].loadv( i-1,j+1,k  ,t, x,x021 );
         co[0].loadv( i  ,j+1,k  ,t, x,x121 );
         co[0].loadv( i+1,j+1,k  ,t, x,x221 );
                                           
         co[0].loadv( i-1,j-1,k+1,t, x,x002 );
         co[0].loadv( i  ,j-1,k+1,t, x,x102 );
         co[0].loadv( i+1,j-1,k+1,t, x,x202 );
                                           
         co[0].loadv( i-1,j  ,k+1,t, x,x012 );
         co[0].loadv( i  ,j  ,k+1,t, x,x112 );
         co[0].loadv( i+1,j  ,k+1,t, x,x212 );
                                           
         co[0].loadv( i-1,j+1,k+1,t, x,x022 );
         co[0].loadv( i  ,j+1,k+1,t, x,x122 );
         co[0].loadv( i+1,j+1,k+1,t, x,x222 );

         co[0].loadv( i  ,j  ,k  ,t, p,q[6] );

         setp3(  x000,  x100,  x200,  x010,  x110,  x210,  x020,  x120,  x220,
                 x001,  x101,  x201,  x011,  x111,  x211,  x021,  x121,  x221,
                 x002,  x102,  x202,  x012,  x112,  x212,  x022,  x122,  x222,  q[6],  dp, 0,1,1, hx[t].d[6][0], hx[t].d[6][1], hx[t].d[6][2], dst );

         res+= fabs( dp[0] );
         res+= fabs( dp[1] );
         res+= fabs( dp[2] );

         q[6][0]+= omega*dp[0];
         q[6][1]+= omega*dp[1];
         q[6][2]+= omega*dp[2];

         i=bsize-1;
         j=bsize-1;
         k=bsize-1;

         co[0].loadv( i-1,j-1,k-1,t, x,x000 );
         co[0].loadv( i  ,j-1,k-1,t, x,x100 );
         co[0].loadv( i+1,j-1,k-1,t, x,x200 );
                                           
         co[0].loadv( i-1,j  ,k-1,t, x,x010 );
         co[0].loadv( i  ,j  ,k-1,t, x,x110 );
         co[0].loadv( i+1,j  ,k-1,t, x,x210 );
                                           
         co[0].loadv( i-1,j+1,k-1,t, x,x020 );
         co[0].loadv( i  ,j+1,k-1,t, x,x120 );
         co[0].loadv( i+1,j+1,k-1,t, x,x220 );
                                           
         co[0].loadv( i-1,j-1,k  ,t, x,x001 );
         co[0].loadv( i  ,j-1,k  ,t, x,x101 );
         co[0].loadv( i+1,j-1,k  ,t, x,x201 );
                                           
         co[0].loadv( i-1,j  ,k  ,t, x,x011 );
         co[0].loadv( i  ,j  ,k  ,t, x,x111 );
         co[0].loadv( i+1,j  ,k  ,t, x,x211 );
                                           
         co[0].loadv( i-1,j+1,k  ,t, x,x021 );
         co[0].loadv( i  ,j+1,k  ,t, x,x121 );
         co[0].loadv( i+1,j+1,k  ,t, x,x221 );
                                           
         co[0].loadv( i-1,j-1,k+1,t, x,x002 );
         co[0].loadv( i  ,j-1,k+1,t, x,x102 );
         co[0].loadv( i+1,j-1,k+1,t, x,x202 );
                                           
         co[0].loadv( i-1,j  ,k+1,t, x,x012 );
         co[0].loadv( i  ,j  ,k+1,t, x,x112 );
         co[0].loadv( i+1,j  ,k+1,t, x,x212 );
                                           
         co[0].loadv( i-1,j+1,k+1,t, x,x022 );
         co[0].loadv( i  ,j+1,k+1,t, x,x122 );
         co[0].loadv( i+1,j+1,k+1,t, x,x222 );

         co[0].loadv( i  ,j  ,k  ,t, p,q[7] );

         setp3(  x000,  x100,  x200,  x010,  x110,  x210,  x020,  x120,  x220,
                 x001,  x101,  x201,  x011,  x111,  x211,  x021,  x121,  x221,
                 x002,  x102,  x202,  x012,  x112,  x212,  x022,  x122,  x222,  q[7],  dp, 1,1,1, hx[t].d[7][0], hx[t].d[7][1], hx[t].d[7][2], dst );

         res+= fabs( dp[0] );
         res+= fabs( dp[1] );
         res+= fabs( dp[2] );

         q[7][0]+= omega*dp[0];
         q[7][1]+= omega*dp[1];
         q[7][2]+= omega*dp[2];

// i-edges

         i=bsize/2;
         j=1;
         k=1;

         co[0].loadv( i-1,j-1,k-1,t, x,x000 );
         co[0].loadv( i  ,j-1,k-1,t, x,x100 );
         co[0].loadv( i+1,j-1,k-1,t, x,x200 );
                                           
         co[0].loadv( i-1,j  ,k-1,t, x,x010 );
         co[0].loadv( i  ,j  ,k-1,t, x,x110 );
         co[0].loadv( i+1,j  ,k-1,t, x,x210 );
                                           
         co[0].loadv( i-1,j+1,k-1,t, x,x020 );
         co[0].loadv( i  ,j+1,k-1,t, x,x120 );
         co[0].loadv( i+1,j+1,k-1,t, x,x220 );
                                           
         co[0].loadv( i-1,j-1,k  ,t, x,x001 );
         co[0].loadv( i  ,j-1,k  ,t, x,x101 );
         co[0].loadv( i+1,j-1,k  ,t, x,x201 );
                                           
         co[0].loadv( i-1,j  ,k  ,t, x,x011 );
         co[0].loadv( i  ,j  ,k  ,t, x,x111 );
         co[0].loadv( i+1,j  ,k  ,t, x,x211 );
                                           
         co[0].loadv( i-1,j+1,k  ,t, x,x021 );
         co[0].loadv( i  ,j+1,k  ,t, x,x121 );
         co[0].loadv( i+1,j+1,k  ,t, x,x221 );
                                           
         co[0].loadv( i-1,j-1,k+1,t, x,x002 );
         co[0].loadv( i  ,j-1,k+1,t, x,x102 );
         co[0].loadv( i+1,j-1,k+1,t, x,x202 );
                                           
         co[0].loadv( i-1,j  ,k+1,t, x,x012 );
         co[0].loadv( i  ,j  ,k+1,t, x,x112 );
         co[0].loadv( i+1,j  ,k+1,t, x,x212 );
                                           
         co[0].loadv( i-1,j+1,k+1,t, x,x022 );
         co[0].loadv( i  ,j+1,k+1,t, x,x122 );
         co[0].loadv( i+1,j+1,k+1,t, x,x222 );

         co[0].loadv( i  ,j  ,k  ,t, p,q[8+0] );

         setp3(  x000,  x100,  x200,  x010,  x110,  x210,  x020,  x120,  x220,
                 x001,  x101,  x201,  x011,  x111,  x211,  x021,  x121,  x221,
                 x002,  x102,  x202,  x012,  x112,  x212,  x022,  x122,  x222,  q[8+0],  dp, 0,0,0, NO_DCNTR, 
                 fmin(hx[t].d[0][1],hx[t].d[1][1]), fmin(hx[t].d[0][2],hx[t].d[1][2]), dst );

         res+= fabs( dp[1] );
         res+= fabs( dp[2] );

         q[8+0][1]+= omega*dp[1];
         q[8+0][2]+= omega*dp[2];

         i=bsize/2;
         j=bsize-1;
         k=1;

         co[0].loadv( i-1,j-1,k-1,t, x,x000 );
         co[0].loadv( i  ,j-1,k-1,t, x,x100 );
         co[0].loadv( i+1,j-1,k-1,t, x,x200 );
                                           
         co[0].loadv( i-1,j  ,k-1,t, x,x010 );
         co[0].loadv( i  ,j  ,k-1,t, x,x110 );
         co[0].loadv( i+1,j  ,k-1,t, x,x210 );
                                           
         co[0].loadv( i-1,j+1,k-1,t, x,x020 );
         co[0].loadv( i  ,j+1,k-1,t, x,x120 );
         co[0].loadv( i+1,j+1,k-1,t, x,x220 );
                                           
         co[0].loadv( i-1,j-1,k  ,t, x,x001 );
         co[0].loadv( i  ,j-1,k  ,t, x,x101 );
         co[0].loadv( i+1,j-1,k  ,t, x,x201 );
                                           
         co[0].loadv( i-1,j  ,k  ,t, x,x011 );
         co[0].loadv( i  ,j  ,k  ,t, x,x111 );
         co[0].loadv( i+1,j  ,k  ,t, x,x211 );
                                           
         co[0].loadv( i-1,j+1,k  ,t, x,x021 );
         co[0].loadv( i  ,j+1,k  ,t, x,x121 );
         co[0].loadv( i+1,j+1,k  ,t, x,x221 );
                                           
         co[0].loadv( i-1,j-1,k+1,t, x,x002 );
         co[0].loadv( i  ,j-1,k+1,t, x,x102 );
         co[0].loadv( i+1,j-1,k+1,t, x,x202 );
                                           
         co[0].loadv( i-1,j  ,k+1,t, x,x012 );
         co[0].loadv( i  ,j  ,k+1,t, x,x112 );
         co[0].loadv( i+1,j  ,k+1,t, x,x212 );
                                           
         co[0].loadv( i-1,j+1,k+1,t, x,x022 );
         co[0].loadv( i  ,j+1,k+1,t, x,x122 );
         co[0].loadv( i+1,j+1,k+1,t, x,x222 );

         co[0].loadv( i  ,j  ,k  ,t, p,q[8+1] );

         setp3(  x000,  x100,  x200,  x010,  x110,  x210,  x020,  x120,  x220,
                 x001,  x101,  x201,  x011,  x111,  x211,  x021,  x121,  x221,
                 x002,  x102,  x202,  x012,  x112,  x212,  x022,  x122,  x222,  q[8+1],  dp, 0,1,0, NO_DCNTR, 
                 fmin(hx[t].d[2][1],hx[t].d[3][1]), fmin(hx[t].d[2][2],hx[t].d[3][2]), dst );

         res+= fabs( dp[1] );
         res+= fabs( dp[2] );

         q[8+1][1]+= omega*dp[1];
         q[8+1][2]+= omega*dp[2];

         i=bsize/2;
         j=1;
         k=bsize-1;

         co[0].loadv( i-1,j-1,k-1,t, x,x000 );
         co[0].loadv( i  ,j-1,k-1,t, x,x100 );
         co[0].loadv( i+1,j-1,k-1,t, x,x200 );
                                           
         co[0].loadv( i-1,j  ,k-1,t, x,x010 );
         co[0].loadv( i  ,j  ,k-1,t, x,x110 );
         co[0].loadv( i+1,j  ,k-1,t, x,x210 );
                                           
         co[0].loadv( i-1,j+1,k-1,t, x,x020 );
         co[0].loadv( i  ,j+1,k-1,t, x,x120 );
         co[0].loadv( i+1,j+1,k-1,t, x,x220 );
                                           
         co[0].loadv( i-1,j-1,k  ,t, x,x001 );
         co[0].loadv( i  ,j-1,k  ,t, x,x101 );
         co[0].loadv( i+1,j-1,k  ,t, x,x201 );
                                           
         co[0].loadv( i-1,j  ,k  ,t, x,x011 );
         co[0].loadv( i  ,j  ,k  ,t, x,x111 );
         co[0].loadv( i+1,j  ,k  ,t, x,x211 );
                                           
         co[0].loadv( i-1,j+1,k  ,t, x,x021 );
         co[0].loadv( i  ,j+1,k  ,t, x,x121 );
         co[0].loadv( i+1,j+1,k  ,t, x,x221 );
                                           
         co[0].loadv( i-1,j-1,k+1,t, x,x002 );
         co[0].loadv( i  ,j-1,k+1,t, x,x102 );
         co[0].loadv( i+1,j-1,k+1,t, x,x202 );
                                           
         co[0].loadv( i-1,j  ,k+1,t, x,x012 );
         co[0].loadv( i  ,j  ,k+1,t, x,x112 );
         co[0].loadv( i+1,j  ,k+1,t, x,x212 );
                                           
         co[0].loadv( i-1,j+1,k+1,t, x,x022 );
         co[0].loadv( i  ,j+1,k+1,t, x,x122 );
         co[0].loadv( i+1,j+1,k+1,t, x,x222 );

         co[0].loadv( i  ,j  ,k  ,t, p,q[8+2] );

         setp3(  x000,  x100,  x200,  x010,  x110,  x210,  x020,  x120,  x220,
                 x001,  x101,  x201,  x011,  x111,  x211,  x021,  x121,  x221,
                 x002,  x102,  x202,  x012,  x112,  x212,  x022,  x122,  x222,  q[8+2],  dp, 0,0,1, NO_DCNTR, 
                 fmin(hx[t].d[4][1],hx[t].d[5][1]), fmin(hx[t].d[4][2],hx[t].d[5][2]), dst );

         res+= fabs( dp[1] );
         res+= fabs( dp[2] );

         q[8+2][1]+= omega*dp[1];
         q[8+2][2]+= omega*dp[2];

         i=bsize/2;
         j=bsize-1;
         k=bsize-1;

         co[0].loadv( i-1,j-1,k-1,t, x,x000 );
         co[0].loadv( i  ,j-1,k-1,t, x,x100 );
         co[0].loadv( i+1,j-1,k-1,t, x,x200 );
                                           
         co[0].loadv( i-1,j  ,k-1,t, x,x010 );
         co[0].loadv( i  ,j  ,k-1,t, x,x110 );
         co[0].loadv( i+1,j  ,k-1,t, x,x210 );
                                           
         co[0].loadv( i-1,j+1,k-1,t, x,x020 );
         co[0].loadv( i  ,j+1,k-1,t, x,x120 );
         co[0].loadv( i+1,j+1,k-1,t, x,x220 );
                                           
         co[0].loadv( i-1,j-1,k  ,t, x,x001 );
         co[0].loadv( i  ,j-1,k  ,t, x,x101 );
         co[0].loadv( i+1,j-1,k  ,t, x,x201 );
                                           
         co[0].loadv( i-1,j  ,k  ,t, x,x011 );
         co[0].loadv( i  ,j  ,k  ,t, x,x111 );
         co[0].loadv( i+1,j  ,k  ,t, x,x211 );
                                           
         co[0].loadv( i-1,j+1,k  ,t, x,x021 );
         co[0].loadv( i  ,j+1,k  ,t, x,x121 );
         co[0].loadv( i+1,j+1,k  ,t, x,x221 );
                                           
         co[0].loadv( i-1,j-1,k+1,t, x,x002 );
         co[0].loadv( i  ,j-1,k+1,t, x,x102 );
         co[0].loadv( i+1,j-1,k+1,t, x,x202 );
                                           
         co[0].loadv( i-1,j  ,k+1,t, x,x012 );
         co[0].loadv( i  ,j  ,k+1,t, x,x112 );
         co[0].loadv( i+1,j  ,k+1,t, x,x212 );
                                           
         co[0].loadv( i-1,j+1,k+1,t, x,x022 );
         co[0].loadv( i  ,j+1,k+1,t, x,x122 );
         co[0].loadv( i+1,j+1,k+1,t, x,x222 );

         co[0].loadv( i  ,j  ,k  ,t, p,q[8+3] );

         setp3(  x000,  x100,  x200,  x010,  x110,  x210,  x020,  x120,  x220,
                 x001,  x101,  x201,  x011,  x111,  x211,  x021,  x121,  x221,
                 x002,  x102,  x202,  x012,  x112,  x212,  x022,  x122,  x222,  q[8+3],  dp, 0,1,1, NO_DCNTR, 
                 fmin(hx[t].d[4][1],hx[t].d[5][1]), fmin(hx[t].d[4][2],hx[t].d[5][2]), dst );

         res+= fabs( dp[1] );
         res+= fabs( dp[2] );

         q[8+3][1]+= omega*dp[1];
         q[8+3][2]+= omega*dp[2];

// j-edges

         i=1;
         j=bsize/2;
         k=1;

         co[0].loadv( i-1,j-1,k-1,t, x,x000 );
         co[0].loadv( i  ,j-1,k-1,t, x,x100 );
         co[0].loadv( i+1,j-1,k-1,t, x,x200 );
                                           
         co[0].loadv( i-1,j  ,k-1,t, x,x010 );
         co[0].loadv( i  ,j  ,k-1,t, x,x110 );
         co[0].loadv( i+1,j  ,k-1,t, x,x210 );
                                           
         co[0].loadv( i-1,j+1,k-1,t, x,x020 );
         co[0].loadv( i  ,j+1,k-1,t, x,x120 );
         co[0].loadv( i+1,j+1,k-1,t, x,x220 );
                                           
         co[0].loadv( i-1,j-1,k  ,t, x,x001 );
         co[0].loadv( i  ,j-1,k  ,t, x,x101 );
         co[0].loadv( i+1,j-1,k  ,t, x,x201 );
                                           
         co[0].loadv( i-1,j  ,k  ,t, x,x011 );
         co[0].loadv( i  ,j  ,k  ,t, x,x111 );
         co[0].loadv( i+1,j  ,k  ,t, x,x211 );
                                           
         co[0].loadv( i-1,j+1,k  ,t, x,x021 );
         co[0].loadv( i  ,j+1,k  ,t, x,x121 );
         co[0].loadv( i+1,j+1,k  ,t, x,x221 );
                                           
         co[0].loadv( i-1,j-1,k+1,t, x,x002 );
         co[0].loadv( i  ,j-1,k+1,t, x,x102 );
         co[0].loadv( i+1,j-1,k+1,t, x,x202 );
                                           
         co[0].loadv( i-1,j  ,k+1,t, x,x012 );
         co[0].loadv( i  ,j  ,k+1,t, x,x112 );
         co[0].loadv( i+1,j  ,k+1,t, x,x212 );
                                           
         co[0].loadv( i-1,j+1,k+1,t, x,x022 );
         co[0].loadv( i  ,j+1,k+1,t, x,x122 );
         co[0].loadv( i+1,j+1,k+1,t, x,x222 );

         co[0].loadv( i  ,j  ,k  ,t, p,q[8+4] );

         setp3(  x000,  x100,  x200,  x010,  x110,  x210,  x020,  x120,  x220,
                 x001,  x101,  x201,  x011,  x111,  x211,  x021,  x121,  x221,
                 x002,  x102,  x202,  x012,  x112,  x212,  x022,  x122,  x222,  q[8+4],  dp, 0,0,0, 
                 fmin(hx[t].d[0][0],hx[t].d[2][0]), NO_DCNTR, fmin(hx[t].d[0][2],hx[t].d[2][2]), dst );

         res+= fabs( dp[0] );
         res+= fabs( dp[2] );

         q[8+4][0]+= omega*dp[0];
         q[8+4][2]+= omega*dp[2];

         i=bsize-1;
         j=bsize/2;
         k=1;

         co[0].loadv( i-1,j-1,k-1,t, x,x000 );
         co[0].loadv( i  ,j-1,k-1,t, x,x100 );
         co[0].loadv( i+1,j-1,k-1,t, x,x200 );
                                           
         co[0].loadv( i-1,j  ,k-1,t, x,x010 );
         co[0].loadv( i  ,j  ,k-1,t, x,x110 );
         co[0].loadv( i+1,j  ,k-1,t, x,x210 );
                                           
         co[0].loadv( i-1,j+1,k-1,t, x,x020 );
         co[0].loadv( i  ,j+1,k-1,t, x,x120 );
         co[0].loadv( i+1,j+1,k-1,t, x,x220 );
                                           
         co[0].loadv( i-1,j-1,k  ,t, x,x001 );
         co[0].loadv( i  ,j-1,k  ,t, x,x101 );
         co[0].loadv( i+1,j-1,k  ,t, x,x201 );
                                           
         co[0].loadv( i-1,j  ,k  ,t, x,x011 );
         co[0].loadv( i  ,j  ,k  ,t, x,x111 );
         co[0].loadv( i+1,j  ,k  ,t, x,x211 );
                                           
         co[0].loadv( i-1,j+1,k  ,t, x,x021 );
         co[0].loadv( i  ,j+1,k  ,t, x,x121 );
         co[0].loadv( i+1,j+1,k  ,t, x,x221 );
                                           
         co[0].loadv( i-1,j-1,k+1,t, x,x002 );
         co[0].loadv( i  ,j-1,k+1,t, x,x102 );
         co[0].loadv( i+1,j-1,k+1,t, x,x202 );
                                           
         co[0].loadv( i-1,j  ,k+1,t, x,x012 );
         co[0].loadv( i  ,j  ,k+1,t, x,x112 );
         co[0].loadv( i+1,j  ,k+1,t, x,x212 );
                                           
         co[0].loadv( i-1,j+1,k+1,t, x,x022 );
         co[0].loadv( i  ,j+1,k+1,t, x,x122 );
         co[0].loadv( i+1,j+1,k+1,t, x,x222 );

         co[0].loadv( i  ,j  ,k  ,t, p,q[8+5] );

         setp3(  x000,  x100,  x200,  x010,  x110,  x210,  x020,  x120,  x220,
                 x001,  x101,  x201,  x011,  x111,  x211,  x021,  x121,  x221,
                 x002,  x102,  x202,  x012,  x112,  x212,  x022,  x122,  x222,  q[8+5],  dp, 1,0,0, 
                 fmin(hx[t].d[1][0],hx[t].d[3][0]), NO_DCNTR, fmin(hx[t].d[1][2],hx[t].d[3][2]), dst );

         res+= fabs( dp[0] );
         res+= fabs( dp[2] );

         q[8+5][0]+= omega*dp[0];
         q[8+5][2]+= omega*dp[2];

         i=1;
         j=bsize/2;
         k=bsize-1;

         co[0].loadv( i-1,j-1,k-1,t, x,x000 );
         co[0].loadv( i  ,j-1,k-1,t, x,x100 );
         co[0].loadv( i+1,j-1,k-1,t, x,x200 );
                                           
         co[0].loadv( i-1,j  ,k-1,t, x,x010 );
         co[0].loadv( i  ,j  ,k-1,t, x,x110 );
         co[0].loadv( i+1,j  ,k-1,t, x,x210 );
                                           
         co[0].loadv( i-1,j+1,k-1,t, x,x020 );
         co[0].loadv( i  ,j+1,k-1,t, x,x120 );
         co[0].loadv( i+1,j+1,k-1,t, x,x220 );
                                           
         co[0].loadv( i-1,j-1,k  ,t, x,x001 );
         co[0].loadv( i  ,j-1,k  ,t, x,x101 );
         co[0].loadv( i+1,j-1,k  ,t, x,x201 );
                                           
         co[0].loadv( i-1,j  ,k  ,t, x,x011 );
         co[0].loadv( i  ,j  ,k  ,t, x,x111 );
         co[0].loadv( i+1,j  ,k  ,t, x,x211 );
                                           
         co[0].loadv( i-1,j+1,k  ,t, x,x021 );
         co[0].loadv( i  ,j+1,k  ,t, x,x121 );
         co[0].loadv( i+1,j+1,k  ,t, x,x221 );
                                           
         co[0].loadv( i-1,j-1,k+1,t, x,x002 );
         co[0].loadv( i  ,j-1,k+1,t, x,x102 );
         co[0].loadv( i+1,j-1,k+1,t, x,x202 );
                                           
         co[0].loadv( i-1,j  ,k+1,t, x,x012 );
         co[0].loadv( i  ,j  ,k+1,t, x,x112 );
         co[0].loadv( i+1,j  ,k+1,t, x,x212 );
                                           
         co[0].loadv( i-1,j+1,k+1,t, x,x022 );
         co[0].loadv( i  ,j+1,k+1,t, x,x122 );
         co[0].loadv( i+1,j+1,k+1,t, x,x222 );

         co[0].loadv( i  ,j  ,k  ,t, p,q[8+6] );

         setp3(  x000,  x100,  x200,  x010,  x110,  x210,  x020,  x120,  x220,
                 x001,  x101,  x201,  x011,  x111,  x211,  x021,  x121,  x221,
                 x002,  x102,  x202,  x012,  x112,  x212,  x022,  x122,  x222,  q[8+6],  dp, 0,0,1, 
                 fmin(hx[t].d[4][0],hx[t].d[6][0]), NO_DCNTR, fmin(hx[t].d[4][2],hx[t].d[6][2]), dst );

         res+= fabs( dp[0] );
         res+= fabs( dp[2] );

         q[8+6][0]+= omega*dp[0];
         q[8+6][2]+= omega*dp[2];

         i=bsize-1;
         j=bsize/2;
         k=bsize-1;

         co[0].loadv( i-1,j-1,k-1,t, x,x000 );
         co[0].loadv( i  ,j-1,k-1,t, x,x100 );
         co[0].loadv( i+1,j-1,k-1,t, x,x200 );
                                           
         co[0].loadv( i-1,j  ,k-1,t, x,x010 );
         co[0].loadv( i  ,j  ,k-1,t, x,x110 );
         co[0].loadv( i+1,j  ,k-1,t, x,x210 );
                                           
         co[0].loadv( i-1,j+1,k-1,t, x,x020 );
         co[0].loadv( i  ,j+1,k-1,t, x,x120 );
         co[0].loadv( i+1,j+1,k-1,t, x,x220 );
                                           
         co[0].loadv( i-1,j-1,k  ,t, x,x001 );
         co[0].loadv( i  ,j-1,k  ,t, x,x101 );
         co[0].loadv( i+1,j-1,k  ,t, x,x201 );
                                           
         co[0].loadv( i-1,j  ,k  ,t, x,x011 );
         co[0].loadv( i  ,j  ,k  ,t, x,x111 );
         co[0].loadv( i+1,j  ,k  ,t, x,x211 );
                                           
         co[0].loadv( i-1,j+1,k  ,t, x,x021 );
         co[0].loadv( i  ,j+1,k  ,t, x,x121 );
         co[0].loadv( i+1,j+1,k  ,t, x,x221 );
                                           
         co[0].loadv( i-1,j-1,k+1,t, x,x002 );
         co[0].loadv( i  ,j-1,k+1,t, x,x102 );
         co[0].loadv( i+1,j-1,k+1,t, x,x202 );
                                           
         co[0].loadv( i-1,j  ,k+1,t, x,x012 );
         co[0].loadv( i  ,j  ,k+1,t, x,x112 );
         co[0].loadv( i+1,j  ,k+1,t, x,x212 );
                                           
         co[0].loadv( i-1,j+1,k+1,t, x,x022 );
         co[0].loadv( i  ,j+1,k+1,t, x,x122 );
         co[0].loadv( i+1,j+1,k+1,t, x,x222 );

         co[0].loadv( i  ,j  ,k  ,t, p,q[8+7] );

         setp3(  x000,  x100,  x200,  x010,  x110,  x210,  x020,  x120,  x220,
                 x001,  x101,  x201,  x011,  x111,  x211,  x021,  x121,  x221,
                 x002,  x102,  x202,  x012,  x112,  x212,  x022,  x122,  x222,  q[8+7],  dp, 1,0,1, 
                 fmin(hx[t].d[5][0],hx[t].d[7][0]), NO_DCNTR, fmin(hx[t].d[5][2],hx[t].d[7][2]), dst );

         res+= fabs( dp[0] );
         res+= fabs( dp[2] );

         q[8+7][0]+= omega*dp[0];
         q[8+7][2]+= omega*dp[2];

// k-edges

         i=1;
         j=1;
         k=bsize/2;

         co[0].loadv( i-1,j-1,k-1,t, x,x000 );
         co[0].loadv( i  ,j-1,k-1,t, x,x100 );
         co[0].loadv( i+1,j-1,k-1,t, x,x200 );
                                           
         co[0].loadv( i-1,j  ,k-1,t, x,x010 );
         co[0].loadv( i  ,j  ,k-1,t, x,x110 );
         co[0].loadv( i+1,j  ,k-1,t, x,x210 );
                                           
         co[0].loadv( i-1,j+1,k-1,t, x,x020 );
         co[0].loadv( i  ,j+1,k-1,t, x,x120 );
         co[0].loadv( i+1,j+1,k-1,t, x,x220 );
                                           
         co[0].loadv( i-1,j-1,k  ,t, x,x001 );
         co[0].loadv( i  ,j-1,k  ,t, x,x101 );
         co[0].loadv( i+1,j-1,k  ,t, x,x201 );
                                           
         co[0].loadv( i-1,j  ,k  ,t, x,x011 );
         co[0].loadv( i  ,j  ,k  ,t, x,x111 );
         co[0].loadv( i+1,j  ,k  ,t, x,x211 );
                                           
         co[0].loadv( i-1,j+1,k  ,t, x,x021 );
         co[0].loadv( i  ,j+1,k  ,t, x,x121 );
         co[0].loadv( i+1,j+1,k  ,t, x,x221 );
                                           
         co[0].loadv( i-1,j-1,k+1,t, x,x002 );
         co[0].loadv( i  ,j-1,k+1,t, x,x102 );
         co[0].loadv( i+1,j-1,k+1,t, x,x202 );
                                           
         co[0].loadv( i-1,j  ,k+1,t, x,x012 );
         co[0].loadv( i  ,j  ,k+1,t, x,x112 );
         co[0].loadv( i+1,j  ,k+1,t, x,x212 );
                                           
         co[0].loadv( i-1,j+1,k+1,t, x,x022 );
         co[0].loadv( i  ,j+1,k+1,t, x,x122 );
         co[0].loadv( i+1,j+1,k+1,t, x,x222 );

         co[0].loadv( i  ,j  ,k  ,t, p,q[8+8] );

         setp3(  x000,  x100,  x200,  x010,  x110,  x210,  x020,  x120,  x220,
                 x001,  x101,  x201,  x011,  x111,  x211,  x021,  x121,  x221,
                 x002,  x102,  x202,  x012,  x112,  x212,  x022,  x122,  x222,  q[8+8],  dp, 0,0,0, 
                 fmin(hx[t].d[0][0],hx[t].d[4][0]), fmin(hx[t].d[0][1],hx[t].d[4][1]), NO_DCNTR, dst );

         res+= fabs( dp[0] );
         res+= fabs( dp[1] );

         q[8+8][0]+= omega*dp[0];
         q[8+8][1]+= omega*dp[1];

         i=bsize-1;
         j=1;
         k=bsize/2;

         co[0].loadv( i-1,j-1,k-1,t, x,x000 );
         co[0].loadv( i  ,j-1,k-1,t, x,x100 );
         co[0].loadv( i+1,j-1,k-1,t, x,x200 );
                                           
         co[0].loadv( i-1,j  ,k-1,t, x,x010 );
         co[0].loadv( i  ,j  ,k-1,t, x,x110 );
         co[0].loadv( i+1,j  ,k-1,t, x,x210 );
                                           
         co[0].loadv( i-1,j+1,k-1,t, x,x020 );
         co[0].loadv( i  ,j+1,k-1,t, x,x120 );
         co[0].loadv( i+1,j+1,k-1,t, x,x220 );
                                           
         co[0].loadv( i-1,j-1,k  ,t, x,x001 );
         co[0].loadv( i  ,j-1,k  ,t, x,x101 );
         co[0].loadv( i+1,j-1,k  ,t, x,x201 );
                                           
         co[0].loadv( i-1,j  ,k  ,t, x,x011 );
         co[0].loadv( i  ,j  ,k  ,t, x,x111 );
         co[0].loadv( i+1,j  ,k  ,t, x,x211 );
                                           
         co[0].loadv( i-1,j+1,k  ,t, x,x021 );
         co[0].loadv( i  ,j+1,k  ,t, x,x121 );
         co[0].loadv( i+1,j+1,k  ,t, x,x221 );
                                           
         co[0].loadv( i-1,j-1,k+1,t, x,x002 );
         co[0].loadv( i  ,j-1,k+1,t, x,x102 );
         co[0].loadv( i+1,j-1,k+1,t, x,x202 );
                                           
         co[0].loadv( i-1,j  ,k+1,t, x,x012 );
         co[0].loadv( i  ,j  ,k+1,t, x,x112 );
         co[0].loadv( i+1,j  ,k+1,t, x,x212 );
                                           
         co[0].loadv( i-1,j+1,k+1,t, x,x022 );
         co[0].loadv( i  ,j+1,k+1,t, x,x122 );
         co[0].loadv( i+1,j+1,k+1,t, x,x222 );

         co[0].loadv( i  ,j  ,k  ,t, p,q[8+9] );

         setp3(  x000,  x100,  x200,  x010,  x110,  x210,  x020,  x120,  x220,
                 x001,  x101,  x201,  x011,  x111,  x211,  x021,  x121,  x221,
                 x002,  x102,  x202,  x012,  x112,  x212,  x022,  x122,  x222,  q[8+9],  dp, 1,0,0, 
                 fmin(hx[t].d[1][0],hx[t].d[5][0]), fmin(hx[t].d[1][1],hx[t].d[5][1]), NO_DCNTR, dst );

         res+= fabs( dp[0] );
         res+= fabs( dp[1] );

         q[8+9][0]+= omega*dp[0];
         q[8+9][1]+= omega*dp[1];

         i=1;
         j=bsize-1;
         k=bsize/2;

         co[0].loadv( i-1,j-1,k-1,t, x,x000 );
         co[0].loadv( i  ,j-1,k-1,t, x,x100 );
         co[0].loadv( i+1,j-1,k-1,t, x,x200 );
                                           
         co[0].loadv( i-1,j  ,k-1,t, x,x010 );
         co[0].loadv( i  ,j  ,k-1,t, x,x110 );
         co[0].loadv( i+1,j  ,k-1,t, x,x210 );
                                           
         co[0].loadv( i-1,j+1,k-1,t, x,x020 );
         co[0].loadv( i  ,j+1,k-1,t, x,x120 );
         co[0].loadv( i+1,j+1,k-1,t, x,x220 );
                                           
         co[0].loadv( i-1,j-1,k  ,t, x,x001 );
         co[0].loadv( i  ,j-1,k  ,t, x,x101 );
         co[0].loadv( i+1,j-1,k  ,t, x,x201 );
                                           
         co[0].loadv( i-1,j  ,k  ,t, x,x011 );
         co[0].loadv( i  ,j  ,k  ,t, x,x111 );
         co[0].loadv( i+1,j  ,k  ,t, x,x211 );
                                           
         co[0].loadv( i-1,j+1,k  ,t, x,x021 );
         co[0].loadv( i  ,j+1,k  ,t, x,x121 );
         co[0].loadv( i+1,j+1,k  ,t, x,x221 );
                                           
         co[0].loadv( i-1,j-1,k+1,t, x,x002 );
         co[0].loadv( i  ,j-1,k+1,t, x,x102 );
         co[0].loadv( i+1,j-1,k+1,t, x,x202 );
                                           
         co[0].loadv( i-1,j  ,k+1,t, x,x012 );
         co[0].loadv( i  ,j  ,k+1,t, x,x112 );
         co[0].loadv( i+1,j  ,k+1,t, x,x212 );
                                           
         co[0].loadv( i-1,j+1,k+1,t, x,x022 );
         co[0].loadv( i  ,j+1,k+1,t, x,x122 );
         co[0].loadv( i+1,j+1,k+1,t, x,x222 );

         co[0].loadv( i  ,j  ,k  ,t, p,q[8+10] );

         setp3(  x000,  x100,  x200,  x010,  x110,  x210,  x020,  x120,  x220,
                 x001,  x101,  x201,  x011,  x111,  x211,  x021,  x121,  x221,
                 x002,  x102,  x202,  x012,  x112,  x212,  x022,  x122,  x222,  q[8+10],  dp, 0,1,0, 
                 fmin(hx[t].d[2][0],hx[t].d[6][0]), fmin(hx[t].d[2][1],hx[t].d[6][1]), NO_DCNTR, dst );

         res+= fabs( dp[0] );
         res+= fabs( dp[1] );

         q[8+10][0]+= omega*dp[0];
         q[8+10][1]+= omega*dp[1];

         i=bsize-1;
         j=bsize-1;
         k=bsize/2;

         co[0].loadv( i-1,j-1,k-1,t, x,x000 );
         co[0].loadv( i  ,j-1,k-1,t, x,x100 );
         co[0].loadv( i+1,j-1,k-1,t, x,x200 );
                                           
         co[0].loadv( i-1,j  ,k-1,t, x,x010 );
         co[0].loadv( i  ,j  ,k-1,t, x,x110 );
         co[0].loadv( i+1,j  ,k-1,t, x,x210 );
                                           
         co[0].loadv( i-1,j+1,k-1,t, x,x020 );
         co[0].loadv( i  ,j+1,k-1,t, x,x120 );
         co[0].loadv( i+1,j+1,k-1,t, x,x220 );
                                           
         co[0].loadv( i-1,j-1,k  ,t, x,x001 );
         co[0].loadv( i  ,j-1,k  ,t, x,x101 );
         co[0].loadv( i+1,j-1,k  ,t, x,x201 );
                                           
         co[0].loadv( i-1,j  ,k  ,t, x,x011 );
         co[0].loadv( i  ,j  ,k  ,t, x,x111 );
         co[0].loadv( i+1,j  ,k  ,t, x,x211 );
                                           
         co[0].loadv( i-1,j+1,k  ,t, x,x021 );
         co[0].loadv( i  ,j+1,k  ,t, x,x121 );
         co[0].loadv( i+1,j+1,k  ,t, x,x221 );
                                           
         co[0].loadv( i-1,j-1,k+1,t, x,x002 );
         co[0].loadv( i  ,j-1,k+1,t, x,x102 );
         co[0].loadv( i+1,j-1,k+1,t, x,x202 );
                                           
         co[0].loadv( i-1,j  ,k+1,t, x,x012 );
         co[0].loadv( i  ,j  ,k+1,t, x,x112 );
         co[0].loadv( i+1,j  ,k+1,t, x,x212 );
                                           
         co[0].loadv( i-1,j+1,k+1,t, x,x022 );
         co[0].loadv( i  ,j+1,k+1,t, x,x122 );
         co[0].loadv( i+1,j+1,k+1,t, x,x222 );

         co[0].loadv( i  ,j  ,k  ,t, p,q[8+11] );

         setp3(  x000,  x100,  x200,  x010,  x110,  x210,  x020,  x120,  x220,
                 x001,  x101,  x201,  x011,  x111,  x211,  x021,  x121,  x221,
                 x002,  x102,  x202,  x012,  x112,  x212,  x022,  x122,  x222,  q[8+11],  dp, 1,1,0, 
                 fmin(hx[t].d[3][0],hx[t].d[7][0]), fmin(hx[t].d[3][1],hx[t].d[7][1]), NO_DCNTR, dst );

         res+= fabs( dp[0] );
         res+= fabs( dp[1] );

         q[8+11][0]+= omega*dp[0];
         q[8+11][1]+= omega*dp[1];

// faces

         i=1;
         j=bsize/2;
         k=bsize/2;

         co[0].loadv( i-1,j-1,k-1,t, x,x000 );
         co[0].loadv( i  ,j-1,k-1,t, x,x100 );
         co[0].loadv( i+1,j-1,k-1,t, x,x200 );
                                           
         co[0].loadv( i-1,j  ,k-1,t, x,x010 );
         co[0].loadv( i  ,j  ,k-1,t, x,x110 );
         co[0].loadv( i+1,j  ,k-1,t, x,x210 );
                                           
         co[0].loadv( i-1,j+1,k-1,t, x,x020 );
         co[0].loadv( i  ,j+1,k-1,t, x,x120 );
         co[0].loadv( i+1,j+1,k-1,t, x,x220 );
                                           
         co[0].loadv( i-1,j-1,k  ,t, x,x001 );
         co[0].loadv( i  ,j-1,k  ,t, x,x101 );
         co[0].loadv( i+1,j-1,k  ,t, x,x201 );
                                           
         co[0].loadv( i-1,j  ,k  ,t, x,x011 );
         co[0].loadv( i  ,j  ,k  ,t, x,x111 );
         co[0].loadv( i+1,j  ,k  ,t, x,x211 );
                                           
         co[0].loadv( i-1,j+1,k  ,t, x,x021 );
         co[0].loadv( i  ,j+1,k  ,t, x,x121 );
         co[0].loadv( i+1,j+1,k  ,t, x,x221 );
                                           
         co[0].loadv( i-1,j-1,k+1,t, x,x002 );
         co[0].loadv( i  ,j-1,k+1,t, x,x102 );
         co[0].loadv( i+1,j-1,k+1,t, x,x202 );
                                           
         co[0].loadv( i-1,j  ,k+1,t, x,x012 );
         co[0].loadv( i  ,j  ,k+1,t, x,x112 );
         co[0].loadv( i+1,j  ,k+1,t, x,x212 );
                                           
         co[0].loadv( i-1,j+1,k+1,t, x,x022 );
         co[0].loadv( i  ,j+1,k+1,t, x,x122 );
         co[0].loadv( i+1,j+1,k+1,t, x,x222 );

         co[0].loadv( i  ,j  ,k  ,t, p,q[8+12+0] );

         setp3(  x000,  x100,  x200,  x010,  x110,  x210,  x020,  x120,  x220,
                 x001,  x101,  x201,  x011,  x111,  x211,  x021,  x121,  x221,
                 x002,  x102,  x202,  x012,  x112,  x212,  x022,  x122,  x222,  q[8+12],  dp, 0,0,0, 
                 fmin(fmin(hx[t].d[0][0],hx[t].d[2][0]),fmin(hx[t].d[4][0],hx[t].d[6][0])), NO_DCNTR,NO_DCNTR, dst );

         res+= fabs( dp[0] );

         q[8+12+0][0]+= omega*dp[0];

         i=bsize-1;
         j=bsize/2;
         k=bsize/2;

         co[0].loadv( i-1,j-1,k-1,t, x,x000 );
         co[0].loadv( i  ,j-1,k-1,t, x,x100 );
         co[0].loadv( i+1,j-1,k-1,t, x,x200 );
                                           
         co[0].loadv( i-1,j  ,k-1,t, x,x010 );
         co[0].loadv( i  ,j  ,k-1,t, x,x110 );
         co[0].loadv( i+1,j  ,k-1,t, x,x210 );
                                           
         co[0].loadv( i-1,j+1,k-1,t, x,x020 );
         co[0].loadv( i  ,j+1,k-1,t, x,x120 );
         co[0].loadv( i+1,j+1,k-1,t, x,x220 );
                                           
         co[0].loadv( i-1,j-1,k  ,t, x,x001 );
         co[0].loadv( i  ,j-1,k  ,t, x,x101 );
         co[0].loadv( i+1,j-1,k  ,t, x,x201 );
                                           
         co[0].loadv( i-1,j  ,k  ,t, x,x011 );
         co[0].loadv( i  ,j  ,k  ,t, x,x111 );
         co[0].loadv( i+1,j  ,k  ,t, x,x211 );
                                           
         co[0].loadv( i-1,j+1,k  ,t, x,x021 );
         co[0].loadv( i  ,j+1,k  ,t, x,x121 );
         co[0].loadv( i+1,j+1,k  ,t, x,x221 );
                                           
         co[0].loadv( i-1,j-1,k+1,t, x,x002 );
         co[0].loadv( i  ,j-1,k+1,t, x,x102 );
         co[0].loadv( i+1,j-1,k+1,t, x,x202 );
                                           
         co[0].loadv( i-1,j  ,k+1,t, x,x012 );
         co[0].loadv( i  ,j  ,k+1,t, x,x112 );
         co[0].loadv( i+1,j  ,k+1,t, x,x212 );
                                           
         co[0].loadv( i-1,j+1,k+1,t, x,x022 );
         co[0].loadv( i  ,j+1,k+1,t, x,x122 );
         co[0].loadv( i+1,j+1,k+1,t, x,x222 );

         co[0].loadv( i  ,j  ,k  ,t, p,q[8+12+1] );

         setp3(  x000,  x100,  x200,  x010,  x110,  x210,  x020,  x120,  x220,
                 x001,  x101,  x201,  x011,  x111,  x211,  x021,  x121,  x221,
                 x002,  x102,  x202,  x012,  x112,  x212,  x022,  x122,  x222,  q[8+13],  dp, 1,0,0, 
                 fmin(fmin(hx[t].d[1][0],hx[t].d[3][0]),fmin(hx[t].d[5][0],hx[t].d[7][0])), NO_DCNTR,NO_DCNTR, dst );

         res+= fabs( dp[0] );

         q[8+12+1][0]+= omega*dp[0];

         i=bsize/2;
         j=1;
         k=bsize/2;

         co[0].loadv( i-1,j-1,k-1,t, x,x000 );
         co[0].loadv( i  ,j-1,k-1,t, x,x100 );
         co[0].loadv( i+1,j-1,k-1,t, x,x200 );
                                           
         co[0].loadv( i-1,j  ,k-1,t, x,x010 );
         co[0].loadv( i  ,j  ,k-1,t, x,x110 );
         co[0].loadv( i+1,j  ,k-1,t, x,x210 );
                                           
         co[0].loadv( i-1,j+1,k-1,t, x,x020 );
         co[0].loadv( i  ,j+1,k-1,t, x,x120 );
         co[0].loadv( i+1,j+1,k-1,t, x,x220 );
                                           
         co[0].loadv( i-1,j-1,k  ,t, x,x001 );
         co[0].loadv( i  ,j-1,k  ,t, x,x101 );
         co[0].loadv( i+1,j-1,k  ,t, x,x201 );
                                           
         co[0].loadv( i-1,j  ,k  ,t, x,x011 );
         co[0].loadv( i  ,j  ,k  ,t, x,x111 );
         co[0].loadv( i+1,j  ,k  ,t, x,x211 );
                                           
         co[0].loadv( i-1,j+1,k  ,t, x,x021 );
         co[0].loadv( i  ,j+1,k  ,t, x,x121 );
         co[0].loadv( i+1,j+1,k  ,t, x,x221 );
                                           
         co[0].loadv( i-1,j-1,k+1,t, x,x002 );
         co[0].loadv( i  ,j-1,k+1,t, x,x102 );
         co[0].loadv( i+1,j-1,k+1,t, x,x202 );
                                           
         co[0].loadv( i-1,j  ,k+1,t, x,x012 );
         co[0].loadv( i  ,j  ,k+1,t, x,x112 );
         co[0].loadv( i+1,j  ,k+1,t, x,x212 );
                                           
         co[0].loadv( i-1,j+1,k+1,t, x,x022 );
         co[0].loadv( i  ,j+1,k+1,t, x,x122 );
         co[0].loadv( i+1,j+1,k+1,t, x,x222 );

         co[0].loadv( i  ,j  ,k  ,t, p,q[8+12+2] );

         setp3(  x000,  x100,  x200,  x010,  x110,  x210,  x020,  x120,  x220,
                 x001,  x101,  x201,  x011,  x111,  x211,  x021,  x121,  x221,
                 x002,  x102,  x202,  x012,  x112,  x212,  x022,  x122,  x222,  q[8+14],  dp, 0,0,0, 
                 NO_DCNTR,fmin(fmin(hx[t].d[0][1],hx[t].d[1][1]),fmin(hx[t].d[4][1],hx[t].d[5][1])), NO_DCNTR, dst );

         res+= fabs( dp[1] );

         q[8+12+2][1]+= omega*dp[1];

         i=bsize/2;
         j=bsize-1;
         k=bsize/2;

         co[0].loadv( i-1,j-1,k-1,t, x,x000 );
         co[0].loadv( i  ,j-1,k-1,t, x,x100 );
         co[0].loadv( i+1,j-1,k-1,t, x,x200 );
                                           
         co[0].loadv( i-1,j  ,k-1,t, x,x010 );
         co[0].loadv( i  ,j  ,k-1,t, x,x110 );
         co[0].loadv( i+1,j  ,k-1,t, x,x210 );
                                           
         co[0].loadv( i-1,j+1,k-1,t, x,x020 );
         co[0].loadv( i  ,j+1,k-1,t, x,x120 );
         co[0].loadv( i+1,j+1,k-1,t, x,x220 );
                                           
         co[0].loadv( i-1,j-1,k  ,t, x,x001 );
         co[0].loadv( i  ,j-1,k  ,t, x,x101 );
         co[0].loadv( i+1,j-1,k  ,t, x,x201 );
                                           
         co[0].loadv( i-1,j  ,k  ,t, x,x011 );
         co[0].loadv( i  ,j  ,k  ,t, x,x111 );
         co[0].loadv( i+1,j  ,k  ,t, x,x211 );
                                           
         co[0].loadv( i-1,j+1,k  ,t, x,x021 );
         co[0].loadv( i  ,j+1,k  ,t, x,x121 );
         co[0].loadv( i+1,j+1,k  ,t, x,x221 );
                                           
         co[0].loadv( i-1,j-1,k+1,t, x,x002 );
         co[0].loadv( i  ,j-1,k+1,t, x,x102 );
         co[0].loadv( i+1,j-1,k+1,t, x,x202 );
                                           
         co[0].loadv( i-1,j  ,k+1,t, x,x012 );
         co[0].loadv( i  ,j  ,k+1,t, x,x112 );
         co[0].loadv( i+1,j  ,k+1,t, x,x212 );
                                           
         co[0].loadv( i-1,j+1,k+1,t, x,x022 );
         co[0].loadv( i  ,j+1,k+1,t, x,x122 );
         co[0].loadv( i+1,j+1,k+1,t, x,x222 );

         co[0].loadv( i  ,j  ,k  ,t, p,q[8+12+3] );

         setp3(  x000,  x100,  x200,  x010,  x110,  x210,  x020,  x120,  x220,
                 x001,  x101,  x201,  x011,  x111,  x211,  x021,  x121,  x221,
                 x002,  x102,  x202,  x012,  x112,  x212,  x022,  x122,  x222,  q[8+15],  dp, 0,1,0, 
                 NO_DCNTR,fmin(fmin(hx[t].d[2][1],hx[t].d[3][1]),fmin(hx[t].d[6][1],hx[t].d[7][1])), NO_DCNTR, dst );

         res+= fabs( dp[1] );

         q[8+12+3][1]+= omega*dp[1];

         i=bsize/2;
         j=bsize/2;
         k=1;

         co[0].loadv( i-1,j-1,k-1,t, x,x000 );
         co[0].loadv( i  ,j-1,k-1,t, x,x100 );
         co[0].loadv( i+1,j-1,k-1,t, x,x200 );
                                           
         co[0].loadv( i-1,j  ,k-1,t, x,x010 );
         co[0].loadv( i  ,j  ,k-1,t, x,x110 );
         co[0].loadv( i+1,j  ,k-1,t, x,x210 );
                                           
         co[0].loadv( i-1,j+1,k-1,t, x,x020 );
         co[0].loadv( i  ,j+1,k-1,t, x,x120 );
         co[0].loadv( i+1,j+1,k-1,t, x,x220 );
                                           
         co[0].loadv( i-1,j-1,k  ,t, x,x001 );
         co[0].loadv( i  ,j-1,k  ,t, x,x101 );
         co[0].loadv( i+1,j-1,k  ,t, x,x201 );
                                           
         co[0].loadv( i-1,j  ,k  ,t, x,x011 );
         co[0].loadv( i  ,j  ,k  ,t, x,x111 );
         co[0].loadv( i+1,j  ,k  ,t, x,x211 );
                                           
         co[0].loadv( i-1,j+1,k  ,t, x,x021 );
         co[0].loadv( i  ,j+1,k  ,t, x,x121 );
         co[0].loadv( i+1,j+1,k  ,t, x,x221 );
                                           
         co[0].loadv( i-1,j-1,k+1,t, x,x002 );
         co[0].loadv( i  ,j-1,k+1,t, x,x102 );
         co[0].loadv( i+1,j-1,k+1,t, x,x202 );
                                           
         co[0].loadv( i-1,j  ,k+1,t, x,x012 );
         co[0].loadv( i  ,j  ,k+1,t, x,x112 );
         co[0].loadv( i+1,j  ,k+1,t, x,x212 );
                                           
         co[0].loadv( i-1,j+1,k+1,t, x,x022 );
         co[0].loadv( i  ,j+1,k+1,t, x,x122 );
         co[0].loadv( i+1,j+1,k+1,t, x,x222 );

         co[0].loadv( i  ,j  ,k  ,t, p,q[8+12+4] );

         setp3(  x000,  x100,  x200,  x010,  x110,  x210,  x020,  x120,  x220,
                 x001,  x101,  x201,  x011,  x111,  x211,  x021,  x121,  x221,
                 x002,  x102,  x202,  x012,  x112,  x212,  x022,  x122,  x222,  q[8+16],  dp, 0,0,0, 
                 NO_DCNTR,NO_DCNTR, fmin(fmin(hx[t].d[0][2],hx[t].d[1][2]),fmin(hx[t].d[2][2],hx[t].d[3][2])), dst );

         res+= fabs( dp[2] );

         q[8+12+4][2]+= omega*dp[2];

         i=bsize/2;
         j=bsize/2;
         k=bsize-1;

         co[0].loadv( i-1,j-1,k-1,t, x,x000 );
         co[0].loadv( i  ,j-1,k-1,t, x,x100 );
         co[0].loadv( i+1,j-1,k-1,t, x,x200 );
                                           
         co[0].loadv( i-1,j  ,k-1,t, x,x010 );
         co[0].loadv( i  ,j  ,k-1,t, x,x110 );
         co[0].loadv( i+1,j  ,k-1,t, x,x210 );
                                           
         co[0].loadv( i-1,j+1,k-1,t, x,x020 );
         co[0].loadv( i  ,j+1,k-1,t, x,x120 );
         co[0].loadv( i+1,j+1,k-1,t, x,x220 );
                                           
         co[0].loadv( i-1,j-1,k  ,t, x,x001 );
         co[0].loadv( i  ,j-1,k  ,t, x,x101 );
         co[0].loadv( i+1,j-1,k  ,t, x,x201 );
                                           
         co[0].loadv( i-1,j  ,k  ,t, x,x011 );
         co[0].loadv( i  ,j  ,k  ,t, x,x111 );
         co[0].loadv( i+1,j  ,k  ,t, x,x211 );
                                           
         co[0].loadv( i-1,j+1,k  ,t, x,x021 );
         co[0].loadv( i  ,j+1,k  ,t, x,x121 );
         co[0].loadv( i+1,j+1,k  ,t, x,x221 );
                                           
         co[0].loadv( i-1,j-1,k+1,t, x,x002 );
         co[0].loadv( i  ,j-1,k+1,t, x,x102 );
         co[0].loadv( i+1,j-1,k+1,t, x,x202 );
                                           
         co[0].loadv( i-1,j  ,k+1,t, x,x012 );
         co[0].loadv( i  ,j  ,k+1,t, x,x112 );
         co[0].loadv( i+1,j  ,k+1,t, x,x212 );
                                           
         co[0].loadv( i-1,j+1,k+1,t, x,x022 );
         co[0].loadv( i  ,j+1,k+1,t, x,x122 );
         co[0].loadv( i+1,j+1,k+1,t, x,x222 );

         co[0].loadv( i  ,j  ,k  ,t, p,q[8+12+5] );

         setp3(  x000,  x100,  x200,  x010,  x110,  x210,  x020,  x120,  x220,
                 x001,  x101,  x201,  x011,  x111,  x211,  x021,  x121,  x221,
                 x002,  x102,  x202,  x012,  x112,  x212,  x022,  x122,  x222,  q[8+17],  dp, 0,0,1, 
                 NO_DCNTR,NO_DCNTR, fmin(fmin(hx[t].d[4][2],hx[t].d[5][2]),fmin(hx[t].d[6][2],hx[t].d[7][2])), dst );

         res+= fabs( dp[2] );

         q[8+12+5][2]+= omega*dp[2];

	 REAL_9 r;

         for( k=1;k<bsize;k++ )
        {
            for( j=1;j<bsize;j++ )
           {
               for( i=1;i<bsize;i++ )
              {
                  u= (REAL_)    (i-1)/(bsize-2);     
                  v= (REAL_)    (j-1)/(bsize-2);     
                  w= (REAL_)    (k-1)/(bsize-2);     

                  ql22( v,w,r );

                  f[0]= ( q[ 0+0][0]*r[0  ]+ q[ 8+4][0]*r[1  ]+ q[ 0+2][0]*r[2  ]+
                          q[ 8+8][0]*r[0+3]+ q[20+0][0]*r[1+3]+ q[10+8][0]*r[2+3]+
                          q[ 0+4][0]*r[0+6]+ q[ 8+6][0]*r[1+6]+ q[ 0+6][0]*r[2+6] )*(1-u)+
                        ( q[ 0+1][0]*r[0  ]+ q[ 8+5][0]*r[1  ]+ q[ 0+3][0]*r[2  ]+
                          q[ 8+9][0]*r[0+3]+ q[20+1][0]*r[1+3]+ q[11+8][0]*r[2+3]+
                          q[ 0+5][0]*r[0+6]+ q[ 8+7][0]*r[1+6]+ q[ 0+7][0]*r[2+6] )* u;

                  ql22( u,w,r );

                  f[1]= ( q[ 0+0][1]*r[0  ]+ q[ 8+0][1]*r[1  ]+ q[ 0+1][1]*r[2  ]+
                          q[ 8+8][1]*r[0+3]+ q[20+2][1]*r[1+3]+ q[ 9+8][1]*r[2+3]+
                          q[ 0+4][1]*r[0+6]+ q[ 8+2][1]*r[1+6]+ q[ 0+5][1]*r[2+6] )*(1-v)+
                        ( q[ 0+2][1]*r[0  ]+ q[ 8+1][1]*r[1  ]+ q[ 0+3][1]*r[2  ]+
                          q[10+8][1]*r[0+3]+ q[20+3][1]*r[1+3]+ q[11+8][1]*r[2+3]+
                          q[ 0+6][1]*r[0+6]+ q[ 8+3][1]*r[1+6]+ q[ 0+7][1]*r[2+6] )* v;

                  ql22( u,v,r );

                  f[2]= ( q[ 0+0][2]*r[0  ]+ q[ 8+0][2]*r[1  ]+ q[ 0+1][2]*r[2  ]+
                          q[ 8+4][2]*r[0+3]+ q[20+4][2]*r[1+3]+ q[ 8+5][2]*r[2+3]+
                          q[ 0+2][2]*r[0+6]+ q[ 8+1][2]*r[1+6]+ q[ 0+3][2]*r[2+6] )*(1-w)+
                        ( q[ 0+4][2]*r[0  ]+ q[ 8+2][2]*r[1  ]+ q[ 0+5][2]*r[2  ]+
                          q[ 8+6][2]*r[0+3]+ q[20+5][2]*r[1+3]+ q[ 8+7][2]*r[2+3]+
                          q[ 0+6][2]*r[0+6]+ q[ 8+3][2]*r[1+6]+ q[ 0+7][2]*r[2+6] )* w;


                  co[0].storev( i  ,j  ,k  ,t, p,f );

              }
           }
        }

     }

      return;
  }    
