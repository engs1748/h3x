#  include <q3x/q3x.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

#  include <q3x/inline/ellp>
#  include <q3x/inline/ellb>

   void q3x_t::smob( REAL_ *y, REAL_ *dy, REAL_ *x, REAL_ *r, REAL_ *g, vsurf_t &su, REAL_ rlx )
  {


      INT_       b,c;
      INT_       i,j, l,m,n, s,t;
      INT_       ist,ien;
      frule_t    r0,r1,r2,r3,r4,r5;
      frule_t    rr;



      REAL_3     f;
      REAL_2     z;

      REAL_3     q[3],w;
      REAL_3     u;
      REAL_2     dz;

      vtx_t      v;
      vtx_t      dv[2];

// update loop


      assert( bsize < 1024 );

// conformity at vertices

// enforce boundary conformity

      ien= 0;
      for( b=0;b<BTYPES;b++ )
     {
         ist= ien;
         ien= bn.l[b][0];
         for( t=ist;t<ien;t++ )
        {

            r0= frule_t( bn.f[t], bsize+1,bsize+1,bsize+1 );
            c= bn.h[t];
            s= bn.s[t];
            if( c > -1 )
           {
               for( j=1;j<bsize;j++ )
              {
                  for( i=1;i<bsize;i++ )
                 {
             
                     r0.transform( i,j,0, l,m,n );
             
                     co[0].loadv(l,m,n,c,   x, u );
                     co[0].loadv(l,m,n,c,   r, f );
             
                     cb.loadv( i,j, t,  y, z );
             
                     su[s]->interp( z, v,dv );
             
                     cross3( dv[0].x,dv[1].x, w ); 
                     REAL_ ww= norm32( w ); scal3( 1./ww,w );
             
                     u[0]= v[0]-u[0]-f[0];
                     u[1]= v[1]-u[1]-f[1];
                     u[2]= v[2]-u[2]-f[2];
             
                     ww= dot3( u,w );
                     f[0]+= ww*w[0];
                     f[1]+= ww*w[1];
                     f[2]+= ww*w[2];

                     co[0].storev( l,m,n,c, r,f );
             
                 }
              }
           }
        }
     }

      ien= 0;
      for( b=0;b<BTYPES;b++ )
     {
         ist= ien;
         ien= bn.l[b][0];
         for( t=ist;t<ien;t++ )
        {

            r0= frule_t( bn.f[t], bsize+1,bsize+1,bsize+1 );
            c= bn.h[t];
            s= bn.s[t];
            if( c > -1 )
           {
               for( j=0;j<bsize+1;j++ )
              {
                  for( i=0;i<bsize+1;i++ )
                 {
             
                     r0.transform( i,j,0, l,m,n );
             
                     co[0].loadv(l,m,n,c,   x, u );
                     co[0].loadv(l,m,n,c,   r, f );
             
                     cb.loadv( i,j, t,  y, z );
             
                     su[s]->interp( z, v,dv );

                     f[0]+= u[0]- v[0];
                     f[1]+= u[1]- v[1];
                     f[2]+= u[2]- v[2];

                     qrf23( dv[0].x,dv[1].x, q[0],q[1], u );
                     qrs23( q[0],q[1], u, dz, f );

                     z[0]+= rlx*dz[0];
                     z[1]+= rlx*dz[1];

                     cb.storev( i,j,  t, y,z );
                 }
              }
           }
        }
     }


      return;
  }


   void q3x_t::smo1( REAL_ *y, REAL_ *dy, REAL_ *x, REAL_ *r, REAL_ *g, vsurf_t &su )
  {


      INT_       b,c;
      INT_       ist,ien;
      frule_t    r0;
      INT_       i,j, l,m,n, s,t;
      frule_t    rr;

      REAL_3     f;
      REAL_2     z;


      vtx_t      v;
      vtx_t      dv[2];

// update loop

      return;
      assert( bsize < 1024 );

// conformity at vertices

// enforce boundary conformity

      ien= 0;
      for( b=0;b<BTYPES;b++ )
     {
         ist= ien;
         ien= bn.l[b][0];
         for( t=ist;t<ien;t++ )
        {

            r0= frule_t( bn.f[t], bsize+1,bsize+1,bsize+1 );
            c= bn.h[t];
            s= bn.s[t];
            if( c > -1 )
           {
               for( j=0;j<bsize+1;j++ )
              {
                  for( i=0;i<bsize+1;i++ )
                 {
             
                     r0.transform( i,j,0, l,m,n );
             
                     cb.loadv( i,j, t,  y, z );
             
                     su[s]->interp( z, v,dv );
             
                     f[0]=v[0];
                     f[1]=v[1];
                     f[2]=v[2];
                     co[0].storev( l,m,n,c, x,f );
             
                 }
              }
           }
        }
     }


      return;
  }


