
#  include <q3x/q3x.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::ort()
  {

      REAL_          *x=NULL, *d=NULL, *r=NULL;
      REAL_          *p=NULL;
      REAL_          *y=NULL, *dy=NULL;

      INT_           it;
      INT_           g,j,b;
      INT_           ien,ist;

      double         stime,etime;

                      REAL_          rlx=0.01;
      gas.init();

      start();

      co[3].malloc( &x  );
      co[3].malloc( &r  );
      co[3].malloc( &p  );
         cb.malloc( &y  );
         cb.malloc( &dy  );

      ca[3].malloc( &d  );

      grid( x,y, p );

      stime=MPI_Wtime();

      for( it=0;it<1000;it++ )
     {

         ort0( y,dy, x,r, d );

         smov( y,dy, x,r, d );
         smoe( y,dy, x,r, d );
         smob( y,dy, x,r, d, rlx );

         REAL_4 res={0,0,0,0};
         for( INT_ t=0;t<hx.n;t++ )
        {
            for( INT_ k=0;k<bsize+1;k++ )
           {
               for( INT_ j=0;j<bsize+1;j++ )
              {
                  for( INT_ i=0;i<bsize+1;i++ )
                 {
                     REAL_3         f;
                   
                     co[0].loadv( i,  j,  k, t, r, f );


                     res[0]+= fabs(f[0]);
                     res[1]+= fabs(f[1]);
                     res[2]+= fabs(f[2]);

                     res[3]++;

                     f[0]*= rlx;
                     f[1]*= rlx;
                     f[2]*= rlx;

                     co[0].addv( i,j,k,t, x,f );

                 }
              }
           }
        }
         printf( "%4d % 9.3e % 9.3e % 9.3e\n", it,res[0],res[1],res[2] );
     }

      FILE *f;
      char  name[512];
      size_t len= co[0].bsize(4);
      size_t off;

      printf( "saving?\n" );

      for( INT_ l=0;l<hx.n;l++ )
     {

         off= co[0].addr( 0,0,0,0,l );
         sprintf( name, "grid/%06d", hx[l].id );
         f= fopen( name, "w" );
       ::fwrite( &bsize,    1,sizeof(INT_), f );
       ::fwrite( &len,      1,sizeof(size_t), f );
       ::fwrite(  x+off,len,sizeof(REAL_),f );
       ::fwrite(  p+off,len,sizeof(REAL_),f );
         fclose( f );
     }

      len= cb.bsize(3);
      ien=0;
      for( INT_ k=0;k<BTYPES;k++ )
     {
         ist=ien;
         ien= bn.l[k][0];

         for( j=ist;j<ien;j++ )
        {

            g= bn.g[j];
            b= bn.n[j];
            off= cb.addr( 0,0,0,j );
            sprintf( name, "bgrid/%06d.%06d", g,b  );
            f= fopen( name, "w" );
          ::fwrite( &bsize,    1,sizeof(INT_), f );
          ::fwrite( &len,      1,sizeof(size_t), f );
          ::fwrite(  y+off,len,sizeof(REAL_),f );
        }
     }

      etime=MPI_Wtime();

      if( rank==0 )
     {
         printf( "elapsed time %9.3e\n", etime-stime );
     }

      delete[] x;
      delete[] y;
      delete[] r;
      delete[] d;

      stop();

      return;
  }
