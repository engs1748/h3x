
#  include <q3x/q3x.h>
#  include <q2.h>


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::trln( INT_ n )
  {
      MMAP_      coo;
      MMAP_      cob;
      INT_         i,j,k,l;
      INT_         i0,i1,i2;

      REAL_       *x; // coordinates
      REAL_       *p; // control functions

      REAL_       *y; // surface coordinates

      REAL_        xi[3];
      REAL_        x0[8][3];
      REAL_        y0[4][3];
      REAL_        w[8];

      size_t len=0;
      coo.dims( len, n+1,3,n+1,n+1, -1 );

      x= new REAL_[len];
      p= new REAL_[len];

      for( l=0;l<hx.n;l++ )
     {
         memset( x,0,len*sizeof(REAL_) );
  

         for( k=0;k<8;k++ )
        {
            j= hx[l].v[k];
            x0[k][0]= vx[j][0];
            x0[k][1]= vx[j][1];
            x0[k][2]= vx[j][2];
        }

         for( k=0; k<n+1;k++ )
        {
            for( j=0; j<n+1;j++ )
           {
               for( i=0; i<n+1;i++ )
              {
                  i0= coo.addr(0,i,j,k);
                  i1= coo.addr(1,i,j,k);
                  i2= coo.addr(2,i,j,k);
          
                  xi[0]= ((REAL_)i)/((REAL_)n);
                  xi[1]= ((REAL_)j)/((REAL_)n);
                  xi[2]= ((REAL_)k)/((REAL_)n);
            
                  w[0]= (1-xi[0])*(1-xi[1])*(1-xi[2]);
                  w[1]=    xi[0] *(1-xi[1])*(1-xi[2]);
                  w[2]= (1-xi[0])*   xi[1] *(1-xi[2]);
                  w[3]=    xi[0] *   xi[1] *(1-xi[2]);
                  w[4]= (1-xi[0])*(1-xi[1])*   xi[2] ;
                  w[5]=    xi[0] *(1-xi[1])*   xi[2] ;
                  w[6]= (1-xi[0])*   xi[1] *   xi[2] ;
                  w[7]=    xi[0] *   xi[1] *   xi[2] ;
 
                  REAL_ ww[27];
                  q222( xi[0],xi[1],xi[2], ww );

                  x[i0]= w[0]*x0[0][0]+ w[1]*x0[1][0]+ w[2]*x0[2][0]+ w[3]*x0[3][0]+ w[4]*x0[4][0]+ w[5]*x0[5][0]+ w[6]*x0[6][0]+ w[7]*x0[7][0];
                  x[i1]= w[0]*x0[0][1]+ w[1]*x0[1][1]+ w[2]*x0[2][1]+ w[3]*x0[3][1]+ w[4]*x0[4][1]+ w[5]*x0[5][1]+ w[6]*x0[6][1]+ w[7]*x0[7][1];
                  x[i2]= w[0]*x0[0][2]+ w[1]*x0[1][2]+ w[2]*x0[2][2]+ w[3]*x0[3][2]+ w[4]*x0[4][2]+ w[5]*x0[5][2]+ w[6]*x0[6][2]+ w[7]*x0[7][2];

                  p[i0]= 0;
                  p[i1]= 0;
                  p[i2]= 0;

              }
           }
        }

// now build control functions

         FILE *f=NULL;
         char  name[512];
         sprintf( name, "grid/%06d", hx[l].id );
         f= fopen( name, "w" );
         assert(f);
       ::fwrite( &n,        1,sizeof(INT_), f );
       ::fwrite( &len,      1,sizeof(size_t), f );
       ::fwrite(  x,len,sizeof(REAL_),f );
       ::fwrite(  p,len,sizeof(REAL_),f );
         fclose( f );

     }

      delete[] x; x= NULL;

      len=0;
      cob.dims( len, n+1,2,n+1,     -1 );
      y= new REAL_[len];

      for( INT_ g=0;g<nb;g++ )
     {
 
         for( k=0;k<bz[g].bo.n;k++ )
        {
            memset( y,0,len*sizeof(REAL_) );

            INT_ h= bz[g].bo[k].h;
            INT_ q= bz[g].bo[k].q;

            INT_ r= hx[h].q[q].r;
            bool o= hx[h].q[q].o;

            INT_4 buf[2];
            INT_4 bvf[2];
            hx[h].pack( q, buf[0],buf[1] );
            hx[h].npack( q, bvf[0],bvf[1] );

            j= bz[g].bo[k].v[0]; j= bz[g].bx[j].id; assert( j == bvf[1][0] );
            j= bz[g].bo[k].v[1]; j= bz[g].bx[j].id; assert( j == bvf[1][1] );
            j= bz[g].bo[k].v[2]; j= bz[g].bx[j].id; assert( j == bvf[1][2] );
            j= bz[g].bo[k].v[3]; j= bz[g].bx[j].id; assert( j == bvf[1][3] );

            for(i=0;i<4;i++ )
           {

               j= mqp[o][r][i];
               j= bz[g].bo[k].v[j];

               y0[i][0]= bz[g].bx[j].y[0];
               y0[i][1]= bz[g].bx[j].y[1];
           } 


            for( j=0; j<n+1;j++ )
           {
               for( i=0; i<n+1;i++ )
              {
                  i0= cob.addr(0,i,j);
                  i1= cob.addr(1,i,j);
          
                  xi[0]= ((REAL_)i)/((REAL_)n);
                  xi[1]= ((REAL_)j)/((REAL_)n);
            
                  w[0]= (1-xi[0])*(1-xi[1]);
                  w[1]=    xi[0] *(1-xi[1]);
                  w[2]= (1-xi[0])*   xi[1] ;
                  w[3]=    xi[0] *   xi[1] ;

                  y[i0]= w[0]*y0[0][0]+ w[1]*y0[1][0]+ w[2]*y0[3][0]+ w[3]*y0[2][0];
                  y[i1]= w[0]*y0[0][1]+ w[1]*y0[1][1]+ w[2]*y0[3][1]+ w[3]*y0[2][1];

              }
           }
            FILE *f= NULL;
            char  name[512];
            sprintf( name, "bgrid/%06d.%06d", g,k );
            f= fopen( name, "w" );
            assert( f );
          ::fwrite( &n,        1,sizeof(INT_), f );
          ::fwrite( &len,      1,sizeof(size_t), f );
          ::fwrite(  y,len,sizeof(REAL_),f );
            fclose( f );
        }
     }
      delete[] y; y= NULL;

  }
