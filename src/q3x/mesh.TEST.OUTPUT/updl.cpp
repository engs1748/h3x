
#  include <q3x/q3x.h>


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::updl( INT_ n )
  {
      MMAP_      coo;
      MMAP_      cob;
      INT_         i,j,k,l;
      REAL_       *x;
      REAL_       *y;

      REAL_3       x000, x100, x010, x110, x001, x101, x011, x111;
      REAL_2       y0[4];

      size_t len=0;
      coo.dims( len, n+1,3,n+1,n+1, -1 );

      x= new REAL_[len];

      for( l=0;l<hx.n;l++ )
     {
         memset( x,0,len*sizeof(REAL_) );

         FILE *f=NULL;
         char  name[512];
         sprintf( name, "grid/%06d", hx[l].id );
         f= fopen( name, "r" );
         assert(f);
       ::fread( &n,        1,sizeof(INT_), f );
       ::fread( &len,      1,sizeof(size_t), f );
       ::fread(  x,len,sizeof(REAL_),f );
         fclose( f );

         coo.loadv(0,0,0,x,x000);
         coo.loadv(n,0,0,x,x100);
         coo.loadv(0,n,0,x,x010);
         coo.loadv(n,n,0,x,x110);
         coo.loadv(0,0,n,x,x001);
         coo.loadv(n,0,n,x,x101);
         coo.loadv(0,n,n,x,x011);
         coo.loadv(n,n,n,x,x111);
 
         i=hx[l].v[0];  vx[i][0]=x000[0];vx[i][1]=x000[1];vx[i][2]=x000[2];
         i=hx[l].v[1];  vx[i][0]=x100[0];vx[i][1]=x100[1];vx[i][2]=x100[2];
         i=hx[l].v[2];  vx[i][0]=x010[0];vx[i][1]=x010[1];vx[i][2]=x010[2];
         i=hx[l].v[3];  vx[i][0]=x110[0];vx[i][1]=x110[1];vx[i][2]=x110[2];
         i=hx[l].v[4];  vx[i][0]=x001[0];vx[i][1]=x001[1];vx[i][2]=x001[2];
         i=hx[l].v[5];  vx[i][0]=x101[0];vx[i][1]=x101[1];vx[i][2]=x101[2];
         i=hx[l].v[6];  vx[i][0]=x011[0];vx[i][1]=x011[1];vx[i][2]=x011[2];
         i=hx[l].v[7];  vx[i][0]=x111[0];vx[i][1]=x111[1];vx[i][2]=x111[2];

     }

      delete[] x; x= NULL;

      len=0;
      cob.dims( len, n+1,2,n+1,     -1 );
      y= new REAL_[len];

      printf( "nb  %d\n", nb );
 
      for( INT_ g=0;g<nb;g++ )
     {
 
         for( k=0;k<bz[g].bo.n;k++ )
        {
            memset( y,0,len*sizeof(REAL_) );

            FILE *f= NULL;
            char  name[512];
            sprintf( name, "bgrid/%06d.%06d", g,k );
            f= fopen( name, "r" );
            assert( f );
          ::fwrite( &n,        1,sizeof(INT_), f );
          ::fwrite( &len,      1,sizeof(size_t), f );
          ::fwrite(  y,len,sizeof(REAL_),f );
            fclose( f );
 
            cob.loadv(0,0,y,y0[0]);
            cob.loadv(n,0,y,y0[1]);
            cob.loadv(n,n,y,y0[2]);
            cob.loadv(0,n,y,y0[3]);
 
            INT_ h= bz[g].bo[k].h;
            INT_ q= bz[g].bo[k].q;

            INT_ r= hx[h].q[q].r;
            bool o= hx[h].q[q].o;

/*          j= bz[g].bo[k].v[ mqp1[o][r][0] ]; bz[g].bx[j].y[0]=y00[0]; bz[g].bx[j].y[1]=y00[1];
            j= bz[g].bo[k].v[ mqp1[o][r][1] ]; bz[g].bx[j].y[0]=y10[0]; bz[g].bx[j].y[1]=y10[1];
            j= bz[g].bo[k].v[ mqp1[o][r][2] ]; bz[g].bx[j].y[0]=y11[0]; bz[g].bx[j].y[1]=y11[1];
            j= bz[g].bo[k].v[ mqp1[o][r][3] ]; bz[g].bx[j].y[0]=y01[0]; bz[g].bx[j].y[1]=y01[1];*/

            INT_4 buf[2];
            INT_4 bvf[2];
            hx[h].pack( q, buf[0],buf[1] );
            hx[h].npack( q, bvf[0],bvf[1] );

            j= bz[g].bo[k].v[0]; j= bz[g].bx[j].id; assert( j == bvf[1][0] );
            j= bz[g].bo[k].v[1]; j= bz[g].bx[j].id; assert( j == bvf[1][1] );
            j= bz[g].bo[k].v[2]; j= bz[g].bx[j].id; assert( j == bvf[1][2] );
            j= bz[g].bo[k].v[3]; j= bz[g].bx[j].id; assert( j == bvf[1][3] );

            for(i=0;i<4;i++ )
           {

               j= mqp[o][r][i];
               j= bz[g].bo[k].v[j];

               y0[i][0]= bz[g].bx[j].y[0];
               y0[i][1]= bz[g].bx[j].y[1];
           }


        }
     }
      delete[] y; y= NULL;

  }
