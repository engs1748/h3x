
#  include <q3x/q3x.h>


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::refn( INT_ n )
  {
      MMAP_      co0,co1;
      MMAP_      cb0,cb1;
      INT_         i,j,k,l,m;
      INT_         p,q,r;
      REAL_       *x0;
      REAL_       *p0;
      REAL_       *x1;
      REAL_       *p1;
      REAL_       *y0;
      REAL_       *y1;

      REAL_        xi[3];

      REAL_        xm[8][3];
      REAL_        pm[8][3];
      REAL_        ym[4][3];

      REAL_        xn[3];
      REAL_        pn[3];
      REAL_        yn[3];
      REAL_        w[8];

      size_t len=0;
      size_t len0,len1;

      FILE *f=NULL;
      char  name[512];

      sprintf( name, "grid/%06d", hx[0].id );
      f= fopen( name, "r" ); assert(f);
    ::fread( &m,        1,sizeof(INT_), f );
      fclose( f );

      len0=0; co0.dims( len0, m+1,  3,m+1,  m+1,   1,-1 );
      len1=0; co1.dims( len1, m*n+1,3,m*n+1,m*n+1, 1,-1 );

      co0.malloc( &x0 );
      co0.malloc( &p0 );
      co1.malloc( &x1 );
      co1.malloc( &p1 );

      for( l=0;l<hx.n;l++ )
     {

         sprintf( name, "grid/%06d", hx[l].id );
         f= fopen( name, "r" );
         assert(f);
       ::fread( &k,        1,sizeof(INT_), f );
       ::fread( &len,      1,sizeof(size_t), f );
         assert( k   ==    m );
         assert( len == len0 );
       ::fread(  x0,len,sizeof(REAL_),f );
       ::fread(  p0,len,sizeof(REAL_),f );
         fclose( f );

         co1.memset( x1,0. );
         co1.memset( p1,0. );

         for( k=0; k<m;k++ )
        {
            for( j=0; j<m;j++ )
           {
               for( i=0; i<m;i++ )
              {

                  co0.loadv( i,  j,  k,   0, x0, xm[0]);      
                  co0.loadv( i+1,j,  k,   0, x0, xm[1]);      
                  co0.loadv( i,  j+1,k,   0, x0, xm[2]);      
                  co0.loadv( i+1,j+1,k,   0, x0, xm[3]);      
                  co0.loadv( i,  j,  k+1, 0, x0, xm[4]);      
                  co0.loadv( i+1,j,  k+1, 0, x0, xm[5]);      
                  co0.loadv( i,  j+1,k+1, 0, x0, xm[6]);      
                  co0.loadv( i+1,j+1,k+1, 0, x0, xm[7]);      

                  co0.loadv( i,  j,  k,   0, p0, pm[0]);      
                  co0.loadv( i+1,j,  k,   0, p0, pm[1]);      
                  co0.loadv( i,  j+1,k,   0, p0, pm[2]);      
                  co0.loadv( i+1,j+1,k,   0, p0, pm[3]);      
                  co0.loadv( i,  j,  k+1, 0, p0, pm[4]);      
                  co0.loadv( i+1,j,  k+1, 0, p0, pm[5]);      
                  co0.loadv( i,  j+1,k+1, 0, p0, pm[6]);      
                  co0.loadv( i+1,j+1,k+1, 0, p0, pm[7]);      


                  for( r=0;r<n+1;r++ )
                 {
                     for( q=0;q<n+1;q++ )
                    {
                        for( p=0;p<n+1;p++ )
                       {
                           xi[0]= ((REAL_)p)/((REAL_)n);
                           xi[1]= ((REAL_)q)/((REAL_)n);
                           xi[2]= ((REAL_)r)/((REAL_)n);

                           w[0]= (1-xi[0])*(1-xi[1])*(1-xi[2]);
                           w[1]=    xi[0] *(1-xi[1])*(1-xi[2]);
                           w[2]= (1-xi[0])*   xi[1] *(1-xi[2]);
                           w[3]=    xi[0] *   xi[1] *(1-xi[2]);
                           w[4]= (1-xi[0])*(1-xi[1])*   xi[2] ;
                           w[5]=    xi[0] *(1-xi[1])*   xi[2] ;
                           w[6]= (1-xi[0])*   xi[1] *   xi[2] ;
                           w[7]=    xi[0] *   xi[1] *   xi[2] ;

                           xn[0]= w[0]*xm[0][0]+ w[1]*xm[1][0]+ w[2]*xm[2][0]+ w[3]*xm[3][0]+ 
                                  w[4]*xm[4][0]+ w[5]*xm[5][0]+ w[6]*xm[6][0]+ w[7]*xm[7][0];
                           xn[1]= w[0]*xm[0][1]+ w[1]*xm[1][1]+ w[2]*xm[2][1]+ w[3]*xm[3][1]+ 
                                  w[4]*xm[4][1]+ w[5]*xm[5][1]+ w[6]*xm[6][1]+ w[7]*xm[7][1];
                           xn[2]= w[0]*xm[0][2]+ w[1]*xm[1][2]+ w[2]*xm[2][2]+ w[3]*xm[3][2]+ 
                                  w[4]*xm[4][2]+ w[5]*xm[5][2]+ w[6]*xm[6][2]+ w[7]*xm[7][2];

                           pn[0]= w[0]*pm[0][0]+ w[1]*pm[1][0]+ w[2]*pm[2][0]+ w[3]*pm[3][0]+ 
                                  w[4]*pm[4][0]+ w[5]*pm[5][0]+ w[6]*pm[6][0]+ w[7]*pm[7][0];
                           pn[1]= w[0]*pm[0][1]+ w[1]*pm[1][1]+ w[2]*pm[2][1]+ w[3]*pm[3][1]+ 
                                  w[4]*pm[4][1]+ w[5]*pm[5][1]+ w[6]*pm[6][1]+ w[7]*pm[7][1];
                           pn[2]= w[0]*pm[0][2]+ w[1]*pm[1][2]+ w[2]*pm[2][2]+ w[3]*pm[3][2]+ 
                                  w[4]*pm[4][2]+ w[5]*pm[5][2]+ w[6]*pm[6][2]+ w[7]*pm[7][2];

                           co1.storev( p+n*i,q+n*j,r+n*k, 0, x1, xn );
                           co1.storev( p+n*i,q+n*j,r+n*k, 0, p1, pn );
                       }
                    }
                 }
          
            

              }
           }
        }

         sprintf( name, "grid_refn/%06d", hx[l].id );
         f= fopen( name, "w" );
         assert(f);
         k= n*m;
       ::fwrite( &k,         1,sizeof(INT_), f );
       ::fwrite( &len1,      1,sizeof(size_t), f );
       ::fwrite(  x1,len1,sizeof(REAL_),f );
       ::fwrite(  p1,len1,sizeof(REAL_),f );
         fclose( f );

     }

      delete[] x0; x0= NULL;
      delete[] x1; x1= NULL;
      delete[] p0; p0= NULL;
      delete[] p1; p1= NULL;

      len0=0; cb0.dims( len0, m+1,  2,m+1,  1,   -1 );
      len1=0; cb1.dims( len1, n*m+1,2,n*m+1,1,   -1 );

      cb0.malloc( &y0 );
      cb1.malloc( &y1 );

      printf( "nb  %d\n", nb );
 
      for( INT_ g=0;g<nb;g++ )
     {
 
         for( k=0;k<bz[g].bo.n;k++ )
        {
            sprintf( name, "bgrid/%06d.%06d", g,k );
            f= fopen( name, "r" );
            assert( f );
          ::fread( &j,        1,sizeof(INT_), f );
          ::fread( &len,      1,sizeof(size_t), f );
            assert( j   ==    m );
            assert( len == len0 );
          ::fread(  y0,len,sizeof(REAL_),f );
            fclose( f );

            for( j=0; j<m;j++ )
           {
               for( i=0; i<m;i++ )
              {

                  cb0.loadv( i,  j,   0, y0, ym[0]);
                  cb0.loadv( i+1,j,   0, y0, ym[1]);
                  cb0.loadv( i,  j+1, 0, y0, ym[2]);
                  cb0.loadv( i+1,j+1, 0, y0, ym[3]);


                  for( q=0;q<n+1;q++ )
                 {
                     for( p=0;p<n+1;p++ )
                    {

                        xi[0]= ((REAL_)p)/((REAL_)n);
                        xi[1]= ((REAL_)q)/((REAL_)n);
                       
                        w[0]= (1-xi[0])*(1-xi[1]);
                        w[1]=    xi[0] *(1-xi[1]);
                        w[2]= (1-xi[0])*   xi[1] ;
                        w[3]=    xi[0] *   xi[1] ;
                       
                        yn[0]= w[0]*ym[0][0]+ w[1]*ym[1][0]+ w[2]*ym[2][0]+ w[3]*ym[3][0];
                        yn[1]= w[0]*ym[0][1]+ w[1]*ym[1][1]+ w[2]*ym[2][1]+ w[3]*ym[3][1];

                        cb1.storev( p+n*i,q+n*j,0, y1, yn );
                    }
                 }

              }
           }

            sprintf( name, "bgrid_refn/%06d.%06d", g,k );
            f= fopen( name, "w" );
            assert( f );
            j= n*m;
          ::fwrite( &j,        1,sizeof(INT_), f );
          ::fwrite( &len1,     1,sizeof(size_t), f );
          ::fwrite( y1,len1,sizeof(REAL_),f );
            fclose( f );
        }
     }
      delete[] y0; y0= NULL;
      delete[] y1; y1= NULL;

  }
