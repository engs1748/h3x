
#  include <q3x/q3x.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::sxpy( REAL_ *x, REAL_ a, REAL_ *y, REAL_ b, REAL_ *z )
  {

      REAL_   f[NVAR_];
      REAL_   g[NVAR_];
      REAL_   p[NVAR_];
      INT_    i,j,k, t;

#pragma omp parallel for private(t,i,j,k,f,g,p) schedule(dynamic)

      for( t=0;t<hx.n;t++ )
     {
         for( k=0;k<bsize;k++ )
        {
            for( j=0;j<bsize;j++ )
           {
               for( i=0;i<bsize;i++ )
              {
                  so[0].loadv(i,j,k,t,  y, f );
                  so[0].loadv(i,j,k,t,  z, g );

                  p[0]= a*f[0]+ b*g[0];
                  p[1]= a*f[1]+ b*g[1];
                  p[2]= a*f[2]+ b*g[2];
                  p[3]= a*f[3]+ b*g[3];
                  p[4]= a*f[4]+ b*g[4];

                  so[0].storev(i,j,k,t,  x, p );
              }
           }
        }
     }

      return;
  }
