#  include <q3x/q3x.h>
#  include <sort/proto.h>

   void q3x_t::part( INT_ n, INT_ m )
  {

      INT_      i,j,k,l,u;
      INT_      i0,i1,i2,i3,i4,i5,i6,i7;
      REAL_    *x= new REAL_[hx.n];
      INT_     *iprm= new INT_[hx.n];
      INT_     *jprm= new INT_[hx.n];
      REAL_     w;

      FILE    *f;

      printf( "partitioning on %d nodes\n", n );
      printf( "entity counts %d (p) %d (h) %d (q) \n", vt.n,hx.n,qv.n );

      switch(m)
     {

// Axial partition

         case(0):
        {
            for( i=0;i<hx.n;i++ )
           {
               i0= hx[i].x[0]; 
               i1= hx[i].x[1]; 
               i2= hx[i].x[2]; 
               i3= hx[i].x[3]; 
               i4= hx[i].x[4]; 
               i5= hx[i].x[5]; 
               i6= hx[i].x[6]; 
               i7= hx[i].x[7]; 
               w= vx[i0][0]+ vx[i1][0]+ 
                  vx[i2][0]+ vx[i3][0]+
                  vx[i4][0]+ vx[i5][0]+ 
                  vx[i6][0]+ vx[i7][0];
               w*=0.125;
               x[i]= w;
           }
          
            hsort( hx.n,x,iprm );
            u= hx.n/n;
            l= 0;
            for( k=0;k<n;k++ )
           {
               for( i=0;i<u;i++ )
              {
                  j= iprm[l++];
                  jprm[j]= k;
              }
           };
            if( hx.n%n != 0 ){ printf( "warning: partition is not balanced\n" ); }
            for( k=n*u;k<hx.n;k++ )
           {
               j= iprm[l++];
               jprm[j]= n-1;
           }
            break;
        }

// manual partition

         case(1):
        {
            FILE *f=NULL;
            f= fopen( "part.dat","r" );
            assert( f ); 
            for( i=0;i<hx.n;i++ )
           {
               fscanf( f,"%d",jprm+i );
               assert( jprm[i]>=0 );
               assert( jprm[i]<n );
           }
            fclose( f );
            break;
        }
     }

      f= fopen( "part.bin","w" );    
    ::fwrite(   &n,1, sizeof(INT_), f ); 
    ::fwrite( jprm,hx.n,sizeof(INT_), f ); 
      fclose( f );

      delete[] x;
      delete[] iprm;
      delete[] jprm;
      return;
  }
