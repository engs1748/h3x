#  include <q3x/q3x.h>

// x: global array of coordinate points
// u: global array of variable values
// f: file stream to write tecplot dataloader file to

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::gnuplot3d( REAL_ *x, FILE *f )
  {
      INT_ t,i,j,k,l;
      REAL_ buf[NVAR_];

      //write file header

      for( t=0;t<hx.n;t++ ) //block coord
     {
         
         fprintf( f,"\n" );
//block ordered must be written single line for each variable

         for( k=0; k<bsize+1; k++ ) 
        {
            fprintf( f,"\n" );
            for( j=0; j<bsize+1; j++ ) 
           {
               fprintf( f,"\n" );
               for( i=0; i<bsize+1; i++ ) 
              {
                  co[0].loadv( i,j,k,t, x, buf );
                  fprintf( f, "% 14.7e % 14.7e % 14.7e \n", buf[0],buf[1],buf[2] );
                  l++;
              } 
           } 
        } 
     }
  }
