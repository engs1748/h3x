
#  include <q3x/q3x.h>
#  include <sys/types.h>
#  include <sys/stat.h>


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::restart( REAL_ *v, REAL_ *vb, REAL_ *vc )
  {

      char       name[1024];
      size_t     len,dlen,off;
      INT_       j,k,m;
      INT_       ist,ien;
      FILE      *f;

      MPI_File fh;
      MPI_Status status;
      MPI_Offset off0,off1, offlen;

// read blocks

      len=    so[0].bsize(4);
      offlen= len* sizeof(REAL_);

      MPI_File_open(MPI_COMM_WORLD, "restart/h", MPI_MODE_RDONLY, MPI_INFO_NULL, &fh); assert( fh );

      off0=0;
      MPI_File_read_at(fh, off0,     &m,1, INT_MPI,&status); off0+= sizeof(INT_);  
      MPI_File_read_at(fh, off0,  &dlen,1,LONG_MPI,&status); off0+= sizeof(size_t);

      assert( m == bsize );
      assert( len == dlen );

      for (k=0;k<hx.n;k++) 
     {

        off= so[0].addr(0,0,0,0,k);
        off1= off0+ hx[k].id*offlen;

        MPI_File_read_at(fh, off1, v+off,len,MPI_DOUBLE,&status);
     }
      MPI_Barrier( MPI_COMM_WORLD );
      MPI_File_close( &fh );

// read boundaries

      printf( "reading boundaries\n" );
      len=    sb.bsize(3);
      offlen= len* sizeof(REAL_);

      MPI_File_open(MPI_COMM_WORLD, "restart/b", MPI_MODE_RDONLY, MPI_INFO_NULL, &fh); assert( fh );

      off0=0;
      MPI_File_read_at(fh, off0,     &m,1, INT_MPI,&status); off0+= sizeof(INT_);  
            printf("new bread\n");
      MPI_File_read_at(fh, off0,  &dlen,1,LONG_MPI,&status); off0+= sizeof(size_t);
            printf("new bread\n");

      assert(   m == bsize );
      assert( len == dlen  );

      ien=0;
      for( k=0;k<BTYPES;k++ )
     {   
         ist=ien;
         ien= bn.l[k][0];
         for( j=ist;j<ien;j++ )
        {

            off= sb.addr(0,0,0,j);
            off1= off0+ offlen*bn.i[j];

            MPI_File_read_at(fh, off1, vb+off, len, REAL_MPI, &status);
            printf("new bread\n");

        }
     }   
      MPI_Barrier( MPI_COMM_WORLD );
      MPI_File_close( &fh );
      printf( "read boundaries\n" );

// read conjugate modes

      if( cjg.n > 0 )
     {
         sprintf( name, "restart/c" );
         f= fopen( name, "r" );
         assert( f );
       ::fread( vc,cjg.n,sizeof(REAL_), f);
         fclose( f );
     }

      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::save( REAL_ *v, REAL_ *vb, REAL_ *vc, INT_ t )
  {

      char       name[1024];
      char       hname[1024];
      char       bname[1024];
      char       cname[1024];
      size_t     len,off;
      INT_       j,k;
      INT_       ist,ien;
      FILE      *f;

      MPI_File fh;
      MPI_Status status;
      MPI_Offset off0,off1, offlen;

      if( t == -1 )
     {
         sprintf( name,"restart_final" );   
         sprintf( hname,"restart_final/h" );
         sprintf( bname,"restart_final/b" );
         sprintf( cname,"restart_final/c" );
     }
      else
     {
         sprintf( name,"restart.%06d",t );
         sprintf( hname,"restart.%06d/h",t ); 
         sprintf( bname,"restart.%06d/b",t ); 
         sprintf( cname,"restart.%06d/c",t );
     }

      if( rank == 0 )
     {
         mkdir( name, 0700 );
     }
      MPI_Barrier( MPI_COMM_WORLD );


      len=    so[0].bsize(4);
      offlen= len* sizeof(REAL_);

      MPI_File_open(MPI_COMM_WORLD, hname, MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);
      assert( fh );

      off0=0;

      MPI_File_write_at(fh, off0, &bsize,1, INT_MPI,&status); off0+= sizeof(INT_);
      MPI_File_write_at(fh, off0,   &len,1,LONG_MPI,&status); off0+= sizeof(size_t);

      for (k=0;k<hx.n;k++) 
     {

        off= so[0].addr(0,0,0,0,k);
        off1= off0+ hx[k].id*offlen;

        MPI_File_write_at(fh, off1, v+off,len,REAL_MPI,&status);
     }
      MPI_Barrier( MPI_COMM_WORLD );
      MPI_File_close( &fh );


      len=    sb.bsize(3);
      offlen= len* sizeof(REAL_);

      MPI_File_open(MPI_COMM_WORLD, bname, MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);
      off0=0;
      MPI_File_write_at(fh, off0, &bsize,1, INT_MPI,&status); off0+= sizeof(INT_);
      MPI_File_write_at(fh, off0,   &len,1,LONG_MPI,&status); off0+= sizeof(size_t);

      ien=0;
      for( k=0;k<BTYPES;k++ )
     {   
         ist=ien;
         ien= bn.l[k][0];
         for( j=ist;j<ien;j++ )
        {

            off= sb.addr(0,0,0,j);
            off1= off0+ offlen*bn.i[j];

            MPI_File_write_at(fh, off1, vb+off, len, REAL_MPI, &status);

        }
     }   
      MPI_Barrier( MPI_COMM_WORLD );
      MPI_File_close( &fh );


      if( cjg.n > 0 )
     {
         if( rank == 0 )
        {
            f= fopen( cname, "w" );
            assert( f );
          ::fwrite( vc,cjg.n,sizeof(REAL_), f);
            fclose( f );
        }
     }

      return;
  }


