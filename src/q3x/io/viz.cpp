#  include <q3x/q3x.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::viz()
  {
      REAL_          vc[NCJG_];

      REAL_          *v=NULL;
      REAL_          *vb=NULL;
      REAL_          *x=NULL;
      REAL_          *y=NULL;
      REAL_          *yi=NULL;

      FILE           *f=NULL;
      char          buf[256];

      gas.init();

      start();

      co[3].malloc( &x  );
         cb.malloc( &y  );
         ci.malloc( &yi  );
      so[1].malloc( &v  );
         sb.malloc( &vb  );

      grid( x,y,yi );

      restart( v, vb, vc );

      sprintf( buf,"tec/%06d", rank );
      f= fopen( buf,"w" );
      assert( f );
      tecplot3d( x, v, f );
      fclose( f );

      delete[] x;
      delete[] v;
      delete[] vb;
      delete[] y;
      delete[] yi;

      stop();

      return;
  }

   void q3x_t::vix()
  {

      REAL_          *x=NULL;
      REAL_          *p=NULL;
      REAL_          *y=NULL;
      REAL_         *yi=NULL;

      FILE           *f=NULL;
      char          buf[256];

      gas.init();

      start();

      co[3].malloc( &x  );
      co[3].malloc( &p  );
         cb.malloc( &y  );
         ci.malloc( &yi );
      grid( x,y,yi, p );

      sprintf( buf,"tec/%06d", rank );
      f= fopen( buf,"w" );
      assert( f );
      tecplot3dx( x, p, f );
//    gnuplot3d( x, f );
      fclose( f );

      delete[] x;
      delete[] p;
      delete[] y;
      delete[] yi;

      stop();

      return;
  }

   void q3x_t::wco()
  {

      REAL_          *x=NULL;
      REAL_          *p=NULL;
      REAL_          *y=NULL;
      REAL_         *yi=NULL;

      FILE           *f=NULL;
      char          buf[256];

      REAL_          *wb=NULL;
      REAL_          *wc=NULL;
      REAL_          *wm=NULL;
      REAL_           w[MSIZE_];

      INT_    c, i,j,k;
      INT_    ist,ien;

      gas.init();

      start();

      co[3].malloc( &x  );
      co[3].malloc( &p  );
         cb.malloc( &y  );
         ci.malloc( &yi );
      mw[1].malloc( &wm ); 
         bw.malloc( &wb ); 
         cw.malloc( &wc ); 
      grid( x,y,yi, p );


      metrics( x, wm,wb,wc );

      ien= bn.l[AWALLS-1][0];
      for( k=AWALLS;k<TWALLS;k++ )
     {
         ist= ien;
         ien= bn.l[k][0];
         for( INT_ t=ist;t<ien;t++ )
        {
    
            c= bn.h[t];
            if( c > -1 )
           {   
               printf( "%d %d\n",t,bn.i[t] );

               sprintf( buf,"wco/x/%06d", bn.i[t] );
               f= fopen( buf,"w" );
               assert( f );

               for( j=0;j<bsize;j++ )
              {
                  for( i=0;i<bsize;i++ )
                 {

                     bw.loadv(i,j,    t,  wb, w   );
                     fprintf( f,"% 9.3e % 9.3e % 9.3e\n", w[0],w[1],w[2] );
             
                 }
              }

               fclose( f );
           }
        }
     }

      delete[] x;
      delete[] p;
      delete[] y;
      delete[] yi;

      stop();

      return;
  }
