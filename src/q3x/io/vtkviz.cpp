# include <q3x/q3x.h>

//
//Prepare the coordinate arrays to write the block layout to vtk file
//
   void q3x_t::vtk_block( INT_ format )
  {
      REAL_          *x=NULL;
      REAL_          *y=NULL;
      REAL_         *yi=NULL;

      FILE           *f=NULL;
      char         name[256];

      gas.init();

      start();

      co[3].malloc( &x  );
         cb.malloc( &y  );
         ci.malloc( &yi );
      grid( x, y, yi );

      if( format==0 ){ sprintf( name,"vtk/block%06d.vtk", rank ); }
      if( format==1 ){ sprintf( name,"vtk/block%06d.vtu", rank ); }

      f= fopen( name,"w" );
      assert( f );

      // legacy or xml format
      if( format==0 ){ vtklegacy_block( x, f ); }
      if( format==1 ){ vtkxml_block(    x, f ); }

      fclose( f );

      delete[]  x;
      delete[]  y;
      delete[] yi;

      stop();

      return;
  }

//
//Prepare the coordinate and size function arrays to write the grid to vtk file
//
   void q3x_t::vtk_grid( INT_ format )
  {
      REAL_          *x=NULL;
      REAL_          *p=NULL;
      REAL_          *y=NULL;
      REAL_         *yi=NULL;

      FILE           *f=NULL;
      char         name[256];

      gas.init();

      start();

      co[3].malloc( &x  );
      co[3].malloc( &p  );
         cb.malloc( &y  );
         ci.malloc( &yi );
      grid( x,y, p );

      if( format==0 ){ sprintf( name,"vtk/grid%06d.vtk", rank ); }
      if( format==1 ){ sprintf( name,"vtk/grid%06d.vtu", rank ); }

      f= fopen( name,"w" );
      assert( f );

      // legacy or xml format
      if( format==0 ){ vtklegacy_grid( x, p, f ); }
      if( format==1 ){ vtkxml_grid(    x, p, f ); }

      fclose( f );

      delete[]  x;
      delete[]  p;
      delete[]  y;
      delete[] yi;

      stop();

      return;
  }

//
//Prepare the coordinate and solution arrays to write the grid and solution to vtk file
//
   void q3x_t::vtk_solution( INT_ format )
  {
      REAL_          vc[NCJG_];

      REAL_          *x=NULL;
      REAL_          *y=NULL;
      REAL_         *yi=NULL;
      REAL_          *v=NULL;
      REAL_         *vb=NULL;

      FILE           *f=NULL;
      char         name[256];

      gas.init();

      start();

      co[3].malloc( &x  );
         cb.malloc( &y  );
         cb.malloc( &yi );
      so[1].malloc( &v  );

      grid( x, y, yi );

      restart( v, vb, vc );

      if( format==0 ){ sprintf( name,"vtk/solution%06d.vtk", rank ); }
      if( format==1 ){ sprintf( name,"vtk/solution%06d.vtu", rank ); }

      f= fopen( name,"w" );
      assert( f );

      if( format==0 ){ vtklegacy_solution( x, v, f ); }
      if( format==1 ){ vtkxml_solution(    x, v, f ); }

      fclose( f );

      delete[]  x;
      delete[]  y;
      delete[] yi;
      delete[]  v;
      delete[] vb;

      stop();

      return;
  }
