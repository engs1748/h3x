

#  include <q3x/q3x.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::grid( REAL_ *x, REAL_ *y, REAL_ *yi )
  {
      INT_     i,k,j,m;
      INT_     ist,ien;
      INT_     b,g;
      size_t dlen,off;

      MPI_File fh;
      MPI_Status status;
      MPI_Offset off0,off1, offlen;

      MPI_File_open(MPI_COMM_WORLD, "grid/h", MPI_MODE_RDONLY, MPI_INFO_NULL, &fh);
      assert( fh );
      off0= 0;

      MPI_File_read_at(fh, off0,    &m, 1,  INT_MPI, &status); off0+= sizeof(INT_);
      MPI_File_read_at(fh, off0, &dlen, 1, LONG_MPI, &status); off0+= sizeof(size_t);

      assert( m == bsize);
      assert( dlen == co[0].bsize(4) );

      offlen= dlen*sizeof(REAL_);
      offlen*=2;

      for( k=0;k<hx.n;k++ )
     {
         off1= off0+ offlen*hx[k].id;
         off= co[0].addr(0,0,0,0,k);

         MPI_File_read_at(fh, off1, x+off, dlen, REAL_MPI, &status);

     }
      MPI_Barrier( MPI_COMM_WORLD );
      MPI_File_close( &fh );

      MPI_File_open(MPI_COMM_WORLD, "grid/b", MPI_MODE_RDONLY, MPI_INFO_NULL, &fh);
      assert( fh );

      off0= 0;

      MPI_File_read_at(fh, off0,    &m, 1, INT_MPI, &status); off0+= sizeof(INT_);
      MPI_File_read_at(fh, off0, &dlen, 1,LONG_MPI, &status); off0+= sizeof(size_t);

      assert( m == bsize);
      assert( dlen == cb.bsize(3) );

      offlen= dlen*sizeof(REAL_);

      ien=0;
      for( k=0;k<BTYPES;k++ )
     {
         ist=ien;
         ien= bn.l[k][0];
         for( j=ist;j<ien;j++ )
        {
            i= bn.i[j];

            off1= off0+ i*offlen;
            off= cb.addr(0,0,0,j);

            MPI_File_read_at(fh, off1, y+off, dlen, REAL_MPI, &status);

        }
     }
      MPI_Barrier( MPI_COMM_WORLD );
      MPI_File_close( &fh );


      MPI_File_open(MPI_COMM_WORLD, "grid/i", MPI_MODE_RDONLY, MPI_INFO_NULL, &fh);
      assert( fh );

      off0= 0;

      MPI_File_read_at(fh, off0,    &m, 1, INT_MPI, &status); off0+= sizeof(INT_);
      MPI_File_read_at(fh, off0, &dlen, 1,LONG_MPI, &status); off0+= sizeof(size_t);

      assert( m == bsize);
      assert( dlen == cb.bsize(3) );

      for( k=0;k<ni;k++ )
     {
         g= bj[k][4];
         b= bj[k][0];

         off1= off0;
         for( i=0;i<g;i++ ){ off1+= offlen*bi[i].bo.n; };
         off1+= offlen*b;

         off= ci.addr(0,0,0,k);
         MPI_File_read_at(fh, off1, yi+off, dlen, REAL_MPI, &status);

     }
      MPI_Barrier( MPI_COMM_WORLD );
      MPI_File_close( &fh );


      return;
  }

   void q3x_t::grid( REAL_ *x, REAL_ *y, REAL_ *yi, REAL_ *p )
  {
      INT_     i,k,j,m;
      INT_     ist,ien;
      INT_     b,g;
      size_t dlen,off;

      MPI_File fh;
      MPI_Status status;
      MPI_Offset off0,off1, offlen;

      MPI_File_open(MPI_COMM_WORLD, "grid/h", MPI_MODE_RDONLY, MPI_INFO_NULL, &fh);
      assert( fh );
      off0= 0;

      MPI_File_read_at(fh, off0,    &m, 1, INT_MPI, &status); off0+= sizeof(INT_);
      MPI_File_read_at(fh, off0, &dlen, 1,LONG_MPI, &status); off0+= sizeof(size_t);

      assert( m == bsize);
      assert( dlen == co[0].bsize(4) );

      offlen= dlen*sizeof(REAL_);
      offlen*= 2;

      for( k=0;k<hx.n;k++ )
     {
         off1= off0+ offlen*hx[k].id;
         off= co[0].addr(0,0,0,0,k);

         MPI_File_read_at(fh, off1, x+off, dlen, REAL_MPI, &status); off1+= dlen*sizeof(REAL_);
         MPI_File_read_at(fh, off1, p+off, dlen, REAL_MPI, &status); 

     }
      MPI_Barrier( MPI_COMM_WORLD );
      MPI_File_close( &fh );

      MPI_File_open(MPI_COMM_WORLD, "grid/b", MPI_MODE_RDONLY, MPI_INFO_NULL, &fh);
      assert( fh );

      off0= 0;

      MPI_File_read_at(fh, off0,    &m, 1, INT_MPI, &status); off0+= sizeof(INT_);
      MPI_File_read_at(fh, off0, &dlen, 1,LONG_MPI, &status); off0+= sizeof(size_t);

      assert( m == bsize);
      assert( dlen == cb.bsize(3) );

      offlen= dlen*sizeof(REAL_);

      ien=0;
      for( k=0;k<BTYPES;k++ )
     {
         ist=ien;
         ien= bn.l[k][0];
         for( j=ist;j<ien;j++ )
        {
            i= bn.i[j];

            off1= off0+ i*offlen;
            off= cb.addr(0,0,0,j);

            MPI_File_read_at(fh, off1, y+off, dlen, REAL_MPI, &status);

        }
     }
      MPI_Barrier( MPI_COMM_WORLD );
      MPI_File_close( &fh );


      MPI_File_open(MPI_COMM_WORLD, "grid/i", MPI_MODE_RDONLY, MPI_INFO_NULL, &fh);
      assert( fh );

      off0= 0;

      MPI_File_read_at(fh, off0,    &m, 1, INT_MPI, &status); off0+= sizeof(INT_);
      MPI_File_read_at(fh, off0, &dlen, 1,LONG_MPI, &status); off0+= sizeof(size_t);

      assert( m == bsize);
      assert( dlen == ci.bsize(3) );

      offlen= dlen*sizeof(REAL_);

      for( k=0;k<ni;k++ )
     {
         g= bj[k][4];
         b= bj[k][0];

         off1= off0;
         for( i=0;i<g;i++ ){ off1+= offlen*bi[i].bo.n; };
         off1+= offlen*b;

         off= ci.addr(0,0,0,k);
         MPI_File_read_at(fh, off1, yi+off, dlen, REAL_MPI, &status);

     }
      MPI_Barrier( MPI_COMM_WORLD );
      MPI_File_close( &fh );


      return;
  }

   void q3x_t::writegrid( REAL_ *x, REAL_ *y, REAL_ *yi, REAL_ *p )
  {
      INT_           g,j,b;
      INT_           ien,ist;
      size_t         len= co[0].bsize(4);
      size_t         off;

      MPI_File fh;
      MPI_Status status;
      MPI_Offset off0,off1, offlen;
      char fname[1024];

      sprintf( fname, "grid/h");
      MPI_File_open(MPI_COMM_WORLD, fname, MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);
      assert( fh );

      off0=0;
      MPI_File_write_at(fh, off0, &bsize, 1,  INT_MPI, &status); off0+= sizeof(INT_);
      MPI_File_write_at(fh, off0,   &len, 1, LONG_MPI, &status); off0+= sizeof(size_t);

      offlen= len*sizeof(REAL_);
      offlen*=2;

      for( INT_ l=0;l<hx.n;l++ )
     {
         off= co[0].addr( 0,0,0,0,l );

         off1= off0+ offlen*hx[l].id;

         MPI_File_write_at(fh, off1, x+off, len, REAL_MPI, &status); off1+= len*sizeof(REAL_);
         MPI_File_write_at(fh, off1, p+off, len, REAL_MPI, &status);
     }
      MPI_Barrier( MPI_COMM_WORLD );
      MPI_File_close( &fh );


      len= cb.bsize(3);

      sprintf( fname, "grid/b");
      MPI_File_open(MPI_COMM_WORLD, fname, MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);
      assert( fh );

      off0=0;
      MPI_File_write_at(fh, off0, &bsize, 1,  INT_MPI, &status); off0+= sizeof(INT_);
      MPI_File_write_at(fh, off0,   &len, 1, LONG_MPI, &status); off0+= sizeof(size_t);

      offlen= len*sizeof(REAL_);

      ien=0;
      for( INT_ k=0;k<BTYPES;k++ )
     {   
         ist=ien;
         ien= bn.l[k][0];
         for( j=ist;j<ien;j++ )
        {

            INT_ i= bn.i[j];

            off= cb.addr( 0,0,0,j );
            off1= off0+ i*offlen;

            MPI_File_write_at(fh, off1, y+off, len, REAL_MPI, &status);
        }
     }  
      MPI_Barrier( MPI_COMM_WORLD );
      MPI_File_close( &fh );


      len= ci.bsize(3);

      sprintf( fname, "grid/i");
      MPI_File_open(MPI_COMM_WORLD, fname, MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);
      assert( fh );

      off0=0;
      MPI_File_write_at(fh, off0, &bsize, 1,  INT_MPI, &status); off0+= sizeof(INT_);
      MPI_File_write_at(fh, off0,   &len, 1, LONG_MPI, &status); off0+= sizeof(size_t);

      offlen= len*sizeof(REAL_);

      ien=0;
      for( INT_ k=0;k<ni;k++ )
     {
         g= bj[k][4];
         b= bj[k][0];
         off= ci.addr( 0,0,0,k );

         off1= off0;
         for (INT_ i=0;i<g;i++){ off1+= offlen*bi[i].bo.n; };
         off1+= b*offlen;

         MPI_File_write_at(fh, off1, yi+off, len, REAL_MPI, &status);

     }
      MPI_Barrier( MPI_COMM_WORLD );
      MPI_File_close( &fh );
  
  }

