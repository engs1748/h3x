#  include <q3x/q3x.h>

// x: global array of coordinate points
// u: global array of variable values
// f: file stream to write tecplot dataloader file to

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::tecplot3d( REAL_ *x, REAL_ *u, FILE *f )
  {
      INT_ t,v,i,j,k,l;
      REAL_ buf[NVAR_];

      const INT_  llen=4;

      //write file header

      fprintf( f, "TITLE = \"q3x data\"\n" );
      fprintf( f, "FILETYPE = FULL\n");
      fprintf( f, "Variables = \"X\", \"Y\", \"Z\", " );
      for( i=0;i<gas.nvar();i++ )
     {
         char buf[256];
         gas.varname( i,buf );
         fprintf( f,"\"%s\",", buf );
     }
      fprintf( f,"\n" );

      for( t=0;t<hx.n;t++ ) //block coord
     {
         
         fprintf( f, "ZONE " );
         fprintf( f, "T=\"BLOCK %d\", ",hx[t].id );
         fprintf( f, "I=%d, J=%d, K=%d, ", bsize+1 ,bsize+1 ,bsize+1 );
         fprintf( f, "DATAPACKING=BLOCK, " );
         fprintf( f, "VARLOCATION=([" );
         fprintf( f, "%d", 4 );
         for( i=1;i<gas.nvar();i++ ){ fprintf(f,",%d",i+4 ); }
         fprintf( f, "]=CELLCENTERED)\n");

//block ordered must be written single line for each variable

         for( v=0; v<3; v++ ) //write coords
        {
            l= 0;
            for( k=0; k<bsize+1; k++ ) //k coord
           {
               for( j=0; j<bsize+1; j++ ) //j coord
              {
                  for( i=0; i<bsize+1; i++ ) //i coord
                 {
                     if( l == llen )
                    {
                        fprintf( f,"\n" );
                        l= 0;
                    }
                     co[0].loadv( i,j,k,t, x, buf );
                     fprintf( f, "% 14.7e ", buf[v] );
                     l++;
                 } //i
              } //j
           } //k
            if( l!= 0 ){ fprintf( f, "\n"); }
//          fprintf( f, "-new variable- \n" );
        } //coords

         for( v=0; v<gas.nvar(); v++ ) //write solution vars
        {
            l= 0;
            for( k=0; k<bsize; k++ ) //k coord
           {
               for( j=0; j<bsize; j++ ) //j coord
              {
                  for( i=0; i<bsize; i++ ) //i coord
                 {
                     if( l == llen )
                    {
                        fprintf( f,"\n" );
                        l= 0;
                    }
                     so[0].loadv( i,j,k,t, u, buf );
                     fprintf( f, "% 14.7e ", buf[v] );
                     l++;
                 } //i
              } //j
           } //k
            if( l!= 0 ){ fprintf( f, "\n"); }
//          fprintf( f, "-new variable- \n" );
        } //vars
     } //block
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::tecplot3dx( REAL_ *x, REAL_ *p, FILE *f )
  {
      INT_ t,v,i,j,k,l;
      REAL_ buf[NVAR_];

      const INT_  llen=4;

      //write file header

      fprintf( f, "TITLE = \"q3x data\"\n" );
      fprintf( f, "FILETYPE = FULL\n");
      fprintf( f, "Variables = \"X\", \"Y\", \"Z\", \"P\", \"Q\", \"R\"," );
      fprintf( f,"\n" );

      for( t=0;t<hx.n;t++ ) //block coord
     {
         
         fprintf( f, "ZONE " );
         fprintf( f, "T=\"BLOCK %d\", ",hx[t].id );
         fprintf( f, "I=%d, J=%d, K=%d, ", bsize+1 ,bsize+1 ,bsize+1 );
         fprintf( f, "DATAPACKING=BLOCK\n" );

//block ordered must be written single line for each variable

         for( v=0; v<3; v++ ) //write coords
        {
            l= 0;
            for( k=0; k<bsize+1; k++ ) //k coord
           {
               for( j=0; j<bsize+1; j++ ) //j coord
              {
                  for( i=0; i<bsize+1; i++ ) //i coord
                 {
                     if( l == llen )
                    {
                        fprintf( f,"\n" );
                        l= 0;
                    }
                     co[0].loadv( i,j,k,t, x, buf );
                     fprintf( f, "% 14.7e ", buf[v] );
                     l++;
                 } 
              }
           } 
            if( l!= 0 ){ fprintf( f, "\n"); }
        } 
         for( v=0; v<3; v++ ) //write coords
        {
            l= 0;
            for( k=0; k<bsize+1; k++ ) //k coord
           {
               for( j=0; j<bsize+1; j++ ) //j coord
              {
                  for( i=0; i<bsize+1; i++ ) //i coord
                 {
                     if( l == llen )
                    {
                        fprintf( f,"\n" );
                        l= 0;
                    }
                     co[0].loadv( i,j,k,t, p, buf );
                     fprintf( f, "% 14.7e ", buf[v] );
                     l++;
                 } 
              }
           } 
            if( l!= 0 ){ fprintf( f, "\n"); }
        } 
       
     }
  }
