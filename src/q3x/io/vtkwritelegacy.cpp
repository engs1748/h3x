# include <q3x/q3x.h>
# include <misc.h>


//
//Write the block layout to vtk legacy file
//
   void q3x_t::vtklegacy_block( REAL_ *x, FILE *f )
  {
     INT_  h,j;
      INT_8 y;
      INT_  listsize, celltype;

      fprintf( f, "# vtk DataFile Version 2.0\n" );

      fprintf( f, "h3x block layout\n" );

      fprintf( f, "ASCII\n" );
      fprintf( f, "DATASET UNSTRUCTURED_GRID\n" );

      fprintf( f, "POINTS %d float\n", vx.n );

   // write point coordinates
      for( j=0; j<vx.n; j++ )
     {
         fprintf( f, "%f %f %f\n", vx[j][0], vx[j][1], vx[j][2] );
     }

      fprintf( f, "\n" );

   // need 8*hx.n numbers for each vertex on each cell, plus hx.n numbers for cell ids
      listsize= 8*hx.n + hx.n;
      celltype= 12; // VTK_HEX

   // write connectivity
      fprintf( f, "CELLS %d %d\n", hx.n, listsize );
      for( h=0; h<hx.n; h++ )
     {
         for( j=0; j<8; j++ ){ y[j]=hx[h].x[j]; }

      // vtk has different vertex numbering to h3x
         fprintf( f, "%d %d %d %d %d %d %d %d %d\n",
                  8,  y[0], y[1], y[3], y[2],
                      y[4], y[5], y[7], y[6] );
     }

      fprintf( f, "\n" );

   // write cell types
      fprintf( f, "CELL_TYPES %d\n", hx.n );
      for( h=0; h<hx.n; h++ )
     {
         fprintf( f, "%d\n", celltype );
     }

      return;
  }


//
//Write the grid to vtk legacy file
//
   void q3x_t::vtklegacy_grid( REAL_ *x, REAL_ *p, FILE *f )
  {
      INT_  h;
      INT_  listsize, celltype, ncells, npoints;
      INT_  n0, n1, n2;

      n0 = bsize*bsize*bsize;
      n2 = (bsize+1)*(bsize+1)*(bsize+1);
      ncells=  n0*hx.n;
      npoints= n2*hx.n;

      fprintf( f, "# vtk DataFile Version 2.0\n" );

      fprintf( f, "h3x grid\n" );

      fprintf( f, "ASCII\n" );
      fprintf( f, "DATASET UNSTRUCTURED_GRID\n" );

   // write point coordinates
      fprintf( f, "POINTS %d float\n", npoints );

      for( h=0; h<hx.n; h++ )
     {
         vtkwritevector( f, co[0], x, h, 3, bsize+1 );
     }
      fprintf( f, "\n" );

   // need 8*ncells numbers for each vertex on each cell, plus ncells numbers for cell ids
      listsize= 8*ncells + ncells;
      celltype= 12; // VTK_HEX

   // write connectivity
      fprintf( f, "CELLS %d %d\n", ncells, listsize );

      for( h=0; h<hx.n; h++ )
     {
         n1= h*n2;
         vtkwritecells( f, n1, bsize, 0 );
     }
      fprintf( f, "\n" );

   // write cell types
      fprintf( f, "CELL_TYPES %d\n", ncells );

      for( h=0; h<hx.n; h++ )
     {
         vtkwriteint( f, h, celltype, bsize );
     }
      fprintf( f, "\n" );

   // write size functions
      fprintf( f, "CELL_DATA %d\n", ncells );
      fprintf( f, "SCALARS P float 3\n"    );
      fprintf( f, "LOOKUP_TABLE default\n" );

      for( h=0; h<hx.n; h++ )
     {
         vtkwritevector( f, co[0], p, h, 3, bsize );
     }
      fprintf( f, "\n" );

      return;
  }


//
//Write the grid and solution to vtk legacy file
//
   void q3x_t::vtklegacy_solution( REAL_ *x, REAL_ *v, FILE *f )
  {
      INT_  h;
      INT_  listsize, celltype, ncells, npoints;
      INT_  n0, n1, n2;

      n0 = bsize*bsize*bsize;
      n2 = (bsize+1)*(bsize+1)*(bsize+1);
      ncells=  n0*hx.n;
      npoints= n2*hx.n;

      fprintf( f, "# vtk DataFile Version 2.0\n" );

      fprintf( f, "h3x grid and solution\n" );

      fprintf( f, "ASCII\n" );
      fprintf( f, "DATASET UNSTRUCTURED_GRID\n" );

   // write point coordinates
      fprintf( f, "POINTS %d float\n", npoints );

      for( h=0; h<hx.n; h++ )
     {
         vtkwritevector( f, co[0], x, h, 3, bsize+1 );
     }
      fprintf( f, "\n" );

   // need 8*ncells numbers for each vertex on each cell, plus ncells numbers for cell ids
      listsize= 8*ncells + ncells;
      celltype= 12; // VTK_HEX

   // write connectivity
      fprintf( f, "CELLS %d %d\n", ncells, listsize );

      for( h=0; h<hx.n; h++ )
     {
         n1= h*n2;
         vtkwritecells( f, n1, bsize, 0 );
     }
      fprintf( f, "\n" );

   // write cell types
      fprintf( f, "CELL_TYPES %d\n", ncells );

      for( h=0; h<hx.n; h++ )
     {
         vtkwriteint( f, h, celltype, bsize );
     }
      fprintf( f, "\n" );

   // write variables
      fprintf( f, "CELL_DATA %d\n", ncells );

   // write pressure
      fprintf( f, "SCALARS Pressure float 1\n" );
      fprintf( f, "LOOKUP_TABLE default\n"     );

      for( h=0; h<hx.n; h++ )
     {
         vtkwritescalar( f, so[0], v, h, 4, bsize );
     }

   // write temperature
      fprintf( f, "SCALARS Temperature float 1\n" );
      fprintf( f, "LOOKUP_TABLE default\n"        );

      for( h=0; h<hx.n; h++ )
     {
         vtkwritescalar( f, so[0], v, h, 3, bsize );
     }

   // write density
      fprintf( f, "SCALARS Density float 1\n" );
      fprintf( f, "LOOKUP_TABLE default\n"    );

      for( h=0; h<hx.n; h++ )
     {
         vtkwriteaux( f, so[0], v, h, 0, bsize );
     }

   // write enthalpy
      fprintf( f, "SCALARS Enthalpy float 1\n" );
      fprintf( f, "LOOKUP_TABLE default\n"     );

      for( h=0; h<hx.n; h++ )
     {
         vtkwriteaux( f, so[0], v, h, 1, bsize );
     }

   // write kinetic energy
      fprintf( f, "SCALARS KineticEnergy float 1\n" );
      fprintf( f, "LOOKUP_TABLE default\n"          );

      for( h=0; h<hx.n; h++ )
     {
         vtkwriteaux( f, so[0], v, h, 2, bsize );
     }

   // write speed of sound
      fprintf( f, "SCALARS SpeedOfSound float 1\n" );
      fprintf( f, "LOOKUP_TABLE default\n"         );

      for( h=0; h<hx.n; h++ )
     {
         vtkwriteaux( f, so[0], v, h, 3, bsize );
     }

   // write velocity
      fprintf( f, "VECTORS Velocity float\n" );

      for( h=0; h<hx.n; h++ )
     {
         vtkwritevector( f, so[0], v, h, 3, bsize );
     }

      return;
  }


