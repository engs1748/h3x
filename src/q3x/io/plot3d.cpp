#  include <q3x/q3x.h>


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::plot3d( REAL_ *x, FILE *f )
  {
      INT_ t,v,i,j,k,l;
      REAL_ buf[3];

      fprintf( f,"%d\n", hx.n );
      for( t=0;t<hx.n;t++ )
     {
         fprintf( f,"%d %d %d\n", bsize+1,bsize+1,bsize+1 );
     }

      for( t=0;t<hx.n;t++ )
     {
         for( v=0;v<3;v++ )
        {
            l= 0;
            for( k=0;k<bsize+1;k++ )
           {
               for( j=0;j<bsize+1;j++ )
              {
                  for( i=0;i<bsize+1;i++ )
                 {
                     co[0].loadv( i,j,k,t, x, buf );
                     fprintf( f,"%e ", buf[v] );
                     l++;
                     if( l == 10 ){ fprintf( f,"\n" );l=0; };
                 }
              }
           }
            if( l != 0 ){ fprintf( f,"\n" ); }
        }
     }

  }
