# include <q3x/q3x.h>
# include <misc.h>

//
//Write the block layout to vtk xml file
//
   void q3x_t::vtkxml_block( REAL_ *x, FILE *f )
  {
      printf( "\nWriting block layout to xml vtk format is not yet implemented\n" );
      return;
  }


//
//Write the grid to vtk xml file
//
   void q3x_t::vtkxml_grid( REAL_ *x, REAL_ *p, FILE *f )
  {
		INT_	n, ncells, npoints;
		INT_	h,k,j,i;
      INT_  celltype=12;   //vtk hexahedron type

      ncells =  bsize   * bsize   * bsize;
      npoints= (bsize+1)*(bsize+1)*(bsize+1);

		fprintf( f, "<VTKFile type=\"UnstructuredGrid\" version=\"2.0\" byte_order=\"LittleEndian\">\n" );

		fprintf( f, "<UnstructuredGrid>\n" );

		for( h=0; h<hx.n; h++ )
	  {
			fprintf( f, "<Piece NumberOfPoints=\"%d\" NumberOfCells=\"%d\">\n", npoints, ncells );

		// points
			fprintf( f, "<Points>\n" );

		// point coordinates
			fprintf( f, "<DataArray type=\"Float32\" NumberOfComponents=\"3\" format=\"ascii\">\n" );

         vtkwritevector( f, co[0], x, h, 3, bsize+1 );

			fprintf( f, "</DataArray>\n" );

			fprintf( f, "</Points>\n" );

		// cells
			fprintf( f, "<Cells>\n" );

		// cell connectivity
			fprintf( f, "<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n" );

         vtkwritecells( f, 0, bsize, 1 );

			fprintf( f, "</DataArray>\n" );

		// cell offsets
			fprintf( f, "<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n" );
			n=8;
			for( k=0; k<bsize; k++ )
		  {
				for( j=0; j<bsize; j++ )
			  {
					for( i=0; i<bsize; i++ )
				  {
						fprintf( f, "%d ", n );
						n+=8;
				  }
			  }
		  }
			fprintf( f, "\n" );
			fprintf( f, "</DataArray>\n" );

		// cell types
			fprintf( f, "<DataArray type=\"Int32\" Name=\"types\" format=\"ascii\">\n" );

         vtkwriteint( f, h, celltype, bsize );

			fprintf( f, "</DataArray>\n" );

			fprintf( f, "</Cells>\n" );

//		// point data
//			fprintf( f, "<PointData Scalars=\"SizeFunction\">\n" );
//
//		// size function
//			fprintf( f, "<DataArray type=\"Float32\" Name=\"SizeFunction\" NumberOfComponents=\"3\" format=\"ascii\">\n" );
//
//         vtkwritevector( f, co[0], p, h, 3, bsize+1 );
//
//			fprintf( f, "</DataArray>\n" );
//
//			fprintf( f, "</PointData>\n" );

			fprintf( f, "</Piece>\n" );
	  }

		fprintf( f, "</UnstructuredGrid>\n" );

		fprintf( f, "</VTKFile>\n" );

      return;
  }


//
//Write the grid and solution layout to vtk xml file
//
   void q3x_t::vtkxml_solution( REAL_ *x, REAL_ *v, FILE *f )
  {
		INT_	n, ncells, npoints;
		INT_	h,k,j,i;
      INT_  celltype=12;   //vtk hexahedron type

   // # of cells and points per block
      ncells =  bsize   * bsize   * bsize;
      npoints= (bsize+1)*(bsize+1)*(bsize+1);

		fprintf( f, "<VTKFile type=\"UnstructuredGrid\" version=\"2.0\" byte_order=\"LittleEndian\">\n" );

      fprintf( f, "<UnstructuredGrid>\n" );

		for( h=0; h<hx.n; h++ )
	  {
			fprintf( f, "<Piece NumberOfPoints=\"%d\" NumberOfCells=\"%d\">\n", npoints, ncells );

		// points
			fprintf( f, "<Points>\n" );

		// point coordinates
			fprintf( f, "<DataArray type=\"Float32\" NumberOfComponents=\"3\" format=\"ascii\">\n" );

         vtkwritevector( f, co[0], x, h, 3, bsize+1 );

			fprintf( f, "</DataArray>\n" );

			fprintf( f, "</Points>\n" );

		// cells
			fprintf( f, "<Cells>\n" );

		// cell connectivity
			fprintf( f, "<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n" );

         vtkwritecells( f, 0, bsize, 1 );

			fprintf( f, "</DataArray>\n" );

		// cell offsets
			fprintf( f, "<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n" );
			n=8;
			for( k=0; k<bsize; k++ )
		  {
				for( j=0; j<bsize; j++ )
			  {
					for( i=0; i<bsize; i++ )
				  {
						fprintf( f, "%d ", n );
						n+=8;
				  }
			  }
		  }
			fprintf( f, "\n" );
			fprintf( f, "</DataArray>\n" );

		// cell types
			fprintf( f, "<DataArray type=\"Int32\" Name=\"types\" format=\"ascii\">\n" );

         vtkwriteint( f, h, celltype, bsize );

			fprintf( f, "</DataArray>\n" );

			fprintf( f, "</Cells>\n" );

      // cell data
			fprintf( f, "<CellData Scalars=\"Pressure Temperature Density Enthalpy KineticEnergy SpeedOfSound\" Vectors=\"Velocity\">\n" );

      // pressure
			fprintf( f, "<DataArray type=\"Float32\" Name=\"Pressure\" format=\"ascii\">\n" );

         vtkwritescalar( f, so[0], v, h, 4, bsize );

			fprintf( f, "</DataArray>\n" );

      // temperature
			fprintf( f, "<DataArray type=\"Float32\" Name=\"Temperature\" format=\"ascii\">\n" );

         vtkwritescalar( f, so[0], v, h, 3, bsize );

			fprintf( f, "</DataArray>\n" );

      // density
			fprintf( f, "<DataArray type=\"Float32\" Name=\"Density\" format=\"ascii\">\n" );

         vtkwriteaux( f, so[0], v, h, 0, bsize );

			fprintf( f, "</DataArray>\n" );

      // enthalpy
			fprintf( f, "<DataArray type=\"Float32\" Name=\"Enthalpy\" format=\"ascii\">\n" );

         vtkwriteaux( f, so[0], v, h, 1, bsize );

			fprintf( f, "</DataArray>\n" );

      // kinetic energy
			fprintf( f, "<DataArray type=\"Float32\" Name=\"KineticEnergy\" format=\"ascii\">\n" );

         vtkwriteaux( f, so[0], v, h, 2, bsize );

			fprintf( f, "</DataArray>\n" );

      // speed of sound
			fprintf( f, "<DataArray type=\"Float32\" Name=\"SpeedOfSound\" format=\"ascii\">\n" );

         vtkwriteaux( f, so[0], v, h, 3, bsize );

			fprintf( f, "</DataArray>\n" );

      // velocity
			fprintf( f, "<DataArray type=\"Float32\" Name=\"Velocity\" NumberOfComponents=\"3\" format=\"ascii\">\n" );

         vtkwritevector( f, so[0], v, h, 3, bsize );

			fprintf( f, "</DataArray>\n" );

			fprintf( f, "</CellData>\n" );

			fprintf( f, "</Piece>\n" );
	  }

		fprintf( f, "</UnstructuredGrid>\n" );

		fprintf( f, "</VTKFile>\n" );

      return;
  }
