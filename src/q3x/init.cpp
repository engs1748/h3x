

#  include <q3x/q3x.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::init( INT_ n, REAL_ *u  )
  {
      size_t     len;
      MMAP_        s;
      INT_         i,j,k,l;
      REAL_       *v;

      FILE *f;
      char  name[1024];


      gas.init();

      len= 0;
      s.dims( len, n,gas.nvar(),n,n, -1 );

      assert( s.size() == len );

      v= new REAL_[len];
      memset( v,-1,len*sizeof(REAL_));

      for( l=0;l<gas.nvar();l++ )
     {
         for( k=0; k<n;k++ )
        {
            for( j=0; j<n;j++ )
           {
               for( i=0; i<n;i++ )
              {
                  size_t m= s.addr(l,i,j,k);
                  v[m]= u[l];
              }
           }
        }
     }

      
      MPI_File fh;
      MPI_Status status;
      MPI_Offset offlen, off0,off1;
      char fname[1024];

// write blocks

      sprintf(fname, "restart_init/h");
      MPI_File_open(MPI_COMM_WORLD, fname, MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);

      offlen= len*sizeof(REAL_);

      off0= 0;

      MPI_File_write_at( fh, off0,   &n, 1, INT_MPI, &status);  off0+= sizeof(INT_);
      MPI_File_write_at( fh, off0, &len, 1, LONG_MPI, &status); off0+= sizeof(size_t);

      for (l=0;l<hx.n;l++)
     {
        assert( hx[l].id > -1 ); 
        off1= off0+ offlen*hx[l].id;
        MPI_File_write_at( fh, off1, v, len, REAL_MPI, &status);
     }
      MPI_Barrier( MPI_COMM_WORLD );
      MPI_File_close( &fh );

// write boundaries

      sprintf( name, "restart_init/b");
      MPI_File_open(MPI_COMM_WORLD, name, MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);

      len=    s.bsize(3);
      offlen= len*sizeof(REAL_);

      off0= 0;

      MPI_File_write_at( fh, off0,   &n, 1, INT_MPI, &status); off0+= sizeof(INT_);
      MPI_File_write_at( fh, off0, &len, 1, LONG_MPI, &status); off0+= sizeof(size_t);

      for( INT_ p=0;p<BTYPES;p++ )
     {
         for( INT_ g=0;g<nb;g++ )
        {
            if( bz[g].lbl[0]== p && bz[g].bo.n > 0 )
           {
               for( k=0;k<bz[g].bo.n;k++ )
              {

                  MPI_File_write_at(fh, off0, v+0,   len, REAL_MPI, &status);
                  off0+= offlen;

              }   
           }   
        }   
     }   
      MPI_Barrier( MPI_COMM_WORLD );
      MPI_File_close( &fh );

      delete[] v; v= NULL;


      REAL_ w[NCJG_];
      memset( w,0,NCJG_*sizeof(REAL_) );

      sprintf( name, "restart_init/c" );
      f= fopen( name, "w" );
      w[0]= 1;
    ::fwrite(    w,NCJG_,sizeof(REAL_),  f );
      fclose( f );

  }
