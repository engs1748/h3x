#  include <q3x/q3x.h>

#  define QVD 0
#  define EDG 1
#  define VTX 2

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::contq( INT_ j, INT_ n, INT_ *c, INT_ *lc, INT_ *cprm, INT_ *m0,  com_t &com )
  {

      INT_       hst,hen;
      INT_       f,g,i,k,l;

      for( l=0;l<qv.n;l++ ){ m0[l]=-1; };

// range of local hex

      hst=  0;
      if( j > 0 ){ hst= lc[j-1]; };
      hen= lc[j];

// select quads with at least one hex in the local partition

      g= 0;
      for( f=hst;f<hen;f++ )  // note loop on local cells here
     {
         i= cprm[f];
         for( k=0;k<6;k++ )
        { 
            l= hx[i].q[k].q; 

// if quad has not been marked yet, mark it and add it to the local list

            if( m0[l] == -1 )
           {
               if( qv[l].h[0] > -1 && qv[l].h[1] > -1 )
              {
                  com.idx[g]= l;
                  m0[l]= g; 
                  g++;
              }
           }
        }
     }
      com.n= g;
      assert( g <= qv.n );

      return;
  }

   void q3x_t::conte( INT_ j, INT_ n, INT_ *c, INT_ *lc, INT_ *cprm, INT_ *m0,  com_t &com )
  {

      INT_       hst,hen;
      INT_       f,g,i,k,l;

      memset( m0,-1,eg.n*sizeof(INT_) );

// range of local hex

      hst=  0;
      if( j > 0 ){ hst= lc[j-1]; };
      hen= lc[j];

// select edges with at least one hex in the local partition

      g= 0;

      for( f=hst;f<hen;f++ )  // note loop on local cells here
     {
         i= cprm[f];
         for( k=0;k<12;k++ )
        { 
            l= hx[i].e[k].e; 

// if quad has not been marked yet, mark it and add it to the local list

            if( m0[l] == -1 )
           {
               com.idx[g]= l;
               m0[l]= g; 
               g++;
           }
        }
     }
      com.n= g;
      assert( g <= eg.n );

      return;
  }

   void q3x_t::contp( INT_ j, INT_ n, INT_ *c, INT_ *lc, INT_ *cprm, INT_ *m0,  com_t &com )
  {

      INT_       f,g,i,k,l;
      INT_       hst,hen;

      memset( m0,-1,vt.n*sizeof(INT_) );

// range of local hex

      hst=  0;
      if( j > 0 ){ hst= lc[j-1]; };
      hen= lc[j];

// select edges with at least one hex in the local partition

      g= 0;
      for( f=hst;f<hen;f++ )  // note loop on local cells here
     {
         i= cprm[f];
         for( k=0;k<8;k++ )
        { 
            l= hx[i].v[k]; 

// if point has not been marked yet, mark it and add it to the local list

            if( m0[l] == -1 )
           {
               com.idx[g]= l;
               m0[l]= g; 
               g++;
           }
        }
     }
      com.n= g;
      assert( g <= vt.n );

      return;
  }

