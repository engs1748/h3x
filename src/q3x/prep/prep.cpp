#  include <q3x/q3x.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 19:45:51 BST 2018
// Changes History
// Next Change(s)  -

   void q3x_t::prep()
  {
      INT_                           *c;
                                 
      INT_                        *cprm;
      INT_                        *hprm;
      INT_                        *bprm;
      INT_                        *qprm;
      INT_                          *lc;
      INT_                         *hid;

      INT_      *wc, *wh, *wq, *we, *wp;
      INT_                            n;
      INT_                        i,j,k;
      INT_                      ist,ien;
                               
      FILE                           *f;

      if( qv.n==0 ){ assert( false ); quads(); };
      if( eg.n==0 ){ edges(); };

      c=   new INT_[hx.n];
      hid= new INT_[hx.n];
      
      f= fopen( "part.bin","r" );
      assert( f );

    ::fread( &n, 1,sizeof(INT_),f );
    ::fread(  c,hx.n,sizeof(INT_),f );

      fclose( f );
 
// sort the cells by rank

      lc=   new INT_[n];
      memset( lc,0,n*sizeof(INT_) );
      cprm= new INT_[hx.n];
      hsort( hx.n,c, cprm );
      for( i=0;i<hx.n;i++ )
     {
         j= c[i]; 
         lc[j]++;
         hid[i]= hx[i].id;
     }
      accml( n,lc );

// generate local cell numbering for all ranks

      hprm= new INT_[hx.n];
      memset( hprm,-1,hx.n*sizeof(INT_) );

      ien= 0; 
      for( k=0;k<n;k++ )
     {
         ist= ien;
         ien= lc[k];
         for( i=ist;i<ien;i++ )
        {
            j= cprm[i];
            assert( hprm[j] == -1 );
            hprm[j]= i-ist;

        }
     }

// read boundary types and parse lb array - note use of wb as workspace

      bprm= new INT_[nb]; 
      qprm= new INT_[qv.n]; 
      INT_ lb[BTYPES];

      memset( lb,0,BTYPES*sizeof(INT_));

      for( k=0;k<nb;k++ )
     {
//       printf( "%d %d\n", k,bz[k].bo.n );
         if( bz[k].bo.n > 0 )
        {
            j= bz[k].lbl[0];
            assert( j > -1 && j < BTYPES );
            lb[j]++;
        }
     }
      accml( BTYPES,lb );
      shftl( BTYPES,lb );
      for( k=0;k<nb;k++ )
     {
         if( bz[k].bo.n > 0 )
        {
            j= bz[k].lbl[0];
            i= lb[j];
            bprm[i]= k;
            lb[j]++;
        }
     }

// allocate arrays for message lists

      com[0].dims( qv.n, 2*qv.n,n );// replace these with small initial size and reallocate as necessary
      com[1].dims( eg.n, 6*eg.n,n );// replace these with small initial size and reallocate as necessary
      com[2].dims( vt.n,10*vt.n,n );// replace these with small initial size and reallocate as necessary

      neig= 0;
      ineg= new INT_[n];

      wc=   new INT_[n];
      wh=   new INT_[hx.n];
      wq=   new INT_[qv.n];
      we=   new INT_[eg.n];
      wp=   new INT_[vt.n];

      i= rank;
      while( i<n )
     {

         printf( "preprocessing rank %6d/%6d\n", i,n );
         prep1( n,c,lc, cprm,hprm,hid, lb, bprm,qprm, i, wc,wh,wq,we,wp );
         i+= size;
     }
      delete[]  lc; lc=NULL;

      delete[]  c;    c=NULL;
      delete[]  cprm; cprm=NULL;
      delete[]  hprm; hprm=NULL;
      delete[]  bprm; bprm=NULL;
      delete[]  qprm; qprm=NULL;
      delete[]  ineg; ineg=NULL;

      delete[] wc;  wc=NULL;
      delete[] wh;  wh=NULL;
      delete[] wq;  wq=NULL;
      delete[] we;  we=NULL;
      delete[] wp;  wp=NULL;
  }
