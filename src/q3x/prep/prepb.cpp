#  include <q3x/q3x.h>


#  define QVD 0
#  define EDG 1
#  define VTX 2


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

// parallel pre-processing for rank j of n


   void q3x_t::prepb( INT_ n, INT_ *c, INT_ *lc, INT_ *cprm, INT_ *hprm, INT_ *hid, INT_ *lb, INT_ *bprm, 
                      INT_ *qprm, INT_ j, INT_ *wc, INT_ *wh, INT_ *wq, INT_ *we, INT_ *wp, FILE *o )
  {

      INT_       i,k,l,m;
      INT_       ist,ien;

      INT_2      buf2;
      INT_12     buf8;

      INT_4     *be;
      INT_      *le;

// count boundaries in each group - ideally set in a communicator

      ien= 0;
      l= 0;
      m=-1;
      for( INT_ g=0;g<BTYPES;g++ )
     {
         ist= ien;
         ien= lb[g];

         for( i=ist;i<ien;i++ ) 
        {

            m= bprm[i];
            for( INT_ f=0;f<bz[m].bo.n;f++ )
           {
               k= bz[m].bo[f].h;
               INT_ q= bz[m].bo[f].q;

               INT_ p0,p1,p2,p3;

               p0= bz[m].bo[f].v[0];
               p1= bz[m].bo[f].v[1];
               p2= bz[m].bo[f].v[2];
               p3= bz[m].bo[f].v[3];

               p0= bz[m].bx[p0].id;
               p1= bz[m].bx[p1].id;
               p2= bz[m].bx[p2].id;
               p3= bz[m].bx[p3].id;
// WARNING: MESH MOTION WON'T WORK IN PARALLEL LIKE THIS!
               if( ( ( wp[p0] > -1 ) || ( wp[p1] > -1 ) || ( wp[p2] > -1 ) || ( wp[p3] > -1 )  ) && wh[k] > -1 )
              { 
                  qprm[ hx[k].q[q].q ]= l;
                  assert( wh[k] > -1 );
                  l++; 
              };
           }
        }
         buf2[0]= l;
         buf2[1]= m;
       ::fwrite( buf2,1,sizeof(INT_2),o );
     }

      l= 0;
      ien= 0;
      for( INT_ g=0;g<BTYPES;g++ )
     {
         ist= ien;
         ien= lb[g];
         for( i=ist;i<ien;i++ ) 
        {

            m= bprm[i];
            for( INT_ f=0;f<bz[m].bo.n;f++ )
           {
          
               INT_ k= bz[m].bo[f].h;
               INT_ d= bz[m].bo[f].q;
               INT_ s= bz[m].bo[f].s;
               INT_ p= bz[m].bo[f].p;
               INT_ b= bz[m].bo[f].b;

               
               INT_ q= bz[m].bo[f].q;

          
               if( qprm[hx[k].q[q].q] > -1 )
              { 
                  assert( wh[k] > -1 );
                  frme_t x;
                  memset( x,0,sizeof(frme_t) );

                  buf8[ 0]=  wh[k];
                  buf8[ 1]=  d;
                  buf8[ 2]=  m;
                  buf8[ 3]=  bz[m].lbl[1]; // printf( "%d %d\n", g, buf8[3] );
                  buf8[ 4]=  f;
                  buf8[ 5]=  s;
                  buf8[ 6]=  p;
                  buf8[ 7]=  b;
                  buf8[ 8]=  l;
                  buf8[ 9]= -1;
                  buf8[10]= -1;
                  buf8[11]= -1;
                  l++;

                ::fwrite( buf8,1,sizeof(INT_12),o );
          
                  hx[k].qdir( d,x );
                  x[2]= abs(x[2]);
                  if( d%2 ){ x[2]=!x[2]; };

                ::fwrite( x,1,sizeof(frme_t),o );
              };
           }
        }
     }

// boundary edges

      
      be= new INT_4[ com[1].n ];
      le= new  INT_[ com[1].n ];

      memset( be,-1, com[1].n*sizeof(INT_4) );
      memset( le, 0, com[1].n*sizeof(INT_ ) );

      ien= 0;
      for( k=0;k<com[1].ldx[0];k++ )
     {
         ist= ien;
         ien= com[1].mdx[k][1];
         INT_ l=   com[1].mdx[k][0];
         i= com[1].idx[l];

         for( j=ist;j<ien;j++ )
        {
            INT_ h= com[1].list[j][0];
            h= indx[h];
            INT_ e= com[1].list[j][1];

            for( INT_ m=0;m<2;m++ )
           {
               INT_ p= meq[e][m];

               INT_ g= hx[h].q[p].g;
               if( g > -1 )
              {
                  INT_ q= hx[h].q[p].q;
                  INT_ b= hx[h].q[p].b;
                  assert( b > -1 );

// printf( "edge %d group %d %d %d \n", i, g,b, qprm[q] );

                  INT_ n= le[l];

                  be[l][n  ]= qprm[q];
                  be[l][n+1]= j;
                  le[l]+= 2;

                  assert( j < com[1].loc() );

              }

           }
        }
     }

    ::fwrite( be, com[1].n, sizeof(INT_4), o );

      delete[] be;
      delete[] le;

// boundary vertices

      INT_  *lv;
      INT_2 *bv;
      
      lv= new  INT_[ com[2].n ];

      memset( lv, 0, com[2].n*sizeof(INT_ ) );

// count boundary quads attached to local vertices
      ien= 0;
      for( k=0;k<com[2].ldx[0];k++ )
     {
         ist= ien;
         ien= com[2].mdx[k][1];
         INT_ l=   com[2].mdx[k][0];
         i= com[2].idx[l];
         assert( l < com[2].n );

         INT_ h= com[2].list[ist][0];
         h= indx[h];

         for( j=ist;j<ien;j++ )
        {
            INT_ h= com[2].list[j][0];
            h= indx[h];
            INT_ v= com[2].list[j][1];
            const INT_ pq[8][3]= { {0,4,2}, {2,4,1}, {3,4,0}, {1,4,3}, 
                             {0,2,5}, {2,1,5}, {3,0,5}, {1,3,5} };      

            for( INT_ m=0;m<3;m++ )
           {

               INT_ p= pq[v][m];

               INT_ g= hx[h].q[p].g;
               if( g > -1 )
              {

                  INT_ b= hx[h].q[p].b;
                  assert( b > -1 );

                  lv[l]++;
              }
           }
        }
     }

      for( k=1;k<com[2].n;k++ )
     {
         lv[k]+= lv[k-1];
     }

      bv= new INT_2[ lv[com[2].n-1] ];
      memset( bv,-1, lv[com[2].n-1]*sizeof(INT_2) );

      for( k=com[2].n-1;k>0;k-- ){ lv[k]= lv[k-1]; }
      lv[0]=0;

      ien= 0;
      for( k=0;k<com[2].ldx[0];k++ )
     {
         ist= ien;
         ien= com[2].mdx[k][1];
         INT_ l=   com[2].mdx[k][0];
         i= com[2].idx[l];


         assert( ien <= com[2].loc() );
         for( j=ist;j<ien;j++ )
        {
            INT_ h= com[2].list[j][0];
            h= indx[h];
            INT_ v= com[2].list[j][1];
            const INT_ pq[8][3]= { {0,4,2}, {2,4,1}, {3,4,0}, {1,4,3}, 
                                   {0,2,5}, {2,1,5}, {3,0,5}, {1,3,5} };      

            for( INT_ m=0;m<3;m++ )
           {

               INT_ p= pq[v][m];

               INT_ g= hx[h].q[p].g;
               if( g > -1 )
              {
                  INT_ q= hx[h].q[p].q;
                  INT_ b= hx[h].q[p].b;
                  assert( b > -1 );

                  INT_ n= lv[l];
   
                  bv[n][0]= qprm[q];
                  bv[n][1]= j;
                  lv[l]++;

              }

           }
        }
     }

    ::fwrite( lv,    com[2].n, sizeof(INT_ ), o );
    ::fwrite( bv, lv[com[2].n-1], sizeof(INT_2), o );
      delete[] bv;
      delete[] lv;

      return;
  }
