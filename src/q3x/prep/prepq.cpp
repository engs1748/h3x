#  include <q3x/q3x.h>

#  define QVD 0
#  define EDG 1
#  define VTX 2


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::prepq( INT_ *c, com_t &com )
  {

      INT_       a,b,d,f,g,i,j,k,o,s;
      frme_t     x;


// now grow list0s of hex on neighboring cores and dependencies

      o= 0;
      g= 0;

      for( j=0;j<neig;j++ )
     {
         for( f=0;f<com.n;f++ )
        {
            i= com.idx[f];
            s= o;
            for( k=0;k<2;k++ )
           {
               a= qv[i].h[k];
// find the rank cell a belongs to and add to halo list0 
               b= c[a];
               if( b == ineg[j] )
              {
                  d= qv[i].q[k];
       
                  com.list[o][0]= a;
                  com.list[o][1]= qv[i].q[k];
                  com.list[o][3]= k;
                 
                  hx[a].qdir( d,x );
                  x[2]= abs(x[2]);
                  if( d%2 ){ x[2]=!x[2]; };
      
                  com.hfx[o][0]= x[0];
                  com.hfx[o][1]= x[1];
                  com.hfx[o][2]= x[2];
                  o++;
              }
           }
            if( o > s )
           { 
               com.mdx[g][0]= f;
               com.mdx[g][1]= o;
               g++;
           }
        }
         com.ldx[j]= g;
     }
      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::prepe( INT_ *c, com_t &com )
  {

      INT_       a,b,d,f,g,i,j,k,o,s;
      frme_t     x;

      VINT_2     list;

// now grow list0s of hex on neighboring cores and dependencies
      o= 0;
      g= 0;
      for( j=0;j<neig;j++ )
     {
         for( f=0;f<com.n;f++ )
        {
            i= com.idx[f];
            edge( i, list );
       
            s= o;
            for( k=0;k<list.n;k++ )
           {
               a= list[k][0];
               d= list[k][1];

// find the rank cell a belongs to and add to halo list0 
               b= c[a];
               if( b == ineg[j] )
              {
                
                  com.list[o][0]= a;
                  com.list[o][1]= d;
                  com.list[o][3]= k;
                
                  hx[a].edir( d,x );
                
                  com.hfx[o][0]= x[0];
                  com.hfx[o][1]= x[1];
                  com.hfx[o][2]= x[2];
                
                  o++;
              }
           }
            if( o > s )
           { 
               com.mdx[g][0]= f;
               com.mdx[g][1]= o;
               g++;
           }
        }
         com.ldx[j]= g;
     }

      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::prepp( INT_ *c, com_t &com )
  {

      VINT_2     list;
      VINT_2     dum;
      INT_       a,b,d,f,g,i,j,k,o,s;
      frme_t     x;

// now grow list0s of hex on neighboring cores and dependencies

      o= 0;
      g= 0;
      for( j=0;j<neig;j++ )
     {
         for( f=0;f<com.n;f++ )
        {
            i= com.idx[f];
            point( i, list,dum );
       
            s= o;
            for( k=0;k<list.n;k++ )
           {
               a= list[k][0];
               d= list[k][1];

// find the rank cell a belongs to and add to halo list0
               b= c[a];
               if( b == ineg[j] )
              {
                  com.list[o][0]= a;
                  com.list[o][1]= d;
                  com.list[o][3]= k;
                
                  hx[a].pdir( d,x );
                
                  com.hfx[o][0]= x[0];
                  com.hfx[o][1]= x[1];
                  com.hfx[o][2]= x[2];
                  o++;
              }
           }
            if( o > s )
           { 
               com.mdx[g][0]= f;
               com.mdx[g][1]= o;
               g++;
           }
        }
         com.ldx[j]= g;
     }

      return;
  }

