#  include <q3x/q3x.h>


#  define QVD 0
#  define EDG 1
#  define VTX 2


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::neigq( INT_ j, INT_ n, INT_ *c, INT_ *lc, INT_ *cprm, INT_ *m0, INT_ *wc, com_t &com )
  {

      INT_       a,b,f,i,k;

// now grow lists of hex on neighboring cores and dependencies

      for( f=0;f<com.n;f++ )
     {
         i= com.idx[f];
         for( k=0;k<2;k++ )
        {
            a= qv[i].h[k];
// find the rank cell a belongs to and add to halo list 
            b= c[a];
            wc[b]= 1; 
        }
     }
      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::neige( INT_ j, INT_ n, INT_ *c, INT_ *lc, INT_ *cprm, INT_ *m0, INT_ *wc, com_t &com )
  {

      INT_       a,b,f,i,k;
      VINT_2     list;

// now grow lists of hex on neighboring cores and dependencies

      for( f=0;f<com.n;f++ )
     {
         i= com.idx[f];
         edge( i,list );

         for( k=0;k<list.n;k++ )
        {
            a= list[k][0];
// find the rank cell a belongs to and add to halo list 
            b= c[a];
            wc[b]= 1; 
        }
     }

      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void q3x_t::neigp( INT_ j, INT_ n, INT_ *c, INT_ *lc, INT_ *cprm, INT_ *m0, INT_ *wc, com_t &com )
  {

      INT_       a,b,f,i,k;

      VINT_2     list,dum;

// now grow lists of hex on neighboring cores and dependencies

      for( f=0;f<com.n;f++ )
     {
         i= com.idx[f];
         point( i, list,dum );

//       printf( "list.n %d\n", list.n );

         for( k=0;k<list.n;k++ )
        {
            a= list[k][0];
// find the rank cell a belongs to and add to halo list
            b= c[a];
            wc[b]= 1; 
        }
     }

      return;
  }

