#  include <q3x/q3x.h>


#  define QVD 0
#  define EDG 1
#  define VTX 2


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

// parallel pre-processing for rank j of n


   void q3x_t::prep1( INT_ n, INT_ *c, INT_ *lc, INT_ *cprm, INT_ *hprm, INT_ *hid, INT_ *lb, INT_ *bprm, 
                      INT_ *qprm, INT_ j, INT_ *wc, INT_ *wh, INT_ *wq, INT_ *we, INT_ *wp )
  {

      INT_       i,k,l;
      INT_       ist,ien,kst,ken;
      char       name[512];

      com[0].reset(qv.n, 2*qv.n,n);
      com[1].reset(eg.n, 6*eg.n,n);
      com[2].reset(vt.n,10*vt.n,n);

// reset marker arrays etc

      memset( wc,-1, n*sizeof(INT_) );
      memset( wh,-1,hx.n*sizeof(INT_) );
      memset( wq,-1,qv.n*sizeof(INT_) );
      memset( we,-1,eg.n*sizeof(INT_) );
      memset( wp,-1,vt.n*sizeof(INT_) );

      memset( qprm,-1,qv.n*sizeof(INT_) );

// select local cells - note full numbering is already available in hprm/cprm

      ist= 0;
      if( j > 0 ){ ist= lc[j-1]; }
      ien= lc[j];
      for( i=ist;i<ien;i++ )
     {
         k= cprm[i];
         wh[k]= i-ist;
     }


// select quads, edges and vertices with at least one hex in the local partition
// also make a list of the remote cells touched by these quads/edges/vertices.

      contq( j, n, c, lc, cprm, wq,     com[0] );
      conte( j, n, c, lc, cprm, we,     com[1] );
      contp( j, n, c, lc, cprm, wp,     com[2] );

      neigq( j, n, c, lc, cprm, wq, wc, com[0] );
      neige( j, n, c, lc, cprm, we, wc, com[1] );
      neigp( j, n, c, lc, cprm, wp, wc, com[2] );

// note com[3] does not contribute neighbors

// select neighboring cores

      l= 0;
      ineg[l]= j;
      wc[j]= l;
      l++;
      for( i=0;i<j;i++ )
     {
         if( wc[i] > -1 )
        { 
            ineg[l]= i; 
            wc[i]= l; 
            l++;
        };
     }
      for( i=j+1;i<n;i++ )
     {
         if( wc[i] > -1 )
        { 
            ineg[l]= i; 
            wc[i]= l; 
            l++;
        };
     }
      neig=l;

// complete preprocessing for connectivity entities

      prepq( c, com[0] );
      prepe( c, com[1] );
      prepp( c, com[2] );

// create dependency lists and receive message lists
// for every connection find the ranks it depends upon

      com[0].rcvl( neig, c, wc, hprm,hid );
      com[1].rcvl( neig, c, wc, hprm,hid );
      com[2].rcvl( neig, c, wc, hprm,hid );

// output section

      sprintf( name,"prep/%06d.bin", j );

      FILE *o=NULL;
      o= fopen( name,"w" );

      assert( o );

    ::fwrite( &n,1,sizeof(INT_),o );
      fwritep( o );
      
      for( k=0;k<com[0].n;k++ )
     {
         i= com[0].idx[k];
         qv[i].fwrite( o,wh );
     }
      for( k=0;k<com[1].n;k++ )
     {
         i= com[1].idx[k];
         eg[i].fwrite( o,wh );
     }
      for( k=0;k<com[2].n;k++ )
     {
         i= com[2].idx[k];
         vt[i].fwrite( o,wh );
     }

      ken= 0;
      kst= 0;
      if( j>0 ){ kst= lc[j-1]; };
      ken= lc[j];

      l= ken-kst;
    ::fwrite( &l,1,sizeof(INT_), o );
      for( k=kst;k<ken;k++ )
     {
         i= cprm[k];
         hx[i].fwrite( o,wp,we,wq );
     }

// boundaries

      prepb(  n,c,lc,cprm,  hprm,hid, lb,  bprm,qprm,  j,  wc,wh,wq,we,wp, o );

      ni= 0;
      for( l=0;l<bi.n;l++ )
     {
         for( i=0;i<bi[l].bo.n;i++ )
        {
            k= bi[l].bo[i].id; 
            if( wq[k] > -1 ){ ni++; };
        }
     }

    ::fwrite( &ni,1,sizeof(INT_),o );
      INT_8 buf;

      for( l=0;l<bi.n;l++ )
     {
         for( i=0;i<bi[l].bo.n;i++ )
        {
            k= bi[l].bo[i].id;
            if( wq[k] > -1 )
           {
               INT_ a,b;

               a= qv[k].h[0];
               b= qv[k].h[1];

               buf[0]=  i;
               buf[1]=  k;
               buf[2]=  wq[k];
               buf[3]=  bi[l].s;
               buf[4]=  l;
               buf[5]=  hx[a].id;
               buf[6]=  hx[b].id;
               buf[7]= -1;
             ::fwrite( buf,1,sizeof( INT_8 ),o );
           }
        }
     }

      fclose( o );
      return;
  }
