


#     include <sizes.h>
#     include <band11.h>

      void inline adotx1( block_t mat, block_t vec, INT_ v, INT_ n, INT_ j, INT_ k, INT_ h, INT_ l, INT_ m,
                          REAL_ *a, REAL_ *x, REAL_ *y )
     {
         INT_ i;
         INT_ i0,i1,i2, j0,j1,j2, k0;

         assert( mat.n[0] == vec.n[0] );
         assert( mat.n[1] == vec.n[1] );
         assert( mat.n[2] == vec.n[2] );

         i= 0;

         i1=   mat.addr( i,j,k, 1,m,n );
         i2=   mat.addr( i,j,k, 2,m,n );

         j1=   vec.addr( i  ,h,l,v,n );
         j2=   vec.addr( i+1,h,l,v,n );

         k0=   vec.addr( i  ,j,k,v,n );

         y[k0]+= a[i1]*x[j1];
         y[k0]+= a[i2]*x[j2];

/*       dot33( a+i1, x+j1, y+k0 );
         dot33( a+i2, x+j2, y+k0 );*/

         for( i=1;i<vec.n[0];i++ )
        {

            i0=   mat.addr( i,j,k, 0,m,n );
            i1=   mat.addr( i,j,k, 1,m,n );
            i2=   mat.addr( i,j,k, 2,m,n );

            j0=   vec.addr( i-1,h,l,v,n );
            j1=   vec.addr( i  ,h,l,v,n );
            j2=   vec.addr( i+1,h,l,v,n );

            k0=   vec.addr( i  ,j,k,v,n );

/*          dot33( a+i0, x+j0, y+k0 );
            dot33( a+i1, x+j1, y+k0 );
            dot33( a+i2, x+j2, y+k0 );*/

            y[k0]+= a[i0]*x[j0];
            y[k0]+= a[i1]*x[j1];
            y[k0]+= a[i2]*x[j2];

        }
         i= vec.n[0];
         i0=   mat.addr( i,j,k, 0,m,n );
         i1=   mat.addr( i,j,k, 1,m,n );

         j0=   vec.addr( i-1,h,l,v,n );
         j1=   vec.addr( i  ,h,l,v,n );

         k0=   vec.addr( i  ,j,k,v,n );

/*       dot33( a+i0, x+j0, y+k0 );
         dot33( a+i1, x+j1, y+k0 );*/

         y[k0]+= a[i0]*x[j0];
         y[k0]+= a[i1]*x[j1];

         return;
     }

      void adotx( block_t mat, block_t vec, INT_ v, INT_ n, REAL_ *a, REAL_ *x, REAL_ *y )
     {
         INT_      j,k;

         assert( mat.n[0] == vec.n[0] );
         assert( mat.n[1] == vec.n[1] );
         assert( mat.n[2] == vec.n[2] );

         memset( y,0,vec.len() );

         k= 0;
         j= 0;

         adotx1( mat,vec, v,n,j,k, j  ,k,   4, a,x, y );
         adotx1( mat,vec, v,n,j,k, j+1,k,   5, a,x, y );

         adotx1( mat,vec, v,n,j,k, j  ,k+1, 7, a,x, y );
         adotx1( mat,vec, v,n,j,k, j+1,k+1, 8, a,x, y );

         for( j=1;j<vec.n[1];j++ )
        {

            adotx1( mat,vec, v,n,j,k, j-1,k,   3, a,x, y );
            adotx1( mat,vec, v,n,j,k, j  ,k,   4, a,x, y );
            adotx1( mat,vec, v,n,j,k, j+1,k,   5, a,x, y );

            adotx1( mat,vec, v,n,j,k, j-1,k+1, 6, a,x, y );
            adotx1( mat,vec, v,n,j,k, j  ,k+1, 7, a,x, y );
            adotx1( mat,vec, v,n,j,k, j+1,k+1, 8, a,x, y );
        }

         j= vec.n[1];

         adotx1( mat,vec, v,n,j,k, j-1,k,   3, a,x, y );
         adotx1( mat,vec, v,n,j,k, j  ,k,   4, a,x, y );

         adotx1( mat,vec, v,n,j,k, j-1,k+1, 6, a,x, y );
         adotx1( mat,vec, v,n,j,k, j  ,k+1, 7, a,x, y );

         for( k=1;k<vec.n[2];k++ )
        {

            j= 0;

            adotx1( mat,vec, v,n,j,k, j  ,k-1, 1, a,x, y );
            adotx1( mat,vec, v,n,j,k, j+1,k-1, 2, a,x, y );

            adotx1( mat,vec, v,n,j,k, j  ,k,   4, a,x, y );
            adotx1( mat,vec, v,n,j,k, j+1,k,   5, a,x, y );

            adotx1( mat,vec, v,n,j,k, j  ,k+1, 7, a,x, y );
            adotx1( mat,vec, v,n,j,k, j+1,k+1, 8, a,x, y );

            for( j=1;j<vec.n[1];j++ )
           {

               adotx1( mat,vec, v,n,j,k, j-1,k-1, 0, a,x, y );
               adotx1( mat,vec, v,n,j,k, j  ,k-1, 1, a,x, y );
               adotx1( mat,vec, v,n,j,k, j+1,k-1, 2, a,x, y );

               adotx1( mat,vec, v,n,j,k, j-1,k,   3, a,x, y );
               adotx1( mat,vec, v,n,j,k, j  ,k,   4, a,x, y );
               adotx1( mat,vec, v,n,j,k, j+1,k,   5, a,x, y );

               adotx1( mat,vec, v,n,j,k, j-1,k+1, 6, a,x, y );
               adotx1( mat,vec, v,n,j,k, j  ,k+1, 7, a,x, y );
               adotx1( mat,vec, v,n,j,k, j+1,k+1, 8, a,x, y );

           }

            j= vec.n[1];

            adotx1( mat,vec, v,n,j,k, j-1,k-1, 0, a,x, y );
            adotx1( mat,vec, v,n,j,k, j  ,k-1, 1, a,x, y );

            adotx1( mat,vec, v,n,j,k, j-1,k,   3, a,x, y );
            adotx1( mat,vec, v,n,j,k, j  ,k,   4, a,x, y );

            adotx1( mat,vec, v,n,j,k, j-1,k+1, 6, a,x, y );
            adotx1( mat,vec, v,n,j,k, j  ,k+1, 7, a,x, y );

        }

         k= vec.n[2];

         j= 0;

         adotx1( mat,vec, v,n,j,k, j  ,k-1, 1, a,x, y );
         adotx1( mat,vec, v,n,j,k, j+1,k-1, 2, a,x, y );

         adotx1( mat,vec, v,n,j,k, j  ,k,   4, a,x, y );
         adotx1( mat,vec, v,n,j,k, j+1,k,   5, a,x, y );

         for( j=1;j<vec.n[1];j++ )
        {
            adotx1( mat,vec, v,n,j,k, j-1,k-1, 0, a,x, y );
            adotx1( mat,vec, v,n,j,k, j  ,k-1, 1, a,x, y );
            adotx1( mat,vec, v,n,j,k, j+1,k-1, 2, a,x, y );
    
            adotx1( mat,vec, v,n,j,k, j-1,k,   3, a,x, y );
            adotx1( mat,vec, v,n,j,k, j  ,k,   4, a,x, y );
            adotx1( mat,vec, v,n,j,k, j+1,k,   5, a,x, y );
   
        }

         j= vec.n[1];

         adotx1( mat,vec, v,n,j,k, j-1,k-1, 0, a,x, y );
         adotx1( mat,vec, v,n,j,k, j  ,k-1, 1, a,x, y );

         adotx1( mat,vec, v,n,j,k, j-1,k,   3, a,x, y );
         adotx1( mat,vec, v,n,j,k, j  ,k,   4, a,x, y );

         return;

     }

