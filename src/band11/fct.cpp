#  include <band11.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void cilufd3( block_t var, INT_ n, INT_ j, INT_ k, REAL_ *a )
  {
      INT_    i;
      INT_    i0,i1,i2,i3;
      REAL_ d;
      for( i=0;i<var.n[0];i++ )
     {

         i0= var.addr(  i,j,k,1,D,n);
         i1= var.addr(  i,j,k,2,D,n);

         i2= var.addr(1+i,j,k,0,D,n);
         i3= var.addr(1+i,j,k,1,D,n);

         d= a[i0];
         d= 1./d;

         a[i0]=  d;
         a[i1]*= d;
         a[i3]-= a[i2]*a[i1];
     }     

      i= var.n[0];
      i0= var.addr(  i,j,k,1,D,n);
      d= a[i0];
      d= 1./d;
      a[i0]=  d;

      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void cilufu3( block_t var, INT_ n, INT_ j, INT_ k, INT_ m, REAL_ *a )
  {

      REAL_         d;
      INT_            i;

      INT_            i0,i1,i2,i3,i4,i5,i6;

      i=0;

      i0= var.addr(  i,j,k,1,D,n);

      i2= var.addr(  i,j,k,1,m,n); 
      i3= var.addr(  i,j,k,2,m,n); 

      i4= var.addr(i+1,j,k,0,m,n); 
      i5= var.addr(i+1,j,k,1,m,n); 

      i6= var.addr(i+1,j,k,0,D,n);
      
      d= a[i0];

      a[i2]*= d;
      a[i3]*= d;
      
      a[i4]-= a[i6]*a[i2];
      a[i5]-= a[i6]*a[i3];


      for( i=1;i<var.n[0];i++ )
     {
         i0= var.addr(  i,j,k,1,D,n);

         i1= var.addr(  i,j,k,0,m,n); 
         i2= var.addr(  i,j,k,1,m,n); 
         i3= var.addr(  i,j,k,2,m,n); 

         i4= var.addr(i+1,j,k,0,m,n); 
         i5= var.addr(i+1,j,k,1,m,n); 

         i6= var.addr(i+1,j,k,0,D,n);
        
         d= a[i0];

         a[i1]*= d;
         a[i2]*= d;
         a[i3]*= d;
        
         a[i4]-= a[i6]*a[i2];
         a[i5]-= a[i6]*a[i3];

     }

      i= var.n[0];
      i0= var.addr(i,j,k,1,D,n);

      i1= var.addr(i,j,k,0,m,n); 
      i2= var.addr(i,j,k,1,m,n); 

      d= a[i0];

      a[i1]*= d;
      a[i2]*= d;
      
      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void cilufl3( block_t var, INT_ n, INT_ j, INT_ k, INT_ h, INT_ l, INT_ m, REAL_ *a )
  {
      INT_        i;

      INT_        i0,i1,i2,i3,i4;
      
      for( i=0;i<var.n[0];i++ )
     {
         i0= var.addr(i  ,h,l,2,m, n);
         i1= var.addr(i+1,h,l,1,m, n);

         i2= var.addr(i  ,h,l,1,m, n);
         i3= var.addr(i+1,h,l,0,m, n);

         i4= var.addr(i  ,j,k,2,D, n);

         a[i0]-= a[i2]*a[i4];
         a[i1]-= a[i3]*a[i4];

     }
      return;

  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void cilufg3( block_t var, INT_ n, INT_ j, INT_ k, INT_ h, INT_ l, INT_ p, INT_ q, INT_ o, 
                      REAL_ *a )
  {
      INT_             i;
      INT_             i0,i1,i2,i3,i4,i5,i6;
      INT_             j0,j1,j2;
      INT_             k0,k1,k2,k3,k4,k5,k6;

      i= 0;

      i3= var.addr(i  ,h,l,1,o,n);
      i4= var.addr(i  ,h,l,2,o,n);

      i5= var.addr(i+1,h,l,0,o,n);
      i6= var.addr(i+1,h,l,1,o,n);
      
      j1= var.addr(i  ,h,l,1,p,n);
      j2= var.addr(i+1,h,l,0,p,n);

      k3= var.addr(i  ,j,k,1,q,n);
      k4= var.addr(i  ,j,k,2,q,n);
        
      k5= var.addr(i  ,j,k,1,q,n);
      k6= var.addr(i  ,j,k,2,q,n);

      a[i3]-= a[j1]*a[k3];
      a[i4]-= a[j1]*a[k4];
                      
      a[i5]-= a[j2]*a[k5];
      a[i6]-= a[j2]*a[k6];

      for( i=1;i<var.n[0];i++ )
     {

         i0= var.addr(i-1,h,l,1,o,n);
         i1= var.addr(i-1,h,l,2,o,n);

         i2= var.addr(i  ,h,l,0,o,n);
         i3= var.addr(i  ,h,l,1,o,n);
         i4= var.addr(i  ,h,l,2,o,n);

         i5= var.addr(i+1,h,l,0,o,n);
         i6= var.addr(i+1,h,l,1,o,n);
         
         j0= var.addr(i-1,h,l,2,p,n);
         j1= var.addr(i  ,h,l,1,p,n);
         j2= var.addr(i+1,h,l,0,p,n);

         k0= var.addr(i  ,j,k,0,q,n);
         k1= var.addr(i  ,j,k,1,q,n);
           
         k2= var.addr(i  ,j,k,0,q,n);
         k3= var.addr(i  ,j,k,1,q,n);
         k4= var.addr(i  ,j,k,2,q,n);
           
         k5= var.addr(i  ,j,k,1,q,n);
         k6= var.addr(i  ,j,k,2,q,n);

         a[i0]-= a[j0]*a[k0];
         a[i1]-= a[j0]*a[k1];
                         
         a[i2]-= a[j1]*a[k2];
         a[i3]-= a[j1]*a[k3];
         a[i4]-= a[j1]*a[k4];
                         
         a[i5]-= a[j2]*a[k5];
         a[i6]-= a[j2]*a[k6];

     }

      i= var.n[0];

      i0= var.addr(i-1,h,l,1,o,n);
      i1= var.addr(i-1,h,l,2,o,n);

      i2= var.addr(i  ,h,l,0,o,n);
      i3= var.addr(i  ,h,l,1,o,n);

      j0= var.addr(i-1,h,l,2,p,n);
      j1= var.addr(i  ,h,l,1,p,n);

      k0= var.addr(i  ,j,k,0,q,n);
      k1= var.addr(i  ,j,k,1,q,n);
        
      k2= var.addr(i  ,j,k,0,q,n);
      k3= var.addr(i  ,j,k,1,q,n);

      a[i0]-= a[j0]*a[k0];
      a[i1]-= a[j0]*a[k1];
                      
      a[i2]-= a[j1]*a[k2];
      a[i3]-= a[j1]*a[k3];
                         
      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void iluf( block_t var, INT_ n, REAL_ *a )
  {
      INT_               j,k;

//... factorize

      for( k=0;k<var.n[2];k++ )
     {
        j= 0;

        cilufd3( var,n, j,k,        a );
        cilufu3( var,n, j,k,      5,a );
       
        cilufu3( var,n, j,k,      7,a );
        cilufu3( var,n, j,k,      8,a );
       
        cilufl3( var,n, j,k,j+1,k  ,3,a );
       
        cilufl3( var,n, j,k,j  ,k+1,1,a );
        cilufl3( var,n, j,k,j+1,k+1,0,a );

//                                  c b d
        cilufg3( var,n, j,k,j+1,  k,3,5,4,a );
        cilufg3( var,n, j,k,j+1,  k,3,7,6,a );
        cilufg3( var,n, j,k,j+1,  k,3,8,7,a );

        cilufg3( var,n, j,k,j  ,k+1,1,5,2,a );
        cilufg3( var,n, j,k,j  ,k+1,1,7,4,a );
        cilufg3( var,n, j,k,j  ,k+1,1,8,5,a );

        cilufg3( var,n, j,k,j+1,k+1,0,5,1,a );
        cilufg3( var,n, j,k,j+1,k+1,0,7,3,a );
        cilufg3( var,n, j,k,j+1,k+1,0,8,4,a );

        for( j=1;j<var.n[1];j++ ) 
       {
       
           cilufd3( var,n, j,k,        a );
           cilufu3( var,n, j,k,      5,a );
        
           cilufu3( var,n, j,k,      6,a );
           cilufu3( var,n, j,k,      7,a );
           cilufu3( var,n, j,k,      8,a );
        
           cilufl3( var,n, j,k,j+1,k  ,3,a );
        
           cilufl3( var,n, j,k,j-1,k+1,2,a );
           cilufl3( var,n, j,k,j  ,k+1,1,a );
           cilufl3( var,n, j,k,j+1,k+1,0,a );

//                                     c b d
           cilufg3( var,n, j,k,j+1,  k,3,5,4,a );
           cilufg3( var,n, j,k,j+1,  k,3,7,6,a );
           cilufg3( var,n, j,k,j+1,  k,3,8,7,a );

           cilufg3( var,n, j,k,j-1,k+1,2,6,4,a );
           cilufg3( var,n, j,k,j-1,k+1,2,7,5,a );

           cilufg3( var,n, j,k,j  ,k+1,1,5,2,a );
           cilufg3( var,n, j,k,j  ,k+1,1,6,3,a );
           cilufg3( var,n, j,k,j  ,k+1,1,7,4,a );
           cilufg3( var,n, j,k,j  ,k+1,1,8,5,a );

           cilufg3( var,n, j,k,j+1,k+1,0,5,1,a );
           cilufg3( var,n, j,k,j+1,k+1,0,7,3,a );
           cilufg3( var,n, j,k,j+1,k+1,0,8,4,a );
       
       }

        j= var.n[1];

        cilufd3( var,n, j,k,        a );
       
        cilufu3( var,n, j,k,      6,a );
        cilufu3( var,n, j,k,      7,a );
       
        cilufl3( var,n, j,k,j-1,k+1,2,a );
        cilufl3( var,n, j,k,j  ,k+1,1,a );

//                                  c b d
        cilufg3( var,n, j,k,j-1,k+1,2,6,4,a );
        cilufg3( var,n, j,k,j-1,k+1,2,7,5,a );

        cilufg3( var,n, j,k,j  ,k+1,1,6,3,a );
        cilufg3( var,n, j,k,j  ,k+1,1,7,4,a );
                                     
     }

      k= var.n[2];

      for( j=0;j<var.n[1];j++ )
     {
      
        cilufd3( var,n, j,k,        a );
        cilufu3( var,n, j,k,      5,a );
      
      
        cilufl3( var,n, j,k,j+1,k  ,3,a );
//                                  c b d
        cilufg3( var,n, j,k,j+1,  k,3,5,4,a );
                                     
     }

      j= var.n[1];
      cilufd3( var,n, j,k,        a );
      
      return;

  }
