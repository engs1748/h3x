#  include <band11.h>


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void cilusdf3( block_t mat, block_t vec, INT_ v, INT_ n, INT_ j, INT_ k, REAL_ *a, REAL_ *b )
  {

      REAL_    d;
      INT_       i;

      INT_       i0,i1;
      INT_       j0,j1;

      for( i=0;i<vec.n[0];i++ )
     {
        i0= mat.addr(  i,j,k,1,D,n);
        i1= mat.addr(i+1,j,k,0,D,n);
        d= a[i0];

        j0= vec.addr(  i,j,k  ,v,n); 
        j1= vec.addr(1+i,j,k  ,v,n); 

        b[j0]*= d;
        b[j1]-= a[i1]*b[j0];
     }

      i=vec.n[0];

      i0= mat.addr(  i,j,k,1,D);
      j0= vec.addr(  i,j,k  ); 

      d= a[i0];
      b[j0]*= d;

      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void cilusf3( block_t mat, block_t vec, INT_ v, INT_ n, INT_ j, INT_ k, INT_ h, INT_ l, INT_ m, REAL_ *a, REAL_ *b )
  {

      INT_               i;
      INT_               j0;
      INT_               i0,i1,i2;
      INT_               k0,k1,k2;

      i= 0;

      i1= mat.addr(i  ,h,l,1,m,n);
      i2= mat.addr(i+1,h,l,0,m,n);

      j0= vec.addr(i,  j,k,v,n);

      k1= vec.addr(i  ,h,l,v,n);
      k2= vec.addr(i+1,h,l,v,n);


      b[k1]-= a[i1]*b[j0];
      b[k2]-= a[i2]*b[j0];

      for( i=1;i<vec.n[0];i++ )
     {

         i0= mat.addr(i-1,h,l,2,m,n);
         i1= mat.addr(i  ,h,l,1,m,n);
         i2= mat.addr(i+1,h,l,0,m,n);

         j0= vec.addr(i,  j,k,v,n);

         k0= vec.addr(i-1,h,l,v,n);
         k1= vec.addr(i  ,h,l,v,n);
         k2= vec.addr(i+1,h,l,v,n);


         b[k0]-= a[i0]*b[j0];
         b[k1]-= a[i1]*b[j0];
         b[k2]-= a[i2]*b[j0];

     }
      i= vec.n[0];

      i0= mat.addr(i-1,h,l,2,m,n);
      i1= mat.addr(i  ,h,l,1,m,n);

      j0= vec.addr(i,  j,k,v,n);

      k0= vec.addr(i-1,h,l,v,n);
      k1= vec.addr(i  ,h,l,v,n);


      b[k0]-= a[i0]*b[j0];
      b[k1]-= a[i1]*b[j0];

      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void cilusdb3( block_t mat, block_t vec, INT_ v, INT_ n, INT_ j, INT_ k, REAL_ *a, REAL_ *b )
  {
      INT_            i;
      INT_            i0,j0,j1;

      for( i=vec.n[0]-1;i>=0;i-- )
     {
         i0= mat.addr(  i,j,k,2,D);
         j0= vec.addr(  i,j,k,v,n);
         j1= vec.addr(1+i,j,k,v,n);

         b[j0]-= a[i0]*b[j1];
     }
      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void cilusb3( block_t mat, block_t vec, INT_ v, INT_ n, INT_ j, INT_ k, INT_ h, INT_ l, INT_ m, REAL_ *a, REAL_ *b )
  {

      REAL_            d;
      INT_               i;
      INT_               i0,i1,i2;
      INT_               j0,j1,j2;
      INT_               k0;
      i= vec.n[0];

      i0= mat.addr(i,j,k,0,m,n);
      i1= mat.addr(i,j,k,1,m,n);

      j0= vec.addr(i-1,h,l,v,n);
      j1= vec.addr(i  ,h,l,v,n);

      k0= vec.addr(i  ,j,k,v,n);

      d= a[i0]*b[j0]+
         a[i1]*b[j1];

      b[k0]-= d;

      for( i=vec.n[0]-1;i>0;i-- )
     {

         i0= mat.addr(i,j,k,0,m,n);
         i1= mat.addr(i,j,k,1,m,n);
         i2= mat.addr(i,j,k,2,m,n);

         j0= vec.addr(i-1,h,l,v,n);
         j1= vec.addr(i  ,h,l,v,n);
         j2= vec.addr(i+1,h,l,v,n);

         k0= vec.addr(i  ,j,k,v,n);

         d= a[i0]*b[j0]+
            a[i1]*b[j1]+
            a[i2]*b[j2];

         b[k0]-= d;

     }

      i= 0;

      i1= mat.addr(i,j,k,1,m,n);
      i2= mat.addr(i,j,k,2,m,n);

      j1= vec.addr(i  ,h,l,v,n);
      j2= vec.addr(i+1,h,l,v,n);

      k0= vec.addr(i  ,j,k,v,n);

      d= a[i1]*b[j1]+
         a[i2]*b[j2];

      b[k0]-= d;


  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void ilus( block_t mat, block_t vec, INT_ v, INT_ n, REAL_ *a, REAL_ *b )
  {

      INT_           j,k;

//    fowrward sweep

      for( k=0;k<vec.n[2];k++ )
     {
         j= 0;
         cilusdf3( mat,vec,v,n,j,k,a,b );
          cilusf3( mat,vec,v,n,j,k,j+1,k,  3,a,b );

          cilusf3( mat,vec,v,n,j,k,j  ,k+1,1,a,b );
          cilusf3( mat,vec,v,n,j,k,j+1,k+1,0,a,b );

         for( j=1;j<vec.n[1];j++ )
        {

            cilusdf3( mat,vec,v,n,j,k,a,b );
             cilusf3( mat,vec,v,n,j,k,j+1,k,  3,a,b );

             cilusf3( mat,vec,v,n,j,k,j-1,k+1,2,a,b );
             cilusf3( mat,vec,v,n,j,k,j  ,k+1,1,a,b );
             cilusf3( mat,vec,v,n,j,k,j+1,k+1,0,a,b );

        }

         j= vec.n[1];
         cilusdf3( mat,vec,v,n,j,k,a,b );

          cilusf3( mat,vec,v,n,j,k,j-1,k+1,2,a,b );
          cilusf3( mat,vec,v,n,j,k,j  ,k+1,1,a,b );
     }

      k= vec.n[2];

      for( j=0;j<vec.n[1];j++ )
     {
         cilusdf3( mat,vec,v,n,j,k,a,b );
          cilusf3( mat,vec,v,n,j,k,j+1,k,  3,a,b );
     }

      j= vec.n[1];
      cilusdf3( mat,vec,v,n,j,k,a,b );

//... backward sweep

      cilusdb3( mat,vec,v,n,j,k,a,b );

      for( j=vec.n[1]-1;j>=0;j-- )
     {
         cilusb3( mat,vec,v,n,j,k,j+1,k,5,a,b );
        cilusdb3( mat,vec,v,n,j,k,        a,b );
     }

      for( k=vec.n[2]-1;k>=0;k-- )
     {
         j= vec.n[1];
       
          cilusb3( mat,vec,v,n,j,k,j-1,k+1,6,a,b );
          cilusb3( mat,vec,v,n,j,k,j  ,k+1,7,a,b );
       
         cilusdb3( mat,vec,v,n,j,k,          a,b );
       
         for( j=vec.n[1]-1;j>0;j-- )
        {
             cilusb3( mat,vec,v,n,j,k,j-1,k+1,6,a,b );
             cilusb3( mat,vec,v,n,j,k,j  ,k+1,7,a,b );
             cilusb3( mat,vec,v,n,j,k,j+1,k+1,8,a,b );
         
             cilusb3( mat,vec,v,n,j,k,j+1,k,  5,a,b );
            cilusdb3( mat,vec,v,n,j,k,          a,b );
       
        }
       
          j= 0;
       
          cilusb3( mat,vec,v,n,j,k,j  ,k+1,7,a,b );
          cilusb3( mat,vec,v,n,j,k,j+1,k+1,8,a,b );
       
          cilusb3( mat,vec,v,n,j,k,j+1,k,  5,a,b );
         cilusdb3( mat,vec,v,n,j,k,          a,b );

     }

      return;

  }
