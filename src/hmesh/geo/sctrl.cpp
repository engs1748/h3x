#  include <hmesh/hmesh.h>

   void hmesh_t::sctrl( INT_ k )
  {
      vsize_t< INT_,DLEN_H>  vp(vt.n,0);
      vsize_t< INT_,DLEN_H>  hp(hx.n,0);

      INT_ j;


      REAL_ sz= 1./sc[k].d;
      for( INT_ i=0;i<sc[k].b.n;i++ )
     {
         INT_ b= sc[k].b[i];
//       for( j=0;j<vt.n;j++ ){ vp[j]=0; };
         for( j=0;j<bz[b].bx.n;j++ )
        {
            INT_ l= bz[b].bx[j].id;
            if( l > -1 ){ vp[l]= 1; };
        }
     }

      for( INT_ i=0;i<sc[k].b.n;i++ )
     {
         INT_ b= sc[k].b[i];

/*       for( j=0;j<vt.n;j++ ){ vp[j]=0; };
         for( j=0;j<bz[b].bx.n;j++ )
        {
            INT_ l= bz[b].bx[j].id;
            if( l > -1 ){ vp[l]= 1; };
        }*/

         for( j=0;j<bz[b].bo.n;j++ )
        {
            INT_ l= bz[b].bo[j].h;
            INT_ g= bz[b].bo[j].q;

            if( g == 0 ){ hx[l].d[0][0]= 1./max( sz,1./hx[l].d[0][0] ); 
                          hx[l].d[2][0]= 1./max( sz,1./hx[l].d[2][0] ); 
                          hx[l].d[4][0]= 1./max( sz,1./hx[l].d[4][0] ); 
                          hx[l].d[6][0]= 1./max( sz,1./hx[l].d[6][0] );  hp[l]= 1; }

            if( g == 1 ){ hx[l].d[1][0]= 1./max( sz,1./hx[l].d[1][0] ); 
                          hx[l].d[3][0]= 1./max( sz,1./hx[l].d[3][0] ); 
                          hx[l].d[5][0]= 1./max( sz,1./hx[l].d[5][0] ); 
                          hx[l].d[7][0]= 1./max( sz,1./hx[l].d[7][0] );  hp[l]= 1; }

            if( g == 2 ){ hx[l].d[0][1]= 1./max( sz,1./hx[l].d[0][1] ); 
                          hx[l].d[1][1]= 1./max( sz,1./hx[l].d[1][1] ); 
                          hx[l].d[4][1]= 1./max( sz,1./hx[l].d[4][1] ); 
                          hx[l].d[5][1]= 1./max( sz,1./hx[l].d[5][1] );  hp[l]= 1; }

            if( g == 3 ){ hx[l].d[2][1]= 1./max( sz,1./hx[l].d[2][1] ); 
                          hx[l].d[3][1]= 1./max( sz,1./hx[l].d[3][1] ); 
                          hx[l].d[6][1]= 1./max( sz,1./hx[l].d[6][1] ); 
                          hx[l].d[7][1]= 1./max( sz,1./hx[l].d[7][1] );  hp[l]= 1; }

            if( g == 4 ){ hx[l].d[0][2]= 1./max( sz,1./hx[l].d[0][2] ); 
                          hx[l].d[1][2]= 1./max( sz,1./hx[l].d[1][2] ); 
                          hx[l].d[2][2]= 1./max( sz,1./hx[l].d[2][2] ); 
                          hx[l].d[3][2]= 1./max( sz,1./hx[l].d[3][2] );  hp[l]= 1; }

            if( g == 5 ){ hx[l].d[4][2]= 1./max( sz,1./hx[l].d[4][2] ); 
                          hx[l].d[5][2]= 1./max( sz,1./hx[l].d[5][2] ); 
                          hx[l].d[6][2]= 1./max( sz,1./hx[l].d[6][2] ); 
                          hx[l].d[7][2]= 1./max( sz,1./hx[l].d[7][2] );  hp[l]= 1; }
        }
     }

      
      for( INT_ l=0;l<hx.n;l++ )
     {
         if( hp[l]  == 0 )
        {

            INT_ i0,i1, j0,j1;
            i0= mhe[ 0][0]; i1= mhe[ 0][1]; j0= hx[l].v[i0]; j1= hx[l].v[i1]; if( (vp[j0]+vp[j1])==2 ){ hx[l].d[0][1]= 1./max(sz,1./hx[l].d[0][1]); 
                                                                                                        hx[l].d[1][1]= 1./max(sz,1./hx[l].d[1][1]); 
                                                                                                        hx[l].d[0][2]= 1./max(sz,1./hx[l].d[0][2]); 
                                                                                                        hx[l].d[1][2]= 1./max(sz,1./hx[l].d[1][2]); hp[l]= 2; }

            i0= mhe[ 1][0]; i1= mhe[ 1][1]; j0= hx[l].v[i0]; j1= hx[l].v[i1]; if( (vp[j0]+vp[j1])==2 ){ hx[l].d[1][0]= 1./max(sz,1./hx[l].d[1][0]); 
                                                                                                        hx[l].d[3][0]= 1./max(sz,1./hx[l].d[3][0]); 
                                                                                                        hx[l].d[1][2]= 1./max(sz,1./hx[l].d[1][2]); 
                                                                                                        hx[l].d[3][2]= 1./max(sz,1./hx[l].d[3][2]); hp[l]= 2; }

            i0= mhe[ 2][0]; i1= mhe[ 2][1]; j0= hx[l].v[i0]; j1= hx[l].v[i1]; if( (vp[j0]+vp[j1])==2 ){ hx[l].d[3][1]= 1./max(sz,1./hx[l].d[3][1]); 
                                                                                                        hx[l].d[2][1]= 1./max(sz,1./hx[l].d[2][1]); 
                                                                                                        hx[l].d[3][2]= 1./max(sz,1./hx[l].d[3][2]); 
                                                                                                        hx[l].d[2][2]= 1./max(sz,1./hx[l].d[2][2]);hp[l]= 2;  }

            i0= mhe[ 3][0]; i1= mhe[ 3][1]; j0= hx[l].v[i0]; j1= hx[l].v[i1]; if( (vp[j0]+vp[j1])==2 ){ hx[l].d[2][0]= 1./max(sz,1./hx[l].d[2][0]); 
                                                                                                        hx[l].d[0][0]= 1./max(sz,1./hx[l].d[0][0]); 
                                                                                                        hx[l].d[2][2]= 1./max(sz,1./hx[l].d[2][2]); 
                                                                                                        hx[l].d[0][2]= 1./max(sz,1./hx[l].d[0][2]); hp[l]= 2; }

            i0= mhe[ 4][0]; i1= mhe[ 4][1]; j0= hx[l].v[i0]; j1= hx[l].v[i1]; if( (vp[j0]+vp[j1])==2 ){ hx[l].d[4][1]= 1./max(sz,1./hx[l].d[4][1]); 
                                                                                                        hx[l].d[5][1]= 1./max(sz,1./hx[l].d[5][1]); 
                                                                                                        hx[l].d[4][2]= 1./max(sz,1./hx[l].d[4][2]); 
                                                                                                        hx[l].d[5][2]= 1./max(sz,1./hx[l].d[5][2]); hp[l]= 2; }

            i0= mhe[ 5][0]; i1= mhe[ 5][1]; j0= hx[l].v[i0]; j1= hx[l].v[i1]; if( (vp[j0]+vp[j1])==2 ){ hx[l].d[5][0]= 1./max(sz,1./hx[l].d[5][0]); 
                                                                                                        hx[l].d[7][0]= 1./max(sz,1./hx[l].d[7][0]); 
                                                                                                        hx[l].d[5][2]= 1./max(sz,1./hx[l].d[5][2]); 
                                                                                                        hx[l].d[7][2]= 1./max(sz,1./hx[l].d[7][2]); hp[l]= 2; }

            i0= mhe[ 6][0]; i1= mhe[ 6][1]; j0= hx[l].v[i0]; j1= hx[l].v[i1]; if( (vp[j0]+vp[j1])==2 ){ hx[l].d[7][1]= 1./max(sz,1./hx[l].d[7][1]); 
                                                                                                        hx[l].d[6][1]= 1./max(sz,1./hx[l].d[6][1]); 
                                                                                                        hx[l].d[7][2]= 1./max(sz,1./hx[l].d[7][2]); 
                                                                                                        hx[l].d[6][2]= 1./max(sz,1./hx[l].d[6][2]); hp[l]= 2; }

            i0= mhe[ 7][0]; i1= mhe[ 7][1]; j0= hx[l].v[i0]; j1= hx[l].v[i1]; if( (vp[j0]+vp[j1])==2 ){ hx[l].d[6][0]= 1./max(sz,1./hx[l].d[6][0]); 
                                                                                                        hx[l].d[4][0]= 1./max(sz,1./hx[l].d[4][0]); 
                                                                                                        hx[l].d[6][2]= 1./max(sz,1./hx[l].d[6][2]); 
                                                                                                        hx[l].d[4][2]= 1./max(sz,1./hx[l].d[4][2]); hp[l]= 2; }

            i0= mhe[ 8][0]; i1= mhe[ 8][1]; j0= hx[l].v[i0]; j1= hx[l].v[i1]; if( (vp[j0]+vp[j1])==2 ){ hx[l].d[0][0]= 1./max(sz,1./hx[l].d[0][0]); 
                                                                                                        hx[l].d[4][0]= 1./max(sz,1./hx[l].d[4][0]); 
                                                                                                        hx[l].d[0][1]= 1./max(sz,1./hx[l].d[0][1]); 
                                                                                                        hx[l].d[4][1]= 1./max(sz,1./hx[l].d[4][1]); hp[l]= 2; }

            i0= mhe[ 9][0]; i1= mhe[ 9][1]; j0= hx[l].v[i0]; j1= hx[l].v[i1]; if( (vp[j0]+vp[j1])==2 ){ hx[l].d[1][0]= 1./max(sz,1./hx[l].d[1][0]); 
                                                                                                        hx[l].d[5][0]= 1./max(sz,1./hx[l].d[5][0]); 
                                                                                                        hx[l].d[1][1]= 1./max(sz,1./hx[l].d[1][1]); 
                                                                                                        hx[l].d[5][1]= 1./max(sz,1./hx[l].d[5][1]); hp[l]= 2; }

            i0= mhe[10][0]; i1= mhe[10][1]; j0= hx[l].v[i0]; j1= hx[l].v[i1]; if( (vp[j0]+vp[j1])==2 ){ hx[l].d[3][0]= 1./max(sz,1./hx[l].d[3][0]); 
                                                                                                        hx[l].d[7][0]= 1./max(sz,1./hx[l].d[7][0]); 
                                                                                                        hx[l].d[3][1]= 1./max(sz,1./hx[l].d[3][1]); 
                                                                                                        hx[l].d[7][1]= 1./max(sz,1./hx[l].d[7][1]); hp[l]= 2; }

            i0= mhe[11][0]; i1= mhe[11][1]; j0= hx[l].v[i0]; j1= hx[l].v[i1]; if( (vp[j0]+vp[j1])==2 ){ hx[l].d[2][0]= 1./max(sz,1./hx[l].d[2][0]); 
                                                                                                        hx[l].d[6][0]= 1./max(sz,1./hx[l].d[6][0]); 
                                                                                                        hx[l].d[2][1]= 1./max(sz,1./hx[l].d[2][1]); 
                                                                                                        hx[l].d[6][1]= 1./max(sz,1./hx[l].d[6][1]); hp[l]= 2; }
        }
     }

/*    for( INT_ l=0;l<hx.n;l++ )
     {
         if( hp[l]  < 1 )
        {

            INT_ j0;
            j0= hx[l].v[0]; if( vp[j0] == 1 ){ hx[l].d[0][0]= 1./max(sz,1./hx[l].d[0][0]); 
                                               hx[l].d[0][1]= 1./max(sz,1./hx[l].d[0][1]); 
                                               hx[l].d[0][2]= 1./max(sz,1./hx[l].d[0][2]); hp[l]= 3; }

            j0= hx[l].v[1]; if( vp[j0] == 1 ){ hx[l].d[1][0]= 1./max(sz,1./hx[l].d[1][0]); 
                                               hx[l].d[1][1]= 1./max(sz,1./hx[l].d[1][1]); 
                                               hx[l].d[1][2]= 1./max(sz,1./hx[l].d[1][2]); hp[l]= 3; }

            j0= hx[l].v[2]; if( vp[j0] == 1 ){ hx[l].d[2][0]= 1./max(sz,1./hx[l].d[2][0]); 
                                               hx[l].d[2][1]= 1./max(sz,1./hx[l].d[2][1]); 
                                               hx[l].d[2][2]= 1./max(sz,1./hx[l].d[2][2]); hp[l]= 3; }

            j0= hx[l].v[3]; if( vp[j0] == 1 ){ hx[l].d[3][0]= 1./max(sz,1./hx[l].d[3][0]); 
                                               hx[l].d[3][1]= 1./max(sz,1./hx[l].d[3][1]); 
                                               hx[l].d[3][2]= 1./max(sz,1./hx[l].d[3][2]); hp[l]= 3; }

            j0= hx[l].v[4]; if( vp[j0] == 1 ){ hx[l].d[4][0]= 1./max(sz,1./hx[l].d[4][0]); 
                                               hx[l].d[4][1]= 1./max(sz,1./hx[l].d[4][1]); 
                                               hx[l].d[4][2]= 1./max(sz,1./hx[l].d[4][2]); hp[l]= 3; }

            j0= hx[l].v[5]; if( vp[j0] == 1 ){ hx[l].d[5][0]= 1./max(sz,1./hx[l].d[5][0]); 
                                               hx[l].d[5][1]= 1./max(sz,1./hx[l].d[5][1]); 
                                               hx[l].d[5][2]= 1./max(sz,1./hx[l].d[5][2]); hp[l]= 3; }

            j0= hx[l].v[6]; if( vp[j0] == 1 ){ hx[l].d[6][0]= 1./max(sz,1./hx[l].d[6][0]); 
                                               hx[l].d[6][1]= 1./max(sz,1./hx[l].d[6][1]); 
                                               hx[l].d[6][2]= 1./max(sz,1./hx[l].d[6][2]); hp[l]= 3; }

            j0= hx[l].v[7]; if( vp[j0] == 1 ){ hx[l].d[7][0]= 1./max(sz,1./hx[l].d[7][0]); 
                                               hx[l].d[7][1]= 1./max(sz,1./hx[l].d[7][1]); 
                                               hx[l].d[7][2]= 1./max(sz,1./hx[l].d[7][2]); hp[l]= 3; }

        }
     }*/
      return;
  }
