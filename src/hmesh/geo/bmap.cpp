#  include <hmesh/hmesh.h>

   extern "C" void dsyev_( char *, char *, INT_ *, REAL_ *, INT_ *, REAL_ *, REAL_ *, INT_ *, INT_ * );

   void hmesh_t::bmap( INT_ b, bool flag )
  {
      INT_ j,k;
      INT_ s;
 
      REAL_                      w;
      REAL_3                     f;

      bvtx_t                   dum;
      INT_                       p;


      assert( b > -1 );
      assert( b < nb );

      VINT_                   list(bz[b].bx.n,-1);

      if(  bz[b].bo.n == 0 ){ return; };

      s= bz[b].bo[0].s;
      p= bz[b].bo[0].p;

      if( s == -1 || p == -1 ){ return; }

      bz[b].x0[0]=0;
      bz[b].x0[1]=0;
      bz[b].x0[2]=0;

      bz[b].z[0]=0;
      bz[b].z[1]=0;
      bz[b].z[2]=0;
      bz[b].z[3]=0;
      bz[b].z[4]=0;
      bz[b].z[5]=0;
      bz[b].z[6]=0;
      bz[b].z[7]=0;
      bz[b].z[8]=0;
 
      bz[b].m= flag;


      w= 0;
      for( j=0;j<bz[b].bx.n;j++ )
     {
         k= bz[b].bx[j].id;
         if( k > -1 )
        {
            w++;

            bz[b].x0[0]+= vx[k][0];
            bz[b].x0[1]+= vx[k][1];
            bz[b].x0[2]+= vx[k][2];
        }
     }
      assert( w > 0 );
      w= 1./w;

      bz[b].x0[0]*= w;
      bz[b].x0[1]*= w;
      bz[b].x0[2]*= w;

      for( j=0;j<bz[b].bx.n;j++ )
     {
         k= bz[b].bx[j].id;
         if( k > -1 )
        {

            f[0]= vx[k][0]- bz[b].x0[0];
            f[1]= vx[k][1]- bz[b].x0[1];
            f[2]= vx[k][2]- bz[b].x0[2];

            bz[b].z[0]+= f[0]*f[0];
            bz[b].z[1]+= f[1]*f[0];
            bz[b].z[2]+= f[2]*f[0];

            bz[b].z[4]+= f[1]*f[1];
            bz[b].z[5]+= f[2]*f[1];

            bz[b].z[8]+= f[2]*f[2];
        }
     }
      bz[b].z[0]*= w;
      bz[b].z[1]*= w;
      bz[b].z[2]*= w;
      bz[b].z[4]*= w;
      bz[b].z[5]*= w;
      bz[b].z[8]*= w;

     {
         char jobz='v';
         char uplo='l';
         INT_ n=3;
         INT_ lda=3;
         INT_ lwork=8;
         INT_ info;
         REAL_                  work[8];
         dsyev_( &jobz,&uplo, &n,bz[b].z,&lda, bz[b].d,work, &lwork,&info );
     }

      bz[b].d[0]= sqrt( fmax(bz[b].d[0],1.e-16) );
      bz[b].d[1]= sqrt( fmax(bz[b].d[1],1.e-16) );
      bz[b].d[2]= sqrt( fmax(bz[b].d[2],1.e-16) );

      return;     
  }

   void hmesh_t::smap( vsurf_t &su )
  {
      aprj_t                  stat;
      INT_ ist,ien;

      INT_                     ref[128];
      REAL_                    dst[128];
      REAL_3                   f,g;

      VINT_                    list(vx.n,-1);
      bvtx_t                   dum;
      vtx_t                    v,w;
      vtx_t                    d;
      INT_                     i,j,k,h,b,s,p;

      vsize_t<vtx_t,8>         tmp(vx.n,xdum);

/*    REAL_   q0[9];
      REAL_2  q1[9];
      REAL_3  q2[9];

      REAL_   q01[9];
      REAL_2  q11[9];
      REAL_3  q21[9]; */

      for( b=0;b<nb;b++ )
     {
         if( bz[b].bo.n > 0 )
        {
            s= bz[b].bo[0].s;
            p= bz[b].bo[0].p;

            if( s > -1 && p > -1 )
           {


               INT_ i0,i1,i2;
               i0=0;
               i1=3;
               i2=6;

               REAL_ c0,c1,c2;

               c0= dot3( bz[b].z+6 ,su[s]->p[p].z+i0 );
               c1= dot3( bz[b].z+6 ,su[s]->p[p].z+i1 );
               c2= dot3( bz[b].z+6 ,su[s]->p[p].z+i2 );

               if( fabs(c0) > fabs(c1) ) { swap(c0,c1);swap(i0,i1); }
               if( fabs(c1) > fabs(c2) ) 
              {   
                  swap(c1,c2);swap(i1,i2);
                  if( fabs(c0)>fabs(c1))
                 {
                     swap(c0,c1);swap(i0,i1);
                 }
              }   

               c0= dot3( bz[b].z+3 ,su[s]->p[p].z+i0 );
               c1= dot3( bz[b].z+3 ,su[s]->p[p].z+i1 );
               if( fabs(c1)<fabs(c0) ){ swap(c1,c0); swap(i1,i0);};

               c0= dot3( bz[b].z+0 ,su[s]->p[p].z+i0 );

               if( c0 < 0 ){ c0=-1; }else{ c0=1; };
               if( c1 < 0 ){ c1=-1; }else{ c1=1; };
               if( c2 < 0 ){ c2=-1; }else{ c2=1; };

               for( j=0;j<bz[b].bx.n;j++ )
              {
                  k= bz[b].bx[j].id;
                  if( k > -1 )
                 {

                     f[0]= vx[k][0];
                     f[1]= vx[k][1];
                     f[2]= vx[k][2];

                     if( bz[b].m )
                    {
                        f[0]-= bz[b].x0[0];
                        f[1]-= bz[b].x0[1];
                        f[2]-= bz[b].x0[2];

                        g[0]= bz[b].z[0+0]*f[0]+ bz[b].z[0+1]*f[1]+ bz[b].z[0+2]*f[2];
                        g[1]= bz[b].z[3+0]*f[0]+ bz[b].z[3+1]*f[1]+ bz[b].z[3+2]*f[2];
                        g[2]= bz[b].z[6+0]*f[0]+ bz[b].z[6+1]*f[1]+ bz[b].z[6+2]*f[2];

                        g[0]*= c0;
                        g[1]*= c1;
                        g[2]*= c2;

                        g[0]/= bz[b].d[0];
                        g[1]/= bz[b].d[1];
                        g[2]/= bz[b].d[2];

                        g[0]*= su[s]->p[p].d[0];
                        g[1]*= su[s]->p[p].d[1];
                        g[2]*= su[s]->p[p].d[2];

                        f[0]= su[s]->p[p].z[i0+0]*g[0]+ su[s]->p[p].z[i1+0]*g[1]+ su[s]->p[p].z[i2+0]*g[2]; 
                        f[1]= su[s]->p[p].z[i0+1]*g[0]+ su[s]->p[p].z[i1+1]*g[1]+ su[s]->p[p].z[i2+1]*g[2]; 
                        f[2]= su[s]->p[p].z[i0+2]*g[0]+ su[s]->p[p].z[i1+2]*g[1]+ su[s]->p[p].z[i2+2]*g[2]; 

                        f[0]+= su[s]->p[p].x0[0]; 
                        f[1]+= su[s]->p[p].x0[1]; 
                        f[2]+= su[s]->p[p].x0[2]; 
                    }
 
                     w= vtx_t(f); 
                     if( bz[b].n )
                    {
                        su[s]->aprj( w, bz[b].bx[j].y, p,stat );
                    }
                     else
                    {
                        su[s]->srch( w, bz[b].bx[j].y );
                    }

                     w= vtx_t(f);
                     su[s]->prj( w, bz[b].bx[j].y,   stat );

                     tmp[k]= stat.x;
                     stat.dbg=false;
                     list[k]=0;

                 }
              }
               bz[b].m= false;
           }
        }
     }

//    printf( "\n" );
//    printf( "\n" );
//    printf( "\n" );

      printf( "%d\n", vz.n );

      ien= 0;
      for( INT_ z=0;z<vz.n;z++ )
     {
         ist= ien;
         ien= vz[z].v;
         if( vz[z].b.n > 0 )
        {
            for( i=ist;i<ien;i++ )
           {
               REAL_ e,f;
               if( list[i]==-1 )
              {
                  for( INT_ g=0;g<vz[z].b.n;g++ )
                 {
                     b= vz[z].b[g];
                     h=-1;
                     f=BIG;
                     for( j=0;j<bz[b].bx.n;j++ )
                    {
                        k= bz[b].bx[j].id;
                        if( k > -1 )
                       {
                           d= vx[i]- vx[k];
                           e= d*d;
                           if( e < f )
                          {
                              f= e;
                              h= k;
                          }
                       }
                    }
                     ref[g]= h;
                     dst[g]= 1./sqrt(f);
                 }
                  e=0;
                  w=vtx_t(0,0,0);
                  for( INT_ g=0;g<vz[z].b.n;g++ )
                 { 
                     b= vz[z].b[g];
                     j= ref[g];
                     if( j> -1 )
                    {
                        f= dst[g];
                        v= f* vx[j];
                        w+= v;
                        e+= f;
                    }
                 }
                  e= 1./e;
                  w*= e;
//                printf( "% 12.5e % 12.5e % 12.5e % 12.5e % 12.5e % 12.5e\n", vx[i][0], vx[i][1], vx[i][2], w[0],w[1],w[2] );
                  vx[i][0]= w[0];
                  vx[i][1]= w[1];
                  vx[i][2]= w[2];

              }
               else
              {
                 vx[i]= tmp[i];
              }
              
           }
        }
     }

      return;     

 
  }

   void hmesh_t::geo( INT_ g, INT_ s, INT_ p, INT_ b, vsurf_t &su )
  {

      if( p > -1 )
     {
         for( INT_ j=0;j<bz[g].bo.n;j++ )
        {
            bz[g].bo[j].s= s;
            bz[g].bo[j].p= p;
            bz[g].bo[j].b= b;
        }
     }
      else
     {
         vtx_t w=vtx_t(0.,0.,0.);
         vtx_t u,v;

         for( INT_ j=0;j<bz[g].bo.n;j++ )
        {
/*          INT_ h= bz[g].bo[j].h;
            INT_ q= bz[g].bo[j].q;*/

            INT_ i0= bz[g].bo[j].v[0]; i0= bz[g].bx[i0].id;
            INT_ i1= bz[g].bo[j].v[1]; i1= bz[g].bx[i1].id;
            INT_ i2= bz[g].bo[j].v[2]; i2= bz[g].bx[i2].id;
            INT_ i3= bz[g].bo[j].v[3]; i3= bz[g].bx[i3].id;

            u= vx[i3]- vx[i1]; 
            v= vx[i2]- vx[i0]; 
// doubts here
            w+= cross( u,v );
        }
         w/= l2norm(w);

//       printf( "%2d % 9.3e % 9.3e % 9.3e\n", g, w[0],w[1],w[2] );

         REAL_ cmin= 2;
         INT_  jmin=-1;
         for( INT_ j=0;j<su[s]->p.n;j++ )
        {
            vtx_t x,d;
            su[s]->pnorm( j, x,d );
            REAL_ c= d*w;
            if( c < cmin )
           {
               cmin= c;
               jmin= j;
           }
//          printf( "%2d % 9.3e % 9.3e % 9.3e % 9.3e % 9.3e % 9.3e\n", j, x[0],x[1],x[2], d[0],d[1],d[2] );
        }
         for( INT_ j=0;j<bz[g].bo.n;j++ )
        {
            bz[g].bo[j].s= s;
            bz[g].bo[j].p= jmin;
            bz[g].bo[j].b= b;
        }
     }
      return;
  }
