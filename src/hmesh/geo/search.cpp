#include<hmesh/hmesh.h>

   INT_ hmesh_t::search(INT_ g, vtx_t p)
  {
      INT_ i;
      REAL_ d, dmin;
      INT_  val;

      dmin=99999.;
      val=-1;
      
      for(i=0; i<bz[g].bo.n; i++)
     {
          INT_ s0=bz[g].bo[i].v[0];
          INT_ s1=bz[g].bo[i].v[1];
          INT_ s2=bz[g].bo[i].v[2];
          INT_ s3=bz[g].bo[i].v[3];

          INT_ x0=bz[g].bx[s0].id;
          INT_ x1=bz[g].bx[s1].id;
          INT_ x2=bz[g].bx[s2].id;
          INT_ x3=bz[g].bx[s3].id;
    
          vtx_t q= vx[x0]+vx[x1]+vx[x2]+vx[x3]; 
          q*= 0.25;

/*       printf("\n");
         printf("% 9.3e % 9.3e % 9.3e\n", vx[x0][0], vx[x0][1],vx[x0][2]);
         printf("% 9.3e % 9.3e % 9.3e\n", vx[x1][0], vx[x1][1],vx[x1][2]);
         printf("% 9.3e % 9.3e % 9.3e\n", vx[x2][0], vx[x2][1],vx[x2][2]);
         printf("% 9.3e % 9.3e % 9.3e\n", vx[x3][0], vx[x3][1],vx[x3][2]);
         printf("% 9.3e % 9.3e % 9.3e\n", vx[x0][0], vx[x0][1],vx[x0][2]);
         printf("% 9.3e % 9.3e % 9.3e\n", q[0], q[1],q[2]);*/
          q-= p;

          d= fabs(q[0])+fabs(q[1])+ fabs(q[2]);

          if(d<dmin)
         {          
             dmin=d; val=i;
         }
//       printf("\n");
      }
          return val;


  }
