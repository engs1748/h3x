
#  include <hmesh/hmesh.h>

#  define IJK(i,j,k) (i)+l0*((j)+m0*(k))
#  define  JK(j,k)   (j)+m0*(k) 
#  define  IK(i,k)   (i)+l0*(k)
#  define  IJ(i,j)   (i)+l0*(j)

   void hmesh_t::ngref( INT_ i, INT_ j, hqref_t &v )
  {
      INT_ h,k,q;
//    k= indx[i];
      k= i;
      hx[ k ].neig( qv.data(), j, h,q );
      if( h > -1 )
     {
         v.b= false;
         v.v[0]= hx[h].id;
         v.v[1]= q;
         v.v[2]=-1;
         v.v[3]=-1;
         v.v[4]=-1;
         v.v[5]=-1;
     }
      else
     {
         v.b= true;
         h= hx[k].q[j].g;
         q= hx[k].q[j].b;
         v.v[0]= h;
         v.v[1]= bz[h].bo[q].s;
         v.v[2]= bz[h].bo[q].p;
         v.v[3]= bz[h].bo[q].b;
         v.v[4]= hx[k].id;
         v.v[5]= j;
     }            
      return;
  }

   void hmesh_t::cvty( INT_ l0, INT_ m0, INT_ n0, dir_t d0, dir_t d1, INT_ kst, cavty_t &var, bool flag )
  {
      href_t hdum;

      INT_                            i,j,k;
      INT_                            l,m,n;
//    INT_                            n1,n2;
      vsize_t<href_t,DLEN_H>          v(l0*m0*n0,hdum );

//    INT_2                           k0[4], k1[4], k2[4], k3[4], k5[4];
//    INT_4                           k4[4];

      vtx_t                           z;
      vtx_t                           s;

//    REAL_                           d;
//    REAL_                           f;

      VINT_                           mark(hx.n,-1);
      INT_                            mask[6]= {-1,-1,-1,-1,-1,-1};

      m= 0;

// first cell: bottom lower left corner

      v[m].f[0]= d1;
      v[m].f[1]= vec( d0,d1 );
      v[m].f[2]= d0;
      v[m].h=    indx[kst];
      m++;
      n=  0;

// build first line of bottom plane

      for( i=0;i<l0-1;i++ )
     {
         v[m].f[0]=   v[n+i].f[0];
         v[m].f[1]=   v[n+i].f[1];
         v[m].f[2]=   v[n+i].f[2];
         v[m].h=      v[n+i].h;
         walk( v[m].h,v[m].f, mark );
         m++;
     }

// build bottom plane

      for( j=1;j<m0;j++ )
     {
         v[m].f[0]=   v[n].f[2];
         v[m].f[1]=   v[n].f[1];
         v[m].f[2]=   v[n].f[0];
         v[m].h=      v[n].h;
         walk( v[m].h,v[m].f, mark );
         swap( v[m].f[0],v[m].f[2] );
         m++;
         n+= l0;
         for( i=0;i<l0-1;i++ )
        {
            v[m].f[0]= v[n+i].f[0];
            v[m].f[1]= v[n+i].f[1];
            v[m].f[2]= v[n+i].f[2];
            v[m].h=    v[n+i].h;
            walk( v[m].h,v[m].f, mark );
            m++;
        }
     }

// now build all other planes
      l= 0;

      for( k=1;k<n0;k++ )
     {

// first cell of plane k

         v[m].f[0]= v[l].f[0];
         v[m].f[1]= v[l].f[2];
         v[m].f[2]= v[l].f[1];
         v[m].h=    v[l].h;
         walk( v[m].h,v[m].f, mark );
         swap( v[m].f[1],v[m].f[2] );
         m++;
         n+= l0;
         l+= (l0*m0);

// first line of plane k

         for( i=0;i<l0-1;i++ )
        {
            v[m].f[0]=   v[n+i].f[0];
            v[m].f[1]=   v[n+i].f[1];
            v[m].f[2]=   v[n+i].f[2];
            v[m].h=      v[n+i].h;
            walk( v[m].h,v[m].f, mark );
            m++;
        }

// plane k

         for( j=1;j<m0;j++ )
        {
            v[m].f[0]=   v[n].f[2];
            v[m].f[1]=   v[n].f[1];
            v[m].f[2]=   v[n].f[0];
            v[m].h=      v[n].h;
            walk( v[m].h,v[m].f, mark );
            swap( v[m].f[0],v[m].f[2] );
            m++;
            n+= l0;
            for( i=0;i<l0-1;i++ )
           {
               v[m].f[0]= v[n+i].f[0];
               v[m].f[1]= v[n+i].f[1];
               v[m].f[2]= v[n+i].f[2];
               v[m].h=    v[n+i].h;
               walk( v[m].h,v[m].f, mark );
               m++;
           }
        }
     }

// collect vertex list - first eight vertices are the corner vertices.
      var.off[0]= 8;
      var.off[1]= var.off[0]+ 4*( l0-1 );
      var.off[2]= var.off[1]+ 4*( m0-1 );
      var.off[3]= var.off[2]+ 4*( n0-1 );

      var.off[4]= var.off[3]+ 2*( m0-1 )*( n0-1 ); 
      var.off[5]= var.off[4]+ 2*( l0-1 )*( n0-1 ); 
      var.off[6]= var.off[5]+ 2*( l0-1 )*( m0-1 ); 

      var.l= l0;
      var.m= m0;
      var.n= n0;

      var.v.n= 8+ 4*(l0-1)+ 4*(m0-1)+ 4*(n0-1)+ 2*(l0-1)*(m0-1)+ 2*(l0-1)*(n0-1)+ 2*(m0-1)*(n0-1);
      assert( var.v.n == var.off[6] );
      var.v.resize(-1);

      m=IJK(   0,   0,   0); var.v[ 0]= hx[ v[m].h ].v[ coor( !(v[m].f[2]),!(v[m].f[0]),!(v[m].f[1]) ) ];
      m=IJK(l0-1,   0,   0); var.v[ 1]= hx[ v[m].h ].v[ coor(  (v[m].f[2]),!(v[m].f[0]),!(v[m].f[1]) ) ]; 
      m=IJK(   0,m0-1,   0); var.v[ 2]= hx[ v[m].h ].v[ coor( !(v[m].f[2]), (v[m].f[0]),!(v[m].f[1]) ) ]; 
      m=IJK(l0-1,m0-1,   0); var.v[ 3]= hx[ v[m].h ].v[ coor(  (v[m].f[2]), (v[m].f[0]),!(v[m].f[1]) ) ];
      m=IJK(   0,   0,n0-1); var.v[ 4]= hx[ v[m].h ].v[ coor( !(v[m].f[2]),!(v[m].f[0]), (v[m].f[1]) ) ];
      m=IJK(l0-1,   0,n0-1); var.v[ 5]= hx[ v[m].h ].v[ coor(  (v[m].f[2]),!(v[m].f[0]), (v[m].f[1]) ) ];
      m=IJK(   0,m0-1,n0-1); var.v[ 6]= hx[ v[m].h ].v[ coor( !(v[m].f[2]), (v[m].f[0]), (v[m].f[1]) ) ]; 
      m=IJK(l0-1,m0-1,n0-1); var.v[ 7]= hx[ v[m].h ].v[ coor(  (v[m].f[2]), (v[m].f[0]), (v[m].f[1]) ) ]; 

// edges

      l= 8;
      for( i=1;i<l0;i++ )
     {
         m= IJK(   i,   0,   0); var.v[l++]= hx[ v[m].h ].v[ coor( !(v[m].f[2]),!(v[m].f[0]),!(v[m].f[1]) ) ]; 
         m= IJK(   i,m0-1,   0); var.v[l++]= hx[ v[m].h ].v[ coor( !(v[m].f[2]), (v[m].f[0]),!(v[m].f[1]) ) ]; 
         m= IJK(   i,   0,n0-1); var.v[l++]= hx[ v[m].h ].v[ coor( !(v[m].f[2]),!(v[m].f[0]), (v[m].f[1]) ) ]; 
         m= IJK(   i,m0-1,n0-1); var.v[l++]= hx[ v[m].h ].v[ coor( !(v[m].f[2]), (v[m].f[0]), (v[m].f[1]) ) ]; 
     }

      for( j=1;j<m0;j++ )
     {
         m= IJK(   0,   j,   0); var.v[l++]= hx[ v[m].h ].v[ coor( !(v[m].f[2]),!(v[m].f[0]),!(v[m].f[1]) ) ]; 
         m= IJK(l0-1,   j,   0); var.v[l++]= hx[ v[m].h ].v[ coor(  (v[m].f[2]),!(v[m].f[0]),!(v[m].f[1]) ) ]; 
         m= IJK(   0,   j,n0-1); var.v[l++]= hx[ v[m].h ].v[ coor( !(v[m].f[2]),!(v[m].f[0]), (v[m].f[1]) ) ]; 
         m= IJK(l0-1,   j,n0-1); var.v[l++]= hx[ v[m].h ].v[ coor(  (v[m].f[2]),!(v[m].f[0]), (v[m].f[1]) ) ]; 
     }

      for( k=1;k<n0;k++ )
     {
         m= IJK(   0,   0,   k); var.v[l++]= hx[ v[m].h ].v[ coor( !(v[m].f[2]),!(v[m].f[0]),!(v[m].f[1]) ) ]; 
         m= IJK(l0-1,   0,   k); var.v[l++]= hx[ v[m].h ].v[ coor(  (v[m].f[2]),!(v[m].f[0]),!(v[m].f[1]) ) ]; 
         m= IJK(   0,m0-1,   k); var.v[l++]= hx[ v[m].h ].v[ coor( !(v[m].f[2]), (v[m].f[0]),!(v[m].f[1]) ) ]; 
         m= IJK(l0-1,m0-1,   k); var.v[l++]= hx[ v[m].h ].v[ coor(  (v[m].f[2]), (v[m].f[0]),!(v[m].f[1]) ) ]; 
     }

// faces

      for( k=1;k<n0;k++ )
     {
         for( j=1;j<m0;j++ )
        {
            m= IJK(   0,   j,   k); var.v[l++]= hx[ v[m].h ].v[ coor( !(v[m].f[2]),!(v[m].f[0]),!(v[m].f[1]) ) ]; 
            m= IJK(l0-1,   j,   k); var.v[l++]= hx[ v[m].h ].v[ coor(  (v[m].f[2]),!(v[m].f[0]),!(v[m].f[1]) ) ]; 
        }
     }
      for( k=1;k<n0;k++ )
     {
         for( i=1;i<l0;i++ )
        {
            m= IJK(   i,   0,   k); var.v[l++]= hx[ v[m].h ].v[ coor( !(v[m].f[2]),!(v[m].f[0]),!(v[m].f[1]) ) ]; 
            m= IJK(   i,m0-1,   k); var.v[l++]= hx[ v[m].h ].v[ coor( !(v[m].f[2]), (v[m].f[0]),!(v[m].f[1]) ) ]; 
        }
     }
      for( j=1;j<m0;j++ )
     {
         for( i=1;i<l0;i++ )
        {
            m= IJK(   i,   j,   0); var.v[l++]= hx[ v[m].h ].v[ coor( !(v[m].f[2]),!(v[m].f[0]),!(v[m].f[1]) ) ]; 
            m= IJK(   i,   j,n0-1); var.v[l++]= hx[ v[m].h ].v[ coor( !(v[m].f[2]),!(v[m].f[0]), (v[m].f[1]) ) ]; 
        }
     }

// Hex and boundary face records

      var.q0.n= JK(m0,n0)+1; var.q0.resize(hqdum);
      var.q1.n= JK(m0,n0)+1; var.q1.resize(hqdum);

      var.q2.n= IK(l0,n0)+1; var.q2.resize(hqdum);
      var.q3.n= IK(l0,n0)+1; var.q3.resize(hqdum);

      var.q4.n= IJ(l0,m0)+1; var.q4.resize(hqdum);
      var.q5.n= IJ(l0,m0)+1; var.q5.resize(hqdum);

      l=0;
      for( k=0;k<n0;k++ )
     {
         for( j=0;j<m0;j++ )
        {
            m= IJK(   0,   j,   k ); ngref( v[m].h, face( !(v[m].f[2]) ), var.q0[l] );
            m= IJK(l0-1,   j,   k ); ngref( v[m].h, face(  (v[m].f[2]) ), var.q1[l] );
            l++;
        }
     }
      l=0;
      for( k=0;k<n0;k++ )
     {
         for( i=0;i<l0;i++ )
        {
            m= IJK(   i,   0,   k ); ngref( v[m].h, face( !(v[m].f[0]) ), var.q2[l] );
            m= IJK(   i,m0-1,   k ); ngref( v[m].h, face(  (v[m].f[0]) ), var.q3[l] );
            l++;
        }
     }
      l=0;
      for( j=0;j<m0;j++ )
     {
         for( i=0;i<l0;i++ )
        {
            m= IJK(   i,   j,   0 ); ngref( v[m].h, face( !(v[m].f[1]) ), var.q4[l] );
            m= IJK(   i,   j,n0-1 ); ngref( v[m].h, face(  (v[m].f[1]) ), var.q5[l] );
            l++;
        }
     }

      for( i=0;i<l0*n0*m0;i++ )
     {
         j= v[i].h;
         j= hx[j].id;
         v[i].id= j;
     }

      if( flag )
     {

         for( i=0;i<l0*n0*m0;i++ )
        {
            del( v[i].id, mask );
        }

// now build reference to neighboring hex

         for( i=0;i<m0*n0;i++ )
        {
            if( !var.q0[i].b ){ detach1( indx[var.q0[i].v[0]],var.q0[i].v[1] ); }
            if( !var.q1[i].b ){ detach1( indx[var.q1[i].v[0]],var.q1[i].v[1] ); }
        }

         for( i=0;i<l0*n0;i++ )
        {
            if( !var.q2[i].b ){ detach1( indx[var.q2[i].v[0]],var.q2[i].v[1] ); }
            if( !var.q3[i].b ){ detach1( indx[var.q3[i].v[0]],var.q3[i].v[1] ); }
        }

         for( i=0;i<l0*m0;i++ )
        {
            if( !var.q4[i].b ){ detach1( indx[var.q4[i].v[0]],var.q4[i].v[1] ); }
            if( !var.q5[i].b ){ detach1( indx[var.q5[i].v[0]],var.q5[i].v[1] ); }
        }
     }



      return;
      
  }
