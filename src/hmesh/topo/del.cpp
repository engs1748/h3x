
#  include <hmesh/hmesh.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void hmesh_t::del( INT_ id, INT_ *mask )
  {

      INT_ i,j,k,l,g,n;

      i= indx[id];
   
      indx[id]= -1;

      assert( i > -1 );

      del0( i,mask );

      n= hx.n;
      n--;

// replace with last hex in the mesh

      hx[i]= hx[n];

      for( j=0;j<6;j++ )
     {
         k= hx[i].q[j].q;
         l= hx[i].q[j].o;

         if( k > -1 )
        {
            qv[k].h[l]= i;
          
            g= hx[i].q[j].g;
            if( g > -1 )
           {
               l= hx[i].q[j].b;
               bz[g].bo[l].h= i;
           }
        }
     }

      for( j=0;j<8;j++ )
     {
         k= hx[i].v[j];
//       if( k > -1 )
        {
            if( vt[k].h == n )
           {
               assert( vt[k].k == j );
               vt[k].h= i; 
           }
        }
     }

      j= hx[i].id;
      indx[j]= i;

      hx.n--;

      trimb();
      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void hmesh_t::del0( INT_ i, INT_ *mask )
  {

      INT_       j,h,k,l,m, a,b;
      bool       o;

      VINT_2          list;
      VINT_2          dum;

      if( hx.n <= 0 ){ return; }

      assert( i > -1  );
      assert( i < hx.n );

// detach element from its vertices

      for( j=0;j<8;j++ )
     {
         k= hx[i].v[j];
         if( i == vt[k].h )
        {
            assert( vt[k].k == j );

            point( k, i,j, list,dum );

            if( list.n > 1 )
           {
               a= list[1][0];
               b= list[1][1];
               assert( hx[a].v[b] == k );
               vt[k].h= list[1][0];
               vt[k].k= list[1][1];
           }
            else
           {
               vt[k].h= -1;
               vt[k].k= -1;
           }
        }
     }

// detach element from its faces
      INT_2 q[6]={ {-1,-1}, {-1,-1}, {-1,-1}, {-1,-1}, {-1,-1}, {-1,-1} };
      for( j=0;j<6;j++ )
     {
         k= hx[i].q[j].q;
         o= hx[i].q[j].o;
         assert( k > -1 );

         l= qv[k].h[!o];
         m= qv[k].q[!o];

         q[j][0]= l;
         q[j][1]= m;

         if( l == -1 )
        {
            assert( k < ng );
            assert( hx[i].q[j].g > -1 ); 
            assert( hx[i].q[j].b > -1 ); 
            destroy( hx[i].q[j].g,hx[i].q[j].b );
        }
         else
        {
            hx[i].q[j].q= -1;
            hx[i].q[j].b= -1;
            hx[i].q[j].g= -1;
            hx[i].q[j].o=  0;
            hx[i].q[j].r=  0;
            qv[k].h[o]= -1;
            qv[k].q[o]= -1;
        }
     }

      for( j=0;j<6;j++ )
     {
         l= q[j][0];
         m= q[j][1];
 
         if( l > -1 )
        {
 
            assert( m > -1 );
 
            h= mask[j];
            if( h == -1 ){ h=(MXGRP-1); };
 
// add to dummy group
            attach( h,l,m, -1,-1,-1 );

        }

     }

      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void hmesh_t::qel( INT_ id )
  {
      INT_ i,j,k,l;

      i= qv.n-1;

      for( l=0;l<2;l++ )
     {
         j= qv[id].h[l];
         assert( j == -1 );

         j= qv[i].h[l];
         k= qv[i].q[l];
         if( j > -1 )
        { 
            hx[j].q[k].q= id; 
        }
     }

      qv[id]= qv[i];
      qv[i].h[0]=-1;
      qv[i].h[1]=-1;
      qv[i].q[0]=-1;
      qv[i].q[1]=-1;
     (qv.n)--;
  }
