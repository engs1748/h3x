#  include <hmesh/hmesh.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   bool hmesh_t::map( INT_ i, INT_ d, dir_t *a )
  {

      bool val=false;
      INT_  j,k,l,m,n;

      j= qv[i].h[d];
      k= qv[i].q[d];

      if( j > -1 )
     {

         val= true;
         l= hx[j].q[k].r;
       
         m= mhq[k][ mqp[d][l][0] ];
         n= mhq[k][ mqp[d][l][1] ];
       
         a[0]= loc[n]- loc[m];
       
         n= mhq[k][ mqp[d][l][3] ];
         a[1]= loc[n]- loc[m];
       
         a[2]= dir_t( d,k/2 );

     }

      return val;

  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void map( dir_t c0, dir_t c1, dir_t *a, dir_t *b, dir_t d0, dir_t d1 )
  {
      INT_ i,j,k;
      INT_ idx[3];

      i= c0.dir();
      j= c1.dir();

      k= a[0].dir(); idx[k]= 0;
      k= a[1].dir(); idx[k]= 1;
      k= a[2].dir(); idx[k]= 2;

      i= idx[i];
      j= idx[j];

      d0= b[i];
      d1= b[j];

      if( c0.sign() != a[i].sign() ){ d0= !d0; };
      if( c1.sign() != a[j].sign() ){ d1= !d1; };

      return;
  }
