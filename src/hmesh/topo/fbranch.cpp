#  include <hmesh/hmesh.h>
#  include <tri.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void hmesh_t::fbranch( REAL_3 vv, INT_ id, dir_t d, INT_ *mask )
  {

      INT_        i,k;
      INT_        n;
      INT_        i0,i1,i2,i3,i4,i5,i6,i7;
      INT_        x0,x1,x2,x3,x4,x5,x6,x7, y0,y1,y2,y3,y4,y5,y6,y7;
      INT_        v0,v1,v2,v3,v4,v5,v6,v7, w0,w1,w2,w3,w4,w5,w6,w7;
      INT_        f0,f1,f2,f3,f4,f5;
      INT_        g0,g1,g2,g3,g4,g5;

      INT_        p1=-1,p2=-1,p3=-1,p4=-1,p5=-1;
      INT_        s1=-1,s2=-1,s3=-1,s4=-1,s5=-1;
      INT_        top,bottom;
      bool        o;

      INT_        x,v;
      vtx_t       f;
 
      const REAL_     c1=1./9.,c2=2./9.,c3=4./9.;

/*    su[7]->test( 0,(char*)"c0.0" );
      su[7]->test( 1,(char*)"c0.1" );
      su[7]->test( 2,(char*)"c0.2" );
      su[7]->test( 3,(char*)"c0.3" );*/

      i= indx[id];
      assert( i > -1 );

      k= d.dir();
      o= d.sign();

      i0= opp[ o][k][0]; i1= opp[ o][k][1]; i2= opp[ o][k][2]; i3= opp[ o][k][3];
      i4= opp[!o][k][0]; i5= opp[!o][k][1]; i6= opp[!o][k][2]; i7= opp[!o][k][3];

      v0= hx[i].v[i0]; v1= hx[i].v[i1]; v2= hx[i].v[i2]; v3= hx[i].v[i3]; 
      v4= hx[i].v[i4]; v5= hx[i].v[i5]; v6= hx[i].v[i6]; v7= hx[i].v[i7]; 

      x0= hx[i].x[i0]; x1= hx[i].x[i1]; x2= hx[i].x[i2]; x3= hx[i].x[i3]; 
      x4= hx[i].x[i4]; x5= hx[i].x[i5]; x6= hx[i].x[i6]; x7= hx[i].x[i7]; 

      f0= oqq[ o][k][0]; f1= oqq[ o][k][1]; f2= oqq[ o][k][2];
      f3= oqq[ o][k][3]; f4= oqq[ o][k][4]; f5= oqq[ o][k][5];

      g0= hx[i].q[f0].g; g1= hx[i].q[f1].g; g2= hx[i].q[f2].g;
      g3= hx[i].q[f3].g; g4= hx[i].q[f4].g; g5= hx[i].q[f5].g;

      if( g1 > -1 ){ p1= hx[i].q[f1].b; s1= bz[g1].bo[p1].s; p1= bz[g1].bo[p1].p; };
      if( g2 > -1 ){ p2= hx[i].q[f2].b; s2= bz[g2].bo[p2].s; p2= bz[g2].bo[p2].p; };
      if( g3 > -1 ){ p3= hx[i].q[f3].b; s3= bz[g3].bo[p3].s; p3= bz[g3].bo[p3].p; };
      if( g4 > -1 ){ p4= hx[i].q[f4].b; s4= bz[g4].bo[p4].s; p4= bz[g4].bo[p4].p; };
      if( g5 > -1 ){ p5= hx[i].q[f5].b; s5= bz[g5].bo[p5].s; p5= bz[g5].bo[p5].p; };

      assert( f0 > -1 );
      
//            qv.n+=24; qv.resize( qdum );
      x= vx.n; vx.n+= 8; vx.resize( xdum );
      v= vt.n; vt.n+= 8; vt.resize( pdum );
      n= hx.n; hx.n+= 5; hx.resize( hdum );

      vx[x]= c3*vx[x0]+ c2*vx[x1]+ c2*vx[x2]+ c1*vx[x3]; y0=x++;
      vx[x]= c2*vx[x0]+ c3*vx[x1]+ c1*vx[x2]+ c2*vx[x3]; y1=x++;
      vx[x]= c2*vx[x0]+ c1*vx[x1]+ c3*vx[x2]+ c2*vx[x3]; y2=x++;
      vx[x]= c1*vx[x0]+ c2*vx[x1]+ c2*vx[x2]+ c3*vx[x3]; y3=x++;
  
      vx[x]= vx[x0]; y4=x++;
      vx[x]= vx[x1]; y5=x++;
      vx[x]= vx[x2]; y6=x++;
      vx[x]= vx[x3]; y7=x++;

      vt[v].id= v; w0= v++;
      vt[v].id= v; w1= v++;
      vt[v].id= v; w2= v++;
      vt[v].id= v; w3= v++;
  
      vt[v].id= v; w4= v++;
      vt[v].id= v; w5= v++;
      vt[v].id= v; w6= v++;
      vt[v].id= v; w7= v++;

      f= 0.125*( vx[x4]+ vx[x5]+ vx[x6]+ vx[x7]- vx[x0]- vx[x1]- vx[x2]- vx[x3] );
      if( (fabs(vv[0])+fabs(vv[1])+ fabs(vv[2]))>0 )
     {
         f[0]= -vv[0];
         f[1]= -vv[1];
         f[2]= -vv[2];
     }

      vx[y0]= vx[y0]-f;
      vx[y1]= vx[y1]-f;
      vx[y2]= vx[y2]-f;
      vx[y3]= vx[y3]-f;
      vx[y4]= vx[y4]-f;
      vx[y5]= vx[y5]-f;
      vx[y6]= vx[y6]-f;
      vx[y7]= vx[y7]-f;

      if( ::vol( vx[y0],vx[y1],vx[y2],vx[y3],vx[x4],vx[x5],vx[x6],vx[x7] ) > 0 )
     {
         top=    5;
         bottom= 4;

         hex( n+0, w0,w1,w2,w3, v4,v5,v6,v7, y0,y1,y2,y3, x4,x5,x6,x7 ); 
         hex( n+1, w4,w5,w0,w1, v0,v1,v4,v5, y4,y5,y0,y1, x0,x1,x4,x5 ); 
         hex( n+2, w2,w3,w6,w7, v6,v7,v2,v3, y2,y3,y6,y7, x6,x7,x2,x3 ); 
         hex( n+3, w4,w0,w6,w2, v0,v4,v2,v6, y4,y0,y6,y2, x0,x4,x2,x6 ); 
         hex( n+4, w1,w5,w3,w7, v5,v1,v7,v3, y1,y5,y3,y7, x5,x1,x7,x3 ); 

     }
      else
     {
         top=    4;
         bottom= 5;

         hex( n+0, v4,v5,v6,v7, w0,w1,w2,w3, x4,x5,x6,x7, y0,y1,y2,y3  ); 
         hex( n+1, v0,v1,v4,v5, w4,w5,w0,w1, x0,x1,x4,x5, y4,y5,y0,y1  ); 
         hex( n+2, v6,v7,v2,v3, w2,w3,w6,w7, x6,x7,x2,x3, y2,y3,y6,y7  ); 
         hex( n+3, v0,v4,v2,v6, w4,w0,w6,w2, x0,x4,x2,x6, y4,y0,y6,y2  ); 
         hex( n+4, v5,v1,v7,v3, w1,w5,w3,w7, x5,x1,x7,x3, y1,y5,y3,y7  ); 

     }
      force( n+0,2, n+1,3 );
      force( n+0,3, n+2,2 );
      force( n+0,0, n+3,1 );
      force( n+0,1, n+4,0 );

      force( n+1,0, n+3,2 );
      force( n+2,0, n+3,3 );
      force( n+1,1, n+4,2 );
      force( n+2,1, n+4,3 );

      force( n+1,2,-1,-1 ); 
      force( n+2,3,-1,-1 ); 
      force( n+3,0,-1,-1 ); 
      force( n+4,1,-1,-1 ); 

      if( g0 == mask[4] ){};

      force( n+0,bottom, -1,-1 );
      force( n+1,bottom, -1,-1 ); 
      force( n+2,bottom, -1,-1 ); 
      force( n+3,bottom, -1,-1 ); 
      force( n+4,bottom, -1,-1 ); 


      if( g1 > -1 ){ force( n+0,top,-1,-1 ); }else{ replace( i,f1, n+0,top ); }

      if( g2 > -1 ){ force( n+3,top,-1,-1 ); }else{ replace( i,f2, n+3,top ); }
      if( g5 > -1 ){ force( n+4,top,-1,-1 ); }else{ replace( i,f5, n+4,top ); }

      if( g3 > -1 ){ force( n+1,top,-1,-1 ); }else{ replace( i,f3, n+1,top ); }
      if( g4 > -1 ){ force( n+2,top,-1,-1 ); }else{ replace( i,f4, n+2,top ); }


      attach( mask[2],n+1,2,-1,-1,-1 ); 
      attach( mask[3],n+2,3,-1,-1,-1 ); 
      attach( mask[0],n+3,0,-1,-1,-1 ); 
      attach( mask[1],n+4,1,-1,-1,-1 ); 

      if( g0 == mask[4] ){};

      attach( mask[4],n+0,bottom, -1,-1,-1 );
      attach( mask[4],n+1,bottom, -1,-1,-1 ); 
      attach( mask[4],n+2,bottom, -1,-1,-1 ); 
      attach( mask[4],n+3,bottom, -1,-1,-1 ); 
      attach( mask[4],n+4,bottom, -1,-1,-1 ); 


      if( g1 > -1 ){ attach( g1,n+0,top,p1,s1,-1 ); }

      if( g2 > -1 ){ attach( g2,n+3,top,p2,s2,-1 ); }
      if( g5 > -1 ){ attach( g5,n+4,top,p5,s5,-1 ); }

      if( g3 > -1 ){ attach( g3,n+1,top,p3,s3,-1 ); }
      if( g4 > -1 ){ attach( g4,n+2,top,p4,s4,-1 ); }

      trimb();
      return;
  }
