#  include <hmesh/hmesh.h>

   void hmesh_t::add( hmesh_t &var, INT_ *mask )
  {
      INT_            b,c,e,i,j,h,l,m,q;
      INT_            h0;
      bool            flag;
      INT_            n1,n2;

      vtx_t           w;

      VINT_           prd(var.vt.n,-1);
      VINT_           wrk(vt.n,-1);
      VINT_4          prq;
      VINT_3          bnd;

      assert( vt.n == vx.n );
      trimb();
      var.trimb();

      n1= nb;
      n2= var.nb;

      for( b=0;b<n1;b++ )
     {
         for( c=0;c<n2;c++ )
        {
            printf( "comparing boundaries %d %d\n", c,b );
            flag= compare( -1,-1, b,var,c, wrk, prq );
            if( flag )
           { 

               printf( "boundaries %d %d match\n", b,c );

               for( j=0;j<bz[b].bx.n;j++ )
              {
                  h= bz[b].bx[j].id;
                  if( h > -1 ) 
                 { 
                     prd[wrk[h]]= h;
                 }
              }

               for( j=var.bz[c].bo.n-1;j>=0;j-- )
              {

                  h= var.bz[c].bo[j].h;
                  q= var.bz[c].bo[j].q;
                  var.destroy( c,j ); 

                  h= bz[b].bo[j].h;
                  q= bz[b].bo[j].q;
                  destroy( b,j ); 
              }
               break; 
           }
        }
         if( flag ){ break; };
     }

      for( i=0;i<var.vt.n;i++ )
     {
         if( var.vt[i].h > -1 )
        {
            if( prd[i] == -1 )
           {
          
               j= vt.append(pdum);
               assert( j==vx.append(xdum) );
          
               vt[j].id= j;
               vx[j][0]= var.vx[i][0];
               vx[j][1]= var.vx[i][1];
               vx[j][2]= var.vx[i][2];
               prd[i]= j;
           }
        }
     }

// replicate elements         

      h0= hx.n;
      for( i=0;i<var.hx.n;i++ )
     {
         j= hx.append(hdum);
         hex( j, prd[var.hx[i].v[0]], prd[var.hx[i].v[1]], prd[var.hx[i].v[2]], prd[var.hx[i].v[3]],
                 prd[var.hx[i].v[4]], prd[var.hx[i].v[5]], prd[var.hx[i].v[6]], prd[var.hx[i].v[7]],
                 prd[var.hx[i].x[0]], prd[var.hx[i].x[1]], prd[var.hx[i].x[2]], prd[var.hx[i].x[3]],
                 prd[var.hx[i].x[4]], prd[var.hx[i].x[5]], prd[var.hx[i].x[6]], prd[var.hx[i].x[7]] );

     }

// replicate internal faces

      for( i=var.ng;i<var.qv.n;i++ )
     {
         j= var.qv[i].h[0];
         h= var.qv[i].h[1];
         l= var.qv[i].q[0];
         m= var.qv[i].q[1];
         force( j+h0,l, h+h0,m );
     }

// seal boundary between two copies

      for( i=0;i<prq.n;i++ )
     {
         j= prq[i][0];
         h= prq[i][1];
         l= prq[i][2];
         m= prq[i][3];
         force( l+h0,m, j,h );
     }

      
// boundary surfaces

      for( b=0;b<n2;b++ )
     {
         if( var.bz[b].bo.n > 0 )
        {
            
            for( j=0;j<var.bz[b].bo.n;j++ )
           {
               h= var.bz[b].bo[j].h;
               q= var.bz[b].bo[j].q;
               force( h+h0,q, -1,-1 );
           }
        }
     } 

      for( b=0;b<n2;b++ )
     {
         e= b;
         if( var.bz[b].bo.n > 0 )
        {
            if( mask[b] == -1 ){ e= n1++; }
            
            for( j=0;j<var.bz[b].bo.n;j++ )
           {
               h= var.bz[b].bo[j].h;
               q= var.bz[b].bo[j].q;
               attach( e, h+h0,q,-1,-1,-1 );
           }
        }
     } 
      trimb();
      return;
  }

   void hmesh_t::add( hmesh_t &var, VINT_2 &list, INT_ *mask, vsurf_t &sa, vsurf_t &sb )
  {
      INT_            b,c,e,i,j,h,l,m,q;
      INT_            h0;
      INT_            n1,n2;

      vtx_t           w;

      VINT_           prd(var.vt.n,-1);
      VINT_           wrk(vt.n,-1);
      VINT_4          prq;
      VINT_3          bnd;

      assert( vt.n == vx.n );
      trimb();
      var.trimb();

      n1= nb;
      n2= var.nb;

      for( INT_ k=0;k<list.n;k++ )
     {
         b= list[k][0];
         c= list[k][1];
//       compare1( -1,-1, b,var,c, wrk, prq );
         compare2( -1,-1, b,var,c, wrk, prq );
         for( j=0;j<bz[b].bx.n;j++ )
        {
            h= bz[b].bx[j].id;
            if( h > -1 ) 
           { 
               prd[wrk[h]]= h;
           }
        }

         for( j=var.bz[c].bo.n-1;j>=0;j-- )
        {

            h= var.bz[c].bo[j].h;
            q= var.bz[c].bo[j].q;
            var.destroy( c,j ); 

            h= bz[b].bo[j].h;
            q= bz[b].bo[j].q;
            destroy( b,j ); 
        }
     }

      for( i=0;i<var.vt.n;i++ )
     {
         if( var.vt[i].h > -1 )
        {
            if( prd[i] == -1 )
           {
          
               j= vt.append(pdum);
               assert( j==vx.append(xdum) );
          
               vt[j].id= j;
               vx[j][0]= var.vx[i][0];
               vx[j][1]= var.vx[i][1];
               vx[j][2]= var.vx[i][2];
               prd[i]= j;
           }
        }
     }

// replicate elements         

      h0= hx.n;
      for( i=0;i<var.hx.n;i++ )
     {
         j= hx.append(hdum);
         hex( j, prd[var.hx[i].v[0]], prd[var.hx[i].v[1]], prd[var.hx[i].v[2]], prd[var.hx[i].v[3]],
                 prd[var.hx[i].v[4]], prd[var.hx[i].v[5]], prd[var.hx[i].v[6]], prd[var.hx[i].v[7]],
                 prd[var.hx[i].x[0]], prd[var.hx[i].x[1]], prd[var.hx[i].x[2]], prd[var.hx[i].x[3]],
                 prd[var.hx[i].x[4]], prd[var.hx[i].x[5]], prd[var.hx[i].x[6]], prd[var.hx[i].x[7]] );
         memcpy( hx[j].iz, var.hx[i].iz,   sizeof(INT_4) );
         memcpy( hx[j].d,  var.hx[i].d,  8*sizeof(REAL_3) );
         

     }

// replicate internal faces

      for( i=var.ng;i<var.qv.n;i++ )
     {
         j= var.qv[i].h[0];
         h= var.qv[i].h[1];
         l= var.qv[i].q[0];
         m= var.qv[i].q[1];
         force( j+h0,l, h+h0,m );
     }

// seal boundary between two copies

      for( i=0;i<prq.n;i++ )
     {
         j= prq[i][0];
         h= prq[i][1];
         l= prq[i][2];
         m= prq[i][3];
         force( l+h0,m, j,h );
     }

      
// boundary surfaces

      for( b=0;b<n2;b++ )
     {
         if( var.bz[b].bo.n > 0 )
        {
            
            for( j=0;j<var.bz[b].bo.n;j++ )
           {
               h= var.bz[b].bo[j].h;
               q= var.bz[b].bo[j].q;
               force( h+h0,q, -1,-1 );
           }
        }
     } 

// now merge surface lists

      VINT_ sprm(sb.n,-1);

      for( i=0;i<sb.n;i++ )
     {
         bool flag= false;
         for( j=0;j<sa.n;j++ )
        {
            if( sa[j]->compare( sb[i] ) )
           {
               flag= true;
               break;
           }
        }
         if( !flag )
        {
            j= sa.append(NULL);
            sb[i]->copy( &(sa[j]) );
        }
         sprm[i]= j;
     }

      for( b=0;b<n2;b++ )
     {
         e= b;
         if( var.bz[b].bo.n > 0 )
        {
            if( mask[b] == -1 ){ e= n1++; }
            
            for( j=0;j<var.bz[b].bo.n;j++ )
           {
               h= var.bz[b].bo[j].h;
               q= var.bz[b].bo[j].q;

               INT_ s= var.bz[b].bo[j].s;
               s= sprm[s];
               INT_ p= var.bz[b].bo[j].p;
               INT_ r= var.bz[b].bo[j].b;
               attach( e, h+h0,q, s,p,r );

               bz[e].lbl[0]= var.bz[b].lbl[0];
               bz[e].lbl[1]= var.bz[b].lbl[1];

               i = hx[h+h0].q[q].b;
               for( h=0;h<4;h++ )
              {
                  INT_ i0= bz[e].bo[i].v[h];
                  INT_ i1= var.bz[b].bo[j].v[h];

                  bz[e].bx[i0].y[0]= var.bz[b].bx[i1].y[0];
                  bz[e].bx[i0].y[1]= var.bz[b].bx[i1].y[1];
              }
           }
        }
     } 

      trimb();

      return;
  }
