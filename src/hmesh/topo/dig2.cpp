#  include <hmesh/hmesh.h>

   void hmesh_t::dig2( dir_t d0, dir_t d1, INT_ kst, REAL_3 v, INT_ *mask )
  {

      INT_            n,m;
      INT_            i;

      vtx_t           w(v);
      vtx_t           z;
      vtx_t           s;
      vtx_t           r;
      vtx_t           c;

      REAL_           d;
      REAL_           f,g;
      const REAL_     i12= 0.25/3.;

      cavty_t         cv;

      g= 0.2;
      z[0]= v[0];
      z[1]= v[1];
      z[2]= v[2];
      f= l2norm(z);
      d= 1./f;
      z*= d;

      cvty( 2,2,1, d0,d1, kst, cv, true );

      r= 0.25*( vx[cv.v[0]]+ vx[cv.v[1]]+ vx[cv.v[2]]+ vx[cv.v[3]] );

// initial points

      n= vx.n; vx.n+= 87; vx.resize( xdum );
               vt.n+= 87; vt.resize( pdum );

      for( i=n;i<vt.n;i++ ){ vt[i].id= i; };

      vx[n+ 0]= vx[cv.v[4]]- r; vx[n+ 0]*= 0.75; vx[n+ 0]+= r;
      vx[n+ 2]= vx[cv.v[5]]- r; vx[n+ 2]*= 0.75; vx[n+ 2]+= r;

      vx[n+ 6]= vx[cv.v[6]]- r; vx[n+ 6]*= 0.75; vx[n+ 6]+= r;
      vx[n+ 8]= vx[cv.v[7]]- r; vx[n+ 8]*= 0.75; vx[n+ 8]+= r;

      vx[n+18]= vx[cv.v[0]]- r; vx[n+18]*= 0.75; vx[n+18]+= r;
      vx[n+20]= vx[cv.v[1]]- r; vx[n+20]*= 0.75; vx[n+20]+= r;

      vx[n+32]= vx[cv.v[2]]- r; vx[n+32]*= 0.75; vx[n+32]+= r;
      vx[n+34]= vx[cv.v[3]]- r; vx[n+34]*= 0.75; vx[n+34]+= r;
      

// complete A section

      vx[n+ 1]=   0.5*( vx[n+ 0]+ vx[n+ 2] );
      vx[n+ 3]=   0.5*( vx[n+ 0]+ vx[n+ 6] );
      vx[n+ 5]=   0.5*( vx[n+ 2]+ vx[n+ 8] );
      vx[n+ 7]=   0.5*( vx[n+ 6]+ vx[n+ 8] );

      vx[n+ 4]=  0.25*( vx[n+ 0]+ vx[n+ 2]+ vx[n+ 6]+ vx[n+ 8] );

// complete C section

      vx[n+19]=   0.5*( vx[n+18]+ vx[n+20] );      
      vx[n+30]=   0.5*( vx[n+18]+ vx[n+32] );      
      vx[n+31]=   0.5*( vx[n+20]+ vx[n+34] );      
      vx[n+33]=   0.5*( vx[n+32]+ vx[n+34] );      

      vx[n+25]=  0.25*( vx[n+19]+ vx[n+30]+ vx[n+31]+ vx[n+33] );

      vx[n+21]=  0.75*vx[n+18]+ i12*( vx[n+19]+ vx[n+25]+ vx[n+30] );
      vx[n+23]=  0.75*vx[n+20]+ i12*( vx[n+19]+ vx[n+25]+ vx[n+31] );
      vx[n+27]=  0.75*vx[n+32]+ i12*( vx[n+25]+ vx[n+30]+ vx[n+33] );
      vx[n+29]=  0.75*vx[n+34]+ i12*( vx[n+25]+ vx[n+31]+ vx[n+33] );

      vx[n+22]=   0.5*( vx[n+21]+ vx[n+23] );      
      vx[n+24]=   0.5*( vx[n+21]+ vx[n+27] );      
      vx[n+26]=   0.5*( vx[n+23]+ vx[n+29] );      
      vx[n+28]=   0.5*( vx[n+27]+ vx[n+29] );      

// B section

      vx[n+ 9]=  0.75*vx[n+ 0]+ i12*( vx[n+ 1]+ vx[n+ 3]+ vx[n+ 4] );
      vx[n+11]=  0.75*vx[n+ 2]+ i12*( vx[n+ 1]+ vx[n+ 4]+ vx[n+ 5] );
      vx[n+15]=  0.75*vx[n+ 6]+ i12*( vx[n+ 3]+ vx[n+ 4]+ vx[n+ 7] );
      vx[n+17]=  0.75*vx[n+ 8]+ i12*( vx[n+ 4]+ vx[n+ 5]+ vx[n+ 7] );

      vx[n+ 9]+= vx[n+21];
      vx[n+11]+= vx[n+23];
      vx[n+15]+= vx[n+27];
      vx[n+17]+= vx[n+29];

      vx[n+ 9]*= 0.5;
      vx[n+11]*= 0.5;
      vx[n+15]*= 0.5;
      vx[n+17]*= 0.5;

      vx[n+10]=   0.5*( vx[n+ 9]+ vx[n+11] );
      vx[n+12]=   0.5*( vx[n+ 9]+ vx[n+15] );
      vx[n+14]=   0.5*( vx[n+11]+ vx[n+17] );
      vx[n+16]=   0.5*( vx[n+15]+ vx[n+17] );
      vx[n+13]=  0.25*( vx[n+ 9]+ vx[n+11]+ vx[n+15]+ vx[n+17] );

// D section      

      s= vtx_t(v);
      s*= g;

      vx[n+35]= vx[n+18]+ s;
      vx[n+36]= vx[n+19]+ s;
      vx[n+37]= vx[n+20]+ s;
      vx[n+38]= vx[n+21]+ s;
      vx[n+39]= vx[n+22]+ s;
      vx[n+40]= vx[n+23]+ s;
      vx[n+41]= vx[n+30]+ s;
      vx[n+42]= vx[n+24]+ s;
      vx[n+43]= vx[n+25]+ s;
      vx[n+44]= vx[n+26]+ s;
      vx[n+45]= vx[n+31]+ s;
      vx[n+46]= vx[n+27]+ s;
      vx[n+47]= vx[n+28]+ s;
      vx[n+48]= vx[n+29]+ s;
      vx[n+49]= vx[n+32]+ s;
      vx[n+50]= vx[n+33]+ s;
      vx[n+51]= vx[n+34]+ s;

// E section

      c= 0.8*vtx_t(v);
      c+=  r;

      orth( c,    z,vx[n+18], vx[n+52] );
      orth( c,    z,vx[n+20], vx[n+53] );
      orth( c,    z,vx[n+21], vx[n+54] );
      orth( c,    z,vx[n+23], vx[n+55] );
      orth( c,    z,vx[n+30], vx[n+56] );
      orth( c,    z,vx[n+24], vx[n+57] );
      orth( c,    z,vx[n+26], vx[n+58] );
      orth( c,    z,vx[n+31], vx[n+59] );
      orth( c,    z,vx[n+27], vx[n+60] );
      orth( c,    z,vx[n+29], vx[n+61] );
      orth( c,    z,vx[n+32], vx[n+62] );
      orth( c,    z,vx[n+34], vx[n+63] );

// F section
      c=  r+    vtx_t(v);

      orth( c,    z,vx[n+18], vx[n+64] );
      orth( c,    z,vx[n+20], vx[n+65] );
      orth( c,    z,vx[n+21], vx[n+66] );
      orth( c,    z,vx[n+23], vx[n+67] );
      orth( c,    z,vx[n+30], vx[n+68] );
      orth( c,    z,vx[n+24], vx[n+69] );
      orth( c,    z,vx[n+26], vx[n+70] );
      orth( c,    z,vx[n+31], vx[n+71] );
      orth( c,    z,vx[n+27], vx[n+72] );
      orth( c,    z,vx[n+29], vx[n+73] );
      orth( c,    z,vx[n+32], vx[n+74] );
      orth( c,    z,vx[n+34], vx[n+75] );

// G section

      c= 0.5*vtx_t(v);
      c+=  r;

      orth( c,    z,vx[n+19], vx[n+76] );
      orth( c,    z,vx[n+20], vx[n+77] );
      orth( c,    z,vx[n+22], vx[n+78] );
      orth( c,    z,vx[n+23], vx[n+79] );
      orth( c,    z,vx[n+25], vx[n+80] );
      orth( c,    z,vx[n+26], vx[n+81] );
      orth( c,    z,vx[n+31], vx[n+82] );
      orth( c,    z,vx[n+28], vx[n+83] );
      orth( c,    z,vx[n+29], vx[n+84] );
      orth( c,    z,vx[n+33], vx[n+85] );
      orth( c,    z,vx[n+34], vx[n+86] );

// form hexahedra

      m= hx.n; hx.n+= 66; hx.resize( hdum );

      hex( m+ 0, n+ 9,n+10,n+12,n+13,n+ 0,n+ 1,n+ 3,n+ 4, n+ 9,n+10,n+12,n+13,n+ 0,n+ 1,n+ 3,n+ 4 );
      hex( m+ 1, n+10,n+11,n+13,n+14,n+ 1,n+ 2,n+ 4,n+ 5, n+10,n+11,n+13,n+14,n+ 1,n+ 2,n+ 4,n+ 5 );
      hex( m+ 2, n+12,n+13,n+15,n+16,n+ 3,n+ 4,n+ 6,n+ 7, n+12,n+13,n+15,n+16,n+ 3,n+ 4,n+ 6,n+ 7 );
      hex( m+ 3, n+13,n+14,n+16,n+17,n+ 4,n+ 5,n+ 7,n+ 8, n+13,n+14,n+16,n+17,n+ 4,n+ 5,n+ 7,n+ 8 );
                                                                                                 
      hex( m+ 4, n+18,n+19,n+21,n+22,n+ 0,n+ 1,n+ 9,n+10, n+18,n+19,n+21,n+22,n+ 0,n+ 1,n+ 9,n+10 );
      hex( m+ 5, n+19,n+20,n+22,n+23,n+ 1,n+ 2,n+10,n+11, n+19,n+20,n+22,n+23,n+ 1,n+ 2,n+10,n+11 );
      hex( m+ 6, n+18,n+21,n+30,n+24,n+ 0,n+ 9,n+ 3,n+12, n+18,n+21,n+30,n+24,n+ 0,n+ 9,n+ 3,n+12 );
      hex( m+ 7, n+23,n+20,n+26,n+31,n+11,n+ 2,n+14,n+ 5, n+23,n+20,n+26,n+31,n+11,n+ 2,n+14,n+ 5 );
      hex( m+ 8, n+30,n+24,n+32,n+27,n+ 3,n+12,n+ 6,n+15, n+30,n+24,n+32,n+27,n+ 3,n+12,n+ 6,n+15 );
      hex( m+ 9, n+26,n+31,n+29,n+34,n+14,n+ 5,n+17,n+ 8, n+26,n+31,n+29,n+34,n+14,n+ 5,n+17,n+ 8 );
      hex( m+10, n+27,n+28,n+32,n+33,n+15,n+16,n+ 6,n+ 7, n+27,n+28,n+32,n+33,n+15,n+16,n+ 6,n+ 7 );
      hex( m+11, n+28,n+29,n+33,n+34,n+16,n+17,n+ 7,n+ 8, n+28,n+29,n+33,n+34,n+16,n+17,n+ 7,n+ 8 );
      hex( m+12, n+21,n+22,n+24,n+25,n+ 9,n+10,n+12,n+13, n+21,n+22,n+24,n+25,n+ 9,n+10,n+12,n+13 );
      hex( m+13, n+22,n+23,n+25,n+26,n+10,n+11,n+13,n+14, n+22,n+23,n+25,n+26,n+10,n+11,n+13,n+14 );
      hex( m+14, n+24,n+25,n+27,n+28,n+12,n+13,n+15,n+16, n+24,n+25,n+27,n+28,n+12,n+13,n+15,n+16 );
      hex( m+15, n+25,n+26,n+28,n+29,n+13,n+14,n+16,n+17, n+25,n+26,n+28,n+29,n+13,n+14,n+16,n+17 );
                                                                                                 
      hex( m+16, n+35,n+36,n+38,n+39,n+18,n+19,n+21,n+22, n+35,n+36,n+38,n+39,n+18,n+19,n+21,n+22 );
      hex( m+17, n+36,n+37,n+39,n+40,n+19,n+20,n+22,n+23, n+36,n+37,n+39,n+40,n+19,n+20,n+22,n+23 );
      hex( m+18, n+35,n+38,n+41,n+42,n+18,n+21,n+30,n+24, n+35,n+38,n+41,n+42,n+18,n+21,n+30,n+24 );
      hex( m+19, n+38,n+39,n+42,n+43,n+21,n+22,n+24,n+25, n+38,n+39,n+42,n+43,n+21,n+22,n+24,n+25 );
      hex( m+20, n+39,n+40,n+43,n+44,n+22,n+23,n+25,n+26, n+39,n+40,n+43,n+44,n+22,n+23,n+25,n+26 );
      hex( m+21, n+40,n+37,n+44,n+45,n+23,n+20,n+26,n+31, n+40,n+37,n+44,n+45,n+23,n+20,n+26,n+31 );
      hex( m+22, n+41,n+42,n+49,n+46,n+30,n+24,n+32,n+27, n+41,n+42,n+49,n+46,n+30,n+24,n+32,n+27 );
      hex( m+23, n+42,n+43,n+46,n+47,n+24,n+25,n+27,n+28, n+42,n+43,n+46,n+47,n+24,n+25,n+27,n+28 );
      hex( m+24, n+43,n+44,n+47,n+48,n+25,n+26,n+28,n+29, n+43,n+44,n+47,n+48,n+25,n+26,n+28,n+29 );
      hex( m+25, n+44,n+45,n+48,n+51,n+26,n+31,n+29,n+34, n+44,n+45,n+48,n+51,n+26,n+31,n+29,n+34 );
      hex( m+26, n+46,n+47,n+49,n+50,n+27,n+28,n+32,n+33, n+46,n+47,n+49,n+50,n+27,n+28,n+32,n+33 );
      hex( m+27, n+47,n+48,n+50,n+51,n+28,n+29,n+33,n+34, n+47,n+48,n+50,n+51,n+28,n+29,n+33,n+34 );
                                                                                                 
      hex( m+28, n+64,n+65,n+66,n+67,n+52,n+53,n+54,n+55, n+64,n+65,n+66,n+67,n+52,n+53,n+54,n+55 );
      hex( m+29, n+64,n+66,n+68,n+69,n+52,n+54,n+56,n+57, n+64,n+66,n+68,n+69,n+52,n+54,n+56,n+57 );
      hex( m+30, n+66,n+67,n+69,n+70,n+54,n+55,n+57,n+58, n+66,n+67,n+69,n+70,n+54,n+55,n+57,n+58 );
      hex( m+31, n+68,n+69,n+74,n+72,n+56,n+57,n+62,n+60, n+68,n+69,n+74,n+72,n+56,n+57,n+62,n+60 );
      hex( m+32, n+69,n+70,n+72,n+73,n+57,n+58,n+60,n+61, n+69,n+70,n+72,n+73,n+57,n+58,n+60,n+61 );
      hex( m+33, n+70,n+71,n+73,n+75,n+58,n+59,n+61,n+63, n+70,n+71,n+73,n+75,n+58,n+59,n+61,n+63 );
      hex( m+34, n+67,n+65,n+70,n+71,n+55,n+53,n+58,n+59, n+67,n+65,n+70,n+71,n+55,n+53,n+58,n+59 );
      hex( m+35, n+72,n+73,n+74,n+75,n+60,n+61,n+62,n+63, n+72,n+73,n+74,n+75,n+60,n+61,n+62,n+63 );
                                                                                                 
      hex( m+36, n+52,n+54,n+56,n+57,n+35,n+38,n+41,n+42, n+52,n+54,n+56,n+57,n+35,n+38,n+41,n+42 );
      hex( m+37, n+56,n+57,n+62,n+60,n+41,n+42,n+49,n+46, n+56,n+57,n+62,n+60,n+41,n+42,n+49,n+46 );
                                                                                                 
      hex( m+38, n+52,n+53,n+54,n+55,n+76,n+77,n+78,n+79, n+52,n+53,n+54,n+55,n+76,n+77,n+78,n+79 );
      hex( m+39, n+54,n+55,n+57,n+58,n+78,n+79,n+80,n+81, n+54,n+55,n+57,n+58,n+78,n+79,n+80,n+81 );
      hex( m+40, n+55,n+53,n+58,n+59,n+79,n+77,n+81,n+82, n+55,n+53,n+58,n+59,n+79,n+77,n+81,n+82 );
      hex( m+41, n+57,n+58,n+60,n+61,n+80,n+81,n+83,n+84, n+57,n+58,n+60,n+61,n+80,n+81,n+83,n+84 );
      hex( m+42, n+58,n+59,n+61,n+63,n+81,n+82,n+84,n+86, n+58,n+59,n+61,n+63,n+81,n+82,n+84,n+86 );
      hex( m+43, n+60,n+61,n+62,n+63,n+83,n+84,n+85,n+86, n+60,n+61,n+62,n+63,n+83,n+84,n+85,n+86 );
                                                                                                 
      hex( m+44, n+76,n+77,n+78,n+79,n+36,n+37,n+39,n+40, n+76,n+77,n+78,n+79,n+36,n+37,n+39,n+40 );
      hex( m+45, n+78,n+79,n+80,n+81,n+39,n+40,n+43,n+44, n+78,n+79,n+80,n+81,n+39,n+40,n+43,n+44 );
      hex( m+46, n+79,n+77,n+81,n+82,n+40,n+37,n+44,n+45, n+79,n+77,n+81,n+82,n+40,n+37,n+44,n+45 );
      hex( m+47, n+80,n+81,n+83,n+84,n+43,n+44,n+47,n+48, n+80,n+81,n+83,n+84,n+43,n+44,n+47,n+48 );
      hex( m+48, n+81,n+82,n+84,n+86,n+44,n+45,n+48,n+51, n+81,n+82,n+84,n+86,n+44,n+45,n+48,n+51 );
      hex( m+49, n+83,n+84,n+85,n+86,n+47,n+48,n+50,n+51, n+83,n+84,n+85,n+86,n+47,n+48,n+50,n+51 );
      hex( m+50, n+52,n+76,n+54,n+78,n+35,n+36,n+38,n+39, n+52,n+76,n+54,n+78,n+35,n+36,n+38,n+39 );
      hex( m+51, n+54,n+78,n+57,n+80,n+38,n+39,n+42,n+43, n+54,n+78,n+57,n+80,n+38,n+39,n+42,n+43 );
      hex( m+52, n+57,n+80,n+60,n+83,n+42,n+43,n+46,n+47, n+57,n+80,n+60,n+83,n+42,n+43,n+46,n+47 );
      hex( m+53, n+60,n+83,n+62,n+85,n+46,n+47,n+49,n+50, n+60,n+83,n+62,n+85,n+46,n+47,n+49,n+50 );

// connecting hexahedra

      hex( m+54, cv.v[ 0],cv.v[ 8],n+18    ,n+19    ,cv.v[ 4],cv.v[10], n+ 0   ,n+ 1    , cv.v[ 0],cv.v[ 8],n+18    ,n+19    ,cv.v[ 4],cv.v[10], n+ 0   ,n+ 1     );
      hex( m+55, cv.v[ 8],cv.v[ 1],n+19    ,n+20    ,cv.v[10],cv.v[ 5], n+ 1   ,n+ 2    , cv.v[ 8],cv.v[ 1],n+19    ,n+20    ,cv.v[10],cv.v[ 5], n+ 1   ,n+ 2     );
      hex( m+56, cv.v[ 0],n+18    ,cv.v[12],n+30    ,cv.v[ 4],n+ 0    ,cv.v[14],n+ 3    , cv.v[ 0],n+18    ,cv.v[12],n+30    ,cv.v[ 4],n+ 0    ,cv.v[14],n+ 3     );
      hex( m+57, n+20    ,cv.v[ 1],n+31    ,cv.v[13],n+ 2    ,cv.v[ 5],n+ 5    ,cv.v[15], n+20    ,cv.v[ 1],n+31    ,cv.v[13],n+ 2    ,cv.v[ 5],n+ 5    ,cv.v[15] );
      hex( m+58, cv.v[12],n+30    ,cv.v[ 2],n+32    ,cv.v[14],n+ 3    ,cv.v[ 6],n+ 6    , cv.v[12],n+30    ,cv.v[ 2],n+32    ,cv.v[14],n+ 3    ,cv.v[ 6],n+ 6     );
      hex( m+59, n+31    ,cv.v[13],n+34    ,cv.v[ 3],n+ 5    ,cv.v[15],n+ 8    ,cv.v[ 7], n+31    ,cv.v[13],n+34    ,cv.v[ 3],n+ 5    ,cv.v[15],n+ 8    ,cv.v[ 7] );
      hex( m+60, n+32    ,n+33    ,cv.v[ 2],cv.v[ 9],n+ 6    ,n+ 7    ,cv.v[ 6],cv.v[11], n+32    ,n+33    ,cv.v[ 2],cv.v[ 9],n+ 6    ,n+ 7    ,cv.v[ 6],cv.v[11] );
      hex( m+61, n+33    ,n+34    ,cv.v[ 9],cv.v[ 3],n+ 7    ,n+ 8    ,cv.v[11],cv.v[ 7], n+33    ,n+34    ,cv.v[ 9],cv.v[ 3],n+ 7    ,n+ 8    ,cv.v[11],cv.v[ 7] );

      hex( m+62, n+ 0    ,n+ 1    ,n+ 3    ,n+ 4    ,cv.v[ 4],cv.v[10],cv.v[14],cv.v[17], n+ 0    ,n+ 1    ,n+ 3    ,n+ 4    ,cv.v[ 4],cv.v[10],cv.v[14],cv.v[17] );
      hex( m+63, n+ 1    ,n+ 2    ,n+ 4    ,n+ 5    ,cv.v[10],cv.v[ 5],cv.v[17],cv.v[15], n+ 1    ,n+ 2    ,n+ 4    ,n+ 5    ,cv.v[10],cv.v[ 5],cv.v[17],cv.v[15] );
      hex( m+64, n+ 3    ,n+ 4    ,n+ 6    ,n+ 7    ,cv.v[14],cv.v[17],cv.v[ 6],cv.v[11], n+ 3    ,n+ 4    ,n+ 6    ,n+ 7    ,cv.v[14],cv.v[17],cv.v[ 6],cv.v[11] );
      hex( m+65, n+ 4    ,n+ 5    ,n+ 7    ,n+ 8    ,cv.v[17],cv.v[15],cv.v[11],cv.v[ 7], n+ 4    ,n+ 5    ,n+ 7    ,n+ 8    ,cv.v[17],cv.v[15],cv.v[11],cv.v[ 7] );

// new faces

      quads( cv.v, m,m+66 );

// Now attach faces to boundaries

      attach( mask[0], m+18,0, -1,-1,-1 );
      attach( mask[0], m+22,0, -1,-1,-1 );
      attach( mask[0], m+29,0, -1,-1,-1 );
      attach( mask[0], m+31,0, -1,-1,-1 );
      attach( mask[0], m+36,0, -1,-1,-1 );
      attach( mask[0], m+37,0, -1,-1,-1 );

      attach( mask[1], m+21,1, -1,-1,-1 );
      attach( mask[1], m+25,1, -1,-1,-1 );
      attach( mask[1], m+33,1, -1,-1,-1 );
      attach( mask[1], m+34,1, -1,-1,-1 );
      attach( mask[1], m+40,1, -1,-1,-1 );
      attach( mask[1], m+42,1, -1,-1,-1 );
      attach( mask[1], m+46,1, -1,-1,-1 );
      attach( mask[1], m+48,1, -1,-1,-1 );

      attach( mask[2], m+16,2, -1,-1,-1 );
      attach( mask[2], m+17,2, -1,-1,-1 );
      attach( mask[2], m+28,2, -1,-1,-1 );
      attach( mask[2], m+38,2, -1,-1,-1 );
      attach( mask[2], m+44,2, -1,-1,-1 );
      attach( mask[2], m+50,2, -1,-1,-1 );

      attach( mask[3], m+26,3, -1,-1,-1 );
      attach( mask[3], m+27,3, -1,-1,-1 );
      attach( mask[3], m+35,3, -1,-1,-1 );
      attach( mask[3], m+43,3, -1,-1,-1 );
      attach( mask[3], m+49,3, -1,-1,-1 );
      attach( mask[3], m+53,3, -1,-1,-1 );

      attach( mask[4], m+28,4, -1,-1,-1 );
      attach( mask[4], m+29,4, -1,-1,-1 );
      attach( mask[4], m+30,4, -1,-1,-1 );
      attach( mask[4], m+31,4, -1,-1,-1 );
      attach( mask[4], m+32,4, -1,-1,-1 );
      attach( mask[4], m+33,4, -1,-1,-1 );
      attach( mask[4], m+34,4, -1,-1,-1 );
      attach( mask[4], m+35,4, -1,-1,-1 );

      attach( cv.q4[0].v[0], m+54,4, cv.q4[0].v[1], cv.q4[0].v[2], cv.q4[0].v[3] );
      attach( cv.q4[0].v[0], m+56,4, cv.q4[0].v[1], cv.q4[0].v[2], cv.q4[0].v[3] );

      attach( cv.q4[1].v[0], m+55,4, cv.q4[1].v[1], cv.q4[1].v[2], cv.q4[1].v[3] );
      attach( cv.q4[1].v[0], m+57,4, cv.q4[1].v[1], cv.q4[1].v[2], cv.q4[1].v[3] );

      attach( cv.q4[2].v[0], m+58,4, cv.q4[2].v[1], cv.q4[2].v[2], cv.q4[2].v[3] );
      attach( cv.q4[2].v[0], m+60,4, cv.q4[2].v[1], cv.q4[2].v[2], cv.q4[2].v[3] );

      attach( cv.q4[3].v[0], m+59,4, cv.q4[3].v[1], cv.q4[3].v[2], cv.q4[3].v[3] );
      attach( cv.q4[3].v[0], m+61,4, cv.q4[3].v[1], cv.q4[3].v[2], cv.q4[3].v[3] );


      bz[ mask[0] ].n= true;
      bz[ mask[1] ].n= true;
      bz[ mask[2] ].n= true;
      bz[ mask[3] ].n= true;
      bz[ mask[4] ].n= true;

      return;
      
  }
