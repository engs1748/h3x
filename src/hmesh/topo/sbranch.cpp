#  include <hmesh/hmesh.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void hmesh_t::sbranch( REAL_3 vv, INT_ id, dir_t  d, INT_ *mask, bool flag )
  {

      INT_            i,k;
      INT_            n;
      INT_            i0,i1,i2,i3,i4,i5,i6,i7;

      INT_            x,v;

      INT_            v0,v1,v2,v3,v4,v5,v6,v7, w0,w1,w2,w3,w4,w5,w6,w7;
      INT_            x0,x1,x2,x3,x4,x5,x6,x7, y0,y1,y2,y3,y4,y5,y6,y7;

      INT_            f0,f1,f2,f3,f4,f5;
      INT_            g0,g1,g2,g3,g4,g5;

      INT_            d0=-1,d1=-1,d2=-1,d3=-1,d4=-1,d5=-1;
      INT_            s0=-1,s1=-1,s2=-1,s3=-1,s4=-1,s5=-1;
      INT_            p0=-1,p1=-1,p2=-1,p3=-1,p4=-1,p5=-1;

      REAL_3          q0,q1,q2,q3,q4,q5,q6,q7;
      REAL_3          r0,r1,r2,r3,r4,r5,r6,r7;

      INT_            top,bottom;
      bool            o;

      vtx_t           l;
 
      const REAL_     c1=1./9.,c2=2./9.,c3=4./9.;

      i= indx[id];
      assert( i > -1 );

      k= d.dir();
      assert( k ==2 );
      o= d.sign();
      assert( !o );

      i0= opp[ o][k][0]; i1= opp[ o][k][1]; i2= opp[ o][k][2]; i3= opp[ o][k][3];
      i4= opp[!o][k][0]; i5= opp[!o][k][1]; i6= opp[!o][k][2]; i7= opp[!o][k][3];

      x0= hx[i].x[i0]; x1= hx[i].x[i1]; x2= hx[i].x[i2]; x3= hx[i].x[i3]; 
      x4= hx[i].x[i4]; x5= hx[i].x[i5]; x6= hx[i].x[i6]; x7= hx[i].x[i7]; 

      v0= hx[i].v[i0]; v1= hx[i].v[i1]; v2= hx[i].v[i2]; v3= hx[i].v[i3]; 
      v4= hx[i].v[i4]; v5= hx[i].v[i5]; v6= hx[i].v[i6]; v7= hx[i].v[i7]; 

      q0[0]= hx[i].d[i0][0]; q0[1]= hx[i].d[i0][1]; q0[2]= hx[i].d[i0][k];
      q1[0]= hx[i].d[i1][0]; q1[1]= hx[i].d[i1][1]; q1[2]= hx[i].d[i1][k];
      q2[0]= hx[i].d[i2][0]; q2[1]= hx[i].d[i2][1]; q2[2]= hx[i].d[i2][k];
      q3[0]= hx[i].d[i3][0]; q3[1]= hx[i].d[i3][1]; q3[2]= hx[i].d[i3][k];
      q4[0]= hx[i].d[i4][0]; q4[1]= hx[i].d[i4][1]; q4[2]= hx[i].d[i4][k];
      q5[0]= hx[i].d[i5][0]; q5[1]= hx[i].d[i5][1]; q5[2]= hx[i].d[i5][k];
      q6[0]= hx[i].d[i6][0]; q6[1]= hx[i].d[i6][1]; q6[2]= hx[i].d[i6][k];
      q7[0]= hx[i].d[i7][0]; q7[1]= hx[i].d[i7][1]; q7[2]= hx[i].d[i7][k];

      f0= oqq[ o][k][0];
      f1= oqq[ o][k][1];
      f2= oqq[ o][k][2];
      f3= oqq[ o][k][3];
      f4= oqq[ o][k][4];
      f5= oqq[ o][k][5];

      g0= hx[i].q[f0].g;
      g1= hx[i].q[f1].g;
      g2= hx[i].q[f2].g;
      g3= hx[i].q[f3].g;
      g4= hx[i].q[f4].g;
      g5= hx[i].q[f5].g;

      d0= hx[i].q[f0].b;
      d1= hx[i].q[f1].b;
      d2= hx[i].q[f2].b;
      d3= hx[i].q[f3].b;
      d4= hx[i].q[f4].b;
      d5= hx[i].q[f5].b;

      if( g0 > -1 ){ p0= bz[g0].bo[d0].p; s0= bz[g0].bo[d0].s; }
      if( g1 > -1 ){ p1= bz[g1].bo[d1].p; s1= bz[g1].bo[d1].s; }
      if( g2 > -1 ){ p2= bz[g2].bo[d2].p; s2= bz[g2].bo[d2].s; }
      if( g3 > -1 ){ p3= bz[g3].bo[d3].p; s3= bz[g3].bo[d3].s; }
      if( g4 > -1 ){ p4= bz[g4].bo[d4].p; s4= bz[g4].bo[d4].s; }
      if( g5 > -1 ){ p5= bz[g5].bo[d5].p; s5= bz[g5].bo[d5].s; }

      assert( f0 > -1 );

      x= vx.n; vx.n+= 8; vx.resize( xdum );
      v= vt.n; vt.n+= 8; vt.resize( pdum );

      n= hx.n; hx.n+= 6; hx.resize( hdum );

      vx[x]= c3*vx[x0]+ c2*vx[x1]+ c2*vx[x2]+ c1*vx[x3]; y0=x++;
      vx[x]= c2*vx[x0]+ c3*vx[x1]+ c1*vx[x2]+ c2*vx[x3]; y1=x++;
      vx[x]= c2*vx[x0]+ c1*vx[x1]+ c3*vx[x2]+ c2*vx[x3]; y2=x++;
      vx[x]= c1*vx[x0]+ c2*vx[x1]+ c2*vx[x2]+ c3*vx[x3]; y3=x++;
      vx[x]= c3*vx[x4]+ c2*vx[x5]+ c2*vx[x6]+ c1*vx[x7]; y4=x++;
      vx[x]= c2*vx[x4]+ c3*vx[x5]+ c1*vx[x6]+ c2*vx[x7]; y5=x++;
      vx[x]= c2*vx[x4]+ c1*vx[x5]+ c3*vx[x6]+ c2*vx[x7]; y6=x++;
      vx[x]= c1*vx[x4]+ c2*vx[x5]+ c2*vx[x6]+ c3*vx[x7]; y7=x++;

      vx[y4]= 0.5*( vx[y4]+ vx[y0] );
      vx[y5]= 0.5*( vx[y5]+ vx[y1] );
      vx[y6]= 0.5*( vx[y6]+ vx[y2] );
      vx[y7]= 0.5*( vx[y7]+ vx[y3] );
 
      vt[v].id= v; w0= v++; 
      vt[v].id= v; w1= v++; 
      vt[v].id= v; w2= v++; 
      vt[v].id= v; w3= v++; 
      vt[v].id= v; w4= v++; 
      vt[v].id= v; w5= v++; 
      vt[v].id= v; w6= v++; 
      vt[v].id= v; w7= v++; 

      r0[0]= q0[0]; r0[1]= q0[1]; r0[2]= q0[2];
      r1[0]= q1[0]; r1[1]= q1[1]; r1[2]= q1[2];
      r2[0]= q2[0]; r2[1]= q2[1]; r2[2]= q2[2];
      r3[0]= q3[0]; r3[1]= q3[1]; r3[2]= q3[2];
      r4[0]= q4[0]; r4[1]= q4[1]; r4[2]= q4[2];
      r5[0]= q5[0]; r5[1]= q5[1]; r5[2]= q5[2];
      r6[0]= q6[0]; r6[1]= q6[1]; r6[2]= q6[2];
      r7[0]= q7[0]; r7[1]= q7[1]; r7[2]= q7[2];

      detach( g0,d0 ); attach( (MXGRP-1),i,f0,-1,-1,-1 ); 

      if( ::vol( vx[y0],vx[y1],vx[y2],vx[y3],vx[y4],vx[y5],vx[y6],vx[y7] ) > 0 )
     {
         hex( n+0, w0,w1,w2,w3, w4,w5,w6,w7, y0,y1,y2,y3, y4,y5,y6,y7  );
         hex( n+1, v0,w0,v2,w2, v4,w4,v6,w6, x0,y0,x2,y2, x4,y4,x6,y6  );
         hex( n+2, v0,v1,w0,w1, v4,v5,w4,w5, x0,x1,y0,y1, x4,x5,y4,y5  );
         hex( n+3, w2,w3,v2,v3, w6,w7,v6,v7, y2,y3,x2,x3, y6,y7,x6,x7  );
         hex( n+4, w1,v1,w3,v3, w5,v5,w7,v7, y1,x1,y3,x3, y5,x5,y7,x7  );
         hex( n+5, w4,w5,w6,w7, v4,v5,v6,v7, y4,y5,y6,y7, x4,x5,x6,x7  );

         hx[n+0].d[0][0]=r0[0]; hx[n+0].d[0][1]=r0[1]; hx[n+0].d[0][2]= r0[2];
         hx[n+0].d[1][0]=r1[0]; hx[n+0].d[1][1]=r1[1]; hx[n+0].d[1][2]= r1[2];
         hx[n+0].d[2][0]=r2[0]; hx[n+0].d[2][1]=r2[1]; hx[n+0].d[2][2]= r2[2];
         hx[n+0].d[3][0]=r3[0]; hx[n+0].d[3][1]=r3[1]; hx[n+0].d[3][2]= r3[2];
         hx[n+0].d[4][0]=r4[0]; hx[n+0].d[4][1]=r4[1]; hx[n+0].d[4][2]= r4[2];
         hx[n+0].d[5][0]=r5[0]; hx[n+0].d[5][1]=r5[1]; hx[n+0].d[5][2]= r5[2];
         hx[n+0].d[6][0]=r6[0]; hx[n+0].d[6][1]=r6[1]; hx[n+0].d[6][2]= r6[2];
         hx[n+0].d[7][0]=r7[0]; hx[n+0].d[7][1]=r7[1]; hx[n+0].d[7][2]= r7[2];

         hx[n+1].d[0][0]=q0[0]; hx[n+1].d[0][1]=q0[1]; hx[n+1].d[0][2]= q0[2];
         hx[n+1].d[1][0]=r0[0]; hx[n+1].d[1][1]=r0[1]; hx[n+1].d[1][2]= r0[2];
         hx[n+1].d[2][0]=q2[0]; hx[n+1].d[2][1]=q2[1]; hx[n+1].d[2][2]= q2[2];
         hx[n+1].d[3][0]=r2[0]; hx[n+1].d[3][1]=r2[1]; hx[n+1].d[3][2]= r2[2];
         hx[n+1].d[4][0]=q4[0]; hx[n+1].d[4][1]=q4[1]; hx[n+1].d[4][2]= q4[2];
         hx[n+1].d[5][0]=r4[0]; hx[n+1].d[5][1]=r4[1]; hx[n+1].d[5][2]= r4[2];
         hx[n+1].d[6][0]=q6[0]; hx[n+1].d[6][1]=q6[1]; hx[n+1].d[6][2]= q6[2];
         hx[n+1].d[7][0]=r6[0]; hx[n+1].d[7][1]=r6[1]; hx[n+1].d[7][2]= r6[2];

         hx[n+2].d[0][0]=q0[0]; hx[n+2].d[0][1]=q0[1]; hx[n+2].d[0][2]= q0[2];
         hx[n+2].d[1][0]=q1[0]; hx[n+2].d[1][1]=q1[1]; hx[n+2].d[1][2]= q1[2];
         hx[n+2].d[2][0]=r0[0]; hx[n+2].d[2][1]=r0[1]; hx[n+2].d[2][2]= r0[2];
         hx[n+2].d[3][0]=r1[0]; hx[n+2].d[3][1]=r1[1]; hx[n+2].d[3][2]= r1[2];
         hx[n+2].d[4][0]=q4[0]; hx[n+2].d[4][1]=q4[1]; hx[n+2].d[4][2]= q4[2];
         hx[n+2].d[5][0]=q5[0]; hx[n+2].d[5][1]=q5[1]; hx[n+2].d[5][2]= q5[2];
         hx[n+2].d[6][0]=r4[0]; hx[n+2].d[6][1]=r4[1]; hx[n+2].d[6][2]= r4[2];
         hx[n+2].d[7][0]=r5[0]; hx[n+2].d[7][1]=r5[1]; hx[n+2].d[7][2]= r5[2];

         hx[n+3].d[0][0]=r2[0]; hx[n+3].d[0][1]=r2[1]; hx[n+3].d[0][2]= r2[2];
         hx[n+3].d[1][0]=r3[0]; hx[n+3].d[1][1]=r3[1]; hx[n+3].d[1][2]= r3[2];
         hx[n+3].d[2][0]=q2[0]; hx[n+3].d[2][1]=q2[1]; hx[n+3].d[2][2]= q2[2];
         hx[n+3].d[3][0]=q3[0]; hx[n+3].d[3][1]=q3[1]; hx[n+3].d[3][2]= q3[2];
         hx[n+3].d[4][0]=r6[0]; hx[n+3].d[4][1]=r6[1]; hx[n+3].d[4][2]= r6[2];
         hx[n+3].d[5][0]=r7[0]; hx[n+3].d[5][1]=r7[1]; hx[n+3].d[5][2]= r7[2];
         hx[n+3].d[6][0]=q6[0]; hx[n+3].d[6][1]=q6[1]; hx[n+3].d[6][2]= q6[2];
         hx[n+3].d[7][0]=q7[0]; hx[n+3].d[7][1]=q7[1]; hx[n+3].d[7][2]= q7[2];

         hx[n+4].d[0][0]=r1[0]; hx[n+4].d[0][1]=r1[1]; hx[n+4].d[0][2]= r1[2];
         hx[n+4].d[1][0]=q1[0]; hx[n+4].d[1][1]=q1[1]; hx[n+4].d[1][2]= q1[2];
         hx[n+4].d[2][0]=r3[0]; hx[n+4].d[2][1]=r3[1]; hx[n+4].d[2][2]= r3[2];
         hx[n+4].d[3][0]=q3[0]; hx[n+4].d[3][1]=q3[1]; hx[n+4].d[3][2]= q3[2];
         hx[n+4].d[4][0]=r5[0]; hx[n+4].d[4][1]=r5[1]; hx[n+4].d[4][2]= r5[2];
         hx[n+4].d[5][0]=q5[0]; hx[n+4].d[5][1]=q5[1]; hx[n+4].d[5][2]= q5[2];
         hx[n+4].d[6][0]=r7[0]; hx[n+4].d[6][1]=r7[1]; hx[n+4].d[6][2]= r7[2];
         hx[n+4].d[7][0]=q7[0]; hx[n+4].d[7][1]=q7[1]; hx[n+4].d[7][2]= q7[2];

         hx[n+5].d[0][0]=r4[0]; hx[n+5].d[0][1]=r4[1]; hx[n+5].d[0][2]= r4[2];
         hx[n+5].d[1][0]=r5[0]; hx[n+5].d[1][1]=r5[1]; hx[n+5].d[1][2]= r5[2];
         hx[n+5].d[2][0]=r6[0]; hx[n+5].d[2][1]=r6[1]; hx[n+5].d[2][2]= r6[2];
         hx[n+5].d[3][0]=r7[0]; hx[n+5].d[3][1]=r7[1]; hx[n+5].d[3][2]= r7[2];
         hx[n+5].d[4][0]=q4[0]; hx[n+5].d[4][1]=q4[1]; hx[n+5].d[4][2]= q4[2];
         hx[n+5].d[5][0]=q5[0]; hx[n+5].d[5][1]=q5[1]; hx[n+5].d[5][2]= q5[2];
         hx[n+5].d[6][0]=q6[0]; hx[n+5].d[6][1]=q6[1]; hx[n+5].d[6][2]= q6[2];
         hx[n+5].d[7][0]=q7[0]; hx[n+5].d[7][1]=q7[1]; hx[n+5].d[7][2]= q7[2];

         bottom=4;
         top=   5;

     }
      else
     {
         hex( n+0, w4,w5,w6,w7, w0,w1,w2,w3, y4,y5,y6,y7, y0,y1,y2,y3 ); 
         hex( n+1, v4,w4,v6,w6, v0,w0,v2,w2, x4,y4,x6,y6, x0,y0,x2,y2 );
         hex( n+2, v4,v5,w4,w5, v0,v1,w0,w1, x4,x5,y4,y5, x0,x1,y0,y1 );
         hex( n+3, w6,w7,v6,v7, w2,w3,v2,v3, y6,y7,x6,x7, y2,y3,x2,x3 );
         hex( n+4, w5,v5,w7,v7, w1,v1,w3,v3, y5,x5,y7,x7, y1,x1,y3,x3 );
         hex( n+5, v4,v5,v6,v7, w4,w5,w6,w7, x4,x5,x6,x7, y4,y5,y6,y7 );

         hx[n+0].d[0][0]=q4[0]; hx[n+0].d[0][1]=q4[1]; hx[n+0].d[0][2]= q4[2];
         hx[n+0].d[1][0]=q5[0]; hx[n+0].d[1][1]=q5[1]; hx[n+0].d[1][2]= q5[2];
         hx[n+0].d[2][0]=q6[0]; hx[n+0].d[2][1]=q6[1]; hx[n+0].d[2][2]= q6[2];
         hx[n+0].d[3][0]=q7[0]; hx[n+0].d[3][1]=q7[1]; hx[n+0].d[3][2]= q7[2];
         hx[n+0].d[4][0]=q0[0]; hx[n+0].d[4][1]=q0[1]; hx[n+0].d[4][2]= q0[2];
         hx[n+0].d[5][0]=q1[0]; hx[n+0].d[5][1]=q1[1]; hx[n+0].d[5][2]= q1[2];
         hx[n+0].d[6][0]=q2[0]; hx[n+0].d[6][1]=q2[1]; hx[n+0].d[6][2]= q2[2];
         hx[n+0].d[7][0]=q3[0]; hx[n+0].d[7][1]=q3[1]; hx[n+0].d[7][2]= q3[2];

         hx[n+1].d[0][0]=q4[0]; hx[n+1].d[0][1]=q4[1]; hx[n+1].d[0][2]= q4[2];
         hx[n+1].d[1][0]=q4[0]; hx[n+1].d[1][1]=q4[1]; hx[n+1].d[1][2]= q4[2];
         hx[n+1].d[2][0]=q6[0]; hx[n+1].d[2][1]=q6[1]; hx[n+1].d[2][2]= q6[2];
         hx[n+1].d[3][0]=q6[0]; hx[n+1].d[3][1]=q6[1]; hx[n+1].d[3][2]= q6[2];
         hx[n+1].d[4][0]=q0[0]; hx[n+1].d[4][1]=q0[1]; hx[n+1].d[4][2]= q0[2];
         hx[n+1].d[5][0]=q0[0]; hx[n+1].d[5][1]=q0[1]; hx[n+1].d[5][2]= q0[2];
         hx[n+1].d[6][0]=q2[0]; hx[n+1].d[6][1]=q2[1]; hx[n+1].d[6][2]= q2[2];
         hx[n+1].d[7][0]=q2[0]; hx[n+1].d[7][1]=q2[1]; hx[n+1].d[7][2]= q2[2];

         hx[n+2].d[0][0]=q4[0]; hx[n+2].d[0][1]=q4[1]; hx[n+2].d[0][2]= q4[2];
         hx[n+2].d[1][0]=q5[0]; hx[n+2].d[1][1]=q5[1]; hx[n+2].d[1][2]= q5[2];
         hx[n+2].d[2][0]=q4[0]; hx[n+2].d[2][1]=q4[1]; hx[n+2].d[2][2]= q4[2];
         hx[n+2].d[3][0]=q5[0]; hx[n+2].d[3][1]=q5[1]; hx[n+2].d[3][2]= q5[2];
         hx[n+2].d[4][0]=q0[0]; hx[n+2].d[4][1]=q0[1]; hx[n+2].d[4][2]= q0[2];
         hx[n+2].d[5][0]=q1[0]; hx[n+2].d[5][1]=q1[1]; hx[n+2].d[5][2]= q1[2];
         hx[n+2].d[6][0]=q0[0]; hx[n+2].d[6][1]=q0[1]; hx[n+2].d[6][2]= q0[2];
         hx[n+2].d[7][0]=q1[0]; hx[n+2].d[7][1]=q1[1]; hx[n+2].d[7][2]= q1[2];

         hx[n+3].d[0][0]=q6[0]; hx[n+3].d[0][1]=q6[1]; hx[n+3].d[0][2]= q6[2];
         hx[n+3].d[1][0]=q7[0]; hx[n+3].d[1][1]=q7[1]; hx[n+3].d[1][2]= q7[2];
         hx[n+3].d[2][0]=q6[0]; hx[n+3].d[2][1]=q6[1]; hx[n+3].d[2][2]= q6[2];
         hx[n+3].d[3][0]=q7[0]; hx[n+3].d[3][1]=q7[1]; hx[n+3].d[3][2]= q7[2];
         hx[n+3].d[4][0]=q2[0]; hx[n+3].d[4][1]=q2[1]; hx[n+3].d[4][2]= q2[2];
         hx[n+3].d[5][0]=q3[0]; hx[n+3].d[5][1]=q3[1]; hx[n+3].d[5][2]= q3[2];
         hx[n+3].d[6][0]=q2[0]; hx[n+3].d[6][1]=q2[1]; hx[n+3].d[6][2]= q2[2];
         hx[n+3].d[7][0]=q3[0]; hx[n+3].d[7][1]=q3[1]; hx[n+3].d[7][2]= q3[2];

         hx[n+4].d[0][0]=q5[0]; hx[n+4].d[0][1]=q5[1]; hx[n+4].d[0][2]= q5[2];
         hx[n+4].d[1][0]=q5[0]; hx[n+4].d[1][1]=q5[1]; hx[n+4].d[1][2]= q5[2];
         hx[n+4].d[2][0]=q7[0]; hx[n+4].d[2][1]=q7[1]; hx[n+4].d[2][2]= q7[2];
         hx[n+4].d[3][0]=q7[0]; hx[n+4].d[3][1]=q7[1]; hx[n+4].d[3][2]= q7[2];
         hx[n+4].d[4][0]=q1[0]; hx[n+4].d[4][1]=q1[1]; hx[n+4].d[4][2]= q1[2];
         hx[n+4].d[5][0]=q1[0]; hx[n+4].d[5][1]=q1[1]; hx[n+4].d[5][2]= q1[2];
         hx[n+4].d[6][0]=q3[0]; hx[n+4].d[6][1]=q3[1]; hx[n+4].d[6][2]= q3[2];
         hx[n+4].d[7][0]=q3[0]; hx[n+4].d[7][1]=q3[1]; hx[n+4].d[7][2]= q3[2];

         hx[n+5].d[0][0]=NO_DCNTR; hx[n+5].d[0][1]=NO_DCNTR; hx[n+5].d[0][2]= NO_DCNTR;
         hx[n+5].d[1][0]=NO_DCNTR; hx[n+5].d[1][1]=NO_DCNTR; hx[n+5].d[1][2]= NO_DCNTR;
         hx[n+5].d[2][0]=NO_DCNTR; hx[n+5].d[2][1]=NO_DCNTR; hx[n+5].d[2][2]= NO_DCNTR;
         hx[n+5].d[3][0]=NO_DCNTR; hx[n+5].d[3][1]=NO_DCNTR; hx[n+5].d[3][2]= NO_DCNTR;
         hx[n+5].d[4][0]=NO_DCNTR; hx[n+5].d[4][1]=NO_DCNTR; hx[n+5].d[4][2]= NO_DCNTR;
         hx[n+5].d[5][0]=NO_DCNTR; hx[n+5].d[5][1]=NO_DCNTR; hx[n+5].d[5][2]= NO_DCNTR;
         hx[n+5].d[6][0]=NO_DCNTR; hx[n+5].d[6][1]=NO_DCNTR; hx[n+5].d[6][2]= NO_DCNTR;
         hx[n+5].d[7][0]=NO_DCNTR; hx[n+5].d[7][1]=NO_DCNTR; hx[n+5].d[7][2]= NO_DCNTR;

         bottom=5;
         top=   4;
     }


      force( n+0,top, n+5,bottom );
      force( n+0,0,   n+1,1 );
      force( n+0,2,   n+2,3 );
      force( n+0,3,   n+3,2 );
      force( n+0,1,   n+4,0 );

      force( n+1,2,   n+2,0 );
      force( n+2,1,   n+4,2 );
      force( n+4,3,   n+3,1 );
      force( n+3,0,   n+1,3 );

      force( n+1,top, n+5,0 );
      force( n+2,top, n+5,2 );
      force( n+4,top, n+5,1 );
      force( n+3,top, n+5,3 );

      if( g1 > -1 ){ force( n+5,top,-1,-1 ); }else{ replace( i,f1, n+5,top ); }
      if( g2 > -1 ){ force( n+1,0  ,-1,-1 ); }else{ replace( i,f2, n+1,0   ); }
      if( g3 > -1 ){ force( n+2,2  ,-1,-1 ); }else{ replace( i,f3, n+2,2   ); }
      if( g4 > -1 ){ force( n+3,3  ,-1,-1 ); }else{ replace( i,f4, n+3,3   ); }
      if( g5 > -1 ){ force( n+4,1  ,-1,-1 ); }else{ replace( i,f5, n+4,1   ); }

      if( !flag )
     {
         force( n+0,bottom,-1,-1 );
         force( n+1,bottom,-1,-1 );
         force( n+2,bottom,-1,-1 );
         force( n+3,bottom,-1,-1 ); 
         force( n+4,bottom,-1,-1 );

         if( g1 > -1 ){ detach( g1,d1 ); attach( (MXGRP-1),i,f1,-1,-1,-1 ); attach( g1,n+5,top,s1,p1,-1 ); }
         if( g2 > -1 ){ detach( g2,d2 ); attach( (MXGRP-1),i,f2,-1,-1,-1 ); attach( g2,n+1,0  ,s2,p2,-1 ); }
         if( g3 > -1 ){ detach( g3,d3 ); attach( (MXGRP-1),i,f3,-1,-1,-1 ); attach( g3,n+2,2  ,s3,p3,-1 ); }
         if( g4 > -1 ){ detach( g4,d4 ); attach( (MXGRP-1),i,f4,-1,-1,-1 ); attach( g4,n+3,3  ,s4,p4,-1 ); }
         if( g5 > -1 ){ detach( g5,d5 ); attach( (MXGRP-1),i,f5,-1,-1,-1 ); attach( g5,n+4,1  ,s5,p5,-1 ); }

     }
      else
     {
         l= ( vx[y4]+ vx[y5]+ vx[y6]+ vx[y7] )- ( vx[y0]+ vx[y1]+ vx[y2]+ vx[y3]  );
         l*= 0.25;

         if( (fabs(vv[0])+fabs(vv[1])+ fabs(vv[2]))>0 )
        {
            l[0]= -vv[0];
            l[1]= -vv[1];
            l[2]= -vv[2];
        }

         y4= y0;
         y5= y1;
         y6= y2;
         y7= y3;

         w4= w0;
         w5= w1;
         w6= w2;
         w7= w3;

         x4= x0;
         x5= x1;
         x6= x2;
         x7= x3;

         v4= v0;
         v5= v1;
         v6= v2;
         v7= v3;

         x= vx.n; vx.n+= 8; vx.resize( xdum );
         v= vt.n; vt.n+= 8; vt.resize( pdum );
         
         vx[x]= vx[y4]- l; y0=x++;
         vx[x]= vx[y5]- l; y1=x++;
         vx[x]= vx[y6]- l; y2=x++;
         vx[x]= vx[y7]- l; y3=x++;

         vx[x]= vx[x4]- l; x0=x++;
         vx[x]= vx[x5]- l; x1=x++;
         vx[x]= vx[x6]- l; x2=x++;
         vx[x]= vx[x7]- l; x3=x++;

         vt[v].id= v; w0= v++; 
         vt[v].id= v; w1= v++; 
         vt[v].id= v; w2= v++; 
         vt[v].id= v; w3= v++; 

         vt[v].id= v; v0= v++; 
         vt[v].id= v; v1= v++; 
         vt[v].id= v; v2= v++; 
         vt[v].id= v; v3= v++; 

         r4[0]= r0[0]; r4[1]= r0[1]; r4[2]= r0[2];
         r5[0]= r1[0]; r5[1]= r1[1]; r5[2]= r1[2];
         r6[0]= r2[0]; r6[1]= r2[1]; r6[2]= r2[2];
         r7[0]= r3[0]; r7[1]= r3[1]; r7[2]= r3[2];
                                            
         q4[0]= q0[0]; q4[1]= q0[1]; q4[2]= q0[2];
         q5[0]= q1[0]; q5[1]= q1[1]; q5[2]= q1[2];
         q6[0]= q2[0]; q6[1]= q2[1]; q6[2]= q2[2];
         q7[0]= q3[0]; q7[1]= q3[1]; q7[2]= q3[2];


         INT_ n0= n;
         n= hx.n; hx.n+= 5; hx.resize( hdum );

         if( bottom ==4 )
        {
            hex( n+0, w0,w1,w2,w3, w4,w5,w6,w7, y0,y1,y2,y3, y4,y5,y6,y7  );  printf( "%d <-----\n", hx[n+0].id );

            printf( "% 9.3e % 9.3e % 9.3e\n", vx[y0][0],vx[y0][1],vx[y0][2] );
            printf( "% 9.3e % 9.3e % 9.3e\n", vx[y1][0],vx[y1][1],vx[y1][2] );
            printf( "% 9.3e % 9.3e % 9.3e\n", vx[y2][0],vx[y2][1],vx[y2][2] );
            printf( "% 9.3e % 9.3e % 9.3e\n", vx[y3][0],vx[y3][1],vx[y3][2] );
            printf( "% 9.3e % 9.3e % 9.3e\n", vx[y4][0],vx[y4][1],vx[y4][2] );
            printf( "% 9.3e % 9.3e % 9.3e\n", vx[y5][0],vx[y5][1],vx[y5][2] );
            printf( "% 9.3e % 9.3e % 9.3e\n", vx[y6][0],vx[y6][1],vx[y6][2] );
            printf( "% 9.3e % 9.3e % 9.3e\n", vx[y7][0],vx[y7][1],vx[y7][2] );

            hex( n+1, v0,w0,v2,w2, v4,w4,v6,w6, x0,y0,x2,y2, x4,y4,x6,y6  );
            hex( n+2, v0,v1,w0,w1, v4,v5,w4,w5, x0,x1,y0,y1, x4,x5,y4,y5  );
            hex( n+3, w2,w3,v2,v3, w6,w7,v6,v7, y2,y3,x2,x3, y6,y7,x6,x7  );
            hex( n+4, w1,v1,w3,v3, w5,v5,w7,v7, y1,x1,y3,x3, y5,x5,y7,x7  );

            hx[n+0].d[0][0]=NO_DCNTR; hx[n+0].d[0][1]=NO_DCNTR; hx[n+0].d[0][2]= NO_DCNTR;
            hx[n+0].d[1][0]=NO_DCNTR; hx[n+0].d[1][1]=NO_DCNTR; hx[n+0].d[1][2]= NO_DCNTR;
            hx[n+0].d[2][0]=NO_DCNTR; hx[n+0].d[2][1]=NO_DCNTR; hx[n+0].d[2][2]= NO_DCNTR;
            hx[n+0].d[3][0]=NO_DCNTR; hx[n+0].d[3][1]=NO_DCNTR; hx[n+0].d[3][2]= NO_DCNTR;
            hx[n+0].d[4][0]=   r4[0]; hx[n+0].d[4][1]=   r4[1]; hx[n+0].d[4][2]=    r4[2];
            hx[n+0].d[5][0]=   r5[0]; hx[n+0].d[5][1]=   r5[1]; hx[n+0].d[5][2]=    r5[2];
            hx[n+0].d[6][0]=   r6[0]; hx[n+0].d[6][1]=   r6[1]; hx[n+0].d[6][2]=    r6[2];
            hx[n+0].d[7][0]=   r7[0]; hx[n+0].d[7][1]=   r7[1]; hx[n+0].d[7][2]=    r7[2];

            hx[n+1].d[0][0]=NO_DCNTR; hx[n+1].d[0][1]=NO_DCNTR; hx[n+1].d[0][2]= NO_DCNTR;
            hx[n+1].d[1][0]=NO_DCNTR; hx[n+1].d[1][1]=NO_DCNTR; hx[n+1].d[1][2]= NO_DCNTR;
            hx[n+1].d[2][0]=NO_DCNTR; hx[n+1].d[2][1]=NO_DCNTR; hx[n+1].d[2][2]= NO_DCNTR;
            hx[n+1].d[3][0]=NO_DCNTR; hx[n+1].d[3][1]=NO_DCNTR; hx[n+1].d[3][2]= NO_DCNTR;
            hx[n+1].d[4][0]=   q4[0]; hx[n+1].d[4][1]=   q4[1]; hx[n+1].d[4][2]=    q4[2];
            hx[n+1].d[5][0]=   r4[0]; hx[n+1].d[5][1]=   r4[1]; hx[n+1].d[5][2]=    r4[2];
            hx[n+1].d[6][0]=   q6[0]; hx[n+1].d[6][1]=   q6[1]; hx[n+1].d[6][2]=    q6[2];
            hx[n+1].d[7][0]=   r6[0]; hx[n+1].d[7][1]=   r6[1]; hx[n+1].d[7][2]=    r6[2];

            hx[n+2].d[0][0]=NO_DCNTR; hx[n+2].d[0][1]=NO_DCNTR; hx[n+2].d[0][2]= NO_DCNTR;
            hx[n+2].d[1][0]=NO_DCNTR; hx[n+2].d[1][1]=NO_DCNTR; hx[n+2].d[1][2]= NO_DCNTR;
            hx[n+2].d[2][0]=NO_DCNTR; hx[n+2].d[2][1]=NO_DCNTR; hx[n+2].d[2][2]= NO_DCNTR;
            hx[n+2].d[3][0]=NO_DCNTR; hx[n+2].d[3][1]=NO_DCNTR; hx[n+2].d[3][2]= NO_DCNTR;
            hx[n+2].d[4][0]=   q4[0]; hx[n+2].d[4][1]=   q4[1]; hx[n+2].d[4][2]=    q4[2];
            hx[n+2].d[5][0]=   q5[0]; hx[n+2].d[5][1]=   q5[1]; hx[n+2].d[5][2]=    q5[2];
            hx[n+2].d[6][0]=   r4[0]; hx[n+2].d[6][1]=   r4[1]; hx[n+2].d[6][2]=    r4[2];
            hx[n+2].d[7][0]=   r5[0]; hx[n+2].d[7][1]=   r5[1]; hx[n+2].d[7][2]=    r5[2];

            hx[n+3].d[0][0]=NO_DCNTR; hx[n+3].d[0][1]=NO_DCNTR; hx[n+3].d[0][2]= NO_DCNTR;
            hx[n+3].d[1][0]=NO_DCNTR; hx[n+3].d[1][1]=NO_DCNTR; hx[n+3].d[1][2]= NO_DCNTR;
            hx[n+3].d[2][0]=NO_DCNTR; hx[n+3].d[2][1]=NO_DCNTR; hx[n+3].d[2][2]= NO_DCNTR;
            hx[n+3].d[3][0]=NO_DCNTR; hx[n+3].d[3][1]=NO_DCNTR; hx[n+3].d[3][2]= NO_DCNTR;
            hx[n+3].d[4][0]=   r6[0]; hx[n+3].d[4][1]=   r6[1]; hx[n+3].d[4][2]=    r6[2];
            hx[n+3].d[5][0]=   r7[0]; hx[n+3].d[5][1]=   r7[1]; hx[n+3].d[5][2]=    r7[2];
            hx[n+3].d[6][0]=   q6[0]; hx[n+3].d[6][1]=   q6[1]; hx[n+3].d[6][2]=    q6[2];
            hx[n+3].d[7][0]=   q7[0]; hx[n+3].d[7][1]=   q7[1]; hx[n+3].d[7][2]=    q7[2];

            hx[n+4].d[0][0]=NO_DCNTR; hx[n+4].d[0][1]=NO_DCNTR; hx[n+4].d[0][2]= NO_DCNTR;
            hx[n+4].d[1][0]=NO_DCNTR; hx[n+4].d[1][1]=NO_DCNTR; hx[n+4].d[1][2]= NO_DCNTR;
            hx[n+4].d[2][0]=NO_DCNTR; hx[n+4].d[2][1]=NO_DCNTR; hx[n+4].d[2][2]= NO_DCNTR;
            hx[n+4].d[3][0]=NO_DCNTR; hx[n+4].d[3][1]=NO_DCNTR; hx[n+4].d[3][2]= NO_DCNTR;
            hx[n+4].d[4][0]=   r5[0]; hx[n+4].d[4][1]=   r5[1]; hx[n+4].d[4][2]=    r5[2];
            hx[n+4].d[5][0]=   q5[0]; hx[n+4].d[5][1]=   q5[1]; hx[n+4].d[5][2]=    q5[2];
            hx[n+4].d[6][0]=   r7[0]; hx[n+4].d[6][1]=   r7[1]; hx[n+4].d[6][2]=    r7[2];
            hx[n+4].d[7][0]=   q7[0]; hx[n+4].d[7][1]=   q7[1]; hx[n+4].d[7][2]=    q7[2];

            printf( "this one?\n" );
        }
         else
        {
            hex( n+0, w4,w5,w6,w7, w0,w1,w2,w3, y4,y5,y6,y7, y0,y1,y2,y3 ); 

            hex( n+1, v4,w4,v6,w6, v0,w0,v2,w2, x4,y4,x6,y6, x0,y0,x2,y2 );
            hex( n+2, v4,v5,w4,w5, v0,v1,w0,w1, x4,x5,y4,y5, x0,x1,y0,y1 );
            hex( n+3, w6,w7,v6,v7, w2,w3,v2,v3, y6,y7,x6,x7, y2,y3,x2,x3 );
            hex( n+4, w5,v5,w7,v7, w1,v1,w3,v3, y5,x5,y7,x7, y1,x1,y3,x3 );

            hx[n+0].d[0][0]=q0[0]; hx[n+0].d[0][1]=q0[1]; hx[n+0].d[0][2]= q0[2];
            hx[n+0].d[1][0]=q1[0]; hx[n+0].d[1][1]=q1[1]; hx[n+0].d[1][2]= q1[2];
            hx[n+0].d[2][0]=q2[0]; hx[n+0].d[2][1]=q2[1]; hx[n+0].d[2][2]= q2[2];
            hx[n+0].d[3][0]=q3[0]; hx[n+0].d[3][1]=q3[1]; hx[n+0].d[3][2]= q3[2];
            hx[n+0].d[4][0]=q4[0]; hx[n+0].d[4][1]=q4[1]; hx[n+0].d[4][2]= q4[2];
            hx[n+0].d[5][0]=q5[0]; hx[n+0].d[5][1]=q5[1]; hx[n+0].d[5][2]= q5[2];
            hx[n+0].d[6][0]=q6[0]; hx[n+0].d[6][1]=q6[1]; hx[n+0].d[6][2]= q6[2];
            hx[n+0].d[7][0]=q7[0]; hx[n+0].d[7][1]=q7[1]; hx[n+0].d[7][2]= q7[2];

            hx[n+1].d[0][0]=q0[0]; hx[n+1].d[0][1]=q0[1]; hx[n+1].d[0][2]= q0[2];
            hx[n+1].d[1][0]=q0[0]; hx[n+1].d[1][1]=q0[1]; hx[n+1].d[1][2]= q0[2];
            hx[n+1].d[2][0]=q2[0]; hx[n+1].d[2][1]=q2[1]; hx[n+1].d[2][2]= q2[2];
            hx[n+1].d[3][0]=q2[0]; hx[n+1].d[3][1]=q2[1]; hx[n+1].d[3][2]= q2[2];
            hx[n+1].d[4][0]=q4[0]; hx[n+1].d[4][1]=q4[1]; hx[n+1].d[4][2]= q4[2];
            hx[n+1].d[5][0]=q4[0]; hx[n+1].d[5][1]=q4[1]; hx[n+1].d[5][2]= q4[2];
            hx[n+1].d[6][0]=q6[0]; hx[n+1].d[6][1]=q6[1]; hx[n+1].d[6][2]= q6[2];
            hx[n+1].d[7][0]=q6[0]; hx[n+1].d[7][1]=q6[1]; hx[n+1].d[7][2]= q6[2];

            hx[n+2].d[0][0]=q0[0]; hx[n+2].d[0][1]=q0[1]; hx[n+2].d[0][2]= q0[2];
            hx[n+2].d[1][0]=q1[0]; hx[n+2].d[1][1]=q1[1]; hx[n+2].d[1][2]= q1[2];
            hx[n+2].d[2][0]=q0[0]; hx[n+2].d[2][1]=q0[1]; hx[n+2].d[2][2]= q0[2];
            hx[n+2].d[3][0]=q1[0]; hx[n+2].d[3][1]=q1[1]; hx[n+2].d[3][2]= q1[2];
            hx[n+2].d[4][0]=q4[0]; hx[n+2].d[4][1]=q4[1]; hx[n+2].d[4][2]= q4[2];
            hx[n+2].d[5][0]=q5[0]; hx[n+2].d[5][1]=q5[1]; hx[n+2].d[5][2]= q5[2];
            hx[n+2].d[6][0]=q4[0]; hx[n+2].d[6][1]=q4[1]; hx[n+2].d[6][2]= q4[2];
            hx[n+2].d[7][0]=q5[0]; hx[n+2].d[7][1]=q5[1]; hx[n+2].d[7][2]= q5[2];

            hx[n+3].d[0][0]=q2[0]; hx[n+3].d[0][1]=q2[1]; hx[n+3].d[0][2]= q2[2];
            hx[n+3].d[1][0]=q3[0]; hx[n+3].d[1][1]=q3[1]; hx[n+3].d[1][2]= q3[2];
            hx[n+3].d[2][0]=q2[0]; hx[n+3].d[2][1]=q2[1]; hx[n+3].d[2][2]= q2[2];
            hx[n+3].d[3][0]=q3[0]; hx[n+3].d[3][1]=q3[1]; hx[n+3].d[3][2]= q3[2];
            hx[n+3].d[4][0]=q6[0]; hx[n+3].d[4][1]=q6[1]; hx[n+3].d[4][2]= q6[2];
            hx[n+3].d[5][0]=q7[0]; hx[n+3].d[5][1]=q7[1]; hx[n+3].d[5][2]= q7[2];
            hx[n+3].d[6][0]=q6[0]; hx[n+3].d[6][1]=q6[1]; hx[n+3].d[6][2]= q6[2];
            hx[n+3].d[7][0]=q7[0]; hx[n+3].d[7][1]=q7[1]; hx[n+3].d[7][2]= q7[2];

            hx[n+4].d[0][0]=q1[0]; hx[n+4].d[0][1]=q1[1]; hx[n+4].d[0][2]= q1[2];
            hx[n+4].d[1][0]=q1[0]; hx[n+4].d[1][1]=q1[1]; hx[n+4].d[1][2]= q1[2];
            hx[n+4].d[2][0]=q3[0]; hx[n+4].d[2][1]=q3[1]; hx[n+4].d[2][2]= q3[2];
            hx[n+4].d[3][0]=q3[0]; hx[n+4].d[3][1]=q3[1]; hx[n+4].d[3][2]= q3[2];
            hx[n+4].d[4][0]=q5[0]; hx[n+4].d[4][1]=q5[1]; hx[n+4].d[4][2]= q5[2];
            hx[n+4].d[5][0]=q5[0]; hx[n+4].d[5][1]=q5[1]; hx[n+4].d[5][2]= q5[2];
            hx[n+4].d[6][0]=q7[0]; hx[n+4].d[6][1]=q7[1]; hx[n+4].d[6][2]= q7[2];
            hx[n+4].d[7][0]=q7[0]; hx[n+4].d[7][1]=q7[1]; hx[n+4].d[7][2]= q7[2];
        }

         force( n0+0,bottom, n+0,top );
         force( n0+1,bottom, n+1,top );
         force( n0+2,bottom, n+2,top );
         force( n0+3,bottom, n+3,top );
         force( n0+4,bottom, n+4,top );

         force( n+1,1, n+0,0 );
         force( n+2,3, n+0,2 );
         force( n+3,2, n+0,3 );
         force( n+4,0, n+0,1 );

         force( n+1,2, n+2,0 );
         force( n+2,1, n+4,2 );
         force( n+1,3, n+3,0 );
         force( n+3,1, n+4,3 );

         force( n+1,0, -1,-1 ); 
         force( n+2,2, -1,-1 ); 
         force( n+3,3, -1,-1 ); 
         force( n+4,1, -1,-1 ); 

         force( n+0,bottom,-1,-1 );
         force( n+1,bottom,-1,-1 );
         force( n+2,bottom,-1,-1 );
         force( n+3,bottom,-1,-1 ); 
         force( n+4,bottom,-1,-1 );

         attach( mask[1],n+1,0, -1,-1,-1 );
         attach( mask[2],n+4,1, -1,-1,-1 );
         attach( mask[3],n+2,2, -1,-1,-1 );
         attach( mask[4],n+3,3, -1,-1,-1 );

         if( g1 > -1 ){ detach( g1,d1 ); attach( (MXGRP-1),i,f1,-1,-1,-1 ); attach( g1,n0+5,top,s1,p1,-1 ); }
         if( g2 > -1 ){ detach( g2,d2 ); attach( (MXGRP-1),i,f2,-1,-1,-1 ); attach( g2,n0+1,0  ,s2,p2,-1 ); }
         if( g3 > -1 ){ detach( g3,d3 ); attach( (MXGRP-1),i,f3,-1,-1,-1 ); attach( g3,n0+2,2  ,s3,p3,-1 ); }
         if( g4 > -1 ){ detach( g4,d4 ); attach( (MXGRP-1),i,f4,-1,-1,-1 ); attach( g4,n0+3,3  ,s4,p4,-1 ); }
         if( g5 > -1 ){ detach( g5,d5 ); attach( (MXGRP-1),i,f5,-1,-1,-1 ); attach( g5,n0+4,1  ,s5,p5,-1 ); }
     }

      attach( mask[0],n+0,bottom,s0,p0,-1 );
      attach( mask[0],n+1,bottom,s0,p0,-1 );
      attach( mask[0],n+2,bottom,s0,p0,-1 );
      attach( mask[0],n+3,bottom,s0,p0,-1 ); 
      attach( mask[0],n+4,bottom,s0,p0,-1 );

      trimb();

      return;
  }

