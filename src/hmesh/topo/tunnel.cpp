
#  include <hmesh/hmesh.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void hmesh_t::tunnel( INT_ k0, frme_t a0, VINT_ &list )
  {

      frme_t                  a;
      dir_t                   g;
      wsize_t<dir_t,3,DLEN_H> d;
      INT_                    i,j,k,n;
      INT_                    x,v;
      INT_                    i0,i1,i2,i3,i4,i5,i6,i7;
      INT_                    v0,v1,v2,v3,v4,v5,v6,v7, w0,w1,w2,w3,w4,w5,w6,w7;
      INT_                    x0,x1,x2,x3,x4,x5,x6,x7, y0,y1,y2,y3,y4,y5,y6,y7;
      INT_                    f0,f1,f2,f3,f4,f5;
      INT_                    g0,g1,g2,g3,g4,g5;

      INT_                       s1=-1,s2=-1,s3=-1,s4=-1,s5=-1;
      INT_                       p1=-1,p2=-1,p3=-1,p4=-1,p5=-1;
      INT_                       b1=-1,b2=-1,b3=-1,b4=-1,b5=-1;

      INT_                    n0;
      VINT_                   mk(hx.n,-1);

      vtx_t                   z[4];

      const REAL_            c1=2./3.;
      const REAL_            c2=1./3.;

      a[0]= vec( a0[1],a0[2] );
      a[1]= a0[1];
      a[2]= a0[2];

      i= indx[k0];
      mk[i]=0;
      assert( i > -1 );

      assert( list.n==0 );
 
      do
     {
         j=   (d.n)++;    d.resize(g);
         d[j][0]= a[0];
         d[j][1]= a[1];
         d[j][2]= a[2];

         j= (list.n)++;list.resize(-1);
         list[j]= i;

     }while( walk(i,a,mk) );

      printf( "will tunnel through %d hex\n", list.n );

// split hex i0

      i= indx[k0];

      i0= coor( !(d[0][0]),!(d[0][1]),!(d[0][2]) );
      i1= coor( !(d[0][0]),!(d[0][1]), (d[0][2]) );
      i2= coor(  (d[0][0]),!(d[0][1]),!(d[0][2]) );
      i3= coor(  (d[0][0]),!(d[0][1]), (d[0][2]) );
      i4= coor( !(d[0][0]), (d[0][1]),!(d[0][2]) );
      i5= coor( !(d[0][0]), (d[0][1]), (d[0][2]) );
      i6= coor(  (d[0][0]), (d[0][1]),!(d[0][2]) );
      i7= coor(  (d[0][0]), (d[0][1]), (d[0][2]) );

      v0= hx[i].v[i0]; v1= hx[i].v[i1]; v2= hx[i].v[i2]; v3= hx[i].v[i3]; 
      v4= hx[i].v[i4]; v5= hx[i].v[i5]; v6= hx[i].v[i6]; v7= hx[i].v[i7]; 

      x0= hx[i].x[i0]; x1= hx[i].x[i1]; x2= hx[i].x[i2]; x3= hx[i].x[i3]; 
      x4= hx[i].x[i4]; x5= hx[i].x[i5]; x6= hx[i].x[i6]; x7= hx[i].x[i7]; 

/*    g0= hx[i].face( !(d[0][2]) );
      g1= hx[i].face(   d[0][2]  );
      g2= hx[i].face( !(d[0][0]) );
      g3= hx[i].face(   d[0][0]  );
      g4= hx[i].face( !(d[0][1]) ); 
      g5= hx[i].face(   d[0][1]  );*/

      g0= face( !(d[0][2]) );
      g1= face(   d[0][2]  );
      g2= face( !(d[0][0]) );
      g3= face(   d[0][0]  );
      g4= face( !(d[0][1]) ); 
      g5= face(   d[0][1]  );

      f0= hx[i].q[g0].g; 
      f1= hx[i].q[g1].g; 
      f2= hx[i].q[g2].g; 
      f3= hx[i].q[g3].g; 
      f4= hx[i].q[g4].g; 
      f5= hx[i].q[g5].g; 

//    b0= hx[i].q[g0].b; 
      b1= hx[i].q[g1].b; 
      b2= hx[i].q[g2].b; 
      b3= hx[i].q[g3].b; 
      b4= hx[i].q[g4].b; 
      b5= hx[i].q[g5].b; 


//    if( f0 > -1 ){ s0= bz[f0].bo[b0].s; p0= bz[f0].bo[b0].p; }
      if( f1 > -1 ){ s1= bz[f1].bo[b1].s; p1= bz[f1].bo[b1].p; }
      if( f2 > -1 ){ s2= bz[f2].bo[b2].s; p2= bz[f2].bo[b2].p; }
      if( f3 > -1 ){ s3= bz[f3].bo[b3].s; p3= bz[f3].bo[b3].p; }
      if( f4 > -1 ){ s4= bz[f4].bo[b4].s; p4= bz[f4].bo[b4].p; }
      if( f5 > -1 ){ s5= bz[f5].bo[b5].s; p5= bz[f5].bo[b5].p; }

      x= vx.n; vx.n+= 8; vx.resize(xdum );
      v= vt.n; vt.n+= 8; vt.resize(pdum );

      z[0]= 0.5*( vx[x0]+ vx[x1] );
      z[1]= 0.5*( vx[x2]+ vx[x3] );
      z[2]= 0.5*( vx[x4]+ vx[x5] );
      z[3]= 0.5*( vx[x6]+ vx[x7] );

      vx[x]= c1*z[0]+ c2*z[1];                           y0=x++;
      vx[x]= c2*z[0]+ c1*z[1];                           y2=x++;

      vx[x]= 0.5*( c1*z[0]+ c2*z[1]+ c1*z[2]+ c2*z[3] ); y4=x++;
      vx[x]= 0.5*( c2*z[0]+ c1*z[1]+ c2*z[2]+ c1*z[3] ); y6=x++;
  
      z[0]= vx[x1];
      z[1]= vx[x3];
      z[2]= vx[x5];
      z[3]= vx[x7];
  
      vx[x]=       c1*z[0]+ c2*z[1];                     y1=x++;
      vx[x]=       c2*z[0]+ c1*z[1];                     y3=x++;
      vx[x]= 0.5*( c1*z[0]+ c2*z[1]+ c1*z[2]+ c2*z[3] ); y5=x++;
      vx[x]= 0.5*( c2*z[0]+ c1*z[1]+ c2*z[2]+ c1*z[3] ); y7=x++;

      vt[v].id= v; w0= v++;
      vt[v].id= v; w2= v++;
      vt[v].id= v; w4= v++;
      vt[v].id= v; w6= v++;
      vt[v].id= v; w1= v++;
      vt[v].id= v; w3= v++;
      vt[v].id= v; w5= v++;
      vt[v].id= v; w7= v++;

      n=  hx.n; hx.n+=5; hx.resize( hdum );

      hex( n+0, w0,w1,w2,w3, w4,w5,w6,w7,  y0,y1,y2,y3, y4,y5,y6,y7 );
      hex( n+1, w2,w3,v2,v3, w6,w7,v6,v7,  y2,y3,x2,x3, y6,y7,x6,x7 );
      hex( n+2, w4,w5,w6,w7, v4,v5,v6,v7,  y4,y5,y6,y7, x4,x5,x6,x7 );
      hex( n+3, v0,v1,w0,w1, v4,v5,w4,w5,  x0,x1,y0,y1, x4,x5,y4,y5 );
      hex( n+4, v0,w0,v2,w2, v4,w4,v6,w6,  x0,y0,x2,y2, x4,y4,x6,y6 );

      force( n+0,4,-1,-1 );
      force( n+1,4,-1,-1 );
      force( n+3,4,-1,-1 );   
      force( n+4,4,-1,-1 );

      force( n+0,1,-1,-1 );
      force( n+1,1,-1,-1 );
      force( n+2,1,-1,-1 );
      force( n+3,1,-1,-1 );

      force( n+0,5, n+2,4 );
      force( n+0,3, n+1,2 );
      force( n+0,2, n+3,3 );

      force( n+2,3, n+1,5 );
      force( n+2,2, n+3,5 );

      force( n+0,0, n+4,1 );
      force( n+1,0, n+4,3 );
      force( n+2,0, n+4,5 );
      force( n+3,0, n+4,2 );

      if( f0 > -1 ){ force( n+4,0,-1,-1 ); }else{ replace( i,g0,n+4,0 ); }
      if( f2 > -1 ){ force( n+3,2,-1,-1 ); }else{ replace( i,g2,n+3,2 ); }
      if( f3 > -1 ){ force( n+1,3,-1,-1 ); }else{ replace( i,g3,n+1,3 ); }
      if( f5 > -1 ){ force( n+2,5,-1,-1 ); }else{ replace( i,g5,n+2,5 ); }

      detach( f4,b4 );
      attach( (MXGRP-1),i,g4,-1,-1,-1 );

      attach( f4,n+0,4,s4,p4,-1 );
      attach( f4,n+1,4,s4,p4,-1 );
      attach( f4,n+3,4,s4,p4,-1 ); 
      attach( f4,n+4,4,s4,p4,-1 );

      if( f0 > -1 ){ attach( f0,n+4,0,-1,-1,-1 ); }
      if( f2 > -1 ){ attach( f2,n+3,2,s2,p2,-1 ); }
      if( f3 > -1 ){ attach( f3,n+1,3,s3,p3,-1 ); }
      if( f5 > -1 ){ attach( f5,n+2,5,s5,p5,-1 ); }

      n0= n;
  
      for( k=1;k<list.n;k++ )
     {

         assert( f1 == -1 );

         i= list[k];

         i0= coor( !(d[0][0]),!(d[0][1]),!(d[0][2]) );
         i1= coor( !(d[0][0]),!(d[0][1]), (d[0][2]) );
         i2= coor(  (d[0][0]),!(d[0][1]),!(d[0][2]) );
         i3= coor(  (d[0][0]),!(d[0][1]), (d[0][2]) );
         i4= coor( !(d[0][0]), (d[0][1]),!(d[0][2]) );
         i5= coor( !(d[0][0]), (d[0][1]), (d[0][2]) );
         i6= coor(  (d[0][0]), (d[0][1]),!(d[0][2]) );
         i7= coor(  (d[0][0]), (d[0][1]), (d[0][2]) );
            
         assert( x3 == hx[i].x[i2] );
         assert( x5 == hx[i].x[i4] );
         assert( x7 == hx[i].x[i6] );

         x0= x1; 
         x2= x3; 
         x4= x5; 
         x6= x7;
         y0= y1; 
         y2= y3; 
         y4= y5; 
         y6= y7;

         x= vx.n; vx.n+= 4; vx.resize( xdum );

         v0= v1; v2= v3; v4= v5; v6= v7;
         w0= w1; w2= w3; w4= w5; w6= w7;

/*       g1= hx[i].face(   d[0][2]  );
         g2= hx[i].face( !(d[0][0]) );
         g3= hx[i].face(   d[0][0]  );
         g4= hx[i].face( !(d[0][1]) );
         g5= hx[i].face(   d[0][1]  );*/

         g1= face(   d[0][2]  );
         g2= face( !(d[0][0]) );
         g3= face(   d[0][0]  );
         g4= face( !(d[0][1]) );
         g5= face(   d[0][1]  );

         f1= hx[i].q[g1].g; 
         f2= hx[i].q[g2].g; 
         f3= hx[i].q[g3].g; 
         f4= hx[i].q[g4].g; 
         f5= hx[i].q[g5].g; 

         b1= hx[i].q[g1].b; 
         b2= hx[i].q[g2].b; 
         b3= hx[i].q[g3].b; 
         b4= hx[i].q[g4].b; 
         b5= hx[i].q[g5].b; 

         if( f1 > -1 ){ s1= bz[f1].bo[b1].s; p1= bz[f1].bo[b1].p; }
         if( f2 > -1 ){ s2= bz[f2].bo[b2].s; p2= bz[f2].bo[b2].p; }
         if( f3 > -1 ){ s3= bz[f3].bo[b3].s; p3= bz[f3].bo[b3].p; }
         if( f4 > -1 ){ s4= bz[f4].bo[b4].s; p4= bz[f4].bo[b4].p; }
         if( f5 > -1 ){ s5= bz[f5].bo[b5].s; p5= bz[f5].bo[b5].p; }

         x1= hx[i].x[i1]; 
         x3= hx[i].x[i3]; 
         x5= hx[i].x[i5]; 
         x7= hx[i].x[i7]; 

         v1= hx[i].v[i1]; 
         v3= hx[i].v[i3]; 
         v5= hx[i].v[i5]; 
         v7= hx[i].v[i7]; 

         z[0]= vx[x1];
         z[1]= vx[x3];
         z[2]= vx[x5];
         z[3]= vx[x7];
  
         vx[x]= c1*z[0]+ c2*z[1]; y1=x++;
         vx[x]= c2*z[0]+ c1*z[1]; y3=x++;
  
         vx[x]= 0.5*( c1*z[0]+ c2*z[1]+ c1*z[2]+ c2*z[3] ); y5=x++;
         vx[x]= 0.5*( c2*z[0]+ c1*z[1]+ c2*z[2]+ c1*z[3] ); y7=x++;
         v= vt.n; vt.n+= 4; vt.resize( pdum );
         vt[v].id= v; w1= v++;
         vt[v].id= v; w3= v++;
         vt[v].id= v; w5= v++;
         vt[v].id= v; w7= v++;

         n= hx.n; hx.n+=4; hx.resize(hdum);
 
         hex( n+0, w0,w1,w2,w3, w4,w5,w6,w7, y0,y1,y2,y3, y4,y5,y6,y7 );
         hex( n+1, w2,w3,v2,v3, w6,w7,v6,v7, y2,y3,x2,x3, y6,y7,x6,x7 );
         hex( n+2, w4,w5,w6,w7, v4,v5,v6,v7, y4,y5,y6,y7, x4,x5,x6,x7 );
         hex( n+3, v0,v1,w0,w1, v4,v5,w4,w5, x0,x1,y0,y1, x4,x5,y4,y5 );

         force( n+0,0, -1,-1 );
         force( n+1,0, -1,-1 );
         force( n+2,0, -1,-1 );
         force( n+3,0, -1,-1 );

         attach( (MXGRP-1),n+0,0, -1,-1,-1 );
         attach( (MXGRP-1),n+1,0, -1,-1,-1 );
         attach( (MXGRP-1),n+2,0, -1,-1,-1 );
         attach( (MXGRP-1),n+3,0, -1,-1,-1 );

         attach( (MXGRP-1),n0+0,1, -1,-1,-1 );
         attach( (MXGRP-1),n0+1,1, -1,-1,-1 );
         attach( (MXGRP-1),n0+2,1, -1,-1,-1 );
         attach( (MXGRP-1),n0+3,1, -1,-1,-1 );

         force( n+0,1, -1,-1 );
         force( n+1,1, -1,-1 );
         force( n+2,1, -1,-1 );
         force( n+3,1, -1,-1 );

         join( n+0,0, n0+0,1 );
         join( n+1,0, n0+1,1 );
         join( n+2,0, n0+2,1 );
         join( n+3,0, n0+3,1 );

         force( n+0,2,  n+3,3 );
         force( n+0,3,  n+1,2 );
         force( n+0,5,  n+2,4 );

         force( n+2,2,  n+3,5 );
         force( n+2,3,  n+1,5 );

         force( n+0,4, -1,-1 );
         force( n+1,4, -1,-1 );
         force( n+3,4, -1,-1 );

         if( f2 > -1 ){ force( n+3,2,-1,-1 ); }else{ replace( i,g2,n+3,2 ); }
         if( f3 > -1 ){ force( n+1,3,-1,-1 ); }else{ replace( i,g3,n+1,3 ); }
         if( f5 > -1 ){ force( n+2,5,-1,-1 ); }else{ replace( i,g5,n+2,5 ); }

         detach( f4,b4 );
         attach( (MXGRP-1),i,g4,-1,-1,-1 );

         attach( f4,n+0,4,s4,p4,-1 );
         attach( f4,n+1,4,s4,p4,-1 );
         attach( f4,n+3,4,s4,p4,-1 );

         if( f2 > -1 ){ attach( f2,n+3,2,s2,p2,-1 ); }
         if( f3 > -1 ){ attach( f3,n+1,3,s3,p3,-1 ); }
         if( f5 > -1 ){ attach( f5,n+2,5,s5,p5,-1 ); }

         n0= n;

     }

      detach( f1,b1 );
      attach( (MXGRP-1),i,g1,-1,-1,-1 );

      attach( f1,n0+0,1,s1,p1,-1 );
      attach( f1,n0+1,1,s1,p1,-1 );
      attach( f1,n0+2,1,s1,p1,-1 );
      attach( f1,n0+3,1,s1,p1,-1 );

      return;
  }

