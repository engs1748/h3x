# include <hmesh/hmesh.h>

/*
splits all hexes in a plane into two hexes normal to the plane.
plane is defined: containing hex i0 with normal d in i0.
plane normal points from quad 0 to quad 1 in new hexes.
*/

   INT_ edr[3][4]= { {0,2,4,6},{1,3,5,7},{8,9,10,11} };
   INT_ spt[3][2][8]= { {{ 0, 8, 2, 9, 4,10, 6,11},{ 8, 1, 9, 3,10, 5,11, 7}},
                        {{ 0, 1, 9, 8, 4, 5,11,10},{ 9, 8, 2, 3,11,10, 6, 7}},
                        {{ 0, 1, 2, 3, 8, 9,10,11},{ 8, 9,10,11, 4, 5, 6, 7}} };

   struct brf_t
  {
      INT_     g,q,s,b,p;
      INT_2    h;
      brf_t(){ g=-1,h[0]=-1,h[1]=-1;q=-1,s=-1,b=-1,p=-1; };
  };
   typedef vsize_t<brf_t,DLEN_H> vbrf_t;
   const brf_t bbdm;

   void markb1( INT_ a, INT_ j, INT_ h, vhex_t &hx, vqvad_t &qv, bsrf_t *bz, vbrf_t &var )
  {
      INT_ l,g,b;
      g= hx[j].q[a].g;

      l= var.append(bbdm);

      if( hx[j].q[a].g > -1 )
     {

         var[l].q= a;
         var[l].h[0]=  hx[h].id;
         var[l].h[1]= -1;
         var[l].g= g;

         b= hx[j].q[a].b;

         var[l].b= bz[g].bo[b].b;
         var[l].p= bz[g].bo[b].p;
         var[l].s= bz[g].bo[b].s;

     }
      else
     {
         assert( hx[j].neig( qv.data(), a, g,b ) );

         var[l].q= b;
         var[l].h[0]=  hx[h].id;
         var[l].h[1]=  hx[g].id;
         var[l].g= -1;

     }
      return;
  }

   void markb2( INT_ a, INT_ j, INT_ h, vhex_t &hx, vqvad_t &qv, bsrf_t *bz, vbrf_t &var )
  {
      INT_ l,g,b;

      g= hx[j].q[a].g;


      if( hx[j].q[a].g > -1 )
     {
         l= var.append(bbdm);
         var[l].q= a;
         var[l].h[0]=  hx[h  ].id;
         var[l].h[1]=  hx[h+1].id;
         var[l].g= g;

         b= hx[j].q[a].b;

         var[l].b= bz[g].bo[b].b;
         var[l].p= bz[g].bo[b].p;
         var[l].s= bz[g].bo[b].s;
     }
      return;
  }

   void hmesh_t::split( INT_ i0, dir_t dir )
  {
      INT_                  a,b,d,e,g,i,j,k,h,l,n,m;
      INT_12                buf;

      VINT_                 list;
      VINT_                 mke;
      VINT_                 mkv;
      vbrf_t                mk1,mk2;

      vsize_t<dir_t,DLEN_H> f(hx.n,ddum); // directions in each marked hex

      INT_6                 msk={-1,-1,-1,-1,-1,-1};


      eg.n=0;
      edges();
      spread( indx[i0], dir, list, f );

      mke.n= eg.n; mke.resize(-1);

      n= vx.n; assert( vt.n == n );
      m= indx.n;

      for( i=0;i<list.n;i++ )
     {
         j= list[i];

         d=f[j].dir();

// make new points and make list of vertices of the hexahedra

         memcpy( buf,hx[j].v,8*sizeof(INT_) );

         for( k=0;k<4;k++ )
        {
            e= edr[d][k];

            a= hx[j].x[ mhe[e][0] ];
            b= hx[j].x[ mhe[e][1] ];

            e= hx[j].e[e].e;

            if( mke[e] == -1 )
           {
               h= vx.append( xdum );
               vx[h]= 0.5*( vx[a]+ vx[b] );
               mke[e]= h;

               h= vt.append(pdum); assert( vt.n == vx.n );
               vt[h].id= h;

               h= mkv.append(-1); mkv[h]= a;
               h= mkv.append(-1); mkv[h]= b;
           }

            buf[8+k]= mke[e];
            
        }

// build new hex

         h= hx.n; hx.n+= 2; hx.resize(hdum);

         hex( h,   buf[ spt[d][0][0] ], buf[ spt[d][0][1] ], buf[ spt[d][0][2] ], buf[ spt[d][0][3] ],
                   buf[ spt[d][0][4] ], buf[ spt[d][0][5] ], buf[ spt[d][0][6] ], buf[ spt[d][0][7] ],
                   buf[ spt[d][0][0] ], buf[ spt[d][0][1] ], buf[ spt[d][0][2] ], buf[ spt[d][0][3] ],
                   buf[ spt[d][0][4] ], buf[ spt[d][0][5] ], buf[ spt[d][0][6] ], buf[ spt[d][0][7] ] );

         hex( h+1, buf[ spt[d][1][0] ], buf[ spt[d][1][1] ], buf[ spt[d][1][2] ], buf[ spt[d][1][3] ],
                   buf[ spt[d][1][4] ], buf[ spt[d][1][5] ], buf[ spt[d][1][6] ], buf[ spt[d][1][7] ],
                   buf[ spt[d][1][0] ], buf[ spt[d][1][1] ], buf[ spt[d][1][2] ], buf[ spt[d][1][3] ],
                   buf[ spt[d][1][4] ], buf[ spt[d][1][5] ], buf[ spt[d][1][6] ], buf[ spt[d][1][7] ] );

         assert( vol(h) > 0 );
         assert( vol(h+1) > 0 );

// mark faces in the d direction

         markb1( oqq[0][d][0], j,  h, hx, qv, bz, mk1 );
         markb1( oqq[0][d][1], j,1+h, hx, qv, bz, mk1 );

// mark remaining boundary faces

         for( k=2;k<6;k++ )
        {
            markb2( oqq[0][d][k], j, h, hx, qv, bz, mk2 );

        }

         list[i]= hx[j].id;
     }

// delete original hexahedra

      for( i=0;i<list.n;i++ ){ del( list[i],msk ); };

// detach orphaned faces

      for( l=0;l<mk1.n;l++ )
     {
         if( mk1[l].g == -1 )
        {
            h= mk1[l].h[1]; h= indx[h];
            b= mk1[l].q;
            detach1( h,b );
        }
     }

      quadz( mkv, m,indx.n);

// restore boundary faces 

      for( i=0;i<mk1.n;i++ )
     {
         g=       mk1[i].g;
         if( g > -1 )
        {
            h= mk1[i].h[0]; h= indx[ h ];
            a= mk1[i].q;
            attach( g, h,a, mk1[i].s,mk1[i].p,mk1[i].b );
        }
     }

// restore boundary faces 

      for( i=0;i<mk2.n;i++ )
     {
         g= mk2[i].g;
         a= mk2[i].q;

         h= mk2[i].h[0]; h= indx[ h ];
         attach( g, h,a, mk2[i].s,mk2[i].p,mk2[i].b );

         h= mk2[i].h[1]; h= indx[ h ];
         attach( g, h,a, mk2[i].s,mk2[i].p,mk2[i].b );

     }

// cleanup dummy group

      trimb();
      eg.n=0;

      return;
  }
