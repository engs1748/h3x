#  include <hmesh/hmesh.h>

   void hmesh_t::dig1( dir_t d0, dir_t d1, INT_ kst, REAL_3 v, INT_ *mask )
  {

      INT_            n,m;
      INT_            i;

      vtx_t           w(v);
      vtx_t           z;
      vtx_t           s;
      vtx_t           r;
      vtx_t           c;

      REAL_           d;
      REAL_           f,g;
      const REAL_     i12= 0.25/3.;

      cavty_t         cv;

      g= 0.2;
      z[0]= v[0];
      z[1]= v[1];
      z[2]= v[2];
      f= l2norm(z);
      d= 1./f;
      z*= d;

      cvty( 2,1,1, d0,d1, kst, cv );

      r= 0.25*( vx[cv.v[0]]+ vx[cv.v[1]]+ vx[cv.v[2]]+ vx[cv.v[3]] );

// initial points

      n= vx.n; vx.n+= 60; vx.resize( xdum );
               vt.n+= 60; vt.resize( pdum );

      for( i=n;i<vt.n;i++ ){ vt[i].id= i; };

      vx[n+ 0]= vx[cv.v[4]]- r; vx[n+ 0]*= 0.8; vx[n+ 0]+= r;
      vx[n+ 2]= vx[cv.v[5]]- r; vx[n+ 2]*= 0.8; vx[n+ 2]+= r;
                         
      vx[n+ 3]= vx[cv.v[6]]- r; vx[n+ 3]*= 0.8; vx[n+ 3]+= r;
      vx[n+ 5]= vx[cv.v[7]]- r; vx[n+ 5]*= 0.8; vx[n+ 5]+= r;
        
      vx[n+12]= vx[cv.v[0]]- r; vx[n+12]*= 0.8; vx[n+12]+= r;
      vx[n+14]= vx[cv.v[1]]- r; vx[n+14]*= 0.8; vx[n+14]+= r;
                        
      vx[n+21]= vx[cv.v[2]]- r; vx[n+21]*= 0.8; vx[n+21]+= r;
      vx[n+23]= vx[cv.v[3]]- r; vx[n+23]*= 0.8; vx[n+23]+= r;
      

// complete A section

      vx[n+ 1]=   0.5*( vx[n+ 0]+ vx[n+ 2] );
      vx[n+ 4]=   0.5*( vx[n+ 3]+ vx[n+ 5] );

// complete C section

      vx[n+13]=   0.5*( vx[n+12]+ vx[n+14] );      
      vx[n+22]=   0.5*( vx[n+21]+ vx[n+23] );      

      vx[n+15]=  0.75*vx[n+12]+ i12*( vx[n+13]+ vx[n+21]+ vx[n+22] );
      vx[n+17]=  0.75*vx[n+14]+ i12*( vx[n+13]+ vx[n+22]+ vx[n+23] );
      vx[n+18]=  0.75*vx[n+21]+ i12*( vx[n+12]+ vx[n+13]+ vx[n+22] );
      vx[n+20]=  0.75*vx[n+23]+ i12*( vx[n+13]+ vx[n+14]+ vx[n+22] );

      vx[n+16]=   0.5*( vx[n+15]+ vx[n+17] );      
      vx[n+19]=   0.5*( vx[n+18]+ vx[n+20] );      

// B section

      vx[n+ 6]=  0.75*vx[n+ 0]+ i12*( vx[n+ 1]+ vx[n+ 3]+ vx[n+ 4] );
      vx[n+ 8]=  0.75*vx[n+ 2]+ i12*( vx[n+ 1]+ vx[n+ 4]+ vx[n+ 5] );
      vx[n+ 9]=  0.75*vx[n+ 3]+ i12*( vx[n+ 0]+ vx[n+ 1]+ vx[n+ 4] );
      vx[n+11]=  0.75*vx[n+ 5]+ i12*( vx[n+ 4]+ vx[n+ 1]+ vx[n+ 2] );

      vx[n+ 6]+= vx[n+15];
      vx[n+ 8]+= vx[n+17];
      vx[n+ 9]+= vx[n+18];
      vx[n+11]+= vx[n+20];

      vx[n+ 6]*= 0.5;
      vx[n+ 8]*= 0.5;
      vx[n+ 9]*= 0.5;
      vx[n+11]*= 0.5;

      vx[n+ 7]=   0.5*( vx[n+ 6]+ vx[n+ 8] );
      vx[n+10]=   0.5*( vx[n+ 9]+ vx[n+11] );

// D section      

      s= vtx_t(v);
      s*= g;

      vx[n+24]= vx[n+12]+ s;
      vx[n+25]= vx[n+13]+ s;
      vx[n+26]= vx[n+14]+ s;
      vx[n+27]= vx[n+15]+ s;
      vx[n+28]= vx[n+16]+ s;
      vx[n+29]= vx[n+17]+ s;
      vx[n+30]= vx[n+18]+ s;
      vx[n+31]= vx[n+19]+ s;
      vx[n+32]= vx[n+20]+ s;
      vx[n+33]= vx[n+21]+ s;
      vx[n+34]= vx[n+22]+ s;
      vx[n+35]= vx[n+23]+ s;

// E section

      c= 0.8*vtx_t(v);
      c+=  r;

      orth( c,    z,vx[n+24], vx[n+41] );
      orth( c,    z,vx[n+26], vx[n+42] );
      orth( c,    z,vx[n+27], vx[n+39] );
      orth( c,    z,vx[n+29], vx[n+40] );
      orth( c,    z,vx[n+30], vx[n+37] );
      orth( c,    z,vx[n+32], vx[n+38] );
      orth( c,    z,vx[n+33], vx[n+59] );
      orth( c,    z,vx[n+35], vx[n+36] );

// F section
      c=  r+    vtx_t(v);

      orth( c,    z,vx[n+24], vx[n+49] );
      orth( c,    z,vx[n+26], vx[n+50] );
      orth( c,    z,vx[n+27], vx[n+47] );
      orth( c,    z,vx[n+29], vx[n+48] );
      orth( c,    z,vx[n+30], vx[n+45] );
      orth( c,    z,vx[n+32], vx[n+46] );
      orth( c,    z,vx[n+33], vx[n+43] );
      orth( c,    z,vx[n+35], vx[n+44] );

// G section

      c= 0.5*vtx_t(v);
      c+=  r;

      orth( c,    z,vx[n+25], vx[n+51] );
      orth( c,    z,vx[n+28], vx[n+53] );
      orth( c,    z,vx[n+31], vx[n+55] );
      orth( c,    z,vx[n+34], vx[n+57] );

      vx[n+52]=  0.5*( vx[n+42]+ vx[n+26] );
      vx[n+54]=  0.5*( vx[n+40]+ vx[n+29] );
      vx[n+56]=  0.5*( vx[n+38]+ vx[n+32] );
      vx[n+58]=  0.5*( vx[n+36]+ vx[n+35] );

// form hexahedra

      m= hx.n; hx.n+= 43; hx.resize( hdum );

      hex( m+ 0, n+ 6,n+ 7,n+ 9,n+10,n+ 0,n+ 1,n+ 3,n+ 4, n+ 6,n+ 7,n+ 9,n+10,n+ 0,n+ 1,n+ 3,n+ 4 );
      hex( m+ 1, n+ 7,n+ 8,n+10,n+11,n+ 1,n+ 2,n+ 4,n+ 5, n+ 7,n+ 8,n+10,n+11,n+ 1,n+ 2,n+ 4,n+ 5 );
      hex( m+ 2, n+12,n+13,n+15,n+16,n+ 0,n+ 1,n+ 6,n+ 7, n+12,n+13,n+15,n+16,n+ 0,n+ 1,n+ 6,n+ 7 );
      hex( m+ 3, n+13,n+14,n+16,n+17,n+ 1,n+ 2,n+ 7,n+ 8, n+13,n+14,n+16,n+17,n+ 1,n+ 2,n+ 7,n+ 8 );

      hex( m+ 4, n+12,n+15,n+21,n+18,n+ 0,n+ 6,n+ 3,n+ 9, n+12,n+15,n+21,n+18,n+ 0,n+ 6,n+ 3,n+ 9 );
      hex( m+ 5, n+15,n+16,n+18,n+19,n+ 6,n+ 7,n+ 9,n+10, n+15,n+16,n+18,n+19,n+ 6,n+ 7,n+ 9,n+10 );
      hex( m+ 6, n+16,n+17,n+19,n+20,n+ 7,n+ 8,n+10,n+11, n+16,n+17,n+19,n+20,n+ 7,n+ 8,n+10,n+11 );
      hex( m+ 7, n+17,n+14,n+20,n+23,n+ 8,n+ 2,n+11,n+ 5, n+17,n+14,n+20,n+23,n+ 8,n+ 2,n+11,n+ 5 );

      hex( m+ 8, n+18,n+19,n+21,n+22,n+ 9,n+10,n+ 3,n+ 4, n+18,n+19,n+21,n+22,n+ 9,n+10,n+ 3,n+ 4 );
      hex( m+ 9, n+19,n+20,n+22,n+23,n+10,n+11,n+ 4,n+ 5, n+19,n+20,n+22,n+23,n+10,n+11,n+ 4,n+ 5 );
      hex( m+10, n+24,n+25,n+27,n+28,n+12,n+13,n+15,n+16, n+24,n+25,n+27,n+28,n+12,n+13,n+15,n+16 );
      hex( m+11, n+25,n+26,n+28,n+29,n+13,n+14,n+16,n+17, n+25,n+26,n+28,n+29,n+13,n+14,n+16,n+17 );

      hex( m+12, n+24,n+27,n+33,n+30,n+12,n+15,n+21,n+18, n+24,n+27,n+33,n+30,n+12,n+15,n+21,n+18 ); 
      hex( m+13, n+27,n+28,n+30,n+31,n+15,n+16,n+18,n+19, n+27,n+28,n+30,n+31,n+15,n+16,n+18,n+19 );
      hex( m+14, n+28,n+29,n+31,n+32,n+16,n+17,n+19,n+20, n+28,n+29,n+31,n+32,n+16,n+17,n+19,n+20 );
      hex( m+15, n+29,n+26,n+32,n+35,n+17,n+14,n+20,n+23, n+29,n+26,n+32,n+35,n+17,n+14,n+20,n+23 );

      hex( m+16, n+30,n+31,n+33,n+34,n+18,n+19,n+21,n+22, n+30,n+31,n+33,n+34,n+18,n+19,n+21,n+22 ); 
      hex( m+17, n+31,n+32,n+34,n+35,n+19,n+20,n+22,n+23, n+31,n+32,n+34,n+35,n+19,n+20,n+22,n+23 );
      hex( m+18, n+51,n+52,n+53,n+54,n+25,n+26,n+28,n+29, n+51,n+52,n+53,n+54,n+25,n+26,n+28,n+29 );
      hex( m+19, n+53,n+54,n+55,n+56,n+28,n+29,n+31,n+32, n+53,n+54,n+55,n+56,n+28,n+29,n+31,n+32 );
      hex( m+20, n+54,n+52,n+56,n+58,n+29,n+26,n+32,n+35, n+54,n+52,n+56,n+58,n+29,n+26,n+32,n+35 );
      hex( m+21, n+55,n+56,n+57,n+58,n+31,n+32,n+34,n+35, n+55,n+56,n+57,n+58,n+31,n+32,n+34,n+35 );
      hex( m+22, n+41,n+51,n+39,n+53,n+24,n+25,n+27,n+28, n+41,n+51,n+39,n+53,n+24,n+25,n+27,n+28 );
      hex( m+23, n+39,n+53,n+37,n+55,n+27,n+28,n+30,n+31, n+39,n+53,n+37,n+55,n+27,n+28,n+30,n+31 );
      hex( m+24, n+37,n+55,n+59,n+57,n+30,n+31,n+33,n+34, n+37,n+55,n+59,n+57,n+30,n+31,n+33,n+34 );
      hex( m+25, n+41,n+39,n+59,n+37,n+24,n+27,n+33,n+30, n+41,n+39,n+59,n+37,n+24,n+27,n+33,n+30 );
      hex( m+26, n+41,n+42,n+39,n+40,n+51,n+52,n+53,n+54, n+41,n+42,n+39,n+40,n+51,n+52,n+53,n+54 );
      hex( m+27, n+39,n+40,n+37,n+38,n+53,n+54,n+55,n+56, n+39,n+40,n+37,n+38,n+53,n+54,n+55,n+56 );

      hex( m+28, n+40,n+42,n+38,n+36,n+54,n+52,n+56,n+58, n+40,n+42,n+38,n+36,n+54,n+52,n+56,n+58 );
      hex( m+29, n+37,n+38,n+59,n+36,n+55,n+56,n+57,n+58, n+37,n+38,n+59,n+36,n+55,n+56,n+57,n+58 );
      hex( m+30, n+49,n+50,n+47,n+48,n+41,n+42,n+39,n+40, n+49,n+50,n+47,n+48,n+41,n+42,n+39,n+40 );
      hex( m+31, n+49,n+47,n+43,n+45,n+41,n+39,n+59,n+37, n+49,n+47,n+43,n+45,n+41,n+39,n+59,n+37 );

      hex( m+32, n+47,n+48,n+45,n+46,n+39,n+40,n+37,n+38, n+47,n+48,n+45,n+46,n+39,n+40,n+37,n+38 );
      hex( m+33, n+48,n+50,n+46,n+44,n+40,n+42,n+38,n+36, n+48,n+50,n+46,n+44,n+40,n+42,n+38,n+36 );
      hex( m+34, n+45,n+46,n+43,n+44,n+37,n+38,n+59,n+36, n+45,n+46,n+43,n+44,n+37,n+38,n+59,n+36 );

// connecting hexahedra

      hex( m+35, cv.v[ 0],cv.v[ 8],n+12    ,n+13    ,cv.v[ 4],cv.v[10], n+ 0   ,n+ 1    , cv.v[ 0],cv.v[ 8],n+12    ,n+13    ,cv.v[ 4],cv.v[10], n+ 0   ,n+ 1     );
      hex( m+36, cv.v[ 8],cv.v[ 1],n+13    ,n+14    ,cv.v[10],cv.v[ 5], n+ 1   ,n+ 2    , cv.v[ 8],cv.v[ 1],n+13    ,n+14    ,cv.v[10],cv.v[ 5], n+ 1   ,n+ 2     );
      hex( m+37, cv.v[ 0],n+12    ,cv.v[ 2],n+21    ,cv.v[ 4], n+ 0   ,cv.v[ 6],n+ 3    , cv.v[ 0],n+12    ,cv.v[ 2],n+21    ,cv.v[ 4], n+ 0   ,cv.v[ 6],n+ 3     );
      hex( m+38, n+14    ,cv.v[ 1],n+23    ,cv.v[ 3], n+ 2   ,cv.v[ 5],n+ 5    ,cv.v[ 7], n+14    ,cv.v[ 1],n+23    ,cv.v[ 3], n+ 2   ,cv.v[ 5],n+ 5    ,cv.v[ 7] );
      hex( m+39, n+21    ,n+22    ,cv.v[ 2],cv.v[ 9], n+ 3   ,n+ 4    ,cv.v[ 6],cv.v[11], n+21    ,n+22    ,cv.v[ 2],cv.v[ 9], n+ 3   ,n+ 4    ,cv.v[ 6],cv.v[11] );
      hex( m+40, n+22    ,n+23    ,cv.v[ 9],cv.v[ 3], n+ 4   ,n+ 5    ,cv.v[11],cv.v[ 7], n+22    ,n+23    ,cv.v[ 9],cv.v[ 3], n+ 4   ,n+ 5    ,cv.v[11],cv.v[ 7] );

      hex( m+41, n+ 0    ,n+ 1    , n+ 3   ,n+ 4    ,cv.v[ 4],cv.v[10],cv.v[ 6],cv.v[11], n+ 0    ,n+ 1    , n+ 3   ,n+ 4    ,cv.v[ 4],cv.v[10],cv.v[ 6],cv.v[11] );
      hex( m+42, n+ 1    ,n+ 2    , n+ 4   ,n+ 5    ,cv.v[10],cv.v[ 5],cv.v[11],cv.v[ 7], n+ 1    ,n+ 2    , n+ 4   ,n+ 5    ,cv.v[10],cv.v[ 5],cv.v[11],cv.v[ 7] );

// new faces

      quads( cv.v, m,m+43 );

// Now attach faces to boundaries

      attach( mask[0], m+12,0, -1,-1,-1 );
      attach( mask[0], m+25,0, -1,-1,-1 );
      attach( mask[0], m+31,0, -1,-1,-1 );

      attach( mask[1], m+15,1, -1,-1,-1 );
      attach( mask[1], m+20,1, -1,-1,-1 );
      attach( mask[1], m+28,1, -1,-1,-1 );
      attach( mask[1], m+33,1, -1,-1,-1 );

      attach( mask[2], m+10,2, -1,-1,-1 );
      attach( mask[2], m+11,2, -1,-1,-1 );
      attach( mask[2], m+18,2, -1,-1,-1 );
      attach( mask[2], m+22,2, -1,-1,-1 );
      attach( mask[2], m+26,2, -1,-1,-1 );
      attach( mask[2], m+30,2, -1,-1,-1 );

      attach( mask[3], m+16,3, -1,-1,-1 );
      attach( mask[3], m+17,3, -1,-1,-1 );
      attach( mask[3], m+21,3, -1,-1,-1 );
      attach( mask[3], m+24,3, -1,-1,-1 );
      attach( mask[3], m+29,3, -1,-1,-1 );
      attach( mask[3], m+34,3, -1,-1,-1 );

      attach( mask[4], m+30,4, -1,-1,-1 );
      attach( mask[4], m+31,4, -1,-1,-1 );
      attach( mask[4], m+32,4, -1,-1,-1 );
      attach( mask[4], m+33,4, -1,-1,-1 );
      attach( mask[4], m+34,4, -1,-1,-1 );

      attach( cv.q4[0].v[0], m+35,4, cv.q4[0].v[1], cv.q4[0].v[2], cv.q4[0].v[3] );
      attach( cv.q4[0].v[0], m+37,4, cv.q4[0].v[1], cv.q4[0].v[2], cv.q4[0].v[3] );
      attach( cv.q4[0].v[0], m+39,4, cv.q4[0].v[1], cv.q4[0].v[2], cv.q4[0].v[3] );

      attach( cv.q4[1].v[0], m+36,4, cv.q4[1].v[1], cv.q4[1].v[2], cv.q4[1].v[3] );
      attach( cv.q4[1].v[0], m+38,4, cv.q4[1].v[1], cv.q4[1].v[2], cv.q4[1].v[3] );
      attach( cv.q4[1].v[0], m+40,4, cv.q4[1].v[1], cv.q4[1].v[2], cv.q4[1].v[3] );

      return;
      
  }
