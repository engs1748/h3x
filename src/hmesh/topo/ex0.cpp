#  include <hmesh/hmesh.h>

#  define VTX(i,j)  i+(l+1)*(j)
#  define HEX(i,j)  i+ l   *(j)

   void hmesh_t::ex0( dir_t d0, dir_t d1, INT_ kst, INT_ l, INT_ m, REAL_3 v, INT_ *mask )
  {

      INT_            n;
      INT_            i,j,h,k;
      INT_            i0,i1,i2,i3,i4,i5,i6,i7;

      vtx_t           z;
      vtx_t           s;
      vtx_t           r;

      cavty_t         cv;

/*    z[0]= v[0];
      z[1]= v[1];
      z[2]= v[2];
      d= l2norm(z);
      z/= d;*/
 
      z= v;
    

      cvty( l,m,1, d0,d1, kst, cv, false );

      VINT_              p;
      p.n= (l+1)*(m+1); p.resize(-1);

      p[VTX(0,0)]= 0;
      p[VTX(l,0)]= 1;
      p[VTX(0,m)]= 2;
      p[VTX(l,m)]= 3;

      k= 8;
      for( i=1;i<l;i++ )
     {
         p[VTX(i,0)]=   k+ (i-1)*4;
         p[VTX(i,m)]= 1+k+ (i-1)*4;
     }
      k+= (l-1)*4;

      for( j=1;j<m;j++ )
     {
         p[VTX(0,j)]=   k+ (j-1)*4;
         p[VTX(l,j)]= 1+k+ (j-1)*4;
     }
      k+= (m-1)*4;

      for( j=1;j<m;j++ )
     {
         for( i=1;i<l;i++ )
        {
            p[VTX(i,j)]= k++;
            k++;
        }
     }

      n= vx.n; vx.n+= (l+1)*(m+1); vx.resize( xdum );
               vt.n+= (l+1)*(m+1); vt.resize( pdum );

      for( j=0;j<(m+1);j++ )
     {
         for( i=0;i<(l+1);i++ )
        {
            k= VTX(i,j);
            p[k]= cv.v[p[k]];
            vx[n+k]= vx[ p[k] ]+ z;
        }
     }

      h= hx.n; hx.n+= l*m; hx.resize(hdum);

      for( j=0;j<m  ;j++ )
     {
         for( i=0;i<l  ;i++ )
        {
            i0= VTX(  i,  j);
            i1= VTX(1+i,  j);
            i2= VTX(  i,1+j);
            i3= VTX(1+i,1+j);

            i4= p[i0];
            i5= p[i1];
            i6= p[i2];
            i7= p[i3];

            k= HEX(i,j);

            hex( h+ k, n+i0,n+i1,n+i2,n+i3, i4,i5,i6,i7, 
                       n+i0,n+i1,n+i2,n+i3, i4,i5,i6,i7 );
 
            detach1( indx[cv.q4[k].v[4]],cv.q4[k].v[5] );

            k= HEX(i,j);
	}
     }

      quads( p, h,h+l*m );

      for( j=0;j<m;j++ ){ attach( mask[0], h+HEX(0,  j),0, -1,-1,-1 ); }
      for( j=0;j<m;j++ ){ attach( mask[1], h+HEX(l-1,j),1, -1,-1,-1 ); }

      for( i=0;i<l;i++ ){ attach( mask[2], h+HEX(i,  0),2, -1,-1,-1 ); }
      for( i=0;i<l;i++ ){ attach( mask[3], h+HEX(i,m-1),3, -1,-1,-1 ); }

      for( j=0;j<m  ;j++ )
     {
         for( i=0;i<l  ;i++ )
        {
            attach( mask[4], h+HEX(i,j),4, -1,-1,-1 );
        }
     }


      return;
      
  }
