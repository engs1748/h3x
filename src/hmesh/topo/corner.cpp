#  include <hmesh/hmesh.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void hmesh_t::corner( INT_ i0, frme_t a0, INT_ *l, VINT_ &list )
  {
      frme_t               a,e,f;
      dir_t                g;
      dir_t                dum;
      wsize_t<dir_t,3,DLEN_H> d;
      INT_                 i,j,k,n,m, u,v,x,y;

      VINT_                mk(hx.n,-1);

      vsize_t<dir_t,DLEN_H>   c(hx.n,dum);
      VINT_                   wrk(vt.n,-1);
      VINT_4                  mkv;
      VINT_4                  mkh;

      VINT_2                  l1,l2,l3,l4;

      INT_  mask[6]={-1,-1,-1,-1,-1,-1};

      a0[2]= vec(a0[0],a0[1]);

      a[0]= a0[0];
      a[1]= a0[1];
      a[2]= a0[2];

      i= indx[i0];
      assert( i > -1 );
 
// find the hex to be turned into corner hex: walk forwards
      j=0;
      do
     {
            (d.n)++; d.resize(dum);
         j= (list.n)++; list.resize(-1);

         c[i].dum0= 1;

         d[j][0]= a[0];
         d[j][1]= a[1];
         d[j][2]= a[2];
         list[j]= i;
     }while( walk(i,a,mk) );

      i= indx[i0];
      a[0]= a0[0];
      a[1]= a0[1];
      a[2]=!a0[2];

// find the hex to be turned into corner hex: walk backwards
      while( walk(i,a,mk) )
     {
            (d.n)++; d.resize(dum);
         j= (list.n)++; list.resize(-1);

         c[i].dum0= 1;

         d[j][0]= a[0];
         d[j][1]= a[1];
         d[j][2]= a[2];
         list[j]= i;
     }
      if( list[list.n-1] == list[0] && list.n > 1 ){ list.n--; };

      l[0]= list.n;

      i= indx[i0];


// find the hex to be collapsed and mark vertices and faces to be merged: first leg of the L pattern

//    k= hx[i].face( a0[0] );
      k= face( a0[0] );
      hx[i].qdir( k,e );
 

      l[1]= l[0];
      if( hx[i].neig( qv.data(),k, n,m ) )
     {

         hx[n].qdir( m,f );
         g= dmap( a0[1],e,f );
         spread( n,g, list,c );
         l[1]= list.n;
         
//       printf( "will collapse %d %d\n", l[0],l[1] );
         for( j=l[0];j<l[1];j++ )
        { 
            k=list[j]; 
            list[j]= hx[k].id;

            collapse( k,c[k], wrk,mkv,mkh );
        }
     }

// find the hex to be collapsed and mark vertices and faces to be merged: first leg of the L pattern

//    k= hx[i].face( a0[1] );
      k= face( a0[1] );
      hx[i].qdir( k,e );

      l[2]= l[1];
      if( hx[i].neig( qv.data(),k, n,m ) )
     {

         hx[n].qdir( m,f );
         g= dmap( a0[0],e,f );
         spread( n,g, list,c );
         l[2]= list.n;

//       printf( "will collapse %d %d\n", l[1],l[2] );
         for( j=l[1];j<l[2];j++ )
        {
            k=list[j]; 
            list[j]= hx[k].id;

            collapse( k,c[k], wrk,mkv,mkh ); 
        }
     }

// find the hex to be collapsed and mark vertices and faces to be merged: corner of the L pattern

      for( j=0;j<l[0];j++ )
     {
         k= list[j];
         list[j]= hx[k].id;

         collapse( k, !(d[j][0]),!(d[j][1]),d[j][2], wrk,mkv,mkh );
     }

      for( j=0;j<mkh.n;j++ )
     {
            k= mkh[j][0]; mkh[j][0]= hx[k].id;
            k= mkh[j][2]; mkh[j][2]= hx[k].id;
     }

// delete collapsed hexahedra

      for( j=0;j<list.n;j++ ){ del( list[j],mask ); }

// modify numbering of surviving hexahedra

      for( j=0;j<mkv.n;j++ )
     {

         u= mkv[j][0];
         v= mkv[j][1];
         x= mkv[j][2];
         y= mkv[j][3];
  
         point( u, l1,l2 );
         point( v, l3,l4 );

         substitute( u,x, l1,l2, v,y, l3,l4 );

     }

// stitch faces back

      for( j=0;j<mkh.n;j++ )
     {

         u= mkh[j][0];
         x= mkh[j][1];

         v= mkh[j][2];
         y= mkh[j][3];

         u= indx[u];
         v= indx[v];

         join( u,x, v,y );
     }

      for( j=0;j<mkh.n;j++ )
     {

         u= mkh[j][0];
         v= mkh[j][2];

         u= indx[u];
         v= indx[v];

         claim(u);
         claim(v);
     }

      trimb();
      return;
  }

