#  include <hmesh/hmesh.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   bool hmesh_t::walk( INT_ &i, INT_ &d, VINT_ &mark )
  {
      bool val= true;
      INT_ j,k;

      j= qv[i].h[d];
      k= qv[i].q[d];

//    printf( "do we ever use this?\n" );

      if( j == -1 )
     {
         val= false;
         i= -1;
     }
      else
     {
         if( mark[j] > -1){ val=false; }
     
         mark[j]= 0;
         k= opq[k];
         i= hx[j].q[k].q;
         d= !(hx[j].q[k].o);
     }
      return val;

  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   bool hmesh_t::walk( INT_ &i, dir_t *a, VINT_ &mark )
  {

      INT_  j,k,l;
      bool val= true;

      dir_t    b[3];
      dir_t    c[3];

//    j= hx[i].face(a[2]);
      j= face(a[2]);
      hx[i].qdir( j,b );

      k= hx[i].q[j].q;
      l= hx[i].q[j].o;

      i= qv[k].h[!l]; 
      j= qv[k].q[!l];

      if( i == -1 )
     {
         val= false;
     }
      else
     {
         if( mark[i] > -1 ){ val= false; }; 

         mark[i]= 0;
         hx[i].qdir( j,c );

         a[0]= dmap( a[0], b,c );
         a[1]= dmap( a[1], b,c );

         a[2]= dir_t( !(j%2),j/2 );
     }
      return val;

  }
