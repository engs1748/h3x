#  include <hmesh/hmesh.h>

   void hmesh_t::dig4( dir_t d0, dir_t d1, INT_ kst, REAL_3 v, INT_ *mask )
  {

      INT_            n,m;
      INT_            i;

      vtx_t           w(v);
      vtx_t           z;
      vtx_t           s;
      vtx_t           r;
      vtx_t           c;

      REAL_           d;
      REAL_           f,g;
      const REAL_     i13= 1./3.;
      const REAL_     i23= 2./3.;

      cavty_t         cv;

      g= 0.2;
      z[0]= v[0];
      z[1]= v[1];
      z[2]= v[2];
      f= l2norm(z);
      d= 1./f;
      z*= d;

      cvty( 3,2,1, d0,d1, kst, cv, true );

      r= 0.25*( vx[cv.v[0]]+ vx[cv.v[1]]+ vx[cv.v[2]]+ vx[cv.v[3]] );

// initial points

      n= vx.n; vx.n+=114; vx.resize( xdum );
               vt.n+=114; vt.resize( pdum );

      for( i=n;i<vt.n;i++ ){ vt[i].id= i; };

      vx[n+  0]= vx[cv.v[4]]- r; vx[n+  0]*= 0.75; vx[n+  0]+= r;
      vx[n+  3]= vx[cv.v[5]]- r; vx[n+  3]*= 0.75; vx[n+  3]+= r;

      vx[n+  8]= vx[cv.v[6]]- r; vx[n+  8]*= 0.75; vx[n+  8]+= r;
      vx[n+ 11]= vx[cv.v[7]]- r; vx[n+ 11]*= 0.75; vx[n+ 11]+= r;

      vx[n+ 24]= vx[cv.v[0]]- r; vx[n+ 24]*= 0.75; vx[n+ 24]+= r;
      vx[n+ 27]= vx[cv.v[1]]- r; vx[n+ 27]*= 0.75; vx[n+ 27]+= r;

      vx[n+ 42]= vx[cv.v[2]]- r; vx[n+ 42]*= 0.75; vx[n+ 42]+= r;
      vx[n+ 45]= vx[cv.v[3]]- r; vx[n+ 45]*= 0.75; vx[n+ 45]+= r;
      

// complete A section+ 

      vx[n+  4]= 0.5*( vx[n+  0]+ vx[n+  8] );
      vx[n+  7]= 0.5*( vx[n+  3]+ vx[n+ 11] );

      vx[n+  1]=   i23*vx[n+  0]+ i13*vx[n+  3];
      vx[n+  2]=   i13*vx[n+  0]+ i23*vx[n+  3];

      vx[n+  5]=   i23*vx[n+  4]+ i13*vx[n+  7];
      vx[n+  6]=   i13*vx[n+  4]+ i23*vx[n+  7];

      vx[n+  9]=   i23*vx[n+  8]+ i13*vx[n+ 11];
      vx[n+ 10]=   i13*vx[n+  8]+ i23*vx[n+ 11];

// complete C section+ 

      vx[n+ 25]=   i23*vx[n+ 24]+ i13*vx[n+ 27];
      vx[n+ 26]=   i13*vx[n+ 24]+ i23*vx[n+ 27];

      vx[n+ 43]=   i23*vx[n+ 42]+ i13*vx[n+ 45];
      vx[n+ 44]=   i13*vx[n+ 42]+ i23*vx[n+ 45];

      vx[n+ 32]=   0.50*vx[n+ 24]+ 0.50*vx[n+ 42];
      vx[n+ 37]=   0.50*vx[n+ 27]+ 0.50*vx[n+ 45];

      vx[n+ 28]=   0.75*vx[n+ 24]+ 0.25*vx[n+ 42];
      vx[n+ 33]=   0.50*vx[n+ 24]+ 0.50*vx[n+ 42];
      vx[n+ 38]=   0.25*vx[n+ 24]+ 0.75*vx[n+ 42];

      vx[n+ 29]=   0.75*vx[n+ 25]+ 0.25*vx[n+ 43];
      vx[n+ 34]=   0.50*vx[n+ 25]+ 0.50*vx[n+ 43];
      vx[n+ 39]=   0.25*vx[n+ 25]+ 0.75*vx[n+ 43];

      vx[n+ 30]=   0.75*vx[n+ 26]+ 0.25*vx[n+ 44];
      vx[n+ 35]=   0.50*vx[n+ 26]+ 0.50*vx[n+ 44];
      vx[n+ 40]=   0.25*vx[n+ 26]+ 0.75*vx[n+ 44];

      vx[n+ 31]=   0.75*vx[n+ 27]+ 0.25*vx[n+ 45];
      vx[n+ 36]=   0.50*vx[n+ 27]+ 0.50*vx[n+ 45];
      vx[n+ 41]=   0.25*vx[n+ 27]+ 0.75*vx[n+ 45];

      vx[n+ 28]+= vx[n+ 29];
      vx[n+ 33]+= vx[n+ 34];
      vx[n+ 38]+= vx[n+ 39];

      vx[n+ 28]*= 0.50;
      vx[n+ 33]*= 0.50;
      vx[n+ 38]*= 0.50;

      vx[n+ 31]+= vx[n+ 30];
      vx[n+ 36]+= vx[n+ 35];
      vx[n+ 41]+= vx[n+ 40];

      vx[n+ 31]*= 0.50;
      vx[n+ 36]*= 0.50;
      vx[n+ 41]*= 0.50;


// B section+ 
      vtx_t u= 0.25*( vx[n+  0]+ vx[n+  3]+ vx[n+  8]+ vx[n+ 11] );
      u-= r;

      u*= 0.5;

      vx[n+ 12]=  vx[n+ 28]+ u;
      vx[n+ 13]=  vx[n+ 29]+ u;
      vx[n+ 14]=  vx[n+ 30]+ u;
      vx[n+ 15]=  vx[n+ 31]+ u;
      vx[n+ 16]=  vx[n+ 33]+ u;
      vx[n+ 17]=  vx[n+ 34]+ u;
      vx[n+ 18]=  vx[n+ 35]+ u;
      vx[n+ 19]=  vx[n+ 36]+ u;
      vx[n+ 20]=  vx[n+ 38]+ u;
      vx[n+ 21]=  vx[n+ 39]+ u;
      vx[n+ 22]=  vx[n+ 40]+ u;
      vx[n+ 23]=  vx[n+ 41]+ u;

// D section+       

      s= vtx_t(v);
      s*= g;

      vx[n+ 46]= vx[n+ 24]+ s;
      vx[n+ 47]= vx[n+ 25]+ s;
      vx[n+ 48]= vx[n+ 26]+ s;
      vx[n+ 49]= vx[n+ 27]+ s;
      vx[n+ 50]= vx[n+ 28]+ s;
      vx[n+ 51]= vx[n+ 29]+ s;
      vx[n+ 52]= vx[n+ 30]+ s;
      vx[n+ 53]= vx[n+ 31]+ s;
      vx[n+ 54]= vx[n+ 32]+ s;
      vx[n+ 55]= vx[n+ 33]+ s;
      vx[n+ 56]= vx[n+ 34]+ s;
      vx[n+ 57]= vx[n+ 35]+ s;
      vx[n+ 58]= vx[n+ 36]+ s;
      vx[n+ 59]= vx[n+ 37]+ s;
      vx[n+ 60]= vx[n+ 38]+ s;
      vx[n+ 61]= vx[n+ 39]+ s;
      vx[n+ 62]= vx[n+ 40]+ s;
      vx[n+ 63]= vx[n+ 41]+ s;
      vx[n+ 64]= vx[n+ 42]+ s;
      vx[n+ 65]= vx[n+ 43]+ s;
      vx[n+ 66]= vx[n+ 44]+ s;
      vx[n+ 67]= vx[n+ 45]+ s;
                      
// E section+ 

      c= 1.3*0.8*vtx_t(v);
      c+=  r;

      orth( c,    z,vx[n+ 24], vx[n+ 68] );
      orth( c,    z,vx[n+ 27], vx[n+ 69] );
      orth( c,    z,vx[n+ 28], vx[n+ 70] );
      orth( c,    z,vx[n+ 31], vx[n+ 71] );
      orth( c,    z,vx[n+ 32], vx[n+ 72] );
      orth( c,    z,vx[n+ 33], vx[n+ 73] );
      orth( c,    z,vx[n+ 36], vx[n+ 74] );
      orth( c,    z,vx[n+ 37], vx[n+ 75] );
      orth( c,    z,vx[n+ 38], vx[n+ 76] );
      orth( c,    z,vx[n+ 41], vx[n+ 77] );
      orth( c,    z,vx[n+ 42], vx[n+ 78] );
      orth( c,    z,vx[n+ 45], vx[n+ 79] );

// F section+ 
      c= 1.3* vtx_t(v);
      c+=  r;

      orth( c,    z,vx[n+ 68], vx[n+ 80] );
      orth( c,    z,vx[n+ 69], vx[n+ 81] );
      orth( c,    z,vx[n+ 70], vx[n+ 82] );
      orth( c,    z,vx[n+ 71], vx[n+ 83] );
      orth( c,    z,vx[n+ 72], vx[n+ 84] );
      orth( c,    z,vx[n+ 73], vx[n+ 85] );
      orth( c,    z,vx[n+ 74], vx[n+ 86] );
      orth( c,    z,vx[n+ 75], vx[n+ 87] );
      orth( c,    z,vx[n+ 76], vx[n+ 88] );
      orth( c,    z,vx[n+ 77], vx[n+ 89] );
      orth( c,    z,vx[n+ 78], vx[n+ 90] );
      orth( c,    z,vx[n+ 79], vx[n+ 91] );

// G section+ 

      c= vtx_t(v);
      c*= i13;
      c+=  r;

      orth( c,    z,vx[n+ 26], vx[n+ 92] );
      orth( c,    z,vx[n+ 27], vx[n+ 93] );
      orth( c,    z,vx[n+ 30], vx[n+ 94] );
      orth( c,    z,vx[n+ 31], vx[n+ 95] );
      orth( c,    z,vx[n+ 35], vx[n+ 96] );
      orth( c,    z,vx[n+ 36], vx[n+ 97] );
      orth( c,    z,vx[n+ 37], vx[n+ 98] );
      orth( c,    z,vx[n+ 40], vx[n+ 99] );
      orth( c,    z,vx[n+ 41], vx[n+100] );
      orth( c,    z,vx[n+ 44], vx[n+101] );
      orth( c,    z,vx[n+ 45], vx[n+102] );

// H section+ 

      c= vtx_t(v);
      c*= i23;
      c+=  r;

      orth( c,    z,vx[n+ 26], vx[n+103] );
      orth( c,    z,vx[n+ 27], vx[n+104] );
      orth( c,    z,vx[n+ 30], vx[n+105] );
      orth( c,    z,vx[n+ 31], vx[n+106] );
      orth( c,    z,vx[n+ 35], vx[n+107] );
      orth( c,    z,vx[n+ 36], vx[n+108] );
      orth( c,    z,vx[n+ 37], vx[n+109] );
      orth( c,    z,vx[n+ 40], vx[n+110] );
      orth( c,    z,vx[n+ 41], vx[n+111] );
      orth( c,    z,vx[n+ 44], vx[n+112] );
      orth( c,    z,vx[n+ 45], vx[n+113] );

// form hexahedra

      m= hx.n; hx.n+= 90; hx.resize( hdum );

      hex( m+  0, n+ 12,n+ 13,n+ 16,n+ 17,n+  0,n+  1,n+  4,n+  5,  n+ 12,n+ 13,n+ 16,n+ 17,n+  0,n+  1,n+  4,n+  5 );
      hex( m+  1, n+ 13,n+ 14,n+ 17,n+ 18,n+  1,n+  2,n+  5,n+  6,  n+ 13,n+ 14,n+ 17,n+ 18,n+  1,n+  2,n+  5,n+  6 );
      hex( m+  2, n+ 14,n+ 15,n+ 18,n+ 19,n+  2,n+  3,n+  6,n+  7,  n+ 14,n+ 15,n+ 18,n+ 19,n+  2,n+  3,n+  6,n+  7 );
      hex( m+  3, n+ 16,n+ 17,n+ 20,n+ 21,n+  4,n+  5,n+  8,n+  9,  n+ 16,n+ 17,n+ 20,n+ 21,n+  4,n+  5,n+  8,n+  9 );
      hex( m+  4, n+ 17,n+ 18,n+ 21,n+ 22,n+  5,n+  6,n+  9,n+ 10,  n+ 17,n+ 18,n+ 21,n+ 22,n+  5,n+  6,n+  9,n+ 10 );
      hex( m+  5, n+ 18,n+ 19,n+ 22,n+ 23,n+  6,n+  7,n+ 10,n+ 11,  n+ 18,n+ 19,n+ 22,n+ 23,n+  6,n+  7,n+ 10,n+ 11 );
      hex( m+  6, n+ 24,n+ 25,n+ 28,n+ 29,n+  0,n+  1,n+ 12,n+ 13,  n+ 24,n+ 25,n+ 28,n+ 29,n+  0,n+  1,n+ 12,n+ 13 );
                                                                                                                   
      hex( m+  7, n+ 25,n+ 26,n+ 29,n+ 30,n+  1,n+  2,n+ 13,n+ 14,  n+ 25,n+ 26,n+ 29,n+ 30,n+  1,n+  2,n+ 13,n+ 14 );
      hex( m+  8, n+ 26,n+ 27,n+ 30,n+ 31,n+  2,n+  3,n+ 14,n+ 15,  n+ 26,n+ 27,n+ 30,n+ 31,n+  2,n+  3,n+ 14,n+ 15 );
      hex( m+  9, n+ 24,n+ 28,n+ 32,n+ 33,n+  0,n+ 12,n+  4,n+ 16,  n+ 24,n+ 28,n+ 32,n+ 33,n+  0,n+ 12,n+  4,n+ 16 );
      hex( m+ 10, n+ 28,n+ 29,n+ 33,n+ 34,n+ 12,n+ 13,n+ 16,n+ 17,  n+ 28,n+ 29,n+ 33,n+ 34,n+ 12,n+ 13,n+ 16,n+ 17 );
      hex( m+ 11, n+ 29,n+ 30,n+ 34,n+ 35,n+ 13,n+ 14,n+ 17,n+ 18,  n+ 29,n+ 30,n+ 34,n+ 35,n+ 13,n+ 14,n+ 17,n+ 18 );
      hex( m+ 12, n+ 30,n+ 31,n+ 35,n+ 36,n+ 14,n+ 15,n+ 18,n+ 19,  n+ 30,n+ 31,n+ 35,n+ 36,n+ 14,n+ 15,n+ 18,n+ 19 );
      hex( m+ 13, n+ 32,n+ 33,n+ 42,n+ 38,n+  4,n+ 16,n+  8,n+ 20,  n+ 32,n+ 33,n+ 42,n+ 38,n+  4,n+ 16,n+  8,n+ 20 );
      hex( m+ 14, n+ 33,n+ 34,n+ 38,n+ 39,n+ 16,n+ 17,n+ 20,n+ 21,  n+ 33,n+ 34,n+ 38,n+ 39,n+ 16,n+ 17,n+ 20,n+ 21 );
      hex( m+ 15, n+ 34,n+ 35,n+ 39,n+ 40,n+ 17,n+ 18,n+ 21,n+ 22,  n+ 34,n+ 35,n+ 39,n+ 40,n+ 17,n+ 18,n+ 21,n+ 22 );
      hex( m+ 16, n+ 35,n+ 36,n+ 40,n+ 41,n+ 18,n+ 19,n+ 22,n+ 23,  n+ 35,n+ 36,n+ 40,n+ 41,n+ 18,n+ 19,n+ 22,n+ 23 );
      hex( m+ 17, n+ 36,n+ 37,n+ 41,n+ 45,n+ 19,n+  7,n+ 23,n+ 11,  n+ 36,n+ 37,n+ 41,n+ 45,n+ 19,n+  7,n+ 23,n+ 11 );
      hex( m+ 18, n+ 38,n+ 39,n+ 42,n+ 43,n+ 20,n+ 21,n+  8,n+  9,  n+ 38,n+ 39,n+ 42,n+ 43,n+ 20,n+ 21,n+  8,n+  9 );
      hex( m+ 19, n+ 39,n+ 40,n+ 43,n+ 44,n+ 21,n+ 22,n+  9,n+ 10,  n+ 39,n+ 40,n+ 43,n+ 44,n+ 21,n+ 22,n+  9,n+ 10 );
      hex( m+ 20, n+ 40,n+ 41,n+ 44,n+ 45,n+ 22,n+ 23,n+ 10,n+ 11,  n+ 40,n+ 41,n+ 44,n+ 45,n+ 22,n+ 23,n+ 10,n+ 11 );
      hex( m+ 21, n+ 31,n+ 27,n+ 36,n+ 37,n+ 15,n+  3,n+ 19,n+  7,  n+ 31,n+ 27,n+ 36,n+ 37,n+ 15,n+  3,n+ 19,n+  7 );
                                                                                                                   
      hex( m+ 22, n+ 46,n+ 47,n+ 50,n+ 51,n+ 24,n+ 25,n+ 28,n+ 29,  n+ 46,n+ 47,n+ 50,n+ 51,n+ 24,n+ 25,n+ 28,n+ 29 );
      hex( m+ 23, n+ 47,n+ 48,n+ 51,n+ 52,n+ 25,n+ 26,n+ 29,n+ 30,  n+ 47,n+ 48,n+ 51,n+ 52,n+ 25,n+ 26,n+ 29,n+ 30 );
      hex( m+ 24, n+ 48,n+ 49,n+ 52,n+ 53,n+ 26,n+ 27,n+ 30,n+ 31,  n+ 48,n+ 49,n+ 52,n+ 53,n+ 26,n+ 27,n+ 30,n+ 31 );
      hex( m+ 25, n+ 46,n+ 50,n+ 54,n+ 55,n+ 24,n+ 28,n+ 32,n+ 33,  n+ 46,n+ 50,n+ 54,n+ 55,n+ 24,n+ 28,n+ 32,n+ 33 );
      hex( m+ 26, n+ 50,n+ 51,n+ 55,n+ 56,n+ 28,n+ 29,n+ 33,n+ 34,  n+ 50,n+ 51,n+ 55,n+ 56,n+ 28,n+ 29,n+ 33,n+ 34 );
      hex( m+ 27, n+ 51,n+ 52,n+ 56,n+ 57,n+ 29,n+ 30,n+ 34,n+ 35,  n+ 51,n+ 52,n+ 56,n+ 57,n+ 29,n+ 30,n+ 34,n+ 35 );
      hex( m+ 28, n+ 52,n+ 53,n+ 57,n+ 58,n+ 30,n+ 31,n+ 35,n+ 36,  n+ 52,n+ 53,n+ 57,n+ 58,n+ 30,n+ 31,n+ 35,n+ 36 );
      hex( m+ 29, n+ 53,n+ 49,n+ 58,n+ 59,n+ 31,n+ 27,n+ 36,n+ 37,  n+ 53,n+ 49,n+ 58,n+ 59,n+ 31,n+ 27,n+ 36,n+ 37 );
      hex( m+ 30, n+ 54,n+ 55,n+ 64,n+ 60,n+ 32,n+ 33,n+ 42,n+ 38,  n+ 54,n+ 55,n+ 64,n+ 60,n+ 32,n+ 33,n+ 42,n+ 38 );
      hex( m+ 31, n+ 55,n+ 56,n+ 60,n+ 61,n+ 33,n+ 34,n+ 38,n+ 39,  n+ 55,n+ 56,n+ 60,n+ 61,n+ 33,n+ 34,n+ 38,n+ 39 );
      hex( m+ 32, n+ 56,n+ 57,n+ 61,n+ 62,n+ 34,n+ 35,n+ 39,n+ 40,  n+ 56,n+ 57,n+ 61,n+ 62,n+ 34,n+ 35,n+ 39,n+ 40 );
      hex( m+ 33, n+ 57,n+ 58,n+ 62,n+ 63,n+ 35,n+ 36,n+ 40,n+ 41,  n+ 57,n+ 58,n+ 62,n+ 63,n+ 35,n+ 36,n+ 40,n+ 41 );
      hex( m+ 34, n+ 58,n+ 59,n+ 63,n+ 67,n+ 36,n+ 37,n+ 41,n+ 45,  n+ 58,n+ 59,n+ 63,n+ 67,n+ 36,n+ 37,n+ 41,n+ 45 );
      hex( m+ 35, n+ 60,n+ 61,n+ 64,n+ 65,n+ 38,n+ 39,n+ 42,n+ 43,  n+ 60,n+ 61,n+ 64,n+ 65,n+ 38,n+ 39,n+ 42,n+ 43 );
      hex( m+ 36, n+ 61,n+ 62,n+ 65,n+ 66,n+ 39,n+ 40,n+ 43,n+ 44,  n+ 61,n+ 62,n+ 65,n+ 66,n+ 39,n+ 40,n+ 43,n+ 44 );
      hex( m+ 37, n+ 62,n+ 63,n+ 66,n+ 67,n+ 40,n+ 41,n+ 44,n+ 45,  n+ 62,n+ 63,n+ 66,n+ 67,n+ 40,n+ 41,n+ 44,n+ 45 );
                                                                                                                   
      hex( m+ 38, n+ 80,n+ 81,n+ 82,n+ 83,n+ 68,n+ 69,n+ 70,n+ 71,  n+ 80,n+ 81,n+ 82,n+ 83,n+ 68,n+ 69,n+ 70,n+ 71 );
      hex( m+ 39, n+ 80,n+ 82,n+ 84,n+ 85,n+ 68,n+ 70,n+ 72,n+ 73,  n+ 80,n+ 82,n+ 84,n+ 85,n+ 68,n+ 70,n+ 72,n+ 73 );
      hex( m+ 40, n+ 82,n+ 83,n+ 85,n+ 86,n+ 70,n+ 71,n+ 73,n+ 74,  n+ 82,n+ 83,n+ 85,n+ 86,n+ 70,n+ 71,n+ 73,n+ 74 );
      hex( m+ 41, n+ 83,n+ 81,n+ 86,n+ 87,n+ 71,n+ 69,n+ 74,n+ 75,  n+ 83,n+ 81,n+ 86,n+ 87,n+ 71,n+ 69,n+ 74,n+ 75 );
      hex( m+ 42, n+ 84,n+ 85,n+ 90,n+ 88,n+ 72,n+ 73,n+ 78,n+ 76,  n+ 84,n+ 85,n+ 90,n+ 88,n+ 72,n+ 73,n+ 78,n+ 76 );
      hex( m+ 43, n+ 85,n+ 86,n+ 88,n+ 89,n+ 73,n+ 74,n+ 76,n+ 77,  n+ 85,n+ 86,n+ 88,n+ 89,n+ 73,n+ 74,n+ 76,n+ 77 );
      hex( m+ 44, n+ 86,n+ 87,n+ 89,n+ 91,n+ 74,n+ 75,n+ 77,n+ 79,  n+ 86,n+ 87,n+ 89,n+ 91,n+ 74,n+ 75,n+ 77,n+ 79 );
      hex( m+ 45, n+ 88,n+ 89,n+ 90,n+ 91,n+ 76,n+ 77,n+ 78,n+ 79,  n+ 88,n+ 89,n+ 90,n+ 91,n+ 76,n+ 77,n+ 78,n+ 79 );
                                                                                                                   
      hex( m+ 46, n+ 92,n+ 93,n+ 94,n+ 95,n+ 48,n+ 49,n+ 52,n+ 53,  n+ 92,n+ 93,n+ 94,n+ 95,n+ 48,n+ 49,n+ 52,n+ 53 );
      hex( m+ 47, n+ 94,n+ 95,n+ 96,n+ 97,n+ 52,n+ 53,n+ 57,n+ 58,  n+ 94,n+ 95,n+ 96,n+ 97,n+ 52,n+ 53,n+ 57,n+ 58 );
      hex( m+ 48, n+ 95,n+ 93,n+ 97,n+ 98,n+ 53,n+ 49,n+ 58,n+ 59,  n+ 95,n+ 93,n+ 97,n+ 98,n+ 53,n+ 49,n+ 58,n+ 59 );
      hex( m+ 49, n+ 96,n+ 97,n+ 99,n+100,n+ 57,n+ 58,n+ 62,n+ 63,  n+ 96,n+ 97,n+ 99,n+100,n+ 57,n+ 58,n+ 62,n+ 63 );
      hex( m+ 50, n+ 97,n+ 98,n+100,n+102,n+ 58,n+ 59,n+ 63,n+ 67,  n+ 97,n+ 98,n+100,n+102,n+ 58,n+ 59,n+ 63,n+ 67 );
      hex( m+ 51, n+ 99,n+100,n+101,n+102,n+ 62,n+ 63,n+ 66,n+ 67,  n+ 99,n+100,n+101,n+102,n+ 62,n+ 63,n+ 66,n+ 67 );
                                                                                                                   
      hex( m+ 52, n+103,n+104,n+105,n+106,n+ 92,n+ 93,n+ 94,n+ 95,  n+103,n+104,n+105,n+106,n+ 92,n+ 93,n+ 94,n+ 95 );
      hex( m+ 53, n+105,n+106,n+107,n+108,n+ 94,n+ 95,n+ 96,n+ 97,  n+105,n+106,n+107,n+108,n+ 94,n+ 95,n+ 96,n+ 97 );
      hex( m+ 54, n+106,n+104,n+108,n+109,n+ 95,n+ 93,n+ 97,n+ 98,  n+106,n+104,n+108,n+109,n+ 95,n+ 93,n+ 97,n+ 98 );
      hex( m+ 55, n+107,n+108,n+110,n+111,n+ 96,n+ 97,n+ 99,n+100,  n+107,n+108,n+110,n+111,n+ 96,n+ 97,n+ 99,n+100 );
      hex( m+ 56, n+108,n+109,n+111,n+113,n+ 97,n+ 98,n+100,n+102,  n+108,n+109,n+111,n+113,n+ 97,n+ 98,n+100,n+102 );
      hex( m+ 57, n+110,n+111,n+112,n+113,n+ 99,n+100,n+101,n+102,  n+110,n+111,n+112,n+113,n+ 99,n+100,n+101,n+102 );
                                                                                                                   
      hex( m+ 58, n+ 68,n+ 69,n+ 70,n+ 71,n+103,n+104,n+105,n+106,  n+ 68,n+ 69,n+ 70,n+ 71,n+103,n+104,n+105,n+106 );
      hex( m+ 59, n+ 70,n+ 71,n+ 73,n+ 74,n+105,n+106,n+107,n+108,  n+ 70,n+ 71,n+ 73,n+ 74,n+105,n+106,n+107,n+108 );
      hex( m+ 60, n+ 71,n+ 69,n+ 74,n+ 75,n+106,n+104,n+108,n+109,  n+ 71,n+ 69,n+ 74,n+ 75,n+106,n+104,n+108,n+109 );
      hex( m+ 61, n+ 73,n+ 74,n+ 76,n+ 77,n+107,n+108,n+110,n+111,  n+ 73,n+ 74,n+ 76,n+ 77,n+107,n+108,n+110,n+111 );
      hex( m+ 62, n+ 74,n+ 75,n+ 77,n+ 79,n+108,n+109,n+111,n+113,  n+ 74,n+ 75,n+ 77,n+ 79,n+108,n+109,n+111,n+113 );
      hex( m+ 63, n+ 76,n+ 77,n+ 78,n+ 79,n+110,n+111,n+112,n+113,  n+ 76,n+ 77,n+ 78,n+ 79,n+110,n+111,n+112,n+113 );
                                                                                                                   
      hex( m+ 64, n+ 68,n+ 70,n+ 72,n+ 73,n+ 46,n+ 50,n+ 54,n+ 55,  n+ 68,n+ 70,n+ 72,n+ 73,n+ 46,n+ 50,n+ 54,n+ 55 );
      hex( m+ 65, n+ 72,n+ 73,n+ 78,n+ 76,n+ 54,n+ 55,n+ 64,n+ 60,  n+ 72,n+ 73,n+ 78,n+ 76,n+ 54,n+ 55,n+ 64,n+ 60 );
                                                                                                                   
      hex( m+ 66, n+ 68,n+103,n+ 70,n+105,n+ 46,n+ 47,n+ 50,n+ 51,  n+ 68,n+103,n+ 70,n+105,n+ 46,n+ 47,n+ 50,n+ 51 );
      hex( m+ 67, n+ 70,n+105,n+ 73,n+107,n+ 50,n+ 51,n+ 55,n+ 56,  n+ 70,n+105,n+ 73,n+107,n+ 50,n+ 51,n+ 55,n+ 56 );
      hex( m+ 68, n+ 73,n+107,n+ 76,n+110,n+ 55,n+ 56,n+ 60,n+ 61,  n+ 73,n+107,n+ 76,n+110,n+ 55,n+ 56,n+ 60,n+ 61 );
      hex( m+ 69, n+ 76,n+110,n+ 78,n+112,n+ 60,n+ 61,n+ 64,n+ 65,  n+ 76,n+110,n+ 78,n+112,n+ 60,n+ 61,n+ 64,n+ 65 );
                                                                                                                   
      hex( m+ 70, n+103,n+ 92,n+105,n+ 94,n+ 47,n+ 48,n+ 51,n+ 52,  n+103,n+ 92,n+105,n+ 94,n+ 47,n+ 48,n+ 51,n+ 52 );
      hex( m+ 71, n+105,n+ 94,n+107,n+ 96,n+ 51,n+ 52,n+ 56,n+ 57,  n+105,n+ 94,n+107,n+ 96,n+ 51,n+ 52,n+ 56,n+ 57 );
      hex( m+ 72, n+107,n+ 96,n+110,n+ 99,n+ 56,n+ 57,n+ 61,n+ 62,  n+107,n+ 96,n+110,n+ 99,n+ 56,n+ 57,n+ 61,n+ 62 );
      hex( m+ 73, n+110,n+ 99,n+112,n+101,n+ 61,n+ 62,n+ 65,n+ 66,  n+110,n+ 99,n+112,n+101,n+ 61,n+ 62,n+ 65,n+ 66 );

// connectin+ g hexahedra

      hex( m+ 74, cv.v[ 0],cv.v[ 8],n+ 24   ,n+ 25   ,cv.v[ 4],cv.v[10],n+  0   ,n+  1   ,  cv.v[ 0],cv.v[ 8],n+ 24   ,n+ 25   ,cv.v[ 4],cv.v[10],n+  0   ,n+  1    );
      hex( m+ 75, cv.v[ 8],cv.v[12],n+ 25   ,n+ 26   ,cv.v[10],cv.v[14],n+  1   ,n+  2   ,  cv.v[ 8],cv.v[12],n+ 25   ,n+ 26   ,cv.v[10],cv.v[14],n+  1   ,n+  2    );
      hex( m+ 76, cv.v[12],cv.v[ 1],n+ 26   ,n+ 27   ,cv.v[14],cv.v[ 5],n+  2   ,n+  3   ,  cv.v[12],cv.v[ 1],n+ 26   ,n+ 27   ,cv.v[14],cv.v[ 5],n+  2   ,n+  3    );

      hex( m+ 77, cv.v[ 0],n+ 24   ,cv.v[16],n+ 32   ,cv.v[ 4],n+  0   ,cv.v[18],n+  4   ,  cv.v[ 0],n+ 24   ,cv.v[16],n+ 32   ,cv.v[ 4],n+  0   ,cv.v[18],n+  4    );

      hex( m+ 78, n+  0   ,n+  1   ,n+  4   ,n+  5   ,cv.v[ 4],cv.v[10],cv.v[18],cv.v[21],  n+  0   ,n+  1   ,n+  4   ,n+  5   ,cv.v[ 4],cv.v[10],cv.v[18],cv.v[21] );
      hex( m+ 79, n+  1   ,n+  2   ,n+  5   ,n+  6   ,cv.v[10],cv.v[14],cv.v[21],cv.v[23],  n+  1   ,n+  2   ,n+  5   ,n+  6   ,cv.v[10],cv.v[14],cv.v[21],cv.v[23] );
      hex( m+ 80, n+  2   ,n+  3   ,n+  6   ,n+  7   ,cv.v[14],cv.v[ 5],cv.v[23],cv.v[19],  n+  2   ,n+  3   ,n+  6   ,n+  7   ,cv.v[14],cv.v[ 5],cv.v[23],cv.v[19] );

      hex( m+ 81, n+ 27   ,cv.v[ 1],n+ 37   ,cv.v[17],n+ 3    ,cv.v[ 5],n+  7   ,cv.v[19],  n+ 27   ,cv.v[ 1],n+ 37   ,cv.v[17],n+ 3    ,cv.v[ 5],n+  7   ,cv.v[19] );

      hex( m+ 82, cv.v[16],n+ 32   ,cv.v[ 2],n+ 42   ,cv.v[18],n+  4   ,cv.v[ 6],n+  8   ,  cv.v[16],n+ 32   ,cv.v[ 2],n+ 42   ,cv.v[18],n+  4   ,cv.v[ 6],n+  8    );

      hex( m+ 83, n+  4   ,n+  5   ,n+  8   ,n+  9   ,cv.v[18],cv.v[21],cv.v[ 6],cv.v[11],  n+  4   ,n+  5   ,n+  8   ,n+  9   ,cv.v[18],cv.v[21],cv.v[ 6],cv.v[11] );
      hex( m+ 84, n+  5   ,n+  6   ,n+  9   ,n+ 10   ,cv.v[21],cv.v[23],cv.v[11],cv.v[15],  n+  5   ,n+  6   ,n+  9   ,n+ 10   ,cv.v[21],cv.v[23],cv.v[11],cv.v[15] );
      hex( m+ 85, n+  6   ,n+  7   ,n+ 10   ,n+ 11   ,cv.v[23],cv.v[19],cv.v[15],cv.v[ 7],  n+  6   ,n+  7   ,n+ 10   ,n+ 11   ,cv.v[23],cv.v[19],cv.v[15],cv.v[ 7] );

      hex( m+ 86, n+ 37   ,cv.v[17],n+ 45   ,cv.v[ 3],n+  7   ,cv.v[19],n+ 11   ,cv.v[ 7],  n+ 37   ,cv.v[17],n+ 45   ,cv.v[ 3],n+  7   ,cv.v[19],n+ 11   ,cv.v[ 7] );

      hex( m+ 87, n+ 42   ,n+ 43   ,cv.v[ 2],cv.v[ 9],n+  8   ,n+  9   ,cv.v[ 6],cv.v[11],  n+ 42   ,n+ 43   ,cv.v[ 2],cv.v[ 9],n+  8   ,n+  9   ,cv.v[ 6],cv.v[11] );
      hex( m+ 88, n+ 43   ,n+ 44   ,cv.v[ 9],cv.v[13],n+  9   ,n+ 10   ,cv.v[11],cv.v[15],  n+ 43   ,n+ 44   ,cv.v[ 9],cv.v[13],n+  9   ,n+ 10   ,cv.v[11],cv.v[15] );
      hex( m+ 89, n+ 44   ,n+ 45   ,cv.v[13],cv.v[ 3],n+ 10   ,n+ 11   ,cv.v[15],cv.v[ 7],  n+ 44   ,n+ 45   ,cv.v[13],cv.v[ 3],n+ 10   ,n+ 11   ,cv.v[15],cv.v[ 7] );

// new faces

      quads( cv.v, m,m+90 );

// Now attach faces to boun+ daries

      attach( mask[0], m+ 25,0, -1,-1,-1 );
      attach( mask[0], m+ 30,0, -1,-1,-1 );
      attach( mask[0], m+ 39,0, -1,-1,-1 );
      attach( mask[0], m+ 42,0, -1,-1,-1 );
      attach( mask[0], m+ 64,0, -1,-1,-1 );
      attach( mask[0], m+ 65,0, -1,-1,-1 );
  
      attach( mask[1], m+ 29,1, -1,-1,-1 );
      attach( mask[1], m+ 34,1, -1,-1,-1 );
      attach( mask[1], m+ 41,1, -1,-1,-1 );
      attach( mask[1], m+ 44,1, -1,-1,-1 );
      attach( mask[1], m+ 48,1, -1,-1,-1 );
      attach( mask[1], m+ 50,1, -1,-1,-1 );
      attach( mask[1], m+ 54,1, -1,-1,-1 );
      attach( mask[1], m+ 56,1, -1,-1,-1 );
      attach( mask[1], m+ 60,1, -1,-1,-1 );
      attach( mask[1], m+ 62,1, -1,-1,-1 );
  
      attach( mask[2], m+ 22,2, -1,-1,-1 );
      attach( mask[2], m+ 23,2, -1,-1,-1 );
      attach( mask[2], m+ 24,2, -1,-1,-1 );
      attach( mask[2], m+ 38,2, -1,-1,-1 );
      attach( mask[2], m+ 46,2, -1,-1,-1 );
      attach( mask[2], m+ 52,2, -1,-1,-1 );
      attach( mask[2], m+ 58,2, -1,-1,-1 );
      attach( mask[2], m+ 66,2, -1,-1,-1 );
      attach( mask[2], m+ 70,2, -1,-1,-1 );
  
      attach( mask[3], m+ 35,3, -1,-1,-1 );
      attach( mask[3], m+ 36,3, -1,-1,-1 );
      attach( mask[3], m+ 37,3, -1,-1,-1 );
      attach( mask[3], m+ 45,3, -1,-1,-1 );
      attach( mask[3], m+ 51,3, -1,-1,-1 );
      attach( mask[3], m+ 57,3, -1,-1,-1 );
      attach( mask[3], m+ 63,3, -1,-1,-1 );
      attach( mask[3], m+ 69,3, -1,-1,-1 );
      attach( mask[3], m+ 73,3, -1,-1,-1 );
  
      attach( mask[4], m+ 38,4, -1,-1,-1 );
      attach( mask[4], m+ 39,4, -1,-1,-1 );
      attach( mask[4], m+ 40,4, -1,-1,-1 );
      attach( mask[4], m+ 41,4, -1,-1,-1 );
      attach( mask[4], m+ 42,4, -1,-1,-1 );
      attach( mask[4], m+ 43,4, -1,-1,-1 );
      attach( mask[4], m+ 44,4, -1,-1,-1 );
      attach( mask[4], m+ 45,4, -1,-1,-1 );
  
      attach( cv.q4[0].v[0], m+ 74,4, cv.q4[0].v[1], cv.q4[0].v[2], cv.q4[0].v[3] );
      attach( cv.q4[0].v[0], m+ 77,4, cv.q4[0].v[1], cv.q4[0].v[2], cv.q4[0].v[3] );
  
      attach( cv.q4[1].v[0], m+ 75,4, cv.q4[1].v[1], cv.q4[1].v[2], cv.q4[1].v[3] );
  
      attach( cv.q4[2].v[0], m+ 76,4, cv.q4[2].v[1], cv.q4[2].v[2], cv.q4[2].v[3] );
      attach( cv.q4[2].v[0], m+ 81,4, cv.q4[2].v[1], cv.q4[2].v[2], cv.q4[2].v[3] );
  
      attach( cv.q4[3].v[0], m+ 82,4, cv.q4[3].v[1], cv.q4[3].v[2], cv.q4[3].v[3] );
      attach( cv.q4[3].v[0], m+ 87,4, cv.q4[3].v[1], cv.q4[3].v[2], cv.q4[3].v[3] );
  
      attach( cv.q4[4].v[0], m+ 88,4, cv.q4[4].v[1], cv.q4[4].v[2], cv.q4[4].v[3] );
  
      attach( cv.q4[5].v[0], m+ 86,4, cv.q4[5].v[1], cv.q4[5].v[2], cv.q4[5].v[3] );
      attach( cv.q4[5].v[0], m+ 89,4, cv.q4[5].v[1], cv.q4[5].v[2], cv.q4[5].v[3] );


      bz[ mask[0] ].n= true;
      bz[ mask[1] ].n= true;
      bz[ mask[2] ].n= true;
      bz[ mask[3] ].n= true;
      bz[ mask[4] ].n= true;

      return;
      
  }
