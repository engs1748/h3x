#  include <hmesh/hmesh.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

      void hmesh_t::collapse( INT_ m, dir_t dir, VINT_ &list, VINT_4 &mkv, VINT_4 &mkh )
     {

         INT_  a,b, c,d, i,j, k, l,n, o,r, u,v,w;

// identify neighboring elements and faces

/*       a= hx[m].face( !dir );
         b= hx[m].face(  dir );*/

         a= face( !dir );
         b= face(  dir );

         hx[m].neig( qv.data(),a, l,c );
         hx[m].neig( qv.data(),b, n,d );

         w= mkh.append( -1 );
         mkh[w][0]= l; //hx[l].id;
         mkh[w][1]= c;
         mkh[w][2]= n; //hx[n].id;
         mkh[w][3]= d;

// change numbering of hex m

         r= dir.dir();
         o= dir.sign(); 

         for( k=0;k<4;k++ )
        {
            i= opp[!o][r][k];
            j= opp[ o][r][k];

            u= hx[m].v[i];
            v= hx[m].v[j];

            if( list[u] == -1 )
           { 
               w= mkv.append(-1);

               mkv[w][0]= u;
               mkv[w][1]= v;
              
               u= hx[m].x[i];
               v= hx[m].x[j];

               mkv[w][2]= u;
               mkv[w][3]= v;

               list[u]= w;
              
           }
            else
           {
               w= list[u];

               assert( mkv[w][0] == u );
               assert( mkv[w][1] == v );

               u= hx[m].x[i];
               v= hx[m].x[j];

               assert( mkv[w][2] == u );
               assert( mkv[w][3] == v );

           }

        }

         return;
     };


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

      void hmesh_t::collapse( INT_ m, dir_t d0, dir_t d1, dir_t d2, VINT_ &list, VINT_4 &mkv, VINT_4 &mkh )
     {

         INT_  a,b, c,d, l,n, r,s,t, u,w,x,y,z;

// identify neighboring elements and faces

/*       a= hx[m].face( d0 );
         b= hx[m].face( d1 );*/

         a= face( d0 );
         b= face( d1 );

         hx[m].neig( qv.data(),a, l,c );
         hx[m].neig( qv.data(),b, n,d );

// change numbering of hex m

// bottom side

         r= coor( !d0, d1,!d2);
         s= coor(  d0,!d1,!d2);
         t= coor( !d0,!d1,!d2);

         w= hx[m].v[t];
         z= hx[m].x[t];

         u= hx[m].v[r];
         x= hx[m].x[r];

         if( list[u] == -1 )
        {
            y= mkv.append(-1);
            mkv[y][0]= u;
            mkv[y][1]= w;
            mkv[y][2]= x;
            mkv[y][3]= z;
            list[u]= y;
        }

         u= hx[m].v[s];
         x= hx[m].x[s];

         if( list[u] == -1 )
        {
            y= mkv.append(-1);
            mkv[y][0]= u;
            mkv[y][1]= w;
            mkv[y][2]= x;
            mkv[y][3]= z;
            list[u]= y;
        }

// top side

         r= coor( !d0, d1, d2);
         s= coor(  d0,!d1, d2);
         t= coor( !d0,!d1, d2);

         w= hx[m].v[t];
         z= hx[m].x[t];

         u= hx[m].v[r];
         x= hx[m].x[r];

         if( list[u] == -1 )
        {
            y= mkv.append(-1);
            mkv[y][0]= u;
            mkv[y][1]= w;
            mkv[y][2]= x;
            mkv[y][3]= z;
            list[u]= y;
        }

         u= hx[m].v[s];
         x= hx[m].x[s];

         if( list[u] == -1 )
        {
            y= mkv.append(-1);
            mkv[y][0]= u;
            mkv[y][1]= w;
            mkv[y][2]= x;
            mkv[y][3]= z;
            list[u]= y;
        }

         y= mkh.append(-1);
         mkh[y][0]= l;
         mkh[y][1]= c;
         mkh[y][2]= n;
         mkh[y][3]= d;

         return;
     };


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void hmesh_t::collapse( INT_ i0, dir_t g, VINT_ &list )
  {

      INT_                 a,b, j,k, x,y, n;
      VINT_ wrk(vt.n,-1);
      VINT_4 mkv;
      VINT_4 mkh;
      VINT_2 l1,l2;
      VINT_2 l3,l4;

//    printf( "this?\n" );

      INT_ mask[6]={-1,-1,-1,-1,-1,-1};
      vsize_t<dir_t,DLEN_H> c(hx.n,ddum);

      for( k=0;k<hx.n;k++ ){ c[k].dum0=0; };

      n= indx[i0];
      assert( n > -1 );

      assert( list.n==0 );

// find hexahedra to collapse and the direction of collapse

      spread( n,g, list,c );

      broken();
// mark vertices and faces to merge

      for( j=0;j<list.n;j++ )
     {
         k=list[j]; 
         collapse( k,c[k], wrk, mkv,mkh ); 
         list[j]= hx[k].id;
         k= mkh[j][0]; mkh[j][0]= hx[k].id;
         k= mkh[j][2]; mkh[j][2]= hx[k].id;
     }
      broken();

// delete collapsed hexahedra

      for( j=0;j<list.n;j++ ){ del( list[j],mask ); }
 
      broken();
// modify numbering of surviving hexahedra

      for( j=0;j<mkv.n;j++ )
     {
         a= mkv[j][0];
         b= mkv[j][1];
         x= mkv[j][2];
         y= mkv[j][3];

         point( a, l1,l2 );
         point( b, l3,l4 );

         substitute( a,x, l1,l2, b,y, l3,l4 );
     }
      broken();

      for( j=0;j<mkh.n;j++ )
     {

         a= mkh[j][0];
         x= mkh[j][1];

         b= mkh[j][2];
         y= mkh[j][3];

         a= indx[a];
         b= indx[b];

         join( a,x, b,y );
     }

      for( j=0;j<mkh.n;j++ )
     {

         a= mkh[j][0];
         x= mkh[j][1];

         b= mkh[j][2];
         y= mkh[j][3];

         a= indx[a];
         b= indx[b];

         claim( a );
         claim( b );
     }

      return;
  }

