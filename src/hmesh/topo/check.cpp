#  include <hmesh/hmesh.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void hmesh_t::check()
  {
      INT_ b,g,i,j,h,k,l,m,n,r;
      REAL_ f;
      INT_4 buf[2];
      INT_4 xbuf[2];

// check boundary connectivity: boundary vertices and manifold geometry near boundaries 
// (i.e. boundary faces only have one hex)

      for( i=0;i<ng;i++ )
     {
         r= 0;
         for( j=0;j<2;j++ )
        {
            k= qv[i].h[j];
            l= qv[i].q[j];
            if( k > -1 )
           {
              
               g= hx[k].q[l].g;
               b= hx[k].q[l].b;

// check boundary and hex record for this face are consistent

               assert( g > -1 );
               assert( b > -1 );

               if( g < (MXGRP-1) )
              {

                  hx[k].npack( l,buf[j],xbuf[j] );

// check vertex records match hex vertex records

                  for( h=0;h<4;h++ )
                 {
                     m= bz[g].bo[b].v[h];
                     assert( bz[g].bx[m].id == xbuf[j][h] );

                 }

// check neighbour data structure is correct

                  for( h=0;h<4;h++ )
                 {
                     m= bz[g].bo[b].n[h];
                     n= bz[g].bo[b].e[h];

                     if( m > -1 )
                    {
                        assert( bz[g].bo[m].n[n] == b );
                        assert( bz[g].bo[m].e[n] == h );
                    }

                 }

              }

               r++;
           }
        }
         assert( r == 1 );
     }

// check internal faces

      for( i=ng;i<qv.n;i++ )
     {
         r= 0;
         for( j=0;j<2;j++ )
        {
            k= qv[i].h[j];
            l= qv[i].q[j];
            if( k > -1 )
           {
               assert( hx[k].q[l].q == i );
               assert( hx[k].q[l].o == j );

               hx[k].pack( l,buf[j],xbuf[j] );

               r++;
           }
        }

// check internal faces are manifold
         assert( r == 2 || r==0 );
         if( r == 2 )
        {

// check internal face vertices are consistent

            assert( buf[0][0]== buf[1][0] );
            assert( buf[0][1]== buf[1][1] );
            assert( buf[0][2]== buf[1][2] );
            assert( buf[0][3]== buf[1][3] );
        }
         else
        {
            assert( false );
        }
     }

// check hex volumes and check that all vertices are correctly attributed

      n= 0;
      for( i=0;i<hx.n;i++ )
     {
         for( j=0;j<6;j++ ){ assert( hx[i].q[j].q > -1 && hx[i].q[j].q < qv.n ); }
         for( j=0;j<8;j++ ){ assert( hx[i].v[j] > -1 && hx[i].v[j] < vt.n ); }

/*       assert( ::vol( vx[ hx[i].x[0] ], vx[ hx[i].x[1] ], vx[ hx[i].x[2] ], vx[ hx[i].x[3] ],
                 vx[ hx[i].x[4] ], vx[ hx[i].x[5] ], vx[ hx[i].x[6] ], vx[ hx[i].x[7] ] ) > 0 );*/

         f= ::vol( vx[ hx[i].x[0] ], vx[ hx[i].x[1] ], vx[ hx[i].x[2] ], vx[ hx[i].x[3] ],
                 vx[ hx[i].x[4] ], vx[ hx[i].x[5] ], vx[ hx[i].x[6] ], vx[ hx[i].x[7] ] );

         if( f <= 0 ){ n++; }
     }
      if( n > 0 )
     {
         printf( "warning: %4d negative volumes \n", n );
     }

// check vertex attribution

      for( i=0;i<vt.n;i++ )
     {
         l= vt[i].h;
         j= vt[i].k;
         if( l > -1 )
        {
            assert( hx[l].v[j] == i );
        }
     }

// check that vertex quad and hex traverse is correct

      n= 0;
      VINT_2 l1,l2;
      for( i=0;i<vt.n;i++ )
     {
         if( vt[i].k > -1 )
        {
            point( i, l1,l2 );
            for( j=0;j<l1.n;j++ )
           {
               h= l1[j][0];
               k= l1[j][1];
               assert( hx[h].v[k] == i );
           }
            for( j=0;j<l2.n;j++ )
           {
               h= l2[j][0];
               k= l2[j][1];
               pts( hx,qv, h, buf[0] );
               assert (buf[0][k] == i );
           }
        }
         else
        {
            n++;
        }
     }
      if( n > 0 )
     {
         printf( "warning: %4d unreachable vertices\n", n );
     }
  }
