# include <hmesh/hmesh.h>
# include <misc.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Joshua Hope-Collins <joshua.hope-collins@eng.ox.ac.uk>
// Created         23 / 10 / 2018


/*
Starting from frame f0 in hex id0, step along direction sign(d)*f0[abs(d)], and return id of this hex (id1), and a frame in id1 aligned with f0 (f1)
*/
   void hmesh_t::frme_walk( INT_ id0, frme_t f0, INT_ d, INT_ &id1, frme_t &f1 )
  {
      const INT_ cycle31[3]={1,2,0};
      const INT_ cycle32[3]={2,0,1};

      INT_    i,j,k;
      VINT_   mk( hx.n, -1 );

      k = abs(d) - 1;
      i = cycle31[k];
      j = cycle32[k];

      // construct f1 so f1[2] points along d
      if( d>0 ){ f1[2] =  f0[ k ]; }
      else     { f1[2] = !f0[ k ]; }

      f1[0] = f0[i];
      f1[1] = f0[j];

      // walk along f1 from id0
      id1 = id0;
      assert( walk( id1, f1, mk ) );

      if( d<0 ){ f1[2] = !f1[2]; }

      // rearrange f1 so it aligns with f0
      frme_t   temp;
      temp[0] = f1[0]; temp[1] = f1[1]; temp[2] = f1[2];

      f1[i] = temp[0];
      f1[j] = temp[1];
      f1[k] = temp[2];

      return;
  }


/*
Returns two 2x2x2 arrays of the (global) vertex (v) and coordinate (x) ids of the vertices on hex id according to frame f.
i.e. v[1][1][0] is the vertex pointed to by ( f[0], f[1], !f[2] )
*/
   void hmesh_t::frame2vertices( INT_ id, frme_t f, INT_ v[2][2][2], INT_ x[2][2][2] )
  {

      v[0][0][0] = coor( !f[0], !f[1], !f[2] );
      v[1][0][0] = coor(  f[0], !f[1], !f[2] );
      v[0][1][0] = coor( !f[0],  f[1], !f[2] );
      v[0][0][1] = coor( !f[0], !f[1],  f[2] );
      v[1][1][0] = coor(  f[0],  f[1], !f[2] );
      v[1][0][1] = coor(  f[0], !f[1],  f[2] );
      v[0][1][1] = coor( !f[0],  f[1],  f[2] );
      v[1][1][1] = coor(  f[0],  f[1],  f[2] );

      x[0][0][0] = hx[id].x[ v[0][0][0] ];
      x[1][0][0] = hx[id].x[ v[1][0][0] ];
      x[0][1][0] = hx[id].x[ v[0][1][0] ];
      x[0][0][1] = hx[id].x[ v[0][0][1] ];
      x[1][1][0] = hx[id].x[ v[1][1][0] ];
      x[1][0][1] = hx[id].x[ v[1][0][1] ];
      x[0][1][1] = hx[id].x[ v[0][1][1] ];
      x[1][1][1] = hx[id].x[ v[1][1][1] ];

      v[0][0][0] = hx[id].v[ v[0][0][0] ];
      v[1][0][0] = hx[id].v[ v[1][0][0] ];
      v[0][1][0] = hx[id].v[ v[0][1][0] ];
      v[0][0][1] = hx[id].v[ v[0][0][1] ];
      v[1][1][0] = hx[id].v[ v[1][1][0] ];
      v[1][0][1] = hx[id].v[ v[1][0][1] ];
      v[0][1][1] = hx[id].v[ v[0][1][1] ];
      v[1][1][1] = hx[id].v[ v[1][1][1] ];

      return;
  }


/*
Creates three new hexahedrals in the slot layout and returns the ids of each (h2-4).
The three new hexahedrals are created from hex hr0 according to frame f0, defined in slot
If hl2-3 are -1, entirely new hexes are created
If hl2-3 are ids of previous row of slot hexes, then new hexes hr2-4 are created using the vertices from face 1 on each of hl2-4
   f0[0] is the direction out of the current mesh
   f0[1] is the direction the slot hole runs in
   f0[2] is the direction across the width of the slot pointing from face 0 to face 1 on h2-4
*/
   void hmesh_t::slot_kernel( INT_  hr0, frme_t f0, INT_ b2, INT_ b3,
                                 INT_  hl2, INT_  hl3, INT_  hl4,
                                 INT_ &hr2, INT_ &hr3, INT_ &hr4 )
  {
      INT_     hr1;
      frme_t   f1;

      INT_     t0,  t1,  t2,  t3,  t4,  t5;
      INT_     t0x, t1x, t2x, t3x, t4x, t5x;
      INT_     c0,  c1;
      INT_     c0x, c1x;
      INT_     m0,  m1,  m2,  m3,  m4,  m5;
      INT_     m0x, m1x, m2x, m3x, m4x, m5x;
      INT_     u0x, u1x;

      INT_     vxn, vtn, hxn;          // total number of (assigned) vertices, vertex coordinates and hexes

      INT_     v0[2][2][2], x0[2][2][2];
      INT_     v1[2][2][2], x1[2][2][2];

      INT_     i, j;
      INT_     a, b, c, d;

      // frme_walk backwards along f[1] to hr1 and get frame1

      // this would be if i were to local direction for hr0. frme_walk walks in f0[i]
      // i = f0[1].dir() +1;
      // if( f0[1].sign() ){ i=-i; }

      i = -2;
      frme_walk( hr0, f0, i, hr1, f1 );

      // find required vertex ids in hexes hr0 and hr1
      frame2vertices( hr0, f0, v0, x0 );
      frame2vertices( hr1, f1, v1, x1 );

      t0 = v0[1][1][0]; t0x = x0[1][1][0];
      t1 = v0[1][1][1]; t1x = x0[1][1][1];

      t2 = v1[1][0][0]; t2x = x1[1][0][0];
      t3 = v1[1][0][1]; t3x = x1[1][0][1];

      m0 = v0[1][0][0]; m0x = x0[1][0][0];
      m1 = v0[1][0][1]; m1x = x0[1][0][1];

/*    u0 = v0[0][1][0]; */ u0x = x0[0][1][0];
/*    u1 = v0[0][1][1]; */ u1x = x0[0][1][1];


      // create new vertices 
      if( hl2 < 0 )
     {
         vxn= vx.n;  vx.n+= 8; vx.resize( xdum );
         vtn= vt.n;  vt.n+= 8; vt.resize( pdum );

         // t4,5 from extrapolating from u0,1 through t0,1
         vx[vxn] = vx[t0x] + ( vx[t0x] - vx[u0x] ); t4x = vxn++;
         vx[vxn] = vx[t1x] + ( vx[t1x] - vx[u1x] ); t5x = vxn++;

         // m2,3,4,5 from halving sides
         vx[vxn] = 0.5*( vx[t2x] + vx[t4x] ); m2x = vxn++;
         vx[vxn] = 0.5*( vx[t3x] + vx[t5x] ); m3x = vxn++;
         vx[vxn] = 0.5*( vx[t0x] + vx[t4x] ); m4x = vxn++;
         vx[vxn] = 0.5*( vx[t1x] + vx[t5x] ); m5x = vxn++;

         // mean centroids of the triangles
         REAL_ t;
         t=1.0/9.0;

         vx[vxn] = 2.0*vx[m0x] + vx[t4x] +
                   2.0*vx[m2x] + vx[t0x] +
                   2.0*vx[m4x] + vx[t2x];
         c0x = vxn++;

         vx[vxn] = 2.0*vx[m1x] + vx[t5x] +
                   2.0*vx[m3x] + vx[t1x] +
                   2.0*vx[m5x] + vx[t3x];
         c1x = vxn++;

         vx[c0x] *= t;
         vx[c1x] *= t;

         vt[vtn].id = vtn; t4 = vtn++;
         vt[vtn].id = vtn; t5 = vtn++;
         vt[vtn].id = vtn; m2 = vtn++;
         vt[vtn].id = vtn; m3 = vtn++;
         vt[vtn].id = vtn; m4 = vtn++;
         vt[vtn].id = vtn; m5 = vtn++;
         vt[vtn].id = vtn; c0 = vtn++;
         vt[vtn].id = vtn; c1 = vtn++;
     }
      else
     {
         // use vertices from last row
         assert( hl3 > -1 );
         assert( hl4 > -1 );

         vxn= vx.n;  vx.n+= 4; vx.resize( xdum );
         vtn= vt.n;  vt.n+= 4; vt.resize( pdum );

         // t4,5 from extrapolating from u0,1 through t0,1
         vx[vxn] = vx[t1x] + ( vx[t1x] - vx[u1x] ); t5x = vxn++;

         // m2,3,4,5 from halving sides
         vx[vxn] = 0.5*( vx[t3x] + vx[t5x] ); m3x = vxn++;
         vx[vxn] = 0.5*( vx[t1x] + vx[t5x] ); m5x = vxn++;

         // mean centroids of the triangles
         REAL_ t;
         t=1.0/9.0;

         vx[vxn] = 2.0*vx[m1x] + vx[t5x] +
                   2.0*vx[m3x] + vx[t1x] +
                   2.0*vx[m5x] + vx[t3x];

         c1x = vxn++;
         vx[c1x] *= t;

         vt[vtn].id = vtn; t5 = vtn++;
         vt[vtn].id = vtn; m3 = vtn++;
         vt[vtn].id = vtn; m5 = vtn++;
         vt[vtn].id = vtn; c1 = vtn++;

         if( f0[2] == vec( f0[0], f0[1] ) )
        {
            t4  = hx[hl4].v[3];
            t4x = hx[hl4].x[3];

            m2  = hx[hl4].v[1];
            m2x = hx[hl4].x[1];

            m4  = hx[hl4].v[7];
            m4x = hx[hl4].x[7];

            c0  = hx[hl4].v[5];
            c0x = hx[hl4].x[5];
        }
         else
        {
            t4  = hx[hl4].v[1];
            t4x = hx[hl4].x[1];

            m2  = hx[hl4].v[3];
            m2x = hx[hl4].x[3];

            m4  = hx[hl4].v[5];
            m4x = hx[hl4].x[5];

            c0  = hx[hl4].v[7];
            c0x = hx[hl4].x[7];
        }
     }


      // create new hexes
      // h2b2 is face on h2 connecting to boundary group b2, h2h3 is face on h2 connecting to h3 etc
      INT_ h2b2, h2h3;
      INT_ h3h2, h3b3;
      INT_ h4b2, h4h3;

      hxn= hx.n;  hx.n+= 3; hx.resize( hdum );
      if( f0[2] == vec( f0[0], f0[1] ) )
     {
         assert( ::vol( vx[c0x], vx[c1x], vx[m4x], vx[m5x], vx[m0x], vx[m1x], vx[t0x], vx[t1x] ) > 0 );
         assert( ::vol( vx[m2x], vx[m3x], vx[c0x], vx[c1x], vx[t2x], vx[t3x], vx[m0x], vx[m1x] ) > 0 );
         assert( ::vol( vx[m2x], vx[m3x], vx[t4x], vx[t5x], vx[c0x], vx[c1x], vx[m4x], vx[m5x] ) > 0 );

         hex( hxn, c0,c1,m4,m5, m0,m1,t0,t1,  c0x,c1x,m4x,m5x, m0x,m1x,t0x,t1x ); hr2=hxn++;
         hex( hxn, m2,m3,c0,c1, t2,t3,m0,m1,  m2x,m3x,c0x,c1x, t2x,t3x,m0x,m1x ); hr3=hxn++;
         hex( hxn, m2,m3,t4,t5, c0,c1,m4,m5,  m2x,m3x,t4x,t5x, c0x,c1x,m4x,m5x ); hr4=hxn++;

         h2b2 = 3;
         h2h3 = 2;
             
         h3h2 = 3;
         h3b3 = 2;
             
         h4b2 = 3;
         h4h3 = 2;
     }
      else
     {
         assert( ::vol( vx[m4x], vx[m5x], vx[c0x], vx[c1x], vx[t0x], vx[t1x], vx[m0x], vx[m1x] ) > 0 );
         assert( ::vol( vx[c0x], vx[c1x], vx[m2x], vx[m3x], vx[m0x], vx[m1x], vx[t2x], vx[t3x] ) > 0 );
         assert( ::vol( vx[t4x], vx[t5x], vx[m2x], vx[m3x], vx[m4x], vx[m5x], vx[c0x], vx[c1x] ) > 0 );

         hex( hxn, m4,m5,c0,c1, t0,t1,m0,m1,  m4x,m5x,c0x,c1x, t0x,t1x,m0x,m1x ); hr2=hxn++;
         hex( hxn, c0,c1,m2,m3, m0,m1,t2,t3,  c0x,c1x,m2x,m3x, m0x,m1x,t2x,t3x ); hr3=hxn++;
         hex( hxn, t4,t5,m2,m3, m4,m5,c0,c1,  t4x,t5x,m2x,m3x, m4x,m5x,c0x,c1x ); hr4=hxn++;

         h2b2 = 2;
         h2h3 = 3;

         h3h2 = 2;
         h3b3 = 3;

         h4b2 = 2;
         h4h3 = 3;
     }

      // remove mouth faces from boundary group
      a = f0[0].dir( );
      b = f0[0].sign();
      c = f1[0].dir( );
      d = f1[0].sign();

      i = oqq[b][a][0];
      j = oqq[d][c][0];

      b = hx[hr0].q[i].g;
      a = hx[hr0].q[i].b;
      detach( b, a );

      d = hx[hr1].q[j].g;
      c = hx[hr1].q[j].b;
      detach( d, c );

      // join mouth faces onto existing hex faces
      force( hr2, 5, -1, -1 );
      force( hr3, 5, -1, -1 );

      attach( (MXGRP-1), hr0, i, -1, -1, -1 );
      attach( (MXGRP-1), hr1, j, -1, -1, -1 );
      attach( (MXGRP-1), hr2, 5, -1, -1, -1 );
      attach( (MXGRP-1), hr3, 5, -1, -1, -1 );

      join( hr0, i, hr2, 5 );
      join( hr1, j, hr3, 5 );

      // side faces
      force( hr2, 0, -1, -1 );
      force( hr3, 0, -1, -1 );
      force( hr4, 0, -1, -1 );

      force( hr2, 1, -1, -1 );
      force( hr3, 1, -1, -1 );
      force( hr4, 1, -1, -1 );

      // force faces between hexes
      force( hr2, h2h3, hr3, h3h2 );
      force( hr2,    4, hr4,    5 );
      force( hr3,    4, hr4, h4h3 );

      // boundary faces
      force( hr2, h2b2, -1, -1 );
      force( hr4, h4b2, -1, -1 );

      force( hr3, h3b3, -1, -1 );
      force( hr4,    4, -1, -1 );

      // attach outside faces to b2, b3
      assert( b2 > -1 );
      assert( b3 > -1 );

      attach( b2, hr2, h2b2, -1, -1, -1 );
      attach( b2, hr4, h4b2, -1, -1, -1 );

      attach( b3, hr3, h3b3, -1, -1, -1 );
      attach( b3, hr4,    4, -1, -1, -1 );

      // join onto last row
      if( hl2 > -1 )
     {
         attach( (MXGRP-1), hl2, 1, -1, -1, -1 );
         attach( (MXGRP-1), hl3, 1, -1, -1, -1 );
         attach( (MXGRP-1), hl4, 1, -1, -1, -1 );

         attach( (MXGRP-1), hr2, 0, -1, -1, -1 );
         attach( (MXGRP-1), hr3, 0, -1, -1, -1 );
         attach( (MXGRP-1), hr4, 0, -1, -1, -1 );

         join( hl2, 1, hr2, 0 );
         join( hl3, 1, hr3, 0 );
         join( hl4, 1, hr4, 0 );
     }

      return;
  }


/* create block layout for slot
   slot extends across current boundary in direction f[0], and points in direction f[1], with width n along f[2]
   b0-3, are boundary surfaces to attach new boundary faces
*/
   void hmesh_t::slot( INT_ id, frme_t f0, INT_ n, INT_ b0, INT_ b1, INT_ b2, INT_ b3 )
  {

      INT_      i,  d;                   // counters
      INT_     hl0, hr0;                  // preexisting hex ids
      INT_     hl2, hl3, hl4;             // 3 new hexes
      INT_     hr2, hr3, hr4;             // 3 new hexes

      frme_t   fl0, fr0;               // frame for d0,d1,d2 in h0

      hl0 = indx[id];
      assert( hl0 > -1);

      assert( b0 > -1);
      assert( b1 > -1);
      assert( b2 > -1);
      assert( b3 > -1);

      fl0[0] = f0[0];
      fl0[1] = f0[1];
      fl0[2] = f0[2];

      // create the first row of slot hexes
      hl2=-1; hl3=-1; hl4=-1;
      slot_kernel( hl0, fl0, b2, b3, -1,  -1,  -1,
                                       hl2, hl3, hl4 );
      hr2=hl2; hr3=hl3; hr4=hl4;

      // attach side faces of new hexes to boundary group
      attach( b0, hl2, 0, -1, -1, -1 );
      attach( b0, hl3, 0, -1, -1, -1 );
      attach( b0, hl4, 0, -1, -1, -1 );


      // create n new rows of slot hexes
      for( i=1; i<n; i++ )
     {
         // find next h0 hex (hr0) and frame in that hex (fr0)
         d = 3;
         frme_walk( hl0, fl0, d, hr0, fr0 );

         // create new row of slot hexes
         slot_kernel( hr0, fr0, b2, b3, hl2, hl3, hl4,
                                           hr2, hr3, hr4 );

         hl0=hr0;
         hl2=hr2;
         hl3=hr3;
         hl4=hr4;

         fl0[0]=fr0[0];
         fl0[1]=fr0[1];
         fl0[2]=fr0[2];
     }

      // attach side faces of final row to boundary group
      attach( b1, hr2, 1, -1, -1, -1 );
      attach( b1, hr3, 1, -1, -1, -1 );
      attach( b1, hr4, 1, -1, -1, -1 );

      return;
  }
