
#  include <hmesh/hmesh.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void hmesh_t::quads()
  {

      INT_         i,j,f,g;

      VINT_        key(vt.n,-1);
      VINT_        link;
      INT_         k,l;
      INT_4        wrk;
      INT_4        buf; 

      
      link.n= qv.n; link.resize(-1);

// place existing faces in the link data structure

      for( i=0;i<qv.n;i++ )
     {
         pts( hx,qv, i, buf );

         k= buf[0];

         l= key[k];
         if( l == -1 )
        {
            link.n++; link.resize(-1);
            key[k]= i;
        }
         else
        {
            while( l > -1 )
           {
               pts( hx,qv, l,wrk );
               assert( !same( wrk,buf ) );
               k= l;
               l= link[l];
           }
            link[k]=i;
        }
     }

// process boundary faces

      ng= 0;
      for( g=0;g<nb;g++ )
     {
         for( f=0;f<bz[g].bo.n;f++ )
        {

            i= bz[g].bo[f].h;
            j= bz[g].bo[f].q;

            hx[i].q[j].g= g;
            hx[i].q[j].b= f;

            quad0( hx,qv, key,link, i,j ); 

            ng++;
        }
     }

// process internal faces

      for( i=0;i<hx.n;i++ )
     {
         for( j=0;j<6;j++ )
        {

            quad0( hx,qv, key,link, i,j );

        }
     }

      return;
  };


   void hmesh_t::quads( VINT_ &p, INT_ ist,INT_ ien )
  {

      INT_         i,j;

      VINT_        key(vt.n,-1);
      VINT_        link;
      INT_         h,k,l,m,n;
      bool         flag;
      INT_4        wrk;
      INT_4        buf; 

      VINT_2       l1,l2;

      
      link.n= qv.n; link.resize(-1);

// place existing faces in the link data structure

      for( h=0;h<p.n;h++ )
     {
         m= p[h];
         if( vt[m].h > -1 && vt[m].h < ist )
        {
            point( m, l1,l2 );

            for( i=0;i<l2.n;i++ )
           {   
               n= l2[i][0];
               pts( hx,qv, n, buf );
       
               k= buf[0];
       
               l= key[k];
               if( l == -1 )
              {
                  link.n++; link.resize(-1);
                  key[k]= n;
              }
               else
              {
                  flag= true;
                  while( l > -1 )
                 {
                     pts( hx,qv, l,wrk );
//                   assert( !same( wrk,buf ) );
                     if( same( wrk,buf ) ){ flag= false;break; };
                     k= l;
                     l= link[l];
                 }
                  if( flag ){ link[k]= n; };
              }
           }
        }
     }

// process internal faces

      for( i=ist;i<ien;i++ )
     {
         for( j=0;j<6;j++ )
        {

            quad0( hx,qv, key,link, i,j );

        }
     }

      return;
  };


   void hmesh_t::quadz( VINT_ &p, INT_ ist,INT_ ien )
  {

      INT_         i,j;

      VINT_        key(vt.n,-1);
      VINT_        link;
      INT_         h,k,l,m,n;
      bool         flag;
      INT_4        wrk;
      INT_4        buf; 

      VINT_2       l1,l2;

      
      link.n= qv.n; link.resize(-1);

// place existing faces in the link data structure

      for( h=0;h<p.n;h++ )
     {
         m= p[h];
         if( vt[m].h > -1 && vt[m].h < ist )
        {
            point( m, l1,l2 );

            for( i=0;i<l2.n;i++ )
           {   
               n= l2[i][0];
               pts( hx,qv, n, buf );
       
               k= buf[0];
       
               l= key[k];
               if( l == -1 )
              {
                  link.n++; link.resize(-1);
                  key[k]= n;
              }
               else
              {
                  flag= true;
                  while( l > -1 )
                 {
                     pts( hx,qv, l,wrk );
//                   assert( !same( wrk,buf ) );
                     if( same( wrk,buf ) ){ flag= false;break; };
                     k= l;
                     l= link[l];
                 }
                  if( flag ){ link[k]= n; };
              }
           }
        }
     }

// process internal faces

      for( i=ist;i<ien;i++ )
     {
         for( j=0;j<6;j++ )
        {

            quad0( hx,qv, key,link, indx[i],j );

        }
     }

      return;
  };

