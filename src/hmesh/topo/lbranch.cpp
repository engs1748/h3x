#include <hmesh/hmesh.h>

/*
create block layout for a laidback cooling hole starting from hex k. branch extends out of the mesh in direction f0[0], and points in direction f0[1].
cross section of the branch is connected to bg[4].
looking up the branch towards the preexisting mesh, the 4 sides of the branch are connected to bg[...
 ---0---
|       |
3       1
|       |
 ---2---
Where surface 0 is the one closest to the preexisting mesh
*/

   void hmesh_t::lbranch( INT_ k, frme_t f0, INT_ *bg )
  {
      INT_     a, b, c, d;
      REAL_    y;

      INT_     x,  v,  n0, n1, n2, n3, n4;
      INT_     h0, h1, hs;
      INT_     k0, k1;
      INT_     i0[8], v0[8], x0[8], w0[8], y0[8];
      INT_     i1[8], v1[8], x1[8], w1[8], y1[8];

      INT_     q0[6], g0[6], b0[6], s0[6], p0[6];
      INT_     q1[6], g1[6], b1[6], s1[6], p1[6];

      frme_t          f1, fs, ft;
      vtx_t         z[4];

      const REAL_  c0=4./3.;
      const REAL_  c1=2./3.;
      const REAL_  c2=1./3.;

      // tunnel-esque section
      memset( q1,-1,sizeof(q1));
      memset( g1,-1,sizeof(q1));
      memset( b1,-1,sizeof(q1));
      memset( s1,-1,sizeof(q1));
      memset( p1,-1,sizeof(q1));
      memset( q0,-1,sizeof(q1));
      memset( g0,-1,sizeof(q1));
      memset( b0,-1,sizeof(q1));
      memset( s0,-1,sizeof(q1));
      memset( p0,-1,sizeof(q1));


      h0 = indx[k];
      assert( h0 > -1 );

      for( a=0; a<5; a++ ){ assert( bg[a]>-1  ); }
      for( a=0; a<5; a++ ){ assert( bg[a]<(MXGRP-1) ); }

      frme_walk( h0, f0, -2, h1, f1 );
      frme_walk( h0, f0, -3, hs, fs );

      k0= hx[h0].id;
      k1= hx[h1].id;

      // ids and coordinates of vertices in h0
      i0[0] = coor(  (f0[0]), (f0[1]),!(f0[2]) );
      i0[1] = coor(  (f0[0]),!(f0[1]),!(f0[2]) );
      i0[2] = coor(  (f0[0]), (f0[1]), (f0[2]) );
      i0[3] = coor(  (f0[0]),!(f0[1]), (f0[2]) );
      i0[4] = coor( !(f0[0]), (f0[1]),!(f0[2]) );
      i0[5] = coor( !(f0[0]),!(f0[1]),!(f0[2]) );
      i0[6] = coor( !(f0[0]), (f0[1]), (f0[2]) );
      i0[7] = coor( !(f0[0]),!(f0[1]), (f0[2]) );

      i1[0] = coor(  (f1[0]), (f1[1]),!(f1[2]) );
      i1[1] = coor(  (f1[0]),!(f1[1]),!(f1[2]) );
      i1[2] = coor(  (f1[0]), (f1[1]), (f1[2]) );
      i1[3] = coor(  (f1[0]),!(f1[1]), (f1[2]) );
      i1[4] = coor( !(f1[0]), (f1[1]),!(f1[2]) );
      i1[5] = coor( !(f1[0]),!(f1[1]),!(f1[2]) );
      i1[6] = coor( !(f1[0]), (f1[1]), (f1[2]) );
      i1[7] = coor( !(f1[0]),!(f1[1]), (f1[2]) );

      // ids and coordinates of vertices in h1
      v0[0] = hx[h0].v[ i0[0] ]; v0[1] = hx[h0].v[ i0[1] ];
      v0[2] = hx[h0].v[ i0[2] ]; v0[3] = hx[h0].v[ i0[3] ]; 
      v0[4] = hx[h0].v[ i0[4] ]; v0[5] = hx[h0].v[ i0[5] ];
      v0[6] = hx[h0].v[ i0[6] ]; v0[7] = hx[h0].v[ i0[7] ]; 

      x0[0] = hx[h0].x[ i0[0] ]; x0[1] = hx[h0].x[ i0[1] ];
      x0[2] = hx[h0].x[ i0[2] ]; x0[3] = hx[h0].x[ i0[3] ]; 
      x0[4] = hx[h0].x[ i0[4] ]; x0[5] = hx[h0].x[ i0[5] ];
      x0[6] = hx[h0].x[ i0[6] ]; x0[7] = hx[h0].x[ i0[7] ]; 

      v1[0] = hx[h1].v[ i1[0] ]; v1[1] = hx[h1].v[ i1[1] ];
      v1[2] = hx[h1].v[ i1[2] ]; v1[3] = hx[h1].v[ i1[3] ]; 
      v1[4] = hx[h1].v[ i1[4] ]; v1[5] = hx[h1].v[ i1[5] ];
      v1[6] = hx[h1].v[ i1[6] ]; v1[7] = hx[h1].v[ i1[7] ]; 

      x1[0] = hx[h1].x[ i1[0] ]; x1[1] = hx[h1].x[ i1[1] ];
      x1[2] = hx[h1].x[ i1[2] ]; x1[3] = hx[h1].x[ i1[3] ]; 
      x1[4] = hx[h1].x[ i1[4] ]; x1[5] = hx[h1].x[ i1[5] ];
      x1[6] = hx[h1].x[ i1[6] ]; x1[7] = hx[h1].x[ i1[7] ]; 


      // faces and boundary information in h0
/*    q0[0] = hx[h0].face(   f0[1]  );
      q0[1] = hx[h0].face( !(f0[1]) );
      q0[2] = hx[h0].face( !(f0[2]) );
      q0[3] = hx[h0].face(   f0[2]  );
      q0[4] = hx[h0].face(   f0[0]  ); 
      q0[5] = hx[h0].face( !(f0[0]) );*/

      q0[0] = face(   f0[1]  );
      q0[1] = face( !(f0[1]) );
      q0[2] = face( !(f0[2]) );
      q0[3] = face(   f0[2]  );
      q0[4] = face(   f0[0]  ); 
      q0[5] = face( !(f0[0]) );

      g0[0] = hx[h0].q[ q0[0] ].g; 
      g0[1] = hx[h0].q[ q0[1] ].g; 
      g0[2] = hx[h0].q[ q0[2] ].g; 
      g0[3] = hx[h0].q[ q0[3] ].g; 
      g0[4] = hx[h0].q[ q0[4] ].g; 
      g0[5] = hx[h0].q[ q0[5] ].g; 

      b0[0] = hx[h0].q[ q0[0] ].b; 
      b0[1] = hx[h0].q[ q0[1] ].b; 
      b0[2] = hx[h0].q[ q0[2] ].b; 
      b0[3] = hx[h0].q[ q0[3] ].b; 
      b0[4] = hx[h0].q[ q0[4] ].b; 
      b0[5] = hx[h0].q[ q0[5] ].b; 

      if( g0[0] > -1 ){ s0[0]= bz[ g0[0] ].bo[ b0[0] ].s; p0[0]= bz[ g0[0] ].bo[ b0[0] ].p; }
      if( g0[1] > -1 ){ s0[1]= bz[ g0[1] ].bo[ b0[1] ].s; p0[1]= bz[ g0[1] ].bo[ b0[1] ].p; }
      if( g0[2] > -1 ){ s0[2]= bz[ g0[2] ].bo[ b0[2] ].s; p0[2]= bz[ g0[2] ].bo[ b0[2] ].p; }
      if( g0[3] > -1 ){ s0[3]= bz[ g0[3] ].bo[ b0[3] ].s; p0[3]= bz[ g0[3] ].bo[ b0[3] ].p; }
      if( g0[4] > -1 ){ s0[4]= bz[ g0[4] ].bo[ b0[4] ].s; p0[4]= bz[ g0[4] ].bo[ b0[4] ].p; }
      if( g0[5] > -1 ){ s0[5]= bz[ g0[5] ].bo[ b0[5] ].s; p0[5]= bz[ g0[5] ].bo[ b0[5] ].p; }

      // faces and boundary information in h1
/*    q1[0] = hx[h1].face(   f1[1]  );
      q1[1] = hx[h1].face( !(f1[1]) );
      q1[2] = hx[h1].face( !(f1[2]) );
      q1[3] = hx[h1].face(   f1[2]  );
      q1[4] = hx[h1].face(   f1[0]  ); 
      q1[5] = hx[h1].face( !(f1[0]) );*/

      q1[0] = face(   f1[1]  );
      q1[1] = face( !(f1[1]) );
      q1[2] = face( !(f1[2]) );
      q1[3] = face(   f1[2]  );
      q1[4] = face(   f1[0]  ); 
      q1[5] = face( !(f1[0]) );

      g1[0] = hx[h1].q[ q1[0] ].g; 
      g1[1] = hx[h1].q[ q1[1] ].g; 
      g1[2] = hx[h1].q[ q1[2] ].g; 
      g1[3] = hx[h1].q[ q1[3] ].g; 
      g1[4] = hx[h1].q[ q1[4] ].g; 
      g1[5] = hx[h1].q[ q1[5] ].g; 

      b1[0] = hx[h1].q[ q1[0] ].b; 
      b1[1] = hx[h1].q[ q1[1] ].b; 
      b1[2] = hx[h1].q[ q1[2] ].b; 
      b1[3] = hx[h1].q[ q1[3] ].b; 
      b1[4] = hx[h1].q[ q1[4] ].b; 

      if( g1[0] > -1 ){ s1[0]= bz[ g1[0] ].bo[ b1[0] ].s; p1[0]= bz[ g1[0] ].bo[ b1[0] ].p; }
      if( g1[1] > -1 ){ s1[1]= bz[ g1[1] ].bo[ b1[1] ].s; p1[1]= bz[ g1[1] ].bo[ b1[1] ].p; }
      if( g1[2] > -1 ){ s1[2]= bz[ g1[2] ].bo[ b1[2] ].s; p1[2]= bz[ g1[2] ].bo[ b1[2] ].p; }
      if( g1[3] > -1 ){ s1[3]= bz[ g1[3] ].bo[ b1[3] ].s; p1[3]= bz[ g1[3] ].bo[ b1[3] ].p; }
      if( g1[4] > -1 ){ s1[4]= bz[ g1[4] ].bo[ b1[4] ].s; p1[4]= bz[ g1[4] ].bo[ b1[4] ].p; }
      if( g1[5] > -1 ){ s1[5]= bz[ g1[5] ].bo[ b1[5] ].s; p1[5]= bz[ g1[5] ].bo[ b1[5] ].p; }

      // new vertices for mini hex in h0
      x= vx.n; vx.n+= 8; vx.resize(xdum );
      v= vt.n; vt.n+= 8; vt.resize(pdum );

      z[0]= 0.5*( vx[ x0[0] ] + vx[ x0[1] ] );
      z[1]= 0.5*( vx[ x0[2] ] + vx[ x0[3] ] );
      z[2]= 0.5*( vx[ x0[4] ] + vx[ x0[5] ] );
      z[3]= 0.5*( vx[ x0[6] ] + vx[ x0[7] ] );

      vx[x]= c1*z[0]+ c2*z[1];                           y0[0]=x++;
      vx[x]= c2*z[0]+ c1*z[1];                           y0[2]=x++;

      vx[x]= 0.5*( c1*z[0]+ c2*z[1]+ c1*z[2]+ c2*z[3] ); y0[4]=x++;
      vx[x]= 0.5*( c2*z[0]+ c1*z[1]+ c2*z[2]+ c1*z[3] ); y0[6]=x++;
  
      z[0]= vx[ x0[1] ];
      z[1]= vx[ x0[3] ];
      z[2]= vx[ x0[5] ];
      z[3]= vx[ x0[7] ];
  
      vx[x]=       c1*z[0]+ c2*z[1];                     y0[1]=x++;
      vx[x]=       c2*z[0]+ c1*z[1];                     y0[3]=x++;
      vx[x]= 0.5*( c1*z[0]+ c2*z[1]+ c1*z[2]+ c2*z[3] ); y0[5]=x++;
      vx[x]= 0.5*( c2*z[0]+ c1*z[1]+ c2*z[2]+ c1*z[3] ); y0[7]=x++;

      vt[v].id= v; w0[0] = v++;
      vt[v].id= v; w0[2] = v++;
      vt[v].id= v; w0[4] = v++;
      vt[v].id= v; w0[6] = v++;
      vt[v].id= v; w0[1] = v++;
      vt[v].id= v; w0[3] = v++;
      vt[v].id= v; w0[5] = v++;
      vt[v].id= v; w0[7] = v++;

      // new vertices for mini hex in h1
      x= vx.n; vx.n+= 8; vx.resize(xdum );
      v= vt.n; vt.n+= 8; vt.resize(pdum );

      z[0]= 0.5*( vx[ x1[0] ] + vx[ x1[1] ] );
      z[1]= 0.5*( vx[ x1[2] ] + vx[ x1[3] ] );
      z[2]= 0.5*( vx[ x1[4] ] + vx[ x1[5] ] );
      z[3]= 0.5*( vx[ x1[6] ] + vx[ x1[7] ] );

      vx[x]= c1*z[0]+ c2*z[1];                           y1[1]=x++;
      vx[x]= c2*z[0]+ c1*z[1];                           y1[3]=x++;

      vx[x]= 0.5*( c1*z[0]+ c2*z[1]+ c1*z[2]+ c2*z[3] ); y1[5]=x++;
      vx[x]= 0.5*( c2*z[0]+ c1*z[1]+ c2*z[2]+ c1*z[3] ); y1[7]=x++;

      w1[0] = w0[1];
      w1[2] = w0[3];
      w1[4] = w0[5];
      w1[6] = w0[7];
  
      y1[0] = y0[1];
      y1[2] = y0[3];
      y1[4] = y0[5];
      y1[6] = y0[7];
  
/*
      z[0]= vx[ x1[0] ];
      z[1]= vx[ x1[2] ];
      z[2]= vx[ x1[4] ];
      z[3]= vx[ x1[6] ];
  
      vx[x]=       c1*z[0]+ c2*z[1];                     y1[0]=x++;
      vx[x]=       c2*z[0]+ c1*z[1];                     y1[2]=x++;
      vx[x]= 0.5*( c1*z[0]+ c2*z[1]+ c1*z[2]+ c2*z[3] ); y1[4]=x++;
      vx[x]= 0.5*( c2*z[0]+ c1*z[1]+ c2*z[2]+ c1*z[3] ); y1[6]=x++;

      vt[v].id= v; w1[0] = v++;
      vt[v].id= v; w1[2] = v++;
      vt[v].id= v; w1[4] = v++;
      vt[v].id= v; w1[6] = v++;
*/

      vt[v].id= v; w1[1] = v++;
      vt[v].id= v; w1[3] = v++;
      vt[v].id= v; w1[5] = v++;
      vt[v].id= v; w1[7] = v++;

      // new hexes in h0
      n0=  hx.n; hx.n+=5; hx.resize( hdum );

      // mini, f0[2], !f0[0], !f0[2], f0[1]
      hex( n0+0, w0[0],w0[1],w0[2],w0[3], w0[4],w0[5],w0[6],w0[7],
                 y0[0],y0[1],y0[2],y0[3], y0[4],y0[5],y0[6],y0[7] );

      hex( n0+1, w0[2],w0[3],v0[2],v0[3], w0[6],w0[7],v0[6],v0[7],
                 y0[2],y0[3],x0[2],x0[3], y0[6],y0[7],x0[6],x0[7] );

      hex( n0+2, w0[4],w0[5],w0[6],w0[7], v0[4],v0[5],v0[6],v0[7],
                 y0[4],y0[5],y0[6],y0[7], x0[4],x0[5],x0[6],x0[7] );

      hex( n0+3, v0[0],v0[1],w0[0],w0[1], v0[4],v0[5],w0[4],w0[5],
                 x0[0],x0[1],y0[0],y0[1], x0[4],x0[5],y0[4],y0[5] );

      hex( n0+4, v0[0],w0[0],v0[2],w0[2], v0[4],w0[4],v0[6],w0[6],
                 x0[0],y0[0],x0[2],y0[2], x0[4],y0[4],x0[6],y0[6] );

      // bottom (external) faces
      force( n0+0,4,-1,-1 );
      force( n0+1,4,-1,-1 );
      force( n0+3,4,-1,-1 );   
      force( n0+4,4,-1,-1 );

      // internal faces
      force( n0+0,5, n0+2,4 );
      force( n0+0,3, n0+1,2 );
      force( n0+0,2, n0+3,3 );

      force( n0+2,3, n0+1,5 );
      force( n0+2,2, n0+3,5 );

      force( n0+0,0, n0+4,1 );
      force( n0+1,0, n0+4,3 );
      force( n0+2,0, n0+4,5 );
      force( n0+3,0, n0+4,2 );

      // faces which will connect to hexes in h1
      force( n0+0,1,-1,-1 );
      force( n0+1,1,-1,-1 );
      force( n0+2,1,-1,-1 );   
      force( n0+3,1,-1,-1 );

      // faces connecting to rest of mesh
      if( g0[0] > -1 ){ force( n0+4,0, -1,-1 ); }else{ replace( h0,q0[0], n0+4,0 ); }
      if( g0[2] > -1 ){ force( n0+3,2, -1,-1 ); }else{ replace( h0,q0[2], n0+3,2 ); }
      if( g0[3] > -1 ){ force( n0+1,3, -1,-1 ); }else{ replace( h0,q0[3], n0+1,3 ); }
      if( g0[5] > -1 ){ force( n0+2,5, -1,-1 ); }else{ replace( h0,q0[5], n0+2,5 ); }

      // sort out boundaries of h0
      detach( g0[4], b0[4] );
      attach( (MXGRP-1), h0,q0[4], -1,-1,-1 );

      // faces which will connect to hexes in h1
      attach( (MXGRP-1), n0+0,1, -1,-1,-1 );
      attach( (MXGRP-1), n0+1,1, -1,-1,-1 );
      attach( (MXGRP-1), n0+2,1, -1,-1,-1 );
      attach( (MXGRP-1), n0+3,1, -1,-1,-1 );

      // this is to show, n0+*,4 is external at this stage. needs to be removed once slot hexes added
      attach( g0[4], n0+0,4, s0[4],p0[4],-1 );
      attach( g0[4], n0+1,4, s0[4],p0[4],-1 );
      attach( g0[4], n0+3,4, s0[4],p0[4],-1 );
      attach( g0[4], n0+4,4, s0[4],p0[4],-1 );

      // external faces
      if( g0[0] > -1 ){ attach( g0[0], n0+4,0,    -1,   -1,-1 ); }
      if( g0[2] > -1 ){ attach( g0[2], n0+3,2, s0[2],p0[2],-1 ); }
      if( g0[3] > -1 ){ attach( g0[3], n0+1,3, s0[3],p0[3],-1 ); }
      if( g0[5] > -1 ){ attach( g0[5], n0+2,5, s0[5],p0[5],-1 ); }

      // new hexes in h1
      n1=  hx.n; hx.n+=5; hx.resize( hdum );

      // mini, f0[2], !f0[0], !f0[2], !f0[1]
      hex( n1+0, w1[0],w1[1],w1[2],w1[3], w1[4],w1[5],w1[6],w1[7],
                 y1[0],y1[1],y1[2],y1[3], y1[4],y1[5],y1[6],y1[7] );

      hex( n1+1, w1[2],w1[3],v1[2],v1[3], w1[6],w1[7],v1[6],v1[7],
                 y1[2],y1[3],x1[2],x1[3], y1[6],y1[7],x1[6],x1[7] );

      hex( n1+2, w1[4],w1[5],w1[6],w1[7], v1[4],v1[5],v1[6],v1[7],
                 y1[4],y1[5],y1[6],y1[7], x1[4],x1[5],x1[6],x1[7] );

      hex( n1+3, v1[0],v1[1],w1[0],w1[1], v1[4],v1[5],w1[4],w1[5],
                 x1[0],x1[1],y1[0],y1[1], x1[4],x1[5],y1[4],y1[5] );

      hex( n1+4, w1[1],v1[1],w1[3],v1[3], w1[5],v1[5],w1[7],v1[7],
                 y1[1],x1[1],y1[3],x1[3], y1[5],x1[5],y1[7],x1[7] );

      // bottom (external) faces
      force( n1+0,4,-1,-1 );
      force( n1+1,4,-1,-1 );
      force( n1+3,4,-1,-1 );   
      force( n1+4,4,-1,-1 );

      // internal faces
      force( n1+0,5, n1+2,4 );
      force( n1+0,3, n1+1,2 );
      force( n1+0,2, n1+3,3 );

      force( n1+2,3, n1+1,5 );
      force( n1+2,2, n1+3,5 );

      force( n1+0,1, n1+4,0 );
      force( n1+1,1, n1+4,3 );
      force( n1+2,1, n1+4,5 );
      force( n1+3,1, n1+4,2 );

      // faces which will connect to hexes in h0
      force( n1+0,0,-1,-1 );
      force( n1+1,0,-1,-1 );
      force( n1+2,0,-1,-1 );   
      force( n1+3,0,-1,-1 );

      // faces connecting to rest of mesh
      if( g1[1] > -1 ){ force( n1+4,1, -1,-1 ); }else{ replace( h1,q1[1], n1+4,1 ); }
      if( g1[2] > -1 ){ force( n1+3,2, -1,-1 ); }else{ replace( h1,q1[2], n1+3,2 ); }
      if( g1[3] > -1 ){ force( n1+1,3, -1,-1 ); }else{ replace( h1,q1[3], n1+1,3 ); }
      if( g1[5] > -1 ){ force( n1+2,5, -1,-1 ); }else{ replace( h1,q1[5], n1+2,5 ); }

      // sort out boundaries of h1
      detach( g1[4], b1[4] );
      attach( (MXGRP-1), h1,q1[4], -1,-1,-1 );

      // faces which will connect to hexes in h0
      attach( (MXGRP-1), n1+0,0, -1,-1,-1 );
      attach( (MXGRP-1), n1+1,0, -1,-1,-1 );
      attach( (MXGRP-1), n1+2,0, -1,-1,-1 );
      attach( (MXGRP-1), n1+3,0, -1,-1,-1 );

      // this is to show n1+*,4 is external. needs to be removed once slot hexes added
      attach( g1[4], n1+0,4, s1[4],p1[4],-1 );
      attach( g1[4], n1+1,4, s1[4],p1[4],-1 );
      attach( g1[4], n1+3,4, s1[4],p1[4],-1 );
      attach( g1[4], n1+4,4, s1[4],p1[4],-1 );

      // external faces
      if( g1[1] > -1 ){ attach( g1[1], n1+4,1,    -1,   -1,-1 ); }
      if( g1[2] > -1 ){ attach( g1[2], n1+3,2, s1[2],p1[2],-1 ); }
      if( g1[3] > -1 ){ attach( g1[3], n1+1,3, s1[3],p1[3],-1 ); }
      if( g1[5] > -1 ){ attach( g1[5], n1+2,5, s1[5],p1[5],-1 ); }
      
      // join faces between h0 and h1
      join( n0+0,1, n1+0,0 );
      join( n0+1,1, n1+1,0 );
      join( n0+2,1, n1+2,0 );
      join( n0+3,1, n1+3,0 );

      // create slot layout width 3 for beginning of laidback hole
      for( a=0; a<3; a++ ){ ft[a]=f0[a]; }
      frme_walk( hs, fs, 3, h0, ft );
      for( a=0; a<3; a++ ){ f0[a]=ft[a]; }

      assert( h0 == n0+3 );
      a=bg[1]; b=bg[3]; c=bg[4]; d=bg[2];
      slot( n0+3, f0, 3, a, b, c, d );

      // hex ids for slot hexes. f2-4 are h2-4 for first row created. similar for m2-4 and l2-4 for middle and last rows
      INT_  f2, f3, f4, m2, m3, m4, l2, l3, l4;
      INT_  h3f3;

      // if( f0[2] == vec( f0[0], f0[1] ) )
     {
//       h2f2 = 3;
         h3f3 = 2;
//       h4f2 = 3;
     }
/*
      else
     {
         h2f2 = 2;
         h3f3 = 3;
         h4f2 = 2;
     }
*/

      l4=hx.n-1; l3=l4-1; l2=l3-1;
      m4=l2  -1; m3=m4-1; m2=m3-1;
      f4=m2  -1; f3=f4-1; f2=f3-1;

      w0[0] = hx[f3].v[4]; y0[0] = hx[f3].x[4];
      w0[1] = hx[l3].v[5]; y0[1] = hx[l3].x[5];
      w0[2] = hx[f3].v[0]; y0[2] = hx[f3].x[0];
      w0[3] = hx[l3].v[1]; y0[3] = hx[l3].x[1];
      w0[4] = hx[f3].v[5]; y0[4] = hx[f3].x[5];
      w0[5] = hx[l3].v[4]; y0[5] = hx[l3].x[4];
      w0[6] = hx[f3].v[1]; y0[6] = hx[f3].x[1];
      w0[7] = hx[l3].v[0]; y0[7] = hx[l3].x[0];

      // create two hexes below slot layout in inlet of laidback hole

      // hex at end of hole
      assert( ::vol( vx[y0[0]],vx[y0[1]],vx[y0[2]],vx[y0[3]], vx[y0[4]],vx[y0[5]],vx[y0[6]],vx[y0[7]] ) > 0 );

      n2=hx.n; hx.n+=1; hx.resize( hdum );

      hex( n2, w0[0],w0[1],w0[2],w0[3], w0[4],w0[5],w0[6],w0[7],
               y0[0],y0[1],y0[2],y0[3], y0[4],y0[5],y0[6],y0[7] );

      // new quads
      force( n2,0, -1,-1 );
      force( n2,1, -1,-1 );
      force( n2,2, -1,-1 );
      force( n2,3, -1,-1 );
      force( n2,4, -1,-1 );
      force( n2,5, -1,-1 );

      // face 4 on surface of branch
      attach( (MXGRP-1),   n2,0, -1,-1,-1 );
      attach( (MXGRP-1),   n2,1, -1,-1,-1 );
      attach( (MXGRP-1),   n2,2, -1,-1,-1 );
      attach( (MXGRP-1),   n2,3, -1,-1,-1 );
      attach( bg[2], n2,4, -1,-1,-1 );
      attach( (MXGRP-1),   n2,5, -1,-1,-1 );

      // detach faces of surrounding hexes
      b = hx[f3].q[h3f3].g;
      a = hx[f3].q[h3f3].b;
      detach( b, a );

      b = hx[l3].q[h3f3].g;
      a = hx[l3].q[h3f3].b;
      detach( b, a );

      b = hx[m3].q[h3f3].g;
      a = hx[m3].q[h3f3].b;
      detach( b, a );

      b = hx[n1+4].q[4].g;
      a = hx[n1+4].q[4].b;
      detach( b, a );

      attach( (MXGRP-1), f3,h3f3, -1,-1,-1 );
      attach( (MXGRP-1), m3,h3f3, -1,-1,-1 );
      attach( (MXGRP-1), l3,h3f3, -1,-1,-1 );
      attach( (MXGRP-1), n1+4, 4, -1,-1,-1 );

      join( n2,0, f3,h3f3 );
      join( n2,1, l3,h3f3 );
      join( n2,5, m3,h3f3 );
      join( n2,2, n1+4, 4 );

      w1[0] = hx[f4].v[0]; y1[0] = hx[f4].x[0];
      w1[1] = hx[l4].v[1]; y1[1] = hx[l4].x[1];
      w1[2] = hx[f4].v[2]; y1[2] = hx[f4].x[2];
      w1[3] = hx[l4].v[3]; y1[3] = hx[l4].x[3];
      w1[4] = hx[f4].v[1]; y1[4] = hx[f4].x[1];
      w1[5] = hx[l4].v[0]; y1[5] = hx[l4].x[0];
      w1[6] = hx[f4].v[3]; y1[6] = hx[f4].x[3];
      w1[7] = hx[l4].v[2]; y1[7] = hx[l4].x[2];

      // hex completely inside hole
      assert( ::vol( vx[y1[0]],vx[y1[1]],vx[y1[2]],vx[y1[3]], vx[y1[4]],vx[y1[5]],vx[y1[6]],vx[y1[7]] ) > 0 );

      n3=hx.n; hx.n+=1; hx.resize( hdum );

      hex( n3, w1[0],w1[1],w1[2],w1[3], w1[4],w1[5],w1[6],w1[7],
               y1[0],y1[1],y1[2],y1[3], y1[4],y1[5],y1[6],y1[7] );

      // new quads
      force( n3,0, -1,-1 );
      force( n3,1, -1,-1 );
      force( n3,2, -1,-1 );
      force( n3,3, -1,-1 );
      force( n3,4, -1,-1 );
      force( n3,5, -1,-1 );

      // face 4 is on surface of branch
      attach( (MXGRP-1),   n3,0, -1,-1,-1 );
      attach( (MXGRP-1),   n3,1, -1,-1,-1 );
      attach( (MXGRP-1),   n3,2, -1,-1,-1 );
      attach( (MXGRP-1),   n3,3, -1,-1,-1 );
      attach( bg[2], n3,4, -1,-1,-1 );
      attach( (MXGRP-1),   n3,5, -1,-1,-1 );

      // detach faces of surrounding hexes
      b = hx[f4].q[4].g;
      a = hx[f4].q[4].b;
      detach( b, a );

      b = hx[m4].q[4].g;
      a = hx[m4].q[4].b;
      detach( b, a );

      b = hx[l4].q[4].g;
      a = hx[l4].q[4].b;
      detach( b, a );

      attach( (MXGRP-1), f4,4, -1,-1,-1 );
      attach( (MXGRP-1), m4,4, -1,-1,-1 );
      attach( (MXGRP-1), l4,4, -1,-1,-1 );

      join( n3,0, f4,4 );
      join( n3,1, l4,4 );
      join( n3,2, n2,3 );
      join( n3,5, m4,4 );

      // create 4 vertices at corners of butterfly layout pointing into the hole

      // x0[0-3] outer corners of existing butterfly
      // x0[4-7] inner corners of existing butterfly
      // y0[0-3] halfway split corners of existing butterfly
      vtx_t pt0, pt1, pt2, pt3;

      x0[0] = hx[f4].x[2]; v0[0] = hx[f4].v[2]; pt0 = vx[x0[0]];
      x0[1] = hx[f2].x[6]; v0[1] = hx[f2].v[6]; pt1 = vx[x0[1]];
      x0[2] = hx[l4].x[3]; v0[2] = hx[l4].v[3]; pt2 = vx[x0[2]];
      x0[3] = hx[l2].x[7]; v0[3] = hx[l2].v[7]; pt3 = vx[x0[3]];

      x0[4] = hx[m4].x[2]; v0[4] = hx[m4].v[2];
      x0[5] = hx[m2].x[6]; v0[5] = hx[m2].v[6];
      x0[6] = hx[m4].x[3]; v0[6] = hx[m4].v[3];
      x0[7] = hx[m2].x[7]; v0[7] = hx[m2].v[7];

      y0[0] = hx[f4].x[6]; w0[0] = hx[f4].v[6];
      y0[1] = hx[l4].x[7]; w0[1] = hx[l4].v[7];
      y0[2] = hx[f4].x[7]; w0[2] = hx[f4].v[7];
      y0[3] = hx[l4].x[6]; w0[3] = hx[l4].v[6];

      // normal vector pointing out from existing plane and downward displacement vector
      vtx_t dn, dd;

      dn = plane_normal( pt0, pt1, pt2, pt3 );

      dd  = vx[y0[2]] - vx[x0[5]];
      y   = l2norm( dd );
      dn  = y*dn;
      dd += dn;
      
      // new plane displaced out and down from existing butterfly

      x= vx.n; vx.n+= 12; vx.resize( xdum );
      v= vt.n; vt.n+= 12; vt.resize( pdum );

      vx[x]= vx[x0[0]]+dd; x1[0]=x++;
      vx[x]= vx[x0[1]]+dd; x1[1]=x++;
      vx[x]= vx[x0[2]]+dd; x1[2]=x++;
      vx[x]= vx[x0[3]]+dd; x1[3]=x++;

      // inner corners of new plane
      vx[x]= c0*vx[x1[0]] + c1*vx[x1[1]] + c1*vx[x1[2]] + c2*vx[x1[3]]; x1[4]=x++;
      vx[x]= c1*vx[x1[0]] + c0*vx[x1[1]] + c2*vx[x1[2]] + c1*vx[x1[3]]; x1[5]=x++;
      vx[x]= c1*vx[x1[0]] + c2*vx[x1[1]] + c0*vx[x1[2]] + c1*vx[x1[3]]; x1[6]=x++;
      vx[x]= c2*vx[x1[0]] + c1*vx[x1[1]] + c1*vx[x1[2]] + c0*vx[x1[3]]; x1[7]=x++;

      vx[x1[4]]*=c2; vx[x1[5]]*=c2; vx[x1[6]]*=c2; vx[x1[7]]*=c2;

      vt[v].id= v; v1[0]= v++;
      vt[v].id= v; v1[1]= v++;
      vt[v].id= v; v1[2]= v++;
      vt[v].id= v; v1[3]= v++;
      vt[v].id= v; v1[4]= v++;
      vt[v].id= v; v1[5]= v++;
      vt[v].id= v; v1[6]= v++;
      vt[v].id= v; v1[7]= v++;

      // halfway split corners of new plane
      vx[x]= 0.5*( vx[x1[0]] + vx[x1[1]] ); y1[0]=x++;
      vx[x]= 0.5*( vx[x1[2]] + vx[x1[3]] ); y1[1]=x++;
      vx[x]= 0.5*( vx[x1[4]] + vx[x1[5]] ); y1[2]=x++;
      vx[x]= 0.5*( vx[x1[6]] + vx[x1[7]] ); y1[3]=x++;

      vt[v].id= v; w1[0]= v++;
      vt[v].id= v; w1[1]= v++;
      vt[v].id= v; w1[2]= v++;
      vt[v].id= v; w1[3]= v++;

      // 8 new hexes of butterfly layout
      assert( ::vol( vx[x0[5]], vx[x0[7]], vx[x1[5]], vx[x1[7]],
                   vx[x0[1]], vx[x0[3]], vx[x1[1]], vx[x1[3]] ) > 0 );

      assert( ::vol( vx[y0[0]], vx[y0[2]], vx[y1[0]], vx[y1[2]],
                   vx[x0[1]], vx[x0[5]], vx[x1[1]], vx[x1[5]] ) > 0 );

      assert( ::vol( vx[x0[0]], vx[x0[4]], vx[x1[0]], vx[x1[4]],
                   vx[y0[0]], vx[y0[2]], vx[y1[0]], vx[y1[2]] ) > 0 );

      assert( ::vol( vx[x0[0]], vx[x0[2]], vx[x1[0]], vx[x1[2]],
                   vx[x0[4]], vx[x0[6]], vx[x1[4]], vx[x1[6]] ) > 0 );
      
      assert( ::vol( vx[x0[6]], vx[x0[2]], vx[x1[6]], vx[x1[2]],
                   vx[y0[3]], vx[y0[1]], vx[y1[3]], vx[y1[1]] ) > 0 );

      assert( ::vol( vx[y0[3]], vx[y0[1]], vx[y1[3]], vx[y1[1]],
                   vx[x0[7]], vx[x0[3]], vx[x1[7]], vx[x1[3]] ) > 0 );

      assert( ::vol( vx[y0[2]], vx[y0[3]], vx[y1[2]], vx[y1[3]],
                   vx[x0[5]], vx[x0[7]], vx[x1[5]], vx[x1[7]] ) > 0 );

      assert( ::vol( vx[x0[4]], vx[x0[6]], vx[x1[4]], vx[x1[6]],
                   vx[y0[2]], vx[y0[3]], vx[y1[2]], vx[y1[3]] ) > 0 );

      n4= hx.n; hx.n+= 8; hx.resize( hdum );

      hex( n4+0, v0[5], v0[7], v1[5], v1[7], v0[1], v0[3], v1[1], v1[3],
                 x0[5], x0[7], x1[5], x1[7], x0[1], x0[3], x1[1], x1[3] );

      hex( n4+1, w0[0], w0[2], w1[0], w1[2], v0[1], v0[5], v1[1], v1[5],
                 y0[0], y0[2], y1[0], y1[2], x0[1], x0[5], x1[1], x1[5] );

      hex( n4+2, v0[0], v0[4], v1[0], v1[4], w0[0], w0[2], w1[0], w1[2],
                 x0[0], x0[4], x1[0], x1[4], y0[0], y0[2], y1[0], y1[2] );

      hex( n4+3, v0[0], v0[2], v1[0], v1[2], v0[4], v0[6], v1[4], v1[6],
                 x0[0], x0[2], x1[0], x1[2], x0[4], x0[6], x1[4], x1[6] );

      hex( n4+4, v0[6], v0[2], v1[6], v1[2], w0[3], w0[1], w1[3], w1[1],
                 x0[6], x0[2], x1[6], x1[2], y0[3], y0[1], y1[3], y1[1] );

      hex( n4+5, w0[3], w0[1], w1[3], w1[1], v0[7], v0[3], v1[7], v1[3],
                 y0[3], y0[1], y1[3], y1[1], x0[7], x0[3], x1[7], x1[3] );

      hex( n4+6, w0[2], w0[3], w1[2], w1[3], v0[5], v0[7], v1[5], v1[7],
                 y0[2], y0[3], y1[2], y1[3], x0[5], x0[7], x1[5], x1[7] );

      hex( n4+7, v0[4], v0[6], v1[4], v1[6], w0[2], w0[3], w1[2], w1[3],
                 x0[4], x0[6], x1[4], x1[6], y0[2], y0[3], y1[2], y1[3] );

      // internal faces
      force( n4+0,0, n4+1,5 );
      force( n4+1,4, n4+2,5 );
      force( n4+2,4, n4+3,0 );
      force( n4+3,1, n4+4,4 );
      force( n4+4,5, n4+5,4 );
      force( n4+5,5, n4+0,1 );

      force( n4+0,4, n4+6,5 );
      force( n4+1,1, n4+6,0 );
      force( n4+2,1, n4+7,0 );
      force( n4+3,5, n4+7,4 );
      force( n4+4,0, n4+7,1 );
      force( n4+5,0, n4+6,1 );
      force( n4+6,4, n4+7,5 );

      // faces towards existing mesh
      force( n4+0,2, -1,-1 );
      force( n4+1,2, -1,-1 );
      force( n4+2,2, -1,-1 );
      force( n4+3,2, -1,-1 );
      force( n4+4,2, -1,-1 );
      force( n4+5,2, -1,-1 );
      force( n4+6,2, -1,-1 );
      force( n4+7,2, -1,-1 );

      // faces towards hole
      force( n4+0,3, -1,-1 );
      force( n4+1,3, -1,-1 );
      force( n4+2,3, -1,-1 );
      force( n4+3,3, -1,-1 );
      force( n4+4,3, -1,-1 );
      force( n4+5,3, -1,-1 );
      force( n4+6,3, -1,-1 );
      force( n4+7,3, -1,-1 );

      // faces around surface of hole
      force( n4+0,5, -1,-1 );
      force( n4+1,0, -1,-1 );
      force( n4+2,0, -1,-1 );
      force( n4+3,4, -1,-1 );
      force( n4+4,1, -1,-1 );
      force( n4+5,1, -1,-1 );

      // boundary group for hole surface
      attach( bg[0], n4+0,5, -1,-1,-1 );
      attach( bg[1], n4+1,0, -1,-1,-1 );
      attach( bg[1], n4+2,0, -1,-1,-1 );
      attach( bg[2], n4+3,4, -1,-1,-1 );
      attach( bg[2], n4+4,1, -1,-1,-1 );
      attach( bg[3], n4+5,1, -1,-1,-1 );

      // boundary group for cross section of hole
      attach( bg[4], n4+0,3, -1,-1,-1 );
      attach( bg[4], n4+1,3, -1,-1,-1 );
      attach( bg[4], n4+2,3, -1,-1,-1 );
      attach( bg[4], n4+3,3, -1,-1,-1 );
      attach( bg[4], n4+4,3, -1,-1,-1 );
      attach( bg[4], n4+5,3, -1,-1,-1 );
      attach( bg[4], n4+6,3, -1,-1,-1 );
      attach( bg[4], n4+7,3, -1,-1,-1 );

      // attaching to existing mesh
      attach( (MXGRP-1), n4+0,2, -1,-1,-1 );
      attach( (MXGRP-1), n4+1,2, -1,-1,-1 );
      attach( (MXGRP-1), n4+2,2, -1,-1,-1 );
      attach( (MXGRP-1), n4+3,2, -1,-1,-1 );
      attach( (MXGRP-1), n4+4,2, -1,-1,-1 );
      attach( (MXGRP-1), n4+5,2, -1,-1,-1 );
      attach( (MXGRP-1), n4+6,2, -1,-1,-1 );
      attach( (MXGRP-1), n4+7,2, -1,-1,-1 );

      b = hx[n0+4].q[4].g;
      a = hx[n0+4].q[4].b;
      detach( b, a );

      b = hx[f2].q[3].g;
      a = hx[f2].q[3].b;
      detach( b, a );

      b = hx[f4].q[3].g;
      a = hx[f4].q[3].b;
      detach( b, a );

      b = hx[n3].q[3].g;
      a = hx[n3].q[3].b;
      detach( b, a );

      b = hx[l4].q[3].g;
      a = hx[l4].q[3].b;
      detach( b, a );

      b = hx[l2].q[3].g;
      a = hx[l2].q[3].b;
      detach( b, a );

      b = hx[m2].q[3].g;
      a = hx[m2].q[3].b;
      detach( b, a );

      b = hx[m4].q[3].g;
      a = hx[m4].q[3].b;
      detach( b, a );

      attach( (MXGRP-1), n0+4,4, -1,-1,-1 );
      attach( (MXGRP-1), f2,  3, -1,-1,-1 );
      attach( (MXGRP-1), f4,  3, -1,-1,-1 );
      attach( (MXGRP-1), n3,  3, -1,-1,-1 );
      attach( (MXGRP-1), m2,  3, -1,-1,-1 );
      attach( (MXGRP-1), m4,  3, -1,-1,-1 );
      attach( (MXGRP-1), l2,  3, -1,-1,-1 );
      attach( (MXGRP-1), l4,  3, -1,-1,-1 );

      join( n4+0,2, n0+4,4 );
      join( n4+1,2, f2,3 );
      join( n4+2,2, f4,3 );
      join( n4+3,2, n3,3 );
      join( n4+4,2, l4,3 );
      join( n4+5,2, l2,3 );
      join( n4+6,2, m2,3 );
      join( n4+7,2, m4,3 );


      INT_6 msk={-1,-1,-1,-1,-1,-1};
      del(k0, msk );
      del(k1, msk );

      return;
  }
