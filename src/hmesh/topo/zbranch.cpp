#  include <hmesh/hmesh.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void hmesh_t::zbranch( REAL_3 vv, INT_ id, dir_t  d0, dir_t d1, INT_ *mask, bool flag )
  {

      INT_            n;

      INT_            iv[64]; // old vertices
      INT_            ix[64]; // old vertices

      INT_            jv[64]; // old vertices
      INT_            jq[64]; // old vertices

      INT_            iq[64]; // old faces
      INT_            ig[64]; // old groups
      INT_            ib[64]; // old group faces

      INT_            x,v;
      INT_            k0,k1;

      VINT_           mark(hx.n,-1);

      const REAL_     c1=1./9.,c2=2./9.,c3=4./9.;
      frme_t          a[2];

      a[0][0]= d0;
      a[0][1]= d1;
      a[0][2]= vec(d0,d1);

      k0= indx[id];
 
      a[1][0]= a[0][0];
      a[1][1]= a[0][1];
      a[1][2]= a[0][2];

      k1= k0;
      assert( walk( k1,a[1], mark ) );

      jv[  0]=  coor( !(a[0][0]),!(a[0][1]),!(a[0][2]) );
      jv[  1]=  coor(  (a[0][0]),!(a[0][1]),!(a[0][2]) );
      jv[  2]=  coor( !(a[0][0]), (a[0][1]),!(a[0][2]) );
      jv[  3]=  coor(  (a[0][0]), (a[0][1]),!(a[0][2]) );

      jv[  4]=  coor( !(a[0][0]),!(a[0][1]), (a[0][2]) );
      jv[  5]=  coor(  (a[0][0]),!(a[0][1]), (a[0][2]) );
      jv[  6]=  coor( !(a[0][0]), (a[0][1]), (a[0][2]) );
      jv[  7]=  coor(  (a[0][0]), (a[0][1]), (a[0][2]) );

      jv[8+0]=  coor( !(a[1][0]),!(a[1][1]),!(a[1][2]) );
      jv[8+1]=  coor(  (a[1][0]),!(a[1][1]),!(a[1][2]) );
      jv[8+2]=  coor( !(a[1][0]), (a[1][1]),!(a[1][2]) );
      jv[8+3]=  coor(  (a[1][0]), (a[1][1]),!(a[1][2]) );

      jv[8+4]=  coor( !(a[1][0]),!(a[1][1]), (a[1][2]) );
      jv[8+5]=  coor(  (a[1][0]),!(a[1][1]), (a[1][2]) );
      jv[8+6]=  coor( !(a[1][0]), (a[1][1]), (a[1][2]) );
      jv[8+7]=  coor(  (a[1][0]), (a[1][1]), (a[1][2]) );

      iv[ 0]= hx[k0].v[ jv[  0] ];
      iv[ 1]= hx[k0].v[ jv[  1] ];
      iv[ 2]= hx[k0].v[ jv[  2] ];
      iv[ 3]= hx[k0].v[ jv[  3] ];

      iv[ 4]= hx[k0].v[ jv[  4] ];
      iv[ 5]= hx[k0].v[ jv[  5] ];
      iv[ 6]= hx[k0].v[ jv[  6] ];
      iv[ 7]= hx[k0].v[ jv[  7] ];

      assert( iv[ 4]==hx[k1].v[ jv[8+0] ] );
      assert( iv[ 5]==hx[k1].v[ jv[8+1] ] );
      assert( iv[ 6]==hx[k1].v[ jv[8+2] ] );
      assert( iv[ 7]==hx[k1].v[ jv[8+3] ] );

      iv[ 8]= hx[k1].v[ jv[8+4] ];
      iv[ 9]= hx[k1].v[ jv[8+5] ];
      iv[10]= hx[k1].v[ jv[8+6] ];
      iv[11]= hx[k1].v[ jv[8+7] ];

      ix[ 0]= hx[k0].x[ jv[  0] ]; assert( ix[ 0]==iv[ 0] );
      ix[ 1]= hx[k0].x[ jv[  1] ]; assert( ix[ 1]==iv[ 1] );
      ix[ 2]= hx[k0].x[ jv[  2] ]; assert( ix[ 2]==iv[ 2] );
      ix[ 3]= hx[k0].x[ jv[  3] ]; assert( ix[ 3]==iv[ 3] );
                                                        
      ix[ 4]= hx[k0].x[ jv[  4] ]; assert( ix[ 4]==iv[ 4] );
      ix[ 5]= hx[k0].x[ jv[  5] ]; assert( ix[ 5]==iv[ 5] );
      ix[ 6]= hx[k0].x[ jv[  6] ]; assert( ix[ 6]==iv[ 6] );
      ix[ 7]= hx[k0].x[ jv[  7] ]; assert( ix[ 7]==iv[ 7] );
                                                        
      ix[ 8]= hx[k1].x[ jv[8+4] ]; assert( ix[ 8]==iv[ 8] );
      ix[ 9]= hx[k1].x[ jv[8+5] ]; assert( ix[ 9]==iv[ 9] );
      ix[10]= hx[k1].x[ jv[8+6] ]; assert( ix[10]==iv[10] );
      ix[11]= hx[k1].x[ jv[8+7] ]; assert( ix[11]==iv[11] );


/*    jq[  0]=  hx[k0].face( !(a[0][0]) );
      jq[  1]=  hx[k0].face(  (a[0][0]) );
      jq[  2]=  hx[k0].face( !(a[0][1]) );
      jq[  3]=  hx[k0].face(  (a[0][1]) );
      jq[  4]=  hx[k0].face( !(a[0][2]) ); // <--- this is the wall!
      jq[  5]=  hx[k0].face(  (a[0][2]) );

      jq[6+0]=  hx[k1].face( !(a[0][0]) );
      jq[6+1]=  hx[k1].face(  (a[0][0]) );
      jq[6+2]=  hx[k1].face( !(a[0][1]) );
      jq[6+3]=  hx[k1].face(  (a[0][1]) );
      jq[6+4]=  hx[k1].face( !(a[0][2]) );
      jq[6+5]=  hx[k1].face(  (a[0][2]) ); // <--- this is the top!*/

      jq[  0]=  face( !(a[0][0]) );
      jq[  1]=  face(  (a[0][0]) );
      jq[  2]=  face( !(a[0][1]) );
      jq[  3]=  face(  (a[0][1]) );
      jq[  4]=  face( !(a[0][2]) ); // <--- this is the wall!
      jq[  5]=  face(  (a[0][2]) );

      jq[6+0]=  face( !(a[0][0]) );
      jq[6+1]=  face(  (a[0][0]) );
      jq[6+2]=  face( !(a[0][1]) );
      jq[6+3]=  face(  (a[0][1]) );
      jq[6+4]=  face( !(a[0][2]) );
      jq[6+5]=  face(  (a[0][2]) ); // <--- this is the top!

      iq[  0]= hx[k0].q[jq[  0]].q;
      iq[  1]= hx[k0].q[jq[  1]].q;
      iq[  2]= hx[k0].q[jq[  2]].q;
      iq[  3]= hx[k0].q[jq[  3]].q;
      iq[  4]= hx[k0].q[jq[  4]].q;
      iq[  5]= hx[k0].q[jq[  5]].q;

      iq[  6]= hx[k0].q[jq[6+0]].q;
      iq[  7]= hx[k0].q[jq[6+1]].q;
      iq[  8]= hx[k0].q[jq[6+2]].q;
      iq[  9]= hx[k0].q[jq[6+3]].q;
      assert( iq[  5]== hx[k1].q[jq[6+4]].q );
      iq[ 10]= hx[k0].q[jq[6+5]].q;

      ig[  0]= hx[k0].q[jq[  0]].g;
      ig[  1]= hx[k0].q[jq[  1]].g;
      ig[  2]= hx[k0].q[jq[  2]].g;
      ig[  3]= hx[k0].q[jq[  3]].g;
      ig[  4]= hx[k0].q[jq[  4]].g;
      ig[  5]= hx[k0].q[jq[  5]].g;

      ig[  6]= hx[k0].q[jq[6+0]].g;
      ig[  7]= hx[k0].q[jq[6+1]].g;
      ig[  8]= hx[k0].q[jq[6+2]].g;
      ig[  9]= hx[k0].q[jq[6+3]].g;
      ig[ 10]= hx[k0].q[jq[6+5]].g;

      ib[  0]= hx[k0].q[jq[  0]].b;
      ib[  1]= hx[k0].q[jq[  1]].b;
      ib[  2]= hx[k0].q[jq[  2]].b;
      ib[  3]= hx[k0].q[jq[  3]].b;
      ib[  4]= hx[k0].q[jq[  4]].b;
      ib[  5]= hx[k0].q[jq[  5]].b;

      ib[  6]= hx[k0].q[jq[6+0]].b;
      ib[  7]= hx[k0].q[jq[6+1]].b;
      ib[  8]= hx[k0].q[jq[6+2]].b;
      ib[  9]= hx[k0].q[jq[6+3]].b;
      ib[ 10]= hx[k0].q[jq[6+5]].b;

      
      x= vx.n; vx.n+= 32; vx.resize( xdum );
      v= vt.n; vt.n+= 32; vt.resize( pdum );

      if( flag )
     {
         n= hx.n; hx.n+= 16; hx.resize( hdum );
     }
      else
     {
         n= hx.n; hx.n+= 11; hx.resize( hdum );
     }

      vx[x]= c3*vx[ix[0]]+ c2*vx[ix[1]]+ c2*vx[ix[ 2]]+ c1*vx[ix[ 3]]; ix[12]=x++;
      vx[x]= c2*vx[ix[0]]+ c3*vx[ix[1]]+ c1*vx[ix[ 2]]+ c2*vx[ix[ 3]]; ix[13]=x++;
      vx[x]= c2*vx[ix[0]]+ c1*vx[ix[1]]+ c3*vx[ix[ 2]]+ c2*vx[ix[ 3]]; ix[14]=x++;
      vx[x]= c1*vx[ix[0]]+ c2*vx[ix[1]]+ c2*vx[ix[ 2]]+ c3*vx[ix[ 3]]; ix[15]=x++;
      vx[x]= c3*vx[ix[4]]+ c2*vx[ix[5]]+ c2*vx[ix[ 6]]+ c1*vx[ix[ 7]]; ix[16]=x++;
      vx[x]= c2*vx[ix[4]]+ c3*vx[ix[5]]+ c1*vx[ix[ 6]]+ c2*vx[ix[ 7]]; ix[17]=x++;
      vx[x]= c2*vx[ix[4]]+ c1*vx[ix[5]]+ c3*vx[ix[ 6]]+ c2*vx[ix[ 7]]; ix[18]=x++;
      vx[x]= c1*vx[ix[4]]+ c2*vx[ix[5]]+ c2*vx[ix[ 6]]+ c3*vx[ix[ 7]]; ix[19]=x++;
      vx[x]= c3*vx[ix[8]]+ c2*vx[ix[9]]+ c2*vx[ix[10]]+ c1*vx[ix[11]]; ix[20]=x++;
      vx[x]= c2*vx[ix[8]]+ c3*vx[ix[9]]+ c1*vx[ix[10]]+ c2*vx[ix[11]]; ix[21]=x++;
      vx[x]= c2*vx[ix[8]]+ c1*vx[ix[9]]+ c3*vx[ix[10]]+ c2*vx[ix[11]]; ix[22]=x++;
      vx[x]= c1*vx[ix[8]]+ c2*vx[ix[9]]+ c2*vx[ix[10]]+ c3*vx[ix[11]]; ix[23]=x++;

      vx[20]= 0.5*( vx[20]+ vx[16] );
      vx[21]= 0.5*( vx[21]+ vx[17] );
      vx[22]= 0.5*( vx[22]+ vx[18] );
      vx[23]= 0.5*( vx[23]+ vx[19] );
 
      vt[v].id= v; iv[12]= v++; 
      vt[v].id= v; iv[13]= v++; 
      vt[v].id= v; iv[14]= v++; 
      vt[v].id= v; iv[15]= v++; 

      vt[v].id= v; iv[16]= v++; 
      vt[v].id= v; iv[17]= v++; 
      vt[v].id= v; iv[18]= v++; 
      vt[v].id= v; iv[19]= v++; 

      vt[v].id= v; iv[20]= v++; 
      vt[v].id= v; iv[21]= v++; 
      vt[v].id= v; iv[22]= v++; 
      vt[v].id= v; iv[23]= v++; 

      assert( fabs(vv[0])+fabs(vv[1])+ fabs(vv[2]) > 0 );

      vx[x]= vx[ ix[0] ]- vv; ix[24]= x++; 
      vx[x]= vx[ ix[1] ]- vv; ix[25]= x++; 
      vx[x]= vx[ ix[2] ]- vv; ix[26]= x++; 
      vx[x]= vx[ ix[3] ]- vv; ix[27]= x++; 

      vx[x]= vx[ ix[12] ]- vv; ix[28]= x++; 
      vx[x]= vx[ ix[13] ]- vv; ix[29]= x++; 
      vx[x]= vx[ ix[14] ]- vv; ix[30]= x++; 
      vx[x]= vx[ ix[15] ]- vv; ix[31]= x++; 

      vt[x].id= v; iv[24]= v++; 
      vt[x].id= v; iv[25]= v++; 
      vt[x].id= v; iv[26]= v++; 
      vt[x].id= v; iv[27]= v++; 

      vt[x].id= v; iv[28]= v++; 
      vt[x].id= v; iv[29]= v++; 
      vt[x].id= v; iv[30]= v++; 
      vt[x].id= v; iv[31]= v++; 

      hex( n+ 0, iv[12],iv[13],iv[14],iv[15], iv[16],iv[17],iv[18],iv[19], ix[12],ix[13],ix[14],ix[15], ix[16],ix[17],ix[18],ix[19] );
      hex( n+ 1, iv[ 0],iv[12],iv[ 2],iv[14], iv[ 4],iv[16],iv[ 6],iv[18], ix[ 0],ix[12],ix[ 2],ix[14], ix[ 4],ix[16],ix[ 6],ix[18] );
      hex( n+ 2, iv[13],iv[ 1],iv[15],iv[ 3], iv[17],iv[ 5],iv[19],iv[ 7], ix[13],ix[ 1],ix[15],ix[ 3], ix[17],ix[ 5],ix[19],ix[ 7] );
      hex( n+ 3, iv[ 0],iv[ 1],iv[12],iv[13], iv[ 4],iv[ 5],iv[16],iv[17], ix[ 0],ix[ 1],ix[12],ix[13], ix[ 4],ix[ 5],ix[16],ix[17] );
      hex( n+ 4, iv[14],iv[15],iv[ 2],iv[ 3], iv[18],iv[19],iv[ 6],iv[ 7], ix[14],ix[15],ix[ 2],ix[ 3], ix[18],ix[19],ix[ 6],ix[ 7] );

      hex( n+ 5, iv[16],iv[17],iv[18],iv[19], iv[20],iv[21],iv[22],iv[23], ix[16],ix[17],ix[18],ix[19], ix[20],ix[21],ix[22],ix[23] );
      hex( n+ 6, iv[ 4],iv[16],iv[ 6],iv[18], iv[ 8],iv[20],iv[10],iv[22], ix[ 4],ix[16],ix[ 6],ix[18], ix[ 8],ix[20],ix[10],ix[22] );
      hex( n+ 7, iv[17],iv[ 5],iv[19],iv[ 7], iv[21],iv[ 9],iv[23],iv[11], ix[17],ix[ 5],ix[19],ix[ 7], ix[21],ix[ 9],ix[23],ix[11] );
      hex( n+ 8, iv[ 4],iv[ 5],iv[16],iv[17], iv[ 8],iv[ 9],iv[20],iv[21], ix[ 4],ix[ 5],ix[16],ix[17], ix[ 8],ix[ 9],ix[20],ix[21] );
      hex( n+ 9, iv[18],iv[19],iv[ 6],iv[ 7], iv[22],iv[23],iv[10],iv[11], ix[18],ix[19],ix[ 6],ix[ 7], ix[22],ix[23],ix[10],ix[11] );

      hex( n+10, iv[20],iv[21],iv[22],iv[23], iv[ 8],iv[ 9],iv[10],iv[11], ix[20],ix[21],ix[22],ix[23], ix[ 8],ix[ 9],ix[10],ix[11] );
      
      if( flag )
     {
         hex( n+11, iv[28],iv[29],iv[30],iv[31], iv[12],iv[13],iv[14],iv[15], ix[28],ix[29],ix[30],ix[31], ix[12],ix[13],ix[14],ix[15] );
         hex( n+12, iv[24],iv[28],iv[26],iv[30], iv[ 0],iv[12],iv[ 2],iv[14], ix[24],ix[28],ix[26],ix[30], ix[ 0],ix[12],ix[ 2],ix[14] );
         hex( n+13, iv[29],iv[25],iv[31],iv[27], iv[13],iv[ 1],iv[15],iv[ 3], ix[29],ix[25],ix[31],ix[27], ix[13],ix[ 1],ix[15],ix[ 3] );
         hex( n+14, iv[24],iv[25],iv[28],iv[29], iv[ 0],iv[ 1],iv[12],iv[13], ix[24],ix[25],ix[28],ix[29], ix[ 0],ix[ 1],ix[12],ix[13] );
         hex( n+15, iv[30],iv[31],iv[26],iv[27], iv[14],iv[15],iv[ 2],iv[ 3], ix[30],ix[31],ix[26],ix[27], ix[14],ix[15],ix[ 2],ix[ 3] );

         force( n+11,0, n+12,1 );
         force( n+11,1, n+13,0 );
         force( n+11,2, n+14,3 );
         force( n+11,3, n+15,2 );
   
         force( n+12,2, n+14,0 );
         force( n+12,3, n+15,0 );
         force( n+13,2, n+14,1 );
         force( n+13,3, n+15,1 );
   
         force( n+11,4, -1,-1 );
         force( n+12,4, -1,-1 );
         force( n+13,4, -1,-1 );
         force( n+14,4, -1,-1 );
         force( n+15,4, -1,-1 );
   
         force( n+12,0, -1,-1 );
         force( n+13,1, -1,-1 );
         force( n+14,2, -1,-1 );
         force( n+15,3, -1,-1 );
   
         force( n+ 0,4, n+11,5 );
         force( n+ 1,4, n+12,5 );
         force( n+ 2,4, n+13,5 );
         force( n+ 3,4, n+14,5 );
         force( n+ 4,4, n+15,5 );
     }
      else
     {
         force( n+ 0,4, -1,-1 );
         force( n+ 1,4, -1,-1 );
         force( n+ 2,4, -1,-1 );
         force( n+ 3,4, -1,-1 );
         force( n+ 4,4, -1,-1 );
     }

      force( n+ 0,0, n+ 1,1 );
      force( n+ 0,1, n+ 2,0 );
      force( n+ 0,2, n+ 3,3 );
      force( n+ 0,3, n+ 4,2 );

      force( n+ 1,2, n+ 3,0 );
      force( n+ 1,3, n+ 4,0 );
      force( n+ 2,2, n+ 3,1 );
      force( n+ 2,3, n+ 4,1 );

      force( n+ 5,0, n+ 6,1 );
      force( n+ 5,1, n+ 7,0 );
      force( n+ 5,2, n+ 8,3 );
      force( n+ 5,3, n+ 9,2 );

      force( n+ 6,2, n+ 8,0 );
      force( n+ 6,3, n+ 9,0 );
      force( n+ 7,2, n+ 8,1 );
      force( n+ 7,3, n+ 9,1 );

      force( n+ 0,5, n+ 5,4 );
      force( n+ 1,5, n+ 6,4 );
      force( n+ 2,5, n+ 7,4 );
      force( n+ 3,5, n+ 8,4 );
      force( n+ 4,5, n+ 9,4 );

      force( n+10,4, n+ 5,5 );
      force( n+10,0, n+ 6,5 );
      force( n+10,1, n+ 7,5 );
      force( n+10,2, n+ 8,5 );
      force( n+10,3, n+ 9,5 );

      replace( k0,jq[ 0], n+ 1,0 );
      replace( k0,jq[ 1], n+ 2,1 );
      replace( k0,jq[ 2], n+ 3,2 );
      replace( k0,jq[ 3], n+ 4,3 );

      replace( k1,jq[ 6], n+ 6,0 );
      replace( k1,jq[ 7], n+ 7,1 );
      replace( k1,jq[ 8], n+ 8,2 );
      replace( k1,jq[ 9], n+ 9,3 );

      replace( k1,jq[11], n+10,5 );

      detach( ig[  4],ib[  4] );  attach( (MXGRP-1), k0,jq[4], -1,-1,-1 ); 
      if( flag )
     {
         attach( mask[4],n+11,4, -1,-1,-1 );
         attach( mask[4],n+12,4, -1,-1,-1 );
         attach( mask[4],n+13,4, -1,-1,-1 );
         attach( mask[4],n+14,4, -1,-1,-1 );
         attach( mask[4],n+15,4, -1,-1,-1 );
         

         attach( mask[0],n+12,0, -1,-1,-1 );
         attach( mask[1],n+13,1, -1,-1,-1 );
         attach( mask[2],n+14,2, -1,-1,-1 );
         attach( mask[3],n+15,3, -1,-1,-1 );
     }
      else
     {

         attach( mask[4],n+ 0,4, -1,-1,-1 );
         attach( mask[4],n+ 1,4, -1,-1,-1 );
         attach( mask[4],n+ 2,4, -1,-1,-1 );
         attach( mask[4],n+ 3,4, -1,-1,-1 );
         attach( mask[4],n+ 4,4, -1,-1,-1 );
     }

      k0= hx[k0].id;
      k1= hx[k1].id;

      INT_ mask0[6]={-1,-1,-1,-1,-1,-1};

      assert( k0 == id );
      del( id,mask0 );

      del( k1,mask0 );
  }
