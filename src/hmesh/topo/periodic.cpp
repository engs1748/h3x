#  include <hmesh/hmesh.h>
#  include <boxes.h>


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void hmesh_t::periodic()
  {
      INT_         a,i,j,k,h,l,m,n,p,q;

      REAL_                        err=-1.;
      REAL_                          d;
      REAL_2                         z;
      REAL_3                         u;
      box_t                          g;

      wsize_t<REAL_,2,DLEN_H>        y;
      VINT_                       list;

      VINT_                        mk0(vt.n,-1);
      VINT_                        mk1(vt.n,-1);

      REAL_2                        y0={ 9999., 9999.};
      REAL_2                        y1={-9999.,-9999.};

      if( pr.n == 0 ){ return; }

// processing of periodic information needs to take place before any topological operation **/


      assert( vt.n == vx.n );
      assert( qv.n == 0 );
      assert( eg.n == 0 );

/* visit periodic surface pairs **/

      for( h=0;h<pr.n;h++ )
     {

         memset( mk0.data(),-1,mk0.n*sizeof(INT_));
         memset( mk1.data(),-1,mk1.n*sizeof(INT_));

         p= pr[h][0]; // frame
         q= pr[h][1]; // periodic axis

         l= pr[h][2]; // left surface
         m= pr[h][3]; // right surface

         assert( bz[l].bo.n == bz[m].bo.n );

         y0[0]= 9999.;
         y0[1]= 9999.;
         y1[0]=-9999.;
         y1[1]=-9999.;

         y.clear();
         list.clear();

// transform the coordinates of the vertices on the left surface and place in a list

         for( j=0;j<bz[l].bo.n;j++ )
        {
            k= bz[l].bo[j].h;
            m= bz[l].bo[j].q;

            for( a=0;a<4;a++ )
           {
               n= mhq[m][a];

               assert( hx[k].v[n] == hx[k].x[n] );

               n= hx[k].v[n];

               if( mk0[n]== -1 )
              {
                  i= y.append(-1.);
                  i= list.append(-1.);
                  
                  list[i]= n;
                  fr[p].redx( vx[n].x, y[i], q );

                  y0[0]= fmin( y0[0],y[i][0] );
                  y0[1]= fmin( y0[1],y[i][1] );

                  y1[0]= fmax( y1[0],y[i][0] );
                  y1[1]= fmax( y1[1],y[i][1] );

                  mk0[n]=i;
              }
           }

// hex face is no longer a boundary

            hx[k].q[m].b=-1;
            hx[k].q[m].g=-1;
        }
         
// sort in a small data structure for searching

         d= max( y1[0]-y0[0],y1[1]-y0[1] );
         d*= 0.01;

         y0[0]-= d;
         y0[1]-= d;

         y1[0]+= d;
         y1[1]+= d;

         g.clear();
         g.build( 1,1, y0,y1 );

         for( j=0;j<y.n;j++ ){ g.insert( j,y[j] ); }

// traverse right boundary and identify periodic vertex pairs

         l= pr[h][3];
         for( j=0;j<bz[l].bo.n;j++ )
        {
            k= bz[l].bo[j].h;
            m= bz[l].bo[j].q;
            for( a=0;a<4;a++ )
           {
               n= mhq[m][a];

               assert( hx[k].v[n] == hx[k].x[n] );

               n= hx[k].v[n];

               if( mk1[n]== -1 )
              {
                  fr[p].redx( vx[n].x, z, q );
                  g.search( z,y.data(), i,d );

                  fr[p].prdx( vx[list[i]].x, u,q, 1 );
                  err= fmax( err, fabs(u[0]- vx[n][0])+
                                  fabs(u[1]- vx[n][1])+
                                  fabs(u[2]- vx[n][2]) );

                  mk1[n   ]=list[i];
                  mk0[list[i]]=   n;

              }
               else
              {
                  assert( mk0[mk1[n]] == n );
              }
           }

// hex face is no longer a boundary 

            hx[k].q[m].b=-1;
            hx[k].q[m].g=-1;
        }

         for( j=0;j<list.n;j++ )
        { 
            l= list[j];
            m= mk0[l];

            assert( mk1[m]== l );

        }

     }

      printf( "maximum periodic error %12.5e\n",err );

/* now traverse the hex and replace matching vertices, but not their coordinates */


      for( k=0;k<hx.n;k++ )
     {
         for( j=0;j<8;j++ )
        {
            i= hx[k].v[j];
            if( mk0[i] > -1 )
           {
               i= mk0[i];
               hx[k].v[j]= i;
               hx[k].w[j]= vt[i].id;
           }
        }
     }

// empty boundary groups

// make a list of vertices to cull

      list.clear();
      for( i=0;i<vt.n;i++ )
     {
         if( mk1[i]>-1){ j=list.append(-1); list[j]=mk1[i]; };            
         mk1[i]=i;
     }

// permute vertices 

      h=vt.n;
      for( j=list.n-1;j>=0;j-- )
     {
         i=list[j];

         h--;
         assert( i<=h );
         swap( vt[i], vt[h] ); 
         swap( mk1[i], mk1[h] ); 

     }
      for( j=0;j<vt.n;j++ )
     {
         i= mk1[j];
         mk0[i]= j;
     }

// cull unused vertices 

      for( k=0;k<hx.n;k++ )
     {
         for( j=0;j<8;j++ )
        { 
            i= hx[k].v[j];
            i= mk0[i];
            assert( i > -1 && i < h );

            hx[k].v[j]= i;
            assert( hx[k].w[j] == vt[i].id );
        }
     }

      vt.n=h;

// destroy periodic boundaries

      for( h=0;h<pr.n;h++ )
     {
         l= pr[h][2]; // left surface
         m= pr[h][3]; // right surface

         bz[l].bo.clear(); 
         bz[m].bo.clear();
     }

      return;

 
  }
