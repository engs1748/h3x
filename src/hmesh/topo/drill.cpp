
#  include <hmesh/hmesh.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void hmesh_t::drill( INT_ i0, frme_t a0, VINT_ &list, frme_t *c )
  {
      frme_t              a;
      INT_                i,j;
      VINT_               mk(hx.n,-1);

      assert( list.n == 0 );
      a0[2]= vec(a0[0],a0[1]);

      i= i0;
      a[0]= a0[0];
      a[1]= a0[1];
      a[2]= a0[2];

      i= indx[i0];
      assert( i > -1 );
 
      j=0;
      do
     {
         j= list.append(-1);
         list[j]= i;
         c[j][0]=a[0];
         c[j][1]=a[1];
         c[j][2]=a[2];

     }while( walk(i,a,mk) );

      i= indx[i0];
      a[0]=  a0[0];
      a[1]=  a0[1];
      a[2]=!(a0[2]);
      
      while( walk(i,a,mk) )
     {
         j= list.append(-1);
         list[j]= i;
         c[j][0]=a[0];
         c[j][1]=a[1];
         c[j][2]=a[2];
     }

      return;
  }

