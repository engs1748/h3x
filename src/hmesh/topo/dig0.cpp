#  include <hmesh/hmesh.h>

/* struct href_t
  {
      frme_t    f;
      INT_      k;
  };*/


   void hmesh_t::dig0( dir_t d0, dir_t d1, INT_ kst, REAL_3 v, INT_ *mask )
  {

      INT_            n,m;
      INT_            i;

      vtx_t           z;
      vtx_t           s;
      vtx_t           r;

      REAL_           d;
      REAL_           f,g;

      cavty_t         cv;

      g= 0.2;
      z[0]= v[0];
      z[1]= v[1];
      z[2]= v[2];
      f= l2norm(z);
      d= 1./f;
      z*= d;

      cvty( 2,2,1, d0,d1, kst, cv, true );

      r= 0.25*( vx[cv.v[0]]+ vx[cv.v[1]]+ vx[cv.v[2]]+ vx[cv.v[3]] );

// initial points

      n= vx.n; vx.n+= 91; vx.resize( xdum );
               vt.n+= 91; vt.resize( pdum );

      for( i=n;i<vt.n;i++ ){ vt[i].id= i; };

      vx[n+ 0]= vx[cv.v[ 4 ]]- r; vx[n+ 0]*= 0.6667; vx[n+ 0]+= r;
      vx[n+ 2]= vx[cv.v[ 5 ]]- r; vx[n+ 2]*= 0.6667; vx[n+ 2]+= r;

      vx[n+ 6]= vx[cv.v[ 6 ]]- r; vx[n+ 6]*= 0.6667; vx[n+ 6]+= r;
      vx[n+ 8]= vx[cv.v[ 7 ]]- r; vx[n+ 8]*= 0.6667; vx[n+ 8]+= r;

      vx[n+18]= vx[cv.v[ 0 ]]- r; vx[n+18]*= 0.6667; vx[n+18]+= r;
      vx[n+20]= vx[cv.v[ 1 ]]- r; vx[n+20]*= 0.6667; vx[n+20]+= r;

      vx[n+32]= vx[cv.v[ 2 ]]- r; vx[n+32]*= 0.6667; vx[n+32]+= r;
      vx[n+34]= vx[cv.v[ 3 ]]- r; vx[n+34]*= 0.6667; vx[n+34]+= r;

// complete A section

      vx[n+ 1]=   0.5*( vx[n+ 0]+ vx[n+ 2] );
      vx[n+ 3]=   0.5*( vx[n+ 0]+ vx[n+ 6] );
      vx[n+ 5]=   0.5*( vx[n+ 2]+ vx[n+ 8] );
      vx[n+ 4]=   0.5*( vx[n+ 3]+ vx[n+ 5] );
      vx[n+ 7]=   0.5*( vx[n+ 6]+ vx[n+ 8] );

// complete C section

      vx[n+19]=   0.5*( vx[n+18]+ vx[n+20] );      
      vx[n+24]=   0.5*( vx[n+18]+ vx[n+32] );      
      vx[n+28]=   0.5*( vx[n+20]+ vx[n+34] );      
      vx[n+33]=   0.5*( vx[n+32]+ vx[n+34] );      
      vx[n+26]=   0.5*( vx[n+24]+ vx[n+28] );      

      vx[n+22]=   0.5*( vx[n+19]+ vx[n+26] );      
      vx[n+25]=   0.5*( vx[n+24]+ vx[n+26] );      
      vx[n+27]=   0.5*( vx[n+28]+ vx[n+26] );      
      vx[n+30]=   0.5*( vx[n+33]+ vx[n+26] );      

      vx[n+21]=  0.25*( vx[n+18]+ vx[n+19]+ vx[n+26]+ vx[n+24] );
      vx[n+23]=  0.25*( vx[n+19]+ vx[n+20]+ vx[n+26]+ vx[n+28] );
      vx[n+29]=  0.25*( vx[n+24]+ vx[n+26]+ vx[n+32]+ vx[n+33] );
      vx[n+31]=  0.25*( vx[n+26]+ vx[n+28]+ vx[n+33]+ vx[n+34] );

// B section

      vx[n+13]=   0.5*( vx[n+ 4]+ vx[n+26] );
      vx[n+10]=  0.25*( vx[n+ 4]+ vx[n+ 1]+ vx[n+26]+ vx[n+19] );
      vx[n+12]=  0.25*( vx[n+ 4]+ vx[n+ 3]+ vx[n+26]+ vx[n+24] );
      vx[n+14]=  0.25*( vx[n+ 4]+ vx[n+ 5]+ vx[n+26]+ vx[n+28] );
      vx[n+16]=  0.25*( vx[n+ 4]+ vx[n+ 7]+ vx[n+26]+ vx[n+33] );

      vx[n+ 9]= 0.125*( vx[n+ 0]+ vx[n+ 1]+ vx[n+ 4]+ vx[n+ 3]+ vx[n+18]+ vx[n+19]+ vx[n+24]+ vx[n+26] );
      vx[n+11]= 0.125*( vx[n+ 1]+ vx[n+ 2]+ vx[n+ 4]+ vx[n+ 5]+ vx[n+19]+ vx[n+20]+ vx[n+26]+ vx[n+28] );
      vx[n+15]= 0.125*( vx[n+ 3]+ vx[n+ 4]+ vx[n+ 6]+ vx[n+ 7]+ vx[n+24]+ vx[n+26]+ vx[n+32]+ vx[n+33] );
      vx[n+17]= 0.125*( vx[n+ 4]+ vx[n+ 5]+ vx[n+ 7]+ vx[n+ 8]+ vx[n+26]+ vx[n+28]+ vx[n+33]+ vx[n+34] );

// D section      

      s= g*z;

      vx[n+35]= vx[n+18]+ s;
      vx[n+36]= vx[n+19]+ s;
      vx[n+37]= vx[n+20]+ s;
      vx[n+38]= vx[n+21]+ s;
      vx[n+39]= vx[n+22]+ s;
      vx[n+40]= vx[n+23]+ s;
      vx[n+41]= vx[n+24]+ s;
      vx[n+42]= vx[n+25]+ s;
      vx[n+43]= vx[n+26]+ s;
      vx[n+44]= vx[n+27]+ s;
      vx[n+45]= vx[n+28]+ s;
      vx[n+46]= vx[n+29]+ s;
      vx[n+47]= vx[n+30]+ s;
      vx[n+48]= vx[n+31]+ s;
      vx[n+49]= vx[n+32]+ s;
      vx[n+50]= vx[n+33]+ s;
      vx[n+51]= vx[n+34]+ s;

// H section

      vx[n+75]= vx[n+26]+ z;

      orth( vx[n+75],z,vx[n+36], vx[n+73] );
      orth( vx[n+75],z,vx[n+39], vx[n+74] );
      orth( vx[n+75],z,vx[n+47], vx[n+76] );
      orth( vx[n+75],z,vx[n+50], vx[n+77] );

      orth( vx[n+75],z,vx[n+35], vx[n+78] );
      orth( vx[n+75],z,vx[n+37], vx[n+79] );
      orth( vx[n+75],z,vx[n+38], vx[n+80] );
      orth( vx[n+75],z,vx[n+40], vx[n+81] );
      orth( vx[n+75],z,vx[n+41], vx[n+82] );
      orth( vx[n+75],z,vx[n+42], vx[n+83] );
      orth( vx[n+75],z,vx[n+44], vx[n+84] );
      orth( vx[n+75],z,vx[n+45], vx[n+85] );
      orth( vx[n+75],z,vx[n+46], vx[n+86] );
      orth( vx[n+75],z,vx[n+48], vx[n+87] );
      orth( vx[n+75],z,vx[n+49], vx[n+88] );
      orth( vx[n+75],z,vx[n+51], vx[n+89] );

// K section

      vx[n+62]=  0.5*( vx[n+73]+ vx[n+36] );
      vx[n+63]=  0.5*( vx[n+79]+ vx[n+37] );
      vx[n+64]=  0.5*( vx[n+74]+ vx[n+39] );

      vx[n+65]=  0.5*( vx[n+81]+ vx[n+40] );
      vx[n+66]=  0.5*( vx[n+75]+ vx[n+43] );
      vx[n+67]=  0.5*( vx[n+84]+ vx[n+44] );
      vx[n+68]=  0.5*( vx[n+85]+ vx[n+45] );
      vx[n+69]=  0.5*( vx[n+76]+ vx[n+47] );
      vx[n+70]=  0.5*( vx[n+87]+ vx[n+48] );
      vx[n+71]=  0.5*( vx[n+77]+ vx[n+50] );
      vx[n+72]=  0.5*( vx[n+89]+ vx[n+51] );

// E section

      vx[n+52]=  0.5*( vx[n+62]+ vx[n+36] );
      vx[n+53]=  0.5*( vx[n+63]+ vx[n+37] );
      vx[n+54]=  0.5*( vx[n+64]+ vx[n+39] );
      vx[n+55]=  0.5*( vx[n+65]+ vx[n+40] );
      vx[n+56]=  0.5*( vx[n+66]+ vx[n+43] );
      vx[n+57]=  0.5*( vx[n+67]+ vx[n+44] );
      vx[n+90]=  0.5*( vx[n+68]+ vx[n+45] ); 
      vx[n+58]=  0.5*( vx[n+69]+ vx[n+47] ); 
      vx[n+59]=  0.5*( vx[n+70]+ vx[n+48] ); 
      vx[n+60]=  0.5*( vx[n+71]+ vx[n+50] ); 
      vx[n+61]=  0.5*( vx[n+72]+ vx[n+51] ); 

// form hexahedra

      m= hx.n; hx.n+= 56+12; hx.resize( hdum );

      hex( m+ 0, n+ 9,n+10,n+12,n+13,n+ 0,n+ 1,n+ 3,n+ 4, n+ 9,n+10,n+12,n+13,n+ 0,n+ 1,n+ 3,n+ 4 );
      hex( m+ 1, n+10,n+11,n+13,n+14,n+ 1,n+ 2,n+ 4,n+ 5, n+10,n+11,n+13,n+14,n+ 1,n+ 2,n+ 4,n+ 5 );
      hex( m+ 2, n+12,n+13,n+15,n+16,n+ 3,n+ 4,n+ 6,n+ 7, n+12,n+13,n+15,n+16,n+ 3,n+ 4,n+ 6,n+ 7 );
      hex( m+ 3, n+13,n+14,n+16,n+17,n+ 4,n+ 5,n+ 7,n+ 8, n+13,n+14,n+16,n+17,n+ 4,n+ 5,n+ 7,n+ 8 );

      hex( m+ 4, n+21,n+22,n+25,n+26,n+ 9,n+10,n+12,n+13, n+21,n+22,n+25,n+26,n+ 9,n+10,n+12,n+13 );
      hex( m+ 5, n+22,n+23,n+26,n+27,n+10,n+11,n+13,n+14, n+22,n+23,n+26,n+27,n+10,n+11,n+13,n+14 );
      hex( m+ 6, n+25,n+26,n+29,n+30,n+12,n+13,n+15,n+16, n+25,n+26,n+29,n+30,n+12,n+13,n+15,n+16 );
      hex( m+ 7, n+26,n+27,n+30,n+31,n+13,n+14,n+16,n+17, n+26,n+27,n+30,n+31,n+13,n+14,n+16,n+17 );

      hex( m+ 8, n+18,n+19,n+21,n+22,n+ 0,n+ 1,n+ 9,n+10, n+18,n+19,n+21,n+22,n+ 0,n+ 1,n+ 9,n+10 );
      hex( m+ 9, n+19,n+20,n+22,n+23,n+ 1,n+ 2,n+10,n+11, n+19,n+20,n+22,n+23,n+ 1,n+ 2,n+10,n+11 );
      hex( m+10, n+29,n+30,n+32,n+33,n+15,n+16,n+ 6,n+ 7, n+29,n+30,n+32,n+33,n+15,n+16,n+ 6,n+ 7 );
      hex( m+11, n+30,n+31,n+33,n+34,n+16,n+17,n+ 7,n+ 8, n+30,n+31,n+33,n+34,n+16,n+17,n+ 7,n+ 8 );

      hex( m+12, n+18,n+21,n+24,n+25,n+ 0,n+ 9,n+ 3,n+12, n+18,n+21,n+24,n+25,n+ 0,n+ 9,n+ 3,n+12 );
      hex( m+13, n+24,n+25,n+32,n+29,n+ 3,n+12,n+ 6,n+15, n+24,n+25,n+32,n+29,n+ 3,n+12,n+ 6,n+15 );
      hex( m+14, n+23,n+20,n+27,n+28,n+11,n+ 2,n+14,n+ 5, n+23,n+20,n+27,n+28,n+11,n+ 2,n+14,n+ 5 );
      hex( m+15, n+27,n+28,n+31,n+34,n+14,n+ 5,n+17,n+ 8, n+27,n+28,n+31,n+34,n+14,n+ 5,n+17,n+ 8 );

      hex( m+16, n+35,n+36,n+38,n+39,n+18,n+19,n+21,n+22, n+35,n+36,n+38,n+39,n+18,n+19,n+21,n+22 );
      hex( m+17, n+36,n+37,n+39,n+40,n+19,n+20,n+22,n+23, n+36,n+37,n+39,n+40,n+19,n+20,n+22,n+23 );
      hex( m+18, n+35,n+38,n+41,n+42,n+18,n+21,n+24,n+25, n+35,n+38,n+41,n+42,n+18,n+21,n+24,n+25 );
      hex( m+19, n+38,n+39,n+42,n+43,n+21,n+22,n+25,n+26, n+38,n+39,n+42,n+43,n+21,n+22,n+25,n+26 );
      hex( m+20, n+39,n+40,n+43,n+44,n+22,n+23,n+26,n+27, n+39,n+40,n+43,n+44,n+22,n+23,n+26,n+27 );
      hex( m+21, n+40,n+37,n+44,n+45,n+23,n+20,n+27,n+28, n+40,n+37,n+44,n+45,n+23,n+20,n+27,n+28 );
      hex( m+22, n+41,n+42,n+49,n+46,n+24,n+25,n+32,n+29, n+41,n+42,n+49,n+46,n+24,n+25,n+32,n+29 );
      hex( m+23, n+42,n+43,n+46,n+47,n+25,n+26,n+29,n+30, n+42,n+43,n+46,n+47,n+25,n+26,n+29,n+30 );
      hex( m+24, n+43,n+44,n+47,n+48,n+26,n+27,n+30,n+31, n+43,n+44,n+47,n+48,n+26,n+27,n+30,n+31 );
      hex( m+25, n+44,n+45,n+48,n+51,n+27,n+28,n+31,n+34, n+44,n+45,n+48,n+51,n+27,n+28,n+31,n+34 );
      hex( m+26, n+46,n+47,n+49,n+50,n+29,n+30,n+32,n+33, n+46,n+47,n+49,n+50,n+29,n+30,n+32,n+33 );
      hex( m+27, n+47,n+48,n+50,n+51,n+30,n+31,n+33,n+34, n+47,n+48,n+50,n+51,n+30,n+31,n+33,n+34 );

      hex( m+28, n+62,n+52,n+64,n+54,n+35,n+36,n+38,n+39, n+62,n+52,n+64,n+54,n+35,n+36,n+38,n+39 );
      hex( m+29, n+64,n+54,n+66,n+56,n+38,n+39,n+42,n+43, n+64,n+54,n+66,n+56,n+38,n+39,n+42,n+43 );
      hex( m+30, n+66,n+56,n+69,n+58,n+42,n+43,n+46,n+47, n+66,n+56,n+69,n+58,n+42,n+43,n+46,n+47 );
      hex( m+31, n+69,n+58,n+71,n+60,n+46,n+47,n+49,n+50, n+69,n+58,n+71,n+60,n+46,n+47,n+49,n+50 );

      hex( m+32, n+63,n+53,n+65,n+55,n+62,n+52,n+64,n+54, n+63,n+53,n+65,n+55,n+62,n+52,n+64,n+54 );
      hex( m+33, n+65,n+55,n+67,n+57,n+64,n+54,n+66,n+56, n+65,n+55,n+67,n+57,n+64,n+54,n+66,n+56 );
      hex( m+34, n+63,n+53,n+68,n+90,n+65,n+55,n+67,n+57, n+63,n+53,n+68,n+90,n+65,n+55,n+67,n+57 );

      hex( m+35, n+67,n+57,n+70,n+59,n+66,n+56,n+69,n+58, n+67,n+57,n+70,n+59,n+66,n+56,n+69,n+58 );
      hex( m+36, n+68,n+90,n+72,n+61,n+67,n+57,n+70,n+59, n+68,n+90,n+72,n+61,n+67,n+57,n+70,n+59 );
      hex( m+37, n+70,n+59,n+72,n+61,n+69,n+58,n+71,n+60, n+70,n+59,n+72,n+61,n+69,n+58,n+71,n+60 );

      hex( m+38, n+53,n+37,n+55,n+40,n+52,n+36,n+54,n+39, n+53,n+37,n+55,n+40,n+52,n+36,n+54,n+39 );
      hex( m+39, n+55,n+40,n+57,n+44,n+54,n+39,n+56,n+43, n+55,n+40,n+57,n+44,n+54,n+39,n+56,n+43 );
      hex( m+40, n+53,n+37,n+90,n+45,n+55,n+40,n+57,n+44, n+53,n+37,n+90,n+45,n+55,n+40,n+57,n+44 );

      hex( m+41, n+57,n+44,n+59,n+48,n+56,n+43,n+58,n+47, n+57,n+44,n+59,n+48,n+56,n+43,n+58,n+47 );
      hex( m+42, n+90,n+45,n+61,n+51,n+57,n+44,n+59,n+48, n+90,n+45,n+61,n+51,n+57,n+44,n+59,n+48 );
      hex( m+43, n+59,n+48,n+61,n+51,n+58,n+47,n+60,n+50, n+59,n+48,n+61,n+51,n+58,n+47,n+60,n+50 );

      hex( m+44, n+80,n+38,n+83,n+42,n+78,n+35,n+82,n+41, n+80,n+38,n+83,n+42,n+78,n+35,n+82,n+41 );
      hex( m+45, n+83,n+42,n+86,n+46,n+82,n+41,n+88,n+49, n+83,n+42,n+86,n+46,n+82,n+41,n+88,n+49 );

      hex( m+46, n+73,n+62,n+74,n+64,n+78,n+35,n+80,n+38, n+73,n+62,n+74,n+64,n+78,n+35,n+80,n+38 );
      hex( m+47, n+74,n+64,n+75,n+66,n+80,n+38,n+83,n+42, n+74,n+64,n+75,n+66,n+80,n+38,n+83,n+42 );
      hex( m+48, n+75,n+66,n+76,n+69,n+83,n+42,n+86,n+46, n+75,n+66,n+76,n+69,n+83,n+42,n+86,n+46 );
      hex( m+49, n+76,n+69,n+77,n+71,n+86,n+46,n+88,n+49, n+76,n+69,n+77,n+71,n+86,n+46,n+88,n+49 );

      hex( m+50, n+79,n+63,n+81,n+65,n+73,n+62,n+74,n+64, n+79,n+63,n+81,n+65,n+73,n+62,n+74,n+64 );
      hex( m+51, n+81,n+65,n+84,n+67,n+74,n+64,n+75,n+66, n+81,n+65,n+84,n+67,n+74,n+64,n+75,n+66 );
      hex( m+52, n+84,n+67,n+87,n+70,n+75,n+66,n+76,n+69, n+84,n+67,n+87,n+70,n+75,n+66,n+76,n+69 );
      hex( m+53, n+87,n+70,n+89,n+72,n+76,n+69,n+77,n+71, n+87,n+70,n+89,n+72,n+76,n+69,n+77,n+71 );

      hex( m+54, n+79,n+63,n+85,n+68,n+81,n+65,n+84,n+67, n+79,n+63,n+85,n+68,n+81,n+65,n+84,n+67 );
      hex( m+55, n+85,n+68,n+89,n+72,n+84,n+67,n+87,n+70, n+85,n+68,n+89,n+72,n+84,n+67,n+87,n+70 );

// Now form a cavity and stich new hex in

      hex( m+56, cv.v[ 0],cv.v[ 8],n+18    ,n+19    ,cv.v[ 4],cv.v[10],n+ 0    ,n+ 1    ,cv.v[ 0],cv.v[ 8],n+18    ,n+19    ,cv.v[ 4],cv.v[ 9],n+ 0    ,n+ 1     );
      hex( m+57, cv.v[ 8],cv.v[ 1],n+19    ,n+20    ,cv.v[10],cv.v[ 5],n+ 1    ,n+ 2    ,cv.v[ 8],cv.v[ 1],n+19    ,n+20    ,cv.v[10],cv.v[ 5],n+ 1    ,n+ 2     );
      hex( m+58, cv.v[ 0],n+18    ,cv.v[12],n+24    ,cv.v[ 4],n+ 0    ,cv.v[14],n+ 3    ,cv.v[ 0],n+18    ,cv.v[12],n+24    ,cv.v[ 4],n+ 0    ,cv.v[14],n+ 3     );
      hex( m+59, n+20    ,cv.v[ 1],n+28    ,cv.v[13],n+ 2    ,cv.v[ 5],n+ 5    ,cv.v[15],n+20    ,cv.v[ 1],n+28    ,cv.v[13],n+ 2    ,cv.v[ 5],n+ 5    ,cv.v[15] );
      hex( m+60, cv.v[12],n+24    ,cv.v[ 2],n+32    ,cv.v[14],n+ 3    ,cv.v[ 6],n+ 6    ,cv.v[12],n+24    ,cv.v[ 2],n+32    ,cv.v[14],n+ 3    ,cv.v[ 6],n+ 6     );
      hex( m+61, n+28    ,cv.v[13],n+34    ,cv.v[ 3],n+ 5    ,cv.v[15],n+ 8    ,cv.v[ 7],n+28    ,cv.v[13],n+34    ,cv.v[ 3],n+ 5    ,cv.v[15],n+ 8    ,cv.v[ 7] );
      hex( m+62, n+32    ,n+33    ,cv.v[ 2],cv.v[ 9],n+ 6    ,n+ 7    ,cv.v[ 6],cv.v[11],n+32    ,n+33    ,cv.v[ 2],cv.v[ 9],n+ 6    ,n+ 7    ,cv.v[ 6],cv.v[11] );
      hex( m+63, n+33    ,n+34    ,cv.v[ 9],cv.v[ 3],n+ 7    ,n+ 8    ,cv.v[11],cv.v[ 7],n+33    ,n+34    ,cv.v[ 9],cv.v[ 3],n+ 7    ,n+ 8    ,cv.v[11],cv.v[ 7] );

      hex( m+64, n+ 0    ,n+ 1    ,n+ 3    ,n+ 4    ,cv.v[ 4],cv.v[10],cv.v[14],cv.v[17],n+ 0    ,n+ 1    ,n+ 3    ,n+ 4    ,cv.v[ 4],cv.v[10],cv.v[14],cv.v[17] );
      hex( m+65, n+ 1    ,n+ 2    ,n+ 4    ,n+ 5    ,cv.v[10],cv.v[ 5],cv.v[17],cv.v[15],n+ 1    ,n+ 2    ,n+ 4    ,n+ 5    ,cv.v[10],cv.v[ 5],cv.v[17],cv.v[15] );
      hex( m+66, n+ 3    ,n+ 4    ,n+ 6    ,n+ 7    ,cv.v[14],cv.v[17],cv.v[ 6],cv.v[11],n+ 3    ,n+ 4    ,n+ 6    ,n+ 7    ,cv.v[14],cv.v[17],cv.v[ 6],cv.v[11] );
      hex( m+67, n+ 4    ,n+ 5    ,n+ 7    ,n+ 8    ,cv.v[17],cv.v[15],cv.v[11],cv.v[ 7],n+ 4    ,n+ 5    ,n+ 7    ,n+ 8    ,cv.v[17],cv.v[15],cv.v[11],cv.v[ 7] );

// new faces

      quads( cv.v, m,m+68 );

// Now attach faces to boundaries

      attach( mask[0], m+18,0, -1,-1,-1 );
      attach( mask[0], m+22,0, -1,-1,-1 );
      attach( mask[0], m+44,5, -1,-1,-1 );
      attach( mask[0], m+45,5, -1,-1,-1 );

      attach( mask[1], m+21,1, -1,-1,-1 );
      attach( mask[1], m+25,1, -1,-1,-1 );
      attach( mask[1], m+34,4, -1,-1,-1 );
      attach( mask[1], m+36,4, -1,-1,-1 );
      attach( mask[1], m+40,4, -1,-1,-1 );
      attach( mask[1], m+42,4, -1,-1,-1 );
      attach( mask[1], m+54,4, -1,-1,-1 );
      attach( mask[1], m+55,4, -1,-1,-1 );

      attach( mask[2], m+16,2, -1,-1,-1 );
      attach( mask[2], m+17,2, -1,-1,-1 );
      attach( mask[2], m+28,2, -1,-1,-1 );
      attach( mask[2], m+32,2, -1,-1,-1 );
      attach( mask[2], m+38,2, -1,-1,-1 );
      attach( mask[2], m+46,2, -1,-1,-1 );
      attach( mask[2], m+50,2, -1,-1,-1 );

      attach( mask[3], m+26,3, -1,-1,-1 );
      attach( mask[3], m+27,3, -1,-1,-1 );
      attach( mask[3], m+31,3, -1,-1,-1 );
      attach( mask[3], m+37,3, -1,-1,-1 );
      attach( mask[3], m+43,3, -1,-1,-1 );
      attach( mask[3], m+49,3, -1,-1,-1 );
      attach( mask[3], m+53,3, -1,-1,-1 );

      attach( mask[4], m+44,0, -1,-1,-1 );
      attach( mask[4], m+45,0, -1,-1,-1 );
      attach( mask[4], m+46,0, -1,-1,-1 );
      attach( mask[4], m+47,0, -1,-1,-1 );
      attach( mask[4], m+48,0, -1,-1,-1 );
      attach( mask[4], m+49,0, -1,-1,-1 );
      attach( mask[4], m+50,0, -1,-1,-1 );
      attach( mask[4], m+51,0, -1,-1,-1 );
      attach( mask[4], m+52,0, -1,-1,-1 );
      attach( mask[4], m+53,0, -1,-1,-1 );
      attach( mask[4], m+54,0, -1,-1,-1 );
      attach( mask[4], m+55,0, -1,-1,-1 );

      attach( cv.q4[0].v[0], m+56,4, cv.q4[0].v[1], cv.q4[0].v[2], cv.q4[0].v[3] );
      attach( cv.q4[0].v[0], m+58,4, cv.q4[0].v[1], cv.q4[0].v[2], cv.q4[0].v[3] );
      attach( cv.q4[1].v[0], m+57,4, cv.q4[1].v[1], cv.q4[1].v[2], cv.q4[1].v[3] );
      attach( cv.q4[1].v[0], m+59,4, cv.q4[1].v[1], cv.q4[1].v[2], cv.q4[1].v[3] );
      attach( cv.q4[2].v[0], m+60,4, cv.q4[2].v[1], cv.q4[2].v[2], cv.q4[2].v[3] );
      attach( cv.q4[2].v[0], m+62,4, cv.q4[2].v[1], cv.q4[2].v[2], cv.q4[2].v[3] );
      attach( cv.q4[3].v[0], m+61,4, cv.q4[3].v[1], cv.q4[3].v[2], cv.q4[3].v[3] );
      attach( cv.q4[3].v[0], m+63,4, cv.q4[3].v[1], cv.q4[3].v[2], cv.q4[3].v[3] );

      bz[ mask[0] ].n= true;
      bz[ mask[1] ].n= true;
      bz[ mask[2] ].n= true;
      bz[ mask[3] ].n= true;
      bz[ mask[4] ].n= true;

      i= vz.n;
      vz.n++;
      vz.resize(dinitz);

      vz[i].v= vx.n;
      vz[i].b.n= 5;
      vz[i].b.resize(-1);
      vz[i].b[0]= mask[0];
      vz[i].b[1]= mask[1];
      vz[i].b[2]= mask[2];
      vz[i].b[3]= mask[3];
      vz[i].b[4]= mask[4];

      return;
      
  }
