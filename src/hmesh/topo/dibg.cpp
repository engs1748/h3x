
#  include <hmesh/hmesh.h>

/* struct href_t
  {
      frme_t    f;
      INT_      k;
  };*/


   void hmesh_t::dig0( dir_t d0, dir_t d1, INT_ kst, REAL_3 v, INT_ *mask )
  {

      INT_            n,m,l;
      INT_            i,j;
      INT_            n0,n1,n2;
      href_t          k[16];

      vtx_t           z;
      vtx_t           s;
      vtx_t           r;

      REAL_           d;
      REAL_           f,g;

      cavty_t         cv;
      REAL_           c3=1./12.;

      g= 0.2;
      z[0]= v[0];
      z[1]= v[1];
      z[2]= v[2];
      f= l2norm(z);
      d= 1./f;
      z*= d;

      cvty( 1,1,1, d0,d1, kst, cv );

      r= 0.25*( vx[cv.v[0]]+ vx[cv.v[1]]+ vx[cv.v[2]]+ vx[cv.v[3]] );
      s= 0.25*( vx[cv.v[4]]+ vx[cv.v[5]]+ vx[cv.v[6]]+ vx[cv.v[7]] );

// initial points

      n= vx.n; vx.n+= 24; vx.resize( xdum );
               vt.n+= 24; vt.resize( pdum );

      for( i=n;i<vt.n;i++ ){ vt[i].id= i; };

      vx[n+ 0]= vx[cv.v[ 4 ]]- r; vx[n+ 0]*= 0.6667; vx[n+ 0]+= r;
      vx[n+ 1]= vx[cv.v[ 5 ]]- r; vx[n+ 1]*= 0.6667; vx[n+ 1]+= r;
                                                            
      vx[n+ 2]= vx[cv.v[ 6 ]]- r; vx[n+ 2]*= 0.6667; vx[n+ 2]+= r;
      vx[n+ 3]= vx[cv.v[ 7 ]]- r; vx[n+ 3]*= 0.6667; vx[n+ 3]+= r;
                                                            
      vx[n+ 8]= vx[cv.v[ 0 ]]- r; vx[n+ 8]*= 0.6667; vx[n+ 8]+= r;
      vx[n+ 9]= vx[cv.v[ 1 ]]- r; vx[n+ 9]*= 0.6667; vx[n+ 9]+= r;
                                                            
      vx[n+14]= vx[cv.v[ 2 ]]- r; vx[n+14]*= 0.6667; vx[n+14]+= r;
      vx[n+15]= vx[cv.v[ 3 ]]- r; vx[n+15]*= 0.6667; vx[n+15]+= r;

// complete A section

// complete C section

      vx[n+10]=  0.75*vx[n+ 8]+ c3*( vx[n+ 9]+ vx[n+14]+ vx[n+15] );
      vx[n+11]=  0.75*vx[n+ 9]+ c3*( vx[n+ 8]+ vx[n+14]+ vx[n+15] );
      vx[n+12]=  0.75*vx[n+14]+ c3*( vx[n+ 8]+ vx[n+ 9]+ vx[n+15] );
      vx[n+13]=  0.75*vx[n+15]+ c3*( vx[n+ 8]+ vx[n+ 9]+ vx[n+14] );

// B section

      s-=r;
      s*=0.66667;

      vx[n+ 4]= vx[n+10]+ s;
      vx[n+ 5]= vx[n+11]+ s;
      vx[n+ 6]= vx[n+12]+ s;
      vx[n+ 7]= vx[n+13]+ s;

// D section      

      s= g*z;

      vx[n+16]= vx[n+ 8]+ s;
      vx[n+17]= vx[n+ 9]+ s;
      vx[n+18]= vx[n+10]+ s;
      vx[n+19]= vx[n+11]+ s;
      vx[n+20]= vx[n+12]+ s;
      vx[n+21]= vx[n+13]+ s;
      vx[n+22]= vx[n+14]+ s;

// H section

// form hexahedra

      m= hx.n; hx.n+= 16; hx.resize( hdum );

      hex( m+ 0, n+ 4,n+ 5,n+ 6,n+ 7,n+ 0,n+ 1,n+ 2,n+ 3, n+ 4,n+ 5,n+ 6,n+ 7,n+ 0,n+ 1,n+ 2,n+ 3 );
      hex( m+ 1, n+ 8,n+ 9,n+10,n+11,n+ 0,n+ 1,n+ 4,n+ 5, n+ 8,n+ 9,n+10,n+11,n+ 0,n+ 1,n+ 4,n+ 5 );
      hex( m+ 2, n+ 8,n+10,n+14,n+12,n+ 0,n+ 4,n+ 2,n+ 6, n+ 8,n+10,n+14,n+12,n+ 0,n+ 4,n+ 2,n+ 6 );
      hex( m+ 3, n+10,n+11,n+12,n+13,n+ 4,n+ 5,n+ 6,n+ 7, n+10,n+11,n+12,n+13,n+ 4,n+ 5,n+ 6,n+ 7 );
      hex( m+ 4, n+11,n+ 9,n+13,n+15,n+ 5,n+ 1,n+ 7,n+ 3, n+11,n+ 9,n+13,n+15,n+ 5,n+ 1,n+ 7,n+ 3 );
      hex( m+ 5, n+12,n+13,n+14,n+15,n+ 6,n+ 7,n+ 2,n+ 3, n+12,n+13,n+14,n+15,n+ 6,n+ 7,n+ 2,n+ 3 );

      hex( m+ 6, n+16,n+17,n+18,n+19,n+ 8,n+ 9,n+10,n+11, n+16,n+17,n+18,n+19,n+ 8,n+ 9,n+10,n+11 );
      hex( m+ 7, n+16,n+18,n+22,n+20,n+ 8,n+10,n+14,n+12, n+16,n+18,n+22,n+20,n+ 8,n+10,n+14,n+12 );
      hex( m+ 8, n+18,n+19,n+20,n+21,n+10,n+11,n+12,n+13, n+18,n+19,n+20,n+21,n+10,n+11,n+12,n+13 );
      hex( m+ 9, n+19,n+17,n+21,n+23,n+11,n+ 9,n+13,n+15, n+19,n+17,n+21,n+23,n+11,n+ 9,n+13,n+15 );
      hex( m+10, n+20,n+21,n+22,n+23,n+12,n+13,n+14,n+15, n+20,n+21,n+22,n+23,n+12,n+13,n+14,n+15 );

// Now form a cavity and stich new hex in

      hex( m+11, cv.v[ 0],cv.v[ 1],n+ 8    ,n+ 9    ,cv.v[ 4],cv.v[ 5],n+ 0    ,n+ 1    ,cv.v[ 0],cv.v[ 1],n+ 8    ,n+ 9    ,cv.v[ 4],cv.v[ 5],n+ 0    ,n+ 1    );
      hex( m+12, cv.v[ 0],    n+ 8,cv.v[ 2],n+14    ,cv.v[ 4],    n+ 0,cv.v[ 2],n+ 6    ,cv.v[ 0],    n+ 8,cv.v[ 2],n+14    ,cv.v[ 4],    n+ 0,cv.v[ 2],n+ 6    );
      hex( m+13,     n+ 0,    n+ 1,    n+ 2,n+ 3    ,cv.v[ 4],cv.v[ 5],cv.v[ 6],cv.v[ 7],    n+ 0,    n+ 1,    n+ 2,n+ 3    ,cv.v[ 4],cv.v[ 5],cv.v[ 6],cv.v[ 7]);
      hex( m+14,     n+ 9,cv.v[ 1],n+ 15   ,cv.v[ 3],n+ 1    ,cv.v[ 5],n+ 3    ,cv.v[ 7],    n+ 9,cv.v[ 1],n+ 15   ,cv.v[ 3],n+ 1    ,cv.v[ 5],n+ 3    ,cv.v[ 7]);
      hex( m+15,     n+14,    n+15,cv.v[ 2],cv.v[ 3],n+ 2    ,n+ 3    ,cv.v[ 6],cv.v[ 7],    n+14,    n+15,cv.v[ 2],cv.v[ 3],n+ 2    ,n+ 3    ,cv.v[ 6],cv.v[ 7]);


// new faces

      quads( cv.v, m,m+16 );

// Now attach faces to boundaries

      attach( mask[0], m+ 8,0, -1,-1,-1 );

      attach( mask[1], m+ 7,1, -1,-1,-1 );

      attach( mask[2], m+ 6,2, -1,-1,-1 );

      attach( mask[3], m+10,3, -1,-1,-1 );

      attach( mask[4], m+ 6,0, -1,-1,-1 );
      attach( mask[4], m+ 7,0, -1,-1,-1 );
      attach( mask[4], m+ 8,0, -1,-1,-1 );
      attach( mask[4], m+ 9,0, -1,-1,-1 );
      attach( mask[4], m+10,0, -1,-1,-1 );

      attach( cv.q4[0].v[0], m+11,4, cv.q4[0].v[1], cv.q4[0].v[2], cv.q4[0].v[3] );
      attach( cv.q4[0].v[0], m+12,4, cv.q4[0].v[1], cv.q4[0].v[2], cv.q4[0].v[3] );
      attach( cv.q4[1].v[0], m+14,4, cv.q4[0].v[1], cv.q4[0].v[2], cv.q4[0].v[3] );
      attach( cv.q4[1].v[0], m+15,4, cv.q4[0].v[1], cv.q4[0].v[2], cv.q4[0].v[3] );
      attach( cv.q4[3].v[0], m+63,4, cv.q4[0].v[1], cv.q4[0].v[2], cv.q4[0].v[3] );

      return;
      
  }
      vz.n= 1; vz.resize( dinit );
      vz[0].v= vx.n;
      vz[0].b.n= 6; vz[0].b.resize( 6 );
      vz[0].b[0]= 0;
      vz[0].b[1]= 1;
      vz[0].b[2]= 2;
      vz[0].b[3]= 3;
      vz[0].b[4]= 4;
      vz[0].b[5]= 5;
