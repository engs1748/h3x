
#  include <hmesh/hmesh.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void hmesh_t::spread( INT_ i, dir_t a, VINT_ &list, vsize_t<dir_t,DLEN_H> &c )
  {

      INT_ qd[3][4]= {{2,3,4,5},{0,1,4,5},{0,1,2,3}};


      dir_t                      d;
      dir_t                      e[3];
      dir_t                      f[3];

      INT_      j,k,l,m,n, r,t,u,v;


      VINT_        stk;

      n= (stk.n)++; stk.resize(-1);
      stk[n]=   i;

      c[i]=      a;
      c[i].dum0= 1;
      
      while( stk.n > 0 )
     {
         n=stk.n-1;
         j= stk[n]; stk.n--;
         d= c[j];

         n= (list.n)++;list.resize(-1);
         list[n]= j;

         k= d.dir();
         for( l=0;l<4;l++ )
        {
            m= qd[k][l];
            
            r= hx[j].q[m].q;
            t= hx[j].q[m].o;

            hx[j].qdir( m,e );

            u= qv[r].h[!t];
            v= qv[r].q[!t];

            if( u > -1 )
           {
               if( !(c[u].dum0) )
              {
             
                   hx[u].qdir( v,f );

                   c[u]= dmap( d,e,f ); 
                   c[u].dum0= 1;

                   n= (stk.n)++; stk.resize(-1);
                   stk[n]=   u;
             
              }
           }
        }
     }
     
      return;
  }

