
#  include <hmesh/hmesh.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void       hmesh_t::edge( INT_  v, VINT_2 &list )
  {

      VINT_    mark(hx.n,-1);
      VINT_2   stk;

      INT_     b,c, o,r,s,t, m,n, i,j;

      INT_2    u,w;

      i= eg[v].h; 
      j= eg[v].k; 
      hx[i].edge( j,u );

      n= stk.append(-1);
      stk[n][0]= i;
      stk[n][1]= j;
      mark[i]= 0;

      list.n=0;

      while( stk.n > 0 )
     {
         n= stk.n-1;
         i= stk[n][0];
         j= stk[n][1];
        (stk.n)--;

         n= list.append(-1);
         list[n][0]= i;
         list[n][1]= j;

         for( m=0;m<2;m++ )
        {

            b= meq[j][m];
            c= neq[j][m];

            r= hx[i].q[b].r;
            o= hx[i].q[b].o;

            c= mqp1[o][r][c];
            if( o ){ c--; if( c<0 ){ c=3; } }

            if( hx[i].neig( qv.data(), b, s,t ) )
           {

               if( mark[s] == -1 )
              {

                  r= hx[s].q[t].r;
                  o= hx[s].q[t].o;

                  c= mqp[o][r][c];
                  if( o ){ c--; if( c<0 ){ c=3; } }

                  hx[s].edge( mqe[t][c],w );
                  assert( w[0]==u[0] && u[1]==w[1] );

                  n= stk.append(-1);
                  stk[n][0]= s;
                  stk[n][1]= mqe[t][c];

                  mark[s]= 0;

              } 
           }
        }
         
     }
      return;
  }


   void       hmesh_t::edge( INT_  v, VINT_2 &hl, VINT_2 &ql )
  {

      VINT_    mark(hx.n,-1);
      VINT_2   stk;

      INT_     b,c, o,r,s,t, m,n, l, i,j;

      INT_2    u,w;

      i= eg[v].h; 
      j= eg[v].k; 
      hx[i].edge( j,u );

      n= stk.append(-1);
      stk[n][0]= i;
      stk[n][1]= j;
      mark[i]= 0;

      hl.n=0;
      ql.n=0;

      while( stk.n > 0 )
     {
         n= stk.n-1;
         i= stk[n][0];
         j= stk[n][1];
        (stk.n)--;

         n= hl.append(-1);
         hl[n][0]= i;
         hl[n][1]= j;

         for( m=0;m<2;m++ )
        {

            b= meq[j][m];
            c= neq[j][m];

            r= hx[i].q[b].r;
            o= hx[i].q[b].o;

            if( ql.has(0, hx[i].q[b].q)==-1 )
           {
//            printf( "adding quad %d to list\n", hx[i].q[b].q );
              l= ql.append(-1); 
              ql[l][0]= hx[i].q[b].q;
              ql[l][1]= c;
           }
            

            c= mqp1[o][r][c];
            if( o ){ c--; if( c<0 ){ c=3; } }

            if( hx[i].neig( qv.data(), b, s,t ) )
           {

               if( mark[s] == -1 )
              {

                  r= hx[s].q[t].r;
                  o= hx[s].q[t].o;

                  c= mqp[o][r][c];
                  if( o ){ c--; if( c<0 ){ c=3; } }

                  hx[s].edge( mqe[t][c],w );
                  assert( w[0]==u[0] && u[1]==w[1] );

                  n= stk.append(-1);
                  stk[n][0]= s;
                  stk[n][1]= mqe[t][c];

                  mark[s]= 0;

              } 
           }
        }
         
     }
//    printf( "at exit fro edge: ql.n= %d\n", ql.n );
      return;
  }

