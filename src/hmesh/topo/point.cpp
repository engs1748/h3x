
#  include <hmesh/hmesh.h>


//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void hmesh_t::point( INT_ v, INT_ w, INT_ x,  VINT_2 &hl, VINT_2 &ql )
  {

      const INT_ pq[8][3]= { {0,4,2}, {2,4,1}, {3,4,0}, {1,4,3}, 
                       {0,2,5}, {2,1,5}, {3,0,5}, {1,3,5} };      

      const INT_ pr[8][3]= { {2,0,3}, {2,1,3}, {2,3,3}, {2,2,3}, 
                       {1,0,3}, {1,0,2}, {1,0,0}, {1,0,1} };      

      INT_4  buf,xbuf;

      INT_   a,b,c,d,e,f,g,i,j,k,l,n,r;
      INT_   lim;

      ql.n=0;
      hl.n=0;
      lim= 0;

      assert( w > -1 );
      assert( x > -1 );

      assert( hx[w].v[x] == v );

      n= hl.append(-1);
      hl[n][0]= w;
      hl[n][1]= x;

/*    for( i=0;i<8;i++ )
     {
         printf( "\n" );
         printf( "faces connected to vertex %d\n", i );
         for( j=0;j<3;j++ )
        {
            printf( "face  %d: %d\n", pq[i][j], mhq[pq[i][j]][pr[i][j]] );
            assert( i == mhq[ pq[i][j] ][ pr[i][j] ] );
        }
     }
      exit(0);*/

      while( lim < hl.n )
     {

         
         a= hl[lim][0];
         b= hl[lim][1];
         lim++;

         assert( v == hx[a].v[b] );

         for( i=0;i<3;i++ ) 
        {

            j= pq[b][i];
            k= pr[b][i];

            c= hx[a].q[j].q;
            d= hx[a].q[j].o;
            e= hx[a].q[j].r;

            assert( c > -1 );

            if( ql.has(0,c) == -1 )
           { 

               hx[a].pack( j,buf,xbuf );
             
               f= mqp1[d][e][k];
               assert( buf[f]== v );

               l= ql.append(-1);
               ql[l][0]= c; 
               ql[l][1]= f; 
          
               d= !d;
          
               g= qv[c].h[d];
               j= qv[c].q[d];
             
               if( g > -1 )
              {
                  r= hx[g].q[j].r;
             
                  assert( hx[g].q[j].q == c );
                  assert( hx[g].q[j].o == d );
             
                  k= mqp[d][r][f];
                  k= mhq[j][k];
             
                  assert( hx[g].v[k] == v );
             
                  if( hl.has(0,g) == -1 ) 
                 {
             
                     n= hl.append(-1);
                     hl[n][0]= g;
                     hl[n][1]= k;
             
                 }
              }
           }
        }
     }

      return;

  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void hmesh_t::point( INT_ v, VINT_2 &hl, VINT_2 &ql )
  {
      INT_   a,b;
      a= vt[v].h;
      b= vt[v].k;

      ql.n=0;
      hl.n=0;
      if( a == -1 ){ return; };

      assert( hx[a].v[b]==v );
      point( v,a,b,hl,ql );


      return;

  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void hmesh_t::point( INT_ v, INT_ w, INT_ x,  VINT_2 &hl, VINT_2 &ql, VINT_2 &bl )
  {

      const INT_ pq[8][3]= { {0,4,2}, {2,4,1}, {3,4,0}, {1,4,3}, 
                       {0,2,5}, {2,1,5}, {3,0,5}, {1,3,5} };      

      const INT_ pr[8][3]= { {2,0,3}, {2,1,3}, {2,3,3}, {2,2,3}, 
                       {1,0,3}, {1,0,2}, {1,0,0}, {1,0,1} };      

      INT_4  buf,xbuf;

      INT_   a,b,c,d,e,f,g,i,j,k,l,n,r;
      INT_   lim;

      ql.n=0;
      hl.n=0;
      bl.n=0;
      lim= 0;

      assert( w > -1 );
      assert( x > -1 );

      assert( hx[w].v[x] == v );

      n= hl.append(-1);
      hl[n][0]= w;
      hl[n][1]= x;

/*    for( i=0;i<8;i++ )
     {
         printf( "\n" );
         printf( "faces connected to vertex %d\n", i );
         for( j=0;j<3;j++ )
        {
            printf( "face  %d: %d\n", pq[i][j], mhq[pq[i][j]][pr[i][j]] );
            assert( i == mhq[ pq[i][j] ][ pr[i][j] ] );
        }
     }
      exit(0);*/

      while( lim < hl.n )
     {

         
         a= hl[lim][0];
         b= hl[lim][1];
         lim++;

         assert( v == hx[a].v[b] );

         for( i=0;i<3;i++ ) 
        {

            j= pq[b][i];
            k= pr[b][i];

            c= hx[a].q[j].q;
            d= hx[a].q[j].o;
            e= hx[a].q[j].r;

            assert( c > -1 );

            if( ql.has(0,c) == -1 )
           { 

               hx[a].pack( j,buf,xbuf );
               g= hx[a].q[j].g;
               if( g > -1 )
              {
                  if( bl.has(0,g) == -1 )
                 {
                     l= bl.append(-1);
                     bl[l][0]= g;
                     bl[l][1]=-1;
                 }
              }
             
               f= mqp1[d][e][k];
               assert( buf[f]== v );

               l= ql.append(-1);
               ql[l][0]= c; 
               ql[l][1]= f; 
          
               d= !d;
          
               g= qv[c].h[d];
               j= qv[c].q[d];
             
               if( g > -1 )
              {
                  r= hx[g].q[j].r;
             
                  assert( hx[g].q[j].q == c );
                  assert( hx[g].q[j].o == d );
             
                  k= mqp[d][r][f];
                  k= mhq[j][k];
             
                  assert( hx[g].v[k] == v );
             
                  if( hl.has(0,g) == -1 ) 
                 {
             
                     n= hl.append(-1);
                     hl[n][0]= g;
                     hl[n][1]= k;
             
                 }
              }
           }
        }
     }

      return;

  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void hmesh_t::point( INT_ v, VINT_2 &hl, VINT_2 &ql, VINT_2 &bl )
  {
      INT_   a,b;
      a= vt[v].h;
      b= vt[v].k;

      ql.n=0;
      hl.n=0;
      bl.n=0;
      if( a == -1 ){ return; };

      assert( hx[a].v[b]==v );
      point( v,a,b,hl,ql,bl );


      return;

  }
