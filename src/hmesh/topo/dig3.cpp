#  include <hmesh/hmesh.h>

   void hmesh_t::dig3( dir_t d0, dir_t d1, INT_ kst, REAL_3 v, INT_ *mask )
  {

      INT_            n,m;
      INT_            i;

      vtx_t           w(v);
      vtx_t           z;
      vtx_t           s;
      vtx_t           r;
      vtx_t           c;

      REAL_           d;
      REAL_           f,g;
      const REAL_     i12= 0.25/3.;
      const REAL_     i13= 1./3.;
      const REAL_     i23= 2./3.;

      cavty_t         cv;

      g= 0.2;
      z[0]= v[0];
      z[1]= v[1];
      z[2]= v[2];
      f= l2norm(z);
      d= 1./f;
      z*= d;

      cvty( 3,1,1, d0,d1, kst, cv, true );

      r= 0.25*( vx[cv.v[0]]+ vx[cv.v[1]]+ vx[cv.v[2]]+ vx[cv.v[3]] );

// initial points

      n= vx.n; vx.n+= 80; vx.resize( xdum );
               vt.n+= 80; vt.resize( pdum );

      for( i=n;i<vt.n;i++ ){ vt[i].id= i; };

      vx[n+ 0]= vx[cv.v[4]]- r; vx[n+ 0]*= 0.8; vx[n+ 0]+= r;
      vx[n+ 3]= vx[cv.v[5]]- r; vx[n+ 3]*= 0.8; vx[n+ 3]+= r;
                         
      vx[n+ 4]= vx[cv.v[6]]- r; vx[n+ 4]*= 0.8; vx[n+ 4]+= r;
      vx[n+ 7]= vx[cv.v[7]]- r; vx[n+ 7]*= 0.8; vx[n+ 7]+= r;
        
      vx[n+16]= vx[cv.v[0]]- r; vx[n+16]*= 0.8; vx[n+16]+= r;
      vx[n+19]= vx[cv.v[1]]- r; vx[n+19]*= 0.8; vx[n+19]+= r;
                        
      vx[n+28]= vx[cv.v[2]]- r; vx[n+28]*= 0.8; vx[n+28]+= r;
      vx[n+31]= vx[cv.v[3]]- r; vx[n+31]*= 0.8; vx[n+31]+= r;
      

// complete A section

      vx[n+ 1]=   i23*vx[n+ 0]+ i13*vx[n+ 3];
      vx[n+ 2]=   i13*vx[n+ 0]+ i23*vx[n+ 3];
      vx[n+ 5]=   i23*vx[n+ 4]+ i13*vx[n+ 7];
      vx[n+ 6]=   i13*vx[n+ 4]+ i23*vx[n+ 7];

// complete C section

      vx[n+17]=   i23*vx[n+16]+ i13*vx[n+19];
      vx[n+18]=   i13*vx[n+16]+ i23*vx[n+19];

      vx[n+29]=   i23*vx[n+28]+ i13*vx[n+31];
      vx[n+30]=   i13*vx[n+28]+ i23*vx[n+31];

      vx[n+21]=   i23*vx[n+17]+ i13*vx[n+29];
      vx[n+25]=   i13*vx[n+17]+ i23*vx[n+29];

      vx[n+22]=   i23*vx[n+18]+ i13*vx[n+30];
      vx[n+26]=   i13*vx[n+18]+ i23*vx[n+30];
      
      vx[n+20]=  0.75*vx[n+16]+ i12*( vx[n+17]+ vx[n+28]+ vx[n+29] );
      vx[n+23]=  0.75*vx[n+19]+ i12*( vx[n+18]+ vx[n+30]+ vx[n+31] );
      vx[n+24]=  0.75*vx[n+28]+ i12*( vx[n+16]+ vx[n+17]+ vx[n+29] );
      vx[n+27]=  0.75*vx[n+31]+ i12*( vx[n+18]+ vx[n+19]+ vx[n+30] );

// B section
      vtx_t u= 0.25*( vx[n+ 0]+ vx[n+ 3]+ vx[n+ 4]+ vx[n+ 7] );
      u-= r;

      u*= 0.5;

      vx[n+ 8]=  vx[n+20]+ u;
      vx[n+ 9]=  vx[n+21]+ u;
      vx[n+10]=  vx[n+22]+ u;
      vx[n+11]=  vx[n+23]+ u;
      vx[n+12]=  vx[n+24]+ u;
      vx[n+13]=  vx[n+25]+ u;
      vx[n+14]=  vx[n+26]+ u;
      vx[n+15]=  vx[n+27]+ u;

// D section      

      s= vtx_t(v);
      s*= g;

      vx[n+32]= vx[n+16]+ s;
      vx[n+33]= vx[n+17]+ s;
      vx[n+34]= vx[n+18]+ s;
      vx[n+35]= vx[n+19]+ s;
      vx[n+36]= vx[n+20]+ s;
      vx[n+37]= vx[n+21]+ s;
      vx[n+38]= vx[n+22]+ s;
      vx[n+39]= vx[n+23]+ s;
      vx[n+40]= vx[n+24]+ s;
      vx[n+41]= vx[n+25]+ s;
      vx[n+42]= vx[n+26]+ s;
      vx[n+43]= vx[n+27]+ s;
      vx[n+44]= vx[n+28]+ s;
      vx[n+45]= vx[n+29]+ s;
      vx[n+46]= vx[n+30]+ s;
      vx[n+47]= vx[n+31]+ s;
                      
// E section

      c= 1.3*0.8*vtx_t(v);
      c+=  r;

      orth( c,    z,vx[n+16], vx[n+48] );
      orth( c,    z,vx[n+19], vx[n+49] );
      orth( c,    z,vx[n+20], vx[n+50] );
      orth( c,    z,vx[n+23], vx[n+51] );
      orth( c,    z,vx[n+24], vx[n+52] );
      orth( c,    z,vx[n+27], vx[n+53] );
      orth( c,    z,vx[n+28], vx[n+54] );
      orth( c,    z,vx[n+31], vx[n+55] );

// F section
      c= 1.3* vtx_t(v);
      c+=  r;

      orth( c,    z,vx[n+16], vx[n+56] );
      orth( c,    z,vx[n+19], vx[n+57] );
      orth( c,    z,vx[n+20], vx[n+58] );
      orth( c,    z,vx[n+23], vx[n+59] );
      orth( c,    z,vx[n+24], vx[n+60] );
      orth( c,    z,vx[n+27], vx[n+61] );
      orth( c,    z,vx[n+28], vx[n+62] );
      orth( c,    z,vx[n+31], vx[n+63] );

// G section

      c= vtx_t(v);
      c*= i13;
      c+=  r;

      orth( c,    z,vx[n+18], vx[n+64] );
      orth( c,    z,vx[n+19], vx[n+65] );
      orth( c,    z,vx[n+22], vx[n+66] );
      orth( c,    z,vx[n+23], vx[n+67] );
      orth( c,    z,vx[n+26], vx[n+68] );
      orth( c,    z,vx[n+27], vx[n+69] );
      orth( c,    z,vx[n+30], vx[n+70] );
      orth( c,    z,vx[n+31], vx[n+71] );

// H section

      c= vtx_t(v);
      c*= i23;
      c+=  r;

      orth( c,    z,vx[n+17], vx[n+72] );
      orth( c,    z,vx[n+19], vx[n+73] );
      orth( c,    z,vx[n+21], vx[n+74] );
      orth( c,    z,vx[n+23], vx[n+75] );
      orth( c,    z,vx[n+25], vx[n+76] );
      orth( c,    z,vx[n+27], vx[n+77] );
      orth( c,    z,vx[n+29], vx[n+78] );
      orth( c,    z,vx[n+31], vx[n+79] );

// form hexahedra

      m= hx.n; hx.n+= 60; hx.resize( hdum );

      hex( m+ 0, n+ 8,n+ 9,n+12,n+13,n+ 0,n+ 1,n+ 4,n+ 5, n+ 8,n+ 9,n+12,n+13,n+ 0,n+ 1,n+ 4,n+ 5 );
      hex( m+ 1, n+ 9,n+10,n+13,n+14,n+ 1,n+ 2,n+ 5,n+ 6, n+ 9,n+10,n+13,n+14,n+ 1,n+ 2,n+ 5,n+ 6 );
      hex( m+ 2, n+10,n+11,n+14,n+15,n+ 2,n+ 3,n+ 6,n+ 7, n+10,n+11,n+14,n+15,n+ 2,n+ 3,n+ 6,n+ 7 );
      hex( m+ 3, n+16,n+17,n+20,n+21,n+ 0,n+ 1,n+ 8,n+ 9, n+16,n+17,n+20,n+21,n+ 0,n+ 1,n+ 8,n+ 9 );
      hex( m+ 4, n+17,n+18,n+21,n+22,n+ 1,n+ 2,n+ 9,n+10, n+17,n+18,n+21,n+22,n+ 1,n+ 2,n+ 9,n+10 );
      hex( m+ 5, n+18,n+19,n+22,n+23,n+ 2,n+ 3,n+10,n+11, n+18,n+19,n+22,n+23,n+ 2,n+ 3,n+10,n+11 );
      hex( m+ 6, n+16,n+20,n+28,n+24,n+ 0,n+ 8,n+ 4,n+12, n+16,n+20,n+28,n+24,n+ 0,n+ 8,n+ 4,n+12 );
      hex( m+ 7, n+23,n+19,n+27,n+31,n+11,n+ 3,n+15,n+ 7, n+23,n+19,n+27,n+31,n+11,n+ 3,n+15,n+ 7 );
      hex( m+ 8, n+24,n+25,n+28,n+29,n+12,n+13,n+ 4,n+ 5, n+24,n+25,n+28,n+29,n+12,n+13,n+ 4,n+ 5 );
      hex( m+ 9, n+25,n+26,n+29,n+30,n+13,n+14,n+ 5,n+ 6, n+25,n+26,n+29,n+30,n+13,n+14,n+ 5,n+ 6 );
      hex( m+10, n+26,n+27,n+30,n+31,n+14,n+15,n+ 6,n+ 7, n+26,n+27,n+30,n+31,n+14,n+15,n+ 6,n+ 7 );
      hex( m+11, n+20,n+21,n+24,n+25,n+ 8,n+ 9,n+12,n+13, n+20,n+21,n+24,n+25,n+ 8,n+ 9,n+12,n+13 );
      hex( m+12, n+21,n+22,n+25,n+26,n+ 9,n+10,n+13,n+14, n+21,n+22,n+25,n+26,n+ 9,n+10,n+13,n+14 );
      hex( m+13, n+22,n+23,n+26,n+27,n+10,n+11,n+14,n+15, n+22,n+23,n+26,n+27,n+10,n+11,n+14,n+15 );
      hex( m+14, n+32,n+33,n+36,n+37,n+16,n+17,n+20,n+21, n+32,n+33,n+36,n+37,n+16,n+17,n+20,n+21 );
      hex( m+15, n+33,n+34,n+37,n+38,n+17,n+18,n+21,n+22, n+33,n+34,n+37,n+38,n+17,n+18,n+21,n+22 );
      hex( m+16, n+34,n+35,n+38,n+39,n+18,n+19,n+22,n+23, n+34,n+35,n+38,n+39,n+18,n+19,n+22,n+23 );
      hex( m+17, n+32,n+36,n+44,n+40,n+16,n+20,n+28,n+24, n+32,n+36,n+44,n+40,n+16,n+20,n+28,n+24 );
      hex( m+18, n+36,n+37,n+40,n+41,n+20,n+21,n+24,n+25, n+36,n+37,n+40,n+41,n+20,n+21,n+24,n+25 );
      hex( m+19, n+37,n+38,n+41,n+42,n+21,n+22,n+25,n+26, n+37,n+38,n+41,n+42,n+21,n+22,n+25,n+26 );
      hex( m+20, n+38,n+39,n+42,n+43,n+22,n+23,n+26,n+27, n+38,n+39,n+42,n+43,n+22,n+23,n+26,n+27 );
      hex( m+21, n+39,n+35,n+43,n+47,n+23,n+19,n+27,n+31, n+39,n+35,n+43,n+47,n+23,n+19,n+27,n+31 );
      hex( m+22, n+40,n+41,n+44,n+45,n+24,n+25,n+28,n+29, n+40,n+41,n+44,n+45,n+24,n+25,n+28,n+29 );
      hex( m+23, n+41,n+42,n+45,n+46,n+25,n+26,n+29,n+30, n+41,n+42,n+45,n+46,n+25,n+26,n+29,n+30 );
      hex( m+24, n+42,n+43,n+46,n+47,n+26,n+27,n+30,n+31, n+42,n+43,n+46,n+47,n+26,n+27,n+30,n+31 );
      hex( m+25, n+56,n+57,n+58,n+59,n+48,n+49,n+50,n+51, n+56,n+57,n+58,n+59,n+48,n+49,n+50,n+51 );
      hex( m+26, n+56,n+58,n+62,n+60,n+48,n+50,n+54,n+52, n+56,n+58,n+62,n+60,n+48,n+50,n+54,n+52 );
      hex( m+27, n+58,n+59,n+60,n+61,n+50,n+51,n+52,n+53, n+58,n+59,n+60,n+61,n+50,n+51,n+52,n+53 );
      hex( m+28, n+59,n+57,n+61,n+63,n+51,n+49,n+53,n+55, n+59,n+57,n+61,n+63,n+51,n+49,n+53,n+55 );
      hex( m+29, n+60,n+61,n+62,n+63,n+52,n+53,n+54,n+55, n+60,n+61,n+62,n+63,n+52,n+53,n+54,n+55 );
      hex( m+30, n+48,n+50,n+54,n+52,n+32,n+36,n+44,n+40, n+48,n+50,n+54,n+52,n+32,n+36,n+44,n+40 );
      hex( m+31, n+50,n+74,n+52,n+76,n+36,n+37,n+40,n+41, n+50,n+74,n+52,n+76,n+36,n+37,n+40,n+41 );
      hex( m+32, n+74,n+66,n+76,n+68,n+37,n+38,n+41,n+42, n+74,n+66,n+76,n+68,n+37,n+38,n+41,n+42 );
      hex( m+33, n+66,n+67,n+68,n+69,n+38,n+39,n+42,n+43, n+66,n+67,n+68,n+69,n+38,n+39,n+42,n+43 );
      hex( m+34, n+67,n+65,n+69,n+71,n+39,n+35,n+43,n+47, n+67,n+65,n+69,n+71,n+39,n+35,n+43,n+47 );
      hex( m+35, n+48,n+72,n+50,n+74,n+32,n+33,n+36,n+37, n+48,n+72,n+50,n+74,n+32,n+33,n+36,n+37 );
      hex( m+36, n+72,n+64,n+74,n+66,n+33,n+34,n+37,n+38, n+72,n+64,n+74,n+66,n+33,n+34,n+37,n+38 );
      hex( m+37, n+64,n+65,n+66,n+67,n+34,n+35,n+38,n+39, n+64,n+65,n+66,n+67,n+34,n+35,n+38,n+39 );
      hex( m+38, n+52,n+76,n+54,n+78,n+40,n+41,n+44,n+45, n+52,n+76,n+54,n+78,n+40,n+41,n+44,n+45 );
      hex( m+39, n+76,n+68,n+78,n+70,n+41,n+42,n+45,n+46, n+76,n+68,n+78,n+70,n+41,n+42,n+45,n+46 );
      hex( m+40, n+68,n+69,n+70,n+71,n+42,n+43,n+46,n+47, n+68,n+69,n+70,n+71,n+42,n+43,n+46,n+47 );
      hex( m+41, n+48,n+49,n+50,n+51,n+72,n+73,n+74,n+75, n+48,n+49,n+50,n+51,n+72,n+73,n+74,n+75 );
      hex( m+42, n+50,n+51,n+52,n+53,n+74,n+75,n+76,n+77, n+50,n+51,n+52,n+53,n+74,n+75,n+76,n+77 );
      hex( m+43, n+51,n+49,n+53,n+55,n+75,n+73,n+77,n+79, n+51,n+49,n+53,n+55,n+75,n+73,n+77,n+79 );
      hex( m+44, n+52,n+53,n+54,n+55,n+76,n+77,n+78,n+79, n+52,n+53,n+54,n+55,n+76,n+77,n+78,n+79 );
      hex( m+45, n+72,n+73,n+74,n+75,n+64,n+65,n+66,n+67, n+72,n+73,n+74,n+75,n+64,n+65,n+66,n+67 );
      hex( m+46, n+74,n+75,n+76,n+77,n+66,n+67,n+68,n+69, n+74,n+75,n+76,n+77,n+66,n+67,n+68,n+69 );
      hex( m+47, n+76,n+77,n+78,n+79,n+68,n+69,n+70,n+71, n+76,n+77,n+78,n+79,n+68,n+69,n+70,n+71 );
      hex( m+48, n+75,n+73,n+77,n+79,n+67,n+65,n+69,n+71, n+75,n+73,n+77,n+79,n+67,n+65,n+69,n+71 );


// connecting hexahedra

      hex( m+49, cv.v[ 0],cv.v[ 8],n+16    ,n+17    ,cv.v[ 4],cv.v[10],n+ 0    ,n+ 1    , cv.v[ 0],cv.v[ 8],n+16    ,n+17    ,cv.v[ 4],cv.v[10],n+ 0    ,n+ 1     );
      hex( m+50, cv.v[ 8],cv.v[12],n+17    ,n+18    ,cv.v[10],cv.v[14],n+ 1    ,n+ 2    , cv.v[ 8],cv.v[12],n+17    ,n+18    ,cv.v[10],cv.v[14],n+ 1    ,n+ 2     );
      hex( m+51, cv.v[12],cv.v[ 1],n+18    ,n+19    ,cv.v[14],cv.v[ 5],n+ 2    ,n+ 3    , cv.v[12],cv.v[ 1],n+18    ,n+19    ,cv.v[14],cv.v[ 5],n+ 2    ,n+ 3     );
                                                                                                                                                                
      hex( m+52, cv.v[ 0],n+16    ,cv.v[ 2],n+28    ,cv.v[ 4],n+ 0    ,cv.v[ 6],n+ 4    , cv.v[ 0],n+16    ,cv.v[ 2],n+28    ,cv.v[ 4],n+ 0    ,cv.v[ 6],n+ 4     );
      hex( m+53, n+ 0    ,n+ 1    ,n+ 4    ,n+ 5    ,cv.v[ 4],cv.v[10],cv.v[ 6],cv.v[11], n+ 0    ,n+ 1    ,n+ 4    ,n+ 5    ,cv.v[ 4],cv.v[10],cv.v[ 6],cv.v[11] );
      hex( m+54, n+ 1    ,n+ 2    ,n+ 5    ,n+ 6    ,cv.v[10],cv.v[14],cv.v[11],cv.v[15], n+ 1    ,n+ 2    ,n+ 5    ,n+ 6    ,cv.v[10],cv.v[14],cv.v[11],cv.v[15] );
      hex( m+55, n+ 2    ,n+ 3    ,n+ 6    ,n+ 7    ,cv.v[14],cv.v[ 5],cv.v[15],cv.v[ 7], n+ 2    ,n+ 3    ,n+ 6    ,n+ 7    ,cv.v[14],cv.v[ 5],cv.v[15],cv.v[ 7] );
      hex( m+56, n+19    ,cv.v[1] ,n+31    ,cv.v[3] ,n+3     ,cv.v[ 5],n+ 7    ,cv.v[ 7], n+19    ,cv.v[1] ,n+31    ,cv.v[3] ,n+3     ,cv.v[ 5],n+ 7    ,cv.v[ 7] );
                                                                                                                                                                 
      hex( m+57, n+28    ,n+29    ,cv.v[ 2],cv.v[ 9],n+ 4    ,n+ 5    ,cv.v[ 6],cv.v[11], n+28    ,n+29    ,cv.v[ 2],cv.v[ 9],n+ 4    ,n+ 5    ,cv.v[ 6],cv.v[11] );
      hex( m+58, n+29    ,n+30    ,cv.v[ 9],cv.v[13],n+ 5    ,n+ 6    ,cv.v[11],cv.v[15], n+29    ,n+30    ,cv.v[ 9],cv.v[13],n+ 5    ,n+ 6    ,cv.v[11],cv.v[15] );
      hex( m+59, n+30    ,n+31    ,cv.v[13],cv.v[ 3],n+ 6    ,n+ 7    ,cv.v[15],cv.v[ 7], n+30    ,n+31    ,cv.v[13],cv.v[ 3],n+ 6    ,n+ 7    ,cv.v[15],cv.v[ 7] );

// new faces

      quads( cv.v, m,m+60 );

// Now attach faces to boundaries

      attach( mask[0], m+17,0, -1,-1,-1 );
      attach( mask[0], m+26,0, -1,-1,-1 );
      attach( mask[0], m+30,0, -1,-1,-1 );

      attach( mask[1], m+21,1, -1,-1,-1 );
      attach( mask[1], m+28,1, -1,-1,-1 );
      attach( mask[1], m+34,1, -1,-1,-1 );
      attach( mask[1], m+43,1, -1,-1,-1 );
      attach( mask[1], m+48,1, -1,-1,-1 );

      attach( mask[2], m+14,2, -1,-1,-1 );
      attach( mask[2], m+15,2, -1,-1,-1 );
      attach( mask[2], m+16,2, -1,-1,-1 );
      attach( mask[2], m+25,2, -1,-1,-1 );
      attach( mask[2], m+35,2, -1,-1,-1 );
      attach( mask[2], m+36,2, -1,-1,-1 );
      attach( mask[2], m+37,2, -1,-1,-1 );
      attach( mask[2], m+41,2, -1,-1,-1 );
      attach( mask[2], m+45,2, -1,-1,-1 );

      attach( mask[3], m+22,3, -1,-1,-1 );
      attach( mask[3], m+23,3, -1,-1,-1 );
      attach( mask[3], m+24,3, -1,-1,-1 );
      attach( mask[3], m+29,3, -1,-1,-1 );
      attach( mask[3], m+38,3, -1,-1,-1 );
      attach( mask[3], m+39,3, -1,-1,-1 );
      attach( mask[3], m+40,3, -1,-1,-1 );
      attach( mask[3], m+44,3, -1,-1,-1 );
      attach( mask[3], m+47,3, -1,-1,-1 );

      attach( mask[4], m+25,4, -1,-1,-1 );
      attach( mask[4], m+26,4, -1,-1,-1 );
      attach( mask[4], m+27,4, -1,-1,-1 );
      attach( mask[4], m+28,4, -1,-1,-1 );
      attach( mask[4], m+29,4, -1,-1,-1 );

      attach( cv.q4[0].v[0], m+49,4, cv.q4[0].v[1], cv.q4[0].v[2], cv.q4[0].v[3] );
      attach( cv.q4[0].v[0], m+52,4, cv.q4[0].v[1], cv.q4[0].v[2], cv.q4[0].v[3] );
      attach( cv.q4[0].v[0], m+57,4, cv.q4[0].v[1], cv.q4[0].v[2], cv.q4[0].v[3] );

      attach( cv.q4[1].v[0], m+50,4, cv.q4[1].v[1], cv.q4[1].v[2], cv.q4[1].v[3] );
      attach( cv.q4[1].v[0], m+58,4, cv.q4[1].v[1], cv.q4[1].v[2], cv.q4[1].v[3] );

      attach( cv.q4[2].v[0], m+51,4, cv.q4[2].v[1], cv.q4[2].v[2], cv.q4[2].v[3] );
      attach( cv.q4[2].v[0], m+56,4, cv.q4[2].v[1], cv.q4[2].v[2], cv.q4[2].v[3] );
      attach( cv.q4[2].v[0], m+59,4, cv.q4[2].v[1], cv.q4[2].v[2], cv.q4[2].v[3] );


      bz[ mask[0] ].n= true;
      bz[ mask[1] ].n= true;
      bz[ mask[2] ].n= true;
      bz[ mask[3] ].n= true;
      bz[ mask[4] ].n= true;

      return;
      
  }
