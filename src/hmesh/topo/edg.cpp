#  include <hmesh/hmesh.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void edg_t::pts( hex_t *hx, INT_2 w )
  {
       INT_       a,b;
       bool      o;

       assert( h > -1 );
       assert( k > -1 );

       o= hx[h].e[k].o;

       a= mhe[k][0];
       b= mhe[k][1];
       if( o ){ swap(a,b); };

       w[0]= hx[h].v[a];
       w[1]= hx[h].v[b];

  }
