
#  include <hmesh/hmesh.h>

   void hmesh_t::lref( dir_t d0, dir_t d1, INT_ kst, INT_ l, INT_ m )
  {

      INT_            i,j,n,q,p,r;
      INT_            a,b,c,d, e,f,g,h;

      REAL_           c13= 1./3.; 
      REAL_           c23= 2./3.;

      cavty_t         cv;
      

      cvty( l,m,1, d0,d1, kst, cv, true );

      p= vx.n; vx.n+= 4*(l+1)*m; vx.resize( xdum );
               vt.n+= 4*(l+1)*m; vt.resize( pdum );
      for( i=p;i<vt.n;i++ ){ vt[i].id= i; };

      q= p;
      for( j=0;j<m;j++ )
     {
         a= cv.vtx4(   0,  j );
         b= cv.vtx4(   0,1+j );
         c= cv.vtx5(   0,  j );
         d= cv.vtx5(   0,1+j );

         e= cv.vtx4(   1,  j );
         f= cv.vtx4(   1,1+j );
         g= cv.vtx5(   1,  j );
         h= cv.vtx5(   1,1+j );

         vx[q++]= 0.5*( c23*vx[a]+ c13*vx[b] )+ 0.5*( c23*vx[e]+ c13*vx[f] ); 
         vx[q++]= 0.5*( c13*vx[a]+ c23*vx[b] )+ 0.5*( c13*vx[e]+ c23*vx[f] );
                                                                   
         vx[q++]= 0.25*( c23*vx[c]+ c13*vx[d] )+ 0.25*( c23*vx[g]+ c13*vx[h] )+ 
                  0.25*( c23*vx[a]+ c13*vx[b] )+ 0.25*( c23*vx[e]+ c13*vx[f] ); 
         vx[q++]= 0.25*( c13*vx[c]+ c23*vx[d] )+ 0.25*( c13*vx[g]+ c23*vx[h] )+ 
                  0.25*( c13*vx[a]+ c23*vx[b] )+ 0.25*( c13*vx[e]+ c23*vx[f] );

         for( i=1;i<l;i++ )
        {
            a= cv.vtx4( i,  j );
            b= cv.vtx4( i,1+j );
            c= cv.vtx5( i,  j );
            d= cv.vtx5( i,1+j );

            vx[q++]= c23*vx[a]+ c13*vx[b]; 
            vx[q++]= c13*vx[a]+ c23*vx[b];
            vx[q++]= 0.5*( c23*vx[c]+ c13*vx[d] )+ 0.5*( c23*vx[a]+ c13*vx[b] ); 
            vx[q++]= 0.5*( c13*vx[c]+ c23*vx[d] )+ 0.5*( c13*vx[a]+ c23*vx[b] );
        }

         a= cv.vtx4( l-1,  j );
         b= cv.vtx4( l-1,1+j );
         c= cv.vtx5( l-1,  j );
         d= cv.vtx5( l-1,1+j );

         e= cv.vtx4(   l,  j );
         f= cv.vtx4(   l,1+j );
         g= cv.vtx5(   l,  j );
         h= cv.vtx5(   l,1+j );

         vx[q++]= 0.5*( c23*vx[a]+ c13*vx[b] )+ 0.5*( c23*vx[e]+ c13*vx[f] ); 
         vx[q++]= 0.5*( c13*vx[a]+ c23*vx[b] )+ 0.5*( c13*vx[e]+ c23*vx[f] );

         vx[q++]= 0.25*( c23*vx[c]+ c13*vx[d] )+ 0.25*( c23*vx[g]+ c13*vx[h] )+ 
                  0.25*( c23*vx[a]+ c13*vx[b] )+ 0.25*( c23*vx[e]+ c13*vx[f] ); 
         vx[q++]= 0.25*( c13*vx[c]+ c23*vx[d] )+ 0.25*( c13*vx[g]+ c23*vx[h] )+ 
                  0.25*( c13*vx[a]+ c23*vx[b] )+ 0.25*( c13*vx[e]+ c23*vx[f] );

     }
   
      n= hx.n; hx.n+= 4*l*m+ 2*m; 
      hx.resize( hdum );

      q= p;
      r= n;
      for( j=0;j<m;j++ )
     {
         a= cv.vtx4(   0,  j );
         b= cv.vtx4(   0,1+j );
         c= cv.vtx5(   0,  j );
         d= cv.vtx5(   0,1+j );

         hex( r++, a,  q+0,  b,q+1,  c,  q+2,  d,q+3,  a,  q+0,  b,q+1,  c,  q+2,  d,q+3 );

         for( i=0;i<l;i++ )
        {
            a= cv.vtx4(   i,  j );
            b= cv.vtx4(   i,1+j );
            c= cv.vtx5(   i,  j );
            d= cv.vtx5(   i,1+j );

            e= cv.vtx4( i+1,  j );
            f= cv.vtx4( i+1,1+j );
            g= cv.vtx5( i+1,  j );
            h= cv.vtx5( i+1,1+j );

            hex( r++,   a,  e,q+0,q+4,  c,  g,q+2,q+6,   a,  e,q+0,q+4,  c,  g,q+2,q+6 );
            hex( r++, q+0,q+4,q+1,q+5,q+2,q+6,q+3,q+7, q+0,q+4,q+1,q+5,q+2,q+6,q+3,q+7 );
            hex( r++, q+1,q+5,  b,  f,q+3,q+7,  d,  h, q+1,q+5,  b,  f,q+3,q+7,  d,  h );
            hex( r++, q+2,q+6,q+3,q+7,  c,  g,  d,  h, q+2,q+6,q+3,q+7,  c,  g,  d,  h );
 

            q+= 4;
        }

         e= cv.vtx4(   l,  j );
         f= cv.vtx4(   l,1+j );
         g= cv.vtx5(   l,  j );
         h= cv.vtx5(   l,1+j );

         hex( r++, q+0,  e,q+1,  f, q+2,  g,q+3,  h, q+0,  e,q+1,  f, q+2,  g,q+3,  h );


         q+= 4;
     }

      assert( r == ( n+ 4*l*m+ 2*m ) );
      quads( cv.v, n,r );

      r= n;
      for( j=0;j<m;j++ )
     {
         i= 0;
         q= i+ l*j;
         attach( cv.q4[q].v[0], r++,4, cv.q4[q].v[1], cv.q4[q].v[2], cv.q4[q].v[3] );
         attach( cv.q4[q].v[0], r++,4, cv.q4[q].v[1], cv.q4[q].v[2], cv.q4[q].v[3] );
         attach( cv.q4[q].v[0], r++,4, cv.q4[q].v[1], cv.q4[q].v[2], cv.q4[q].v[3] );
         attach( cv.q4[q].v[0], r++,4, cv.q4[q].v[1], cv.q4[q].v[2], cv.q4[q].v[3] );
         r++;
         for( i=1;i<l-1;i++ )
        {
            q= i+ l*j;
            attach( cv.q4[q].v[0], r++,4, cv.q4[q].v[1], cv.q4[q].v[2], cv.q4[q].v[3] );
            attach( cv.q4[q].v[0], r++,4, cv.q4[q].v[1], cv.q4[q].v[2], cv.q4[q].v[3] );
            attach( cv.q4[q].v[0], r++,4, cv.q4[q].v[1], cv.q4[q].v[2], cv.q4[q].v[3] );
            r++;
        }
         i= l-1;
         q= i+ l*j;
         attach( cv.q4[q].v[0], r++,4, cv.q4[q].v[1], cv.q4[q].v[2], cv.q4[q].v[3] );
         attach( cv.q4[q].v[0], r++,4, cv.q4[q].v[1], cv.q4[q].v[2], cv.q4[q].v[3] );
         attach( cv.q4[q].v[0], r++,4, cv.q4[q].v[1], cv.q4[q].v[2], cv.q4[q].v[3] );
         r++;
         attach( cv.q4[q].v[0], r++,4, cv.q4[q].v[1], cv.q4[q].v[2], cv.q4[q].v[3] );
     }

      return;
      
  }
