#  include <hmesh/hmesh.h>

   void hmesh_t::inter( INT_ b, vsurf_t &su )
  {

      aprj_t                  stat;
      INT_       i,h,k,m,n;
      INT_       s,p,q,r;
      VINT_      v;
      INT_4      buf,xbuf; 
      INT_4      bvf,xbvf; 

      vtx_t      x;
 
      v.n= vx.n; v.resize(-1);

      m= 0;
      n= 0;

      for( i=0;i<qv.n;i++ )
     {
         h= qv[i].h[0];
         k= qv[i].h[1];

         if( h > -1 && k > -1 )
        {

            if( ( ( hx[h].iz[0] == bi[b].z[0] ) && 
                  ( hx[h].iz[1] == bi[b].z[1] ) &&
                  ( hx[k].iz[0] == bi[b].z[2] ) && 
                  ( hx[k].iz[1] == bi[b].z[3] ) ) ||
                ( ( hx[k].iz[0] == bi[b].z[0] ) && 
                  ( hx[k].iz[1] == bi[b].z[1] ) &&
                  ( hx[h].iz[0] == bi[b].z[2] ) && 
                  ( hx[h].iz[1] == bi[b].z[3] ) ) )
           {
          
               q= qv[i].q[0];
               r= qv[i].q[1];
          
               hx[h].pack( q,buf,xbuf );
               hx[k].pack( r,bvf,xbvf );

               assert( xbuf[0]==xbvf[0] );
               assert( xbuf[1]==xbvf[1] );
               assert( xbuf[2]==xbvf[2] );
               assert( xbuf[3]==xbvf[3] );

               if( v[xbuf[0]] == -1 ){ v[xbuf[0]]= m++; };
               if( v[xbuf[1]] == -1 ){ v[xbuf[1]]= m++; };
               if( v[xbuf[2]] == -1 ){ v[xbuf[2]]= m++; };
               if( v[xbuf[3]] == -1 ){ v[xbuf[3]]= m++; };

               n++;
               
           }
        }
     }

      bi[b].bx.n= m; bi[b].bx.resize( ydum );
      bi[b].bo.n= n; bi[b].bo.resize( jbdum );

      n= 0;
      for( i=0;i<qv.n;i++ )
     {
         h= qv[i].h[0];
         k= qv[i].h[1];

         if( h > -1 && k > -1 )
        {

            if( ( ( hx[h].iz[0] == bi[b].z[0] ) && 
                  ( hx[h].iz[1] == bi[b].z[1] ) &&
                  ( hx[k].iz[0] == bi[b].z[2] ) && 
                  ( hx[k].iz[1] == bi[b].z[3] ) ) ||
                ( ( hx[k].iz[0] == bi[b].z[0] ) && 
                  ( hx[k].iz[1] == bi[b].z[1] ) &&
                  ( hx[h].iz[0] == bi[b].z[2] ) && 
                  ( hx[h].iz[1] == bi[b].z[3] ) ) )
           {
               q= qv[i].q[0];
               r= qv[i].q[1];

               bi[b].bo[n].id=   i;

               printf( "%3d %3d %3d\n", b,n,i );

               bi[b].bo[n].h[0]= h;
               bi[b].bo[n].h[1]= k;

               bi[b].bo[n].q[0]= q;
               bi[b].bo[n].q[1]= r;

               hx[h].npack( q,buf,xbuf );

               bi[b].bo[n].v[0]= v[xbuf[0]];
               bi[b].bo[n].v[1]= v[xbuf[1]];
               bi[b].bo[n].v[2]= v[xbuf[2]];
               bi[b].bo[n].v[3]= v[xbuf[3]];

               n++;

/*             if( bi[b].d > 0 )
              {
                  INT_ dum;
                  hx[h].sctrl( q, bi[b].d, dum );
                  hx[k].sctrl( r, bi[b].d, dum );
              }*/
 
               
           }
        }
     }

      s= bi[b].s;
      p= bi[b].p;

      for( i=0;i<v.n;i++ )
     {
         m= v[i];
         if( m > -1 )
        { 
            bi[b].bx[m].id= i;

            x= vx[i];
            su[s]->aprj( x, bi[b].bx[m].y, p,stat );

            x= stat.x;
            su[s]->prj( x, bi[b].bx[m].y,    stat );

            printf( " % 9.3e % 9.3e % 9.3e % 9.3e % 9.3e\n", x[0],x[1],x[2], bi[b].bx[m].y[0], bi[b].bx[m].y[1] );
        } 
     }

      return;
  }
