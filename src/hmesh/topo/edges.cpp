#  include <hmesh/hmesh.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void hmesh_t::edges()
  {
      edg_t       edum;

      VINT_        key(vt.n,-1);
      VINT_       link;

      bool        flag;

      INT_2           v;
      INT_    i,j,m,n, a,b;


      n=-1;
      m=-1;
      for( i=0;i<hx.n;i++ )
     {
         for( j=0;j<12;j++ )
        {
            hx[i].edge( j,v );

            a= v[0];
            b= v[1];

            m= key[a];
            flag= false;

            if( m == -1 )
           {
               n=     -1;
               key[a]= eg.n;
               link.n++; link.resize(-1);
           }
            else
           {
               while( m > -1 )
              {
                  eg[m].pts( hx.data(),v ); assert( a == v[0] );
                  if( b == v[1] )
                 {
                     flag= true;
                     break;
                 }
                  else
                 {
                     n= m;
                     m= link[m];
                 }
              }
           }
            if( !flag )
           {

               m= (eg.n)++; eg.resize(edum);
               eg[m].h= i;
               eg[m].k= j;
               if( n > -1 )
              { 
                  link.n++; link.resize(-1);
                  link[n]= m; 
              }
           } 
            hx[i].e[j].e= m;
        }
     }
   
  }
