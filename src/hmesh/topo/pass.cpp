

#  include <hmesh/hmesh.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void hmesh_t::pass( INT_ k0, frme_t a0, INT_ nn, INT_ u0, INT_ u1, VINT_ &list )
  {

      frme_t                  a;
      dir_t                   g;
      wsize_t<dir_t,3,DLEN_H> d;
      INT_                    i,j,h,k,n,l;
      INT_                    lim;
      INT_                    ist;
      INT_                    top,pot;
      INT_                    x,v;
      INT_                    i0,i1,i2,i3,i4,i5,i6,i7;
      INT_                    j0,j1,j2,j3,j4,j5,j6,j7;
      INT_                    v0,v1,v2,v3,v4,v5,v6,v7, w0,w1,w2,w3,w4,w5,w6,w7;
      INT_                    x0,x1,x2,x3,x4,x5,x6,x7, y0,y1,y2,y3,y4,y5,y6,y7;
      INT_                    f0,f1,f2,f3,f4,f5;
      INT_                    g0,g1,g2,g3,g4,g5;

      INT_                    e0,e1,e2,e3,e4;

      INT_                    n0,n1,n2,n3,n4;
      VINT_                   mk(hx.n,-1);
      vtx_t                   z[8];

      const REAL_             c1=1./9.,c2=2./9.,c3=4./9.;

      a0[2]= vec(a0[0],a0[1]);

      a[0]= a0[0];
      a[1]= a0[1];
      a[2]= a0[2];

      i= indx[k0];
      ist= i;
      assert( i > -1 );

      do
     {
         j=   (d.n)++;    d.resize(g);
         d[j][0]= a[0];
         d[j][1]= a[1];
         d[j][2]= a[2];

         j= (list.n)++;list.resize(-1);
         list[j]= i;

     }while( walk(i,a,mk) && list.n < nn );

      assert( list.n == nn ); // remove later
      assert( i != ist ); // no periodic wrap around allowed for pass-through

// split hex k0;

      i= indx[k0];

      i0= coor( !(d[0][0]),!(d[0][1]),!(d[0][2]) ); 
      i1= coor(  (d[0][0]),!(d[0][1]),!(d[0][2]) );
      i2= coor( !(d[0][0]), (d[0][1]),!(d[0][2]) );
      i3= coor(  (d[0][0]), (d[0][1]),!(d[0][2]) );
      i4= coor( !(d[0][0]),!(d[0][1]), (d[0][2]) );
      i5= coor(  (d[0][0]),!(d[0][1]), (d[0][2]) );
      i6= coor( !(d[0][0]), (d[0][1]), (d[0][2]) );
      i7= coor(  (d[0][0]), (d[0][1]), (d[0][2]) );

      v0= hx[i].v[i0]; v1= hx[i].v[i1]; v2= hx[i].v[i2]; v3= hx[i].v[i3]; 
      v4= hx[i].v[i4]; v5= hx[i].v[i5]; v6= hx[i].v[i6]; v7= hx[i].v[i7]; 

      x0= hx[i].x[i0]; x1= hx[i].x[i1]; x2= hx[i].x[i2]; x3= hx[i].x[i3]; 
      x4= hx[i].x[i4]; x5= hx[i].x[i5]; x6= hx[i].x[i6]; x7= hx[i].x[i7]; 

/*    g0= hx[i].face( !(d[0][0]) );
      g1= hx[i].face(   d[0][0]  );
      g2= hx[i].face( !(d[0][1]) ); 
      g3= hx[i].face(   d[0][1]  );
      g4= hx[i].face( !(d[0][2]) );
      g5= hx[i].face(   d[0][2]  );*/

      g0= face( !(d[0][0]) );
      g1= face(   d[0][0]  );
      g2= face( !(d[0][1]) ); 
      g3= face(   d[0][1]  );
      g4= face( !(d[0][2]) );
      g5= face(   d[0][2]  );

      f0= hx[i].q[g0].g; 
      f1= hx[i].q[g1].g; 
      f2= hx[i].q[g2].g; 
      f3= hx[i].q[g3].g; 
      f4= hx[i].q[g4].g; 
      f5= hx[i].q[g5].g; 

      if( u0 == 0 ) // sharp edge layout 
     {
         x= vx.n; vx.n+= 8; vx.resize( xdum );
         v= vt.n; vt.n+= 8; vt.resize( pdum );
   
         n= hx.n; hx.n+= 6; hx.resize( hdum );
   
         vx[x]= c3*vx[x0]+ c2*vx[x1]+ c2*vx[x2]+ c1*vx[x3]; y0=x++;
         vx[x]= c2*vx[x0]+ c3*vx[x1]+ c1*vx[x2]+ c2*vx[x3]; y1=x++;
         vx[x]= c2*vx[x0]+ c1*vx[x1]+ c3*vx[x2]+ c2*vx[x3]; y2=x++;
         vx[x]= c1*vx[x0]+ c2*vx[x1]+ c2*vx[x2]+ c3*vx[x3]; y3=x++;
         vx[x]= c3*vx[x4]+ c2*vx[x5]+ c2*vx[x6]+ c1*vx[x7]; y4=x++;
         vx[x]= c2*vx[x4]+ c3*vx[x5]+ c1*vx[x6]+ c2*vx[x7]; y5=x++;
         vx[x]= c2*vx[x4]+ c1*vx[x5]+ c3*vx[x6]+ c2*vx[x7]; y6=x++;
         vx[x]= c1*vx[x4]+ c2*vx[x5]+ c2*vx[x6]+ c3*vx[x7]; y7=x++;
   
         vx[y0]= 0.5*( vx[y4]+ vx[y0] );
         vx[y1]= 0.5*( vx[y5]+ vx[y1] );
         vx[y2]= 0.5*( vx[y6]+ vx[y2] );
         vx[y3]= 0.5*( vx[y7]+ vx[y3] );
    
         vt[v].id= v; w0= v++; 
         vt[v].id= v; w1= v++; 
         vt[v].id= v; w2= v++; 
         vt[v].id= v; w3= v++; 
         vt[v].id= v; w4= v++; 
         vt[v].id= v; w5= v++; 
         vt[v].id= v; w6= v++; 
         vt[v].id= v; w7= v++; 
   
         if( ::vol( vx[y0],vx[y1],vx[y2],vx[y3],vx[y4],vx[y5],vx[y6],vx[y7] ) > 0 )
        {
            hex( n+0, w0,w1,w2,w3, w4,w5,w6,w7, y0,y1,y2,y3, y4,y5,y6,y7  ); 
            hex( n+1, v0,w0,v2,w2, v4,w4,v6,w6, x0,y0,x2,y2, x4,y4,x6,y6  ); 
            hex( n+2, w1,v1,w3,v3, w5,v5,w7,v7, y1,x1,y3,x3, y5,x5,y7,x7  ); 
            hex( n+3, v0,v1,w0,w1, v4,v5,w4,w5, x0,x1,y0,y1, x4,x5,y4,y5  ); 
            hex( n+4, w2,w3,v2,v3, w6,w7,v6,v7, y2,y3,x2,x3, y6,y7,x6,x7  ); 
            hex( n+5, v0,v1,v2,v3, w0,w1,w2,w3, x0,x1,x2,x3, y0,y1,y2,y3  ); 
   
            pot=4;
            top=   5;

            e0= 5;
            e1= 5;
            e2= 5;
            e3= 5;
            e4= 5;
        }
         else
        {
            hex( n+0, w4,w5,w6,w7, w0,w1,w2,w3, y4,y5,y6,y7, y0,y1,y2,y3  ); 
            hex( n+1, v4,w4,v6,w6, v0,w0,v2,w2, x4,y4,x6,y6, x0,y0,x2,y2  );
            hex( n+2, w5,v5,w7,v7, w1,v1,w3,v3, y5,x5,y7,x7, y1,x1,y3,x3  );
            hex( n+3, v4,v5,w4,w5, v0,v1,w0,w1, x4,x5,y4,y5, x0,x1,y0,y1  );
            hex( n+4, w6,w7,v6,v7, w2,w3,v2,v3, y6,y7,x6,x7, y2,y3,x2,x3  );
            hex( n+5, w0,w1,w2,w3, v0,v1,v2,v3, y0,y1,y2,y3, x0,x1,x2,x3  );
   
            pot=5;
            top=   4;

            e0= 4;
            e1= 4;
            e2= 4;
            e3= 4;
            e4= 4;
        }
   
         force( n+0,pot, n+5,top );
         force( n+0,0,   n+1,1 );
         force( n+0,1,   n+2,0 );
         force( n+0,2,   n+3,3 );
         force( n+0,3,   n+4,2 );
   
         force( n+1,2,   n+3,0 );
         force( n+1,3,   n+4,0 );
         force( n+2,2,   n+3,1 );
         force( n+2,3,   n+4,1 );
   
         force( n+1,pot, n+5,0 );
         force( n+2,pot, n+5,1 );
         force( n+3,pot, n+5,2 );
         force( n+4,pot, n+5,3 );
   
         if( f4 > -1 ){ attach( f4,n+5,pot,-1,-1,-1 ); }else{ replace( i,g4, n+5,pot ); }
         if( f0 > -1 ){ attach( f0,n+1,0  ,-1,-1,-1 ); }else{ replace( i,g0, n+1,0   ); }
         if( f1 > -1 ){ attach( f1,n+2,1  ,-1,-1,-1 ); }else{ replace( i,g1, n+2,1   ); }
         if( f2 > -1 ){ attach( f2,n+3,2  ,-1,-1,-1 ); }else{ replace( i,g2, n+3,2   ); }
         if( f3 > -1 ){ attach( f3,n+4,3  ,-1,-1,-1 ); }else{ replace( i,g3, n+4,3   ); }

         n0= n+0;
         n1= n+1;
         n2= n+2;
         n3= n+3;
         n4= n+4;

         lim= list.n-1;

     }
      else
     {

         assert( hx[i].neig( qv.data(), g0, n1,e1 ) ); 
         assert( hx[i].neig( qv.data(), g1, n2,e2 ) ); 
         assert( hx[i].neig( qv.data(), g2, n3,e3 ) ); 
         assert( hx[i].neig( qv.data(), g3, n4,e4 ) ); 

         assert( hx[i].neig( qv.data(), g4, n0,e0 ) ); 

         z[0]= c3*vx[x0]+ c2*vx[x1]+ c2*vx[x2]+ c1*vx[x3]; 
         z[1]= c2*vx[x0]+ c3*vx[x1]+ c1*vx[x2]+ c2*vx[x3]; 
         z[2]= c2*vx[x0]+ c1*vx[x1]+ c3*vx[x2]+ c2*vx[x3]; 
         z[3]= c1*vx[x0]+ c2*vx[x1]+ c2*vx[x2]+ c3*vx[x3]; 
         z[4]= c3*vx[x4]+ c2*vx[x5]+ c2*vx[x6]+ c1*vx[x7]; 
         z[5]= c2*vx[x4]+ c3*vx[x5]+ c1*vx[x6]+ c2*vx[x7]; 
         z[6]= c2*vx[x4]+ c1*vx[x5]+ c3*vx[x6]+ c2*vx[x7]; 
         z[7]= c1*vx[x4]+ c2*vx[x5]+ c2*vx[x6]+ c3*vx[x7]; 
   
         vx[x0]= 0.5*( z[4]+ z[0] );
         vx[x1]= 0.5*( z[5]+ z[1] );
         vx[x2]= 0.5*( z[6]+ z[2] );
         vx[x3]= 0.5*( z[7]+ z[3] );

         y4= x0;
         y5= x1;
         y6= x2;
         y7= x3;

         w4= v0;
         w5= v1;
         w6= v2;
         w7= v3;

         l= hx[i].q[g0].q; 
         hx[i].q[g0].q= -1; hx[n1].q[e1].q= -1; 
         qv[l].h[0]=-1;     qv[l].h[1]= -1; 
         qel(l);

         l= hx[i].q[g1].q; 
         hx[i].q[g1].q= -1; hx[n2].q[e2].q= -1; 
         qv[l].h[0]=-1;     qv[l].h[1]= -1; 
         qel(l);

         l= hx[i].q[g2].q; 
         hx[i].q[g2].q= -1; hx[n3].q[e3].q= -1; 
         qv[l].h[0]=-1;     qv[l].h[1]= -1; 
         qel(l);

         l= hx[i].q[g3].q; 
         hx[i].q[g3].q= -1; hx[n4].q[e4].q= -1; 
         qv[l].h[0]=-1;     qv[l].h[1]= -1; 
         qel(l);

         l= hx[i].q[g4].q;  
         hx[i].q[g4].q= -1; hx[n0].q[e0].q= -1;  
         qv[l].h[0]=-1;     qv[l].h[1]= -1;  
         qel(l);

         lim= list.n-2;
     }

      for( k=1;k<lim;k++ )
     {
         i= list[k];

         i0= coor( !(d[k][0]),!(d[k][1]),!(d[k][2]) );
         i1= coor(  (d[k][0]),!(d[k][1]),!(d[k][2]) );
         i2= coor( !(d[k][0]), (d[k][1]),!(d[k][2]) );
         i3= coor(  (d[k][0]), (d[k][1]),!(d[k][2]) );
         i4= coor( !(d[k][0]),!(d[k][1]), (d[k][2]) );
         i5= coor(  (d[k][0]),!(d[k][1]), (d[k][2]) );
         i6= coor( !(d[k][0]), (d[k][1]), (d[k][2]) );
         i7= coor(  (d[k][0]), (d[k][1]), (d[k][2]) );

         v0= hx[i].v[i0]; v1= hx[i].v[i1]; v2= hx[i].v[i2]; v3= hx[i].v[i3]; 
         assert( v0== v4 );
         assert( v1== v5 );
         assert( v2== v6 );
         assert( v3== v7 );

         v4= hx[i].v[i4]; v5= hx[i].v[i5]; v6= hx[i].v[i6]; v7= hx[i].v[i7]; 
   
         x0= hx[i].x[i0]; x1= hx[i].x[i1]; x2= hx[i].x[i2]; x3= hx[i].x[i3]; 
         assert( x0== x4 );
         assert( x1== x5 );
         assert( x2== x6 );
         assert( x3== x7 );
         x4= hx[i].x[i4]; x5= hx[i].x[i5]; x6= hx[i].x[i6]; x7= hx[i].x[i7]; 
   
/*       g0= hx[i].face( !(d[k][0]) );
         g1= hx[i].face(   d[k][0]  );
         g2= hx[i].face( !(d[k][1]) ); 
         g3= hx[i].face(   d[k][1]  );
         g4= hx[i].face( !(d[k][2]) );
         g5= hx[i].face(   d[k][2]  );*/
   
         g0= face( !(d[k][0]) );
         g1= face(   d[k][0]  );
         g2= face( !(d[k][1]) ); 
         g3= face(   d[k][1]  );
         g4= face( !(d[k][2]) );
         g5= face(   d[k][2]  );

         f0= hx[i].q[g0].g; 
         f1= hx[i].q[g1].g; 
         f2= hx[i].q[g2].g; 
         f3= hx[i].q[g3].g; 
         f4= hx[i].q[g4].g; 
         f5= hx[i].q[g5].g; 
   
         x= vx.n; vx.n+= 4; vx.resize( xdum );
         v= vt.n; vt.n+= 4; vt.resize( pdum );
   
         n= hx.n; hx.n+= 5; hx.resize( hdum );
 
         y0= y4;
         y1= y5;
         y2= y6;
         y3= y7;
   
         vx[x]= c3*vx[x4]+ c2*vx[x5]+ c2*vx[x6]+ c1*vx[x7]; y4=x++;
         vx[x]= c2*vx[x4]+ c3*vx[x5]+ c1*vx[x6]+ c2*vx[x7]; y5=x++;
         vx[x]= c2*vx[x4]+ c1*vx[x5]+ c3*vx[x6]+ c2*vx[x7]; y6=x++;
         vx[x]= c1*vx[x4]+ c2*vx[x5]+ c2*vx[x6]+ c3*vx[x7]; y7=x++;

         w0= w4;
         w1= w5;
         w2= w6;
         w3= w7;
   
         vt[v].id= v; w4= v++; 
         vt[v].id= v; w5= v++; 
         vt[v].id= v; w6= v++; 
         vt[v].id= v; w7= v++; 
   
         if( ::vol( vx[y0],vx[y1],vx[y2],vx[y3],vx[y4],vx[y5],vx[y6],vx[y7] ) > 0 )
        {
            hex( n+0, w0,w1,w2,w3, w4,w5,w6,w7, y0,y1,y2,y3, y4,y5,y6,y7  ); 
            hex( n+1, v0,w0,v2,w2, v4,w4,v6,w6, x0,y0,x2,y2, x4,y4,x6,y6  );
            hex( n+2, w1,v1,w3,v3, w5,v5,w7,v7, y1,x1,y3,x3, y5,x5,y7,x7  );
            hex( n+3, v0,v1,w0,w1, v4,v5,w4,w5, x0,x1,y0,y1, x4,x5,y4,y5  );
            hex( n+4, w2,w3,v2,v3, w6,w7,v6,v7, y2,y3,x2,x3, y6,y7,x6,x7  );
   
            pot=4;
            top=   5;
        }
         else
        {
            hex( n+0, w4,w5,w6,w7, w0,w1,w2,w3, y4,y5,y6,y7, y0,y1,y2,y3  ); 
            hex( n+1, v4,w4,v6,w6, v0,w0,v2,w2, x4,y4,x6,y6, x0,y0,x2,y2  );
            hex( n+2, w5,v5,w7,v7, w1,v1,w3,v3, y5,x5,y7,x7, y1,x1,y3,x3  );
            hex( n+3, v4,v5,w4,w5, v0,v1,w0,w1, x4,x5,y4,y5, x0,x1,y0,y1  );
            hex( n+4, w6,w7,v6,v7, w2,w3,v2,v3, y6,y7,x6,x7, y2,y3,x2,x3  );

            pot=5;
            top=   4;
        }

         force( n+0,0,   n+1,1 );
         force( n+0,1,   n+2,0 );
         force( n+0,2,   n+3,3 );
         force( n+0,3,   n+4,2 );

         force( n+1,2,   n+3,0 );
         force( n+1,3,   n+4,0 );
         force( n+2,2,   n+3,1 );
         force( n+2,3,   n+4,1 );

         force( n+0,4,   n0,e0 );
         force( n+1,4,   n1,e1 );
         force( n+2,4,   n2,e2 );
         force( n+3,4,   n3,e3 );
         force( n+4,4,   n4,e4 );

         if( f0 > -1 ){ attach( f0,n+1,0,-1,-1,-1 ); }else{ replace( i,g0, n+1,0   ); }
         if( f1 > -1 ){ attach( f1,n+2,1,-1,-1,-1 ); }else{ replace( i,g1, n+2,1   ); }
         if( f2 > -1 ){ attach( f2,n+3,2,-1,-1,-1 ); }else{ replace( i,g2, n+3,2   ); }
         if( f3 > -1 ){ attach( f3,n+4,3,-1,-1,-1 ); }else{ replace( i,g3, n+4,3   ); }

         n0= n+0;
         n1= n+1;
         n2= n+2;
         n3= n+3;
         n4= n+4;

         e0= top;
         e1= top;
         e2= top;
         e3= top;
         e4= top;

     }


      if( u1 == 0 )
     {
         k= list.n-1;
         i= list[ k ];

         i0= coor( !(d[k][0]),!(d[k][1]),!(d[k][2]) ); 
         i1= coor(  (d[k][0]),!(d[k][1]),!(d[k][2]) );
         i2= coor( !(d[k][0]), (d[k][1]),!(d[k][2]) );
         i3= coor(  (d[k][0]), (d[k][1]),!(d[k][2]) );
         i4= coor( !(d[k][0]),!(d[k][1]), (d[k][2]) );
         i5= coor(  (d[k][0]),!(d[k][1]), (d[k][2]) );
         i6= coor( !(d[k][0]), (d[k][1]), (d[k][2]) );
         i7= coor(  (d[k][0]), (d[k][1]), (d[k][2]) );

         v0= hx[i].v[i0]; v1= hx[i].v[i1]; v2= hx[i].v[i2]; v3= hx[i].v[i3]; 
         assert( v0== v4 );
         assert( v1== v5 );
         assert( v2== v6 );
         assert( v3== v7 );
         v4= hx[i].v[i4]; v5= hx[i].v[i5]; v6= hx[i].v[i6]; v7= hx[i].v[i7]; 
      
         x0= hx[i].x[i0]; x1= hx[i].x[i1]; x2= hx[i].x[i2]; x3= hx[i].x[i3]; 
         assert( x0== x4 );
         assert( x1== x5 );
         assert( x2== x6 );
         assert( x3== x7 );
         x4= hx[i].x[i4]; x5= hx[i].x[i5]; x6= hx[i].x[i6]; x7= hx[i].x[i7]; 
   
/*       g0= hx[i].face( !(d[k][0]) );
         g1= hx[i].face(   d[k][0]  );
         g2= hx[i].face( !(d[k][1]) ); 
         g3= hx[i].face(   d[k][1]  );
         g4= hx[i].face( !(d[k][2]) );
         g5= hx[i].face(   d[k][2]  );*/
         g0= face( !(d[k][0]) );
         g1= face(   d[k][0]  );
         g2= face( !(d[k][1]) ); 
         g3= face(   d[k][1]  );
         g4= face( !(d[k][2]) );
         g5= face(   d[k][2]  );
   
         f0= hx[i].q[g0].g; 
         f1= hx[i].q[g1].g; 
         f2= hx[i].q[g2].g; 
         f3= hx[i].q[g3].g; 
         f4= hx[i].q[g4].g; 
         f5= hx[i].q[g5].g; 
   
         x= vx.n; vx.n+= 4; vx.resize( xdum );
         v= vt.n; vt.n+= 4; vt.resize( pdum );
   
         n= hx.n; hx.n+= 6; hx.resize( hdum );
   
         y0= y4;
         y1= y5;
         y2= y6;
         y3= y7;
   
         vx[x]= c3*vx[x4]+ c2*vx[x5]+ c2*vx[x6]+ c1*vx[x7]; y4=x++;
         vx[x]= c2*vx[x4]+ c3*vx[x5]+ c1*vx[x6]+ c2*vx[x7]; y5=x++;
         vx[x]= c2*vx[x4]+ c1*vx[x5]+ c3*vx[x6]+ c2*vx[x7]; y6=x++;
         vx[x]= c1*vx[x4]+ c2*vx[x5]+ c2*vx[x6]+ c3*vx[x7]; y7=x++;
   
         vx[y4]= 0.5*( vx[y4]+ vx[y0] );
         vx[y5]= 0.5*( vx[y5]+ vx[y1] );
         vx[y6]= 0.5*( vx[y6]+ vx[y2] );
         vx[y7]= 0.5*( vx[y7]+ vx[y3] );
   
         w0= w4;
         w1= w5;
         w2= w6;
         w3= w7;
    
         vt[v].id= v; w4= v++; 
         vt[v].id= v; w5= v++; 
         vt[v].id= v; w6= v++; 
         vt[v].id= v; w7= v++; 
   
   
         if( ::vol( vx[y0],vx[y1],vx[y2],vx[y3],vx[y4],vx[y5],vx[y6],vx[y7] ) > 0 )
        {
            hex( n+0, w0,w1,w2,w3, w4,w5,w6,w7, y0,y1,y2,y3, y4,y5,y6,y7  ); 
            hex( n+1, v0,w0,v2,w2, v4,w4,v6,w6, x0,y0,x2,y2, x4,y4,x6,y6  );
            hex( n+2, w1,v1,w3,v3, w5,v5,w7,v7, y1,x1,y3,x3, y5,x5,y7,x7  );
            hex( n+3, v0,v1,w0,w1, v4,v5,w4,w5, x0,x1,y0,y1, x4,x5,y4,y5  );
            hex( n+4, w2,w3,v2,v3, w6,w7,v6,v7, y2,y3,x2,x3, y6,y7,x6,x7  );
            hex( n+5, w4,w5,w6,w7, v4,v5,v6,v7, y4,y5,y6,y7, x4,x5,x6,x7  );
   
            pot=4;
            top=   5;
        }
         else
        {
            hex( n+0, w4,w5,w6,w7, w0,w1,w2,w3, y4,y5,y6,y7, y0,y1,y2,y3  ); 
            hex( n+1, v4,w4,v6,w6, v0,w0,v2,w2, x4,y4,x6,y6, x0,y0,x2,y2  );
            hex( n+2, w5,v5,w7,v7, w1,v1,w3,v3, y5,x5,y7,x7, y1,x1,y3,x3  );
            hex( n+3, v4,v5,w4,w5, v0,v1,w0,w1, x4,x5,y4,y5, x0,x1,y0,y1  );
            hex( n+4, w6,w7,v6,v7, w2,w3,v2,v3, y6,y7,x6,x7, y2,y3,x2,x3  );
            hex( n+5, v4,v5,v6,v7, w4,w5,w6,w7, x4,x5,x6,x7, y4,y5,y6,y7  );
   
            pot=5;
            top=   4;
        }
   
         force( n+0,top, n+5,pot );
         force( n+0,0,   n+1,1 );
         force( n+0,1,   n+2,0 );
         force( n+0,2,   n+3,3 );
         force( n+0,3,   n+4,2 );
   
         force( n+1,2,   n+3,0 );
         force( n+1,3,   n+4,0 );
         force( n+2,2,   n+3,1 );
         force( n+2,3,   n+4,1 );
   
         force( n+1,top, n+5,0 );
         force( n+2,top, n+5,1 );
         force( n+3,top, n+5,2 );
         force( n+4,top, n+5,3 );
   
         force( n+0,4,   n0,5 );
         force( n+1,4,   n1,5 );
         force( n+2,4,   n2,5 );
         force( n+3,4,   n3,5 );
         force( n+4,4,   n4,5 );
   
         if( f5 > -1 ){ attach( f5,n+5,top,-1,-1,-1 ); }else{ replace( i,g5, n+5,top ); }
         if( f0 > -1 ){ attach( f0,n+1,0  ,-1,-1,-1 ); }else{ replace( i,g0, n+1,0   ); }
         if( f1 > -1 ){ attach( f1,n+2,1  ,-1,-1,-1 ); }else{ replace( i,g1, n+2,1   ); }
         if( f2 > -1 ){ attach( f2,n+3,2  ,-1,-1,-1 ); }else{ replace( i,g2, n+3,2   ); }
         if( f3 > -1 ){ attach( f3,n+4,3  ,-1,-1,-1 ); }else{ replace( i,g3, n+4,3   ); }
     }
      else
     {
         h= list.n-2;
         k= list.n-1;
         i= list[ h ];
         j= list[ k ];

         i0= coor( !(d[h][0]),!(d[h][1]),!(d[h][2]) ); 
         i1= coor(  (d[h][0]),!(d[h][1]),!(d[h][2]) );
         i2= coor( !(d[h][0]), (d[h][1]),!(d[h][2]) );
         i3= coor(  (d[h][0]), (d[h][1]),!(d[h][2]) );
         i4= coor( !(d[h][0]),!(d[h][1]), (d[h][2]) );
         i5= coor(  (d[h][0]),!(d[h][1]), (d[h][2]) );
         i6= coor( !(d[h][0]), (d[h][1]), (d[h][2]) );
         i7= coor(  (d[h][0]), (d[h][1]), (d[h][2]) );

         j0= coor( !(d[k][0]),!(d[k][1]),!(d[k][2]) ); 
         j1= coor(  (d[k][0]),!(d[k][1]),!(d[k][2]) );
         j2= coor( !(d[k][0]), (d[k][1]),!(d[k][2]) );
         j3= coor(  (d[k][0]), (d[k][1]),!(d[k][2]) );
         j4= coor( !(d[k][0]),!(d[k][1]), (d[k][2]) );
         j5= coor(  (d[k][0]),!(d[k][1]), (d[k][2]) );
         j6= coor( !(d[k][0]), (d[k][1]), (d[k][2]) );
         j7= coor(  (d[k][0]), (d[k][1]), (d[k][2]) );

         v0= hx[i].v[i0]; v1= hx[i].v[i1]; v2= hx[i].v[i2]; v3= hx[i].v[i3]; 
         v4= hx[i].v[i4]; v5= hx[i].v[i5]; v6= hx[i].v[i6]; v7= hx[i].v[i7]; 

         assert( v4 == hx[j].v[j0] );
         assert( v5 == hx[j].v[j1] );
         assert( v6 == hx[j].v[j2] );
         assert( v7 == hx[j].v[j3] );

         w0= w4;
         w1= w5;
         w2= w6;
         w3= w7;

         w4= hx[j].v[j4]; w5= hx[j].v[j5]; w6= hx[j].v[j6]; w7= hx[j].v[j7]; 
      
         x0= hx[i].x[i0]; x1= hx[i].x[i1]; x2= hx[i].x[i2]; x3= hx[i].x[i3]; 
         x4= hx[i].x[i4]; x5= hx[i].x[i5]; x6= hx[i].x[i6]; x7= hx[i].x[i7]; 

         assert( x4 == hx[j].x[j0] );
         assert( x5 == hx[j].x[j1] );
         assert( x6 == hx[j].x[j2] );
         assert( x7 == hx[j].x[j3] );

         y0= y4;
         y1= y5;
         y2= y6;
         y3= y7;

         y4= hx[j].x[j4]; y5= hx[j].x[j5]; y6= hx[j].x[j6]; y7= hx[j].x[j7]; 
   
/*       g0= hx[i].face( !(d[h][0]) );
         g1= hx[i].face(   d[h][0]  );
         g2= hx[i].face( !(d[h][1]) ); 
         g3= hx[i].face(   d[h][1]  );
         g4= hx[i].face( !(d[h][2]) );
         g5= hx[i].face(   d[h][2]  );*/

         g0= face( !(d[h][0]) );
         g1= face(   d[h][0]  );
         g2= face( !(d[h][1]) ); 
         g3= face(   d[h][1]  );
         g4= face( !(d[h][2]) );
         g5= face(   d[h][2]  );
   
         f0= hx[i].q[g0].g; 
         f1= hx[i].q[g1].g; 
         f2= hx[i].q[g2].g; 
         f3= hx[i].q[g3].g; 
         f4= hx[i].q[g4].g; 
         f5= hx[i].q[g5].g; 

         z[0]= c3*vx[x4]+ c2*vx[x5]+ c2*vx[x6]+ c1*vx[x7]; 
         z[1]= c2*vx[x4]+ c3*vx[x5]+ c1*vx[x6]+ c2*vx[x7]; 
         z[2]= c2*vx[x4]+ c1*vx[x5]+ c3*vx[x6]+ c2*vx[x7]; 
         z[3]= c1*vx[x4]+ c2*vx[x5]+ c2*vx[x6]+ c3*vx[x7]; 
         z[4]= c3*vx[y4]+ c2*vx[y5]+ c2*vx[y6]+ c1*vx[y7]; 
         z[5]= c2*vx[y4]+ c3*vx[y5]+ c1*vx[y6]+ c2*vx[y7]; 
         z[6]= c2*vx[y4]+ c1*vx[y5]+ c3*vx[y6]+ c2*vx[y7]; 
         z[7]= c1*vx[y4]+ c2*vx[y5]+ c2*vx[y6]+ c3*vx[y7]; 
   
         vx[y4]= 0.5*( z[4]+ z[0] );
         vx[y5]= 0.5*( z[5]+ z[1] );
         vx[y6]= 0.5*( z[6]+ z[2] );
         vx[y7]= 0.5*( z[7]+ z[3] );

         n= hx.n; hx.n+= 5; hx.resize( hdum );

         if( ::vol( vx[y0],vx[y1],vx[y2],vx[y3],vx[y4],vx[y5],vx[y6],vx[y7] ) > 0 )
        {
            hex( n+0, w0,w1,w2,w3, w4,w5,w6,w7, y0,y1,y2,y3, y4,y5,y6,y7  ); 
            hex( n+1, v0,w0,v2,w2, v4,w4,v6,w6, x0,y0,x2,y2, x4,y4,x6,y6  );
            hex( n+2, w1,v1,w3,v3, w5,v5,w7,v7, y1,x1,y3,x3, y5,x5,y7,x7  );
            hex( n+3, v0,v1,w0,w1, v4,v5,w4,w5, x0,x1,y0,y1, x4,x5,y4,y5  );
            hex( n+4, w2,w3,v2,v3, w6,w7,v6,v7, y2,y3,x2,x3, y6,y7,x6,x7  );

            pot=  4;
            top=  5;
        }
         else
        {
            hex( n+0, w4,w5,w6,w7, w0,w1,w2,w3, y4,y5,y6,y7, y0,y1,y2,y3  ); 
            hex( n+1, v4,w4,v6,w6, v0,w0,v2,w2, x4,y4,x6,y6, x0,y0,x2,y2  );
            hex( n+2, w5,v5,w7,v7, w1,v1,w3,v3, y5,x5,y7,x7, y1,x1,y3,x3  );
            hex( n+3, v4,v5,w4,w5, v0,v1,w0,w1, x4,x5,y4,y5, x0,x1,y0,y1  );
            hex( n+4, w6,w7,v6,v7, w2,w3,v2,v3, y6,y7,x6,x7, y2,y3,x2,x3  );
   
            pot=  5;
            top=  4;
        }
         force( n+0,0,   n+1,1 );
         force( n+0,1,   n+2,0 );
         force( n+0,2,   n+3,3 );
         force( n+0,3,   n+4,2 );

         force( n+1,2,   n+3,0 );
         force( n+1,3,   n+4,0 );
         force( n+2,2,   n+3,1 );
         force( n+2,3,   n+4,1 );

         force( n+0,4,   n0,e0 );
         force( n+1,4,   n1,e1 );
         force( n+2,4,   n2,e2 );
         force( n+3,4,   n3,e3 );
         force( n+4,4,   n4,e4 );

         if( f0 > -1 ){ attach( f0,n+1,0,-1,-1,-1 ); }else{ replace( i,g0, n+1,0   ); }
         if( f1 > -1 ){ attach( f1,n+2,1,-1,-1,-1 ); }else{ replace( i,g1, n+2,1   ); }
         if( f2 > -1 ){ attach( f2,n+3,2,-1,-1,-1 ); }else{ replace( i,g2, n+3,2   ); }
         if( f3 > -1 ){ attach( f3,n+4,3,-1,-1,-1 ); }else{ replace( i,g3, n+4,3   ); }

/*       g0= hx[j].face( !(d[k][0]) );
         g1= hx[j].face(   d[k][0]  );
         g2= hx[j].face( !(d[k][1]) ); 
         g3= hx[j].face(   d[k][1]  );
         g4= hx[j].face( !(d[k][2]) );
         g5= hx[j].face(   d[k][2]  );*/

         g0= face( !(d[k][0]) );
         g1= face(   d[k][0]  );
         g2= face( !(d[k][1]) ); 
         g3= face(   d[k][1]  );
         g4= face( !(d[k][2]) );
         g5= face(   d[k][2]  );

         replace( j,g0, n+1,top );
         replace( j,g1, n+2,top );
         replace( j,g2, n+3,top );
         replace( j,g3, n+4,top );
         replace( j,g5, n+0,top );

     }
 
      return;
  }
