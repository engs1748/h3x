#include <hmesh/hmesh.h>

/*
create block layout which looks like an sbranch exit but 2 hexes long, not one

h0: hex id to start from
f[0]: direction of refinement
!f[1]: direction to other hex
n0: return id of small hex in h0
n1: return id of small hex in h1

      f[1]
     <---
 _____________
|\     |     /|  ||
| \____|____/ |  \/ f[0]
| | n0 | n1 | |
| | h0 |    | |
---------------
*/

   void hmesh_t::sbranch2kernel( INT_ h0, frme_t f0, INT_ *n )
  {
      INT_  h1;
      INT_  n0, n1;

      INT_  x, v;

      INT_  i0[8], v0[8], x0[8], w0[8], y0[8];
      INT_  i1[8], v1[8], x1[8], w1[8], y1[8];

      INT_  q0[6], g0[6], b0[6], s0[6], p0[6];
      INT_  q1[6], g1[6], b1[6], s1[6], p1[6];

      frme_t   f1;
      vtx_t    z[4];

      const REAL_  c1=2./3.;
      const REAL_  c2=1./3.;


      memset( q1,-1,sizeof(q1) );
      memset( g1,-1,sizeof(g1) );
      memset( b1,-1,sizeof(b1) );
      memset( s1,-1,sizeof(s1) );
      memset( p1,-1,sizeof(p1) );

      memset( q0,-1,sizeof(q0) );
      memset( g0,-1,sizeof(g0) );
      memset( b0,-1,sizeof(b0) );
      memset( s0,-1,sizeof(s0) );
      memset( p0,-1,sizeof(p0) );


      frme_walk( h0, f0, -2, h1, f1 );

/*    k0= hx[h0].id;
      k1= hx[h1].id;*/

      // ids and coordinates of vertices in h0
      i0[0] = coor(  (f0[0]), (f0[1]),!(f0[2]) );
      i0[1] = coor(  (f0[0]),!(f0[1]),!(f0[2]) );
      i0[2] = coor(  (f0[0]), (f0[1]), (f0[2]) );
      i0[3] = coor(  (f0[0]),!(f0[1]), (f0[2]) );
      i0[4] = coor( !(f0[0]), (f0[1]),!(f0[2]) );
      i0[5] = coor( !(f0[0]),!(f0[1]),!(f0[2]) );
      i0[6] = coor( !(f0[0]), (f0[1]), (f0[2]) );
      i0[7] = coor( !(f0[0]),!(f0[1]), (f0[2]) );

      i1[0] = coor(  (f1[0]), (f1[1]),!(f1[2]) );
      i1[1] = coor(  (f1[0]),!(f1[1]),!(f1[2]) );
      i1[2] = coor(  (f1[0]), (f1[1]), (f1[2]) );
      i1[3] = coor(  (f1[0]),!(f1[1]), (f1[2]) );
      i1[4] = coor( !(f1[0]), (f1[1]),!(f1[2]) );
      i1[5] = coor( !(f1[0]),!(f1[1]),!(f1[2]) );
      i1[6] = coor( !(f1[0]), (f1[1]), (f1[2]) );
      i1[7] = coor( !(f1[0]),!(f1[1]), (f1[2]) );

      // ids and coordinates of vertices in h1
      v0[0] = hx[h0].v[ i0[0] ]; v0[1] = hx[h0].v[ i0[1] ];
      v0[2] = hx[h0].v[ i0[2] ]; v0[3] = hx[h0].v[ i0[3] ]; 
      v0[4] = hx[h0].v[ i0[4] ]; v0[5] = hx[h0].v[ i0[5] ];
      v0[6] = hx[h0].v[ i0[6] ]; v0[7] = hx[h0].v[ i0[7] ]; 

      x0[0] = hx[h0].x[ i0[0] ]; x0[1] = hx[h0].x[ i0[1] ];
      x0[2] = hx[h0].x[ i0[2] ]; x0[3] = hx[h0].x[ i0[3] ]; 
      x0[4] = hx[h0].x[ i0[4] ]; x0[5] = hx[h0].x[ i0[5] ];
      x0[6] = hx[h0].x[ i0[6] ]; x0[7] = hx[h0].x[ i0[7] ]; 

      v1[0] = hx[h1].v[ i1[0] ]; v1[1] = hx[h1].v[ i1[1] ];
      v1[2] = hx[h1].v[ i1[2] ]; v1[3] = hx[h1].v[ i1[3] ]; 
      v1[4] = hx[h1].v[ i1[4] ]; v1[5] = hx[h1].v[ i1[5] ];
      v1[6] = hx[h1].v[ i1[6] ]; v1[7] = hx[h1].v[ i1[7] ]; 

      x1[0] = hx[h1].x[ i1[0] ]; x1[1] = hx[h1].x[ i1[1] ];
      x1[2] = hx[h1].x[ i1[2] ]; x1[3] = hx[h1].x[ i1[3] ]; 
      x1[4] = hx[h1].x[ i1[4] ]; x1[5] = hx[h1].x[ i1[5] ];
      x1[6] = hx[h1].x[ i1[6] ]; x1[7] = hx[h1].x[ i1[7] ]; 


      // faces and boundary information in h0
/*    q0[0] = hx[h0].face(   f0[1]  );
      q0[1] = hx[h0].face( !(f0[1]) );
      q0[2] = hx[h0].face( !(f0[2]) );
      q0[3] = hx[h0].face(   f0[2]  );
      q0[4] = hx[h0].face(   f0[0]  ); 
      q0[5] = hx[h0].face( !(f0[0]) );*/

      q0[0] = face(   f0[1]  );
      q0[1] = face( !(f0[1]) );
      q0[2] = face( !(f0[2]) );
      q0[3] = face(   f0[2]  );
      q0[4] = face(   f0[0]  ); 
      q0[5] = face( !(f0[0]) );

      g0[0] = hx[h0].q[ q0[0] ].g; 
      g0[1] = hx[h0].q[ q0[1] ].g; 
      g0[2] = hx[h0].q[ q0[2] ].g; 
      g0[3] = hx[h0].q[ q0[3] ].g; 
      g0[4] = hx[h0].q[ q0[4] ].g; 
      g0[5] = hx[h0].q[ q0[5] ].g; 

      b0[0] = hx[h0].q[ q0[0] ].b; 
      b0[1] = hx[h0].q[ q0[1] ].b; 
      b0[2] = hx[h0].q[ q0[2] ].b; 
      b0[3] = hx[h0].q[ q0[3] ].b; 
      b0[4] = hx[h0].q[ q0[4] ].b; 
      b0[5] = hx[h0].q[ q0[5] ].b; 

      if( g0[0] > -1 ){ s0[0]= bz[ g0[0] ].bo[ b0[0] ].s; p0[0]= bz[ g0[0] ].bo[ b0[0] ].p; }
      if( g0[1] > -1 ){ s0[1]= bz[ g0[1] ].bo[ b0[1] ].s; p0[1]= bz[ g0[1] ].bo[ b0[1] ].p; }
      if( g0[2] > -1 ){ s0[2]= bz[ g0[2] ].bo[ b0[2] ].s; p0[2]= bz[ g0[2] ].bo[ b0[2] ].p; }
      if( g0[3] > -1 ){ s0[3]= bz[ g0[3] ].bo[ b0[3] ].s; p0[3]= bz[ g0[3] ].bo[ b0[3] ].p; }
      if( g0[4] > -1 ){ s0[4]= bz[ g0[4] ].bo[ b0[4] ].s; p0[4]= bz[ g0[4] ].bo[ b0[4] ].p; }
      if( g0[5] > -1 ){ s0[5]= bz[ g0[5] ].bo[ b0[5] ].s; p0[5]= bz[ g0[5] ].bo[ b0[5] ].p; }

      // faces and boundary information in h1
/*    q1[0] = hx[h1].face(   f1[1]  );
      q1[1] = hx[h1].face( !(f1[1]) );
      q1[2] = hx[h1].face( !(f1[2]) );
      q1[3] = hx[h1].face(   f1[2]  );
      q1[4] = hx[h1].face(   f1[0]  ); 
      q1[5] = hx[h1].face( !(f1[0]) );*/

      q1[0] = face(   f1[1]  );
      q1[1] = face( !(f1[1]) );
      q1[2] = face( !(f1[2]) );
      q1[3] = face(   f1[2]  );
      q1[4] = face(   f1[0]  ); 
      q1[5] = face( !(f1[0]) );

      g1[0] = hx[h1].q[ q1[0] ].g; 
      g1[1] = hx[h1].q[ q1[1] ].g; 
      g1[2] = hx[h1].q[ q1[2] ].g; 
      g1[3] = hx[h1].q[ q1[3] ].g; 
      g1[4] = hx[h1].q[ q1[4] ].g; 
      g1[5] = hx[h1].q[ q1[5] ].g; 

      b1[0] = hx[h1].q[ q1[0] ].b; 
      b1[1] = hx[h1].q[ q1[1] ].b; 
      b1[2] = hx[h1].q[ q1[2] ].b; 
      b1[3] = hx[h1].q[ q1[3] ].b; 
      b1[4] = hx[h1].q[ q1[4] ].b; 

      if( g1[0] > -1 ){ s1[0]= bz[ g1[0] ].bo[ b1[0] ].s; p1[0]= bz[ g1[0] ].bo[ b1[0] ].p; }
      if( g1[1] > -1 ){ s1[1]= bz[ g1[1] ].bo[ b1[1] ].s; p1[1]= bz[ g1[1] ].bo[ b1[1] ].p; }
      if( g1[2] > -1 ){ s1[2]= bz[ g1[2] ].bo[ b1[2] ].s; p1[2]= bz[ g1[2] ].bo[ b1[2] ].p; }
      if( g1[3] > -1 ){ s1[3]= bz[ g1[3] ].bo[ b1[3] ].s; p1[3]= bz[ g1[3] ].bo[ b1[3] ].p; }
      if( g1[4] > -1 ){ s1[4]= bz[ g1[4] ].bo[ b1[4] ].s; p1[4]= bz[ g1[4] ].bo[ b1[4] ].p; }
      if( g1[5] > -1 ){ s1[5]= bz[ g1[5] ].bo[ b1[5] ].s; p1[5]= bz[ g1[5] ].bo[ b1[5] ].p; }

      // new vertices for mini hex in h0
      x= vx.n; vx.n+= 8; vx.resize( xdum );
      v= vt.n; vt.n+= 8; vt.resize( pdum );

      z[0]= 0.5*( vx[ x0[0] ] + vx[ x0[1] ] );
      z[1]= 0.5*( vx[ x0[2] ] + vx[ x0[3] ] );
      z[2]= 0.5*( vx[ x0[4] ] + vx[ x0[5] ] );
      z[3]= 0.5*( vx[ x0[6] ] + vx[ x0[7] ] );

      vx[x]= c1*z[0]+ c2*z[1];                           y0[0]=x++;
      vx[x]= c2*z[0]+ c1*z[1];                           y0[2]=x++;

      vx[x]= 0.5*( c1*z[0]+ c2*z[1]+ c1*z[2]+ c2*z[3] ); y0[4]=x++;
      vx[x]= 0.5*( c2*z[0]+ c1*z[1]+ c2*z[2]+ c1*z[3] ); y0[6]=x++;
  
      z[0]= vx[ x0[1] ];
      z[1]= vx[ x0[3] ];
      z[2]= vx[ x0[5] ];
      z[3]= vx[ x0[7] ];
  
      vx[x]=       c1*z[0]+ c2*z[1];                     y0[1]=x++;
      vx[x]=       c2*z[0]+ c1*z[1];                     y0[3]=x++;
      vx[x]= 0.5*( c1*z[0]+ c2*z[1]+ c1*z[2]+ c2*z[3] ); y0[5]=x++;
      vx[x]= 0.5*( c2*z[0]+ c1*z[1]+ c2*z[2]+ c1*z[3] ); y0[7]=x++;

      vt[v].id= v; w0[0] = v++;
      vt[v].id= v; w0[2] = v++;
      vt[v].id= v; w0[4] = v++;
      vt[v].id= v; w0[6] = v++;
      vt[v].id= v; w0[1] = v++;
      vt[v].id= v; w0[3] = v++;
      vt[v].id= v; w0[5] = v++;
      vt[v].id= v; w0[7] = v++;

      // new vertices for mini hex in h1
      x= vx.n; vx.n+= 8; vx.resize(xdum );
      v= vt.n; vt.n+= 8; vt.resize(pdum );

      z[0]= 0.5*( vx[ x1[0] ] + vx[ x1[1] ] );
      z[1]= 0.5*( vx[ x1[2] ] + vx[ x1[3] ] );
      z[2]= 0.5*( vx[ x1[4] ] + vx[ x1[5] ] );
      z[3]= 0.5*( vx[ x1[6] ] + vx[ x1[7] ] );

      vx[x]= c1*z[0]+ c2*z[1];                           y1[1]=x++;
      vx[x]= c2*z[0]+ c1*z[1];                           y1[3]=x++;

      vx[x]= 0.5*( c1*z[0]+ c2*z[1]+ c1*z[2]+ c2*z[3] ); y1[5]=x++;
      vx[x]= 0.5*( c2*z[0]+ c1*z[1]+ c2*z[2]+ c1*z[3] ); y1[7]=x++;

      w1[0] = w0[1];
      w1[2] = w0[3];
      w1[4] = w0[5];
      w1[6] = w0[7];
  
      y1[0] = y0[1];
      y1[2] = y0[3];
      y1[4] = y0[5];
      y1[6] = y0[7];
  
      vt[v].id= v; w1[1] = v++;
      vt[v].id= v; w1[3] = v++;
      vt[v].id= v; w1[5] = v++;
      vt[v].id= v; w1[7] = v++;

      // new hexes in h0
      n0=  hx.n; hx.n+=5; hx.resize( hdum );

      // mini, f0[2], !f0[0], !f0[2], f0[1]
      hex( n0+0, w0[0],w0[1],w0[2],w0[3], w0[4],w0[5],w0[6],w0[7],
                 y0[0],y0[1],y0[2],y0[3], y0[4],y0[5],y0[6],y0[7] );

      hex( n0+1, w0[2],w0[3],v0[2],v0[3], w0[6],w0[7],v0[6],v0[7],
                 y0[2],y0[3],x0[2],x0[3], y0[6],y0[7],x0[6],x0[7] );

      hex( n0+2, w0[4],w0[5],w0[6],w0[7], v0[4],v0[5],v0[6],v0[7],
                 y0[4],y0[5],y0[6],y0[7], x0[4],x0[5],x0[6],x0[7] );

      hex( n0+3, v0[0],v0[1],w0[0],w0[1], v0[4],v0[5],w0[4],w0[5],
                 x0[0],x0[1],y0[0],y0[1], x0[4],x0[5],y0[4],y0[5] );

      hex( n0+4, v0[0],w0[0],v0[2],w0[2], v0[4],w0[4],v0[6],w0[6],
                 x0[0],y0[0],x0[2],y0[2], x0[4],y0[4],x0[6],y0[6] );

      // bottom (external) faces
      force( n0+0,4,-1,-1 );
      force( n0+1,4,-1,-1 );
      force( n0+3,4,-1,-1 );   
      force( n0+4,4,-1,-1 );

      // internal faces
      force( n0+0,5, n0+2,4 );
      force( n0+0,3, n0+1,2 );
      force( n0+0,2, n0+3,3 );

      force( n0+2,3, n0+1,5 );
      force( n0+2,2, n0+3,5 );

      force( n0+0,0, n0+4,1 );
      force( n0+1,0, n0+4,3 );
      force( n0+2,0, n0+4,5 );
      force( n0+3,0, n0+4,2 );

      // faces which will connect to hexes in h1
      force( n0+0,1,-1,-1 );
      force( n0+1,1,-1,-1 );
      force( n0+2,1,-1,-1 );   
      force( n0+3,1,-1,-1 );

      // faces connecting to rest of mesh
      if( g0[0] > -1 ){ force( n0+4,0, -1,-1 ); }else{ replace( h0,q0[0], n0+4,0 ); }
      if( g0[2] > -1 ){ force( n0+3,2, -1,-1 ); }else{ replace( h0,q0[2], n0+3,2 ); }
      if( g0[3] > -1 ){ force( n0+1,3, -1,-1 ); }else{ replace( h0,q0[3], n0+1,3 ); }
      if( g0[5] > -1 ){ force( n0+2,5, -1,-1 ); }else{ replace( h0,q0[5], n0+2,5 ); }

      // sort out boundaries of h0
      detach( g0[4], b0[4] );
      attach( (MXGRP-1), h0,q0[4], -1,-1,-1 );

      // faces which will connect to hexes in h1
      attach( (MXGRP-1), n0+0,1, -1,-1,-1 );
      attach( (MXGRP-1), n0+1,1, -1,-1,-1 );
      attach( (MXGRP-1), n0+2,1, -1,-1,-1 );
      attach( (MXGRP-1), n0+3,1, -1,-1,-1 );

      // external faces
      //if( g0[0] > -1 ){ attach( g0[0], n0+4,0,    -1,   -1,-1 ); }
      if( g0[0] > -1 ){ attach( g0[0], n0+4,0, s0[0],p0[0],-1 ); }
      if( g0[2] > -1 ){ attach( g0[2], n0+3,2, s0[2],p0[2],-1 ); }
      if( g0[3] > -1 ){ attach( g0[3], n0+1,3, s0[3],p0[3],-1 ); }
      if( g0[5] > -1 ){ attach( g0[5], n0+2,5, s0[5],p0[5],-1 ); }

      if( g0[4] > -1 ){ attach( g0[4], n0+0,4, s0[4],p0[4],-1 ); }
      if( g0[4] > -1 ){ attach( g0[4], n0+1,4, s0[4],p0[4],-1 ); }
      if( g0[4] > -1 ){ attach( g0[4], n0+3,4, s0[4],p0[4],-1 ); }
      if( g0[4] > -1 ){ attach( g0[4], n0+4,4, s0[4],p0[4],-1 ); }

      // new hexes in h1
      n1=  hx.n; hx.n+=5; hx.resize( hdum );

      // mini, f0[2], !f0[0], !f0[2], !f0[1]
      hex( n1+0, w1[0],w1[1],w1[2],w1[3], w1[4],w1[5],w1[6],w1[7],
                 y1[0],y1[1],y1[2],y1[3], y1[4],y1[5],y1[6],y1[7] );

      hex( n1+1, w1[2],w1[3],v1[2],v1[3], w1[6],w1[7],v1[6],v1[7],
                 y1[2],y1[3],x1[2],x1[3], y1[6],y1[7],x1[6],x1[7] );

      hex( n1+2, w1[4],w1[5],w1[6],w1[7], v1[4],v1[5],v1[6],v1[7],
                 y1[4],y1[5],y1[6],y1[7], x1[4],x1[5],x1[6],x1[7] );

      hex( n1+3, v1[0],v1[1],w1[0],w1[1], v1[4],v1[5],w1[4],w1[5],
                 x1[0],x1[1],y1[0],y1[1], x1[4],x1[5],y1[4],y1[5] );

      hex( n1+4, w1[1],v1[1],w1[3],v1[3], w1[5],v1[5],w1[7],v1[7],
                 y1[1],x1[1],y1[3],x1[3], y1[5],x1[5],y1[7],x1[7] );

      // bottom (external) faces
      force( n1+0,4,-1,-1 );
      force( n1+1,4,-1,-1 );
      force( n1+3,4,-1,-1 );   
      force( n1+4,4,-1,-1 );

      // internal faces
      force( n1+0,5, n1+2,4 );
      force( n1+0,3, n1+1,2 );
      force( n1+0,2, n1+3,3 );

      force( n1+2,3, n1+1,5 );
      force( n1+2,2, n1+3,5 );

      force( n1+0,1, n1+4,0 );
      force( n1+1,1, n1+4,3 );
      force( n1+2,1, n1+4,5 );
      force( n1+3,1, n1+4,2 );

      // faces which will connect to hexes in h0
      force( n1+0,0,-1,-1 );
      force( n1+1,0,-1,-1 );
      force( n1+2,0,-1,-1 );   
      force( n1+3,0,-1,-1 );

      // faces connecting to rest of mesh
      if( g1[1] > -1 ){ force( n1+4,1, -1,-1 ); }else{ replace( h1,q1[1], n1+4,1 ); }
      if( g1[2] > -1 ){ force( n1+3,2, -1,-1 ); }else{ replace( h1,q1[2], n1+3,2 ); }
      if( g1[3] > -1 ){ force( n1+1,3, -1,-1 ); }else{ replace( h1,q1[3], n1+1,3 ); }
      if( g1[5] > -1 ){ force( n1+2,5, -1,-1 ); }else{ replace( h1,q1[5], n1+2,5 ); }

      // sort out boundaries of h1
      detach( g1[4], b1[4] );
      attach( (MXGRP-1), h1,q1[4], -1,-1,-1 );

      // faces which will connect to hexes in h0
      attach( (MXGRP-1), n1+0,0, -1,-1,-1 );
      attach( (MXGRP-1), n1+1,0, -1,-1,-1 );
      attach( (MXGRP-1), n1+2,0, -1,-1,-1 );
      attach( (MXGRP-1), n1+3,0, -1,-1,-1 );

      // external faces
      //if( g1[1] > -1 ){ attach( g1[1], n1+4,1,    -1,   -1,-1 ); }
      if( g1[1] > -1 ){ attach( g1[1], n1+4,1, s1[1],p1[1],-1 ); }
      if( g1[2] > -1 ){ attach( g1[2], n1+3,2, s1[2],p1[2],-1 ); }
      if( g1[3] > -1 ){ attach( g1[3], n1+1,3, s1[3],p1[3],-1 ); }
      if( g1[5] > -1 ){ attach( g1[5], n1+2,5, s1[5],p1[5],-1 ); }
      
      if( g1[4] > -1 ){ attach( g1[4], n1+0,4, s1[4],p1[4],-1 ); }
      if( g1[4] > -1 ){ attach( g1[4], n1+1,4, s1[4],p1[4],-1 ); }
      if( g1[4] > -1 ){ attach( g1[4], n1+3,4, s1[4],p1[4],-1 ); }
      if( g1[4] > -1 ){ attach( g1[4], n1+4,4, s1[4],p1[4],-1 ); }

      // join faces between h0 and h1
      join( n0+0,1, n1+0,0 );
      join( n0+1,1, n1+1,0 );
      join( n0+2,1, n1+2,0 );
      join( n0+3,1, n1+3,0 );

      n[0]=n0;
      n[1]=n1;

      return;
  }
