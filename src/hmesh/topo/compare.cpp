#  include <hmesh/hmesh.h>

   bool hmesh_t::compare( INT_ f, INT_ d, INT_ b, hmesh_t &o, INT_ c, VINT_ &pv, VINT_4 &qv )
  {
      INT_            i,j,h,k,l,m,n,p,q,v;
      INT_            n1=0,n2=0;
      bool            flag;
      REAL_           tol= 5e-2;
      VINT_2          list,dum;
      INT_4           buf,buf1,xbuf;
      VINT_           tmp( bz[b].bo.n,-1 );

      vtx_t           w;
      INT_ pq[8][3]= { {0,4,2}, {2,4,1}, {3,4,0}, {1,4,3}, 
                       {0,2,5}, {2,1,5}, {3,0,5}, {1,3,5} };      

      for( i=0;i<bz[b].bx.n;i++ ){ if( bz[b].bx[i].id > -1 ){ n1++; }; }
      for( i=0;i<o.bz[c].bx.n;i++ ){ if( o.bz[c].bx[i].id > -1 ){ n2++; }; }

      flag= false;
      if( ( bz[b].bo.n == o.bz[c].bo.n ) && ( n1 == n2 ) )
     {

         m=0;
 
         n= bz[b].bx.n;
// try overlaying c to b
         for( j=0;j<n;j++ )
        {
            i=   bz[b].bx[j].id;
            if( i > -1 )
           {
               for( k=0;k<n;k++ )
              {
                  h= o.bz[c].bx[k].id;
             
                  if( h > -1 )
                 {
                     if( f>-1 ){  fr[f].prdx( vx[i], w, d, 1 ); }else{ w=vx[i]; };
                     w-= o.vx[h];
                     if( norminf(w) <= tol )
                    {
                        m++;
                        pv[i]= h;
                        break;
                    }
                 }
              }
           }
        } 
         printf( "%d %d points close enough\n", m,n1 );
         flag= (m==n1 && m > 0);
     }

      if( flag )
     {
         for( j=0;j<bz[b].bo.n;j++ )
        {
            h= bz[b].bo[j].h;            
            q= bz[b].bo[j].q;            
            hx[h].pack( q,buf,xbuf );

            buf[0]= pv[buf[0]];
            buf[1]= pv[buf[1]];
            buf[2]= pv[buf[2]];
            buf[3]= pv[buf[3]];

            list.n=0;
            o.point( buf[0],list,dum );
 
            for( m=0;m<list.n;m++ )
           {
               k= list[m][0];
               v= list[m][1];
               for( l=0;l<3;l++ )
              {
                  p= pq[v][l];
                  if( ( o.hx[k].q[p].g== c ) && ( tmp[o.hx[k].q[p].b] ==-1 ))
                 {
                     o.hx[k].pack( p, buf1,xbuf );
                     if( buf1[0] == buf[0] &&
                         buf1[1] == buf[1] &&
                         buf1[2] == buf[2] &&
                         buf1[3] == buf[3] )
                    {
                        i= qv.append(-1);
                        printf( "matching face?\n" );
                        qv[i][0]= h;
                        qv[i][1]= q;
                        qv[i][2]= k;
                        qv[i][3]= p;
                        tmp[ o.hx[k].q[p].b ]=0;
                        break;
                    }
                 }
              }        
           }
        }
     }
      return flag;
   
  }


   void hmesh_t::compare1( INT_ f, INT_ d, INT_ b, hmesh_t &o, INT_ c, VINT_ &pv, VINT_4 &qv )
  {
      INT_            i,j,h,k,l,m,n,p,q,v;
      INT_            n1=0,n2=0;
      VINT_2          list,dum;
      INT_4           buf,buf1,xbuf;
      INT_4           prm;
      VINT_           tmp( bz[b].bo.n,-1 );

      vtx_t           w;
      INT_ pq[8][3]= { {0,4,2}, {2,4,1}, {3,4,0}, {1,4,3}, 
                       {0,2,5}, {2,1,5}, {3,0,5}, {1,3,5} };      

      for( i=0;i<bz[b].bx.n;i++ ){ if( bz[b].bx[i].id > -1 ){ n1++; }; }
      for( i=0;i<o.bz[c].bx.n;i++ ){ if( o.bz[c].bx[i].id > -1 ){ n2++; }; }

      assert( n1 == n2 && bz[b].bo.n == o.bz[c].bo.n );


      m=0;

      n= bz[b].bx.n;

// try overlaying c to b

      for( j=0;j<n;j++ )
     {
         i=   bz[b].bx[j].id;
         if( i > -1 )
        {

            REAL_ dmin=99999.;
            INT_ imin=-1;

            for( k=0;k<o.bz[c].bx.n;k++ )
           {
               h= o.bz[c].bx[k].id;
          
               if( h > -1 )
              {
                  if( f>-1 ){  fr[f].prdx( vx[i], w, d, 1 ); }else{ w=vx[i]; };

                  w-= o.vx[h];
                  REAL_ d1= norminf(w);

                  printf( "comapre point %3d %3d %9.3e\n", i,h, d1 );

                  if( d1 < dmin )
                 {
                     imin= h;
                     dmin=d1;
                 }
              }
           }
            pv[i]= imin;
            printf( "point %3d is matched to pont %3d\n", i,imin );
        }
     } 

      for( j=0;j<bz[b].bo.n;j++ )
     {
         h= bz[b].bo[j].h;            
         q= bz[b].bo[j].q;            
         hx[h].pack( q,buf,xbuf );

         printf( "looking for face %3d %3d %3d %3d \n", buf[0],buf[1],buf[2],buf[3] );

         buf[0]= pv[buf[0]];
         buf[1]= pv[buf[1]];
         buf[2]= pv[buf[2]];
         buf[3]= pv[buf[3]];

         sort4( buf,prm );
         printf( "should become    %3d %3d %3d %3d \n", buf[prm[0]],buf[prm[1]],buf[prm[2]],buf[prm[3]] );

         list.n=0;
         dum.n=0;
         o.point( buf[0],list,dum );

         for( m=0;m<list.n;m++ )
        {
            k= list[m][0];
            v= list[m][1];
            for( l=0;l<3;l++ )
           {
               p= pq[v][l];
               if( ( o.hx[k].q[p].g== c ) && ( tmp[o.hx[k].q[p].b] ==-1 ))
              {
                  o.hx[k].pack( p, buf1,xbuf );
                  printf( " %3d %3d %3d %3d \n", buf1[0],buf1[1],buf1[2],buf1[3] );
                  if( buf1[0] == buf[prm[0]] &&
                      buf1[1] == buf[prm[1]] &&
                      ( ( buf1[2] == buf[prm[2]] && buf1[3] == buf[prm[3]] ) || 
                        ( buf1[3] == buf[prm[2]] && buf1[2] == buf[prm[3]] ) ) )
                 {
                     i= qv.append(-1);
                     printf( "matching face?\n" );
                     qv[i][0]= h;
                     qv[i][1]= q;
                     qv[i][2]= k;
                     qv[i][3]= p;
                     tmp[ o.hx[k].q[p].b ]=0;

                     goto esc;
                 }
              }
           }        
        }
esc:
         continue;
     }
      assert( qv.n == bz[b].bo.n );

      return ;
   
  }


   void hmesh_t::compare2( INT_ f, INT_ d, INT_ b, hmesh_t &o, INT_ c, VINT_ &pv, VINT_4 &qv )
  {
      INT_            i,j,h,k,l,m,n,p,q;
      INT_            n1=0,n2=0;
      VINT_2          list,dum;
      INT_4           buf,xbuf;
      VINT_           tmp( bz[b].bo.n,-1 );

      VINT_           u( bz[b].bx.n,0 );
      VINT_          ou( o.bz[c].bx.n,0 );

      vtx_t           w;
      vtx_t           x0,y0;
      INT_ pq[8][3]= { {0,4,2}, {2,4,1}, {3,4,0}, {1,4,3}, 
                       {0,2,5}, {2,1,5}, {3,0,5}, {1,3,5} };      

      for( i=0;i<bz[b].bx.n;i++ ){ if( bz[b].bx[i].id > -1 ){ n1++; }; }
      for( i=0;i<o.bz[c].bx.n;i++ ){ if( o.bz[c].bx[i].id > -1 ){ n2++; }; }

      assert( n1 == n2 && bz[b].bo.n == o.bz[c].bo.n );

// compute baricenter of surface x0. Also find which points are on the surface and which are on the edge

      x0= vtx_t(0.,0.,0.);
      m= 0;
      for( i=0;i<bz[b].bx.n;i++ )
     {
         j= bz[b].bx[i].id;
         if( j > -1 )
        { 
            x0+= vx[j]; 
            m++; 

            VINT_2 hl,ql,bl;
            point( j, hl,ql,bl );
            u[i]= bl.n; 
        };
     }
      x0/= m;
      printf( "\n" );
      for( i=0;i<bz[b].bx.n;i++ )
     {
         j= bz[b].bx[i].id;
         if( j > -1 ){ printf( "% 9.3e % 9.3e % 9.3e\n", vx[j][0]-x0[0], vx[j][1]-x0[1], vx[j][2]-x0[2] ); }
     }
      printf( "\n" );

      y0= vtx_t(0.,0.,0.);
      m= 0;
      for( i=0;i<o.bz[c].bx.n;i++ )
     {
         j= o.bz[c].bx[i].id;
         if( j > -1 )
        { 
            y0+= o.vx[j]; 
            m++; 
            VINT_2 hl,ql,bl;
            o.point( j, hl,ql,bl );
            ou[i]= bl.n; 
        };
     }
      y0/= m;
      for( i=0;i<o.bz[c].bx.n;i++ )
     {
         j= o.bz[b].bx[i].id;
         if( j > -1 ){ printf( "% 9.3e % 9.3e % 9.3e\n", o.vx[j][0]-y0[0], o.vx[j][1]-y0[1], o.vx[j][2]-y0[2] ); }
     }
      printf( "\n" );


      m=0;

      n= bz[b].bx.n;

// try overlaying c to b

      for( j=0;j<n;j++ )
     {
         i=   bz[b].bx[j].id;
         if( i > -1 )
        {

            REAL_ dmin=99999.;
            INT_ imin=-1;

            for( k=0;k<o.bz[c].bx.n;k++ )
           {
               h= o.bz[c].bx[k].id;
          
               if( h > -1 )
              {
                  if( ( u[j]==1 ) == ( ou[k]==1 ) )
                 {
                     if( f>-1 ){  fr[f].prdx( vx[i], w, d, 1 ); }else{ w=vx[i]; };
                     w-= x0; 
                   
                     w-= (o.vx[h]-y0);
                     REAL_ d1= norminf(w);
                   
                     printf( "comapre point %3d %3d %9.3e\n", i,h, d1 );
                   
                     if( d1 < dmin )
                    {
                        imin= h;
                        dmin=d1;
                    }
                 }
              }
           }
            pv[i]= imin;
            printf( "point %3d is matched to pont %3d \n", i,imin );
            printf( "% 9.3e % 9.3e % 9.3e\n", vx[i][0]-x0[0], vx[i][1]-x0[1], vx[i][2]-x0[2] );
            printf( "% 9.3e % 9.3e % 9.3e\n", o.vx[imin][0]-y0[0], o.vx[imin][1]-y0[1], o.vx[imin][2]-y0[2] );

            printf( "% 9.3e % 9.3e % 9.3e\n", vx[i][0], vx[i][1], vx[i][2] );
            printf( "% 9.3e % 9.3e % 9.3e\n", o.vx[imin][0], o.vx[imin][1], o.vx[imin][2] );
        }
     } 

      for( j=0;j<bz[b].bo.n;j++ )
     {
         h= bz[b].bo[j].h;            
         q= bz[b].bo[j].q;            
         hx[h].pack( q,buf,xbuf );

         printf( "looking for face %3d %3d %3d %3d \n", buf[0],buf[1],buf[2],buf[3] );

         INT_4  buf1;
         buf1[0]= pv[buf[0]];
         buf1[1]= pv[buf[1]];
         buf1[2]= pv[buf[2]];
         buf1[3]= pv[buf[3]];

//       sort4( buf,prm );
         INT_ u= min4(buf1);

         INT_ a= mqp[0][u][1];
         INT_ b= mqp[1][u][1];

         bool v= ( buf1[b] < buf1[a] );

         for( i=0;i<4;i++ ){ buf[i]= buf1[ mqp[v][u][i] ]; };

         printf( "should become    %3d %3d %3d %3d \n", buf[0],buf[1],buf[2],buf[3] );


         list.n=0;
         dum.n=0;
         o.point( buf[0],list,dum );

         for( m=0;m<list.n;m++ )
        {
            k= list[m][0];
            v= list[m][1];
            for( l=0;l<3;l++ )
           {
               p= pq[v][l];
               if( ( o.hx[k].q[p].g== c ) && ( tmp[o.hx[k].q[p].b] ==-1 ))
              {
                  o.hx[k].pack( p, buf1,xbuf );
                  printf( " %3d %3d %3d %3d \n", buf1[0],buf1[1],buf1[2],buf1[3] );
                  if( buf1[0] == buf[0] &&
                      buf1[1] == buf[1] &&
                      ( ( buf1[2] == buf[2] && buf1[3] == buf[3] ) || 
                        ( buf1[3] == buf[2] && buf1[2] == buf[3] ) ) )
                 {
                     i= qv.append(-1);
                     printf( "matching face?\n" );
                     qv[i][0]= h;
                     qv[i][1]= q;
                     qv[i][2]= k;
                     qv[i][3]= p;
                     tmp[ o.hx[k].q[p].b ]=0;

                     goto esc;
                 }
              }
           }        
        }
esc:
         continue;
     }
      assert( qv.n == bz[b].bo.n );
      printf( "found %d matching faces\n", qv.n ); 

      return ;
   
  }


