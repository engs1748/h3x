#  include <hmesh/hmesh.h>



   void hmesh_t::mul( INT_ f, INT_ d, INT_ n, INT_ *mask )
  {
      INT_            b,c,e,i,j,h,k,l,m,q,r;
      INT_            v0,h0,q0,n0,g0;
      bool            flag;

      vtx_t           w;

      VINT_           prd(vt.n,-1);
      VINT_           wrk(vt.n,-1);
      INT_            prb[MXGRP];
      VINT_4          prq;
      VINT_3          bnd;

      assert( vt.n == vx.n );
      g0= ng;
      v0= vt.n;
      h0= hx.n;
      q0= qv.n;

// identify matching surfaces

      for( b=0;b<nb;b++ ){   prb[b]=-1; }
      for( b=0;b<nb;b++ )
     {
         for( c=b+1;c<nb;c++ )
        {
            flag= compare( f,d, b,*this,c, wrk, prq );
            if( flag )
           { 

               prb[b]=1;
               for( j=bz[c].bo.n-1;j>=0;j-- )
              {
                  i= bnd.append(-1);
                  h= bz[c].bo[j].h;
                  q= bz[c].bo[j].q;
                  detach( c,j );
                  attach( (MXGRP-1),h,q,-1,-1,-1 );
                  bnd[i][0]= h;
                  bnd[i][1]= q;
                  bnd[i][2]= c;
              }
           }
            else
           { 
               flag= compare( f,d, c,*this,b, wrk, prq ); 
               if( flag )
              { 
                  prb[c]=1;
                  for( j=bz[b].bo.n-1;j>=0;j-- )
                 {
                     i= bnd.append(-1);
                     h= bz[b].bo[j].h;
                     q= bz[b].bo[j].q;
                     detach( b,j );
                     attach( (MXGRP-1),h,q,-1,-1,-1 );
                     bnd[i][0]= h;
                     bnd[i][1]= q;
                     bnd[i][2]= b;
                 }
              }
           }
            
            if( flag )
           { 
               
               for( j=0;j<bz[c].bx.n;j++ )
              {
                  i= bz[b].bx[j].id;
                  h= bz[c].bx[j].id;
                  prd[i]= wrk[i];
                  prd[h]= wrk[h];
              }
           }
        }
     }

// prepare vertex alias

      r= 0;
      for( i=0;i<vt.n;i++ )
     { 
         if( prd[i] == -1 ){ prd[i]= i; r++; };
         wrk[i]= prd[i];
     }


      for( k=0;k<n;k++ )
     {

// replicate vertices
         n0= vt.n;
         for( i=0;i<v0;i++ )
        { 
            if( prd[i]!= i ){ wrk[i]= wrk[prd[i]]; }
        }
         for( i=0;i<v0;i++ )
        {
            if( prd[i] == i )
           {
               fr[f].prdx( vx[wrk[i]], w, d, 1 );

               wrk[i]= vt.append(pdum);
               assert( wrk[i]==vx.append(xdum) );

               j= wrk[i];
               vt[j].id= j;
               vx[j][0]= w[0];
               vx[j][1]= w[1];
               vx[j][2]= w[2];
               
           }
        }

// replicate elements         
         for( i=0;i<h0;i++ )
        {
            j= hx.append(hdum);
            hex( j, wrk[hx[i].v[0]], wrk[hx[i].v[1]], wrk[hx[i].v[2]], wrk[hx[i].v[3]],
                    wrk[hx[i].v[4]], wrk[hx[i].v[5]], wrk[hx[i].v[6]], wrk[hx[i].v[7]],
                    wrk[hx[i].x[0]], wrk[hx[i].x[1]], wrk[hx[i].x[2]], wrk[hx[i].x[3]],
                    wrk[hx[i].x[4]], wrk[hx[i].x[5]], wrk[hx[i].x[6]], wrk[hx[i].x[7]] );

        }

// replicate internal faces
         for( i=g0;i<q0;i++ )
        {
            j= qv[i].h[0];
            h= qv[i].h[1];
            l= qv[i].q[0];
            m= qv[i].q[1];
            force( j+(k+1)*h0,l, h+(k+1)*h0,m );
        }

     }
// seal boundary between two copies
      for( k=0;k<n;k++ )
     {
         for( i=0;i<prq.n;i++ )
        {
            j= prq[i][0];
            h= prq[i][1];
            l= prq[i][2];
            m= prq[i][3];
            
            force( j+(k+1)*h0,h, -1,-1 );
            attach( (MXGRP-1), j+(k+1)*h0,h, -1,-1,-1 ); 

            if( k > 0 )
           { 
               force( l+k*h0,m,-1,-1 );  
               attach( (MXGRP-1), l+k*h0,m, -1,-1,-1 ); 
           }
            
            join( l+k*h0,m, j+(k+1)*h0,h );
        }

     }

      n0= nb;

      printf( "nb is now %d\n", nb );
      for( b=0;b<n0;b++ )
     {
         if( prb[b] == -1 )
        {
            q0= bz[b].bo.n;
            e= b;
           if( q0 > 0 )
           {
               for( k=0;k<n;k++ )
              {
                  if( mask[b] == -1 ){ e= nb++; }
                  
                  for( j=0;j<q0;j++ )
                 {
                     h= bz[b].bo[j].h;
                     q= bz[b].bo[j].q;
                     force( h+h0*(k+1),q,-1,-1 );
                 }
              }
           }
        }
     }

      INT_ h1= h0*n;
      for( j=0;j<bnd.n;j++ )
     {
         h= bnd[j][0];
         q= bnd[j][1];
         b= bnd[j][2];
         force( h+h1,q,-1,-1 );
     }

      for( b=0;b<n0;b++ )
     {
         if( prb[b] == -1 )
        {
            q0= bz[b].bo.n;
            e= b;
           if( q0 > 0 )
           {
               for( k=0;k<n;k++ )
              {
                  if( mask[b] == -1 ){ e= nb++; }
                  
                  for( j=0;j<q0;j++ )
                 {
                     h= bz[b].bo[j].h;
                     q= bz[b].bo[j].q;
                     attach( e, h+h0*(k+1),q,-1,-1,-1 );
                 }
              }
           }
        }
         else
        {
        }
     }

      for( j=0;j<bnd.n;j++ )
     {
         h= bnd[j][0];
         q= bnd[j][1];
         b= bnd[j][2];
         attach( b, h+h1,q,-1,-1,-1 );
     }

      nb= 0;
      for( j=0;j<128;j++ ){ if( bz[j].bo.n>0 ){ nb=j+1; }; }
      return;
  }
