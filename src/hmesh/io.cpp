
#  include <hmesh/hmesh.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void hmesh_t::fwrite( FILE *g )
  {
      INT_ buf[16];
      memset( buf,-1,sizeof(buf));

      buf[ 0]= fr.n;
      buf[ 1]= vt.n;
      buf[ 2]= vx.n;
      buf[ 3]= hx.n;
      buf[ 4]= qv.n;
      buf[ 5]= eg.n;
      buf[ 6]= nb;
      buf[ 7]= ng;
      buf[ 8]= indx.n;
      buf[ 9]= ns;
      buf[10]= bi.n;
      buf[11]= ni;

    ::fwrite(         buf,     16, sizeof(INT_),   g );
    ::fwrite(   fr.data(),   fr.n, sizeof(FRAME_), g );
    ::fwrite(   vt.data(),   vt.n, sizeof(pt_t),   g );
    ::fwrite(   vx.data(),   vx.n, sizeof(REAL_3), g );
    ::fwrite(   hx.data(),   hx.n, sizeof(hex_t),  g );
    ::fwrite(   qv.data(),   qv.n, sizeof(qvad_t), g );
    ::fwrite(   eg.data(),   eg.n, sizeof(edg_t),  g );
      for( INT_ i=0;i<nb;i++ )
     {
       ::fwrite(&(bz[i].bo.n),           1,sizeof(INT_),   g );
       ::fwrite(&(bz[i].bx.n),           1,sizeof(INT_),   g );
       ::fwrite(  bz[i].lbl,              2,sizeof(INT_),   g );
       ::fwrite(&(bz[i].b),               1,sizeof(bool),   g );
       ::fwrite(  bz[i].bo.data() ,bz[i].bo.n,sizeof(bnd_t),  g );
       ::fwrite(  bz[i].bx.data() ,bz[i].bx.n,sizeof(bvtx_t),  g );
     }
      for( INT_ i=0;i<ns;i++ )
     {
         sc[i].fwrite( g );
     }
      for( INT_ i=0;i<bi.n;i++ )
     {
         bi[i].fwrite( g ); 
     }
    ::fwrite( indx.data(),indx.n,sizeof(INT_),   g );

/*  ::fwrite( &(su.n),1,sizeof(INT_), g );
      for( INT_ i=0;i<su.n;i++ )
     {
         surf_e t= su[i]->type();
       ::fwrite( &t,1,sizeof(surf_e), g );
         su[i]->fwrite( g );
     }*/
      return; 
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         
// Changes History
// Next Change(s)  -

   void hmesh_t::fread( FILE *g )
  {

      INT_ buf[16];

    ::fread(  buf, 16,sizeof(INT_),    g );

      fr.n=   buf[ 0];
      vt.n=   buf[ 1];
      vx.n=   buf[ 2];
      hx.n=   buf[ 3];
      qv.n=   buf[ 4];
      eg.n=   buf[ 5];
      nb=     buf[ 6];
      ng=     buf[ 7];
      indx.n= buf[ 8];
      ns=     buf[ 9];
      bi.n=   buf[10];
      ni=     buf[11];

         fr.resize( fdum );
         vt.resize( pdum );
         vx.resize( -1. );
         hx.resize( hdum );
         qv.resize( qdum );
         eg.resize( edum );
      indx.resize(-1);

    ::fread(   fr.data(),   fr.n,sizeof(FRAME_), g );
    ::fread(   vt.data(),   vt.n,sizeof(pt_t),   g );
    ::fread(   vx.data(),   vx.n,sizeof(REAL_3), g );
    ::fread(   hx.data(),   hx.n,sizeof(hex_t),  g );
    ::fread(   qv.data(),   qv.n,sizeof(qvad_t), g );
    ::fread(   eg.data(),   eg.n,sizeof(edg_t),  g );
      for( INT_ i=0;i<nb;i++ )
     {
         bvtx_t bd;
       ::fread(&(bz[i].bo.n),     1,sizeof(INT_),  g ); bz[i].bo.resize(bdum);
       ::fread(&(bz[i].bx.n),     1,sizeof(INT_),  g ); bz[i].bx.resize(bd);
       ::fread(  bz[i].lbl,              2,sizeof(INT_),   g );
       ::fread(&(bz[i].b),               1,sizeof(bool),   g );
       ::fread(  bz[i].bo.data(), bz[i].bo.n,sizeof(bnd_t), g );
       ::fread(  bz[i].bx.data(), bz[i].bx.n,sizeof(bvtx_t), g );
     }
      for( INT_ i=0;i<ns;i++ )
     {
         sc[i].fwrite( g );
     }
      bi.resize(ibdum);
      for( INT_ i=0;i<bi.n;i++ )
     {
         bi[i].fread( g ); 
     }
    ::fread(indx.data(),indx.n,sizeof(INT_),   g );

/*  ::fread( &(su.n),1,sizeof(INT_), g );
      su.resize(NULL);

      for( INT_ i=0;i<su.n;i++ )
     {
         surf_e t=surf_bad;
       ::fread( &t,1,sizeof(surf_e), g );
         
         surf_c *wrk= NULL;
         newsurf( t,&wrk );
         su[i]= wrk;
         su[i]->fread( g );
     }*/

/*    vz.n= 1;
      vz.resize( dinitz );
      vz[0].b.n= 0;
      vz[0].v= vx.n;*/
  }
