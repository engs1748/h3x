
#  include <hmesh/hmesh.h>

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:34:20 BST 2018
// Changes History
// Next Change(s)  -

   hmesh_t::hmesh_t()
  {
      nb=0;
      ng=0;
      ni=0;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:34:20 BST 2018
// Changes History
// Next Change(s)  -

   hmesh_t::~hmesh_t()
  {
      nb=0;
      ng=0;
      ni=0;

//    for( INT_ i=0;i<su.n;i++ ){ delete su[i]; su[i]=NULL; };
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:34:20 BST 2018
// Changes History
// Next Change(s)  -

   void hmesh_t::read( FILE *f )
  {
      INT_ i,j;

      fscanf( f,"%d",&(vx.n) );

      assert( vx.n>0 );
      vx.resize( -1. );

      for( i=0;i<vx.n;i++ )
     {
         REAL_3 buf;
         fscanf( f,"%lf %lf %lf", buf+0,buf+1,buf+2 );
         vx[i]= vtx_t(buf);
     }

      fscanf( f,"%d",&(hx.n) );
      hx.resize( hdum );

      indx.n= hx.n; indx.resize(-1);
       
      for( i=0;i<hx.n;i++ )
     {

         hx[i].id= i;
         
         fscanf( f,"%d %d %d %d %d %d %d %d", hx[i].v+0,  hx[i].v+1,
                                              hx[i].v+2,  hx[i].v+3,
                                              hx[i].v+4,  hx[i].v+5,
                                              hx[i].v+6,  hx[i].v+7 );

         j= hx[i].v[0]; vt[j].h= i; vt[j].k= 0;
         j= hx[i].v[1]; vt[j].h= i; vt[j].k= 1;
         j= hx[i].v[2]; vt[j].h= i; vt[j].k= 2;
         j= hx[i].v[3]; vt[j].h= i; vt[j].k= 3;
         j= hx[i].v[4]; vt[j].h= i; vt[j].k= 4;
         j= hx[i].v[5]; vt[j].h= i; vt[j].k= 5;
         j= hx[i].v[6]; vt[j].h= i; vt[j].k= 6;
         j= hx[i].v[7]; vt[j].h= i; vt[j].k= 7;

         indx[i]= i;

         hx[i].q[0].init();
         hx[i].q[1].init();
         hx[i].q[2].init();
         hx[i].q[3].init();
         hx[i].q[4].init();
         hx[i].q[5].init();

     }
  };

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:34:20 BST 2018
// Changes History
// Next Change(s)  -

   void hmesh_t::hrend( INT_ i, dir_t a, FILE *f )
  {
      INT_ j;
      j= face( a );
      j= hx[i].q[j].q;
      qrend( j,f );
  };

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:34:20 BST 2018
// Changes History
// Next Change(s)  -

   void hmesh_t::qrend( INT_ l, FILE *f )
  {
      INT_       i,j;
      INT_4      wrk;

      pts( hx,qv, l, wrk );

/*    fprintf( f,"%3d, %2d %2d %2d %2d, %2d %2d %2d %2d\n",l,wrk[0],wrk[1],wrk[2],wrk[3],
                                                   qv[l].h[0], qv[l].q[0],
                                                   qv[l].h[1], qv[l].q[1] );*/

      fprintf( f,"\n# %d, %2d %2d %2d %2d, %d %d\n\n", l,wrk[0], wrk[1], wrk[2], wrk[3], qv[l].h[0],qv[l].h[1] );
      for( j=0;j<4;j++ )
     {
         i= wrk[j];
         fprintf( f,"%f %f %f\n", vx[i][0], vx[i][1], vx[i][2] );
     }
      i= wrk[0];
      fprintf( f,"%f %f %f\n", vx[i][0], vx[i][1], vx[i][2] );
      i= wrk[2];
      fprintf( f,"%f %f %f\n", vx[i][0], vx[i][1], vx[i][2] );

      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:34:20 BST 2018
// Changes History
// Next Change(s)  -

   void hmesh_t::hrend( INT_ l, FILE *f )
  {
      INT_       i0,i1,i2,i3,i4,i5,i6,i7;

      i0= hx[l].v[0];
      i1= hx[l].v[1];
      i2= hx[l].v[2];
      i3= hx[l].v[3];
      i4= hx[l].v[4];
      i5= hx[l].v[5];
      i6= hx[l].v[6];
      i7= hx[l].v[7];
/*    fprintf( f,"%3d, %2d %2d %2d %2d %2d %2d %2d %2d \n", 
               l,i0,i1,i2,i3,i4,i5,i6,i7 );*/

      fprintf( f,"\n# %d, %2d %2d %2d %2d %2d %2d %2d %2d id %2d \n\n", 
               l,i0,i1,i2,i3,i4,i5,i6,i7, hx[l].id );
      fprintf( f,"# %f %f %f\n", vx[i0][0],vx[i0][1],vx[i0][2] );
      fprintf( f,"# %f %f %f\n", vx[i1][0],vx[i1][1],vx[i1][2] );
      fprintf( f,"# %f %f %f\n", vx[i2][0],vx[i2][1],vx[i2][2] );
      fprintf( f,"# %f %f %f\n", vx[i3][0],vx[i3][1],vx[i3][2] );
      fprintf( f,"# %f %f %f\n", vx[i4][0],vx[i4][1],vx[i4][2] );
      fprintf( f,"# %f %f %f\n", vx[i5][0],vx[i5][1],vx[i5][2] );
      fprintf( f,"# %f %f %f\n", vx[i6][0],vx[i6][1],vx[i6][2] );
      fprintf( f,"# %f %f %f\n", vx[i7][0],vx[i7][1],vx[i7][2] );
      fprintf( f,"\n" );
      fprintf( f,"%f %f %f\n", vx[i0][0], vx[i0][1], vx[i0][2] );
      fprintf( f,"%f %f %f\n", vx[i1][0], vx[i1][1], vx[i1][2] );
      fprintf( f,"%f %f %f\n", vx[i3][0], vx[i3][1], vx[i3][2] );
      fprintf( f,"%f %f %f\n", vx[i2][0], vx[i2][1], vx[i2][2] );
      fprintf( f,"%f %f %f\n", vx[i0][0], vx[i0][1], vx[i0][2] );
      fprintf( f,"\n" );
      fprintf( f,"%f %f %f\n", vx[i4][0], vx[i4][1], vx[i4][2] );
      fprintf( f,"%f %f %f\n", vx[i5][0], vx[i5][1], vx[i5][2] );
      fprintf( f,"%f %f %f\n", vx[i7][0], vx[i7][1], vx[i7][2] );
      fprintf( f,"%f %f %f\n", vx[i6][0], vx[i6][1], vx[i6][2] );
      fprintf( f,"%f %f %f\n", vx[i4][0], vx[i4][1], vx[i4][2] );
      fprintf( f,"\n" );
      fprintf( f,"%f %f %f\n", vx[i0][0], vx[i0][1], vx[i0][2] );
      fprintf( f,"%f %f %f\n", vx[i4][0], vx[i4][1], vx[i4][2] );
      fprintf( f,"\n" );
      fprintf( f,"%f %f %f\n", vx[i1][0], vx[i1][1], vx[i1][2] );
      fprintf( f,"%f %f %f\n", vx[i5][0], vx[i5][1], vx[i5][2] );
      fprintf( f,"\n" );
      fprintf( f,"%f %f %f\n", vx[i3][0], vx[i3][1], vx[i3][2] );
      fprintf( f,"%f %f %f\n", vx[i7][0], vx[i7][1], vx[i7][2] );
      fprintf( f,"\n" );
      fprintf( f,"%f %f %f\n", vx[i2][0], vx[i2][1], vx[i2][2] );
      fprintf( f,"%f %f %f\n", vx[i6][0], vx[i6][1], vx[i6][2] );
      fprintf( f,"\n" );

      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:34:20 BST 2018
// Changes History
// Next Change(s)  -

   void hmesh_t::checq( const char *name )
  {
      INT_       k;
      FILE     *f=NULL;

      f= fopen( name,"w" );
      if( f )
     {

         for( k=0;k<qv.n;k++ )
        {
            qrend( k,f ); 
        }
         fclose(f);
     }
      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:34:20 BST 2018
// Changes History
// Next Change(s)  -

   void hmesh_t::checx( const char *name )
  {
      INT_       k;
      FILE     *f=NULL;

      f= fopen( name,"w" );
      if( f )
     {
         for( k=0;k<hx.n;k++ )
        {
            hrend( k,f ); 
        }
         fclose(f);
     }
      return;
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:34:20 BST 2018
// Changes History
// Next Change(s)  -

   void hmesh_t::ijk( INT_ l, INT_ m, INT_ n, INT_2 *mask, vsurf_t &su )
  {

      INT_ i,j,k;
      INT_ a,f,c,d;

      vtx_t x[8];
      REAL_ u,v,w,q[8];

      assert( l > 0 );
      assert( m > 0 );
      assert( n > 0 );

      hx.n= l*m*n; hx.resize( hdum );
      
      i= l+1;
      j= m+1; 
      k= n+1; 

      vt.n= i*j*k; vt.resize( pdum );
      vx.n= i*j*k; vx.resize( -1. );


      d= 0;
      for( k=0;k<n;k++ )
     {
         for( j=0;j<m;j++ )
        {
            for( i=0;i<l;i++ )
           {

               a= i+1;
               f= j+1;
               c= k+1;

               hx[d].id= d;
               hx[d].iz[0]= 0;
               hx[d].iz[1]= 0;
               hx[d].iz[2]= 0;
               hx[d].iz[3]= 0;

               INT_ i0= i  +(l+1)*( j+ (m+1)*k );
               INT_ i1= a  +(l+1)*( j+ (m+1)*k );
               INT_ i2= i  +(l+1)*( f+ (m+1)*k );
               INT_ i3= a  +(l+1)*( f+ (m+1)*k );
               INT_ i4= i  +(l+1)*( j+ (m+1)*c );
               INT_ i5= a  +(l+1)*( j+ (m+1)*c );
               INT_ i6= i  +(l+1)*( f+ (m+1)*c );
               INT_ i7= a  +(l+1)*( f+ (m+1)*c );

               hex( d, i0,i1,i2,i3,i4,i5,i6,i7, i0,i1,i2,i3,i4,i5,i6,i7 );

               indx[d]= d;
               d++;
               
           }
        }
     }

      if( mask[0][0] > -1 )
     {
         INT_ s,p;

         k= 0;
         printf( "\n Initial positions\n" );
         for( INT_ i=0;i<2;i++ )
        {
            s= mask[i][0], p= mask[i][1]; 
            INT_ i0,i1,i2,i3;

            i0= su[s]->p[p].v[0];
            i1= su[s]->p[p].v[1];
            i2= su[s]->p[p].v[2];
            i3= su[s]->p[p].v[3];

            vtx_t dx[2];
            su[s]->interp( su[s]->v[i0], x[0+k],dx );
            su[s]->interp( su[s]->v[i1], x[2+k],dx );
            su[s]->interp( su[s]->v[i2], x[4+k],dx );
            su[s]->interp( su[s]->v[i3], x[6+k],dx );

/*          printf( "(% 9.3e % 9.3e) -> % 9.3e % 9.3e % 9.3e \n", su[s]->v[i0][0],su[s]->v[i0][1], x[0+k][0],x[0+k][1],x[0+k][2] );
            printf( "(% 9.3e % 9.3e) -> % 9.3e % 9.3e % 9.3e \n", su[s]->v[i1][0],su[s]->v[i1][1], x[2+k][0],x[2+k][1],x[2+k][2] );
            printf( "(% 9.3e % 9.3e) -> % 9.3e % 9.3e % 9.3e \n", su[s]->v[i2][0],su[s]->v[i2][1], x[4+k][0],x[4+k][1],x[4+k][2] );
            printf( "(% 9.3e % 9.3e) -> % 9.3e % 9.3e % 9.3e \n", su[s]->v[i3][0],su[s]->v[i3][1], x[6+k][0],x[6+k][1],x[6+k][2] );*/

            k++;
        }
     }
      else 
     {
         x[0][0]= 0; x[0][1]= 0; x[0][2]= 0;
         x[1][0]= l; x[1][1]= 0; x[1][2]= 0;
         x[2][0]= 0; x[2][1]= m; x[2][2]= 0;
         x[3][0]= l; x[3][1]= m; x[3][2]= 0;
         x[4][0]= 0; x[4][1]= 0; x[4][2]= n;
         x[5][0]= l; x[5][1]= 0; x[5][2]= n;
         x[6][0]= 0; x[6][1]= m; x[6][2]= n;
         x[7][0]= l; x[7][1]= m; x[7][2]= n;
     }

      d= 0;
      for( k=0;k<n+1;k++ )
     {
         for( j=0;j<m+1;j++ )
        {
            for( i=0;i<l+1;i++ )
           {
               
               u= i;
               u/=l;

               v= j;
               v/=m;

               w= k;
               w/=n;

               q[0]= (1-u)*(1-v)*(1-w);
               q[1]=    u *(1-v)*(1-w);
               q[2]= (1-u)*   v *(1-w);
               q[3]=    u *   v *(1-w);
               q[4]= (1-u)*(1-v)*   w ;
               q[5]=    u *(1-v)*   w ;
               q[6]= (1-u)*   v *   w ;
               q[7]=    u *   v *   w ;

               vx[d]= q[0]*x[0]+ q[1]*x[1]+ q[2]*x[2]+ q[3]*x[3]+
                      q[4]*x[4]+ q[5]*x[5]+ q[6]*x[6]+ q[7]*x[7];

               vt[d].id= d;

               d++;
           }
        }
     }

      quads(); 
// boundaries

      nb=6;

      for( k=0;k<n;k++ )
     {
         for( j=0;j<m;j++ )
        {
            i= 0;   attach( 0, i+l*(j+m*k),0, mask[0][0],mask[0][1],-1 );
            i= l-1; attach( 1, i+l*(j+m*k),1, mask[1][0],mask[1][1],-1 );
        }
     }
      bmap( 0,true );
      bmap( 1,true );

      for( k=0;k<n;k++ )
     {
         for( i=0;i<l;i++ )
        {
            j= 0;   attach( 2, i+l*(j+m*k),2, mask[2][0],mask[2][1],-1 );
            j= m-1; attach( 3, i+l*(j+m*k),3, mask[3][0],mask[3][1],-1 );
        }
     }
      bmap( 2,true );
      bmap( 3,true );

      for( j=0;j<m;j++ )
     {
         for( i=0;i<l;i++ )
        {
            k= 0;   attach( 4, i+l*(j+m*k),4, mask[4][0],mask[4][1],-1 );
            k= n-1; attach( 5, i+l*(j+m*k),5, mask[5][0],mask[5][1],-1 );
        }
     }
      bmap( 4,true );
      bmap( 5,true );

// initialization groups
/*    vz.n= 1; vz.resize( dinitz );
      vz[0].v= vx.n;
      vz[0].b.n= 2; vz[0].b.resize( -1 );
      vz[0].b[0]= 0;
      vz[0].b[1]= 1;*/

/*    vz[0].b[2]= 2;
      vz[0].b[3]= 3;
      vz[0].b[4]= 4;
      vz[0].b[5]= 5;*/

      vz.n= 0; 

      bz[0].n= true;
      bz[1].n= true;
      bz[2].n= true;
      bz[3].n= true;
      bz[4].n= true;
      bz[5].n= true;

      smap( su );
       

/*    periodic();
      quads();*/

      return;
 
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:34:20 BST 2018
// Changes History
// Next Change(s)  -
   
   void hmesh_t::tec( const char *name )
  {
      INT_ t,v;
      INT_ i0,i1,i2,i3,i4,i5,i6,i7;

      FILE *f;
      f=fopen( name,"w" );

      //write file header

      fprintf( f, "TITLE = \"q3x data\"\n" );
      fprintf( f, "FILETYPE = GRID\n");
      fprintf( f, "Variables = \"X\", \"Y\", \"Z\", \"Z0\" \"Z1\" \"Z2\" \"Z3\" \"DI\" \"DJ\" \"DK\" \n" );

      for( t=0;t<hx.n;t++ ) //block coord
     {
         
         fprintf( f, "ZONE " );
         fprintf( f, "T=\"BLOCK %d\", ",hx[t].id );
         fprintf( f, "I=%d, J=%d, K=%d, ", 2,2,2 );
         fprintf( f, "DATAPACKING=BLOCK, VARLOCATION=([4,5,6,7]=CELLCENTERED)\n" );

//block ordered must be written single line for each variable
         i0= hx[t].x[0];
         i1= hx[t].x[1];
         i2= hx[t].x[2];
         i3= hx[t].x[3];
         i4= hx[t].x[4];
         i5= hx[t].x[5];
         i6= hx[t].x[6];
         i7= hx[t].x[7];

         for( v=0; v<3; v++ ) //write coords
        {
            fprintf( f," % 14.7e % 14.7e % 14.7e % 14.7e % 14.7e % 14.7e % 14.7e % 14.7e \n", 
                         vx[i0][v], vx[i1][v], vx[i2][v], vx[i3][v],
                         vx[i4][v], vx[i5][v], vx[i6][v], vx[i7][v] );
        } //coords

         fprintf( f," %d %d %d %d\n", hx[t].iz[0] ,hx[t].iz[1] ,hx[t].iz[2] ,hx[t].iz[3] );

//       printf( "%d %d %d %d\n", hx[t].iz[0], hx[t].iz[1], hx[t].iz[2], hx[t].iz[3] );

         for( v=0; v<3; v++ ) //write distances
        {
            fprintf( f," % 14.7e % 14.7e % 14.7e % 14.7e % 14.7e % 14.7e % 14.7e % 14.7e \n", 
                         hx[t].d[0][v], hx[t].d[1][v], hx[t].d[2][v], hx[t].d[3][v],
                         hx[t].d[4][v], hx[t].d[5][v], hx[t].d[6][v], hx[t].d[7][v] );
        } //distance
         fprintf( f,"\n" );

     }
      fclose( f );
      
  }

//3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
//       1         2         3         4         5         6         7         8         9         0         1         2
//
// Author          Luca di Mare <l.di.mare@ic.ac.uk>
// Created         Tue  7 Aug 00:34:20 BST 2018
// Changes History
// Next Change(s)  -
   
   void hmesh_t::btec( const char *name )
  {
      INT_ v,b;
      INT_ i,j;
      INT_ ist,ien;

      FILE *f;
      f=fopen( name,"w" );

      //write file header

      fprintf( f, "TITLE = \"q3x data\"\n" );
      fprintf( f, "FILETYPE = GRID\n");
      fprintf( f, "Variables = \"X\", \"Y\", \"Z\" \"U\" \"V\" \"W\"" );
      fprintf( f,"\n" );

//    assert( bz[nb].bo.n==0 );


      for( b=0;b<nb;b++ )   //boundary surface
     {

         if( bz[b].bo.n > 0 && b < (MXGRP-1))
        {
         
            fprintf( f, "ZONE " );
            fprintf( f, "T=\"SURFACE %d\", ",b );
            fprintf( f, "NODES=%d, ELEMENTS=%d, ZONETYPE=FEQUADRILATERAL, ", bz[b].bx.n,bz[b].bo.n );

            fprintf( f, "DATAPACKING=BLOCK\n" );
            for( v=0;v<3;v++ )
           {
               ien=0;
               do
              {
                  ist= ien;
                  ien+= 128;
                  ien= min( ien,bz[b].bx.n );
                  for( j=ist;j<ien;j++ )
                 {
                     i= bz[b].bx[j].id;   
                     if( i > -1 )
                    {
                        fprintf( f,"% 14.7e ",vx[i][v] );
                    }
                     else
                    {
                        fprintf( f,"% 14.7e ",0. );
                    }
                 }
                  fprintf( f,"\n" );
              }while( ien < bz[b].bx.n );
           }
            for( v=0;v<3;v++ )
           {
               ien=0;
               do
              {
                  ist= ien;
                  ien+= 128;
                  ien= min( ien,bz[b].bx.n );
                  for( j=ist;j<ien;j++ )
                 {
                     i= bz[b].bx[j].id;   
                     if( i > -1 )
                    {
                        fprintf( f,"% 14.7e ",bz[b].bx[j].y[v] );
                    }
                     else
                    {
                        fprintf( f,"% 14.7e ",0. );
                    }
                 }
                  fprintf( f,"\n" );
              }while( ien < bz[b].bx.n );
           }
            for( j=0;j<bz[b].bo.n;j++ )
           {    
               fprintf( f,"%6d %6d %6d %6d\n", 1+bz[b].bo[j].v[0], 1+bz[b].bo[j].v[1], 
                                               1+bz[b].bo[j].v[2], 1+bz[b].bo[j].v[3]
);        
           }
        }
     }
      fclose( f );
      
  }

   void hmesh_t::dump()
  {
      INT_ i,j;
      printf( "\n" );
      printf( "%3d %3d \n", nb,ng );
      for( i=0;i<hx.n;i++ )
     {
         printf( "%3d: [",i );
         for( j=0;j<8;j++ ){ printf( " %3d",hx[i].v[j]   );  } printf( "] [" );for( j=0;j<6;j++ ){ printf( " %3d",hx[i].q[j].q ); }printf( "]" ); 
         printf( "\n" );
     }
      printf( "\n" );
      for( i=0;i<qv.n;i++ )
     {
         printf( "%3d: [",i );
         for( j=0;j<2;j++ ){ printf( " %3d",qv[i].h[j]   );  }printf( "] [" ); for( j=0;j<2;j++ ){ printf( " %3d",qv[i].q[j] );  }printf( "]" );
         printf( "\n" );
     }
  }

   void hmesh_t::box( REAL_3 x0, REAL_3 x1, VINT_ &list )
  {
      INT_  i,j,k,l;
      VINT_ wrk(vx.n,-1);
      list.n=0;
      for( i=0;i<vx.n;i++ )
     {
         if( ( ( x0[0]< vx[i][0] ) && ( vx[i][0] < x1[0] ) ) &&
             ( ( x0[1]< vx[i][1] ) && ( vx[i][1] < x1[1] ) ) &&
             ( ( x0[2]< vx[i][2] ) && ( vx[i][2] < x1[2] ) ) )
        {
            wrk[i]=0; 
        }
     }

      for( i=0;i<hx.n;i++ )
     {
         l=0;
         for( j=0;j<8;j++ )
        {
            k= hx[i].x[j];
            if( wrk[k] > -1 ){ l++; };
        }
         if( l == 8 ){ l= list.append(-1); list[l]= hx[i].id; };
     }
  }

   void hmesh_t::vtk( const char *name )
  {
      INT_  i,j;
      INT_8 x;
      INT_  listsize, celltype;

      FILE *f; 
      f=fopen( name,"w" );

      fprintf( f, "# vtk DataFile Version 2.0\n" );

      fprintf( f, "h3x block layout\n" );

      fprintf( f, "ASCII\n" );
      fprintf( f, "DATASET UNSTRUCTURED_GRID\n" );

      fprintf( f, "POINTS %d float\n", vx.n );

   // write point coordinates
      for( i=0; i<vx.n; i++ )
     {   
         fprintf( f, "%f %f %f\n", vx[i][0], vx[i][1], vx[i][2] );
     }   

      fprintf( f, "\n" );

   // need 8*hx.n numbers for each vertex on each cell, plus hx.n numbers for cell ids
      listsize= 8*hx.n + hx.n;
      celltype= 12; // VTK_HEX

   // write connectivity
      fprintf( f, "CELLS %d %d\n", hx.n, listsize );
      for( i=0; i<hx.n; i++ )
     {   
         for( j=0; j<8; j++ ){ x[j]=hx[i].x[j]; }

         fprintf( f, "%d %d %d %d %d %d %d %d %d\n",
                  8,  x[0], x[1], x[3], x[2],
                      x[4], x[5], x[7], x[6] );
     }   

      fprintf( f, "\n" );

   // write cell types
      fprintf( f, "CELL_TYPES %d\n", hx.n );
      for( i=0; i<hx.n; i++ )
     {   
         fprintf( f, "%d\n", celltype );
     }   
    
      fclose( f );
    
      return;
  }

